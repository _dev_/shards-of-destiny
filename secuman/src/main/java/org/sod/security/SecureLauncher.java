/**
 * L2 Shards of Destiny<BR>
 * Security pack by savormix
 */
package org.sod.security;

import java.io.File;
import java.util.zip.Adler32;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.l2jfree.versionning.Locator;

/**
 * @author savormix
 * 
 * @param <E> Element
 * @param <T> A comparable type
 */
public final class SecureLauncher<E, T extends Comparable<E>> {
	private static final Log _log = LogFactory.getLog(SecureLauncher.class);
	private static final String _mainClass = "com.l2jfree.gameserver.GameServer";
	private static SecureLauncher<?, ?> _serial;

	private final E _e;
	private final T _t;

	private SecureLauncher(E e, T t) {
		_e = e;
		_t = t;
	}

	/**
	 * A validator that can verify the argument.
	 * @return the validator
	 */
	public final boolean validate() {
		return _t.compareTo(_e) == 0;
	}

	/**
	 * @return launcher used to launch the application
	 */
	public static SecureLauncher<?, ?> getInstance() {
		return _serial;
	}

	/**
	 * @param args nothing
	 */
	public static void main(String[] args) {
		Long l = Long.valueOf(args[0], 13);
		System.setProperty("KeyRef", l.toString());
		File f = new File("Shards of Destiny is one of the best servers.\u0097\u0666\u04C3");
		Adler32 valid = new Adler32();
		try {
			valid.update(f.getCanonicalPath().getBytes("UTF-8"));
		} catch (Exception e) {
			_log.error("Cannot obtain file path!", e);
		}
		_serial = new SecureLauncher<Long, Long>(valid.getValue(), l);
		if (!_serial.validate())
			_log.warn("Cannot validate integrity code. Please use the proper tool to generate a valid code " +
			"to ensure a loophole-free server.");
		try {
			Class<?> loader = Class.forName(_mainClass);
			File jar = Locator.getClassSource(loader);
			if (jar.getName().contains(".jar"))
			{
				String signed = String.valueOf(SecureLauncher.class.getSigners()[0]);
				_log.info(signed);
			}
			loader.getMethod("main", String[].class).invoke(null, new Object[] { args });
		} catch (Throwable e) {
			_log.error("Deployed serverpack is damaged."/*, e*/);
		}
	}
}
