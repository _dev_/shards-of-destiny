/**
 * L2 Shards of Destiny<BR>
 * Security pack by savormix
 */
package org.sod.security.terminal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

/**
 * @author savormix
 *
 */
public class Commander {
	private Commander() {
	}

	/**
	 * Still undecided
	 * @param cmd command?
	 * @param out stream?
	 */
	public void executeShellCmd(String cmd, PrintStream out) {
		try {
			ProcessBuilder pb = new ProcessBuilder(cmd);
			pb.redirectErrorStream(true);
			Process p = pb.start();
			BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line;
			while ((line = br.readLine()) != null)
				out.write(line.getBytes());
			br.close();
		} catch (IOException e) {
			e.printStackTrace(out);
		}
	}

	/**
	 * @return the command interface
	 */
	public static final Commander getInstance() {
		return SingletonHolder._instance;
	}

	@SuppressWarnings("synthetic-access")
	private static final class SingletonHolder {
		private static final Commander _instance = new Commander();
	}
}
