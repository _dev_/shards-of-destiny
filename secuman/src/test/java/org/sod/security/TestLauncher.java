/**
 * L2 Shards of Destiny<BR>
 * Security pack by savormix
 */
package org.sod.security;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

/**
 * @author savormix
 *
 */
public class TestLauncher {

	/**
	 * @throws java.lang.Exception any exception
	 */
	@Before
	public void setUp() throws Exception {
		SecureLauncher.main(new String[] { "-1" });
	}

	/**
	 * Test method for {@link org.sod.security.SecureLauncher#getInstance()}.
	 */
	@Test
	public final void testGetInstance() {
		assertNotNull(SecureLauncher.getInstance());
		assertNotNull(System.getProperty("KeyRef"));
	}

	/**
	 * Test method for {@link org.sod.security.SecureLauncher#validate()}.
	 */
	@Test
	public final void testValidate() {
		assertFalse(SecureLauncher.getInstance().validate());
	}
}
