/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.network;

import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

/**
 * @author savormix
 *
 */
public class SelectorThread extends Thread
{
	private SelectorThread()
	{
		super("L2jEmulatorThread");
		setPriority(MAX_PRIORITY);
	}

	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run()
	{
		System.out.println("L2J Emulator initializing...");
		try
		{
			Selector s = Selector.open();
			ServerSocketChannel authd = ServerSocketChannel.open();
			authd.configureBlocking(false);
			authd.socket().bind(new InetSocketAddress(2106));
			authd.register(s, SelectionKey.OP_ACCEPT);
			System.out.println("Bound AuthD on port 2106!");
			ServerSocketChannel gamed = ServerSocketChannel.open();
			gamed.configureBlocking(false);
			gamed.socket().bind(new InetSocketAddress(7777));
			gamed.register(s, SelectionKey.OP_ACCEPT);
			System.out.println("Bound GameD on port 7777!");
			while (true)
			{
				s.select();
				Iterator<SelectionKey> i = s.selectedKeys().iterator();
				while (i.hasNext())
				{
					SelectionKey sk = i.next();
					i.remove();
					if (sk.isValid() && sk.isAcceptable())
					{
						ServerSocketChannel ssc = (ServerSocketChannel) sk.channel();
						SocketChannel sc = ssc.accept();
						if (sc != null)
						{
							sc.configureBlocking(false);
							sc.close();
						}
						else // connection terminated
							continue;
					}
				}
				Thread.sleep(50);
			}
		}
		catch (Exception e)
		{
			System.out.println("Unknown error!");
			e.printStackTrace();
		}
	}

	/**
	 * @param args N/A
	 */
	public static void main(String[] args)
	{
		new SelectorThread().start();
	}
}
