package org.sod.security;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Generates an answer to the question given by .__req_typedef
 * voiced command.<BR>
 * Answer should be .version <generated code>
 * @author savormix
 */
public final class HashProducer {
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the question number: ");
		int quest = Integer.parseInt(br.readLine());
		String s = Integer.toString(quest >> 2, 7);
		System.out.println(s);
		//int ans = Integer.parseInt(s, 7);
		//System.out.println(ans << 2);
	}
}
