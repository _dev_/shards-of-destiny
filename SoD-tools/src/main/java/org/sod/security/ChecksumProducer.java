package org.sod.security;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.zip.Adler32;

/**
 * Generates the path checksum which must be passed as an argument to the
 * SecureLauncher.
 * @author savormix
 */
public final class ChecksumProducer {
	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the path where JAR is: ");
		String path = br.readLine();
		File f = new File(path, "Shards of Destiny is one of the best servers.\u0097\u0666\u04C3");
		Adler32 check = new Adler32();
		try {
			check.update(f.getCanonicalPath().getBytes("UTF-8"));
		} catch (Exception e) {
			//_log.error("Cannot obtain file path!", e);
			e.printStackTrace();
		}
		String s = Long.toString(check.getValue(), 13);
		System.out.println(s);
		//System.out.println(Long.parseLong(s, 13));
	}
}
