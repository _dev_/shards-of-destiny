/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.ai;

import static com.l2jfree.gameserver.ai.CtrlIntention.AI_INTENTION_ATTACK;

import org.sod.model.conquest.ConquestZoneMember;

import com.l2jfree.gameserver.geodata.GeoData;
import com.l2jfree.gameserver.model.actor.L2Attackable;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.L2Playable;
import com.l2jfree.gameserver.model.actor.L2Character.AIAccessor;
import com.l2jfree.gameserver.model.actor.instance.L2ZoneGuardInstance;
import com.l2jfree.gameserver.model.actor.instance.L2ZoneSpawnGuardInstance;
import com.l2jfree.gameserver.model.zone.L2FactionZone;
import com.l2jfree.gameserver.util.Util;

/**
 * TODO Auto-generated JavaDoc
 * @author savormix
 * @since 2009.12.29
 */
@Deprecated
public final class L2ZoneTestAI extends L2ControllableMobAI
{
	private volatile boolean			_ready;

	public L2ZoneTestAI(AIAccessor accessor)
	{
		super(accessor);
		if (!(getActor() instanceof ConquestZoneMember))
			_log.info("Zone AI only works with faction zone members.", new IllegalStateException());
	}

	private final int square(int i)
	{
		return i * i;
	}

	public final void invalidate(L2Character cha)
	{
		if (getAttackTarget() == cha || getCastTarget() == cha)
		{
			getActor().reduceHate(cha, Integer.MAX_VALUE);
			notifyEvent(CtrlEvent.EVT_FORGET_OBJECT, cha);
			changeIntention(CtrlIntention.AI_INTENTION_ACTIVE, null, null);
		}
	}

	public L2Playable getNextTarget()
	{
		L2FactionZone master = ((ConquestZoneMember) getActor()).getFactionZone();
		if (master == null)
			return null;
		L2Attackable me = getActor();
		boolean noMove = (me instanceof L2ZoneSpawnGuardInstance);

		for (L2Character cha : master.getCharactersInsideActivated())
		{
			if (cha == null || cha.isDead() || !(cha instanceof L2Playable))
				continue;
			if (noMove
					&& (!Util.checkIfInShortRadius(me.getPhysicalAttackRange(), me, cha, false)
					|| !GeoData.getInstance().canSeeTarget(me, cha))
				)
				continue;

			if (master.isEnemy(cha.getActingPlayer()))
				return (L2Playable) cha;
		}

		return null;
	}

	// not sure if it's worth keeping (from earlier AI impl)
	private final boolean isReady()
	{
		return _ready;
	}

	@Override
	protected void onEvtThink()
	{
		if (isThinking())
			return;

		setThinking(true);

		try
		{
			if (getIntention() == AI_INTENTION_ATTACK)
				thinkAttack();
			else
				thinkActive();
		}
		finally
		{
			setThinking(false);
		}
	}

	@Override
	protected void thinkActive()
	{
		L2Playable hated = getNextTarget();
		if (hated == null)
			return;

		L2Attackable me = getActor();
		me.setRunning();
		if (me instanceof L2ZoneGuardInstance)
		{
			double dist2 = me.getPlanDistanceSq(hated);
			if (dist2 > 160000) // more than 400
			{
				//if (System.currentTimeMillis() > _nextTalk)
				//{
				//	_nextTalk = System.currentTimeMillis() + 4000;
				//	me.broadcastPacket(new CreatureSay(me.getObjectId(), SystemChatChannelId.Chat_Normal, me.getName(), hated.getName() + " runner noob!11! poutaka malaka!"));
				//}
				//me.broadcastPacket(_tele1);
				me.teleToLocation(hated.getX(), hated.getY(), hated.getZ(), hated.getHeading(), true);
			}
		}
		setIntention(CtrlIntention.AI_INTENTION_ATTACK, hated);
	}

	@Override
	protected void thinkAttack()
	{
		L2Character cha = getAttackTarget();
		L2Attackable me = getActor();
		if (cha == null || cha.isDead() || me.getPlanDistanceSq(cha) > 2250000)
		{
			invalidate(cha);
			return;
		}

		int physRange = (int) (me.getPhysicalAttackRange() + me.getCollisionRadius() + cha.getTemplate().getCollisionRadius());
		if (!isReady() && me.getPlanDistanceSq(cha) > square(physRange + 20))
		{
			moveToPawn(cha, 5);
			return;
		}
		_accessor.doAttack(getAttackTarget());
	}
}
