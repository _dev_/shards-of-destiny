/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.ai;

import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.L2Playable;
import com.l2jfree.gameserver.model.actor.L2Character.AIAccessor;
import com.l2jfree.gameserver.model.actor.instance.L2ZoneServitorInstance;
import com.l2jfree.gameserver.model.zone.L2FactionZone;
import com.l2jfree.gameserver.network.serverpackets.MagicSkillLaunched;
import com.l2jfree.gameserver.network.serverpackets.MagicSkillUse;
import com.l2jfree.gameserver.util.Util;

/**
 * AI for the unholy zone servitor, that can be summoned by casting "Awakening"
 * skill on the artefact. All he does is selects an enemy in the zone, teleports
 * to him and starts attacking. When he manages to kill an enemy, he then selects
 * another one or waits.
 * @author savormix
 * @since 2009.12.04
 * @deprecated replaced with L2ZoneTestAI
 */
@Deprecated
public final class L2ZoneServitorAI extends L2AttackableAI
{
	private volatile boolean			_thinking;
	private boolean						_ready;
	private L2FactionZone				_zone;
	private final MagicSkillUse			_tele1;
	private final MagicSkillLaunched	_tele2;

	public L2ZoneServitorAI(AIAccessor accessor)
	{
		super(accessor);
		_thinking = false;
		_zone = null;
		_tele1 = new MagicSkillUse(getActor(), 2013, 1, 100, 0);
		_tele2 = new MagicSkillLaunched(getActor(), 2013, 1);
		setIntention(CtrlIntention.AI_INTENTION_IDLE);
	}

	/** @return the master zone */
	public final L2FactionZone getZone()
	{
		return _zone;
	}

	/**
	 * Assigns a zone so this servitor would attack only enemies
	 * in zone, but not in knownlist.
	 * @param zone the master zone to set
	 */
	public final void setZone(L2FactionZone zone)
	{
		_zone = zone;
		notifyEvent(CtrlEvent.EVT_THINK);
	}

	private L2Playable getNextTarget()
	{
		L2FactionZone master = getZone();
		if (master == null)
			return null;

		for (L2Character cha : master.getCharactersInsideActivated())
		{
			if (cha == null || cha.isDead() || !(cha instanceof L2Playable))
				continue;

			if (master.isEnemy(cha.getActingPlayer()))
				return (L2Playable) cha;
		}

		return null;
	}

	@Override
	protected void thinkActive()
	{
		L2Playable hated = getNextTarget();
		if (hated == null)
			return;

		setAttackTarget(hated);
		L2ZoneServitorInstance me = getActor();
		me.setRunning();
		double dist2 = me.getPlanDistanceSq(hated);
		if (dist2 > 160000) // more than 150
		{
			me.broadcastPacket(_tele1);
			me.broadcastPacket(_tele2);
			me.teleToLocation(hated.getX(), hated.getY(), hated.getZ(), hated.getHeading(), true);
		}
		// cast a debuff first, then attack?
		setIntention(CtrlIntention.AI_INTENTION_ATTACK, hated);
	}

	private void thinkCast()
	{
		L2Character cha = getAttackTarget();
		if (cha == null || cha.isDead())
		{
			setAttackTarget(getNextTarget());
			clientStopMoving(null);
		}
		cha = getAttackTarget();
		if (cha == null)
		{
			setIntention(CtrlIntention.AI_INTENTION_ACTIVE);
			return;
		}
		L2ZoneServitorInstance me = getActor();
		me.setTarget(cha);
		if (me.isMuted()) // can't cast - attack
		{
			setIntention(CtrlIntention.AI_INTENTION_ATTACK);
			return;
		}
		L2Skill[] skills = me.getAllSkills();
		int maxRange = 0;
		for (L2Skill sk : skills)
		{
			if (Util.checkIfInRange(sk.getCastRange(), me, getAttackTarget(), true) && !me.isSkillDisabled(sk.getId())
					&& me.getStatus().getCurrentMp() > me.getStat().getMpConsume(sk)
					&& sk.isDebuff()) // only debuffs
			{
				_accessor.doCast(sk);
				setIntention(CtrlIntention.AI_INTENTION_ATTACK);
				return;
			}

			maxRange = Math.max(maxRange, sk.getCastRange());
		}
		moveToPawn(cha, maxRange);
	}

	@Override
	protected void thinkAttack()
	{
		L2Character cha = getAttackTarget();
		L2ZoneServitorInstance me = getActor();
		double dist2 = me.getPlanDistanceSq(cha);
		if (cha == null || cha.isDead() || dist2 > 120000)
		{
			if (cha != null)
				me.stopHating(cha);
			setIntention(CtrlIntention.AI_INTENTION_ACTIVE);
			return;
		}

		//L2Skill[] skills = me.getAllSkills();
		int physRange = (int) (me.getPhysicalAttackRange() + me.getCollisionRadius() + cha.getTemplate().getCollisionRadius());
		// move only if haven't moved!!
		if (!isReady() && dist2 > square(physRange + 20))
		{
			_ready = true;
			moveToPawn(cha, 5);
			return;
		}
		_ready = false;
/*		int skillRange = physRange;

		if (!me.isMuted() && dist2 > square(physRange + 20))
		{
			for (L2Skill sk : skills)
			{
				int castRange = sk.getCastRange();
				if (square(castRange) >= dist2 && !me.isSkillDisabled(sk.getId())
						&& me.getStatus().getCurrentMp() > me.getStat().getMpConsume(sk))
				{
					_accessor.doCast(sk);
					return;
				}

				skillRange = Math.max(skillRange, castRange);
			}

			//moveToPawn(cha, skillRange);
			me.broadcastPacket(_teleport);
			me.teleToLocation(cha.getX(), cha.getY(), cha.getZ(), cha.getHeading(), true);
			return;
		}

		if (!me.isMuted() && skills.length > 0 && Rnd.nextInt(5) == 3)
			for (L2Skill sk : skills)
				if (square(sk.getCastRange()) >= dist2 && !me.isSkillDisabled(sk.getId())
						&& me.getStatus().getCurrentMp() < me.getStat().getMpConsume(sk))
				{
					_accessor.doCast(sk);
					return;
				}
*/
		_accessor.doAttack(getAttackTarget());
	}

	private final int square(int i)
	{
		return i * i;
	}

	public final void invalidate(L2Character cha)
	{
		if (getAttackTarget() == cha)
		{
			getActor().stopHating(cha);
			setAttackTarget(null);
			setIntention(CtrlIntention.AI_INTENTION_ACTIVE);
		}
	}

	private final boolean isReady()
	{
		return _ready;
	}

	@Override
	public L2ZoneServitorInstance getActor()
	{
		return (L2ZoneServitorInstance) super.getActor();
	}

	@Override
	protected void onEvtThink()
	{
		if (_thinking)
			return;

		try
		{
			switch (getIntention())
			{
			case AI_INTENTION_IDLE:
			case AI_INTENTION_ACTIVE:
				thinkActive();
				break;
			case AI_INTENTION_ATTACK:
				thinkAttack();
				break;
			case AI_INTENTION_CAST:
				thinkCast();
				break;
			}
		}
		finally
		{
			_thinking = false;
		}
	}
}
