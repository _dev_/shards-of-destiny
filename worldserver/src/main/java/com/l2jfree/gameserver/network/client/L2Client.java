/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.network.client;

import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SocketChannel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javolution.util.FastList;

import com.l2jfree.ClientProtocolVersion;
import com.l2jfree.Config;
import com.l2jfree.L2DatabaseFactory;
import com.l2jfree.gameserver.LoginServerThread;
import com.l2jfree.gameserver.ThreadPoolManager;
import com.l2jfree.gameserver.datatables.ClanTable;
import com.l2jfree.gameserver.model.CharSelectInfoPackage;
import com.l2jfree.gameserver.model.L2Clan;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.Disconnection;
import com.l2jfree.gameserver.network.client.packets.L2ClientPacket;
import com.l2jfree.gameserver.network.client.packets.L2ServerPacket;
import com.l2jfree.gameserver.network.client.packets.sendable.LeaveWorld.CloseClient;
import com.l2jfree.gameserver.network.client.packets.sendable.ServerClose;
import com.l2jfree.gameserver.network.client.packets.sendable.ServerCloseSocketPacket.ConnectionTerminated;
import com.l2jfree.gameserver.network.clientpackets.L2GameClientPacket;
import com.l2jfree.gameserver.threadmanager.FIFORunnableQueue;
import com.l2jfree.gameserver.util.TableOptimizer;
import com.l2jfree.gameserver.util.TableOptimizer.CharacterRelatedTable;
import com.l2jfree.gameserver.util.TableOptimizer.ItemRelatedTable;
import com.l2jfree.lang.L2TextBuilder;
import com.l2jfree.network.mmocore.DataSizeHolder;
import com.l2jfree.network.mmocore.MMOConnection;
import com.l2jfree.network.mmocore.MMOController;
import com.l2jfree.security.CoreCipher;
import com.l2jfree.security.EmptyCipher;
import com.l2jfree.security.ICipher;
import com.l2jfree.security.ObfuscationService;
import com.l2jfree.util.concurrent.RunnableStatsManager;
import com.l2jfree.util.logging.L2Logger;

/**
 * Represents a client connected on Game Server
 * 
 * @author KenM
 */
public final class L2Client extends MMOConnection<L2Client, L2ClientPacket, L2ServerPacket> implements IL2Client
{
	private static final L2Logger _log = L2Logger.getLogger(L2Client.class);
	
	private ClientProtocolVersion _version;
	private ObfuscationService _deobfuscator;
	private ICipher _cipher;
	
	private volatile L2ClientState _state;
	private int _bitsInBlock;
	
	private String _accountName;
	private L2PcInstance _activeChar;
	// private PersistentId[] _playerSlotMap;
	private int[] _charSlotMapping;
	private int _sessionId;
	
	private volatile boolean _disconnected;
	
	private boolean _isAuthedGG;
	
	public L2Client(MMOController<L2Client, L2ClientPacket, L2ServerPacket> mmoController, SocketChannel socketChannel)
			throws ClosedChannelException
	{
		super(mmoController, socketChannel);
		
		_cipher = new EmptyCipher();
		
		_state = L2ClientState.CONNECTED;
		_bitsInBlock = 0;
		
		// default (before we actually know it) and not important
		setVersion(ClientProtocolVersion.FREYA);
	}
	
	public ClientProtocolVersion getVersion()
	{
		return _version;
	}
	
	public void setVersion(ClientProtocolVersion version)
	{
		_version = version;
		_deobfuscator = new ObfuscationService(version);
	}
	
	private ICipher getCipher()
	{
		return _cipher;
	}
	
	public void setCipher(CoreCipher cipher)
	{
		_cipher = cipher;
	}
	
	/**
	 * Returns the packet opcode deobfuscator.
	 * 
	 * @return opcode deobfuscator
	 */
	public ObfuscationService getDeobfuscator()
	{
		return _deobfuscator;
	}
	
	/**
	 * Returns current connection's state.
	 * 
	 * @return connection's state
	 */
	@SuppressWarnings("unchecked")
	@Override
	public L2ClientState getState()
	{
		return _state;
	}
	
	/**
	 * Changes the connection's state.
	 * 
	 * @param state
	 *            connection's state
	 */
	public void setState(L2ClientState state)
	{
		_state = state;
	}
	
	/**
	 * Returns how many bits to demand client to process in regular intervals.
	 * 
	 * @return bits in block
	 */
	public int getBitsInBlock()
	{
		return _bitsInBlock;
	}
	
	/**
	 * Changes how many bits to demand client to process in regular intervals.
	 * 
	 * @param bitsInBlock
	 *            bit count (should be <TT>bitsInBlock & 7 == 0</TT>)
	 */
	public void setBitsInBlock(int bitsInBlock)
	{
		_bitsInBlock = bitsInBlock;
	}
	
	@Override
	protected boolean decipher(ByteBuffer buf, DataSizeHolder size)
	{
		// at this point, cipher cannot be null
		getCipher().decipher(buf, size.getSize());
		
		getDeobfuscator().decodeOpcodes(buf, size.getSize());
		return true;
	}
	
	@Override
	protected boolean encipher(ByteBuffer buf, int size)
	{
		getCipher().encipher(buf, size);
		
		buf.position(buf.position() + size);
		return true;
	}
	
	@Override
	protected String getUID()
	{
		return getAccountName();
	}
	
	@Override
	protected boolean isAuthed()
	{
		return getState() != L2ClientState.CONNECTED;
	}
	
	public L2PcInstance getActiveChar()
	{
		return _activeChar;
	}
	
	@Override
	public void setActiveChar(L2PcInstance pActiveChar)
	{
		_activeChar = pActiveChar;
	}
	
	public void setGameGuardOk(boolean val)
	{
		_isAuthedGG = val;
	}
	
	public boolean isAuthedGG()
	{
		return _isAuthedGG;
	}
	
	public void setAccountName(String pAccountName)
	{
		_accountName = pAccountName;
	}
	
	public String getAccountName()
	{
		return _accountName;
	}
	
	public int getSessionId()
	{
		return _sessionId;
	}
	
	/**
	 * @param sessionId
	 */
	public void setSessionId(int sessionId)
	{
		_sessionId = sessionId;
	}
	
	/**
	 * Method to handle character deletion
	 * 
	 * @return a byte: <li>-1: Error: No char was found for such charslot, caught exception, etc... <li>0: character is
	 *         not member of any clan, proceed with deletion <li>1: character is member of a clan, but not clan leader
	 *         <li>2: character is clan leader
	 */
	public byte markToDeleteChar(int charslot)
	{
		int objid = getObjectIdForSlot(charslot);
		
		if (objid < 0)
			return -1;
		
		byte result = -1;
		
		Connection con = null;
		
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection(con);
			PreparedStatement statement = con.prepareStatement("SELECT clanId from characters WHERE charId=?");
			statement.setInt(1, objid);
			ResultSet rs = statement.executeQuery();
			byte answer = -1;
			if (rs.next())
			{
				int clanId = rs.getInt(1);
				if (clanId != 0)
				{
					L2Clan clan = ClanTable.getInstance().getClan(clanId);
					
					if (clan == null)
						answer = 0; // jeezes!
					else if (clan.getLeaderId() == objid)
						answer = 2;
					else
						answer = 1;
				}
				else
					answer = 0;
				
				// Setting delete time
				if (answer == 0)
				{
					if (Config.DELETE_DAYS == 0)
					{
						deleteCharByObjId(objid);
					}
					else
					{
						statement = con.prepareStatement("UPDATE characters SET deletetime=? WHERE charId=?");
						statement.setLong(1, System.currentTimeMillis() + Config.DELETE_DAYS * 86400000L); // 24*60*60*1000
																											// =
																											// 86400000
						statement.setInt(2, objid);
						statement.execute();
						statement.close();
					}
				}
			}
			result = answer;
			rs.close();
			statement.close();
		}
		catch (Exception e)
		{
			_log.error("Error updating delete time of character.", e);
			result = -1;
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
		
		return result;
	}
	
	public void markRestoredChar(int charslot) throws Exception
	{
		// have to make sure active character must be nulled
		/*
		 * if (getActiveChar() != null) { saveCharToDisk (getActiveChar()); if
		 * (_log.isDebugEnabled()) _log.debug("active Char saved"); _activeChar
		 * = null; }
		 */
		
		int objid = getObjectIdForSlot(charslot);
		if (objid < 0)
			return;
		
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection(con);
			PreparedStatement statement = con.prepareStatement("UPDATE characters SET deletetime=0 WHERE charId=?");
			statement.setInt(1, objid);
			statement.execute();
			statement.close();
		}
		catch (Exception e)
		{
			_log.error("Error restoring character.", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	public static void deleteCharByObjId(int objid)
	{
		if (objid < 0)
			return;
		
		Connection con = null;
		
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection(con);
			PreparedStatement statement;
			
			for (ItemRelatedTable table : TableOptimizer.getItemRelatedTables())
			{
				statement = con.prepareStatement(table.getDeleteQuery());
				statement.setInt(1, objid);
				statement.execute();
				statement.close();
			}
			
			for (CharacterRelatedTable table : TableOptimizer.getCharacterRelatedTables())
			{
				statement = con.prepareStatement(table.getDeleteQuery());
				statement.setInt(1, objid);
				statement.execute();
				statement.close();
			}
			
			statement = con.prepareStatement("DELETE FROM items WHERE owner_id=?");
			statement.setInt(1, objid);
			statement.execute();
			statement.close();
			
			statement = con.prepareStatement("DELETE FROM characters WHERE charId=?");
			statement.setInt(1, objid);
			statement.execute();
			statement.close();
			
			statement = con.prepareStatement("DELETE FROM bbs_memo_auction WHERE ownerId=? AND bidderId=?");
			statement.setInt(1, objid);
			statement.setInt(2, objid);
			statement.execute();
			statement.close();
			
			statement = con.prepareStatement("DELETE FROM char_buff_templates WHERE charId=?");
			statement.setInt(1, objid);
			statement.execute();
			statement.close();
			
			statement = con.prepareStatement("DELETE FROM character_bounty WHERE charId=?");
			statement.setInt(1, objid);
			statement.execute();
			statement.close();
			
			statement = con.prepareStatement("DELETE FROM character_challenges WHERE charId=?");
			statement.setInt(1, objid);
			statement.execute();
			statement.close();
			
			statement = con.prepareStatement("DELETE FROM character_factions WHERE charId=?");
			statement.setInt(1, objid);
			statement.execute();
			statement.close();
			
			statement = con.prepareStatement("DELETE FROM character_jail WHERE charId=?");
			statement.setInt(1, objid);
			statement.execute();
			statement.close();
			
			statement = con.prepareStatement("DELETE FROM character_perks WHERE charId=?");
			statement.setInt(1, objid);
			statement.execute();
			statement.close();
			
			statement = con.prepareStatement("DELETE FROM character_records WHERE charId=?");
			statement.setInt(1, objid);
			statement.execute();
			statement.close();
			
			statement = con.prepareStatement("DELETE FROM character_settings WHERE charId=?");
			statement.setInt(1, objid);
			statement.execute();
			statement.close();
			
			statement = con.prepareStatement("DELETE FROM donators WHERE charId=?");
			statement.setInt(1, objid);
			statement.execute();
			statement.close();
		}
		catch (Exception e)
		{
			_log.error("Error deleting character.", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	public L2PcInstance loadCharFromDisk(int charslot)
	{
		return L2PcInstance.load(getObjectIdForSlot(charslot));
	}
	
	/**
	 * @param chars
	 */
	public void setCharSelection(CharSelectInfoPackage[] chars)
	{
		_charSlotMapping = new int[chars.length];
		
		int i = 0;
		for (CharSelectInfoPackage element : chars)
			_charSlotMapping[i++] = element.getObjectId();
	}
	
	/**
	 * @param charslot
	 * @return
	 */
	private int getObjectIdForSlot(int charslot)
	{
		if (_charSlotMapping == null || charslot < 0 || charslot >= _charSlotMapping.length)
		{
			_log.warn(toString() + " tried to delete Character in slot " + charslot
					+ " but no characters exits at that slot.");
			return -1;
		}
		
		return _charSlotMapping[charslot];
	}
	
	@Override
	public String toString()
	{
		L2TextBuilder tb = new L2TextBuilder();
		
		tb.append("[State: ").append(getState());
		
		String host = getHostAddress();
		if (host != null)
			tb.append(" | IP: ").append(String.format("%-15s", host));
		
		String account = getAccountName();
		if (account != null)
			tb.append(" | Account: ").append(String.format("%-15s", account));
		
		L2PcInstance player = getActiveChar();
		if (player != null)
			tb.append(" | Character: ").append(String.format("%-15s", player.getName()));
		
		tb.append("]");
		
		return tb.moveToString();
	}
	
	void execute(L2GameClientPacket rp)
	{
		getPacketQueue().execute(rp);
	}
	
	private FIFORunnableQueue<Runnable> _packetQueue;
	
	public FIFORunnableQueue<Runnable> getPacketQueue()
	{
		if (_packetQueue == null)
			_packetQueue = new FIFORunnableQueue<Runnable>()
			{
			};
		
		return _packetQueue;
	}
	
	private final class ServerPacketQueue extends FastList<L2ServerPacket> implements Runnable
	{
		private static final long serialVersionUID = 6715576112277597425L;
		
		public ServerPacketQueue()
		{
			ThreadPoolManager.getInstance().schedule(this, Config.ENTERWORLD_TICK);
		}
		
		@Override
		public void run()
		{
			synchronized (L2Client.this)
			{
				if (_serverPacketQueue != this)
					return;
				
				if (isEmpty())
				{
					_serverPacketQueue = null;
					return;
				}
				
				int pn = size();
				if (pn > Config.ENTERWORLD_PPT)
					pn = Config.ENTERWORLD_PPT;
				
				for (int i = 0; i < pn; i++)
				{
					try
					{
						sendPacketImpl(removeFirst());
					}
					catch (Exception e)
					{
						_log.error("GDQ noob error (report in forum!) i=" + i + ", pn=" + pn, e);
					}
				}
				
				ThreadPoolManager.getInstance().schedule(this, Config.ENTERWORLD_TICK);
			}
		}
	}
	
	private ServerPacketQueue _serverPacketQueue;
	
	@Override
	public boolean sendPacket(L2ServerPacket sp)
	{
		if (_serverPacketQueue != null)
		{
			synchronized (this)
			{
				if (_serverPacketQueue != null)
				{
					_serverPacketQueue.add(sp);
					return true;
				}
			}
		}
		
		return sendPacketImpl(sp);
	}
	
	public void initServerPacketQueue()
	{
		if (!Config.ENTERWORLD_QUEUING)
			return;
		
		if (_serverPacketQueue == null)
		{
			synchronized (this)
			{
				if (_serverPacketQueue == null)
				{
					_serverPacketQueue = new ServerPacketQueue();
				}
			}
		}
	}
	
	/**
	 * {@link RunnableStatsManager} used here mostly for counting, since constructors - usually the longest parts - are
	 * excluded.
	 */
	private boolean sendPacketImpl(L2ServerPacket sp)
	{
		if (sp == null)
			return false;
		
		final long begin = System.nanoTime();
		final L2PcInstance activeChar = getActiveChar();
		
		try
		{
			if (_disconnected)
				return false;
			
			if (!sp.canBeSentTo(this, activeChar))
				return false;
			
			sp.prepareToSend(this, activeChar);
			
			if (!super.sendPacket(sp))
				return false;
			
			sp.packetSent(this, activeChar);
			return true;
		}
		finally
		{
			RunnableStatsManager.handleStats(sp.getClass(), "runImpl()", System.nanoTime() - begin);
		}
	}
	
	public synchronized void close(boolean toLoginScreen)
	{
		close(toLoginScreen ? ServerClose.PACKET : CloseClient.PACKET);
	}
	
	@Override
	public synchronized void close(L2ServerPacket sp)
	{
		super.close(sp);
		
		_disconnected = true;
		
		new Disconnection(this).defaultSequence(false);
	}
	
	@Override
	public synchronized void closeNow()
	{
		super.closeNow();
		
		_disconnected = true;
		
		new Disconnection(this).defaultSequence(false);
	}
	
	@Override
	protected L2ServerPacket getDefaultClosePacket()
	{
		// if (getVersion().isNewerThan(ClientProtocolVersion.GRACIA_FINAL))
		return ConnectionTerminated.PACKET;
	}
	
	@Override
	protected void onDisconnection()
	{
		L2ClientNetPing.getInstance().stopTask(this);
		
		_disconnected = true;
		
		new Disconnection(this).onDisconnection();
		LoginServerThread.getInstance().sendLogout(getAccountName());
	}
	
	@Override
	protected void onForcedDisconnection()
	{
		if (_log.isDebugEnabled())
			_log.info("Client " + toString() + " disconnected abnormally.");
	}
}
