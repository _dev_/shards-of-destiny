/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.network.client.packets.sendable;

import com.l2jfree.gameserver.model.L2Object;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.client.L2Client;
import com.l2jfree.gameserver.network.client.packets.L2ServerPacket;
import com.l2jfree.network.mmocore.MMOBuffer;

/**
 * @author savormix (generated)
 */
public abstract class PlaySoundPacket extends L2ServerPacket
{
	/**
	 * A nicer name for {@link PlaySoundPacket}.
	 * 
	 * @author savormix (generated)
	 * @see PlaySoundPacket
	 */
	public static final class PlaySound extends PlaySoundPacket
	{
		public PlaySound(SoundType type, CharSequence sound)
		{
			super(type.ordinal(), sound, null, 0);
		}
		
		public PlaySound(SoundType type, CharSequence sound, L2Object source, int radius)
		{
			super(type.ordinal(), sound, source, radius);
		}
	}
	
	private final int _type;
	private final CharSequence _sound;
	private final L2Object _source;
	private final int _radius;
	
	protected PlaySoundPacket(int type, CharSequence sound, L2Object source, int radius)
	{
		_type = type;
		_sound = sound;
		_source = source;
		_radius = radius;
	}
	
	@Override
	protected int getOpcode()
	{
		return 0x9e;
	}
	
	@Override
	protected void writeImpl(L2Client client, L2PcInstance activeChar, MMOBuffer buf) throws RuntimeException
	{
		// TODO: when implementing, consult an up-to-date packets_game_server.xml and/or savormix
		buf.writeD(_type); // Type
		buf.writeS(_sound); // File
		if (_source != null)
		{
			buf.writeD(true); // 3D (bind to object)
			buf.writeD(_source.getObjectId()); // Source OID
			buf.writeD(_source.getX()); // Location X
			buf.writeD(_source.getY()); // Location Y
			buf.writeD(_source.getZ()); // Location Z
			buf.writeD(_radius); // Radius??
		}
		else
			buf.write0(6 * 4);
	}
	
	public enum SoundType
	{
		SOUND, MUSIC, VOICE;
	}
}
