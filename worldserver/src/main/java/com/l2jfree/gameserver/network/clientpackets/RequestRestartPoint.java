/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.network.clientpackets;

import org.sod.manager.RespawnManager;

import com.l2jfree.gameserver.model.Location;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.restriction.global.GlobalRestrictions;
import com.l2jfree.gameserver.model.zone.L2JailZone;
import com.l2jfree.gameserver.model.zone.L2Zone;
import com.l2jfree.gameserver.network.SystemMessageId;
import com.l2jfree.gameserver.network.serverpackets.Die;

public class RequestRestartPoint extends L2GameClientPacket
{
	private static final String	_C__6d_REQUESTRESTARTPOINT	= "[C] 6d RequestRestartPoint";
	
	protected int				_requestedPointType;
	protected boolean			_continuation;
	
	/**
	 * packet type id 0x6d
	 * format: c
	 * @param decrypt
	 */
	@Override
	protected void readImpl()
	{
		_requestedPointType = readD();
	}
	
	private class DeathTask implements Runnable
	{
		private final L2PcInstance	activeChar;
		
		public DeathTask(L2PcInstance _activeChar)
		{
			activeChar = _activeChar;
		}
		
		@Override
		@SuppressWarnings("synthetic-access")
		public void run()
		{
			Location loc = null;
			
			if (activeChar.isInJail())
				_requestedPointType = 27;
			else if (activeChar.isFestivalParticipant())
				_requestedPointType = 5;
			
			switch (_requestedPointType)
			{
			/*
			case 1: // to clanhall
				break;
			case 2: // to castle
				break;
			case 3: // to Fortress
				break;
			case 4: // to HQ
				break;
			 */
			case 5: // Fixed or Player is a festival participant
				boolean can = false;
				if (activeChar.isGM() || activeChar.isFestivalParticipant())
					can = true;
				else if (activeChar.destroyItemByItemId("Fixed Resurrection", Die.FEATHER_OF_BLESSING_1, 1, activeChar, false)
						|| activeChar.destroyItemByItemId("Fixed Resurrection", Die.FEATHER_OF_BLESSING_2, 1, activeChar, false)
						|| activeChar.destroyItemByItemId("Fixed Resurrection", Die.PHOENIX_FEATHER, 1, activeChar, false))
				{
					activeChar.sendPacket(SystemMessageId.USED_FEATHER_TO_RESURRECT);
					can = true;
				}
				if (!can)
				{
					_log.warn("Player [" + activeChar.getName() + "] called RestartPointPacket - Fixed and he isn't GM/festival participant!");
					return;
				}
				if (activeChar.isGM())
					activeChar.restoreExp(100.0);
				// spawn them where they died
				loc = new Location(activeChar.getX(), activeChar.getY(), activeChar.getZ(), activeChar.getHeading());
				activeChar.setIsPendingRevive(true);
				activeChar.teleToLocation(loc);
				return;
			case 27: // to jail
				if (!activeChar.isInJail())
					return;
				loc = L2JailZone.JAIL_LOCATION;
				break;
			default:
				if (activeChar.isInsideZone(L2Zone.FLAG_JAIL))
					loc = new Location(activeChar.getX(), activeChar.getY(), activeChar.getZ());
				else
				{
					RespawnManager.getInstance().addPlayer(activeChar, _requestedPointType == 4);
					return;
				}
				break;
			}
			// Teleport and revive
			activeChar.setInstanceId(0);
			activeChar.setIsIn7sDungeon(false);
			activeChar.setIsPendingRevive(true);
			activeChar.teleToLocation(loc, true);
		}
	}
	
	@Override
	protected void runImpl()
	{
		L2PcInstance activeChar = getClient().getActiveChar();
		if (activeChar == null)
			return;
		
		if (activeChar.isFakeDeath())
		{
			activeChar.stopFakeDeath(true);
			sendAF();
			return;
		}
		else if (!activeChar.isDead())
		{
			sendAF();
			return;
		}
		else if (!GlobalRestrictions.canRequestRevive(activeChar))
		{
			sendAF();
			return;
		}
		
		/*
		Castle castle = CastleManager.getInstance().getCastle(activeChar.getX(), activeChar.getY(), activeChar.getZ());
		if (castle != null && castle.getSiege().getIsInProgress())
		{
			if (activeChar.getClan() != null && castle.getSiege().checkIsAttacker(activeChar.getClan()))
			{
				// Schedule respawn delay for attacker
				ThreadPoolManager.getInstance().scheduleGeneral(new DeathTask(activeChar), castle.getSiege().getAttackerRespawnDelay());
				if (castle.getSiege().getAttackerRespawnDelay() > 0)
					activeChar.sendMessage("You will be re-spawned in " + castle.getSiege().getAttackerRespawnDelay() / 1000 + " seconds");
				sendAF();
				return;
			}
		}
		 */
		
		// run immediately (no need to schedule)
		new DeathTask(activeChar).run();
		
		sendAF();
	}
	
	@Override
	public String getType()
	{
		return _C__6d_REQUESTRESTARTPOINT;
	}
}
