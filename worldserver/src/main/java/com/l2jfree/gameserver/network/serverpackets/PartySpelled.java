/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.network.serverpackets;

import org.sod.model.L2PlayerData;

import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.actor.instance.L2PetInstance;
import com.l2jfree.gameserver.model.actor.instance.L2SummonInstance;

public final class PartySpelled extends EffectInfoPacket
{
	private static final String _S__EE_PartySpelled = "[S] EE PartySpelled";
	
	public PartySpelled(EffectInfoPacketList list)
	{
		super(list);
	}
	
	@Override
	protected void writeImpl()
	{
		writeC(0xf4);
		writeD(getPlayable() instanceof L2SummonInstance ? 2 : getPlayable() instanceof L2PetInstance ? 1 : 0);
		writeD(getPlayable().getObjectId());
		L2PcInstance player = getPlayable().getActingPlayer();
		L2PlayerData dat = player.getSoDPlayer();
		if (dat.isGodlike())
			writeD(0x00);
		else
		{
			writeD(size());
			writeEffectInfos();
		}
	}
	
	@Override
	public String getType()
	{
		return _S__EE_PartySpelled;
	}
}
