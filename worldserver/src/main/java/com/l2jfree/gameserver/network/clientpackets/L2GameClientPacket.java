/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.network.clientpackets;

import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.SystemMessageId;
import com.l2jfree.gameserver.network.client.L2Client;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.Chat;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.ChatMessage;
import com.l2jfree.gameserver.network.client.packets.sendable.ActionFail.InteractionFinished;
import com.l2jfree.gameserver.network.serverpackets.L2GameServerPacket;
import com.l2jfree.network.mmocore.ReceivablePacket;
import com.l2jfree.util.logging.L2Logger;

/**
 * Packets received by the game server from clients
 * 
 * @author KenM
 */
public abstract class L2GameClientPacket extends ReceivablePacket<L2Client, L2GameClientPacket, L2GameServerPacket>
{
	protected static final L2Logger _log = L2Logger.getLogger(L2GameClientPacket.class);
	
	protected L2GameClientPacket()
	{
	}
	
	protected final void sendPacket(SystemMessageId sm)
	{
		sendPacket(sm.getSystemMessage());
	}
	
	protected final L2PcInstance getActiveChar()
	{
		return getClient().getActiveChar();
	}
	
	protected final void requestFailed(SystemMessageId sm)
	{
		requestFailed(sm.getSystemMessage());
	}
	
	protected final void requestFailed(L2GameServerPacket gsp)
	{
		sendPacket(gsp);
		sendAF();
	}
	
	protected final void sendAF()
	{
		sendPacket(InteractionFinished.PACKET);
	}
	
	public void sendCMessage(Chat chatType, String sender, String message)
	{
		ChatMessage cm = new ChatMessage(chatType, sender, message);
		sendPacket(cm);
	}
}
