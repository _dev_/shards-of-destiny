/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.network.clientpackets;

import org.sod.manager.conquest.ConquestFactionManager;
import org.sod.tutorial.TutorialManager;

import com.l2jfree.gameserver.communitybbs.Manager.SettingsBBSManager;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.actor.instance.L2RaidTeleporterInstance;
import com.l2jfree.gameserver.model.quest.QuestState;
import com.l2jfree.mmocore.network.InvalidPacketException;

public class RequestTutorialPassCmdToServer extends L2GameClientPacket
{
	private String _bypass = null;
	
	@Override
	protected void readImpl()
	{
		_bypass = readS();
	}
	
	@Override
	protected void runImpl() throws InvalidPacketException
	{
		L2PcInstance player = getClient().getActiveChar();
		if (player == null)
			return;
		
		player.validateTutorialBypass(_bypass);
		
		if (_bypass.startsWith("raidman_"))
			L2RaidTeleporterInstance.handleBypass(_bypass.substring(8), player);
		else if (_bypass.startsWith("_bbsgetfav"))
			SettingsBBSManager.getInstance().parsecmd(_bypass, player);
		else if (_bypass.startsWith("tut "))
			TutorialManager.getInstance().handleBypass(player, _bypass);
		else if (_bypass.startsWith("fact "))
			ConquestFactionManager.getInstance().handleBypass(player, _bypass);
		
		QuestState qs = player.getQuestState("255_Tutorial");
		if (qs != null)
			qs.getQuest().notifyEvent(_bypass, null, player);
	}
	
	@Override
	public String getType()
	{
		return "[C] 86 RequestTutorialPassCmdToServer";
	}
}
