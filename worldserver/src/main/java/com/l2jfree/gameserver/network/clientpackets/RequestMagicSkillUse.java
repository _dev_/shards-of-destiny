/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.network.clientpackets;

import org.sod.Calc;
import org.sod.model.event.global.GlobalEvent;

import com.l2jfree.gameserver.datatables.SkillTable;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.restriction.AvailableRestriction;
import com.l2jfree.gameserver.model.restriction.ObjectRestrictions;
import com.l2jfree.gameserver.skills.SkillUsageRequest;

public class RequestMagicSkillUse extends L2GameClientPacket
{
	private static final String	_C__REQUESTMAGICSKILLUSE	= "[C] 39 RequestMagicSkillUse c[ddc]";

	private int					_skillId;
	private boolean				_ctrl;
	private boolean				_shift;

	@Override
	protected void readImpl()
	{
		_skillId = readD(); // display ID
		_skillId = Calc.getSkillFromDisplayId(_skillId);
		_ctrl = readD() != 0;
		_shift = readC() != 0;
	}

	@Override
	protected void runImpl()
	{
		L2PcInstance activeChar = getActiveChar();
		if (activeChar == null)
			return;

		// removes spawn protection
		activeChar.onActionRequest();

		if (ObjectRestrictions.getInstance().checkRestriction(activeChar, AvailableRestriction.PlayerCast))
		{
			activeChar.sendMessage("You cannot cast a skill due to a restriction.");
			return;
		}

		// Lightweight Pro perk
		if (_skillId == 4 && activeChar.getSkillLevel(9205) == 2 && !activeChar.isSkillDisabled(9206))
			_skillId = 9206;

		// Get the level of the used skill
		int level = activeChar.getSkillLevel(_skillId);
		if (level <= 0)
		{
			sendAF();
			return;
		}

		// Get the L2Skill template corresponding to the skillID received from the client
		L2Skill skill = SkillTable.getInstance().getInfo(_skillId, level);

		GlobalEvent ge = GlobalEvent.getEvent(activeChar);
		if (ge != null && !ge.canUseSkill(activeChar, activeChar, skill))
		{
			sendAF();
			return;
		}

		// Check the validity of the skill
		if (activeChar.canUseMagic(skill))
		{
			switch (skill.getTargetType())
			{
			case TARGET_AREA:
			case TARGET_AURA:
			case TARGET_FRONT_AREA:
			case TARGET_BEHIND_AREA:
			case TARGET_FRONT_AURA:
			case TARGET_BEHIND_AURA:
			case TARGET_SERVITOR_AURA:
			case TARGET_AREA_CORPSE_MOB:
			case TARGET_AREA_CORPSE_PLAYABLE:
			case TARGET_AREA_CORPSES:
				activeChar.getSoDPlayer().setLastAoE(System.currentTimeMillis());
			}
			if (skill.isToggle())
				activeChar.doSimultaneousCast(skill);
			else
				activeChar.useMagic(new SkillUsageRequest(skill, _ctrl, _shift));
		}
		else
		{
			sendAF();
		}
	}

	@Override
	public String getType()
	{
		return _C__REQUESTMAGICSKILLUSE;
	}
}
