/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.network.clientpackets;

import org.sod.manager.ItemAuctionManager;
import org.sod.manager.ItemAuctionManager.ItemReflection;

import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.SystemMessageId;

/**
 * This packet is sent by the client once the player confirms his bid
 * (or clicks a pre-defined bid) in the dialog opened by ExInfoItemAuction.
 * @author savormix
 */
public final class RequestBidItemAuction extends L2GameClientPacket
{
	private int _auctionId;
	private long _bid;

	@Override
	protected void readImpl()
	{
		_auctionId = readD();
		_bid = readCompQ();
	}

	@Override
	protected void runImpl()
	{
		L2PcInstance player = getActiveChar();
		if (player == null)
			return;

		ItemReflection ir = ItemAuctionManager.getInstance().getAuction(_auctionId);
		if (ir == null || ir.getState() == 0)
		{
			player.sendPacket(SystemMessageId.NO_AUCTION_PERIOD);
			return;
		}
		if (ir.getOwnerId() == player.getObjectId())
		{
			player.sendMessage("Cannot bid on own item auction!");
			return;
		}

		if (ir.getOwnerId() == ir.getBidderId())
		{
			if (_bid < ir.getBid())
			{
				player.sendPacket(SystemMessageId.BID_PRICE_MUST_BE_HIGHER);
				return;
			}
		}
		else if (_bid <= ir.getBid())
		{
			player.sendPacket(SystemMessageId.BID_MUST_BE_HIGHER_THAN_CURRENT_BID);
			return;
		}

		if (player.reduceAdena("Auction_Bid", _bid, null, true))
		{
			ir.setBid(_bid, player.getObjectId());
			player.sendPacket(SystemMessageId.BID_ON_ITEM_AUCTION);
		}
		else
		{
			player.sendPacket(SystemMessageId.NOT_ENOUGH_ADENA_FOR_THIS_BID);
		}
	}
}
