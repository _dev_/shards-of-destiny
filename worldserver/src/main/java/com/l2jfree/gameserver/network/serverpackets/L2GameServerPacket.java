/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.network.serverpackets;

import com.l2jfree.Config;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.client.L2Client;
import com.l2jfree.gameserver.network.clientpackets.L2GameClientPacket;
import com.l2jfree.lang.L2Math;
import com.l2jfree.network.mmocore.SendablePacket;
import com.l2jfree.util.logging.L2Logger;

/**
 * @author KenM
 */
public abstract class L2GameServerPacket extends SendablePacket<L2Client, L2GameClientPacket, L2GameServerPacket>
{
	protected static final L2Logger _log = L2Logger.getLogger(L2GameServerPacket.class);
	
	protected L2GameServerPacket()
	{
	}
	
	@Override
	protected final void write(L2Client client)
	{
		writeImpl(client, client.getActiveChar());
	}
	
	public void prepareToSend(L2Client client, L2PcInstance activeChar)
	{
	}
	
	public void packetSent(L2Client client, L2PcInstance activeChar)
	{
	}
	
	protected void writeImpl()
	{
	}
	
	protected void writeImpl(L2Client client, L2PcInstance activeChar)
	{
		writeImpl();
	}
	
	public boolean canBeSentTo(L2Client client, L2PcInstance activeChar)
	{
		return true;
	}
	
	// TODO: HACK TO BYPASS THE EXPLOIT CHECKS WHICH CAN BE REMOVED NOW
	protected final void writeCompQ(long value)
	{
		if (Config.PACKET_FINAL)
			writeQ(value);
		else
			writeD(L2Math.limit(Integer.MIN_VALUE, value, Integer.MAX_VALUE));
	}
	
	protected final void writeCompH(int value)
	{
		if (Config.PACKET_FINAL)
			writeH(value);
		else
			writeD(value);
	}
	
	protected final void writeElementalInfo(ElementalOwner owner)
	{
		writeCompH(owner.getAttackElementType());
		writeCompH(owner.getAttackElementPower());
		for (byte i = 0; i < 6; i++)
		{
			writeCompH(owner.getElementDefAttr(i));
		}
	}
}
