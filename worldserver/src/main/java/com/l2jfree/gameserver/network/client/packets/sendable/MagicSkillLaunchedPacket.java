/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.network.client.packets.sendable;

import com.l2jfree.ClientProtocolVersion;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.client.L2Client;
import com.l2jfree.gameserver.network.client.packets.L2ServerPacket;
import com.l2jfree.network.mmocore.MMOBuffer;

/**
 * @author savormix (generated)
 */
public abstract class MagicSkillLaunchedPacket extends L2ServerPacket
{
	/**
	 * A nicer name for {@link MagicSkillLaunchedPacket}.
	 * 
	 * @author savormix (generated)
	 * @see MagicSkillLaunchedPacket
	 */
	public static final class MagicSkillLaunched extends MagicSkillLaunchedPacket
	{
		public MagicSkillLaunched(L2Character caster, L2Skill skill, L2Character... affected)
		{
			super(caster, skill, affected);
		}
		
		public MagicSkillLaunched(L2Character caster, int skill, int level, L2Character... affected)
		{
			super(caster, skill, level, affected);
		}
	}
	
	private final Integer _casterId;
	private final int _skill;
	private final int _level;
	private final L2Character[] _affected;
	
	protected MagicSkillLaunchedPacket(L2Character caster, L2Skill skill, L2Character... affected)
	{
		this(caster, skill.getDisplayId(), skill.getLevel());
	}
	
	protected MagicSkillLaunchedPacket(L2Character caster, int skill, int level, L2Character... affected)
	{
		_casterId = caster.getObjectId();
		_skill = skill;
		_level = level;
		_affected = affected;
	}
	
	@Override
	protected int getOpcode()
	{
		return 0x54;
	}
	
	@Override
	protected void writeImpl(L2Client client, L2PcInstance activeChar, MMOBuffer buf) throws RuntimeException
	{
		// TODO: when implementing, consult an up-to-date packets_game_server.xml and/or savormix
		final ClientProtocolVersion cpv = client.getVersion();
		if (cpv.isNewerThanOrEqualTo(ClientProtocolVersion.GODDESS_OF_DESTRUCTION))
			buf.writeD(0); // ??? 0
		buf.writeD(_casterId); // Caster OID
		buf.writeD(_skill); // Skill
		buf.writeD(_level); // Level
		buf.writeD(_affected.length); // Affected
		for (L2Character aff : _affected)
			buf.writeD(aff.getObjectId()); // Affected OID
	}
}
