/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.network.client.packets.sendable;

import com.l2jfree.ClientProtocolVersion;
import com.l2jfree.gameserver.model.L2Object;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.client.L2Client;
import com.l2jfree.gameserver.network.client.packets.L2ServerPacket;
import com.l2jfree.network.mmocore.MMOBuffer;

/**
 * @author savormix (generated)
 */
public class MagicSkillUse extends L2ServerPacket
{
	private final L2Object _caster;
	private final L2Object _target;
	private final int _skill;
	private final int _level;
	private final int _castTime;
	private final int _cooldown;
	
	public MagicSkillUse(L2Object caster, int skill, int level, int castTime)
	{
		this(caster, skill, level, castTime, 0);
	}
	
	public MagicSkillUse(L2Object caster, int skill, int level, int castTime, int cooldown)
	{
		this(caster, caster, skill, level, castTime, cooldown);
	}
	
	public MagicSkillUse(L2Object caster, L2Skill skill)
	{
		this(caster, caster, skill, skill.getHitTime(), skill.getReuseDelay());
	}
	
	public MagicSkillUse(L2Object caster, L2Skill skill, int castTime, int cooldown)
	{
		this(caster, caster, skill, castTime, cooldown);
	}
	
	public MagicSkillUse(L2Object caster, L2Object target, L2Skill skill, int castTime, int cooldown)
	{
		this(caster, target, skill.getDisplayId(), skill.getLevel(), castTime, cooldown);
	}
	
	public MagicSkillUse(L2Object caster, L2Object target, int skill, int level, int castTime, int cooldown)
	{
		_caster = caster;
		_target = target;
		_skill = skill;
		_level = level;
		_castTime = castTime;
		_cooldown = cooldown;
	}
	
	@Override
	protected int getOpcode()
	{
		return 0x48;
	}
	
	@Override
	protected void writeImpl(L2Client client, L2PcInstance activeChar, MMOBuffer buf) throws RuntimeException
	{
		// TODO: when implementing, consult an up-to-date packets_game_server.xml and/or savormix
		final ClientProtocolVersion cpv = client.getVersion();
		if (cpv.isNewerThanOrEqualTo(ClientProtocolVersion.GODDESS_OF_DESTRUCTION))
			buf.writeD(0); // ??? 0
		buf.writeD(_caster.getObjectId()); // Caster OID
		buf.writeD(_target.getObjectId()); // Main target OID
		if (cpv.isNewerThanOrEqualTo(ClientProtocolVersion.GODDESS_OF_DESTRUCTION))
			buf.writeC(0); // ??? 0
		buf.writeD(_skill); // Skill
		buf.writeD(_level); // Level
		buf.writeD(_castTime); // Cast time
		if (cpv.isNewerThanOrEqualTo(ClientProtocolVersion.GODDESS_OF_DESTRUCTION))
			buf.writeD(-1); // ??? -1/0
		buf.writeD(_cooldown); // Cooldown
		buf.writeD(_caster.getX()); // Caster X
		buf.writeD(_caster.getY()); // Caster Y
		buf.writeD(_caster.getZ()); // Caster Z
		final int sizeA = 0; // ???
		buf.writeD(sizeA);
		for (int i = 0; i < sizeA; i++)
		{
			buf.writeH(0); // ??? 0
		}
		buf.writeD(_target.getX()); // Main target X
		buf.writeD(_target.getY()); // Main target Y
		buf.writeD(_target.getZ()); // Main target Z
	}
}
