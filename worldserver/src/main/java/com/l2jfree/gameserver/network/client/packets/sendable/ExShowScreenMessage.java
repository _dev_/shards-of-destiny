/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.network.client.packets.sendable;

import org.apache.commons.lang3.ArrayUtils;

import com.l2jfree.ClientProtocolVersion;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.SystemMessageId;
import com.l2jfree.gameserver.network.client.L2Client;
import com.l2jfree.gameserver.network.client.packets.L2ServerPacket;
import com.l2jfree.network.mmocore.MMOBuffer;

/**
 * @author savormix (generated)
 */
public abstract class ExShowScreenMessage extends L2ServerPacket
{
	/**
	 * A nicer name for {@link ExShowScreenMessage}.
	 * 
	 * @author savormix (generated)
	 * @see ExShowScreenMessage
	 */
	public static final class ScreenMessage extends ExShowScreenMessage
	{
		public ScreenMessage(ScreenPosition position, boolean small, boolean decorated, int duration, boolean fade,
				String message)
		{
			super(-1, position.ordinal(), small, decorated, duration, fade, -1, message);
		}
		
		public ScreenMessage(ScreenPosition position, boolean small, boolean decorated, int duration, boolean fade,
				SystemMessageId message)
		{
			super(message.getId(), position.ordinal(), small, decorated, duration, fade, -1);
		}
	}
	
	private static final int[] EXT_OPCODES =
	{
			0x39, 0x00
	};
	
	private final int _message;
	private final int _position;
	private final boolean _small;
	private final boolean _decorated;
	private final int _duration;
	private final boolean _fade;
	private final int _fString;
	private final String[] _tokens;
	
	protected ExShowScreenMessage(int message, int position, boolean small, boolean decorated, int duration,
			boolean fade, int fString, String... tokens)
	{
		_message = message;
		_position = position;
		_small = small;
		_decorated = decorated;
		_duration = duration;
		_fade = fade;
		_fString = fString;
		_tokens = tokens;
	}
	
	@Override
	protected int getOpcode()
	{
		return 0xfe;
	}
	
	@Override
	protected int[] getAdditionalOpcodes()
	{
		return EXT_OPCODES;
	}
	
	@Override
	protected void writeImpl(L2Client client, L2PcInstance activeChar, MMOBuffer buf) throws RuntimeException
	{
		// TODO: when implementing, consult an up-to-date packets_game_server.xml and/or savormix
		buf.writeD(_message < 0); // Custom
		buf.writeD(_message); // Message (SM)
		buf.writeD(_position); // Position
		buf.writeD(0); // ??? 0
		buf.writeD(_small); // Small
		buf.writeD(0); // ??? 4 or 0
		buf.writeD(0); // ??? 1/0
		buf.writeD(_decorated); // Decorated
		buf.writeD(_duration); // Duration
		buf.writeD(_fade); // Fade
		if (client.getVersion().isNewerThan(ClientProtocolVersion.GRACIA_FINAL))
		{
			buf.writeD(_fString); // Message (fstring)
			int i;
			for (i = 0; i < Math.min(5, _tokens.length); i++)
				buf.writeS(_tokens[i]); // 1st-5th token
			for (; i < 5; i++)
				buf.writeS(""); // 1st-5th token
		}
		else
		{
			if (ArrayUtils.isEmpty(_tokens))
				buf.writeS("");
			else
				buf.writeS(_tokens[0]);
		}
	}
	
	public enum ScreenPosition
	{
		DEFAULT, TOP_LEFT, TOP, TOP_RIGHT, LEFT, CENTER, RIGHT, BOTTOM, BOTTOM_RIGHT;
	}
}
