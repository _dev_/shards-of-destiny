/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.network.clientpackets;

import java.util.Locale;

import org.sod.L2Htm;
import org.sod.i18n.Internationalization;
import org.sod.model.ChallengeTemplate;
import org.sod.manager.PerkManager;
import org.sod.manager.PerkManager.PerkTemplate;
import org.sod.model.L2PlayerData;

import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.actor.instance.L2RaidTeleporterInstance;
import com.l2jfree.gameserver.network.serverpackets.TutorialShowHtml;
import com.l2jfree.gameserver.network.client.packets.sendable.TutorialShowQuestionMarkPacket.ShowTutorialMark;
import com.l2jfree.lang.L2TextBuilder;

/**
 * Format: (c) d
 * 
 * @author DaDummy
 */
public class RequestTutorialQuestionMark extends L2GameClientPacket
{
	private static final String _C__7D_REQUESTTUTORIALQUESTIONMARK = "[C] 7D RequestTutorialQuestionMark";
	private int _id;
	
	@Override
	protected void readImpl()
	{
		_id = readD(); // id
	}
	
	@Override
	protected void runImpl()
	{
		L2PcInstance player = getClient().getActiveChar();
		if (player == null)
			return;
		
		L2PlayerData dat = player.getSoDPlayer();
		dat.questMarkViewed(_id);
		
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = dat.getSettings().getLocale();
		if (_id > ShowTutorialMark.CHALLENGE_OFFSET)
		{
			ChallengeTemplate ct;
			try
			{
				ct = ChallengeTemplate.getTemplate(_id - ShowTutorialMark.CHALLENGE_OFFSET);
			}
			catch (Exception e)
			{
				sendAF();
				return;
			}
			
			L2TextBuilder tb = new L2TextBuilder();
			L2Htm.writeHeader(tb, i18n.get(loc, "CHALLENGE_COMPLETED_TITLE"));
			tb.append(i18n.get(loc, "CHALLENGE_COMPLETED_TEXT", "%challenge%", ct.getFullName()));
			tb.append("<br1>");
			tb.append(i18n.get(loc, "CHALLENGE_COMPLETED_REWARD", "%reward%", ct.getReward()));
			tb.append("<br>");
			L2Htm.writeTutorialCloseButton(tb, 150, 30, i18n.get(loc, "CLOSE"));
			L2Htm.writeFooter(tb);
			
			player.sendPacket(new TutorialShowHtml(tb.moveToString()));
		}
		else if (_id > ShowTutorialMark.PERK_UPGRADE_OFFSET)
		{
			PerkTemplate pt = PerkManager.getInstance().getTemplate(_id - ShowTutorialMark.PERK_UPGRADE_OFFSET);
			if (pt == null)
			{
				sendAF();
				return;
			}
			
			L2TextBuilder tb = new L2TextBuilder();
			L2Htm.writeHeader(tb, i18n.get(loc, "PERK_UPGRADED_TITLE"));
			tb.append(i18n.get(loc, "PERK_UPGRADED_TEXT", "%perk%", pt.getName()));
			tb.append("<br>");
			L2Htm.writeTutorialCloseButton(tb, 150, 30, i18n.get(loc, "CLOSE"));
			L2Htm.writeFooter(tb);
			
			player.sendPacket(new TutorialShowHtml(tb.moveToString()));
		}
		else if (_id > ShowTutorialMark.PERK_UNLOCK_OFFSET)
		{
			PerkTemplate pt = PerkManager.getInstance().getTemplate(_id - ShowTutorialMark.PERK_UNLOCK_OFFSET);
			if (pt == null)
			{
				sendAF();
				return;
			}
			
			L2TextBuilder tb = new L2TextBuilder();
			L2Htm.writeHeader(tb, i18n.get(loc, "PERK_UNLOCKED_TITLE"));
			tb.append(i18n.get(loc, "PERK_UNLOCKED_TEXT", "%perk%", pt.getName()));
			tb.append("<br>");
			L2Htm.writeTutorialCloseButton(tb, 150, 30, i18n.get(loc, "CLOSE"));
			L2Htm.writeFooter(tb);
			
			player.sendPacket(new TutorialShowHtml(tb.moveToString()));
		}
		else if (_id == ShowTutorialMark.RAID_REQUEST)
		{
			player.getSoDPlayer().showRaidRequest();
		}
		else if (_id == ShowTutorialMark.RAID_REQUEST_INVALIDATED)
		{
			player.sendPacket(L2RaidTeleporterInstance.INVALIDATED_REQUEST);
		}
		else if (_id == ShowTutorialMark.RAID_REQUEST_DENIED)
		{
			player.sendPacket(L2RaidTeleporterInstance.DENIED_REQUEST);
		}
		else if (_id == ShowTutorialMark.RAID_REQUEST_VIEWED)
		{
			player.sendPacket(L2RaidTeleporterInstance.VIEWED_REQUEST);
		}
		/*
				player.onTutorialQuestionMark(_id);

				QuestState qs = player.getQuestState("255_Tutorial");
				if (qs != null)
					qs.getQuest().notifyEvent("QM" + _id + "", null, player);

				sendAF();
		*/
	}
	
	@Override
	public String getType()
	{
		return _C__7D_REQUESTTUTORIALQUESTIONMARK;
	}
}
