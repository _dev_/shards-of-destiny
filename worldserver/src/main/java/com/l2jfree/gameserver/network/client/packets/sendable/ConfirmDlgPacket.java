/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.network.client.packets.sendable;

import com.l2jfree.gameserver.model.L2Object;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.SystemMessageId;
import com.l2jfree.gameserver.network.client.L2Client;
import com.l2jfree.gameserver.network.client.packets.sendable.generic.ClientSideMessage;
import com.l2jfree.gameserver.network.clientpackets.ConfirmDlgAnswer.AnswerHandler;
import com.l2jfree.network.mmocore.MMOBuffer;

/**
 * @author savormix (generated)
 */
public abstract class ConfirmDlgPacket extends ClientSideMessage
{
	/**
	 * A nicer name for {@link ConfirmDlgPacket}.
	 * 
	 * @author savormix (generated)
	 * @see ConfirmDlgPacket
	 */
	public static final class ShowDialog extends ConfirmDlgPacket
	{
		public ShowDialog(SystemMessageId message, L2Object initiator, AnswerHandler handler)
		{
			this(message, 0, initiator, handler);
		}
		
		public ShowDialog(SystemMessageId message, int time, L2Object initiator, AnswerHandler handler)
		{
			super(message, time, initiator.getObjectId(), handler);
		}
		
		public static ClientSideMessage valueOf(String message, int time, L2Object initiator, AnswerHandler handler)
		{
			return new ShowDialog(SystemMessageId.S1, time, initiator, handler).addString(message);
		}
	}
	
	private final int _time;
	private final Integer _initiatorId;
	private final AnswerHandler _handler;
	
	protected ConfirmDlgPacket(SystemMessageId message, int time, Integer initiatorId, AnswerHandler handler)
	{
		this(message.getId(), time, initiatorId, handler);
	}
	
	protected ConfirmDlgPacket(int message, int time, Integer initiatorId, AnswerHandler handler)
	{
		super(message);
		
		_time = time;
		_initiatorId = initiatorId;
		_handler = handler;
		_handler.setRequesterId(_initiatorId);
	}
	
	@Override
	protected int getOpcode()
	{
		return 0xf3;
	}
	
	@Override
	public boolean canBeSentTo(L2Client client, L2PcInstance activeChar)
	{
		return activeChar != null;
	}
	
	@Override
	public void prepareToSend(L2Client client, L2PcInstance activeChar)
	{
		activeChar.setAnswerHandler(_handler);
	}
	
	@Override
	protected void writeImpl(L2Client client, L2PcInstance activeChar, MMOBuffer buf) throws RuntimeException
	{
		// TODO: when implementing, consult an up-to-date packets_game_server.xml and/or savormix
		super.writeImpl(client, activeChar, buf);
		
		buf.writeD(_time); // Time to answer
		buf.writeD(_initiatorId); // Requestor OID
	}
}
