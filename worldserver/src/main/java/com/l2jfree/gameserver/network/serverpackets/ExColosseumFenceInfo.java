package com.l2jfree.gameserver.network.serverpackets;

import org.sod.manager.EventFenceManager.FenceInfo;

import com.l2jfree.gameserver.model.actor.instance.L2FenceInstance;

/**
 * Format: (ch)ddddddd
 * d: object id
 * d: type (00 - no fence, 01 - only 4 columns, 02 - columns with fences)
 * d: x coord
 * d: y coord
 * d: z coord
 * d: width
 * d: height
 */
public class ExColosseumFenceInfo extends L2GameServerPacket
{
	private static final String	_S__EXCOLOSSEUMFENCEINFO = "[S] FE:03 ExColosseumFenceInfoPacket ch[ddddddd]";
	private L2FenceInstance _activeChar;
	
	public ExColosseumFenceInfo(L2FenceInstance activeChar)
	{
		_activeChar = activeChar;
	}
	
	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0x03);
		
		FenceInfo fi = _activeChar.getFence();
		writeD(_activeChar.getObjectId());
		writeD(fi.getType());
		writeD(_activeChar.getX());
		writeD(_activeChar.getY());
		writeD(_activeChar.getZ()); // hm.
		writeD(fi.getWidth()); // W-E
		writeD(fi.getHeight()); // N-S
	}
	
	@Override
	public String getType()
	{
		return _S__EXCOLOSSEUMFENCEINFO;
	}
}
