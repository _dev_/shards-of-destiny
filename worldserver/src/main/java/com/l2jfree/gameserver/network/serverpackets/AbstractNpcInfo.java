/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.network.serverpackets;

import org.sod.manager.conquest.ConquestFactionManager;
import org.sod.manager.conquest.ConquestFactionManager.FactionTemplate;
import org.sod.model.PlayerLookalike;
import org.sod.model.conquest.ConquestFactionMember;

import com.l2jfree.Config;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.L2Summon;
import com.l2jfree.gameserver.model.actor.L2Trap;
import com.l2jfree.gameserver.model.actor.instance.AdvertiserInstance;
import com.l2jfree.gameserver.model.actor.instance.AdvertiserInstance.AdvertEquip;
import com.l2jfree.gameserver.model.actor.instance.L2MartyrInstance;
import com.l2jfree.gameserver.model.actor.instance.L2MonsterInstance;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.itemcontainer.Inventory;
import com.l2jfree.gameserver.model.restriction.global.GlobalRestrictions;
import com.l2jfree.gameserver.network.client.L2Client;
import com.l2jfree.gameserver.skills.AbnormalEffect;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;

public abstract class AbstractNpcInfo extends L2GameServerPacket
{
	private static final String _S__22_NPCINFO = "[S] 0c NpcInfo";
	
	protected int _x, _y, _z, _heading;
	protected int _idTemplate;
	protected boolean _isSummoned;
	protected int _mAtkSpd, _pAtkSpd;
	
	/**
	 * Run speed, swimming run speed and flying run speed
	 */
	protected int _runSpd;
	
	/**
	 * Walking speed, swimming walking speed and flying walking speed
	 */
	protected int _walkSpd;
	
	protected int _rhand, _lhand, _chest;
	protected double _collisionHeight, _collisionRadius;
	protected String _name = "";
	protected int _nameColor;
	protected String _title = "";
	protected int _titleColor;
	
	public AbstractNpcInfo(L2Character cha)
	{
		_isSummoned = cha.isShowSummonAnimation();
		_x = cha.getX();
		_y = cha.getY();
		_z = cha.getZ();
		_heading = cha.getHeading();
		_mAtkSpd = cha.getMAtkSpd();
		_pAtkSpd = cha.getPAtkSpd();
		_runSpd = cha.getTemplate().getBaseRunSpd();
		_walkSpd = cha.getTemplate().getBaseWalkSpd();
	}
	
	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.serverpackets.ServerBasePacket#getType()
	 */
	@Override
	public String getType()
	{
		return _S__22_NPCINFO;
	}
	
	/**
	 * Packet for Npcs
	 */
	public static class NpcInfo extends AbstractNpcInfo
	{
		private final L2Npc _npc;
		
		public NpcInfo(L2Npc cha)
		{
			super(cha);
			_npc = cha;
			_idTemplate = cha.getTemplate().getIdTemplate(); // On every subclass
			_rhand = cha.getRightHandItem(); // On every subclass
			_lhand = cha.getLeftHandItem(); // On every subclass
			_collisionHeight = cha.getCollisionHeight(); // On every subclass
			_collisionRadius = cha.getCollisionRadius(); // On every subclass
			
			if (cha.getTemplate().isServerSideName())
				_name = cha.getTemplate().getName(); // On every subclass
			_nameColor = GlobalRestrictions.getNpcNameColor(cha);
			if (_nameColor == -1)
				_nameColor = 0;
			
			if (cha.isChampion())
				_title = Config.CHAMPION_TITLE; // On every subclass
			else if (cha.getTemplate().isServerSideTitle())
				_title = cha.getTemplate().getTitle(); // On every subclass
			else
				_title = cha.getTitle(); // On every subclass
				
			if (Config.SHOW_NPC_LVL && _npc instanceof L2MonsterInstance)
			{
				String t = "Lvl " + cha.getLevel();
				if (cha.getAggroRange() > 0)
					if (!cha.isChampion() || (cha.isChampion() && !Config.CHAMPION_PASSIVE))
						t += "*";
				
				if (_title != null && !_title.isEmpty())
					t += " " + _title;
				
				_title = t;
			}
			_titleColor = GlobalRestrictions.getNpcTitleColor(cha);
			if (_titleColor == -1)
				_titleColor = 0;
		}
		
		@Override
		protected void writeImpl(L2Client client, L2PcInstance activeChar)
		{
			writeC(0x0c);
			writeD(_npc.getObjectId());
			writeD(_idTemplate + 1000000); // npctype id
			writeD(_npc.isAutoAttackable(activeChar) ? 1 : 0);
			writeD(_x);
			writeD(_y);
			writeD(_z);
			writeD(_heading);
			writeD(0x00);
			writeD(_mAtkSpd);
			writeD(_pAtkSpd);
			writeD(_runSpd);
			writeD(_walkSpd);
			writeD(_runSpd); // swim run speed
			writeD(_walkSpd); // swim walk speed
			writeD(_runSpd);
			writeD(_walkSpd);
			writeD(_runSpd); // fly run speed
			writeD(_walkSpd); // fly walk speed
			writeF(_npc.getStat().getMovementSpeedMultiplier());
			writeF(_npc.getStat().getAttackSpeedMultiplier());
			writeF(_collisionRadius);
			writeF(_collisionHeight);
			writeD(_rhand); // right hand weapon
			writeD(_chest);
			writeD(_lhand); // left hand weapon
			writeC(1); // name above char 1=true ... ??
			writeC(_npc.isRunning() ? 1 : 0);
			writeC(_npc.isInCombat() ? 1 : 0);
			writeC(_npc.isAlikeDead() ? 1 : 0);
			writeC(_isSummoned ? 2 : 0); // 0=teleported 1=default 2=summoned
			writeS(_name);
			writeS(_title);
			writeD(_titleColor); // Title color 0=client default
			writeD(0x00);
			writeD(0x00); // pvp flag
			
			writeD(_npc.getAbnormalEffect()); // C2
			writeD(0x00); // clan id
			writeD(0x00); // crest id
			writeD(0000); // C2
			writeD(0000); // C2
			writeC(_npc.isFlying() ? 2 : 0); // C2
			writeC(0x00); // title color 0=client
			
			writeF(_collisionRadius);
			writeF(_collisionHeight);
			writeD(_npc.getWeaponEnchantLevel()); // C4
			writeD(_npc.isFlying() ? 1 : 0); // C6
			writeD(0x00);
			writeD(0x00);// CT1.5 Pet form and skills
			if (Config.PACKET_FINAL)
			{
				writeC(0x01);
				writeC(0x01);
				writeD(_npc.getSpecialEffect());
			}
		}
		
		@Override
		public boolean canBeSentTo(L2Client client, L2PcInstance activeChar)
		{
			if (!activeChar.canSee(_npc))
				return false;
			
			return true;
		}
	}
	
	public static class TrapInfo extends AbstractNpcInfo
	{
		private final L2Trap _trap;
		
		public TrapInfo(L2Trap cha)
		{
			super(cha);
			_trap = cha;
			_idTemplate = cha.getTemplate().getIdTemplate();
			_rhand = 0;
			_lhand = 0;
			_collisionHeight = _trap.getTemplate().getCollisionHeight();
			_collisionRadius = _trap.getTemplate().getCollisionRadius();
			_title = cha.getOwner().getName();
			_runSpd = _trap.getStat().getRunSpeed();
			_walkSpd = _trap.getStat().getWalkSpeed();
		}
		
		@Override
		protected void writeImpl(L2Client client, L2PcInstance activeChar)
		{
			writeC(0x0c);
			writeD(_trap.getObjectId());
			writeD(_idTemplate + 1000000); // npctype id
			writeD(_trap.isAutoAttackable(activeChar) ? 1 : 0);
			writeD(_x);
			writeD(_y);
			writeD(_z);
			writeD(_heading);
			writeD(0x00);
			writeD(_mAtkSpd);
			writeD(_pAtkSpd);
			writeD(_runSpd);
			writeD(_walkSpd);
			writeD(_runSpd); // swim run speed
			writeD(_walkSpd); // swim walk speed
			writeD(_runSpd);
			writeD(_walkSpd);
			writeD(_runSpd); // fly run speed
			writeD(_walkSpd); // fly walk speed
			writeF(1.1/*_trap.getStat().getMovementSpeedMultiplier()*/);
			writeF(_trap.getStat().getAttackSpeedMultiplier());
			writeF(_collisionRadius);
			writeF(_collisionHeight);
			writeD(_rhand); // right hand weapon
			writeD(_chest);
			writeD(_lhand); // left hand weapon
			writeC(1); // name above char 1=true ... ??
			writeC(1);
			writeC(_trap.isInCombat() ? 1 : 0);
			writeC(_trap.isAlikeDead() ? 1 : 0);
			writeC(_isSummoned ? 2 : 0); // 0=teleported 1=default 2=summoned
			writeS(_name);
			writeS(_title);
			writeD(0x00); // title color 0 = client default
			
			writeD(0x00);
			writeD(0x00); // pvp flag
			
			writeD(_trap.getAbnormalEffect()); // C2
			writeD(0x00); // clan id
			writeD(0x00); // crest id
			writeD(0000); // C2
			writeD(0000); // C2
			writeD(0000); // C2
			writeC(0000); // C2
			
			writeC(0x00); // Title color 0=client default
			
			writeF(_collisionRadius);
			writeF(_collisionHeight);
			writeD(0x00); // C4
			writeD(0x00); // C6
			writeD(0x00);
			writeD(0);// CT1.5 Pet form and skills
			if (Config.PACKET_FINAL)
			{
				writeC(0x01);
				writeC(0x01);
				writeD(0x00);
			}
		}
		
		@Override
		public boolean canBeSentTo(L2Client client, L2PcInstance activeChar)
		{
			if (!activeChar.canSee(_trap))
				return false;
			
			return true;
		}
	}
	
	/**
	 * Packet for summons
	 */
	public static class SummonInfo extends AbstractNpcInfo
	{
		private final L2Summon _summon;
		private int _form = 0;
		private int _val = 0;
		
		public SummonInfo(L2Summon cha, int val)
		{
			super(cha);
			_summon = cha;
			_val = val;
			
			int npcId = cha.getTemplate().getNpcId();
			
			if (npcId == 16041 || npcId == 16042)
			{
				if (cha.getLevel() > 84)
					_form = 3;
				else if (cha.getLevel() > 79)
					_form = 2;
				else if (cha.getLevel() > 74)
					_form = 1;
			}
			else if (npcId == 16025 || npcId == 16037)
				if (cha.getLevel() > 69)
					_form = 3;
				else if (cha.getLevel() > 64)
					_form = 2;
				else if (cha.getLevel() > 59)
					_form = 1;
			
			// fields not set on AbstractNpcInfo
			_rhand = cha.getWeapon();
			_lhand = 0;
			_chest = cha.getArmor();
			_collisionHeight = _summon.getTemplate().getCollisionHeight();
			_collisionRadius = _summon.getTemplate().getCollisionRadius();
			_name = cha.getName();
			_title = cha.getOwner() != null ? (cha.getOwner().isOnline() == 0 ? "" : cha.getOwner().getName()) : ""; // when
																														// owner
																														// online,
																														// summon
																														// will
																														// show
																														// in
																														// title
																														// owner
																														// name
			_idTemplate = cha.getTemplate().getIdTemplate();
			
			_collisionHeight = cha.getTemplate().getCollisionHeight();
			_collisionRadius = cha.getTemplate().getCollisionRadius();
			
			// few fields needing fix from AbstractNpcInfo
			_runSpd = cha.getPetSpeed();
			_walkSpd = cha.isMountable() ? 45 : 30;
		}
		
		@Override
		protected void writeImpl(L2Client client, L2PcInstance activeChar)
		{
			writeC(0x0c);
			writeD(_summon.getObjectId());
			writeD(_idTemplate + 1000000); // npctype id
			writeD(_summon.isAutoAttackable(activeChar) ? 1 : 0);
			writeD(_x);
			writeD(_y);
			writeD(_z);
			writeD(_heading);
			writeD(0x00);
			writeD(_mAtkSpd);
			writeD(_pAtkSpd);
			writeD(_runSpd);
			writeD(_walkSpd);
			writeD(_runSpd); // swim run speed
			writeD(_walkSpd); // swim walk speed
			writeD(_runSpd);
			writeD(_walkSpd);
			writeD(_runSpd); // fly run speed
			writeD(_walkSpd); // fly walk speed
			writeF(_summon.getStat().getMovementSpeedMultiplier());
			writeF(_summon.getStat().getAttackSpeedMultiplier());
			writeF(_collisionRadius);
			writeF(_collisionHeight);
			writeD(_rhand); // right hand weapon
			writeD(_chest);
			writeD(_lhand); // left hand weapon
			writeC(1); // name above char 1=true ... ??
			writeC(1);
			writeC(_summon.isInCombat() ? 1 : 0);
			writeC(_summon.isAlikeDead() ? 1 : 0);
			writeC(_val); // 0=teleported 1=default 2=summoned
			writeS(_name);
			writeS(_title);
			writeD(0x01);// Title color 0=client default
			
			writeD(_summon.getPvpFlag());
			writeD(_summon.getKarma());
			
			if (_summon.getOwner().getAppearance().isInvisible())
				writeD((_summon.getAbnormalEffect() | AbnormalEffect.STEALTH.getMask()));
			else
				writeD(_summon.getAbnormalEffect());
			writeD(_summon.getOwner().getClanId());
			writeD(0x00); // crest id
			writeD(0000); // C2
			writeD(0000); // C2
			writeC(0000); // C2
			
			writeC(_summon.getTeam());
			writeF(_collisionRadius);
			writeF(_collisionHeight);
			writeD(0x00); // C4
			writeD(0x00); // C6
			writeD(0x00);
			writeD(_form);// CT1.5 Pet form and skills
			if (Config.PACKET_FINAL)
			{
				writeC(0x01);
				writeC(0x01);
				writeD(0x00);
			}
		}
		
		@Override
		public boolean canBeSentTo(L2Client client, L2PcInstance activeChar)
		{
			// Owner gets PetInfo
			if (_summon.getOwner() == activeChar)
				return false;
			
			if (!activeChar.canSee(_summon))
				return false;
			
			return true;
		}
	}
	
	/**
	 * Packet for morphed PCs
	 */
	public static class PcMorphInfo extends AbstractNpcInfo
	{
		private final L2PcInstance _pc;
		private final L2NpcTemplate _template;
		
		public PcMorphInfo(L2PcInstance cha, L2NpcTemplate template)
		{
			super(cha);
			_pc = cha;
			_template = template;
		}
		
		@Override
		protected void writeImpl(L2Client client, L2PcInstance activeChar)
		{
			writeC(0x0c);
			writeD(_pc.getObjectId());
			writeD(_pc.getPoly().getPolyId() + 1000000); // npctype id
			writeD(_pc.isAutoAttackable(activeChar) ? 1 : 0);
			writeD(_x);
			writeD(_y);
			writeD(_z);
			writeD(_heading);
			writeD(0x00);
			writeD(_mAtkSpd);
			writeD(_pAtkSpd);
			writeD(_runSpd);
			writeD(_walkSpd);
			writeD(_runSpd); // swim run speed
			writeD(_walkSpd); // swim walk speed
			writeD(_runSpd);
			writeD(_walkSpd);
			writeD(_runSpd); // fly run speed
			writeD(_walkSpd); // fly walk speed
			writeF(_pc.getStat().getMovementSpeedMultiplier());
			writeF(_pc.getStat().getAttackSpeedMultiplier());
			writeF(_template.getCollisionRadius());
			writeF(_template.getCollisionHeight());
			writeD(_pc.getInventory().getPaperdollItemDisplayId(Inventory.PAPERDOLL_RHAND)); // right hand weapon
			writeD(0); // Chest
			writeD(_pc.getInventory().getPaperdollItemDisplayId(Inventory.PAPERDOLL_LHAND)); // left hand weapon
			writeC(1); // name above char 1=true ... ??
			writeC(_pc.isRunning() ? 1 : 0);
			writeC(_pc.isInCombat() ? 1 : 0);
			writeC(_pc.isAlikeDead() ? 1 : 0);
			writeC(0);
			writeS(_pc.getAppearance().getVisibleName());
			
			writeD(0x00); // Title Color
			writeD(_pc.getKarma());
			writeD(_pc.getPvpFlag());
			
			if (_pc.getAppearance().isInvisible())
				writeD((_pc.getAbnormalEffect() | AbnormalEffect.STEALTH.getMask()));
			else
				writeD(_pc.getAbnormalEffect()); // C2
				
			writeD(0x00);
			writeD(0x00);
			writeD(0x00);
			writeD(0x00);
			writeC(0x00);
			writeC(0x00);
			writeF(_template.getCollisionRadius());
			writeF(_template.getCollisionHeight());
			writeD(0x00);
			writeD(0x00);
			writeD(0x00);
			writeD(0x00);
			if (Config.PACKET_FINAL)
			{
				writeC(0x01);
				writeC(0x01);
				writeD(0x00);
			}
		}
		
		@Override
		public boolean canBeSentTo(L2Client client, L2PcInstance activeChar)
		{
			// Won't work
			if (_pc == activeChar)
				return false;
			
			if (!activeChar.canSee(_pc))
				return false;
			
			return true;
		}
	}
	
	public static class TestInfo extends AbstractNpcInfo
	{
		private static final String TITLE = "Spawn Guard";
		private static final String NAME = "Veteran";
		
		private final ConquestFactionMember _slave;
		private final L2Npc _test;
		private final boolean _cw;
		
		// private final L2PcInstance owner;
		
		public TestInfo(ConquestFactionMember slave, boolean cw)
		{
			super(slave.getActingCharacter());
			_slave = slave;
			_test = (L2Npc) slave.getActingCharacter();
			_cw = cw;
			// owner = player;
		}
		
		@Override
		protected void writeImpl()
		{
			// Inventory inv = owner.getInventory();
			
			writeC(0x31);
			writeD(_x);
			writeD(_y);
			writeD(_z);
			writeD(0x00); // airship ID
			writeD(_test.getObjectId());
			if (_cw)
				writeS("Zariche");
			else
				writeS(NAME); // writeS(owner.getAppearance().getVisibleName());
			writeD(0x00); // writeD(owner.getRace().ordinal());
			writeD(0x00); // writeD(owner.getAppearance().getSex() ? 1 : 0);
			
			/*
			if (owner.getClassIndex() == 0)
				writeD(owner.getClassId().getId());
			else
				writeD(owner.getBaseClass());
			 */writeD(0x00);
			
			int wpn = 14473;
			if (_cw)
				wpn = 8190;
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_UNDER));
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_HEAD));
			writeD(wpn); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_RHAND));
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_LHAND));
			writeD(14109); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_GLOVES));
			writeD(14105); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_CHEST));
			writeD(14108); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_LEGS));
			writeD(14110); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_FEET));
			writeD(14609); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_BACK));
			writeD(wpn); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_RHAND));
			writeD(20020); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_HAIR));
			writeD(20020); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_HAIR2));
			
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_RBRACELET));
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_LBRACELET));
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_DECO1));
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_DECO2));
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_DECO3));
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_DECO4));
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_DECO5));
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_DECO6));
			if (Config.PACKET_FINAL) // belt item ID
				writeD(0x00); // CT 2.3
				
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_UNDER));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_HEAD));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_RHAND));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_LHAND));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_GLOVES));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_CHEST));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_LEGS));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_FEET));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_BACK));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_LRHAND));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_HAIR));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_HAIR2));
			
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_RBRACELET));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_LBRACELET));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_DECO1));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_DECO2));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_DECO3));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_DECO4));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_DECO5));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_DECO6));
			if (Config.PACKET_FINAL)
			{
				// belt aug ID
				writeD(0x00); // CT 2.3
				writeD(0x00);
				writeD(0x00);
			}
			
			writeD(0x00); // writeD(owner.getPvpFlag());
			writeD(0xBAD); // writeD(owner.getKarma());
			
			writeD(_mAtkSpd);
			writeD(_pAtkSpd);
			
			writeD(0x00); // writeD(owner.getPvpFlag());
			writeD(0xBAD); // writeD(owner.getKarma());
			
			writeD(_runSpd);
			writeD(_walkSpd);
			writeD(0x32); // swim run speed (50)
			writeD(0x32); // swim walk speed (50)
			writeD(_runSpd);
			writeD(_walkSpd);
			writeD(_runSpd); // fly run speed
			writeD(_walkSpd); // fly walk speed
			/*writeF(1.5);*/writeF(_test.getStat().getMovementSpeedMultiplier()); // _activeChar.getProperMultiplier()
			/*writeF(1.8);*/writeF(_test.getStat().getAttackSpeedMultiplier()); // _activeChar.getAttackSpeedMultiplier
			
			/*
			if (owner.getAppearance().getSex())
			{
			writeF(owner.getBaseTemplate().getFCollisionRadius());
			writeF(owner.getBaseTemplate().getFCollisionHeight());
			}
			else
			{
			writeF(owner.getBaseTemplate().getCollisionRadius());
			writeF(owner.getBaseTemplate().getCollisionHeight());
			}
			 */
			if (_cw)
			{
				writeF(0x0C);
				writeF(31.58);
			}
			else
			{
				writeF(0x09);
				writeF(0x17);
			}
			
			writeD(0x00); // writeD(owner.getAppearance().getHairStyle());
			writeD(0x00); // writeD(owner.getAppearance().getHairColor());
			writeD(0x00); // writeD(owner.getAppearance().getFace());
			
			if (_cw)
				writeS("Unholy Servitor");
			else
				writeS(TITLE); // writeS(owner.getAppearance().getVisibleTitle());
				
			writeD(0x00); // writeD(owner.getClanId());
			writeD(0x00); // writeD(owner.getClanCrestId());
			writeD(0x00); // writeD(owner.getAllyId());
			writeD(0x00); // writeD(owner.getAllyCrestId());
			// In UserInfo leader rights and siege flags, but here found nothing??
			// Therefore RelationChanged packet with that info is required
			writeD(0);
			
			writeC(0x01); // writeC(owner.isSitting() ? 0 : 1); // standing = 1 sitting = 0
			writeC(0x01); // writeC(owner.isRunning() ? 1 : 0); // running = 1 walking = 0
			writeC(_test.isInCombat()); // writeC(owner.isInCombat() ? 1 : 0);
			writeC(0x00); // writeC(owner.isAlikeDead() ? 1 : 0);
			
			writeC(0x00); // writeC(owner.getAppearance().isInvisible() ? 1 : 0); // invisible = 1 visible =0
			
			writeC(0x00); // writeC(owner.getMountType()); // 1 on strider 2 on wyvern 3 on Great Wolf 0 no mount
			writeC(0x00); // writeC(owner.getPrivateStoreType()); // 1 - sellshop
			
			/*
			writeH(owner.getCubics().size());
			for (int id : owner.getCubics().keySet())
			writeH(id);
			 */writeH(0x00);
			
			writeC(0x00); // writeC(owner.isLookingForParty());
			
			writeD(0x00); // writeD(owner.getAbnormalEffect());
			
			writeC(0x00); // writeC(owner.isFlying() ? 2 : 0);
			writeH(0xFF); // writeH(owner.getEvalPoints()); // Blue value for name (0 = white, 255 = pure blue)
			writeD(0x00); // writeD(owner.getMountNpcId() + 1000000);
			
			writeD(0x00); // writeD(owner.getClassId().getId());
			writeD(0x00); // ?
			writeC(127); // writeC(owner.isMounted() ? 0 : owner.getEnchantEffect());
			
			/*
			if (owner.getTeam() == 1)
			writeC(0x01); // team circle around feet 1= Blue, 2 = red
			else if (owner.getTeam() == 2)
			writeC(0x02); // team circle around feet 1= Blue, 2 = red
			else
			writeC(0x00); // team circle around feet 1= Blue, 2 = red
			 */writeC(0x00);
			
			writeD(0x00); // writeD(owner.getClanCrestLargeId());
			writeC(0x00); // writeC(owner.isNoble() ? 1 : 0); // Symbol on char menu ctrl+I
			writeC(0x00); // writeC((owner.isHero() || (owner.isGM() && Config.GM_HERO_AURA)) ? 1 : 0); // Hero Aura
			
			writeC(0x00); // writeC(owner.isFishing() ? 1 : 0); // 0x01: Fishing Mode (Cant be undone by setting back to
							// 0)
			writeD(0x00); // writeD(owner.getFishx());
			writeD(0x00); // writeD(owner.getFishy());
			writeD(0x00); // writeD(owner.getFishz());
			
			writeD(0xFFFFFF); // writeD(owner.getAppearance().getNameColor());
			
			writeD(_heading);
			
			writeD(0x0B); // writeD(owner.getPledgeClass());
			writeD(0x00); // writeD(owner.getSubPledgeType());
			
			FactionTemplate ft = ConquestFactionManager.getInstance().getFactionTemplate(_slave);
			if (ft == null)
				writeD(0xFF0000);
			else
				writeD(ft.getColor()); // writeD(owner.getAppearance().getTitleColor());
				
			/*
			if (owner.isCursedWeaponEquipped())
			writeD(CursedWeaponsManager.getInstance().getLevel(owner.getCursedWeaponEquippedId()));
			else
			writeD(0x00);
			 */writeD(0x00); // > 1 removes title
			
			// T1
			/*
			if (owner.getClan() != null)
			writeD(owner.getClan().getReputationScore());
			else
			writeD(0x00);
			 */writeD(0x00);
			
			// T1
			if (_cw)
				writeD(301);
			else
				writeD(0x00);
			writeD(0x00); // Can Decoys have Agathions?
			
			writeD(0x00);
			
			if (Config.PACKET_FINAL)
			{
				// T2.3
				writeD(0x00); // writeD(owner.getSpecialEffect());
				writeD(0x00);
				writeD(0x00);
				writeD(0x00);
			}
		}
		
		@Override
		public boolean canBeSentTo(L2Client client, L2PcInstance activeChar)
		{
			if (!activeChar.canSee(_test))
				return false;
			
			return true;
		}
	}
	
	public static class MartyrInfo extends AbstractNpcInfo
	{
		private static final int MARTYR = 3434;
		
		private final int _oid;
		
		public MartyrInfo(L2MartyrInstance martyr)
		{
			super(martyr);
			_oid = martyr.getObjectId();
		}
		
		@Override
		protected void writeImpl()
		{
			writeC(0x05);
			writeD(_oid);
			writeD(MARTYR);
			writeD(_x);
			writeD(_y);
			writeD(_z);
			writeD(0x00); // not stackable
			writeCompQ(0x01); // count
			writeD(0x00);
		}
	}
	
	public static class AdvertInfo extends AbstractNpcInfo
	{
		private final AdvertiserInstance _ad;
		
		public AdvertInfo(AdvertiserInstance ad)
		{
			super(ad);
			_ad = ad;
		}
		
		@Override
		protected void writeImpl()
		{
			writeC(0x31);
			writeD(_x);
			writeD(_y);
			writeD(_z);
			writeD(0x00); // airship ID
			writeD(_ad.getObjectId());
			writeS(String.valueOf(_ad.getObjectId()));
			writeD(_ad.getRace()); // writeD(owner.getRace().ordinal());
			writeD(_ad.getSex()); // writeD(owner.getAppearance().getSex() ? 1 : 0);
			
			writeD(_ad.getClassType());
			
			AdvertEquip eq = _ad.getEquip();
			writeD(eq.getUnder());
			writeD(eq.getHelmet());
			writeD(eq.getWeapon());
			writeD(eq.getShield());
			writeD(eq.getGloves());
			writeD(eq.getChest());
			writeD(eq.getLegs());
			writeD(eq.getBoots());
			writeD(eq.getCloak());
			writeD(eq.getWeapon());
			writeD(eq.getFace1());
			writeD(eq.getFace2());
			
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_RBRACELET));
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_LBRACELET));
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_DECO1));
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_DECO2));
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_DECO3));
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_DECO4));
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_DECO5));
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_DECO6));
			if (Config.PACKET_FINAL) // belt item ID
				writeD(0x00); // CT 2.3
				
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_UNDER));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_HEAD));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_RHAND));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_LHAND));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_GLOVES));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_CHEST));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_LEGS));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_FEET));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_BACK));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_LRHAND));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_HAIR));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_HAIR2));
			
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_RBRACELET));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_LBRACELET));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_DECO1));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_DECO2));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_DECO3));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_DECO4));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_DECO5));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_DECO6));
			if (Config.PACKET_FINAL)
			{
				// belt aug ID
				writeD(0x00); // CT 2.3
				writeD(0x00);
				writeD(0x00);
			}
			
			writeD(0x00); // writeD(owner.getPvpFlag());
			writeD(0x00); // writeD(owner.getKarma());
			
			writeD(_mAtkSpd);
			writeD(_pAtkSpd);
			
			writeD(0x00); // writeD(owner.getPvpFlag());
			writeD(0x00); // writeD(owner.getKarma());
			
			writeD(_runSpd);
			writeD(_walkSpd);
			writeD(0x32); // swim run speed (50)
			writeD(0x32); // swim walk speed (50)
			writeD(_runSpd);
			writeD(_walkSpd);
			writeD(_runSpd); // fly run speed
			writeD(_walkSpd); // fly walk speed
			writeF(_ad.getStat().getMovementSpeedMultiplier()); // _activeChar.getProperMultiplier()
			writeF(_ad.getStat().getAttackSpeedMultiplier()); // _activeChar.getAttackSpeedMultiplier
			
			writeF(_ad.getCr());
			writeF(_ad.getCh());
			
			writeD(_ad.getHairStyle());
			writeD(_ad.getHairColor());
			writeD(_ad.getFace());
			
			writeS("Generic");
			
			writeD(0x00); // writeD(owner.getClanId());
			writeD(0x00); // writeD(owner.getClanCrestId());
			writeD(0x00); // writeD(owner.getAllyId());
			writeD(0x00); // writeD(owner.getAllyCrestId());
			// In UserInfo leader rights and siege flags, but here found nothing??
			// Therefore RelationChanged packet with that info is required
			writeD(0);
			
			writeC(0x01); // writeC(owner.isSitting() ? 0 : 1); // standing = 1 sitting = 0
			writeC(0x01); // writeC(owner.isRunning() ? 1 : 0); // running = 1 walking = 0
			writeC(_ad.isInCombat()); // writeC(owner.isInCombat() ? 1 : 0);
			writeC(0x00); // writeC(owner.isAlikeDead() ? 1 : 0);
			
			writeC(0x00); // writeC(owner.getAppearance().isInvisible() ? 1 : 0); // invisible = 1 visible =0
			
			writeC(0x00); // writeC(owner.getMountType()); // 1 on strider 2 on wyvern 3 on Great Wolf 0 no mount
			writeC(0x00); // writeC(owner.getPrivateStoreType()); // 1 - sellshop
			
			/*
			writeH(owner.getCubics().size());
			for (int id : owner.getCubics().keySet())
				writeH(id);
			 */writeH(0x00);
			
			writeC(0x00); // writeC(owner.isLookingForParty());
			
			writeD(0x00); // writeD(owner.getAbnormalEffect());
			
			writeC(0x00); // writeC(owner.isFlying() ? 2 : 0);
			writeH(0x00); // writeH(owner.getEvalPoints()); // Blue value for name (0 = white, 255 = pure blue)
			writeD(0x00); // writeD(owner.getMountNpcId() + 1000000);
			
			writeD(_ad.getClassType()); // writeD(owner.getClassId().getId());
			writeD(0x00); // ?
			writeC(0x00); // writeC(owner.isMounted() ? 0 : owner.getEnchantEffect());
			
			/*
			if (owner.getTeam() == 1)
			writeC(0x01); // team circle around feet 1= Blue, 2 = red
			else if (owner.getTeam() == 2)
			writeC(0x02); // team circle around feet 1= Blue, 2 = red
			else
			writeC(0x00); // team circle around feet 1= Blue, 2 = red
			 */writeC(0x00);
			
			writeD(0x00); // writeD(owner.getClanCrestLargeId());
			writeC(0x00); // writeC(owner.isNoble() ? 1 : 0); // Symbol on char menu ctrl+I
			writeC(0x00); // writeC((owner.isHero() || (owner.isGM() && Config.GM_HERO_AURA)) ? 1 : 0); // Hero Aura
			
			writeC(0x00); // writeC(owner.isFishing() ? 1 : 0); // 0x01: Fishing Mode (Cant be undone by setting back to
							// 0)
			writeD(0x00); // writeD(owner.getFishx());
			writeD(0x00); // writeD(owner.getFishy());
			writeD(0x00); // writeD(owner.getFishz());
			
			writeD(0xFFFFFF); // writeD(owner.getAppearance().getNameColor());
			
			writeD(_heading);
			
			writeD(0x00); // writeD(owner.getPledgeClass());
			writeD(0x00); // writeD(owner.getSubPledgeType());
			
			writeD(0xFF7700); // writeD(owner.getAppearance().getTitleColor());
			
			writeD(0x00); // > 1 removes title
			
			// T1
			writeD(0x00); // clan rep
			
			// T1
			writeD(0x00); // transform id
			writeD(0x00); // Can Decoys have Agathions?
			
			writeD(0x00);
			
			if (Config.PACKET_FINAL)
			{
				// T2.3
				writeD(0x00); // writeD(owner.getSpecialEffect());
				writeD(0x00);
				writeD(0x00);
				writeD(0x00);
			}
		}
	}
	
	public static class CaravanInfo extends AbstractNpcInfo
	{
		private final L2MonsterInstance _traveler;
		private final PlayerLookalike _looks;
		
		public CaravanInfo(L2MonsterInstance traveler)
		{
			super(traveler);
			_traveler = traveler;
			_looks = (PlayerLookalike) _traveler;
			_name = traveler.getTemplate().getName();
			_title = traveler.getTemplate().getTitle();
		}
		
		@Override
		protected void writeImpl()
		{
			writeC(0x31);
			writeD(_x);
			writeD(_y);
			writeD(_z);
			writeD(0x00); // airship ID
			writeD(_traveler.getObjectId());
			writeS(_name);
			writeD(_looks.getRace());
			writeD(_looks.getSex());
			
			writeD(_looks.getClassType());
			
			AdvertEquip eq = _looks.getEquip();
			writeD(eq.getUnder());
			writeD(eq.getHelmet());
			writeD(/*eq.getWeapon()*/0x00);
			writeD(eq.getShield());
			writeD(eq.getGloves());
			writeD(eq.getChest());
			writeD(eq.getLegs());
			writeD(eq.getBoots());
			writeD(eq.getCloak());
			writeD(/*eq.getWeapon()*/0x00);
			writeD(eq.getFace1());
			writeD(eq.getFace2());
			
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_RBRACELET));
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_LBRACELET));
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_DECO1));
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_DECO2));
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_DECO3));
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_DECO4));
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_DECO5));
			writeD(0x00); // writeD(inv.getPaperdollItemId(Inventory.PAPERDOLL_DECO6));
			if (Config.PACKET_FINAL) // belt item ID
				writeD(0x00); // CT 2.3
				
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_UNDER));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_HEAD));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_RHAND));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_LHAND));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_GLOVES));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_CHEST));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_LEGS));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_FEET));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_BACK));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_LRHAND));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_HAIR));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_HAIR2));
			
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_RBRACELET));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_LBRACELET));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_DECO1));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_DECO2));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_DECO3));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_DECO4));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_DECO5));
			writeD(0x00); // writeD(inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_DECO6));
			if (Config.PACKET_FINAL)
			{
				// belt aug ID
				writeD(0x00); // CT 2.3
				writeD(0x00);
				writeD(0x00);
			}
			
			writeD(0x00); // writeD(owner.getPvpFlag());
			writeD(0x00); // writeD(owner.getKarma());
			
			writeD(_mAtkSpd);
			writeD(_pAtkSpd);
			
			writeD(0x00); // writeD(owner.getPvpFlag());
			writeD(0x00); // writeD(owner.getKarma());
			
			writeD(_runSpd);
			writeD(_walkSpd);
			writeD(0x32); // swim run speed (50)
			writeD(0x32); // swim walk speed (50)
			writeD(_runSpd);
			writeD(_walkSpd);
			writeD(_runSpd); // fly run speed
			writeD(_walkSpd); // fly walk speed
			writeF(_traveler.getStat().getMovementSpeedMultiplier()); // _activeChar.getProperMultiplier()
			writeF(_traveler.getStat().getAttackSpeedMultiplier()); // _activeChar.getAttackSpeedMultiplier
			
			writeF(_looks.getCr());
			writeF(_looks.getCh());
			
			writeD(_looks.getHairStyle());
			writeD(_looks.getHairColor());
			writeD(_looks.getFace());
			
			writeS(_title);
			
			writeD(0x00); // writeD(owner.getClanId());
			writeD(0x00); // writeD(owner.getClanCrestId());
			writeD(0x00); // writeD(owner.getAllyId());
			writeD(0x00); // writeD(owner.getAllyCrestId());
			// In UserInfo leader rights and siege flags, but here found nothing??
			// Therefore RelationChanged packet with that info is required
			writeD(0);
			
			writeC(0x01); // writeC(owner.isSitting() ? 0 : 1); // standing = 1 sitting = 0
			writeC(_traveler.isRunning());
			writeC(_traveler.isInCombat());
			writeC(0x00); // writeC(owner.isAlikeDead() ? 1 : 0);
			
			writeC(0x00); // writeC(owner.getAppearance().isInvisible() ? 1 : 0); // invisible = 1 visible =0
			
			writeC(0x01); // writeC(owner.getMountType()); // 1 on strider 2 on wyvern 3 on Great Wolf 0 no mount
			writeC(0x00); // writeC(owner.getPrivateStoreType()); // 1 - sellshop
			
			/*
			writeH(owner.getCubics().size());
			for (int id : owner.getCubics().keySet())
				writeH(id);
			 */writeH(0x00);
			
			writeC(0x00); // writeC(owner.isLookingForParty());
			
			writeD(0x00); // writeD(owner.getAbnormalEffect());
			
			writeC(0x00); // writeC(owner.isFlying() ? 2 : 0);
			writeH(0x00); // writeH(owner.getEvalPoints()); // Blue value for name (0 = white, 255 = pure blue)
			if (_traveler.getNpcId() == 90059)
				writeD(1016038);
			else
				writeD(1012528);
			
			writeD(_looks.getClassType()); // writeD(owner.getClassId().getId());
			writeD(0x00); // ?
			writeC(0x00); // writeC(owner.isMounted() ? 0 : owner.getEnchantEffect());
			
			/*
			if (owner.getTeam() == 1)
			writeC(0x01); // team circle around feet 1= Blue, 2 = red
			else if (owner.getTeam() == 2)
			writeC(0x02); // team circle around feet 1= Blue, 2 = red
			else
			writeC(0x00); // team circle around feet 1= Blue, 2 = red
			 */writeC(0x00);
			
			writeD(0x00); // writeD(owner.getClanCrestLargeId());
			writeC(0x00); // writeC(owner.isNoble() ? 1 : 0); // Symbol on char menu ctrl+I
			writeC(_traveler.getNpcId() == 90059); // writeC((owner.isHero() || (owner.isGM() && Config.GM_HERO_AURA)) ?
													// 1 : 0); // Hero Aura
			
			writeC(0x00); // writeC(owner.isFishing() ? 1 : 0); // 0x01: Fishing Mode (Cant be undone by setting back to
							// 0)
			writeD(0x00); // writeD(owner.getFishx());
			writeD(0x00); // writeD(owner.getFishy());
			writeD(0x00); // writeD(owner.getFishz());
			
			writeD(0xFFFFFF); // writeD(owner.getAppearance().getNameColor());
			
			writeD(_heading);
			
			writeD(0x00); // writeD(owner.getPledgeClass());
			writeD(0x00); // writeD(owner.getSubPledgeType());
			
			writeD(0xFFFF77); // writeD(owner.getAppearance().getTitleColor());
			
			writeD(0x00); // > 1 removes title
			
			// T1
			writeD(0x00); // clan rep
			
			// T1
			writeD(0x00); // transform id
			writeD(0x00); // Can Decoys have Agathions?
			
			writeD(0x00);
			
			if (Config.PACKET_FINAL)
			{
				// T2.3
				writeD(0x00); // writeD(owner.getSpecialEffect());
				writeD(0x00);
				writeD(0x00);
				writeD(0x00);
			}
		}
	}
}
