/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.network.clientpackets;

import org.sod.manager.ItemAuctionManager;
import org.sod.manager.ItemAuctionManager.ItemReflection;

import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.serverpackets.ExItemAuctionInfo;

/**
 * This packet is sent by client at a second's intervals once it first receives
 * the ExItemAuctionInfo packet.
 * @author savormix
 */
public final class RequestInfoItemAuction extends L2GameClientPacket
{
	private int _auctionId;

	@Override
	protected void readImpl()
	{
		_auctionId = readD();
	}

	@Override
	protected void runImpl()
	{
		L2PcInstance player = getActiveChar();
		if (player == null)
			return;

		ItemReflection ir = ItemAuctionManager.getInstance().getAuction(_auctionId);
		if (ir == null || ir.getState() == 0)
		{
			//sendAF();
			return;
		}
		player.sendPacket(new ExItemAuctionInfo(ir, false));
	}
}
