/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.network.client.packets.sendable;

import com.l2jfree.ClientProtocolVersion;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.client.L2Client;
import com.l2jfree.gameserver.network.client.packets.L2ServerPacket;
import com.l2jfree.network.mmocore.MMOBuffer;

/**
 * @author savormix (generated)
 */
public abstract class SocialActionPacket extends L2ServerPacket
{
	/**
	 * A nicer name for {@link SocialActionPacket}.
	 * 
	 * @author savormix (generated)
	 * @see SocialActionPacket
	 */
	public static final class SocialAction extends SocialActionPacket
	{
		public static final int LEVEL_UP = 2122;
		
		/**
		 * Constructs this packet.
		 * 
		 * @see SocialActionPacket#SocialActionPacket()
		 */
		public SocialAction(L2Character actor, int action)
		{
			super(actor, action);
		}
	}
	
	private final Integer _objectId;
	private final int _action;
	
	protected SocialActionPacket(L2Character actor, int action)
	{
		_objectId = actor.getObjectId();
		_action = action;
	}
	
	@Override
	protected int getOpcode()
	{
		return 0x27;
	}
	
	@Override
	protected void writeImpl(L2Client client, L2PcInstance activeChar, MMOBuffer buf) throws RuntimeException
	{
		// TODO: when implementing, consult an up-to-date packets_game_server.xml and/or savormix
		buf.writeD(_objectId); // Actor OID
		if (_action == SocialAction.LEVEL_UP && client.getVersion().isOlderThan(ClientProtocolVersion.GRACIA_FINAL))
			buf.writeD(15);
		else
			buf.writeD(_action); // Action ID
		if (client.getVersion().isNewerThanOrEqualTo(ClientProtocolVersion.GODDESS_OF_DESTRUCTION))
			buf.writeD(0); // ??? 0
	}
}
