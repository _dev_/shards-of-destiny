/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.network.client.packets.sendable;

import com.l2jfree.ClientProtocolVersion;
import com.l2jfree.gameserver.cache.HtmCache;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.client.L2Client;
import com.l2jfree.gameserver.network.client.packets.L2ServerPacket;
import com.l2jfree.gameserver.templates.item.L2Item;
import com.l2jfree.lang.Replaceable;
import com.l2jfree.network.mmocore.MMOBuffer;

/**
 * @author savormix (generated)
 */
public class NpcHtmlMessage extends L2ServerPacket
{
	public static final String[] VALID_TAGS =
	{
			"volumn", "unknown", "ul", "u", "tt", "tr", "title", "textcode", "textarea", "td", "table", "sup", "sub",
			"strike", "spin", "select", "right", "pre", "p", "option", "ol", "multiedit", "li", "left", "input", "img",
			"i", "html", "h7", "h6", "h5", "h4", "h3", "h2", "h1", "font", "extend", "edit", "comment", "combobox",
			"center", "button", "br", "br1", "body", "bar", "address", "a", "sel", "list", "var", "fore", "readonl",
			"rows", "valign", "fixwidth", "bordercolorli", "bordercolorda", "bordercolor", "border", "bgcolor",
			"background", "align", "valu", "readonly", "multiple", "selected", "typ", "type", "maxlength", "checked",
			"src", "y", "x", "querydelay", "noscrollbar", "imgsrc", "b", "fg", "size", "face", "color", "deffon",
			"deffixedfont", "width", "value", "tooltip", "name", "min", "max", "height", "disabled", "align", "msg",
			"link", "href", "action"
	};
	
	private final Integer _objectId;
	private Replaceable _message;
	private final int _item;
	
	public NpcHtmlMessage()
	{
		this(null, 0);
	}
	
	public NpcHtmlMessage(L2Npc npc)
	{
		this(npc, null);
	}
	
	public NpcHtmlMessage(CharSequence message)
	{
		this(message, 0);
	}
	
	public NpcHtmlMessage(CharSequence message, int item)
	{
		this(0, message, false, item);
	}
	
	public NpcHtmlMessage(CharSequence message, L2Item title)
	{
		this(message, title.getItemDisplayId());
	}
	
	public NpcHtmlMessage(CharSequence content, boolean file, L2Item title)
	{
		this(0, content, file, title.getItemDisplayId());
	}
	
	public NpcHtmlMessage(L2Npc npc, CharSequence message)
	{
		this(npc, message, false);
	}
	
	public NpcHtmlMessage(L2Npc npc, CharSequence content, boolean file)
	{
		this(npc.getObjectId(), content, file, 0);
	}
	
	public NpcHtmlMessage(L2Npc npc, CharSequence content, boolean file, L2Item title)
	{
		this(npc.getObjectId(), content, file, title.getItemDisplayId());
	}
	
	private NpcHtmlMessage(Integer npcId, CharSequence content, boolean file, int item)
	{
		_objectId = npcId;
		if (content != null)
		{
			if (file)
				setFile(content.toString());
			else
				setMessage(content);
		}
		_item = item;
	}
	
	@Deprecated
	public NpcHtmlMessage setHtml(CharSequence message)
	{
		return setMessage(message);
	}
	
	public NpcHtmlMessage setMessage(CharSequence message)
	{
		_message = Replaceable.valueOf(message);
		return this;
	}
	
	@Deprecated
	public NpcHtmlMessage setPath(String file)
	{
		return setFile(file);
	}
	
	public NpcHtmlMessage setFile(String file)
	{
		_message = Replaceable.valueOf(HtmCache.getInstance().getHtm(file));
		return this;
	}
	
	public void replace(String pattern, String value)
	{
		_message.replace(pattern, value);
	}
	
	public void replace(String pattern, long value)
	{
		_message.replace(pattern, value);
	}
	
	public void replace(String pattern, double value)
	{
		_message.replace(pattern, value);
	}
	
	public void replace(String pattern, Object value)
	{
		_message.replace(pattern, value);
	}
	
	@Override
	protected int getOpcode()
	{
		return 0x19;
	}
	
	@Override
	public boolean canBeSentTo(L2Client client, L2PcInstance activeChar)
	{
		return _message != null && _message.length() < 8192;
	}
	
	@Override
	public void prepareToSend(L2Client client, L2PcInstance activeChar)
	{
		if (activeChar != null)
			activeChar.buildBypassCache(_message);
	}
	
	@Override
	protected void writeImpl(L2Client client, L2PcInstance activeChar, MMOBuffer buf) throws RuntimeException
	{
		// TODO: when implementing, consult an up-to-date packets_game_server.xml and/or savormix
		buf.writeD(_objectId); // NPC OID
		buf.writeS(_message); // Message
		buf.writeD(_item); // Title
		if (client.getVersion().isNewerThanOrEqualTo(ClientProtocolVersion.GODDESS_OF_DESTRUCTION))
			buf.writeD(0); // ??? 0
	}
}
