/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.network.client.packets.sendable;

import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.client.L2Client;
import com.l2jfree.gameserver.network.client.packets.L2ServerPacket;
import com.l2jfree.network.mmocore.MMOBuffer;

/**
 * @author savormix (generated)
 */
public abstract class TutorialShowQuestionMarkPacket extends L2ServerPacket
{
	/**
	 * A nicer name for {@link TutorialShowQuestionMarkPacket}.
	 * 
	 * @author savormix (generated)
	 * @see TutorialShowQuestionMarkPacket
	 */
	public static final class ShowTutorialMark extends TutorialShowQuestionMarkPacket
	{
		public static final int QUEUED_DELAY = 2000;
		public static final int RAID_REQUEST = 0x4FFF;
		public static final int RAID_REQUEST_INVALIDATED = 0x4FFE;
		public static final int RAID_REQUEST_DENIED = 0x4FFD;
		public static final int RAID_REQUEST_VIEWED = 0x4FFC;
		public static final int CHALLENGE_OFFSET = 0x5800;
		public static final int PERK_UNLOCK_OFFSET = 0x5000;
		public static final int PERK_UPGRADE_OFFSET = 0x5400;
		
		/**
		 * Constructs this packet.
		 * 
		 * @see TutorialShowQuestionMarkPacket#TutorialShowQuestionMarkPacket(int)
		 */
		public ShowTutorialMark(int mark)
		{
			super(mark);
		}
	}
	
	private final int _mark;
	
	protected TutorialShowQuestionMarkPacket(int mark)
	{
		_mark = mark;
	}
	
	@Override
	protected int getOpcode()
	{
		return 0xa7;
	}
	
	@Override
	protected void writeImpl(L2Client client, L2PcInstance activeChar, MMOBuffer buf) throws RuntimeException
	{
		// TODO: when implementing, consult an up-to-date packets_game_server.xml and/or savormix
		buf.writeD(_mark); // Mark ID
	}
}
