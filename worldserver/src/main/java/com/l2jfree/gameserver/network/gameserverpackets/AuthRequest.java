/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.network.gameserverpackets;

import java.util.LinkedList;
import java.util.List;

import com.l2jfree.Config;

public final class AuthRequest extends GameServerBasePacket {
	public AuthRequest(int id, boolean acceptAlternate, byte[] hexid, String _gsNetConfig1,
			String _gsNetConfig2, int port, boolean reserveHost, int maxplayer) {
		super(0x01);
		writeC(id);
		writeC(acceptAlternate ? 0x01 : 0x00);
		writeC(reserveHost ? 0x01 : 0x00);
		// 258 protocol hosts
		// writeS(_gsNetConfig1);
		// writeS(_gsNetConfig2);
		writeH(port);
		writeD(maxplayer);
		writeD(hexid.length);
		writeB(hexid);
		// 262+ protocol subnets
		List<String> items = new LinkedList<String>();
		String[] intExt = Config.SUBNETWORKS.split(";");
		for (String cfg : intExt) {
			String[] hostAndSub = cfg.split(",");
			for (int i = 1; i < hostAndSub.length; i++) {
				items.add(hostAndSub[i]);
				items.add(hostAndSub[0]);
			}
		}
		writeD(items.size() / 2);
		for (String i : items)
			writeS(i);
	}
}
