/*
 * MISSING LICENSING INFO
 */
package com.l2jfree.gameserver.network.client.packets.sendable.generic;

import java.util.LinkedList;
import java.util.List;

import com.l2jfree.ClientProtocolVersion;
import com.l2jfree.gameserver.model.L2ItemInstance;
import com.l2jfree.gameserver.model.L2Object;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.entity.Castle;
import com.l2jfree.gameserver.model.entity.ClanHall;
import com.l2jfree.gameserver.model.entity.Fort;
import com.l2jfree.gameserver.network.client.L2Client;
import com.l2jfree.gameserver.network.client.packets.L2ServerPacket;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;
import com.l2jfree.gameserver.templates.item.L2Item;
import com.l2jfree.network.mmocore.MMOBuffer;

/**
 * @author savormix
 * 
 */
public abstract class ClientSideMessage extends L2ServerPacket
{
	private final int _message;
	private final List<Element> _params;
	
	public ClientSideMessage(int message)
	{
		_message = message;
		_params = new LinkedList<Element>();
	}
	
	@Override
	protected void writeImpl(L2Client client, L2PcInstance activeChar, MMOBuffer buf) throws RuntimeException
	{
		// TODO: when implementing, consult an up-to-date packets_game_server.xml and/or savormix
		buf.writeD(_message); // Message
		buf.writeD(_params.size()); // Parameter count
		for (Element e : _params)
		{
			buf.writeD(e.getType()); // Parameter, branching condition
			e.write(client.getVersion(), buf);
		}
	}
	
	private abstract static class Element
	{
		public abstract int getType();
		
		public abstract void write(ClientProtocolVersion cpv, MMOBuffer buf);
	}
	
	public ClientSideMessage addString(String value)
	{
		_params.add(new StringElement(value, false));
		return this;
	}
	
	@Deprecated
	public ClientSideMessage addNumber(int value)
	{
		return addInt(value);
	}
	
	public ClientSideMessage addInt(int value)
	{
		_params.add(new IntElement(value, 1));
		return this;
	}
	
	@Deprecated
	public ClientSideMessage addNpcName(L2Npc npc)
	{
		return addNpc(npc);
	}
	
	public ClientSideMessage addNpc(L2Npc npc)
	{
		return addNpc(npc.getTemplate());
	}
	
	public ClientSideMessage addNpc(L2NpcTemplate npc)
	{
		return addNpc(npc.getIdTemplate());
	}
	
	public ClientSideMessage addNpc(int npc)
	{
		_params.add(new IntElement(npc, 2));
		return this;
	}
	
	@Deprecated
	public ClientSideMessage addItemName(L2ItemInstance item)
	{
		return addItem(item);
	}
	
	@Deprecated
	public ClientSideMessage addItemName(int item)
	{
		return addItem(item);
	}
	
	public ClientSideMessage addItem(L2ItemInstance item)
	{
		return addItem(item.getItemDisplayId());
	}
	
	public ClientSideMessage addItem(L2Item item)
	{
		return addItem(item.getItemDisplayId());
	}
	
	public ClientSideMessage addItem(int item)
	{
		_params.add(new IntElement(item, 3));
		return this;
	}
	
	@Deprecated
	public ClientSideMessage addSkillName(L2Skill skill)
	{
		return addSkill(skill);
	}
	
	public ClientSideMessage addSkill(L2Skill skill)
	{
		return addSkill(skill.getDisplayId(), skill.getLevel());
	}
	
	public ClientSideMessage addSkill(int skill, int level)
	{
		_params.add(new SkillElement(skill, level));
		return this;
	}
	
	public ClientSideMessage addPledgeBase(Castle pledgeBase)
	{
		return addPledgeBase(pledgeBase.getCastleId());
	}
	
	public ClientSideMessage addPledgeBase(Fort pledgeBase)
	{
		return addPledgeBase(pledgeBase.getFortId());
	}
	
	public ClientSideMessage addPledgeBase(ClanHall pledgeBase)
	{
		return addPledgeBase(pledgeBase.getId());
	}
	
	public ClientSideMessage addPledgeBase(int pledgeBase)
	{
		_params.add(new IntElement(pledgeBase, 5));
		return this;
	}
	
	@Deprecated
	public ClientSideMessage addExpNumber(long quantity)
	{
		return addQuantity(quantity);
	}
	
	@Deprecated
	public ClientSideMessage addItemNumber(long quantity)
	{
		return addQuantity(quantity);
	}
	
	public ClientSideMessage addQuantity(long quantity)
	{
		return addLong(quantity);
	}
	
	public ClientSideMessage addLong(long value)
	{
		_params.add(new LongElement(value));
		return this;
	}
	
	public ClientSideMessage addRegion(int x, int y, int z)
	{
		_params.add(new ZoneElement(x, y, z));
		return this;
	}
	
	public ClientSideMessage addEquipment(int item)
	{
		_params.add(new EquipmentElement(item));
		return this;
	}
	
	public ClientSideMessage addElemental(byte element)
	{
		return addElemental((int) element);
	}
	
	public ClientSideMessage addElemental(int element)
	{
		_params.add(new IntElement(element, 9));
		return this;
	}
	
	public ClientSideMessage addInstance(int instance)
	{
		_params.add(new IntElement(instance, 10));
		return this;
	}
	
	public ClientSideMessage addFString(int string)
	{
		_params.add(new IntElement(string, 11));
		return this;
	}
	
	public ClientSideMessage addPlayer(String name)
	{
		_params.add(new StringElement(name, true));
		return this;
	}
	
	public ClientSideMessage addSysString(int string)
	{
		_params.add(new IntElement(string, 13));
		return this;
	}
	
	public ClientSideMessage addDamage(L2Object target, L2Object attacker, int damage)
	{
		_params.add(new DamageElement(target.getObjectId(), attacker.getObjectId(), damage));
		return this;
	}
	
	private static class StringElement extends Element
	{
		private final String _value;
		private final boolean _player;
		
		public StringElement(String value, boolean player)
		{
			_value = value;
			_player = player;
		}
		
		@Override
		public int getType()
		{
			return _player ? 12 : 0;
		}
		
		@Override
		public void write(ClientProtocolVersion cpv, MMOBuffer buf)
		{
			buf.writeS(_value);
		}
	}
	
	private static class IntElement extends Element
	{
		private final int _value;
		private final int _type;
		
		public IntElement(int value, int type)
		{
			_value = value;
			_type = type;
		}
		
		@Override
		public int getType()
		{
			return _type;
		}
		
		@Override
		public void write(ClientProtocolVersion cpv, MMOBuffer buf)
		{
			buf.writeD(_value);
		}
	}
	
	private static class EquipmentElement extends IntElement
	{
		public EquipmentElement(int item)
		{
			super(item, 8);
		}
		
		@Override
		public void write(ClientProtocolVersion cpv, MMOBuffer buf)
		{
			super.write(cpv, buf);
			buf.writeD(0);
		}
	}
	
	private static class LongElement extends Element
	{
		private final long _value;
		
		public LongElement(long value)
		{
			_value = value;
		}
		
		@Override
		public int getType()
		{
			return 6;
		}
		
		@Override
		public void write(ClientProtocolVersion cpv, MMOBuffer buf)
		{
			buf.writeQ(_value);
		}
	}
	
	private static class SkillElement extends Element
	{
		private final int _skill;
		private final int _level;
		
		public SkillElement(int skill, int level)
		{
			_skill = skill;
			_level = level;
		}
		
		@Override
		public int getType()
		{
			return 4;
		}
		
		@Override
		public void write(ClientProtocolVersion cpv, MMOBuffer buf)
		{
			buf.writeD(_skill);
			buf.writeD(_level);
		}
	}
	
	private static class ZoneElement extends Element
	{
		private final int _x;
		private final int _y;
		private final int _z;
		
		public ZoneElement(int x, int y, int z)
		{
			_x = x;
			_y = y;
			_z = z;
		}
		
		@Override
		public int getType()
		{
			return 7;
		}
		
		@Override
		public void write(ClientProtocolVersion cpv, MMOBuffer buf)
		{
			buf.writeD(_x);
			buf.writeD(_y);
			buf.writeD(_z);
		}
	}
	
	private static class DamageElement extends Element
	{
		private final Integer _targetId;
		private final Integer _attackerId;
		private final int _damage;
		
		public DamageElement(Integer targetId, Integer attackerId, int damage)
		{
			_targetId = targetId;
			_attackerId = attackerId;
			_damage = damage;
		}
		
		@Override
		public int getType()
		{
			return 16;
		}
		
		@Override
		public void write(ClientProtocolVersion cpv, MMOBuffer buf)
		{
			buf.writeD(_targetId);
			buf.writeD(_attackerId);
			buf.writeD(_damage);
		}
	}
}
