/*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation, either version 3 of the License, or (at your option) any later
* version.
* 
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
* 
* You should have received a copy of the GNU General Public License along with
* this program. If not, see <http://www.gnu.org/licenses/>.
*/
package com.l2jfree.gameserver.network.serverpackets;

import org.sod.manager.ItemAuctionManager.ItemReflection;

public final class ExItemAuctionInfo extends L2GameServerPacket
{
	private static final String _S__FE_68_EXITEMAUCTIONINFO = "[S] FE:68 ExItemAuctionInfo";

	private final ItemReflection _item;
	private final boolean _initial;

	public ExItemAuctionInfo(ItemReflection item, boolean initial)
	{
		_item = item;
		_initial = initial;
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0x68);

		writeC(_initial ? 0x01 : 0x00);
		writeD(_item.getId());
		writeCompQ(_item.getBid());
		writeD(_item.getTimeLeft());
		writeD(_item.getDisplayId());
		writeCompQ(_item.getCount());

		writeH(_item.getType2()); // confirmed
		writeH(_item.getCType1()); // ??
		writeD(_item.getBodypart()); // confirmed
		writeH(_item.getEnchant()); // confirmed
		writeH(_item.getCType2()); // confirmed
		writeD(_item.getAug()); // confirmed
		writeH(0x00); // ??

		writeElementalInfo(_item); // confirmed
	}

	@Override
	public String getType()
	{
		return _S__FE_68_EXITEMAUCTIONINFO;
	}
}
