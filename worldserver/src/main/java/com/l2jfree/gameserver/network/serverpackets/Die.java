/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.network.serverpackets;

import com.l2jfree.Config;
import com.l2jfree.gameserver.model.L2Clan;
import com.l2jfree.gameserver.model.actor.L2Attackable;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.L2Playable;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.restriction.global.GlobalRestrictions;

/**
 * sample
 * 0b
 * 952a1048     objectId
 * 00000000 00000000 00000000 00000000 00000000 00000000

 * format  dddddd   rev 377
 * format  ddddddd   rev 417
 * 
 * @version $Revision: 1.3.2.1.2.5 $ $Date: 2005/03/27 18:46:18 $
 */
public class Die extends L2GameServerPacket
{
	private static final String _S__00_DIE			= "[S] 00 Die [dddddddd]";
	public static final int FEATHER_OF_BLESSING_1	= 10649;
	public static final int FEATHER_OF_BLESSING_2	= 13300;
	public static final int PHOENIX_FEATHER			= 13128;

	private final int _charObjId;
	private final boolean _fallDown;
	private final boolean _sweepable;

	private final boolean _showVillage;
	//private final int _showClanhall;
	//private final int _showCastle;
	private final boolean _showHeadquarters;
	private final boolean _showFeather;
	//private final int _showFortress;

	public Die(L2Character cha)
	{
		_charObjId = cha.getObjectId();
		_fallDown = cha.mustFallDownOnDeath();
		if (cha instanceof L2Attackable)
			_sweepable = ((L2Attackable) cha).isSweepActive();
		else if (cha instanceof L2Playable)
			_sweepable = ((L2Playable) cha).isSweepable();
		else
			_sweepable = false;
		if (cha instanceof L2PcInstance)
		{
			L2PcInstance player = cha.getActingPlayer();
			if (!GlobalRestrictions.canRequestRevive(player))
			{
				if (player.getAccessLevel() >= Config.GM_FIXED)
				{
					_showVillage = true;
					_showHeadquarters = true;
					_showFeather = true;
				}
				else
				{
					_showVillage = false;
					_showHeadquarters = false;
					_showFeather = false;
				}
				return;
			}

			_showVillage = true;

			L2Clan clan = player.getClan();
			if (clan != null && clan.getNumFlags() > 0)
				_showHeadquarters = true;
			else
				_showHeadquarters = false;

			if (player.getAccessLevel() >= Config.GM_FIXED ||
					player.getInventory().getItemByItemId(FEATHER_OF_BLESSING_1) != null ||
					player.getInventory().getItemByItemId(FEATHER_OF_BLESSING_2) != null ||
					player.getInventory().getItemByItemId(PHOENIX_FEATHER) != null)
				_showFeather = true;
			else
				_showFeather = false;
		}
		else
		{
			_showVillage = false;
			_showHeadquarters = false;
			_showFeather = false;
		}
	}

	@Override
	protected final void writeImpl()
	{
		if (!_fallDown)
			return;

		writeC(0x00);
		writeD(_charObjId);
		writeD(_showVillage);
		writeD(0x00);
		writeD(0x00);
		writeD(_showHeadquarters);
		writeD(_sweepable);
		writeD(_showFeather);
		writeD(0x00);
	}
/*
	@Override
	public void packetSent(L2GameClient client, L2PcInstance deceased)
	{
		if (_charObjId == deceased.getObjectId())
			RespawnManager.getInstance().addPlayer(deceased, _showFeather);
	}
*/
	@Override
	public String getType()
	{
		return _S__00_DIE;
	}
}
