/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.communitybbs.Manager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.l2jfree.util.logging.L2Logger;

import org.sod.i18n.Internationalization;

import com.l2jfree.Config;
import com.l2jfree.L2DatabaseFactory;
import com.l2jfree.gameserver.communitybbs.Manager.req.Null;
import com.l2jfree.gameserver.model.L2World;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.lang.L2TextBuilder;

public class TopBBSManager extends BaseBBSManager<Null>
{
	private static final L2Logger _log = L2Logger.getLogger(TopBBSManager.class);
	
	private final DateFormat _format;
	private volatile long _lastUpdate;
	private String _page;
	
	private TopBBSManager()
	{
		_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
	}
	
	@Override
	public void parsecmd(String command, L2PcInstance activeChar)
	{
		if (command.equals("_bbstop") || command.equals("_bbshome"))
		{
			if (System.currentTimeMillis() > _lastUpdate + Config.TOP_UPDATE_INTERVAL)
				buildText();
			
			Internationalization i18n = Internationalization.getInstance();
			Locale loc = activeChar.getSoDPlayer().getSettings().getLocale();
			
			String page = i18n.localize(_page, loc, "%CB_TOP_KILLERS_TITLE%", "%CB_TOP_KILLERS_DISCLAIMER%",
					"%CB_TOP_KILLERS_LAST_UPDATE%");
			page = page.replace("%num%", String.valueOf(Config.TOP_MAX_PLAYERS));
			page = page.replace("%dateTime%", _format.format(new Date(_lastUpdate)));
			separateAndSend(page, activeChar);
		}
		else
			notImplementedYet(activeChar, command);
	}
	
	@Override
	public void parsewrite(String ar1, String ar2, String ar3, String ar4, String ar5, L2PcInstance activeChar)
	{
	}
	
	private void buildText()
	{
		_lastUpdate = System.currentTimeMillis();
		
		L2TextBuilder tb = new L2TextBuilder();
		writeHeader(tb, "%CB_TOP_KILLERS_TITLE%");
		tb.append("<table width=\"600\">");
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection(con);
			PreparedStatement ps = con
					.prepareStatement("SELECT char_name,pkkills+pvpkills AS kills FROM characters WHERE accessLevel=0 ORDER BY kills DESC LIMIT ?");
			ps.setInt(1, Config.TOP_MAX_PLAYERS);
			L2World w = L2World.getInstance();
			ResultSet rs = ps.executeQuery();
			for (int i = 1; rs.next(); i++)
			{
				String name = rs.getString("char_name");
				int kills = rs.getInt("kills");
				int mod = (i % 5);
				if (mod == 1)
					tb.append("<tr>");
				tb.append("<td>");
				tb.append(i);
				tb.append(". <font color=\"");
				if (w.getPlayer(name) != null)
					tb.append("00BE00");
				else
					tb.append("BE0000");
				tb.append("\">");
				tb.append(name);
				tb.append("</font>(");
				tb.append(kills);
				tb.append(")</td>");
				if (mod == 0)
					tb.append("</tr>");
			}
			rs.close();
			ps.close();
		}
		catch (SQLException e)
		{
			_log.warn("Cannot generate BBS top list!", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
		tb.append("</table>%CB_TOP_KILLERS_DISCLAIMER%<br>%CB_TOP_KILLERS_LAST_UPDATE%</center></body></html>");
		
		_page = tb.moveToString();
	}
	
	public static TopBBSManager getInstance()
	{
		return SingletonHolder._instance;
	}
	
	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final TopBBSManager _instance = new TopBBSManager();
	}
}
