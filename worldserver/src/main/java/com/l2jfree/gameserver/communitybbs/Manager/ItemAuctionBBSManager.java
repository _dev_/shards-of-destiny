/*
 * MISSING LICENSING INFO
 */
package com.l2jfree.gameserver.communitybbs.Manager;

import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import org.sod.L2Htm;
import org.sod.i18n.Internationalization;
import org.sod.manager.ItemAuctionManager;
import org.sod.manager.ItemAuctionManager.ItemReflection;
import org.sod.model.L2PlayerData;

import com.l2jfree.Config;
import com.l2jfree.gameserver.communitybbs.Manager.req.AuctionRequest;
import com.l2jfree.gameserver.model.L2ItemInstance;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.serverpackets.InventoryUpdate;
import com.l2jfree.lang.L2TextBuilder;

/**
 * @author savormix
 * 
 */
public class ItemAuctionBBSManager extends BaseBBSManager<AuctionRequest>
{
	private ItemAuctionBBSManager()
	{
		
	}
	
	@Override
	protected String getCmdPrefix()
	{
		return "_bbsmemo";
	}
	
	@Override
	public void parsecmd(String command, L2PcInstance activeChar)
	{
		StringTokenizer st = new StringTokenizer(command, ";");
		st.nextToken(); // prefix
		
		if (!st.hasMoreTokens())
		{
			showMenu(activeChar);
			return;
		}
		
		int request;
		try
		{
			request = Integer.parseInt(st.nextToken());
		}
		catch (RuntimeException e)
		{
			return;
		}
		
		if (request < 0 || request >= AuctionRequest.values().length)
			return;
		
		AuctionRequest ar = AuctionRequest.values()[request];
		switch (ar)
		{
		case VIEW_AUCTIONS:
			showAuctions(activeChar, st);
			break;
		case AUCTION_INFO:
			showAuction(activeChar, st);
			break;
		case VIEW_SELLABLES:
			showSellableItems(activeChar);
			break;
		case SALE_SETUP:
			showSaleSetup(activeChar, st);
			break;
		case SELL:
			sellOnAuction(activeChar, st);
			break;
		case VIEW_PURCHASED:
			showClaimMenu(activeChar);
			break;
		case CLAIM_PURCHASE:
			claimItem(activeChar, st);
			break;
		default:
			notImplementedYet(activeChar, command);
			break;
		}
	}
	
	@Override
	public void parsewrite(String ar1, String ar2, String ar3, String ar4, String ar5, L2PcInstance activeChar)
	{
		// TODO Auto-generated method stub
	}
	
	public void showMenu(L2PcInstance activeChar)
	{
		Internationalization i18n = Internationalization.getInstance();
		L2PlayerData dat = activeChar.getSoDPlayer();
		Locale loc = dat.getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		writeHeader(tb, i18n.get(loc, "CB_AUCTION_TITLE"));
		if (!activeChar.isInJail())
		{
			writeButton(tb, AuctionRequest.VIEW_AUCTIONS, 250, 35, i18n.get(loc, "CB_AUCTION_TITLE"));
			tb.append("<br1>");
			writeButton(tb, AuctionRequest.VIEW_SELLABLES, 250, 35, i18n.get(loc, "CB_AUCTION_SELLABLE_ITEMS"));
			tb.append("<br1>");
			writeButton(tb, AuctionRequest.VIEW_PURCHASED, 250, 35, i18n.get(loc, "CB_AUCTION_PURCHASED_ITEMS"));
		}
		else
			tb.append(i18n.get(loc, "THIS_FEATURE_HAS_BEEN_DISABLED"));
		writeFooter(tb, null);
		
		separateAndSend(tb, activeChar);
	}
	
	private void showAuctions(L2PcInstance player, StringTokenizer st)
	{
		Internationalization i18n = Internationalization.getInstance();
		L2PlayerData dat = player.getSoDPlayer();
		Locale loc = dat.getSettings().getLocale();
		
		int page = 1;
		try
		{
			page = Integer.parseInt(st.nextToken());
		}
		catch (RuntimeException e)
		{
		}
		if (page < 1)
			return;
		
		L2TextBuilder tb = new L2TextBuilder();
		writeHeader(tb, i18n.get(loc, "CB_AUCTION_LIST_TITLE", "%page%", String.valueOf(page)));
		tb.append("<table width=\"720\"><tr><td><font color=\"af9878\">");
		tb.append(i18n.get(loc, "CB_AUCTION_ITEM_ID"));
		tb.append("</font></td><td><font color=\"af9878\">");
		tb.append(i18n.get(loc, "CB_AUCTION_ITEM_NAME"));
		tb.append("</font></td><td><font color=\"af9878\">");
		tb.append(i18n.get(loc, "CB_AUCTION_ITEM_COUNT"));
		tb.append("</font></td><td><font color=\"af9878\">");
		tb.append(i18n.get(loc, "CB_AUCTION_ITEM_CURRENT_BID"));
		tb.append("</font></td><td></td></tr>");
		
		List<ItemReflection> all = ItemAuctionManager.getInstance().getHolder().getAuctions();
		boolean more = true;
		final int end = Config.AUCTION_ITEMS_PER_PAGE * page;
		for (int i = Config.AUCTION_ITEMS_PER_PAGE * (page - 1); i < end; i++)
		{
			if (i >= all.size())
			{
				more = false;
				break;
			}
			
			ItemReflection ir = all.get(i);
			tb.append("<tr><td>");
			tb.append(i + 1);
			tb.append("</td><td>");
			tb.append(ir.getName());
			tb.append("</td><td>");
			tb.append(ir.getCount());
			tb.append("</td><td>");
			tb.append(ir.getBid());
			tb.append(" adena</td><td>");
			tb.append("</td><td>");
			writeButton(tb, AuctionRequest.AUCTION_INFO, ir.getId(), 85, 17, i18n.get(loc, "CB_AUCTION_ITEM_VIEW"));
			tb.append("</td></tr>");
		}
		tb.append("<br1><table width=\"240\"><tr><td width=\"120\">");
		if (page > 1)
			writeButton(tb, AuctionRequest.VIEW_AUCTIONS, page - 1, 75, 17, i18n.get(loc, "PREVIOUS"));
		tb.append("</td><td width=\"120\">");
		if (more)
			writeButton(tb, AuctionRequest.VIEW_AUCTIONS, page + 1, 75, 17, i18n.get(loc, "NEXT"));
		tb.append("</td></tr></table>");
		writeFooter(tb, i18n.get(loc, "BACK"));
		
		separateAndSend(tb, player);
	}
	
	private void showAuction(L2PcInstance player, StringTokenizer st)
	{
		int id;
		try
		{
			id = Integer.parseInt(st.nextToken());
		}
		catch (RuntimeException e)
		{
			return;
		}
		
		ItemAuctionManager.getInstance().openBid(id, player);
	}
	
	private void showSellableItems(L2PcInstance player)
	{
		Internationalization i18n = Internationalization.getInstance();
		L2PlayerData dat = player.getSoDPlayer();
		Locale loc = dat.getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		writeHeader(tb, i18n.get(loc, "CB_AUCTION_SELLABLE_ITEMS"));
		L2TextBuilder tmp = new L2TextBuilder();
		L2ItemInstance[] items = player.getInventory().getItems();
		for (L2ItemInstance item : items)
		{
			if (!item.isAvailable(player, false, false))
				continue;
			
			if (item.isEquipable() && item.getEnchantLevel() > 0)
			{
				tmp.append('+');
				tmp.append(item.getEnchantLevel());
				tmp.append(' ');
			}
			
			writeButton(tb, AuctionRequest.SALE_SETUP, item.getObjectId(), 450, 20, true, false, item.getItemName(),
					tmp.toString());
			tmp.setLength(0);
		}
		tmp.setLength(0);
		tmp.moveToString();
		writeFooter(tb, i18n.get(loc, "BACK"));
		
		separateAndSend(tb, player);
	}
	
	private void showSaleSetup(L2PcInstance player, StringTokenizer st)
	{
		int objectId;
		try
		{
			objectId = Integer.parseInt(st.nextToken());
		}
		catch (RuntimeException e)
		{
			return;
		}
		
		{
			L2ItemInstance item = player.getInventory().getItemByObjectId(objectId);
			if (item == null || !item.isAvailable(player, false, false))
				return;
		}
		
		Internationalization i18n = Internationalization.getInstance();
		L2PlayerData dat = player.getSoDPlayer();
		Locale loc = dat.getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		writeHeader(tb, i18n.get(loc, "CB_AUCTION_SELLABLE_ITEMS"));
		tb.append("<table width=\"240\"><tr><td><font color=\"af9878\">");
		tb.append(i18n.get(loc, "CB_AUCTION_SELL_STARTER_BID"));
		tb.append("</font></td><td><combobox width=\"70\" var=\"bid\" list=\"10;50;100;250;500;1000;2500;5000\"></td></tr><tr><td><font color=\"af9878\">");
		tb.append(i18n.get(loc, "CB_AUCTION_SELL_DURATION_H"));
		tb.append("</font></td><td><combobox width=\"70\" var=\"len\" list=\"1;2;5;10;24;48\"></td></tr><tr><td><font color=\"af9878\">");
		tb.append(i18n.get(loc, "CB_AUCTION_SELL_QUANTITY"));
		tb.append("</font></td><td>");
		L2Htm.writeTextField(tb, "cnt", 80, 15, 9);
		tb.append("</td></tr></table><br><br>");
		tb.append(i18n.get(loc, "CB_AUCTION_TAX_DISCLAIMER", "%tax%", "<font color=\"LEVEL\">" + Config.AUCTION_TAX
				+ "</font>"));
		tb.append("<br>");
		writeButton(tb, AuctionRequest.SELL, objectId + " $bid $len $cnt", 100, 20, i18n.get(loc, "CB_AUCTION_SELL"));
		writeFooter(tb, AuctionRequest.VIEW_SELLABLES, i18n.get(loc, "BACK"));
		
		separateAndSend(tb, player);
	}
	
	private void sellOnAuction(L2PcInstance player, StringTokenizer st)
	{
		if (Config.GM_DISABLE_TRANSACTION && player.getAccessLevel() >= Config.GM_TRANSACTION_MIN
				&& player.getAccessLevel() <= Config.GM_TRANSACTION_MAX)
			return;
		
		int objectId, hours;
		long bid, quantity;
		try
		{
			objectId = Integer.parseInt(st.nextToken(" ").substring(1));
			bid = Long.parseLong(st.nextToken());
			hours = Integer.parseInt(st.nextToken());
			quantity = Long.parseLong(st.nextToken());
			
			if (bid < 10 || hours > 48 || quantity < 1)
				return;
		}
		catch (RuntimeException e)
		{
			return;
		}
		
		L2ItemInstance oldItem = player.checkItemManipulation(objectId, quantity, "trade");
		if (oldItem == null || !oldItem.isAvailable(player, false, false))
			return;
		if (player.getActiveTradeList() != null || player.getActiveEnchantItem() != null)
			return;
		if (!player.reduceAdena("Auction_Start", Math.max(1, bid * Config.AUCTION_TAX / 100), null, true))
			return;
		
		final L2ItemInstance newItem = player.getInventory().transferItem("Auction start", objectId, quantity,
				player.getSoDPlayer().getAuction(), player, null);
		if (newItem == null)
			return;
		
		InventoryUpdate iu = new InventoryUpdate();
		if (oldItem.getCount() > 0 && oldItem != newItem)
			iu.addModifiedItem(oldItem);
		else
			iu.addRemovedItem(oldItem);
		player.sendPacket(iu);
		player.refreshOverloaded();
		
		ItemAuctionManager.getInstance().addToAuction(newItem, quantity, hours * 60, bid);
		
		showSellableItems(player);
	}
	
	private void showClaimMenu(L2PcInstance player)
	{
		Internationalization i18n = Internationalization.getInstance();
		L2PlayerData dat = player.getSoDPlayer();
		Locale loc = dat.getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		writeHeader(tb, i18n.get(loc, "CB_AUCTION_PURCHASED_ITEMS"));
		tb.append("<table width=\"720\"><tr><td><font color=\"af9878\">");
		tb.append(i18n.get(loc, "CB_AUCTION_ITEM_ID"));
		tb.append("</font></td><td><font color=\"af9878\">");
		tb.append(i18n.get(loc, "CB_AUCTION_ITEM_NAME"));
		tb.append("</font></td><td><font color=\"af9878\">");
		tb.append(i18n.get(loc, "CB_AUCTION_ITEM_COUNT"));
		tb.append("</font></td><td></td></tr>");
		
		ItemAuctionManager iam = ItemAuctionManager.getInstance();
		int id = 1;
		for (L2ItemInstance item : dat.getAuction().getItems())
		{
			long claimable = iam.getClaimableCount(item);
			if (claimable < 1)
				continue;
			
			tb.append("<tr><td>");
			tb.append(id++);
			tb.append("</td><td>");
			tb.append(item.getItemName());
			tb.append("</td><td>");
			tb.append(claimable);
			tb.append("</td><td>");
			writeButton(tb, AuctionRequest.CLAIM_PURCHASE, item.getObjectId(), 85, 17,
					i18n.get(loc, "CB_AUCTION_PURCHASED_ITEM_CLAIM"));
			tb.append("</td></tr>");
		}
		tb.append("</table>");
		if (id == 1)
			tb.append(i18n.get(loc, "CB_AUCTION_NO_PURCHASES"));
		writeFooter(tb, i18n.get(loc, "BACK"));
		
		separateAndSend(tb, player);
	}
	
	private void claimItem(L2PcInstance player, StringTokenizer st)
	{
		int objectId;
		try
		{
			objectId = Integer.parseInt(st.nextToken());
		}
		catch (RuntimeException e)
		{
			return;
		}
		
		L2PlayerData dat = player.getSoDPlayer();
		L2ItemInstance item = dat.getAuction().getItemByObjectId(objectId);
		long count = ItemAuctionManager.getInstance().getClaimableCount(item);
		if (count < 1)
			return;
		
		InventoryUpdate iu = new InventoryUpdate();
		L2ItemInstance newItem = dat.getAuction().transferItem("Auction Claim", objectId, count, player.getInventory(),
				player, null);
		if (item != newItem)
			iu.addModifiedItem(newItem);
		else
			iu.addNewItem(newItem);
		player.sendPacket(iu);
		player.refreshOverloaded();
		
		showClaimMenu(player);
	}
	
	public static ItemAuctionBBSManager getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static final class SingletonHolder
	{
		private static final ItemAuctionBBSManager INSTANCE = new ItemAuctionBBSManager();
	}
}
