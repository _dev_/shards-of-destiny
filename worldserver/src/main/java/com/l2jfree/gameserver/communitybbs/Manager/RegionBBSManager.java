/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.communitybbs.Manager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import javolution.util.FastMap;

import org.sod.i18n.Internationalization;

import com.l2jfree.Config;
import com.l2jfree.gameserver.communitybbs.Manager.req.Null;
import com.l2jfree.gameserver.model.BlockList;
import com.l2jfree.gameserver.model.L2World;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.base.Experience;
import com.l2jfree.gameserver.network.SystemChatChannelId;
import com.l2jfree.gameserver.network.SystemMessageId;
import com.l2jfree.gameserver.network.serverpackets.CreatureSay;
import com.l2jfree.gameserver.network.client.packets.sendable.SystemMessagePacket.SystemMessage;
import com.l2jfree.lang.L2TextBuilder;

/**
 * Never done, needs revision.
 * 
 * @author Horus
 * @author savormix
 * @since 2010.10.19
 */
public class RegionBBSManager extends BaseBBSManager<Null>
{
	// private static final Logger _logChat = ((Jdk14Logger) L2Logger.getLogger("chat")).getLogger();
	
	@Override
	public void parsecmd(String command, L2PcInstance activeChar)
	{
		if (command.equals("_bbsloc"))
			showOldCommunity(activeChar, 1);
		else if (command.startsWith("_bbsloc;page;"))
		{
			StringTokenizer st = new StringTokenizer(command, ";");
			st.nextToken();
			st.nextToken();
			int page = 0;
			try
			{
				page = Integer.parseInt(st.nextToken());
			}
			catch (NumberFormatException nfe)
			{
			}
			
			showOldCommunity(activeChar, page);
		}
		else if (command.startsWith("_bbsloc;playerinfo;"))
		{
			StringTokenizer st = new StringTokenizer(command, ";");
			st.nextToken();
			st.nextToken();
			String name = st.nextToken();
			
			showOldCommunityPI(activeChar, name);
		}
		else if (Config.COMMUNITY_TYPE == 1)
			showOldCommunity(activeChar, 1);
		else
			notImplementedYet(activeChar, command);
	}
	
	/**
	 * @param activeChar
	 * @param name
	 */
	private void showOldCommunityPI(L2PcInstance activeChar, String name)
	{
		final L2TextBuilder htmlCode = new L2TextBuilder();
		htmlCode.append("<html><body><br>");
		htmlCode.append("<table border=0><tr><td FIXWIDTH=15></td><td align=center>L2J Community Board<img src=\"sek.cbui355\" width=610 height=1></td></tr><tr><td FIXWIDTH=15></td><td>");
		L2PcInstance player = L2World.getInstance().getPlayer(name);
		
		if (player != null)
		{
			String sex = "Male";
			if (player.getAppearance().getSex())
				sex = "Female";
			String levelApprox = "low";
			if (player.getLevel() >= 60)
				levelApprox = "very high";
			else if (player.getLevel() >= 40)
				levelApprox = "high";
			else if (player.getLevel() >= 20)
				levelApprox = "medium";
			htmlCode.append("<table border=0><tr><td>").append(player.getName()).append(" (").append(sex).append(" ")
					.append(player.getTemplate().getClassName()).append("):</td></tr>");
			htmlCode.append("<tr><td>Level: ").append(levelApprox).append("</td></tr>");
			htmlCode.append("<tr><td><br></td></tr>");
			
			if (activeChar != null
					&& (activeChar.isGM() || player.getObjectId() == activeChar.getObjectId() || Config.SHOW_LEVEL_COMMUNITYBOARD))
			{
				long nextLevelExp = 0;
				long nextLevelExpNeeded = 0;
				if (player.getLevel() < (Experience.MAX_LEVEL - 1))
				{
					nextLevelExp = Experience.LEVEL[player.getLevel() + 1];
					nextLevelExpNeeded = nextLevelExp - player.getExp();
				}
				
				htmlCode.append("<tr><td>Level: ").append(player.getLevel()).append("</td></tr>");
				htmlCode.append("<tr><td>Experience: ").append(player.getExp()).append("/").append(nextLevelExp)
						.append("</td></tr>");
				htmlCode.append("<tr><td>Experience needed for level up: ").append(nextLevelExpNeeded)
						.append("</td></tr>");
				htmlCode.append("<tr><td><br></td></tr>");
			}
			
			int uptime = (int) player.getUptime() / 1000;
			int h = uptime / 3600;
			int m = (uptime - (h * 3600)) / 60;
			int s = ((uptime - (h * 3600)) - (m * 60));
			
			htmlCode.append("<tr><td>Uptime: ").append(h).append("h ").append(m).append("m ").append(s)
					.append("s</td></tr>");
			htmlCode.append("<tr><td><br></td></tr>");
			
			if (player.getClan() != null)
			{
				htmlCode.append("<tr><td>Clan: ").append(player.getClan().getName()).append("</td></tr>");
				htmlCode.append("<tr><td><br></td></tr>");
			}
			
			htmlCode.append(
					"<tr><td><multiedit var=\"pm\" width=240 height=40><button value=\"Send PM\" action=\"Write Region PM ")
					.append(player.getName())
					.append(" pm pm pm\" width=110 height=15 back=\"sek.cbui94\" fore=\"sek.cbui92\"></td></tr><tr><td><br><button value=\"Back\" action=\"bypass _bbsloc\" width=40 height=15 back=\"sek.cbui94\" fore=\"sek.cbui92\"></td></tr></table>");
			htmlCode.append("</td></tr></table>");
			htmlCode.append("</body></html>");
			separateAndSend(htmlCode, activeChar);
		}
		else
			separateAndSend("<html><body><br><br><center>No player with name " + name
					+ "</center><br><br></body></html>", activeChar);
	}
	
	/**
	 * @param activeChar
	 */
	private void showOldCommunity(L2PcInstance activeChar, int page)
	{
		separateAndSend(
				CommunityPageType
						.getType(activeChar)
						.getPage(page)
						.replace(
								"%THIS_FEATURE_HAS_BEEN_DISABLED%",
								Internationalization.getInstance().get(
										activeChar.getSoDPlayer().getSettings().getLocale(),
										"THIS_FEATURE_HAS_BEEN_DISABLED")), activeChar);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.l2jfree.gameserver.communitybbs.Manager.BaseBBSManager#parsewrite
	 * (java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String,
	 * com.l2jfree.gameserver.model.actor.instance.L2PcInstance)
	 */
	@Override
	public void parsewrite(String ar1, String ar2, String ar3, String ar4, String ar5, L2PcInstance activeChar)
	{
		if (activeChar == null)
			return;
		
		if (ar1.equals("PM"))
		{
			final L2TextBuilder htmlCode = new L2TextBuilder();
			htmlCode.append("<html><body><br>");
			htmlCode.append("<table border=0><tr><td FIXWIDTH=15></td><td align=center>L2J Community Board<img src=\"sek.cbui355\" width=610 height=1></td></tr><tr><td FIXWIDTH=15></td><td>");
			
			try
			{
				
				L2PcInstance receiver = L2World.getInstance().getPlayer(ar2);
				if (receiver == null)
				{
					htmlCode.append("Player not found!<br><button value=\"Back\" action=\"bypass _bbsloc;playerinfo;")
							.append(ar2).append("\" width=40 height=15 back=\"sek.cbui94\" fore=\"sek.cbui92\">");
					htmlCode.append("</td></tr></table></body></html>");
					separateAndSend(htmlCode, activeChar);
					return;
				}
				if (Config.JAIL_DISABLE_CHAT && receiver.isInJail())
				{
					activeChar.sendMessage("Player is in jail.");
					return;
				}
				if (receiver.isChatBanned())
				{
					activeChar.sendMessage("Player is chat banned.");
					return;
				}
				if (activeChar.isInJail() && Config.JAIL_DISABLE_CHAT)
				{
					activeChar.sendMessage("You cannot chat while in jail.");
					return;
				}
				
				if (Config.LOG_CHAT)
				{
					LogRecord record = new LogRecord(Level.INFO, ar3);
					record.setLoggerName("chat");
					record.setParameters(new Object[]
					{
							"TELL", "[" + activeChar.getName() + " to " + receiver.getName() + "]"
					});
					// _logChat.log(record);
				}
				CreatureSay cs = new CreatureSay(activeChar.getObjectId(), SystemChatChannelId.Chat_Tell,
						activeChar.getName(), ar3);
				if (!BlockList.isBlocked(receiver, activeChar))
				{
					if (!receiver.getMessageRefusal())
					{
						receiver.sendPacket(cs);
						activeChar.sendPacket(new CreatureSay(activeChar.getObjectId(), SystemChatChannelId.Chat_Tell,
								"->" + receiver.getName(), ar3));
						htmlCode.append("Message Sent<br><button value=\"Back\" action=\"bypass _bbsloc;playerinfo;")
								.append(receiver.getName())
								.append("\" width=40 height=15 back=\"sek.cbui94\" fore=\"sek.cbui92\">");
						htmlCode.append("</td></tr></table></body></html>");
						separateAndSend(htmlCode, activeChar);
					}
					else
					{
						activeChar.sendPacket(SystemMessageId.THE_PERSON_IS_IN_MESSAGE_REFUSAL_MODE);
						parsecmd("_bbsloc;playerinfo;" + receiver.getName(), activeChar);
					}
				}
				else
				{
					SystemMessage sm = new SystemMessage(SystemMessageId.S1_IS_NOT_ONLINE);
					sm.addString(receiver.getName());
					activeChar.sendPacket(sm);
					sm = null;
				}
			}
			catch (StringIndexOutOfBoundsException e)
			{
				// ignore
			}
		}
		else
			notImplementedYet(activeChar, ar1);
	}
	
	/**
	 * @return
	 */
	public static RegionBBSManager getInstance()
	{
		return SingletonHolder._instance;
	}
	
	private RegionBBSManager()
	{
	}
	
	public static void changeCommunityBoard(L2PcInstance player, PlayerStateOnCommunity maxInfluencedState)
	{
		if (!maxInfluencedState.showState())
			return;
		
		CommunityPageType.PLAYER.changeCommunityBoard(player, maxInfluencedState);
		CommunityPageType.GM.changeCommunityBoard(player, maxInfluencedState);
	}
	
	/*
	private static final SimpleDateFormat	format		= new SimpleDateFormat("H:mm");
	private static final String				tdClose		= "</td>";
	private static final String				tdOpen		= "<td align=left valign=top>";
	private static final String				trClose		= "</tr>";
	private static final String				trOpen		= "<tr>";
	private static final String				colSpacer	= "<td FIXWIDTH=15></td>";
	 */
	
	public static enum PlayerStateOnCommunity
	{
		NONE
		{
			@Override
			protected boolean showState()
			{
				return true;
			}
			
			@Override
			protected boolean isInState(L2PcInstance player, CommunityPageType type)
			{
				if (player == null)
					return true;
				
				if (player.isGM() && player.getAppearance().isInvisible())
					if (CommunityPageType.PLAYER == type)
						return true;
				
				return false;
			}
		},
		GM
		{
			@Override
			protected boolean showState()
			{
				return true;
			}
			
			@Override
			protected boolean isInState(L2PcInstance player, CommunityPageType type)
			{
				return player.isGM();
			}
		},
		IN_JAIL
		{
			@Override
			protected boolean showState()
			{
				return Config.SHOW_JAILED_PLAYERS;
			}
			
			@Override
			protected boolean isInState(L2PcInstance player, CommunityPageType type)
			{
				return player.isInJail();
			}
		},
		CURSED_WEAPON_OWNER
		{
			@Override
			protected boolean showState()
			{
				return Config.SHOW_CURSED_WEAPON_OWNER;
			}
			
			@Override
			protected boolean isInState(L2PcInstance player, CommunityPageType type)
			{
				return player.isCursedWeaponEquipped();
			}
		},
		KARMA_OWNER
		{
			@Override
			protected boolean showState()
			{
				return Config.SHOW_KARMA_PLAYERS;
			}
			
			@Override
			protected boolean isInState(L2PcInstance player, CommunityPageType type)
			{
				return player.getKarma() > 0;
			}
		},
		LEADER
		{
			@Override
			protected boolean showState()
			{
				return Config.SHOW_CLAN_LEADER;
			}
			
			@Override
			protected boolean isInState(L2PcInstance player, CommunityPageType type)
			{
				return player.isClanLeader() && player.getClan().getLevel() >= Config.SHOW_CLAN_LEADER_CLAN_LEVEL;
			}
		},
		OFFLINE
		{
			@Override
			protected boolean showState()
			{
				return true;
			}
			
			@Override
			protected boolean isInState(L2PcInstance player, CommunityPageType type)
			{
				return player.isInOfflineMode();
			}
		},
		NORMAL
		{
			@Override
			protected boolean showState()
			{
				return true;
			}
			
			@Override
			protected boolean isInState(L2PcInstance player, CommunityPageType type)
			{
				return true;
			}
		};
		
		/**
		 * @return is the state currently active or not
		 */
		protected abstract boolean showState();
		
		/**
		 * @param player
		 * @param type
		 * @return is the player in the state or not
		 */
		protected abstract boolean isInState(L2PcInstance player, CommunityPageType type);
		
		public static PlayerStateOnCommunity getPlayerState(L2PcInstance player, CommunityPageType type)
		{
			for (PlayerStateOnCommunity state : VALUES)
				if (state.showState() && state.isInState(player, type))
					return state;
			
			throw new InternalError("Shouldn't happen!");
		}
		
		private static final PlayerStateOnCommunity[] VALUES = PlayerStateOnCommunity.values();
	}
	
	private static enum CommunityPageType
	{
		PLAYER, GM;
		
		private static CommunityPageType getType(L2PcInstance activeChar)
		{
			return activeChar.isGM() ? GM : PLAYER;
		}
		
		private final List<L2PcInstance> _players = new ArrayList<L2PcInstance>();
		private final Map<Integer, String> _communityPages = new FastMap<Integer, String>();
		
		public void changeCommunityBoard(L2PcInstance player, PlayerStateOnCommunity maxInfluencedState)
		{
			if (getPlayerState(player).ordinal() >= maxInfluencedState.ordinal())
				clear();
		}
		
		private PlayerStateOnCommunity getPlayerState(L2PcInstance player)
		{
			return PlayerStateOnCommunity.getPlayerState(player, this);
		}
		
		private synchronized void clear()
		{
			_players.clear();
			_communityPages.clear();
		}
		
		private synchronized String getPage(int page)
		{
			if (_players.isEmpty())
			{
				clear();
				
				for (L2PcInstance player : L2World.getInstance().getAllPlayers())
				{
					if (player == null)
						continue;
					
					if (player.isGM() && player.getAppearance().isInvisible())
						if (CommunityPageType.PLAYER == this)
							continue;
					
					_players.add(player);
				}
				
				Collections.sort(_players, new Comparator<L2PcInstance>()
				{
					@Override
					public int compare(L2PcInstance p1, L2PcInstance p2)
					{
						final int value = getPlayerState(p1).compareTo(getPlayerState(p2));
						
						if (value != 0)
							return value;
						
						return p1.getName().compareToIgnoreCase(p2.getName());
					}
				});
			}
			
			final String communityPage = _communityPages.get(page);
			
			if (communityPage != null)
				return communityPage;
			
			final String generatedPage = generateHtml(page);
			
			_communityPages.put(page, generatedPage);
			
			return generatedPage;
		}
		
		private String generateHtml(int page)
		{
			return "<html><body><br><center>%THIS_FEATURE_HAS_BEEN_DISABLED%</center></body></html>";/*
																										L2TextBuilder htmlCode = new L2TextBuilder();
																										_lastUpdated = Calendar.getInstance();
																										SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

																										int emages = 0, earchers = 0, edaggers = 0, ehealers = 0, emelee = 0, etanks = 0, esummoners = 0, eothers = 0;
																										int wmages = 0, warchers = 0, wdaggers = 0, whealers = 0, wmelee = 0, wtanks = 0, wsummoners = 0, wothers = 0;
																										int epoints = 0, wpoints = 0;
																										int elevels = 0, wlevels = 0, elevelavg = 0, wlevelavg = 0;
																										FastSet<L2PcInstance> example = ConquestFactionManager.getInstance().getFactionTemplate(ConquestFactionManager.ANCIENTS).getActiveMembers();
																										for (FastSet.Record r = example.head(), end = example.tail(); (r = r.getNext()) != end;)
																										{
																										L2PcInstance p = example.valueOf(r);
																										epoints = epoints + p.getSoDPlayer().getTempFaction();
																										elevels = elevels + p.getLevel();
																										int pclass = p.getActiveClass();
																										if (pclass == 9 || pclass == 24 || pclass == 37 || pclass == 92 || pclass == 109 || pclass == 102)
																										earchers++;
																										else if (pclass == 58 || pclass == 36 || pclass == 23 || pclass == 93 || pclass == 108 || pclass == 101)
																										edaggers++;
																										else if (pclass == 112 || pclass == 16 || pclass == 30 || pclass == 97 || pclass == 105 || pclass == 112)
																										ehealers++;
																										else if (pclass == 5 || pclass == 99 || pclass == 33 || pclass == 106 || pclass == 90 || pclass == 20)
																										etanks++;
																										else if (p.getClassId().isSummoner())
																										esummoners++;
																										else if (p.isMageClass())
																										emages++;
																										else if (p.getRace() == Race.Kamael)
																										emelee++;
																										else
																										eothers++;
																										}
																										example = ConquestFactionManager.getInstance().getFactionPlayers(ConquestFactionManager.WEST);
																										for (FastSet.Record r = example.head(), end = example.tail(); (r = r.getNext()) != end;)
																										{
																										L2PcInstance p = example.valueOf(r);
																										L2PlayerData dat = p.getSoDPlayer();
																										if (dat == null)
																										continue;
																										wpoints = wpoints + dat.getTempFaction();
																										wlevels = wlevels + p.getLevel();
																										int pclass = p.getActiveClass();
																										if (pclass == 9 || pclass == 24 || pclass == 37 || pclass == 92 || pclass == 109 || pclass == 102)
																										warchers++;
																										else if (pclass == 58 || pclass == 36 || pclass == 23 || pclass == 93 || pclass == 108 || pclass == 101)
																										wdaggers++;
																										else if (pclass == 112 || pclass == 16 || pclass == 30 || pclass == 97 || pclass == 105 || pclass == 112)
																										whealers++;
																										else if (pclass == 5 || pclass == 99 || pclass == 33 || pclass == 106 || pclass == 90 || pclass == 20)
																										wtanks++;
																										else if (p.getClassId().isSummoner())
																										wsummoners++;
																										else if (p.getRace() == Race.Kamael)
																										wmelee++;
																										else if (p.getRace() == Race.Dwarf)
																										wothers++;
																										else if (p.isMageClass())
																										wmages++;
																										else
																										wothers++;
																										}

																										if (elevels == 0)
																										elevels = 1;
																										if (wlevels == 0)
																										wlevels = 1;
																										int eplayers = ConquestFactionManager.getInstance().getOnlineCount(ConquestFactionManager.EAST);
																										int wplayers = ConquestFactionManager.getInstance().getOnlineCount(ConquestFactionManager.WEST);
																										if (eplayers != 0)
																										elevelavg = elevels / eplayers;
																										else // TODO!
																										elevelavg = elevels / 1;
																										if (wplayers != 0)
																										wlevelavg = wlevels / wplayers;
																										else // TODO!
																										wlevelavg = wlevels / 1;

																										htmlCode.append("<html><br><center>Welcome to <font color=\"808000\">East</font> vs <font color=\"E9AB17\">West</font><br1>");
																										htmlCode.append("<img src=\"L2UI_CH3.herotower_deco\" width=256 height=32></center>");

																										htmlCode.append("<br1><table width=800 height=30><tr><td><font color=\"LEVEL\">Game Information</font></td></tr>");
																										/*if(ZoneHandler.getInstance()._status == 3)
																										{
																										htmlCode.append("<tr><td>Game Type:</td>");  <- Relate it with the Event Manager
																										htmlCode.append("<td>Event Name ?</td></tr>");
																										}
																										else
																										{ *//*
																											htmlCode.append("<tr><td>Game Type:</td>");
																											htmlCode.append("<td>Zone Battles</td></tr>");
																											//}
																											htmlCode.append("<tr><td>Game Map: </td>");
																											htmlCode.append("<td>L2World</td></tr>");
																											htmlCode.append("<tr><td>Game Time Remaining: Days</td>");
																											htmlCode.append("<td>Days</td></tr>");
																											htmlCode.append("<tr><td>Next Swarm Spawn:</td>");
																											htmlCode.append("<td>In ");
																											htmlCode.append(RespawnManager.getInstance().getNextSpawnAfter());
																											htmlCode.append(" Seconds.</td></tr>");

																											htmlCode.append("<tr></tr>");

																											double diff = eplayers * 100;
																											diff /= wplayers;
																											String east, west;
																											if (diff > 110)
																											{
																											east = "High";
																											west = "Low";
																											}
																											else if (diff < 90)
																											{
																											east = "Low";
																											west = "High";
																											}
																											else
																											{
																											east = "Even";
																											west = "Even";
																											}

																											htmlCode.append("<tr><td><font color=\"LEVEL\">Team Information:</font></td></tr></ br1>");
																											htmlCode.append("<tr><td>East Score: </td>");
																											htmlCode.append("<td>" + epoints + "</td>");
																											htmlCode.append("<td>West Score: </td>");
																											htmlCode.append("<td>" + wpoints + "</td></tr>");
																											htmlCode.append("<tr><td>East Players: </td>");
																											htmlCode.append("<td>" + east + "</td>");
																											htmlCode.append("<td>West Players: </td>");
																											htmlCode.append("<td>" + west + "</td></tr>");
																											htmlCode.append("<tr><td>East Average Level: </td>");
																											htmlCode.append("<td>" + elevelavg + "</td>");
																											htmlCode.append("<td>West Average Level: </td>");
																											htmlCode.append("<td>" + wlevelavg + "</td></tr>");
																											htmlCode.append("<tr><td>Owned Zones: </td>");

																											int eastZones = 0, westZones = 0;
																											FastMap<Integer, L2FactionZone> map = ConquestZoneManager.getInstance().getZoneMap();
																											for (FastMap.Entry<Integer, L2FactionZone> entry = map.head(), end = map.tail();
																											(entry = entry.getNext()) != end;)
																											{
																											L2FactionZone zone = entry.getValue();
																											switch (zone.getOwner())
																											{
																											case ConquestFactionManager.EAST: eastZones++; break;
																											case ConquestFactionManager.WEST: westZones++; break;
																											}
																											}

																											htmlCode.append("<td>" + eastZones + "</td>");
																											htmlCode.append("<td>Owned Zones: </td>");
																											htmlCode.append("<td>" + westZones + "</td></tr>");

																											htmlCode.append("<tr></tr>");

																											htmlCode.append("<br><tr><td><font color=\"LEVEL\">Class Info:</font></td>");
																											/*
																											htmlCode.append("<td><font color=\"808000\">East Classes</font></td>");
																											htmlCode.append("<td></td>");
																											htmlCode.append("<td><font color=\"E9AB17\">West Classes</font></td></tr>");
																											htmlCode.append("<tr><td>Mages:</td>");
																											htmlCode.append("<td><font color = \"808000\">" + emages + "</font></td>");
																											htmlCode.append("<td>Mages:</td>");
																											htmlCode.append("<td><font color = \"E9AB17\">" + wmages + "</font></td></tr>");
																											htmlCode.append("<tr><td>Archers:</td>");
																											htmlCode.append("<td><font color = \"808000\">" + earchers + "</font></td>");
																											htmlCode.append("<td>Archers:</td>");
																											htmlCode.append("<td><font color = \"E9AB17\">" + warchers + "</font></td></tr>");
																											htmlCode.append("<tr><td>Daggers:</td>");
																											htmlCode.append("<td><font color = \"808000\">" + edaggers + "</font></td>");
																											htmlCode.append("<td>Daggers:</td>");
																											htmlCode.append("<td><font color = \"E9AB17\">" + wdaggers + "</font></td></tr>");
																											htmlCode.append("<tr><td>Healers:</td>");
																											htmlCode.append("<td><font color = \"808000\">" + ehealers + "</font></td>");
																											htmlCode.append("<td>Healers:</td>");
																											htmlCode.append("<td><font color = \"E9AB17\">" + whealers + "</font></td></tr>");
																											htmlCode.append("<tr><td>Melee:</td>");
																											htmlCode.append("<td><font color = \"808000\">" + emelee + "</font></td>");
																											htmlCode.append("<td>Melee:</td>");
																											htmlCode.append("<td><font color = \"E9AB17\">" + wmelee + "</font></td></tr>");
																											htmlCode.append("<tr><td>Tanks:</td>");
																											htmlCode.append("<td><font color = \"808000\">" + etanks + "</font></td>");
																											htmlCode.append("<td>Tanks:</td>");
																											htmlCode.append("<td><font color = \"E9AB17\">" + wtanks + "</font></td></tr>");
																											htmlCode.append("<tr><td>Summoners:</td>");
																											htmlCode.append("<td><font color = \"808000\">" + esummoners + "</font></td>");
																											htmlCode.append("<td>Summoners:</td>");
																											htmlCode.append("<td><font color = \"E9AB17\">" + wsummoners + "</font></td></tr>");
																											htmlCode.append("<tr><td>Etc:</td>");
																											htmlCode.append("<td><font color = \"808000\">" + eothers + "</font></td>");
																											htmlCode.append("<td>Etc:</td>");
																											htmlCode.append("<td><font color = \"E9AB17\">" + wothers + "</font></td></tr>");
																											*//*
																												htmlCode.append("</tr>");

																												htmlCode.append("<tr></tr>");

																												htmlCode.append("</table></ br><center>Last Updated: " + sdf.format(_lastUpdated.getTime()) + "</center>");
																												htmlCode.append("<center><img src=\"L2UI_CH3.herotower_deco\" width=256 height=32></center></html>");

																												htmlCode.append("</body></html>");
																												return htmlCode.moveToString();*/
		}
	}
	
	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final RegionBBSManager _instance = new RegionBBSManager();
	}
}
