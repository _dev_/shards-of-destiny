/*
 * MISSING LICENSING INFO
 */
package com.l2jfree.gameserver.communitybbs.Manager.req;

/**
 * @author savormix
 * 
 */
public enum FriendRequest
{
	REMOVE, SHOW_MESSAGING, SEND_MESSAGE;
}
