/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.communitybbs.Manager;

import java.util.Locale;
import java.util.StringTokenizer;

import com.l2jfree.util.logging.L2Logger;

import org.sod.L2Htm;
import org.sod.i18n.Internationalization;

import com.l2jfree.Config;
import com.l2jfree.gameserver.communitybbs.Manager.req.FriendRequest;
import com.l2jfree.gameserver.handler.ChatHandler;
import com.l2jfree.gameserver.handler.IChatHandler;
import com.l2jfree.gameserver.model.L2FriendList;
import com.l2jfree.gameserver.model.L2World;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.SystemChatChannelId;
import com.l2jfree.lang.L2TextBuilder;

/**
 * Manages the "Friends" tab in Community Board.
 * 
 * @author savormix
 * @since 2010.05.26
 */
public final class FriendsBBSManager extends BaseBBSManager<FriendRequest>
{
	private static final L2Logger _logChat = L2Logger.getLogger("chat");
	
	private FriendsBBSManager()
	{
	}
	
	@Override
	protected String getCmdPrefix()
	{
		return "_friendlist_0_";
	}
	
	@Override
	public void parsecmd(String command, L2PcInstance activeChar)
	{
		StringTokenizer st = new StringTokenizer(command, ";");
		st.nextToken(); // prefix
		
		if (!st.hasMoreTokens())
		{
			showFriends(activeChar);
			return;
		}
		
		int request;
		try
		{
			request = Integer.parseInt(st.nextToken());
		}
		catch (RuntimeException e)
		{
			return;
		}
		
		if (request < 0 || request >= FriendRequest.values().length)
			return;
		
		FriendRequest fr = FriendRequest.values()[request];
		switch (fr)
		{
		case SHOW_MESSAGING:
			showMessageBox(activeChar, st);
			break;
		case SEND_MESSAGE:
			sendMessage(activeChar, st);
			break;
		case REMOVE:
			removeFriend(activeChar, st);
			break;
		default:
			notImplementedYet(activeChar, command);
			break;
		}
	}
	
	@Override
	public void parsewrite(String ar1, String ar2, String ar3, String ar4, String ar5, L2PcInstance activeChar)
	{
		// TODO Auto-generated method stub
	}
	
	private void showFriends(L2PcInstance player)
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		L2FriendList fl = player.getFriendList();
		L2World w = L2World.getInstance();
		
		L2TextBuilder tb = new L2TextBuilder();
		writeHeader(tb, i18n.get(loc, "CB_FRIENDS_TITLE"));
		tb.append("<table width=\"280\">");
		boolean none = true;
		for (Integer fId : fl.getFriendIds())
		{
			L2PcInstance friend = w.findPlayer(fId);
			if (friend == null)
				continue;
			
			none = false;
			tb.append("<tr><td width=\"140\">");
			tb.append(friend.getName());
			tb.append("</td><td>");
			writeButton(tb, FriendRequest.SHOW_MESSAGING, fId, 150, 35, i18n.get(loc, "CB_FRIENDS_SEND_MESSAGE"));
			tb.append("</td><td>");
			writeButton(tb, FriendRequest.REMOVE, fId, 150, 35, i18n.get(loc, i18n.get(loc, "CB_FRIENDS_REMOVE")));
			tb.append("</td></tr>");
		}
		tb.append("</table>");
		if (none)
		{
			tb.append("<font color=\"LEVEL\">");
			tb.append(i18n.get(loc, "CB_FRIENDS_EMPTY"));
			tb.append("</font>");
		}
		tb.append("<br><img src=\"L2UI_CH3.herotower_deco\" width=\"256\" height=\"32\"></center></body></html>");
		
		separateAndSend(tb, player);
	}
	
	private void showMessageBox(L2PcInstance player, StringTokenizer st)
	{
		L2FriendList fl = player.getFriendList();
		L2World w = L2World.getInstance();
		
		L2PcInstance friend = null;
		try
		{
			Integer objectId = Integer.valueOf(st.nextToken());
			friend = w.findPlayer(objectId);
			if (!fl.contains(friend))
				friend = null;
		}
		catch (Exception e)
		{
		}
		
		showMessageBox(player, friend);
	}
	
	private void showMessageBox(L2PcInstance player, L2PcInstance friend)
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		writeHeader(tb, i18n.get(loc, "CB_FRIENDS_TITLE"));
		if (friend != null)
		{
			tb.append("<font color=\"LEVEL\">");
			tb.append(i18n.get(loc, "CB_FRIENDS_ENTER_MESSAGE", "%friendName%", friend.getName()));
			tb.append("</font><br1><table width=\"270\"><tr><td>");
			L2Htm.writeTextField(tb, "msg", 150, 100);
			tb.append("</td><td>");
			writeButton(tb, FriendRequest.SEND_MESSAGE, friend.getObjectId() + " $msg", 20, 45,
					i18n.get(loc, "CB_FRIENDS_SEND"));
			tb.append("</td></tr></table><br>");
		}
		else
			tb.append(i18n.get(loc, "CB_FRIENDS_RECEIVER_OFFLINE"));
		writeFooter(tb, i18n.get(loc, "BACK"));
		
		separateAndSend(tb, player);
	}
	
	private void removeFriend(L2PcInstance player, StringTokenizer st)
	{
		L2FriendList fl = player.getFriendList();
		L2World w = L2World.getInstance();
		
		L2PcInstance friend = null;
		try
		{
			Integer objectId = Integer.valueOf(st.nextToken());
			friend = w.findPlayer(objectId);
			if (!fl.contains(friend))
				friend = null;
		}
		catch (Exception e)
		{
		}
		if (friend != null)
			fl.remove(friend.getName());
		
		showFriends(player);
	}
	
	private void sendMessage(L2PcInstance player, StringTokenizer st)
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		L2FriendList fl = player.getFriendList();
		L2World w = L2World.getInstance();
		
		L2TextBuilder tb = new L2TextBuilder();
		writeHeader(tb, i18n.get(loc, "CB_FRIENDS_TITLE"));
		L2PcInstance friend = null;
		try
		{
			Integer objectId = Integer.parseInt(st.nextToken(" ").substring(1));
			friend = w.findPlayer(objectId);
			if (!fl.contains(friend))
				friend = null;
		}
		catch (RuntimeException e)
		{
		}
		if (friend != null)
		{
			try
			{
				String s = st.nextToken();
				if (s.length() > 100)
					s = s.substring(0, 100);
				if (Config.LOG_CHAT)
				{
					StringBuilder sb = new StringBuilder(SystemChatChannelId.Chat_Tell.getName());
					sb.append('[');
					sb.append(player.getName());
					sb.append(" to ");
					sb.append(friend.getName());
					sb.append("]: ");
					sb.append(s);
					_logChat.info(sb);
				}
				IChatHandler ch = ChatHandler.getInstance().getChatHandler(SystemChatChannelId.Chat_Tell);
				ch.useChatHandler(player, friend.getName(), SystemChatChannelId.Chat_Tell, s);
				showMessageBox(player, friend);
				return;
			}
			catch (Exception e)
			{
				tb.append(i18n.get(loc, "CB_FRIENDS_SEND_FAILED"));
			}
			
			writeFooter(tb, FriendRequest.SHOW_MESSAGING, friend.getObjectId(), i18n.get(loc, "BACK"));
		}
		else
		{
			tb.append(i18n.get(loc, "CB_FRIENDS_RECEIVER_OFFLINE"));
			writeFooter(tb, i18n.get(loc, "BACK"));
		}
		separateAndSend(tb, player);
	}
	
	public static final FriendsBBSManager getInstance()
	{
		return SingletonHolder._instance;
	}
	
	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final FriendsBBSManager _instance = new FriendsBBSManager();
	}
}
