/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.communitybbs.Manager;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javolution.util.FastMap;

import org.sod.Calc;
import org.sod.L2Htm;
import org.sod.i18n.Internationalization;
import org.sod.manager.ChallengeManager.Challenge;
import org.sod.manager.GlobalEventManager;
import org.sod.manager.PerkManager.Perk;
import org.sod.model.ChallengeTemplate;
import org.sod.model.L2PlayerData;
import org.sod.model.L2PlayerData.Record;
import org.sod.model.PlayerSettings;
import org.sod.tutorial.TutorialManager;

import com.l2jfree.gameserver.communitybbs.Manager.req.SetupRequest;
import com.l2jfree.gameserver.instancemanager.MapRegionManager;
import com.l2jfree.gameserver.model.L2Clan;
import com.l2jfree.gameserver.model.L2ItemInstance;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.itemcontainer.Inventory;
import com.l2jfree.gameserver.model.itemcontainer.PcInventory;
import com.l2jfree.gameserver.model.mapregion.L2MapRegion;
import com.l2jfree.gameserver.model.mapregion.L2MapRegionRestart;
import com.l2jfree.lang.L2TextBuilder;

/**
 * Manages the "Favorites" tab in Community Board.
 * 
 * @author savormix
 * @since 2010.01.19
 */
public final class SettingsBBSManager extends BaseBBSManager<SetupRequest>
{
	private final Map<String, Locale> _lang;
	private final Locale[] _allLocales;
	
	private SettingsBBSManager()
	{
		_lang = new HashMap<String, Locale>();
		{
			_lang.put("English", Locale.ENGLISH);
			_lang.put("Lietuvių", new Locale("lt"));
			_lang.put("Português", new Locale("pt"));
		}
		_allLocales = _lang.values().toArray(new Locale[_lang.size()]);
	}
	
	public Locale[] getAllLocales()
	{
		return _allLocales;
	}
	
	public int indexOf(Locale loc)
	{
		for (int i = 0; i < getAllLocales().length; i++)
			if (getAllLocales()[i].equals(loc))
				return i;
		return -1;
	}
	
	@Override
	protected String getCmdPrefix()
	{
		return "_bbsgetfav";
	}
	
	@Override
	public void parsecmd(String command, L2PcInstance activeChar)
	{
		StringTokenizer st = new StringTokenizer(command, ";");
		st.nextToken(); // prefix
		
		if (!st.hasMoreTokens())
		{
			showMenu(activeChar);
			return;
		}
		
		SetupRequest sr = L2Htm.getRequest(st, SetupRequest.values());
		if (sr == null)
			return;
		switch (sr)
		{
		case CLOSE:
			activeChar.sendPacket(HideBoard.PACKET);
			break;
		case LOCALE:
			changeLocale(activeChar, st);
			break;
		case EQUIPMENT:
			showEquipmentInfo(activeChar);
			break;
		case CHALLENGES:
			showChallenges(activeChar);
			break;
		case CHALLENGE_DETAILS:
			showChallenge(activeChar, st);
			break;
		case PERKS:
			showPerks(activeChar);
			break;
		case ACTIVATE_PERK:
			activatePerk(activeChar, st);
			break;
		case CLAN_HQ:
			showClanHq(activeChar);
			break;
		case DESTROY_HQ:
			destroyClanHq(activeChar, st);
			break;
		case SETTINGS:
			showSettings(activeChar);
			break;
		case VOTE_WINDOWS:
			toggleVoting(activeChar);
			break;
		case WAYPOINT:
			toggleWaypoint(activeChar);
			break;
		case SCREEN_MESSAGES:
			toggleScreenMessages(activeChar);
			break;
		case GHOST_MODE:
			toggleGhostMode(activeChar);
			break;
		case SKILL_SUCCESS:
			toggleSkillRates(activeChar, st);
			break;
		case JAIL_AND_OTHER:
			showOtherInfo(activeChar);
			break;
		case GE_PRELIMINARY:
			showGeInfo(activeChar, st);
			break;
		default:
			notImplementedYet(activeChar, command);
			break;
		}
	}
	
	@Override
	public void parsewrite(String ar1, String ar2, String ar3, String ar4, String ar5, L2PcInstance activeChar)
	{
		// TODO Auto-generated method stub
		
	}
	
	private void showMenu(L2PcInstance activeChar)
	{
		Internationalization i18n = Internationalization.getInstance();
		L2PlayerData dat = activeChar.getSoDPlayer();
		PlayerSettings pset = dat.getSettings();
		Locale loc = pset.getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		writeHeader(tb, "<img src=\"L2UI_CH3.herotower_deco\" width=\"256\" height=\"32\">");
		writeButton(tb, SetupRequest.EQUIPMENT, 150, 35, i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_EQUIPMENT"));
		tb.append("<br1>");
		writeButton(tb, SetupRequest.CHALLENGES, 150, 35, i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CHALLENGES"));
		tb.append("<br1>");
		writeButton(tb, SetupRequest.PERKS, 150, 35, i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_PERKS"));
		tb.append("<br1>");
		writeButton(tb, SetupRequest.CLAN_HQ, 150, 35, i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CLAN_HQ"));
		tb.append("<br1>");
		writeButton(tb, SetupRequest.SETTINGS, 150, 35, i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SETTINGS"));
		tb.append("<br1>");
		writeButton(tb, SetupRequest.JAIL_AND_OTHER, 150, 35, i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_OTHER"));
		writeFooter(tb, null);
		
		separateAndSend(tb, activeChar);
	}
	
	private void changeLocale(L2PcInstance activeChar, StringTokenizer st)
	{
		String lang;
		try
		{
			lang = st.nextToken().substring(1);
		}
		catch (RuntimeException e)
		{
			return;
		}
		Locale nl = _lang.get(lang);
		if (nl != null)
		{
			activeChar.getSoDPlayer().getSettings().setLocale(nl);
			
			if (!TutorialManager.getInstance().notifyLanguageSelected(activeChar))
				showSettings(activeChar);
		}
	}
	
	private void showEquipmentInfo(L2PcInstance activeChar)
	{
		Internationalization i18n = Internationalization.getInstance();
		L2PlayerData dat = activeChar.getSoDPlayer();
		PlayerSettings pset = dat.getSettings();
		Locale loc = pset.getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		writeHeader(tb, i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_EQUIPMENT"));
		PcInventory inv = activeChar.getInventory();
		L2ItemInstance item = inv.getPaperdollItem(Inventory.PAPERDOLL_RHAND);
		if (item != null)
		{
			tb.append(i18n
					.get(loc, "CB_PLAYER_INFO_SETTINGS_EQUIPMENT_KILL_COUNT")
					.replace(
							"%equipmentType%",
							"<font color=\"LEVEL\">" + i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_EQUIPMENT_TYPE_WEAPON")
									+ "</font>")
					.replace("%killCount%", "<font color=\"LEVEL\">" + item.getKills() + "</font>"));
			tb.append("<br>");
			StringBuilder sb = new StringBuilder("<font color=\"LEVEL\">");
			sb.append(Calc.calcNeededKills(item.getEnchantLevel(), item.getItem().getItemGrade(), true)
					- item.getKills());
			sb.append("</font>");
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_EQUIPMENT_REWARD_AFTER_KILLS", "%kills%", sb.toString()));
		}
		else
		{
			StringBuilder sb = new StringBuilder("<font color=\"LEVEL\">");
			sb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_EQUIPMENT_TYPE_WEAPON"));
			sb.append("</font>");
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_EQUIPMENT_NOT_EQUIPPED", "%equipmentType%", sb.toString()));
		}
		tb.append("<br><br>");
		
		item = inv.getPaperdollItem(Inventory.PAPERDOLL_CHEST);
		if (item != null)
		{
			tb.append(i18n
					.get(loc, "CB_PLAYER_INFO_SETTINGS_EQUIPMENT_KILL_COUNT")
					.replace(
							"%equipmentType%",
							"<font color=\"LEVEL\">"
									+ i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_EQUIPMENT_TYPE_CHEST_ARMOR") + "</font>")
					.replace("%killCount%", "<font color=\"LEVEL\">" + item.getKills() + "</font>"));
			tb.append("<br>");
			StringBuilder sb = new StringBuilder("<font color=\"LEVEL\">");
			sb.append(Calc.calcNeededKills(Calc.calcMaxArmorEnchant(activeChar, item.getItem().getItemGrade()), item
					.getItem().getItemGrade(), false)
					- item.getKills());
			sb.append("</font>");
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_EQUIPMENT_REWARD_AFTER_KILLS", "%kills%", sb.toString()));
		}
		else
		{
			StringBuilder sb = new StringBuilder("<font color=\"LEVEL\">");
			sb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_EQUIPMENT_TYPE_CHEST_ARMOR"));
			sb.append("</font>");
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_EQUIPMENT_NOT_EQUIPPED", "%equipmentType%", sb.toString()));
		}
		tb.append("<br>");
		writeFooter(tb, i18n.get(loc, "BACK"));
		
		separateAndSend(tb, activeChar);
	}
	
	private void showChallenges(L2PcInstance activeChar)
	{
		Internationalization i18n = Internationalization.getInstance();
		L2PlayerData dat = activeChar.getSoDPlayer();
		PlayerSettings pset = dat.getSettings();
		Locale loc = pset.getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		writeHeader(tb, i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CHALLENGES"));
		tb.append("<font color=\"LEVEL\">");
		tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CHALLENGES_COMPLETED"));
		tb.append("</font><br><table width=\"600\">");
		FastMap<ChallengeTemplate, Challenge> challs = dat.getChallenges();
		int i = 0, j = 0;
		L2TextBuilder incomplete = new L2TextBuilder();
		for (ChallengeTemplate ct : ChallengeTemplate.values())
		{
			if (!ct.isEnabled())
				continue;
			Challenge ch = challs.get(ct);
			if (ch == null || !ch.isUpgraded())
			{
				if (++i % 5 == 1)
					incomplete.append("<tr>");
				incomplete.append("<td>");
				writeButton(incomplete, SetupRequest.CHALLENGE_DETAILS, ct.getId(), 150, 20, true, true,
						ct.getFullName());
				incomplete.append("</td>");
				if (i % 5 == 0)
					incomplete.append("</tr>");
			}
			else
			{
				if (++j % 5 == 1)
					tb.append("<tr>");
				tb.append("<td>");
				writeButton(tb, SetupRequest.CHALLENGE_DETAILS, ct.getId(), 150, 20, true, false, ct.getFullName());
				tb.append("</td>");
				if (j % 5 == 0)
					tb.append("</tr>");
			}
		}
		if (i == 0)
		{
			incomplete.append("<tr><td>");
			incomplete.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CHALLENGES_ALL_COMPLETED"));
			incomplete.append("</td></tr>");
		}
		else if (i % 5 != 0)
			incomplete.append("</tr>");
		if (j == 0)
		{
			tb.append("<tr><td>");
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CHALLENGES_ALL_INCOMPLETE"));
			tb.append("</td></tr>");
		}
		else if (j % 5 != 0)
			tb.append("</tr>");
		tb.append("</table><br><font color=\"LEVEL\">");
		tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CHALLENGES_NOT_COMPLETED"));
		tb.append("</font><br><table width=\"600\">");
		tb.append(incomplete.moveToString());
		tb.append("</table>");
		writeFooter(tb, i18n.get(loc, "BACK"));
		
		separateAndSend(tb, activeChar);
	}
	
	private void showChallenge(L2PcInstance activeChar, StringTokenizer st)
	{
		Internationalization i18n = Internationalization.getInstance();
		L2PlayerData dat = activeChar.getSoDPlayer();
		PlayerSettings pset = dat.getSettings();
		Locale loc = pset.getLocale();
		
		ChallengeTemplate ct;
		try
		{
			int id = Integer.parseInt(st.nextToken());
			ct = ChallengeTemplate.getTemplate(id);
			if (ct == null)
				return;
		}
		catch (RuntimeException e)
		{
			return;
		}
		
		L2TextBuilder tb = new L2TextBuilder();
		writeHeader(tb, i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CHALLENGES"));
		tb.append("<table><tr><td>");
		tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CHALLENGE_NAME"));
		tb.append("</td><td>");
		tb.append(ct.getFullName());
		tb.append("</td></tr><tr><td>");
		tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CHALLENGE_DESCRIPTION"));
		tb.append("</td><td>");
		tb.append(ct.getDesc());
		tb.append("</td></tr><tr><td>");
		tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CHALLENGE_REWARD"));
		tb.append("</td><td>");
		tb.append(ct.getReward());
		tb.append("</td></tr><tr><td>");
		tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CHALLENGE_COMPLETION"));
		tb.append("</td><td>");
		int percent = 0;
		Challenge ch = dat.getChallenges().get(ct);
		if (ch != null)
			percent = (ch.getPoints() * 100 / ct.getNeededPoints());
		tb.append("<font color=\"");
		if (percent < 34)
			tb.append("FF0000");
		else if (percent < 100)
			tb.append("FFFF00");
		else
			tb.append("00FF00");
		tb.append("\">");
		tb.append(percent);
		tb.append("%</font></td></tr></table>");
		writeFooter(tb, SetupRequest.CHALLENGES, i18n.get(loc, "BACK"));
		
		separateAndSend(tb, activeChar);
	}
	
	private void writePerk(L2TextBuilder tb, Locale loc, Perk p)
	{
		Internationalization i18n = Internationalization.getInstance();
		
		tb.append(p.getTemplate().getName());
		if (p.isUpgraded())
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_PERK_UPGRADE_SUFFIX"));
		tb.append("<br1>");
		if (!p.isUpgraded())
		{
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_PERK_POINTS", "%points%", String.valueOf(p.getPoints())));
			tb.append("<br1>");
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_PERK_UPGRADE_PERCENTAGE", "%percent%",
					String.valueOf(p.getPoints() * 100 / p.getTemplate().getNeededPoints())));
		}
		else
			tb.append("<br>");
	}
	
	public void showPerks(L2PcInstance activeChar)
	{
		Internationalization i18n = Internationalization.getInstance();
		L2PlayerData dat = activeChar.getSoDPlayer();
		PlayerSettings pset = dat.getSettings();
		Locale loc = pset.getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		writeHeader(tb, i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_PERKS"));
		tb.append("<font color=\"LEVEL\">");
		tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_PERKS_ACTIVE"));
		tb.append("</font><br><table width=\"600\"><tr>");
		Perk[] eq = dat.getActivePerks();
		for (int i = 0; i < eq.length; i++)
		{
			Perk p = eq[i];
			tb.append("<td>");
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_PERK_SLOT", "%slot%", String.valueOf(i + 1)));
			tb.append(": ");
			if (p != null)
				writePerk(tb, loc, p);
			else
				tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_PERK_SLOT_EMPTY"));
			tb.append("</td>");
		}
		tb.append("</tr></table><br><br><font color=\"LEVEL\">");
		tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_PERKS_UNLOCKED"));
		tb.append("</font><br><table width=\"600\">");
		FastMap<Integer, Perk> earned = dat.getEarnedPerks();
		int i = 0;
		for (FastMap.Entry<Integer, Perk> entry = earned.head(), end = earned.tail(); (entry = entry.getNext()) != end;)
		{
			Perk p = entry.getValue();
			if (p.isActive())
				continue;
			
			if (++i % 5 == 1)
				tb.append("<tr>");
			tb.append("<td>(");
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_PERK_SLOT", "%slot%",
					String.valueOf(p.getTemplate().getSlot() + 1)));
			tb.append(")<br1>");
			writePerk(tb, loc, p);
			writeButton(tb, SetupRequest.ACTIVATE_PERK, p.getTemplate().getId(), 100, 35,
					i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_PERK_ACTIVATE"));
			tb.append("</td>");
			if (i % 5 == 0)
				tb.append("</tr>");
		}
		if (i % 5 != 0)
			tb.append("</tr>");
		tb.append("</table>");
		writeFooter(tb, i18n.get(loc, "BACK"));
		
		separateAndSend(tb, activeChar);
	}
	
	private void activatePerk(L2PcInstance activeChar, StringTokenizer st)
	{
		Internationalization i18n = Internationalization.getInstance();
		L2PlayerData dat = activeChar.getSoDPlayer();
		PlayerSettings pset = dat.getSettings();
		Locale loc = pset.getLocale();
		
		Integer id;
		try
		{
			id = Integer.valueOf(st.nextToken());
		}
		catch (RuntimeException e)
		{
			return;
		}
		
		L2TextBuilder tb = new L2TextBuilder();
		writeHeader(tb, i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_PERKS"), " – ",
				i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_PERK_ACTIVATE"));
		Perk p = dat.getEarnedPerks().get(id);
		if (p != null && !activeChar.isLocked())
		{
			dat.getNextPerks()[p.getTemplate().getSlot()] = p;
			String re;
			if (dat.schedulePerkEquip())
				re = i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_PERK_ACTIVATE_SOON");
			else
				re = i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_PERK_ACTIVATE_REVIVAL");
			tb.append(re.replace("%perk%", "<font color=\"LEVEL\">" + p.getTemplate().getName() + "</font>"));
		}
		else
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_PERK_ACTIVATE_FAILED"));
		writeFooter(tb, SetupRequest.PERKS, i18n.get(loc, "BACK"));
		
		separateAndSend(tb, activeChar);
	}
	
	private void showClanHq(L2PcInstance activeChar)
	{
		Internationalization i18n = Internationalization.getInstance();
		L2PlayerData dat = activeChar.getSoDPlayer();
		PlayerSettings pset = dat.getSettings();
		Locale loc = pset.getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		writeHeader(tb, i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CLAN_HQ"));
		L2Clan clan = activeChar.getClan();
		if (clan != null)
		{
			if (clan.getNumFlags() > 0)
			{
				MapRegionManager mrm = MapRegionManager.getInstance();
				for (L2Npc flag : clan.getFlags())
				{
					tb.append("<br>");
					final String dest;
					L2MapRegion region = mrm.getRegion(flag);
					if (region != null)
					{
						L2MapRegionRestart restart = mrm.getRestartLocation(region.getRestartId());
						switch (restart.getLocName())
						{
						case 910:
							dest = "Talking Island";
							break;
						case 911:
							dest = "Gludin Territory";
							break;
						case 912:
							dest = "Gludio Territory";
							break;
						case 913:
							dest = "Neutral Zone";
							break;
						case 914:
							dest = "Elven Territory";
							break;
						case 915:
							dest = "Dark Elven Territory";
							break;
						case 916:
							dest = "Dion Territory";
							break;
						case 917:
							dest = "Floran Territory";
							break;
						case 918:
							dest = "Giran Territory";
							break;
						case 919:
							dest = "Giran Harbor";
							break;
						case 920:
							dest = "Orc Territory";
							break;
						case 921:
							dest = "Dwarven Territory";
							break;
						case 922:
							dest = "Oren Territory";
							break;
						case 923:
							dest = "Hunters' Territory";
							break;
						case 924:
							dest = "Aden Territory";
							break;
						case 925:
							dest = "Coliseum";
							break;
						case 926:
							dest = "Innadril Territory";
							break;
						case 1537:
							dest = "Rune Territory";
							break;
						case 1538:
							dest = "Goddard Territory";
							break;
						case 1714:
							dest = "Schuttgart Territory";
							break;
						case 1924:
							dest = "Primeval Isle";
							break;
						case 2189:
							dest = "Kamael Territory";
							break;
						case 2190:
							dest = "Wastelands";
							break;
						case 2259:
							dest = "Fantasy Isle";
							break;
						case 2710:
						case 2711:
						case 2712:
						case 2716:
							dest = "Gracia Territory";
							break;
						default:
							dest = i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CLAN_HQ_LOCATION_UNKNOWN");
							break;
						}
					}
					else
						dest = i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CLAN_HQ_LOCATION_UNKNOWN");
					tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CLAN_HQ_LOCATION", "%location%", dest));
					tb.append("<br>");
					tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CLAN_HQ_HP", "%hp%",
							String.valueOf((int) flag.getCurrentHp()), "%maxHp%", String.valueOf(flag.getMaxHp())));
					if (L2Clan.checkPrivileges(activeChar, L2Clan.CP_CS_MANAGE_SIEGE))
					{
						tb.append("<br>");
						writeButton(tb, SetupRequest.DESTROY_HQ, flag.getObjectId(), 100, 35,
								i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CLAN_HQ_DESTROY"));
					}
				}
			}
			else
				tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CLAN_HQ_MISSING"));
		}
		else
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_NO_CLAN"));
		writeFooter(tb, i18n.get(loc, "BACK"));
		
		separateAndSend(tb, activeChar);
	}
	
	private void destroyClanHq(L2PcInstance activeChar, StringTokenizer st)
	{
		Internationalization i18n = Internationalization.getInstance();
		L2PlayerData dat = activeChar.getSoDPlayer();
		PlayerSettings pset = dat.getSettings();
		Locale loc = pset.getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CLAN_HQ"));
		L2Clan clan = activeChar.getClan();
		if (L2Clan.checkPrivileges(activeChar, L2Clan.CP_CS_MANAGE_SIEGE))
		{
			String key = "CB_PLAYER_INFO_SETTINGS_CLAN_HQ_INVALID";
			try
			{
				int objectId = Integer.parseInt(st.nextToken());
				for (L2Npc flag : clan.getFlags())
				{
					if (flag.getObjectId() == objectId)
					{
						flag.doDie(activeChar);
						key = "CB_PLAYER_INFO_SETTINGS_CLAN_HQ_DESTROYED";
						break;
					}
				}
			}
			catch (RuntimeException e)
			{
			}
			tb.append(i18n.get(loc, key));
		}
		else
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CLAN_HQ_ELEVATED"));
		writeFooter(tb, SetupRequest.CLAN_HQ, i18n.get(loc, "BACK"));
		
		separateAndSend(tb, activeChar);
	}
	
	private void writeOnOff(L2TextBuilder tb, boolean on)
	{
		if (on)
			tb.append("<font color=\"00FF00\">ON</font>");
		else
			tb.append("<font color=\"FF0000\">OFF</font>");
	}
	
	public void writeLanguageSelect(L2TextBuilder tb, Locale loc, boolean ok)
	{
		Internationalization i18n = Internationalization.getInstance();
		
		tb.append("<table><tr><td fixWIDTH=\"80\" align=\"center\">");
		tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_LANGUAGE"));
		tb.append("</td><td align=\"center\">");
		tb.append("<combobox width=\"190\" var=\"lang\" list=\"");
		L2TextBuilder rest = new L2TextBuilder();
		for (Entry<String, Locale> e : _lang.entrySet())
		{
			if (!e.getValue().equals(loc))
			{
				rest.append(';');
				rest.append(e.getKey());
			}
			else
				tb.append(e.getKey());
		}
		tb.append(rest.moveToString());
		tb.append("\">");
		writeButton(tb, SetupRequest.LOCALE, " $lang", 150, 35,
				ok ? "OK" : i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CHANGE"));
		tb.append("<br></td></tr></table>");
	}
	
	private void showSettings(L2PcInstance activeChar)
	{
		Internationalization i18n = Internationalization.getInstance();
		L2PlayerData dat = activeChar.getSoDPlayer();
		PlayerSettings pset = dat.getSettings();
		Locale loc = pset.getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		writeHeader(tb, i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SETTINGS"));
		writeLanguageSelect(tb, loc, false);
		if (TutorialManager.getInstance().isInTutorial(activeChar))
		{
			writeFooter(tb, SetupRequest.CLOSE, i18n.get(loc, "CLOSE"));
			separateAndSend(tb, activeChar);
			return;
		}
		tb.append("<table><tr><td align=\"center\">");
		tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_VOTING"));
		tb.append(": ");
		writeOnOff(tb, pset.showVoting());
		writeButton(tb, SetupRequest.VOTE_WINDOWS, 150, 35, i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CHANGE"));
		tb.append("<br></td><td align=\"center\">");
		tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_WAYPOINT"));
		tb.append(": ");
		writeOnOff(tb, pset.showWaypoint());
		writeButton(tb, SetupRequest.WAYPOINT, 150, 35, i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CHANGE"));
		tb.append("<br></td></tr><tr><td align=\"center\">");
		tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SCREEN_MESSAGES"));
		tb.append(": ");
		writeOnOff(tb, pset.showMessages());
		writeButton(tb, SetupRequest.SCREEN_MESSAGES, 150, 35, i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CHANGE"));
		tb.append("<br></td><td align=\"center\">");
		tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_GHOST_MODE"));
		tb.append(": ");
		writeOnOff(tb, pset.isGhostMode());
		writeButton(tb, SetupRequest.GHOST_MODE, 150, 35, i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_CHANGE"));
		tb.append("<br></td></tr></table><br>");
		tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SKILL_SUCCESS"));
		tb.append(": <font color=\"");
		final int color = tb.length();
		tb.append("00FF00\">");
		switch (pset.showSuccessRates())
		{
		case 1:
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SKILL_SUCCESS_UPPER_LEFT"));
			break;
		case 2:
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SKILL_SUCCESS_UPPER_CENTER"));
			break;
		case 3:
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SKILL_SUCCESS_UPPER_RIGHT"));
			break;
		case 4:
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SKILL_SUCCESS_LEFT"));
			break;
		case 5:
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SKILL_SUCCESS_CENTER"));
			break;
		case 6:
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SKILL_SUCCESS_RIGHT"));
			break;
		case 7:
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SKILL_SUCCESS_LOWER_CENTER"));
			break;
		case 8:
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SKILL_SUCCESS_LOWER_RIGHT"));
			break;
		default:
			tb.setCharAt(color, 'F');
			tb.setCharAt(color + 1, 'F');
			tb.setCharAt(color + 2, '0');
			tb.setCharAt(color + 3, '0');
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SKILL_SUCCESS_OFF"));
			break;
		}
		tb.append("</font><table><tr><td>");
		writeButton(tb, SetupRequest.SKILL_SUCCESS, 1, 35, 35,
				i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SKILL_SUCCESS_UPPER_LEFT_ABBR"));
		tb.append("</td><td>");
		writeButton(tb, SetupRequest.SKILL_SUCCESS, 2, 35, 35,
				i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SKILL_SUCCESS_UPPER_CENTER_ABBR"));
		tb.append("</td><td>");
		writeButton(tb, SetupRequest.SKILL_SUCCESS, 3, 35, 35,
				i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SKILL_SUCCESS_UPPER_RIGHT_ABBR"));
		tb.append("</td></tr><tr><td>");
		writeButton(tb, SetupRequest.SKILL_SUCCESS, 4, 35, 35,
				i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SKILL_SUCCESS_LEFT_ABBR"));
		tb.append("</td><td>");
		writeButton(tb, SetupRequest.SKILL_SUCCESS, 5, 35, 35,
				i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SKILL_SUCCESS_CENTER_ABBR"));
		tb.append("</td><td>");
		writeButton(tb, SetupRequest.SKILL_SUCCESS, 6, 35, 35,
				i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SKILL_SUCCESS_RIGHT_ABBR"));
		tb.append("</td></tr><tr><td></td><td>");
		writeButton(tb, SetupRequest.SKILL_SUCCESS, 7, 35, 35,
				i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SKILL_SUCCESS_LOWER_CENTER_ABBR"));
		tb.append("</td><td>");
		writeButton(tb, SetupRequest.SKILL_SUCCESS, 8, 35, 35,
				i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SKILL_SUCCESS_LOWER_RIGHT_ABBR"));
		tb.append("</td></tr></table>");
		writeButton(tb, SetupRequest.SKILL_SUCCESS, 0, 105, 35,
				i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SKILL_SUCCESS_DISABLE"));
		writeFooter(tb, i18n.get(loc, "BACK"));
		
		separateAndSend(tb, activeChar);
	}
	
	private void toggleVoting(L2PcInstance activeChar)
	{
		Internationalization i18n = Internationalization.getInstance();
		L2PlayerData dat = activeChar.getSoDPlayer();
		PlayerSettings pset = dat.getSettings();
		Locale loc = pset.getLocale();
		
		boolean status = !pset.showVoting();
		pset.setShowVoting(status);
		
		L2TextBuilder tb = new L2TextBuilder();
		writeHeader(tb, i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SETTINGS"), " – ",
				i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_VOTING"));
		if (status)
		{
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_VOTING_ENABLED"));
			GlobalEventManager.getInstance().askVote(activeChar);
		}
		else
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_VOTING_DISABLED"));
		writeFooter(tb, SetupRequest.SETTINGS, i18n.get(loc, "BACK"));
		
		separateAndSend(tb, activeChar);
	}
	
	private void toggleWaypoint(L2PcInstance activeChar)
	{
		Internationalization i18n = Internationalization.getInstance();
		L2PlayerData dat = activeChar.getSoDPlayer();
		PlayerSettings pset = dat.getSettings();
		Locale loc = pset.getLocale();
		
		boolean status = !pset.showWaypoint();
		pset.setWaypoint(status);
		
		L2TextBuilder tb = new L2TextBuilder();
		writeHeader(tb, i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SETTINGS"), " – ",
				i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_WAYPOINT"));
		if (status)
		{
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_WAYPOINT_ENABLED"));
			GlobalEventManager.getInstance().askVote(activeChar);
		}
		else
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_WAYPOINT_DISABLED"));
		writeFooter(tb, SetupRequest.SETTINGS, i18n.get(loc, "BACK"));
		
		separateAndSend(tb, activeChar);
	}
	
	private void toggleScreenMessages(L2PcInstance activeChar)
	{
		Internationalization i18n = Internationalization.getInstance();
		L2PlayerData dat = activeChar.getSoDPlayer();
		PlayerSettings pset = dat.getSettings();
		Locale loc = pset.getLocale();
		
		boolean status = !pset.showMessages();
		pset.setMessages(status);
		L2TextBuilder tb = new L2TextBuilder();
		writeHeader(tb, i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SETTINGS"), " – ",
				i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SCREEN_MESSAGES"));
		if (status)
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SCREEN_MESSAGES_ENABLED"));
		else
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SCREEN_MESSAGES_DISABLED"));
		writeFooter(tb, SetupRequest.SETTINGS, i18n.get(loc, "BACK"));
		
		separateAndSend(tb, activeChar);
	}
	
	private void toggleGhostMode(L2PcInstance activeChar)
	{
		Internationalization i18n = Internationalization.getInstance();
		L2PlayerData dat = activeChar.getSoDPlayer();
		PlayerSettings pset = dat.getSettings();
		Locale loc = pset.getLocale();
		
		boolean status = !pset.isGhostMode();
		pset.setGhostMode(status);
		L2TextBuilder tb = new L2TextBuilder();
		writeHeader(tb, i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SETTINGS"), " – ",
				i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_GHOST_MODE"));
		if (status)
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_GHOST_MODE_ENABLED"));
		else
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_GHOST_MODE_DISABLED"));
		writeFooter(tb, SetupRequest.SETTINGS, i18n.get(loc, "BACK"));
		
		separateAndSend(tb, activeChar);
	}
	
	private void toggleSkillRates(L2PcInstance activeChar, StringTokenizer st)
	{
		int state;
		try
		{
			state = Integer.parseInt(st.nextToken());
		}
		catch (RuntimeException e)
		{
			return;
		}
		
		Internationalization i18n = Internationalization.getInstance();
		L2PlayerData dat = activeChar.getSoDPlayer();
		PlayerSettings pset = dat.getSettings();
		Locale loc = pset.getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		writeHeader(tb, i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SETTINGS"), " – ",
				i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SKILL_SUCCESS"));
		pset.setSuccessRates(state);
		if (state > 0)
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SKILL_SUCCESS_ENABLED"));
		else
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_SKILL_SUCCESS_DISABLED"));
		writeFooter(tb, SetupRequest.SETTINGS, i18n.get(loc, "BACK"));
		
		separateAndSend(tb, activeChar);
	}
	
	private void showOtherInfo(L2PcInstance activeChar)
	{
		Internationalization i18n = Internationalization.getInstance();
		L2PlayerData dat = activeChar.getSoDPlayer();
		PlayerSettings pset = dat.getSettings();
		Locale loc = pset.getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		writeHeader(tb, i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_OTHER"));
		tb.append("<table><tr><td>");
		tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_OTHER_TIMES_JAILED"));
		tb.append("</td><td>");
		tb.append(dat.getJailTotal());
		tb.append("</td></tr>");
		if (activeChar.isInJail())
		{
			tb.append("<tr><td>");
			tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_OTHER_JAIL_REASON"));
			tb.append("</td><td>");
			tb.append(dat.getJailReason());
			tb.append("</td></tr>");
			ScheduledFuture<?> task = activeChar.getJailTask();
			if (task != null)
			{
				tb.append("<tr><td>");
				tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_OTHER_JAIL_TIME_LEFT"));
				tb.append("</td><td>");
				tb.append(task.getDelay(TimeUnit.MINUTES));
				tb.append("</td></tr>");
			}
		}
		Record r = dat.getRecords();
		tb.append("<tr><td>");
		tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_OTHER_TIMES_DIED"));
		tb.append("</td><td>");
		tb.append(r.getDeathCount());
		tb.append("</td></tr><tr><td>");
		tb.append(i18n.get(loc, "CB_PLAYER_INFO_SETTINGS_OTHER_KILL_STREAK"));
		tb.append("</td><td>");
		tb.append(r.getMaxStreak());
		tb.append("</td></tr></table><br>");
		writeFooter(tb, i18n.get(loc, "BACK"));
		
		separateAndSend(tb, activeChar);
	}
	
	private void showGeInfo(L2PcInstance activeChar, StringTokenizer st)
	{
		int page = 0;
		try
		{
			page = Integer.parseInt(st.nextToken());
		}
		catch (RuntimeException e)
		{
		}
		
		showGeInfo(activeChar, page);
	}
	
	public void showGeInfo(L2PcInstance activeChar, int page)
	{
		Internationalization i18n = Internationalization.getInstance();
		PlayerSettings pset = activeChar.getSoDPlayer().getSettings();
		Locale loc = pset.getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		writeHeader(tb, i18n.get(loc, "CB_GLOBAL_EVENT_PRELIMINARY_RESULTS"));
		tb.append(GlobalEventManager.getInstance().getPrelimResults(page, activeChar));
		writeFooter(tb, SetupRequest.CLOSE, i18n.get(loc, "CLOSE"));
		
		separateAndSend(tb, activeChar);
	}
	
	public static final SettingsBBSManager getInstance()
	{
		return SingletonHolder._instance;
	}
	
	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final SettingsBBSManager _instance = new SettingsBBSManager();
	}
}
