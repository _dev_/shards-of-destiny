/*
 * MISSING LICENSING INFO
 */
package com.l2jfree.gameserver.communitybbs.Manager.req;

/**
 * @author savormix
 * 
 */
public enum AuctionRequest
{
	VIEW_AUCTIONS, VIEW_SELLABLES, VIEW_PURCHASED, AUCTION_INFO, SALE_SETUP, SELL, CLAIM_PURCHASE;
}
