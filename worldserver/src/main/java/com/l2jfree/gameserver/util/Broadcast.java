/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.util;

import java.util.Collection;
import java.util.Locale;
import java.util.Set;

import javolution.util.FastSet;

import org.sod.i18n.Internationalization;
import org.sod.tutorial.TutorialManager;

import com.l2jfree.gameserver.model.L2World;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.SystemMessageId;
import com.l2jfree.gameserver.network.client.packets.L2ServerPacket;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.Chat;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.ChatMessage;
import com.l2jfree.gameserver.network.serverpackets.CharInfo;

/**
 * @author luisantonioa
 */
public final class Broadcast
{
	private Broadcast()
	{
	}
	
	public static void toKnownPlayers(L2Character character, L2ServerPacket mov)
	{
		if (!character.getKnownList().getKnownPlayers().isEmpty())
			for (L2PcInstance player : character.getKnownList().getKnownPlayers().values())
				if (player != null)
					player.sendPacket(mov);
	}
	
	public static void toKnownPlayersCharInfo(L2PcInstance character)
	{
		CharInfo ci = new CharInfo(character);
		if (!character.getKnownList().getKnownPlayers().isEmpty())
			for (L2PcInstance player : character.getKnownList().getKnownPlayers().values())
				if (player != null)
					player.sendPacket(ci);
	}
	
	public static void toKnownPlayersInRadius(L2Character character, L2ServerPacket mov, int radius)
	{
		if (radius < 0)
		{
			toKnownPlayers(character, mov);
			return;
		}
		else if (radius > 10000)
		{
			toKnownPlayersInRadius(character, mov, (long) radius);
			return;
		}
		
		if (!character.getKnownList().getKnownPlayers().isEmpty())
			for (L2PcInstance player : character.getKnownList().getKnownPlayers().values())
				if (character.isInsideRadius(player, radius, false, false))
					if (player != null)
						player.sendPacket(mov);
	}
	
	public static void toKnownPlayersInRadius(L2Character character, L2ServerPacket mov, long radiusSq)
	{
		if (radiusSq < 0)
		{
			toKnownPlayers(character, mov);
			return;
		}
		else if (radiusSq < 10000)
		{
			toKnownPlayersInRadius(character, mov, (int) radiusSq);
			return;
		}
		
		if (!character.getKnownList().getKnownPlayers().isEmpty())
			for (L2PcInstance player : character.getKnownList().getKnownPlayers().values())
				if (character.getDistanceSq(player) <= radiusSq)
					if (player != null)
						player.sendPacket(mov);
	}
	
	public static void toSelfAndKnownPlayers(L2Character character, L2ServerPacket mov)
	{
		if (character instanceof L2PcInstance)
			((L2PcInstance) character).sendPacket(mov);
		
		toKnownPlayers(character, mov);
	}
	
	public static void toSelfAndKnownPlayersInRadius(L2Character character, L2ServerPacket mov, int radius)
	{
		if (character instanceof L2PcInstance)
			((L2PcInstance) character).sendPacket(mov);
		
		toKnownPlayersInRadius(character, mov, radius);
	}
	
	public static void toSelfAndKnownPlayersInRadius(L2Character character, L2ServerPacket mov, long radiusSq)
	{
		if (character instanceof L2PcInstance)
			((L2PcInstance) character).sendPacket(mov);
		
		toKnownPlayersInRadius(character, mov, radiusSq);
	}
	
	public static void toAllOnlinePlayers(L2ServerPacket mov)
	{
		for (L2PcInstance player : L2World.getInstance().getAllPlayers())
			if (player != null)
				player.sendPacket(mov);
	}
	
	public static void toAllOnlinePlayers(SystemMessageId sm)
	{
		toAllOnlinePlayers(sm.getSystemMessage());
	}
	
	public static void toAllOnlinePlayers(Chat channel, String name, String key, String... repVals)
	{
		toAllOnlinePlayers(channel, name, false, key, repVals);
	}
	
	public static void toAllOnlinePlayers(Chat channel, String name, boolean locName, String key, String... repVals)
	{
		broadcastMessage(L2World.getInstance().getAllPlayers(), channel, name, locName, key, repVals);
	}
	
	public static void broadcastMessage(FastSet<L2PcInstance> pcs, Chat channel, String name, boolean locName,
			String key, String... repVals)
	{
		TutorialManager tm = TutorialManager.getInstance();
		Internationalization i18n = Internationalization.getInstance();
		for (FastSet.Record r = pcs.head(), end = pcs.tail(); (r = r.getNext()) != end;)
		{
			L2PcInstance player = pcs.valueOf(r);
			if (tm.isInTutorial(player))
				continue;
			
			Locale loc = player.getSoDPlayer().getSettings().getLocale();
			ChatMessage cs = new ChatMessage(channel, locName ? i18n.get(loc, name) : name, i18n.get(loc, key, repVals));
			player.sendPacket(cs);
		}
	}
	
	public static void broadcastMessage(Collection<L2PcInstance> pcs, Chat channel, String name, boolean locName,
			String key, String... repVals)
	{
		TutorialManager tm = TutorialManager.getInstance();
		Internationalization i18n = Internationalization.getInstance();
		for (L2PcInstance player : pcs)
		{
			if (tm.isInTutorial(player))
				continue;
			
			Locale loc = player.getSoDPlayer().getSettings().getLocale();
			ChatMessage cs = new ChatMessage(channel, locName ? i18n.get(loc, name) : name, i18n.get(loc, key, repVals));
			player.sendPacket(cs);
		}
	}
	
	public static void broadcastMessage(Collection<L2PcInstance> pcs, L2Character talker, Chat channel, String key,
			String... repVals)
	{
		TutorialManager tm = TutorialManager.getInstance();
		Internationalization i18n = Internationalization.getInstance();
		for (L2PcInstance player : pcs)
		{
			if (tm.isInTutorial(player))
				continue;
			
			Locale loc = player.getSoDPlayer().getSettings().getLocale();
			ChatMessage cs = new ChatMessage(channel, talker.getName(), i18n.get(loc, key, repVals));
			player.sendPacket(cs);
		}
	}
	
	public static void broadcastMessage(Set<L2Character> pcs, L2Character talker, Chat channel, String key,
			String... repVals)
	{
		TutorialManager tm = TutorialManager.getInstance();
		Internationalization i18n = Internationalization.getInstance();
		for (L2Character pc : pcs)
		{
			if (!(pc instanceof L2PcInstance))
				continue;
			
			L2PcInstance player = pc.getActingPlayer();
			
			if (tm.isInTutorial(player))
				continue;
			
			Locale loc = player.getSoDPlayer().getSettings().getLocale();
			ChatMessage cs = new ChatMessage(channel, talker.getName(), i18n.get(loc, key, repVals));
			player.sendPacket(cs);
		}
	}
	
	public static void broadcastMessage(Set<L2Character> pcs, Chat channel, String name, boolean locName, String key,
			String... repVals)
	{
		TutorialManager tm = TutorialManager.getInstance();
		Internationalization i18n = Internationalization.getInstance();
		for (L2Character pc : pcs)
		{
			if (!(pc instanceof L2PcInstance))
				continue;
			
			L2PcInstance player = pc.getActingPlayer();
			
			if (tm.isInTutorial(player))
				continue;
			
			Locale loc = player.getSoDPlayer().getSettings().getLocale();
			ChatMessage cs = new ChatMessage(channel, locName ? i18n.get(loc, name) : name, i18n.get(loc, key, repVals));
			player.sendPacket(cs);
		}
	}
}
