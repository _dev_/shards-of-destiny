/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.instancemanager.grandbosses;

import javolution.util.FastMap;
import javolution.util.FastSet;

import org.sod.manager.RespawnManager;

import com.l2jfree.gameserver.ThreadPoolManager;
import com.l2jfree.gameserver.instancemanager.BossSpawnManager;
import com.l2jfree.gameserver.instancemanager.InstanceManager;
import com.l2jfree.gameserver.model.L2Party;
import com.l2jfree.gameserver.model.Location;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.entity.Entity;
import com.l2jfree.gameserver.model.entity.GrandBossState;
import com.l2jfree.gameserver.model.quest.QuestState;

/**
 * 
 * This class ...
 * control for sequence of fight against Antharas.
 * 
 * @version $Revision: $ $Date: $
 * @author L2J_JP SANDMAN
 */
public abstract class BossLair extends Entity
{
	private static final FastMap<Integer, FastSet<L2Party>> _raiders = new FastMap<Integer, FastSet<L2Party>>()
			.setShared(true);
	private final int _bossId;
	protected final int _instanceId;
	protected L2Npc _bossSpawn;
	private final Location _teleport;
	
	@SuppressWarnings("deprecation")
	public BossLair(int bossId, Location teleport)
	{
		_bossId = bossId;
		_instanceId = BossSpawnManager.BOSS_INSTANCE_OFFSET + bossId;
		if (_instanceId != BossSpawnManager.BOSS_INSTANCE_OFFSET)
			InstanceManager.getInstance().createInstance(_instanceId);
		_bossSpawn = null;
		_teleport = teleport;
	}
	
	public BossLair(int bossId)
	{
		this(bossId, null);
	}
	
	public abstract void init();
	
	public abstract void setUnspawn();
	
	protected GrandBossState _state;
	protected String _questName;
	
	public GrandBossState.StateEnum getState()
	{
		return _state.getState();
	}
	
	public boolean isEnableEnterToLair()
	{
		switch (getState())
		{
		case INTERVAL:
		case DEAD:
			return false;
		case NOTSPAWN:
			return true;
		default:
			L2Npc npc = getBossSpawn();
			if (npc == null)
				return true;
			double percent = npc.getCurrentHp() * 100 / npc.getMaxHp();
			L2PcInstance leader = getLeader(_bossId);
			return (percent > 5 || leader == null);
		}
	}
	
	public synchronized boolean isPlayersAnnihilated()
	{
		for (L2PcInstance pc : getPlayersInside())
		{
			if (!pc.isDead())
				return false;
		}
		return true;
	}
	
	public void checkAnnihilated()
	{
		if (isPlayersAnnihilated())
		{
			ThreadPoolManager.getInstance().scheduleGeneral(new Runnable()
			{
				@Override
				public void run()
				{
					setUnspawn();
				}
			}, 5000);
		}
	}
	
	@Override
	public void banishForeigners()
	{
		for (L2PcInstance player : getPlayersInside())
		{
			if (_questName != null)
			{
				QuestState qs = player.getQuestState(_questName);
				if (qs != null)
					qs.exitQuest(true);
			}
			// player.teleToLocation(TeleportWhereType.Town);
			RespawnManager.getInstance().moveToSpawn(player);
		}
	}
	
	public static final FastMap<Integer, FastSet<L2Party>> getRaiders()
	{
		return _raiders;
	}
	
	public static final void addRaider(Integer raidId, L2Party party)
	{
		if (party == null || party.getMemberCount() < 2)
			return;
		party.setRaid(raidId);
		getParties(raidId).add(party);
	}
	
	public static final void remRaider(L2Party party)
	{
		if (party == null || party.getRaid() == null)
			return;
		getParties(party.getRaid()).remove(party);
		party.setRaid(null);
	}
	
	public static final FastSet<L2Party> getParties(Integer raidId)
	{
		FastSet<L2Party> result = getRaiders().get(raidId);
		if (result == null)
		{
			result = new FastSet<L2Party>();
			getRaiders().put(raidId, result);
		}
		return result;
	}
	
	public static final L2PcInstance getLeader(Integer raidId)
	{
		FastSet<L2Party> set = getParties(raidId);
		for (FastSet.Record r = set.head(), end = set.tail(); (r = r.getNext()) != end;)
		{
			L2Party p = set.valueOf(r);
			if (p != null)
				return p.getLeader();
		}
		return null;
	}
	
	public final int getInstanceId()
	{
		return _instanceId;
	}
	
	public final L2Npc getBossSpawn()
	{
		return _bossSpawn;
	}
	
	public final Location getTeleport()
	{
		return _teleport;
	}
	
	public void prepareBoss()
	{
	}
	
	public abstract int getTimeToAppearance();
}
