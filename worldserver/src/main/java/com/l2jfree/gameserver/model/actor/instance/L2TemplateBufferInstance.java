/*
 * MISSING LICENSING INFO
 */
package com.l2jfree.gameserver.model.actor.instance;

import static com.l2jfree.gameserver.network.SystemMessageId.NOTHING_HAPPENED;
import static org.sod.buffs.BuffTemplateType.FIGHTER;
import static org.sod.buffs.BuffTemplateType.MYSTIC;

import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import org.sod.L2Htm;
import org.sod.buffs.BuffManager;
import org.sod.buffs.BuffSkill;
import org.sod.buffs.BuffTemplate;
import org.sod.buffs.BuffTemplateType;
import org.sod.buffs.PlayerBuffs;
import org.sod.i18n.Internationalization;
import org.sod.model.L2PlayerData;

import com.l2jfree.Config;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.zone.L2Zone;
import com.l2jfree.gameserver.network.SystemMessageId;
import com.l2jfree.gameserver.network.client.packets.sendable.NpcHtmlMessage;
import com.l2jfree.gameserver.network.client.packets.sendable.SystemMessagePacket.SystemMessage;
import com.l2jfree.gameserver.skills.Stats;
import com.l2jfree.gameserver.taskmanager.AttackStanceTaskManager;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;
import com.l2jfree.lang.L2TextBuilder;

/**
 * @author savormix
 * 
 */
public class L2TemplateBufferInstance extends L2Npc
{
	private static String CHAT;
	private static String MANAGE;
	private static String CREATE;
	private static String BADNAME;
	private static String TEMPLATE;
	private static String[] BUFFS;
	private static String[] SPECIAL;
	
	static
	{
		init();
	}
	
	public static void init()
	{
		L2TextBuilder tb = new L2TextBuilder("<html><title>%NPC_BUFFER_TITLE%</title><body><center>");
		{
			tb.append("<font color=\"LEVEL\">%serverName%</font><br1>");
			writeButton(tb, PlayerRequest.MANAGE_TEMPLATES, 250, 50, true, "%NPC_BUFFER_SET_UP%");
			tb.append(getSeparator());
			tb.append("%buffWithActive%");
			writeButton(tb, PlayerRequest.RECOVER_ALL, 250, 50, false, "%NPC_BUFFER_RECOVER_ALL%");
			writeButton(tb, PlayerRequest.CANCEL_BUFFS, 250, 50, false, "%NPC_BUFFER_CANCEL_BUFFS%");
			tb.append("</center></body></html>");
			CHAT = tb.toString();
			tb.setLength(0);
		}
		{
			tb.append("<html><title>%NPC_BUFFER_TITLE%</title><body><center>");
			tb.append("<font color=\"LEVEL\">%serverName%</font><br1>");
			writeButton(tb, PlayerRequest.BACK_TO_MAIN, 250, 30, true, "%BACK%");
			tb.append(getSeparator());
			tb.append("%createTemplate%");
			tb.append("%templates%");
			tb.append("</center></body></html>");
			MANAGE = tb.toString();
			tb.setLength(0);
		}
		{
			tb.append("<html><title>%NPC_BUFFER_TITLE%</title><body><center>");
			tb.append("<font color=\"LEVEL\">%serverName%</font><br1>");
			writeButton(tb, PlayerRequest.MANAGE_TEMPLATES, 250, 30, true, "%BACK%");
			tb.append(getSeparator());
			tb.append("<br><table width=\"270\">");
			tb.append("<tr><td fixWIDTH=\"80\">%NPC_BUFFER_TEMPLATE_NAME%</td><td><edit var=\"name\" width=\"190\" length=\"16\"></td></tr>");
			tb.append("<tr><td fixWIDTH=\"80\">%NPC_BUFFER_TEMPLATE_TYPE%</td><td><combobox width=\"190\" var=\"type\" list=\"%NPC_BUFFER_TEMPLATE_TYPE_FIGHTER%;%NPC_BUFFER_TEMPLATE_TYPE_MYSTIC%;%NPC_BUFFER_TEMPLATE_TYPE_SERVITOR%\"></td></tr>");
			tb.append("</table><br>");
			writeButton(tb, PlayerRequest.CREATE_TEMPLATE, "$type $name", 250, 30, true, "%NPC_BUFFER_CREATE_TEMPLATE%");
			tb.append("</center></body></html>");
			CREATE = tb.toString();
			tb.setLength(0);
		}
		{
			tb.append("<html><title>%NPC_BUFFER_TITLE%</title><body><center>");
			tb.append("<font color=\"LEVEL\">%serverName%</font><br1>");
			tb.append("Invalid name specified.");
			writeButton(tb, PlayerRequest.CREATE_TEMPLATE_MENU, 250, 30, true, "%BACK%");
			tb.append("</center></body></html>");
			BADNAME = tb.toString();
			tb.setLength(0);
		}
		BUFFS = new String[BuffTemplateType.values().length];
		SPECIAL = new String[BUFFS.length];
		for (int i = 0; i < BUFFS.length; i++)
		{
			BuffTemplateType btt = BuffTemplateType.values()[i];
			
			tb.append("<html><title>%NPC_BUFFER_TITLE%</title><body><center>");
			writeButton(tb, PlayerRequest.SHOW_TEMPLATE, "%templateId%", 250, 20, true, "%BACK%");
			tb.append("<table width=\"270\">");
			List<BuffSkill> buffs = btt.getBuffs();
			if (Config.BUFFER_SKILL_ICONS)
			{
				for (int j = 0; j < buffs.size(); j++)
				{
					L2Skill sk = buffs.get(j).getSkill();
					tb.append("<tr><td>");
					StringBuilder sb = new StringBuilder("icon.skill");
					{
						String skill = String.valueOf(sk.getDisplayId());
						for (int k = skill.length(); k < 4; k++)
							sb.append('0');
						sb.append(skill);
					}
					L2Htm._writeImage(tb, sb.toString(), 32, 32);
					tb.append("</td><td>");
					_writeButton(tb, PlayerRequest.ADD_BUFF, "%templateId% " + j, 240, 32, true, sk.getName());
					tb.append("</td></tr>");
				}
			}
			else
			{
				for (int j = 0; j < buffs.size(); j++)
				{
					L2Skill sk = buffs.get(j).getSkill();
					boolean start = ((j & 1) == 0);
					if (start)
						tb.append("<tr>");
					tb.append("<td>");
					writeButton(tb, PlayerRequest.ADD_BUFF, "%templateId% " + j, 127, 20, true, sk.getName());
					tb.append("</td>");
					if (!start)
						tb.append("</tr>");
				}
				if (tb.charAt(tb.length() - 2) != 'r')
					tb.append("</tr>");
			}
			tb.append("</table></center></body></html>");
			
			BUFFS[i] = tb.toString();
			tb.setLength(0);
			
			tb.append("<html><title>%NPC_BUFFER_TITLE%</title><body><center>");
			writeButton(tb, PlayerRequest.SHOW_TEMPLATE, "%templateId%", 250, 20, true, "%BACK%");
			tb.append("<table width=\"270\">");
			buffs = btt.getDanceSongs();
			if (Config.BUFFER_SKILL_ICONS)
			{
				for (int j = 0; j < buffs.size(); j++)
				{
					L2Skill sk = buffs.get(j).getSkill();
					tb.append("<tr><td>");
					StringBuilder sb = new StringBuilder("icon.skill");
					{
						String skill = String.valueOf(sk.getDisplayId());
						for (int k = skill.length(); k < 4; k++)
							sb.append('0');
						sb.append(skill);
					}
					L2Htm._writeImage(tb, sb.toString(), 32, 32);
					tb.append("</td><td>");
					_writeButton(tb, PlayerRequest.ADD_DANCE_SONG, "%templateId% " + j, 240, 32, true, sk.getName());
					tb.append("</td></tr>");
				}
			}
			else
			{
				for (int j = 0; j < buffs.size(); j++)
				{
					L2Skill sk = buffs.get(j).getSkill();
					boolean start = ((j & 1) == 0);
					if (start)
						tb.append("<tr>");
					tb.append("<td>");
					writeButton(tb, PlayerRequest.ADD_DANCE_SONG, "%templateId% " + j, 127, 20, true, sk.getName());
					tb.append("</td>");
					if (!start)
						tb.append("</tr>");
				}
				if (tb.charAt(tb.length() - 2) != 'r')
					tb.append("</tr>");
			}
			tb.append("</table></center></body></html>");
			
			SPECIAL[i] = tb.toString();
			tb.setLength(0);
		}
		{
			tb.append("<html><title>%NPC_BUFFER_TITLE%</title><body><center>");
			writeButton(tb, PlayerRequest.MANAGE_TEMPLATES, 250, 20, true, "%BACK%");
			tb.append("<font color=\"LEVEL\">%templateName%</font><table>");
			tb.append("<tr><td>");
			writeButton(tb, PlayerRequest.DELETE_TEMPLATE, "%templateId%", 127, 20, true,
					"%NPC_BUFFER_DELETE_TEMPLATE%");
			tb.append("</td><td>");
			writeButton(tb, PlayerRequest.CLEAR_TEMPLATE, "%templateId%", 127, 20, true, "%NPC_BUFFER_CLEAR_TEMPLATE%");
			tb.append("</td></tr></table>");
			tb.append("%addBuffs%");
			tb.append("%removeBuffs%");
			tb.append("</center></body></html>");
			TEMPLATE = tb.moveToString();
		}
	}
	
	private static String getSeparator()
	{
		return "<img src=\"L2UI_ct1.gauge_df_attribute_divine\" width=\"270\" height=\"7\">";
	}
	
	private static void writeButton(L2TextBuilder tb, PlayerRequest event, int w, int h, boolean hide, String... value)
	{
		writeButton(tb, event, null, w, h, hide, value);
	}
	
	private static void writeButton(L2TextBuilder tb, PlayerRequest event, Object subEvent, int w, int h, boolean hide,
			String... value)
	{
		tb.append("<button value=\"");
		for (String val : value)
			tb.append(val);
		tb.append("\" action=\"bypass ");
		if (hide)
			tb.append("-h ");
		tb.append("npc_%objectId%_");
		tb.append(event.ordinal());
		if (subEvent != null)
		{
			tb.append(' ');
			tb.append(subEvent);
		}
		tb.append("\" width=\"");
		tb.append(w);
		tb.append("\" height=\"");
		tb.append(h);
		tb.append("\" back=\"L2UI_ct1.button_df_down\" fore=\"L2UI_ct1.button_df\">");
	}
	
	private static void _writeButton(L2TextBuilder tb, PlayerRequest event, Object subEvent, int w, int h,
			boolean hide, String... value)
	{
		tb.append("<button value=\"");
		for (String val : value)
			tb.append(val);
		tb.append("\" action=\"bypass ");
		if (hide)
			tb.append("-h ");
		tb.append("npc_%objectId%_");
		tb.append(event.ordinal());
		if (subEvent != null)
		{
			tb.append(' ');
			tb.append(subEvent);
		}
		tb.append("\" width=");
		tb.append(w);
		tb.append(" height=");
		tb.append(h);
		tb.append(" fore=L2UI_ct1.button_df>");
	}
	
	public L2TemplateBufferInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}
	
	@Override
	public final void showChatWindow(L2PcInstance player, int page)
	{
		// init();
		L2PlayerData dat = player.getSoDPlayer();
		Locale loc = dat.getSettings().getLocale();
		Internationalization i18n = Internationalization.getInstance();
		
		NpcHtmlMessage htm = new NpcHtmlMessage(this, CHAT);
		PlayerBuffs pb = dat.getBuffs();
		if (pb.getActive(player.isMageClass() ? MYSTIC : FIGHTER) != null)
		{
			L2TextBuilder tb = new L2TextBuilder();
			writeButton(tb, PlayerRequest.BUFF_WITH_ACTIVE, 270, 50, false, i18n.get(loc, "NPC_BUFFER_BUFF"));
			htm.replace("%buffWithActive%", tb.moveToString());
		}
		else
			htm.replace("%buffWithActive%", "");
		i18n.localize(htm, loc, "%NPC_BUFFER_TITLE%", "%NPC_BUFFER_SET_UP%", "%NPC_BUFFER_RECOVER_ALL%",
				"%NPC_BUFFER_CANCEL_BUFFS%");
		htm.replace("%serverName%", Config.SERVER_NAME);
		htm.replace("%objectId%", getObjectId());
		player.sendPacket(htm);
	}
	
	@Override
	public void onBypassFeedback(L2PcInstance player, String command)
	{
		StringTokenizer st = new StringTokenizer(command, " ");
		PlayerRequest pr = L2Htm.getRequest(st, PlayerRequest.values());
		if (pr == null)
			return;
		
		L2PlayerData dat = player.getSoDPlayer();
		switch (pr)
		{
		case MANAGE_TEMPLATES:
			showManageWindow(dat);
			break;
		case RECOVER_ALL:
			recover(player);
			break;
		case CANCEL_BUFFS:
			player.stopAllEffectsExceptThoseThatLastThroughDeath();
			break;
		case BUFF_WITH_ACTIVE:
			BuffManager.getInstance().buff(dat);
			break;
		case BACK_TO_MAIN:
			showChatWindow(player);
			break;
		case CREATE_TEMPLATE_MENU:
			showCreateWindow(dat);
			break;
		case CREATE_TEMPLATE:
			tryCreateTemplate(dat, st);
			break;
		case SHOW_TEMPLATE:
			showTemplate(dat, st);
			break;
		case CLEAR_TEMPLATE:
			clearTemplate(dat, st);
			break;
		case DELETE_TEMPLATE:
			deleteTemplate(dat, st);
			break;
		case REMOVE_BUFF:
			removeBuff(dat, st);
			break;
		case SELECT_BUFF_MENU:
		case SELECT_DANCE_SONG_MENU:
			showSelectBuff(dat, st, pr == PlayerRequest.SELECT_DANCE_SONG_MENU);
			break;
		case ADD_BUFF:
		case ADD_DANCE_SONG:
			addBuff(dat, st, pr == PlayerRequest.ADD_DANCE_SONG);
			break;
		default:
			player.sendPacket(SystemMessageId.NOT_WORKING_PLEASE_TRY_AGAIN_LATER);
			break;
		}
	}
	
	private void recover(L2PcInstance player)
	{
		if (player.isDead() || player.isInsideZone(L2Zone.FLAG_NOHEAL))
			player.sendPacket(NOTHING_HAPPENED);
		else if (AttackStanceTaskManager.getInstance().getAttackStanceTask(player) || player.getKarma() > 0)
		{
			if (player.getCurrentCp() <= 3000)
				player.getStatus().setCurrentCp(1);
			else
				player.getStatus().reduceCp(3000);
			player.sendPacket(SystemMessageId.INDIVIDUALS_NOT_SURRENDER_DURING_COMBAT);
		}
		else
		{
			player.getStatus().setCurrentCp(player.getMaxCp());
			player.getStatus().setCurrentHp(player.getMaxHp());
			player.getStatus().setCurrentMp(player.getMaxMp());
			
			SystemMessage sm = new SystemMessage(SystemMessageId.UNNAMED_S1_2369);
			sm.addNpcName(this);
			player.sendPacket(sm);
		}
	}
	
	private void showManageWindow(L2PlayerData dat)
	{
		NpcHtmlMessage htm = new NpcHtmlMessage(this, MANAGE);
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = dat.getSettings().getLocale();
		
		PlayerBuffs pb = dat.getBuffs();
		L2TextBuilder tb = new L2TextBuilder();
		for (Integer id : pb.getTemplates())
		{
			BuffTemplate bt = BuffManager.getInstance().getTemplate(id);
			boolean active = pb.getActive(bt.getType()) == id;
			writeButton(tb, PlayerRequest.SHOW_TEMPLATE, id, 250, 30, true, i18n.get(loc, "NPC_BUFFER_MANAGE_TEMPLATE",
					"%templateName%", bt.getName()), " (", bt.getType().toString(loc),
					active ? (", " + i18n.get(loc, "NPC_BUFFER_ACTIVE_TEMPLATE")) : "", ")");
		}
		htm.replace("%templates%", tb.moveToString());
		if (pb.getTemplates().isEmpty())
			htm.replace("%templates%", "");
		
		if (pb.getTemplates().size() < Config.BUFFER_MAX_TEMPLATES)
		{
			tb = new L2TextBuilder();
			writeButton(tb, PlayerRequest.CREATE_TEMPLATE_MENU, 250, 30, true,
					i18n.get(loc, "NPC_BUFFER_CREATE_TEMPLATE"));
			tb.append(getSeparator());
			htm.replace("%createTemplate%", tb.moveToString());
		}
		else
			htm.replace("%createTemplate%", "");
		
		i18n.localize(htm, loc, "%NPC_BUFFER_TITLE%", "%BACK%");
		htm.replace("%serverName%", Config.SERVER_NAME);
		htm.replace("%objectId%", getObjectId());
		htm.replace("<table>", "");
		htm.replace("</table>", "");
		dat.getActingPlayer().sendPacket(htm);
	}
	
	private void showCreateWindow(L2PlayerData dat)
	{
		NpcHtmlMessage htm = new NpcHtmlMessage(this, CREATE);
		Internationalization.getInstance().localize(htm, dat.getSettings().getLocale(), "%NPC_BUFFER_TITLE%",
				"%NPC_BUFFER_TEMPLATE_NAME%", "%NPC_BUFFER_TEMPLATE_TYPE%", "%NPC_BUFFER_TEMPLATE_TYPE_FIGHTER%",
				"%NPC_BUFFER_TEMPLATE_TYPE_MYSTIC%", "%NPC_BUFFER_TEMPLATE_TYPE_SERVITOR%",
				"%NPC_BUFFER_CREATE_TEMPLATE%", "%BACK%");
		htm.replace("%serverName%", Config.SERVER_NAME);
		htm.replace("%objectId%", getObjectId());
		dat.getActingPlayer().sendPacket(htm);
	}
	
	private void tryCreateTemplate(L2PlayerData dat, StringTokenizer st)
	{
		PlayerBuffs pb = dat.getBuffs();
		if (pb.getTemplates().size() >= Config.BUFFER_MAX_TEMPLATES)
			return;
		
		String name, type;
		try
		{
			type = st.nextToken();
			if (st.hasMoreTokens())
				name = st.nextToken("\t").substring(1);
			else
				name = null;
		}
		catch (RuntimeException e)
		{
			return;
		}
		
		Locale loc = dat.getSettings().getLocale();
		if (name == null || !Config.BUFFER_NAMING_TEMPLATE.matcher(name).matches())
		{
			NpcHtmlMessage htm = new NpcHtmlMessage(this, BADNAME);
			Internationalization.getInstance().localize(htm, loc, "%NPC_BUFFER_TITLE%", "%BACK%");
			htm.replace("%serverName%", Config.SERVER_NAME);
			htm.replace("%objectId%", getObjectId());
			dat.getActingPlayer().sendPacket(htm);
			return;
		}
		
		BuffTemplateType real = null;
		for (BuffTemplateType btt : BuffTemplateType.values())
			if (btt.toString(loc).equals(type))
				real = btt;
		if (real == null)
			return;
		
		Integer id = BuffManager.getInstance().add(dat.getObjectId(), real, name);
		if (id != null)
		{
			pb.getTemplates().add(id);
			if (pb.getActive(real) == null)
				BuffManager.getInstance().setActive(dat, id);
		}
		
		showManageWindow(dat);
	}
	
	private Integer parseAndCheckId(L2PlayerData dat, StringTokenizer st)
	{
		Integer id;
		try
		{
			id = Integer.valueOf(st.nextToken());
		}
		catch (RuntimeException e)
		{
			return null;
		}
		
		if (!dat.getBuffs().getTemplates().contains(id))
			return null;
		
		return id;
	}
	
	private void showTemplate(L2PlayerData dat, StringTokenizer st)
	{
		Integer id = parseAndCheckId(dat, st);
		if (id != null)
		{
			showTemplate(dat, id);
			BuffManager.getInstance().setActive(dat, id);
		}
	}
	
	private void showTemplate(L2PlayerData dat, Integer id)
	{
		if (id == null)
			return;
		
		PlayerBuffs pb = dat.getBuffs();
		if (!pb.getTemplates().contains(id))
			return;
		
		BuffTemplate bt = BuffManager.getInstance().getTemplate(id);
		if (bt == null)
			return;
		BuffTemplateType btt = bt.getType();
		
		NpcHtmlMessage htm = new NpcHtmlMessage(this, TEMPLATE);
		htm.replace("%templateId%", id);
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = dat.getSettings().getLocale();
		L2TextBuilder tb = new L2TextBuilder();
		{
			int openSpec = btt.getDanceSongSlots() - bt.getDanceSongCount();
			int openShared = btt.getSharedSlots()
					+ (int) dat.getActingPlayer().calcStat(Stats.NPC_BUFFER_SLOTS, 0, null, null);
			if (openSpec < 0)
				openShared += openSpec;
			int openNorm = btt.getBuffSlots() - bt.getNormalBuffCount();
			if (openNorm < 0)
				openShared += openNorm;
			if (openShared > 0 || openSpec > 0 || openNorm > 0)
			{
				tb.append(getSeparator());
			}
			if (openShared > 0 || openNorm > 0)
			{
				writeButton(tb, PlayerRequest.SELECT_BUFF_MENU, id, 250, 20, true, "[+] ",
						i18n.get(loc, "NPC_BUFFER_ADD_BUFF"), " (", String.valueOf(Math.max(0, openNorm)), "+",
						String.valueOf(Math.max(0, openShared)), ") [+]");
			}
			if (openShared > 0 || openSpec > 0)
			{
				writeButton(tb, PlayerRequest.SELECT_DANCE_SONG_MENU, id, 250, 20, true, "[+] ",
						i18n.get(loc, "NPC_BUFFER_ADD_DANCE_SONG"), " (", String.valueOf(Math.max(0, openSpec)), "+",
						String.valueOf(Math.max(0, openShared)), ") [+]");
			}
		}
		tb.append(getSeparator());
		htm.replace("%addBuffs%", tb.toString());
		tb.setLength(0);
		
		for (BuffSkill bs : bt.getBuffs())
		{
			L2Skill sk = bs.getSkill();
			writeButton(tb, PlayerRequest.REMOVE_BUFF, id + " " + sk.getId() + " " + sk.getLevel(), 250, 20, true,
					"[-] ", i18n.get(loc, "NPC_BUFFER_REMOVE_BUFF", "%skillName%", sk.getName()), " [-]");
		}
		htm.replace("%removeBuffs%", tb.moveToString());
		
		i18n.localize(htm, loc, "%NPC_BUFFER_TITLE%", "%BACK%", "%NPC_BUFFER_DELETE_TEMPLATE%",
				"%NPC_BUFFER_CLEAR_TEMPLATE%");
		
		htm.replace("%templateName%", bt.getName());
		htm.replace("%objectId%", getObjectId());
		dat.getActingPlayer().sendPacket(htm);
	}
	
	private void clearTemplate(L2PlayerData dat, StringTokenizer st)
	{
		Integer id = parseAndCheckId(dat, st);
		if (id == null)
			return;
		
		if (BuffManager.getInstance().reset(id))
			showTemplate(dat, id);
	}
	
	private void deleteTemplate(L2PlayerData dat, StringTokenizer st)
	{
		Integer id = parseAndCheckId(dat, st);
		if (id == null)
			return;
		
		PlayerBuffs pb = dat.getBuffs();
		pb.getTemplates().remove(id);
		BuffManager bm = BuffManager.getInstance();
		BuffTemplate bt = bm.getTemplate(id);
		if (pb.getActive(bt.getType()) == id)
		{
			pb.getActive()[bt.getType().ordinal()] = null;
			for (Integer tmp : pb.getTemplates())
			{
				BuffTemplate nbt = bm.getTemplate(tmp);
				if (nbt.getType() == bt.getType())
				{
					pb.getActive()[bt.getType().ordinal()] = tmp;
					break;
				}
			}
		}
		if (bm.remove(id))
			showManageWindow(dat);
	}
	
	private void removeBuff(L2PlayerData dat, StringTokenizer st)
	{
		Integer id = parseAndCheckId(dat, st);
		if (id == null)
			return;
		
		int skill, level;
		try
		{
			skill = Integer.parseInt(st.nextToken());
			level = Integer.parseInt(st.nextToken());
		}
		catch (RuntimeException e)
		{
			return;
		}
		
		if (BuffManager.getInstance().remove(id, skill, level))
			showTemplate(dat, id);
	}
	
	private void showSelectBuff(L2PlayerData dat, StringTokenizer st, boolean danceSong)
	{
		Integer id = parseAndCheckId(dat, st);
		if (id == null)
			return;
		
		BuffTemplate bt = BuffManager.getInstance().getTemplate(id);
		String[] s = danceSong ? SPECIAL : BUFFS;
		NpcHtmlMessage htm = new NpcHtmlMessage(this, s[bt.getType().ordinal()]);
		Internationalization.getInstance().localize(htm, dat.getSettings().getLocale(), "%NPC_BUFFER_TITLE%", "%BACK%");
		htm.replace("%templateId%", id);
		htm.replace("%objectId%", getObjectId());
		dat.getActingPlayer().sendPacket(htm);
	}
	
	private void addBuff(L2PlayerData dat, StringTokenizer st, boolean danceSong)
	{
		Integer id = parseAndCheckId(dat, st);
		if (id == null)
			return;
		
		int idx;
		try
		{
			idx = Integer.parseInt(st.nextToken());
		}
		catch (RuntimeException e)
		{
			return;
		}
		BuffTemplate bt = BuffManager.getInstance().getTemplate(id);
		List<BuffSkill> buffs = danceSong ? bt.getType().getDanceSongs() : bt.getType().getBuffs();
		if (idx < 0 || idx >= buffs.size())
			return;
		
		if (BuffManager.getInstance().add(id, buffs.get(idx)))
			showTemplate(dat, id);
	}
	
	private enum PlayerRequest
	{
		MANAGE_TEMPLATES, RECOVER_ALL, CANCEL_BUFFS, BUFF_WITH_ACTIVE, BACK_TO_MAIN, SHOW_TEMPLATE, CREATE_TEMPLATE_MENU, CREATE_TEMPLATE, DELETE_TEMPLATE, CLEAR_TEMPLATE, ADD_BUFF, ADD_DANCE_SONG, REMOVE_BUFF, SELECT_BUFF_MENU, SELECT_DANCE_SONG_MENU;
	}
}
