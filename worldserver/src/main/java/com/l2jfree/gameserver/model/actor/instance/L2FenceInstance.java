/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.model.actor.instance;

import org.sod.manager.EventFenceManager.FenceInfo;

import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.knownlist.CharKnownList;
import com.l2jfree.gameserver.model.actor.knownlist.FenceKnownList;
import com.l2jfree.gameserver.network.serverpackets.ExColosseumFenceInfo;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;

/**
 * A rectangular fence.
 * @author savormix
 * @since 2010.12.07
 */
public class L2FenceInstance extends L2Npc
{
	private FenceInfo _fence;
	
	public L2FenceInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
		_fence = null;
	}
	
	public FenceInfo getFence()
	{
		return _fence;
	}
	
	public void setFence(FenceInfo fence)
	{
		_fence = fence;
	}
	
	@Override
	protected CharKnownList initKnownList()
	{
		return new FenceKnownList(this);
	}
	
	@Override
	public FenceKnownList getKnownList()
	{
		return (FenceKnownList) _knownList;
	}
	
	@Override
	public boolean hasRandomAnimation()
	{
		return false;
	}

	@Override
	public void onSpawn()
	{
		setIsNoRndWalk(true);
		super.onSpawn();
	}

	@Override
	public void sendInfo(L2PcInstance activeChar)
	{
		if (getFence() != null)
			activeChar.sendPacket(new ExColosseumFenceInfo(this));
	}

	@Override
	public void broadcastFullInfoImpl()
	{
		if (getFence() != null)
			broadcastPacket(new ExColosseumFenceInfo(this));
	}

	@Override
	public void showChatWindow(L2PcInstance player, int page)
	{
	}
}
