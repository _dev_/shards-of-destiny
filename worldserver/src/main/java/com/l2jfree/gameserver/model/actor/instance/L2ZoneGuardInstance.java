/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.model.actor.instance;

import org.sod.model.ChallengeTemplate;
import org.sod.manager.conquest.ConquestFactionManager;
import org.sod.model.conquest.ConquestZoneMember;

import com.l2jfree.gameserver.model.L2Object;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.L2Playable;
import com.l2jfree.gameserver.model.zone.L2FactionZone;
import com.l2jfree.gameserver.network.SystemChatChannelId;
import com.l2jfree.gameserver.network.serverpackets.CreatureSay;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;

/**
 * TODO Auto-generated JavaDoc
 * @author savormix
 * @since 2009.12.30
 */
public class L2ZoneGuardInstance extends L2MonsterInstance implements ConquestZoneMember
{
	private L2FactionZone _zone;

	public L2ZoneGuardInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
		setIsInvul(false);
	}

	@Override
	public final int getConquestFaction()
	{
		if (_zone == null)
			return ConquestFactionManager.NONE;
		else
			return _zone.getConquestFaction();
	}

	@Override
	public final L2FactionZone getFactionZone()
	{
		return _zone;
	}

	@Override
	public final void setFactionZone(L2FactionZone zone)
	{
		_zone = zone;
	}

	@Override
	public boolean doDie(L2Character killer)
	{
		if (!super.doDie(killer))
			return false;

		L2FactionZone zone = getFactionZone();
		if (zone != null)
		{
			boolean servitor = this instanceof L2ZoneServitorInstance;
			if (servitor)
			{
				getSpawn().stopRespawn();
				zone.servitorDied();
				setFactionZone(null);
			}
			L2PcInstance killOwner = L2Object.getActingPlayer(killer);
			if (killOwner != null)
			{
				killOwner.getSoDPlayer().tryAddChallengePoints(ChallengeTemplate.SPARTAN, 1);
			}
			if (!servitor && zone.tryAlert())
				ConquestFactionManager.getInstance().getFactionTemplate(
						zone.getConquestFaction()).broadcast(
					new CreatureSay(0, SystemChatChannelId.Chat_Battlefield, zone.getName(),
							"Intruder alert!"));
		}
		return true;
	}

	@Override
	public boolean isAggressive()
	{
		return true;
	}

	@Override
	public final boolean isAutoAttackable(L2Character attacker)
	{
		if (getConquestFaction() == ConquestFactionManager.NONE ||
				!(attacker instanceof L2Playable))
			return false;

		return !ConquestFactionManager.getInstance().isSameFaction(this, attacker);
	}

	@Override
	public void showChatWindow(L2PcInstance player, int page)
	{
	}
}
