/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.model.actor.instance;

import org.sod.manager.conquest.ConquestMercenaryManager;

import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;

/**
 * TODO Auto-generated JavaDoc
 * @author savormix
 * @since 2009.02.06
 */
public class L2ZoneHirelingInstance extends L2ZoneGuardInstance
{
	public L2ZoneHirelingInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
		setIsInvul(false);
	}

	@Override
	public final boolean doDie(L2Character killer)
	{
		if (!super.doDie(killer))
			return false;

		getSpawn().stopRespawn();
		ConquestMercenaryManager.getInstance().remPosition(this);
		return true;
	}
}
