/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.model.actor.instance;

import javolution.util.FastMap;

import org.sod.Punishment;
import org.sod.Skills;
import org.sod.manager.GrandBossIntegrationManager;
import org.sod.manager.LootedGoodsManager;
import org.sod.model.L2PlayerData;

import com.l2jfree.gameserver.datatables.SkillTable;
import com.l2jfree.gameserver.instancemanager.BossSpawnManager;
import com.l2jfree.gameserver.instancemanager.BossSpawnManager.StatusEnum;
import com.l2jfree.gameserver.instancemanager.GrandBossSpawnManager;
import com.l2jfree.gameserver.instancemanager.RaidBossSpawnManager;
import com.l2jfree.gameserver.model.L2Party;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.L2Spawn;
import com.l2jfree.gameserver.model.L2World;
import com.l2jfree.gameserver.model.Location;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.restriction.global.GlobalRestrictions;
import com.l2jfree.gameserver.network.SystemMessageId;
import com.l2jfree.gameserver.network.client.packets.sendable.NpcHtmlMessage;
import com.l2jfree.gameserver.network.client.packets.sendable.TutorialCloseHtmlPacket.HideTutorialHtml;
import com.l2jfree.gameserver.network.client.packets.sendable.TutorialShowHtml.ShowTutorialHtml;
import com.l2jfree.gameserver.network.client.packets.sendable.TutorialShowQuestionMarkPacket.ShowTutorialMark;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;
import com.l2jfree.lang.L2TextBuilder;
import com.l2jfree.util.Rnd;

/**
 * @author Horus & savormix
 * 
 */
public final class L2RaidTeleporterInstance extends L2Npc
{
	private static final String[] CMD =
	{
			"tele", "ask", "allow", "deny"
	};
	private static final int UPDATE_DELAY = 5000;
	private static volatile long VALIDITY = 0;
	private static volatile NpcHtmlMessage CHAT = new NpcHtmlMessage();
	public static final ShowTutorialHtml INVALID_REQUEST = new ShowTutorialHtml(
			"<html><body>This request has already expired.<br><a action=\"link close\">Close</a></body></html>");
	public static final ShowTutorialHtml INVALIDATED_REQUEST = new ShowTutorialHtml(
			"<html><body>Conditions have changed since you issued a raid request, so it was denied.<br><a action=\"link close\">Close</a></body></html>");
	public static final ShowTutorialHtml DENIED_REQUEST = new ShowTutorialHtml(
			"<html><body>Your raid request has been denied.<br><a action=\"link close\">Close</a></body></html>");
	public static final ShowTutorialHtml VIEWED_REQUEST = new ShowTutorialHtml(
			"<html><body>Your raid request is currently being viewed by the raiding party leader.<br><a action=\"link close\">Close</a></body></html>");
	private static final FastMap<Integer, Integer> _pending = new FastMap<Integer, Integer>().setShared(true);
	
	public L2RaidTeleporterInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}
	
	@Override
	public void showChatWindow(L2PcInstance player, int page)
	{
		if (System.currentTimeMillis() > VALIDITY)
		{
			VALIDITY = System.currentTimeMillis() + UPDATE_DELAY;
			buildChat();
		}
		player.sendPacket(CHAT);
	}
	
	public static final void handleBypass(String cmd, L2PcInstance player)
	{
		if (cmd.startsWith(CMD[2]))
			doReqTele(player, cmd);
		else if (cmd.startsWith(CMD[3]))
			doRefuse(player, cmd);
		
		if (player.isAlikeDead())
			return;
		L2Npc transObj = player.getLastFolkNPC();
		if (!transObj.canInteract(player) || !GlobalRestrictions.canTeleport(player))
			return;
		
		if (cmd.startsWith(CMD[0]))
			doTeleport(player, cmd);
		else if (cmd.startsWith(CMD[1]))
			doRequest(player, cmd);
	}
	
	private static void doRequest(L2PcInstance player, String command)
	{
		String[] cmd = command.split(" ");
		if (cmd.length < 2)
		{
			Punishment.handleIllegalPlayerAction(player, "Raid Manager exploitation 3 (bypass not validated??!).");
			return;
		}
		Integer raid = -1;
		try
		{
			raid = Integer.valueOf(cmd[1]);
		}
		catch (Exception e)
		{
			Punishment.handleIllegalPlayerAction(player, "Raid Manager exploitation 2 (bypass not validated??!).");
			return;
		}
		L2Party party = player.getParty();
		if (party != null && !party.isLeader(player))
		{
			player.sendPacket(SystemMessageId.ONLY_PARTY_LEADER_CAN_ENTER);
			return;
		}
		
		if (GrandBossIntegrationManager.getInstance().requestEntry(player, party, raid))
			return;
		
		BossSpawnManager bsm = RaidBossSpawnManager.getInstance();
		if (!bsm.isDefined(raid))
			bsm = GrandBossSpawnManager.getInstance();
		if (bsm.getRaidBossStatusId(raid) != StatusEnum.ALIVE)
		{
			player.sendPacket(SystemMessageId.PLEASE_TRY_AGAIN_LATER);
			return;
		}
		L2PcInstance leader = BossSpawnManager.getLeader(raid);
		if (leader == null)
		{
			player.sendMessage("There is no leader to request.");
			return;
		}
		L2Npc npc = bsm.getBosses().get(raid);
		double percent = npc.getCurrentHp() * 100 / npc.getMaxHp();
		if (percent < 5)
		{
			player.sendMessage("Currently one or more parties are finishing off the raid boss, so you may not enter.");
			return;
		}
		else if (percent < 90)
		{
			L2PlayerData dat = leader.getSoDPlayer();
			if (!dat.isRaidRequested(player))
				dat.addRaidRequest(player, party, raid);
			else
			{
				L2TextBuilder tb = new L2TextBuilder();
				tb.append("<html><title>Raid Manager</title><body><center><img src=\"L2UI_CH3.herotower_deco\" ");
				tb.append("width=256 height=32><br>Do I look retarded to you? ");
				tb.append("<font color=\"LEVEL\">The raid leader did not answer yet!</font>");
				tb.append("<br><img src=\"L2UI_CH3.herotower_deco\" width=256 height=32></center></body></html>");
				player.sendPacket(new NpcHtmlMessage(tb.moveToString()));
			}
		}
		else
			player.sendMessage("You may attempt entrance without any requests.");
	}
	
	private static void doRefuse(L2PcInstance insider, String command)
	{
		String[] cmd = command.split(" ");
		if (cmd.length < 3)
		{
			Punishment.handleIllegalPlayerAction(insider, "Raid Manager exploitation 6 (bypass not validated??!).");
			return;
		}
		
		Integer raid = -1;
		int partyLeader = -1;
		try
		{
			raid = Integer.valueOf(cmd[1]);
			partyLeader = Integer.parseInt(cmd[2]);
		}
		catch (Exception e)
		{
			Punishment.handleIllegalPlayerAction(insider, "Raid Manager exploitation 7 (bypass not validated??!).");
			return;
		}
		
		insider.sendPacket(HideTutorialHtml.PACKET);
		
		/*
		BossSpawnManager bsm = RaidBossSpawnManager.getInstance();
		if (!bsm.isDefined(raid))
			bsm = GrandBossSpawnManager.getInstance();
		if (bsm.getRaidBossStatusId(raid) != StatusEnum.ALIVE)
			return;
		*/
		L2PcInstance leader = BossSpawnManager.getLeader(raid);
		if (insider != leader)
			return;
		
		L2PcInstance requestor = L2World.getInstance().findPlayer(partyLeader);
		if (requestor == null)
			return;
		requestor.getSoDPlayer().showQuestionMark(ShowTutorialMark.RAID_REQUEST_DENIED);
	}
	
	private static void doReqTele(L2PcInstance insider, String command)
	{
		String[] cmd = command.split(" ");
		if (cmd.length < 3)
		{
			Punishment.handleIllegalPlayerAction(insider, "Raid Manager exploitation 4 (bypass not validated??!).");
			return;
		}
		
		Integer raid = -1;
		int partyLeader = -1;
		try
		{
			raid = Integer.valueOf(cmd[1]);
			partyLeader = Integer.parseInt(cmd[2]);
		}
		catch (Exception e)
		{
			Punishment.handleIllegalPlayerAction(insider, "Raid Manager exploitation 5 (bypass not validated??!).");
			return;
		}
		
		insider.sendPacket(HideTutorialHtml.PACKET);
		
		BossSpawnManager bsm = RaidBossSpawnManager.getInstance();
		if (!bsm.isDefined(raid))
			bsm = GrandBossSpawnManager.getInstance();
		if (!bsm.isDefined(raid))
		{
			GrandBossIntegrationManager.getInstance().teleByRequest(insider, raid, partyLeader);
			return;
		}
		if (bsm.getRaidBossStatusId(raid) != StatusEnum.ALIVE)
			return;
		L2PcInstance leader = BossSpawnManager.getLeader(raid);
		if (insider != leader)
			return;
		L2Npc npc = bsm.getBosses().get(raid);
		double percent = npc.getCurrentHp() * 100 / npc.getMaxHp();
		if (percent < 5)
			return;
		L2PcInstance requestor = L2World.getInstance().findPlayer(partyLeader);
		if (requestor == null)
			return;
		L2Party party = requestor.getParty();
		if (party != null && !party.isLeader(requestor))
			return;
		if (!requestor.destroyItemByItemId("Raid Teleport", LootedGoodsManager.GATEKEEPER_TOKEN,
				getNeededItemCount(raid, party), insider, true))
			return;
		
		Location loc = npc.getLoc();
		L2Skill sk = SkillTable.getInstance().getInfo(Skills.CURSE_OF_ANOMALY, 1);
		if (party != null)
		{
			BossSpawnManager.addRaider(raid, party);
			for (L2PcInstance member : party.getPartyMembers())
			{
				member.teleToLocation(loc, true);
				if (sk != null)
					sk.getEffects(member, member);
				member.setInstanceId(BossSpawnManager.BOSS_INSTANCE_OFFSET + raid);
			}
		}
		else
		{
			requestor.teleToLocation(loc, true);
			if (sk != null)
				sk.getEffects(requestor, requestor);
			requestor.setInstanceId(BossSpawnManager.BOSS_INSTANCE_OFFSET + raid);
		}
	}
	
	private static void doTeleport(L2PcInstance player, String command)
	{
		String[] cmd = command.split(" ");
		if (cmd.length < 3)
		{
			Punishment.handleIllegalPlayerAction(player, "Raid Manager exploitation 1 (bypass not validated??!).");
			return;
		}
		Integer raid = -1;
		try
		{
			raid = Integer.valueOf(cmd[1]);
		}
		catch (Exception e)
		{
			Punishment.handleIllegalPlayerAction(player, "Raid Manager exploitation 2 (bypass not validated??!).");
			return;
		}
		L2Npc transObj = player.getLastFolkNPC();
		boolean solo = (cmd[2].charAt(0) == '1');
		L2Party party = player.getParty();
		if (solo)
			party = null;
		else if (party == null || !party.isLeader(player))
		{
			player.sendPacket(SystemMessageId.ONLY_PARTY_LEADER_CAN_ENTER);
			return;
		}
		
		BossSpawnManager bsm = RaidBossSpawnManager.getInstance();
		if (!bsm.isDefined(raid))
			bsm = GrandBossSpawnManager.getInstance();
		if (!bsm.isDefined(raid))
		{
			GrandBossIntegrationManager.getInstance().tele(player, party, raid);
			return;
		}
		if (bsm.getRaidBossStatusId(raid) != StatusEnum.ALIVE)
		{
			player.sendPacket(SystemMessageId.PLEASE_TRY_AGAIN_LATER);
			return;
		}
		L2PcInstance leader = BossSpawnManager.getLeader(raid);
		L2Npc npc = bsm.getBosses().get(raid);
		double percent = npc.getCurrentHp() * 100 / npc.getMaxHp();
		if (percent < 5 && leader != null)
		{
			player.sendMessage("Currently a party is finishing off the raid boss, so you may not enter.");
			return;
		}
		else if (percent < 90 && leader != null)
		{
			L2TextBuilder sb = new L2TextBuilder(ASK[0]);
			sb.append(raid);
			sb.append(ASK[1]);
			player.sendPacket(new NpcHtmlMessage(transObj, sb.moveToString()));
			return;
		}
		
		if (!player.destroyItemByItemId("Raid Teleport", LootedGoodsManager.GATEKEEPER_TOKEN,
				getNeededItemCount(raid, party), transObj, true))
			return;
		
		Location loc = npc.getLoc();
		int x = loc.getX(), y = loc.getY(), z = loc.getZ();
		if (bsm instanceof RaidBossSpawnManager)
		{
			if (Rnd.nextBoolean())
				x += Rnd.get(1100, 1600);
			else
				y += Rnd.get(1100, 1600);
			z += 300;
		}
		if (party != null)
		{
			BossSpawnManager.addRaider(raid, party);
			for (L2PcInstance member : party.getPartyMembers())
			{
				member.teleToLocation(x, y, z, true);
				member.setInstanceId(BossSpawnManager.BOSS_INSTANCE_OFFSET + raid);
			}
		}
		else
		{
			player.teleToLocation(x, y, z, true);
			player.setInstanceId(BossSpawnManager.BOSS_INSTANCE_OFFSET + raid);
		}
	}
	
	private static final String[] ASK =
	{
			"<html><title>Raid Manager</title><body><center><img src=\"L2UI_CH3.herotower_deco\" "
					+ "width=256 height=32><br>Hey listen, <font color=\"LEVEL\">you can't go</font> there. "
					+ "The current raiding party paid me a good sum of adena to get rid of all useless competitors, "
					+ "if you get what I mean.<br1>However, I can arrange my assistant (that good-for-nothing boy "
					+ "is gone again!) to <font color=\"LEVEL\">send a message to the current raiding party leader</font>"
					+ ". If he approves your presence in the battlefield, I'll send you there.<br1>Just remember, "
					+ "the anomalous beast will not tolerate such interference, so you are likely to be "
					+ "<font color=\"LEVEL\">disabled and attacked at once</font> when you get there.<br>"
					+ "<a action=\"bypass -h raidman_ask ",
			"\">Send a message.</a><br><img src=\"L2UI_CH3.herotower_deco\" width=256 height=32>"
					+ "</center></body></html>"
	};
	
	private static final String[] PARTS =
	{
			"<html><title>Raid Manager</title><body><center><img src=\"L2UI_CH3.herotower_deco\" "
					+ "width=256 height=32><br>Fighting the bosses is a great challenge: it is not for the weak! "
					+ "Do it if you want to, but be warned, you do it at your own risk, I take no responsibility!<br>"
					+ "In order to teleport you need <font color=\"LEVEL\">Teleport Tokens</font>. "
					+ "Select the teleporting method:<table><tr><td width=\"200\">Boss</td>"
					+ "<td width=20>TT</td><td>Solo:</td><td>Party:</td></tr>", "<tr>", // 1
			"</tr>", // 2
			"<td>", // 3
			"</td>", // 4
			"<td width=\"180\">", // 5
			"<button value=\"Go\" action=\"bypass -h raidman_tele ", // 6
			"\" width=40 height=20 back=\"L2UI_CT1.Gauge_DF_SSQ_Dawn_bg\" fore=\"L2UI_CT1.Gauge_DF_SSQ_Dawn\">", // 7
			"\" width=40 height=20 back=\"L2UI_CT1.Gauge_DF_SSQ_Dusk_bg\" fore=\"L2UI_CT1.Gauge_DF_SSQ_Dusk\">", // 8
			"<font color=\"red\">Dead</font>", // 9
			"</table><br><img src=\"L2UI_CH3.herotower_deco\" width=256 height=32></center></body></html>" // 10
	};
	
	private static void buildChat()
	{
		L2TextBuilder htm = new L2TextBuilder();
		htm.append(PARTS[0]);
		
		GrandBossIntegrationManager.getInstance().insertManagedBosses(htm);
		
		BossSpawnManager bsm = RaidBossSpawnManager.getInstance();
		FastMap<Integer, L2Spawn> spawns = bsm.getSpawns();
		for (FastMap.Entry<Integer, L2Spawn> entry = spawns.head(), end = spawns.tail(); (entry = entry.getNext()) != end;)
		{
			L2Spawn sp = entry.getValue();
			htm.append(PARTS[1]);
			htm.append(PARTS[5]);
			htm.append(sp.getTemplate().getName());
			htm.append(PARTS[4]);
			Integer raidId = entry.getKey();
			htm.append(PARTS[3]);
			htm.append(getNeededItemCount(raidId, null));
			htm.append(PARTS[4]);
			for (int i = 1; i < 3; i++)
			{
				htm.append(PARTS[3]);
				if (bsm.getRaidBossStatusId(raidId) == StatusEnum.ALIVE)
				{
					htm.append(PARTS[6]);
					htm.append(raidId);
					htm.append(' ');
					htm.append(i);
					L2PcInstance leader = BossSpawnManager.getLeader(raidId);
					L2Npc npc = sp.getLastSpawn();
					double percent = npc.getCurrentHp() * 100 / npc.getMaxHp();
					if (percent > 90 || leader == null)
						htm.append(PARTS[7]);
					else
						htm.append(PARTS[8]);
				}
				else
					htm.append(PARTS[9]);
				htm.append(PARTS[4]);
			}
			htm.append(PARTS[2]);
		}
		bsm = GrandBossSpawnManager.getInstance();
		spawns = bsm.getSpawns();
		for (FastMap.Entry<Integer, L2Spawn> entry = spawns.head(), end = spawns.tail(); (entry = entry.getNext()) != end;)
		{
			L2Spawn sp = entry.getValue();
			htm.append(PARTS[1]);
			htm.append(PARTS[5]);
			htm.append(sp.getTemplate().getName());
			htm.append(PARTS[4]);
			Integer raidId = entry.getKey();
			htm.append(PARTS[3]);
			htm.append(getNeededItemCount(raidId, null));
			htm.append(PARTS[4]);
			for (int i = 1; i < 3; i++)
			{
				htm.append(PARTS[3]);
				if (bsm.getRaidBossStatusId(raidId) == StatusEnum.ALIVE)
				{
					htm.append(PARTS[6]);
					htm.append(raidId);
					htm.append(' ');
					htm.append(i);
					L2PcInstance leader = BossSpawnManager.getLeader(raidId);
					L2Npc npc = sp.getLastSpawn();
					double percent = npc.getCurrentHp() * 100 / npc.getMaxHp();
					if (percent > 90 || leader == null)
						htm.append(PARTS[7]);
					else
						htm.append(PARTS[8]);
				}
				else
					htm.append(PARTS[9]);
				htm.append(PARTS[4]);
			}
			htm.append(PARTS[2]);
		}
		htm.append(PARTS[10]);
		CHAT.setMessage(htm.moveToString());
	}
	
	/**
	 * @return Amount of gatekeeper tokens needed to teleport to the rb
	 */
	public static final long getNeededItemCount(int raid, L2Party party)
	{
		long quantity;
		switch (raid)
		{
		case 29001: // Queen ant
			quantity = 35;
			break;
		case 29006: // Core
			quantity = 20;
			break;
		case 29014: // Orfen
			quantity = 25;
			break;
		case 29019: // Old Antharas (template)
			quantity = 60;
			break;
		case 29020: // Baium (monster)
			quantity = 60;
			break;
		case 29022: // Zaken
			quantity = 45;
			break;
		case 29028: // Valakas
			quantity = 70;
			break;
		// 7x Raids
		case 25244:
		case 25245:
			quantity = 14;
			break;
		default:
			// 8x Raids
			quantity = 18;
			break;
		}
		if (party != null)
			quantity *= party.getMemberCount();
		return quantity;
	}
	
	public static final void addPendingRequest(Integer player, Integer req)
	{
		_pending.put(player, req);
		L2PcInstance requester = L2World.getInstance().findPlayer(req);
		if (requester != null)
			requester.getSoDPlayer().showQuestionMark(ShowTutorialMark.RAID_REQUEST_VIEWED);
	}
	
	public static final void remPendingRequest(Integer player, boolean invalidate)
	{
		Integer req = _pending.remove(player);
		if (req == null)
			return;
		L2PcInstance requester = L2World.getInstance().findPlayer(req);
		if (requester != null)
		{
			if (invalidate)
				requester.getSoDPlayer().showQuestionMark(ShowTutorialMark.RAID_REQUEST_INVALIDATED);
			else
				requester.getSoDPlayer().showQuestionMark(ShowTutorialMark.RAID_REQUEST_DENIED);
		}
	}
}
