/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.model.actor.instance;

import java.util.Locale;

import org.sod.L2Htm;
import org.sod.i18n.Internationalization;
import org.sod.manager.RespawnManager;
import org.sod.manager.RespawnManager.BaseInfo;

import com.l2jfree.gameserver.communitybbs.Manager.SettingsBBSManager;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.network.client.packets.sendable.NpcHtmlMessage;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;
import com.l2jfree.lang.L2TextBuilder;

/**
 * TODO Auto-generated JavaDoc
 * 
 * @author savormix
 * @since 2010.12.23
 */
public class L2BaseTeleporterInstance extends L2Npc
{
	public L2BaseTeleporterInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}
	
	@Override
	public final void showChatWindow(L2PcInstance player, int page)
	{
		NpcHtmlMessage htm = new NpcHtmlMessage(getObjectId());
		RespawnManager rm = RespawnManager.getInstance();
		final int idx = player.getSoDPlayer().getSettings().getLocaleIndex();
		if (!rm.isInsideBase(player) && !player.isGM())
		{
			htm.setHtml(MSG_FAIL[idx]);
			player.sendPacket(htm);
			return;
		}
		
		final int shop = rm.getShopAreaId(player);
		if (shop != -1)
			htm.setHtml(MSG_SHOP[shop][idx]);
		else
			htm.setHtml(MSG_BASE[idx]);
		htm.replace("%objectId%", getObjectId());
		player.sendPacket(htm);
	}
	
	@Override
	public void onBypassFeedback(L2PcInstance player, String command)
	{
		if (command.isEmpty())
			return;
		
		RespawnManager rm = RespawnManager.getInstance();
		if (command.charAt(0) == 'b')
		{
			rm.moveToSpawn(player);
			return;
		}
		
		final int shop;
		try
		{
			shop = Integer.parseInt(command);
		}
		catch (RuntimeException e)
		{
			return;
		}
		rm.moveToShopArea(player, shop);
	}
	
	private static String[] MSG_BASE;
	private static String[] MSG_FAIL;
	private static String[][] MSG_SHOP;
	
	static
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale[] locs = SettingsBBSManager.getInstance().getAllLocales();
		BaseInfo[] shops = RespawnManager.getInstance().getShops();
		
		MSG_BASE = new String[locs.length];
		MSG_FAIL = new String[locs.length];
		MSG_SHOP = new String[shops.length][locs.length];
		
		L2TextBuilder tb = new L2TextBuilder();
		for (int i = 0; i < locs.length; i++)
		{
			Locale loc = locs[i];
			
			L2Htm.writeHeader(tb, null);
			for (int j = 0; j < shops.length; j++)
				L2Htm.writeNpcButton(tb, null, j, null, 200, 40, true,
						i18n.get(loc, "NPC_VENDOR_MANAGER_AREA", "%area%", String.valueOf(j + 1)));
			L2Htm.writeFooter(tb);
			MSG_BASE[i] = tb.toString();
			tb.setLength(0);
			
			L2Htm.writeHeader(tb, null);
			tb.append(i18n.get(loc, "NPC_VENDOR_MANAGER_UNAVAILABLE"));
			L2Htm.writeFooter(tb);
			MSG_FAIL[i] = tb.toString();
			tb.setLength(0);
			
			for (int j = 0; j < shops.length; j++)
			{
				L2Htm.writeHeader(tb, null);
				for (int k = 0; k < shops.length; k++)
					if (k != j)
						L2Htm.writeNpcButton(tb, null, k, null, 200, 40, true,
								i18n.get(loc, "NPC_VENDOR_MANAGER_AREA", "%area%", String.valueOf(k + 1)));
				L2Htm.writeNpcButton(tb, null, "b", null, 200, 40, true, i18n.get(loc, "NPC_VENDOR_MANAGER_BASE"));
				L2Htm.writeFooter(tb);
				MSG_SHOP[j][i] = tb.toString();
				tb.setLength(0);
			}
		}
		tb.setLength(0);
		tb.moveToString();
	}
}
