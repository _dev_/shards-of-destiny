/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.model.actor.instance;

import org.sod.manager.conquest.ConquestFactionManager;
import org.sod.model.conquest.ConquestFactionMember;

import com.l2jfree.gameserver.ai.L2ControllableMobAI;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;

/**
 * Represents the AtraTrueor.
 * 
 * @author savormix
 * @since 2010.02.04
 * @deprecated MobGroup forces spawns to be of {@link L2ControllableMobInstance}.
 */
@Deprecated
public class L2FollowerInstance extends L2MonsterInstance implements ConquestFactionMember
{
	public L2FollowerInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}
	
	/* (non-Javadoc)
	 * @see org.sod.model.FactionMember#getFaction()
	 */
	@Override
	public int getConquestFaction()
	{
		if (!(getAI() instanceof L2ControllableMobAI))
			return ConquestFactionManager.NONE;
		L2Character cha = ((L2ControllableMobAI) getAI()).getOwner();
		if (cha == null)
			return ConquestFactionManager.NONE;
		L2PcInstance player = cha.getActingPlayer();
		if (player == null)
			return ConquestFactionManager.NONE;
		else
			return player.getConquestFaction();
	}
}
