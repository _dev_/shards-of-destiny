/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.model.actor.instance;

import org.sod.model.PlayerLookalike;

import com.l2jfree.gameserver.model.base.ClassId;
import com.l2jfree.gameserver.network.serverpackets.AbstractNpcInfo;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;
import com.l2jfree.util.Rnd;

/**
 * An army NPC instance.
 * @author savormix
 * @since 2010.09.02
 */
public class AdvertiserInstance extends L2NpcInstance implements PlayerLookalike
{
	public static final AdvertEquip UNK = new AdvertEquip(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	public static final AdvertEquip TANK_B_DOOM = new AdvertEquip(0, 2417, 175, 110, 5722, 2381, 0, 5738, 0, 0, 0);
	public static final AdvertEquip DESTRO_B_DOOM = new AdvertEquip(0, 2417, 78, 0, 5722, 2381, 0, 5738, 0, 0, 0);
	public static final AdvertEquip TYRANT_B_DOOM = new AdvertEquip(0, 2417, 4806, 0, 5723, 2392, 0, 5739, 0, 0, 0);
	public static final AdvertEquip ARCHER_B_DOOM = new AdvertEquip(0, 2417, 287, 0, 5723, 2392, 0, 5739, 0, 0, 0);
	public static final AdvertEquip ARBA_B_DOOM = new AdvertEquip(0, 2417, 9324, 0, 5723, 2392, 0, 5739, 0, 0, 0);
	public static final AdvertEquip ZERKER_B_DOOM = new AdvertEquip(0, 2417, 9308, 0, 5723, 2392, 0, 5739, 0, 0, 0);
	public static final AdvertEquip MAGE_B_AVA = new AdvertEquip(0, 2415, 148, 110, 5716, 2406, 0, 5732, 0, 0, 0);
	public static final AdvertEquip TANK_A_NM = new AdvertEquip(0, 2418, 8788, 2498, 5771, 374, 0, 5783, 0, 0, 0);
	public static final AdvertEquip DESTRO_A_TAL = new AdvertEquip(0, 547, 8793, 0, 5768, 2382, 0, 5780, 0, 0, 0);
	public static final AdvertEquip TYRANT_A_DC = new AdvertEquip(0, 512, 8810, 0, 5766, 2385, 2389, 5778, 0, 0, 0);
	public static final AdvertEquip ARCHER_A_MAJ = new AdvertEquip(0, 2419, 8807, 0, 5775, 2395, 0, 5787, 0, 0, 0);
	public static final AdvertEquip ARBA_A_MAJ = new AdvertEquip(0, 2419, 9362, 0, 5775, 2395, 0, 5787, 0, 0, 0);
	public static final AdvertEquip ZERKER_A_DC = new AdvertEquip(0, 512, 9359, 0, 5766, 2385, 2389, 5778, 0, 0, 0);
	public static final AdvertEquip MAGE_A_DC = new AdvertEquip(0, 512, 151, 641, 5767, 2407, 0, 5779, 0, 0, 0);

	public static int ANY = -1;
	public static int TANK_RACE = ANY;
	public static int MAGE_RACE = ANY;
	public static int ARCHER_RACE = ANY;
	public static int DEFAULT_RACE = ANY;
	public static int DEFAULT_SEX = ANY;

	private final int _race;
	private final boolean _sex;
	private final int _class;
	private final double _cr;
	private final double _ch;
	private final int _hairStyle;
	private final int _hairColor;
	private final int _face;
	private AdvertEquip _equip;

	public AdvertiserInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
		_equip = UNK;
		boolean onlyFighter = false;
		boolean onlyRanger = false;
		boolean onlyMage = false;
		boolean aGrade;
		switch (getNpcId())
		{
		case 90027:
		case 90028:
		case 90029:
		case 90031:
		case 90059:
			aGrade = true;
			break;
		default:
			aGrade = false;
			break;
		}
		if (getNpcId() == 90024 || getNpcId() == 90027)
		{
			if (TANK_RACE == ANY)
				_race = Rnd.get(5); // tanks
			else
				_race = TANK_RACE;
			if (aGrade)
				_equip = TANK_A_NM;
			else
				_equip = TANK_B_DOOM;
			onlyFighter = true;
		}
		else if (getNpcId() == 90025 || getNpcId() == 90028)
		{
			if (MAGE_RACE == ANY)
				_race = Rnd.get(4); // mages
			else
				_race = MAGE_RACE;
			if (aGrade)
				_equip = MAGE_A_DC;
			else
				_equip = MAGE_B_AVA;
			onlyMage = true;
		}
		else if (getNpcId() == 90030 || getNpcId() == 90031)
		{
			if (ARCHER_RACE == ANY)
			{
				int rnd = Rnd.get(4);
				if (rnd == 3)
				{
					_race = 5;
					if (aGrade)
						_equip = ARBA_A_MAJ;
					else
						_equip = ARBA_B_DOOM;
				}
				else
				{
					_race = rnd;
					if (aGrade)
						_equip = ARCHER_A_MAJ;
					else
						_equip = ARCHER_B_DOOM;
				}
			}
			else
				_race = ARCHER_RACE;
			onlyFighter = true;
			onlyRanger = true;
		}
		else
		{
			if (DEFAULT_RACE == ANY)
				_race = Rnd.get(6);
			else
				_race = DEFAULT_RACE;
			if (getRace() == 4)
			{
				// may be pole?
				if (aGrade)
					_equip = TANK_A_NM;
				else
					_equip = TANK_B_DOOM;
				onlyFighter = true;
			}
		}
		if (onlyRanger && getRace() == 5)
			_sex = true;
		else if (DEFAULT_SEX == ANY)
			_sex = Rnd.nextBoolean();
		else
			_sex = (DEFAULT_SEX == 1);
		int c1, c2;
		double cr1, ch1, cr2, ch2;
		switch (getRace())
		{
		case 0:
			c1 = ClassId.HumanFighter.getId();
			if (getSex())
			{
				cr1 = 8;
				ch1 = 23.5;
			}
			else
			{
				cr1 = 9;
				ch1 = 23;
			}
			c2 = ClassId.HumanMystic.getId();
			if (getSex())
			{
				cr2 = 6.5;
				ch2 = 22.5;
			}
			else
			{
				cr2 = 7.5;
				ch2 = 22.8;
			}
			break;
		case 1:
			c1 = ClassId.ElvenFighter.getId();
			if (getSex())
			{
				cr1 = 7.5;
				ch1 = 23;
			}
			else
			{
				cr1 = 7.5;
				ch1 = 24;
			}
			c2 = ClassId.ElvenMystic.getId();
			cr2 = cr1;
			ch2 = ch1;
			break;
		case 2:
			c1 = ClassId.DarkFighter.getId();
			if (getSex())
			{
				cr1 = 7;
				ch1 = 23.5;
			}
			else
			{
				cr1 = 7.5;
				ch1 = 24;
			}
			c2 = ClassId.DarkMystic.getId();
			cr2 = cr1;
			ch2 = ch1;
			break;
		case 3:
			c1 = ClassId.OrcFighter.getId();
			if (getSex())
			{
				cr1 = 7;
				ch1 = 27;
			}
			else
			{
				cr1 = 11;
				ch1 = 28;
			}
			c2 = ClassId.OrcMystic.getId();
			if (getSex())
			{
				cr2 = 8;
				ch2 = 25.5;
			}
			else
			{
				cr2 = 7;
				ch2 = 27.5;
			}
			break;
		case 4:
			c1 = ClassId.DwarvenFighter.getId();
			if (getSex())
			{
				cr1 = 5;
				ch1 = 19;
			}
			else
			{
				cr1 = 9;
				ch1 = 18;
			}
			c2 = c1;
			cr2 = cr1;
			ch2 = ch1;
			break;
		//case 5:
		default:
			if (getSex())
			{
				c1 = ClassId.FemaleSoldier.getId();
				cr1 = 7.5;
				ch1 = 22;
			}
			else
			{
				c1 = ClassId.MaleSoldier.getId();
				cr1 = 7.5;
				ch1 = 24;
			}
			c2 = c1;
			cr2 = cr1;
			ch2 = ch1;
			break;
		}
		if (onlyFighter && getRace() != 5)
		{
			c2 = c1;
			cr2 = cr1;
			ch2 = ch1;
		}
		else if (onlyMage)
		{
			c1 = c2;
			cr1 = cr2;
			ch1 = ch2;
		}
		if (Rnd.nextBoolean())
		{
			_class = c1;
			_cr = cr1;
			_ch = ch1;
		}
		else
		{
			_class = c2;
			_cr = cr2;
			_ch = ch2;
		}
		boolean mage = false;
		for (ClassId ci : ClassId.values())
		{
			if (ci.getId() == getClassType())
			{
				mage = ci.isMage();
				break;
			}
		}
		if (getEquip() == UNK)
		{
			if (mage)
			{
				if (aGrade)
					_equip = MAGE_A_DC;
				else
					_equip = MAGE_B_AVA;
			}

			// dwarves taken care of
			if (getRace() == 3)
			{
				if (Rnd.nextBoolean())
				{
					if (aGrade)
						_equip = TYRANT_A_DC;
					else
						_equip = TYRANT_B_DOOM;
				}
				else
				{
					if (aGrade)
						_equip = DESTRO_A_TAL;
					else
						_equip = DESTRO_B_DOOM;
				}
			}
			else if (getRace() == 5)
			{
				if (getSex())
				{
					if (aGrade)
						_equip = ARBA_A_MAJ;
					else
						_equip = ARBA_B_DOOM;
				}
				else
				{
					if (aGrade)
						_equip = ZERKER_A_DC;
					else
						_equip = ZERKER_B_DOOM;
				}
			}
			else
			{
				// TODO: eq based on class
				if (aGrade)
					_equip = ARCHER_A_MAJ;
				else
					_equip = ARCHER_B_DOOM;
			}
		}
		if (getSex())
			_hairStyle = Rnd.get(7);
		else
			_hairStyle = Rnd.get(5);
		_hairColor = Rnd.get(4);
		_face = Rnd.get(3);
	}

	@Override
	public int getRace()
	{
		return _race;
	}

	@Override
	public boolean getSex()
	{
		return _sex;
	}

	@Override
	public int getClassType()
	{
		return _class;
	}

	@Override
	public double getCr()
	{
		return _cr;
	}

	@Override
	public double getCh()
	{
		return _ch;
	}

	@Override
	public AdvertEquip getEquip()
	{
		return _equip;
	}

	@Override
	public int getHairStyle()
	{
		return _hairStyle;
	}

	@Override
	public int getHairColor()
	{
		return _hairColor;
	}

	@Override
	public int getFace()
	{
		return _face;
	}

	@Override
	public boolean hasRandomAnimation()
	{
		return false;
	}

	@Override
	public void showChatWindow(L2PcInstance player, int page)
	{
	}

	@Override
	public void sendInfo(L2PcInstance activeChar)
	{
		activeChar.sendPacket(new AbstractNpcInfo.AdvertInfo(this));
	}

	@Override
	public void broadcastFullInfoImpl()
	{
		broadcastPacket(new AbstractNpcInfo.AdvertInfo(this));
	}

	public static class AdvertEquip
	{
		private final int _under;
		private final int _helmet;
		private final int _weapon;
		private final int _shield;
		private final int _gloves;
		private final int _chest;
		private final int _legs;
		private final int _boots;
		private final int _cloak;
		private final int _face1;
		private final int _face2;

		public AdvertEquip(int under, int helmet, int weapon, int shield, int gloves, int chest,
				int legs, int boots, int cloak, int face1, int face2)
		{
			_under = under;
			_helmet = helmet;
			_weapon = weapon;
			_shield = shield;
			_gloves = gloves;
			_chest = chest;
			_legs = legs;
			_boots = boots;
			_cloak = cloak;
			_face1 = face1;
			_face2 = face2;
		}

		public int getUnder()
		{
			return _under;
		}

		public int getHelmet()
		{
			return _helmet;
		}

		public int getWeapon()
		{
			return _weapon;
		}

		public int getShield()
		{
			return _shield;
		}

		public int getGloves()
		{
			return _gloves;
		}

		public int getChest()
		{
			return _chest;
		}

		public int getLegs()
		{
			return _legs;
		}

		public int getBoots()
		{
			return _boots;
		}

		public int getCloak()
		{
			return _cloak;
		}

		public int getFace1()
		{
			return _face1;
		}

		public int getFace2()
		{
			return _face2;
		}
	}
}
