/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.model.actor.instance;

import org.sod.manager.conquest.ConquestFactionManager;
import org.sod.model.conquest.ConquestZoneMember;

import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.L2Playable;
import com.l2jfree.gameserver.model.actor.status.CharStatus;
import com.l2jfree.gameserver.model.actor.status.ImperviousAttackableStatus;
import com.l2jfree.gameserver.model.zone.L2FactionZone;
import com.l2jfree.gameserver.network.serverpackets.AbstractNpcInfo;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;

public final class L2ZoneSpawnGuardInstance extends L2MonsterInstance
	implements ConquestZoneMember
{
	private L2FactionZone _zone;

	public L2ZoneSpawnGuardInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public final int getConquestFaction()
	{
		if (_zone == null)
			return ConquestFactionManager.NONE;
		else
			return _zone.getConquestFaction();
	}

	@Override
	public final L2FactionZone getFactionZone()
	{
		return _zone;
	}

	@Override
	public final void setFactionZone(L2FactionZone zone)
	{
		_zone = zone;
	}
/*
	@Override
	public final L2ZoneTestAI getAI()
	{
		return (L2ZoneTestAI) super.getAI();
	}
*/
	@Override
	public boolean hasRandomAnimation()
	{
		return false;
	}

	@Override
	public boolean isAggressive()
	{
		return true;
	}
/*
	@Override
	protected final L2CharacterAI initAI()
	{
		return new L2ZoneTestAI(new AIAccessor());
	}
*/
	@Override
	protected CharStatus initStatus()
	{
		return new ImperviousAttackableStatus(this);
	}

	@Override
	public final boolean isAutoAttackable(L2Character attacker)
	{
		if (getConquestFaction() == ConquestFactionManager.NONE || !(attacker instanceof L2Playable))
			return false;

		return !ConquestFactionManager.getInstance().isSameFaction(this, attacker);
	}

	@Override
	public void onSpawn()
	{
		setIsNoRndWalk(true);
		super.onSpawn();
	}

	@Override
	public void sendInfo(L2PcInstance activeChar)
	{
		activeChar.sendPacket(new AbstractNpcInfo.TestInfo(this, false));
	}

	@Override
	public void broadcastFullInfoImpl()
	{
		broadcastPacket(new AbstractNpcInfo.TestInfo(this, false));
	}

	@Override
	public void showChatWindow(L2PcInstance player, int page)
	{
	}
}
