/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.model.zone;

import javolution.util.FastSet;

import org.sod.model.event.global.GlobalEvent;

import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.L2Playable;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;

/**
 * TODO Auto-generated JavaDoc
 * @author savormix
 * @since 2010.04.02
 */
public class L2GlobalEventZone extends L2DynamicZone
{
	private FastSet<GlobalEvent> _events;
	
	protected final FastSet<GlobalEvent> getEvents()
	{
		return _events;
	}
	
	public void register(GlobalEvent event)
	{
		if (getEvents() == null)
			_events = FastSet.newInstance();
		getEvents().add(event);
	}
	
	private GlobalEvent getPlayerEvent(L2PcInstance player)
	{
		if (player == null || getEvents() == null)
			return null;
		for (FastSet.Record r = getEvents().head(), end = getEvents().tail(); (r = r.getNext()) != end;)
		{
			GlobalEvent ge = getEvents().valueOf(r);
			if (ge.isPlaying(player))
				return ge;
		}
		return null;
	}
	
	@Override
	protected void onEnter(L2Character character)
	{
		super.onEnter(character);
		L2PcInstance player = character.getActingPlayer();
		GlobalEvent ge = getPlayerEvent(player);
		if (ge != null)
			ge.zoneStateChanged(this, (L2Playable) character, player, true);
	}
	
	@Override
	protected void onExit(L2Character character)
	{
		super.onExit(character);
		L2PcInstance player = character.getActingPlayer();
		GlobalEvent ge = getPlayerEvent(player);
		if (ge != null)
			ge.zoneStateChanged(this, (L2Playable) character, player, false);
	}
	
	@Override
	protected boolean checkDynamicConditions(L2Character character)
	{
		return getPlayerEvent(character.getActingPlayer()) != null;
	}
}
