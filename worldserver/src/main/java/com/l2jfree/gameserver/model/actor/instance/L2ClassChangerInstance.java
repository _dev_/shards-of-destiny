/*
 * MISSING LICENSING INFO
 */
package com.l2jfree.gameserver.model.actor.instance;

import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.StringTokenizer;

import org.sod.L2Htm;
import org.sod.i18n.Internationalization;

import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.base.ClassId;
import com.l2jfree.gameserver.model.base.ClassLevel;
import com.l2jfree.gameserver.network.SystemMessageId;
import com.l2jfree.gameserver.network.client.packets.sendable.NpcHtmlMessage;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;
import com.l2jfree.lang.L2TextBuilder;

/**
 * @author savormix
 * 
 */
public class L2ClassChangerInstance extends L2Npc
{
	private static final String EVENT = "cc";
	
	public L2ClassChangerInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}
	
	@Override
	public void onBypassFeedback(L2PcInstance player, String command)
	{
		StringTokenizer st = new StringTokenizer(command, " ");
		if (!st.nextToken().equals(EVENT))
			return;
		
		ClassId req = L2Htm.getRequest(st, ClassId.values());
		if (req == null)
			return;
		
		Set<ClassId> allowed = getAvailableClasses(player);
		if (!allowed.contains(req))
			return;
		
		player.setClassId(req.getId());
		player.setBaseClass(player.getActiveClass());
		
		player.broadcastUserInfo();
		
		if (req.getLevel() == ClassLevel.Fourth)
			player.sendPacket(SystemMessageId.THIRD_CLASS_TRANSFER);
		else
			player.sendPacket(SystemMessageId.CLASS_TRANSFER);
		
		player.refreshOverloaded();
		player.refreshExpertisePenalty();
	}
	
	@Override
	public final void showChatWindow(L2PcInstance player, int page)
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, i18n.get(loc, "NPC_CLASS_CHANGER_TITLE"));
		
		int nextLevel = player.getClassId().getLevel().getLevelToAdvance();
		if (nextLevel == -1)
			tb.append(i18n.get(loc, "NPC_CLASS_CHANGER_NO_FURTHER_CHANGES"));
		else if (player.getLevel() < nextLevel)
			tb.append(i18n.get(loc, "NPC_CLASS_CHANGER_LEVEL_REQUIRED", "%level%", String.valueOf(nextLevel)));
		else
		{
			tb.append(i18n.get(loc, "NPC_CLASS_CHANGER_AVAILABLE_CLASSES"));
			Set<ClassId> classes = getAvailableClasses(player);
			for (ClassId cid : classes)
			{
				StringBuilder sb = new StringBuilder(cid.name());
				for (int i = 1; i < sb.length(); i++)
				{
					if (Character.isUpperCase(sb.charAt(i)))
						sb.insert(i++, ' ');
				}
				L2Htm.writeNpcButton(tb, getObjectId(), EVENT, cid.ordinal(), 250, 30, true, sb.toString());
			}
		}
		
		L2Htm.writeFooter(tb);
		
		NpcHtmlMessage htm = new NpcHtmlMessage(getObjectId(), tb.moveToString());
		player.sendPacket(htm);
	}
	
	private static Set<ClassId> getAvailableClasses(L2PcInstance player)
	{
		Set<ClassId> result = new HashSet<ClassId>();
		
		ClassId cid = player.getClassId();
		ClassLevel cl = cid.getLevel();
		while (cl.getLevelToAdvance() != -1 && player.getLevel() >= cl.getLevelToAdvance())
			cl = ClassLevel.values()[cl.ordinal() + 1];
		
		if (cl == cid.getLevel())
			return Collections.emptySet();
		
		for (ClassId c : ClassId.values())
			if (c.childOf(cid) && c.getLevel() == cl)
				result.add(c);
		
		return result;
	}
}
