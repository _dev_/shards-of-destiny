/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.model.actor.instance;

import java.util.Locale;
import java.util.StringTokenizer;
import java.util.concurrent.atomic.AtomicLong;

import javolution.util.FastMap;
import javolution.util.FastMap.Entry;

import org.sod.L2Htm;
import org.sod.i18n.Internationalization;
import org.sod.manager.BountyManager;
import org.sod.manager.GambleManager;
import org.sod.manager.GambleManager.GambleReward;

import com.l2jfree.Config;
import com.l2jfree.gameserver.communitybbs.Manager.SettingsBBSManager;
import com.l2jfree.gameserver.datatables.CharNameTable;
import com.l2jfree.gameserver.datatables.ItemTable;
import com.l2jfree.gameserver.model.itemcontainer.PcInventory;
import com.l2jfree.gameserver.network.serverpackets.MagicSkillUse;
import com.l2jfree.gameserver.network.client.packets.sendable.NpcHtmlMessage;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;
import com.l2jfree.lang.L2TextBuilder;

/**
 * @author savormix
 * @since 2010.02.01
 */
public class L2BountyHunterInstance extends L2NpcInstance
{
	private static final String[][] PAGES;
	
	public L2BountyHunterInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}
	
	private void showBounties(L2PcInstance player, StringTokenizer st)
	{
		final int page;
		try
		{
			page = Integer.parseInt(st.nextToken());
		}
		catch (RuntimeException e)
		{
			return;
		}
		
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, i18n.get(loc, "NPC_BOUNTY_MANAGER_TITLE"));
		Integer most = BountyManager.getInstance().getMostWanted();
		if (most != null)
		{
			boolean next = false;
			FastMap<Integer, AtomicLong> bounties = BountyManager.getInstance().getBounties();
			tb.append(i18n.get(loc, "NPC_BOUNTY_MOST_WANTED"));
			tb.append(" <font color=\"LEVEL\">");
			tb.append(CharNameTable.getInstance().getByObjectId(most));
			tb.append("</font> (");
			tb.append(bounties.get(most));
			tb.append(")<br>");
			
			CharNameTable cnt = CharNameTable.getInstance();
			int start = page * Config.BOUNTY_HUNTER_PLAYERS_PER_PAGE;
			int last = page + Config.BOUNTY_HUNTER_PLAYERS_PER_PAGE;
			int j = 0;
			for (Entry<Integer, AtomicLong> entry = bounties.head(), end = bounties.tail(); (entry = entry.getNext()) != end;)
			{
				if (++j <= start)
					continue;
				
				if (j == last)
				{
					next = true;
					break;
				}
				
				tb.append(j);
				tb.append(". ");
				tb.append(cnt.getByObjectId(entry.getKey()));
				tb.append(" (");
				tb.append(entry.getValue().get());
				tb.append(")<br1>");
			}
			tb.append("<table><tr><td width=\"135\">");
			if (page > 0)
				L2Htm.writeNpcButton(tb, getObjectId(), BountyRequest.VIEW_LIST, page - 1, 135, 20, true,
						i18n.get(loc, "PREVIOUS"));
			if (next)
			{
				tb.append("</td><td width=\"135\">");
				L2Htm.writeNpcButton(tb, getObjectId(), BountyRequest.VIEW_LIST, page + 1, 135, 20, true,
						i18n.get(loc, "NEXT"));
			}
			tb.append("</td></tr></table>");
		}
		else
			tb.append(i18n.get(loc, "NPC_BOUNTY_MANAGER_NO_OFFERS"));
		tb.append("<br>");
		L2Htm.writeNpcButton(tb, getObjectId(), "Chat", 0, 270, 20, true, i18n.get(loc, "BACK"));
		L2Htm.writeFooter(tb);
		
		NpcHtmlMessage htm = new NpcHtmlMessage(getObjectId(), tb.moveToString());
		player.sendPacket(htm);
	}
	
	private void showOfferResponse(L2PcInstance player, StringTokenizer st)
	{
		String target;
		long offer;
		try
		{
			target = st.nextToken();
			offer = Long.parseLong(st.nextToken());
		}
		catch (RuntimeException e)
		{
			return;
		}
		
		Integer objId = CharNameTable.getInstance().getByName(target);
		if (objId == null)
		{
			showChatWindow(player, 7);
			return;
		}
		
		if (player.reduceAdena("Bounty offer", offer, this, true))
		{
			if (objId.equals(player.getObjectId()))
			{
				broadcastPacket(new MagicSkillUse(this, 13, 1, 1100, 0));
				showChatWindow(player, 10);
				player.doDie(this);
				return;
			}
			
			BountyManager.getInstance().increaseBounty(player, objId, offer);
			showChatWindow(player, 6);
			return;
		}
		
		showChatWindow(player, 8);
	}
	
	private void showGamblingResult(L2PcInstance player, StringTokenizer st)
	{
		final int bet;
		try
		{
			bet = Integer.parseInt(st.nextToken());
		}
		catch (RuntimeException e)
		{
			return;
		}
		
		final boolean paid;
		if (bet == GambleManager.COIN_OF_LUCK)
			paid = player.destroyItemByItemId("Gamble", bet, 1, this, true);
		else
			paid = player.reduceAdena("Gamble", bet, this, true);
		
		if (!paid)
		{
			showChatWindow(player, 8);
			return;
		}
		
		// some time later - gamble for recipes, gift packs, accessory, etc
		GambleReward gr = GambleManager.getInstance().getGambleReward(bet);
		if (gr != null)
		{
			// might not work well with non-stackable items
			player.addItem("Gamble", gr.getItemId(), gr.getCount(), this, true);
			showChatWindow(player);
		}
		else
			showChatWindow(player, 9);
	}
	
	@Override
	public void onBypassFeedback(L2PcInstance player, String command)
	{
		StringTokenizer st = new StringTokenizer(command, " ");
		
		BountyRequest br = L2Htm.getRequest(st, BountyRequest.values());
		if (br == null)
		{
			super.onBypassFeedback(player, command);
			return;
		}
		
		switch (br)
		{
		case VIEW_LIST:
			showBounties(player, st);
			break;
		case ADD_BOUNTY:
			showOfferResponse(player, st);
			break;
		case GAMBLE:
			showGamblingResult(player, st);
			break;
		default:
			_log.warn("Unhandled bounty hunter NPC state: " + br);
			break;
		}
	}
	
	@Override
	public void showChatWindow(L2PcInstance player, int page)
	{
		NpcHtmlMessage htm = new NpcHtmlMessage(getObjectId(), PAGES[page][player.getSoDPlayer().getSettings()
				.getLocaleIndex()]);
		htm.replace("%objectId%", getObjectId());
		player.sendPacket(htm);
	}
	
	private enum BountyRequest
	{
		VIEW_LIST, ADD_BOUNTY, GAMBLE;
	}
	
	static
	{
		Locale[] locs = SettingsBBSManager.getInstance().getAllLocales();
		PAGES = new String[12][locs.length];
		
		Internationalization i18n = Internationalization.getInstance();
		L2TextBuilder tb = new L2TextBuilder();
		for (int i = 0; i < locs.length; i++)
		{
			Locale loc = locs[i];
			// page 0
			L2Htm.writeHeader(tb, i18n.get(loc, "NPC_BOUNTY_MANAGER_TITLE"));
			tb.append(i18n.get(loc, "NPC_BOUNTY_MANAGER_HELLO"));
			tb.append("<br><br>");
			tb.append(i18n.get(loc, "NPC_BOUNTY_MANAGER_BOUNTY_HUNTING_INTRO"));
			tb.append("<br1>");
			L2Htm.writeNpcButton(tb, null, BountyRequest.VIEW_LIST, 0, 250, 20, true,
					i18n.get(loc, "NPC_BOUNTY_MANAGER_VIEW_BOUNTIES"));
			tb.append(i18n.get(loc, "NPC_BOUNTY_MANAGER_BOUNTY_SETUP_INTRO"));
			tb.append("<br1>");
			L2Htm.writeNpcButton(tb, null, "Chat", 1, 250, 20, true, i18n.get(loc, "NPC_BOUNTY_MANAGER_SETUP_BOUNTY"));
			tb.append(i18n.get(loc, "NPC_BOUNTY_MANAGER_GAMBLING_INTRO"));
			tb.append("<br1>");
			{
				ItemTable it = ItemTable.getInstance();
				String item = it.getTemplate(PcInventory.ADENA_ID).getName();
				L2Htm.writeNpcButton(tb, null, /*"Chat", 2*/BountyRequest.GAMBLE, 10, 250, 20, true,
						i18n.get(loc, "NPC_BOUNTY_MANAGER_GAMBLE", "%itemName%", item, "%count%", "10"));
				L2Htm.writeNpcButton(tb, null, /*"Chat", 3*/BountyRequest.GAMBLE, 100, 250, 20, true,
						i18n.get(loc, "NPC_BOUNTY_MANAGER_GAMBLE", "%itemName%", item, "%count%", "100"));
				L2Htm.writeNpcButton(tb, null, /*"Chat", 4*/BountyRequest.GAMBLE, 500, 250, 20, true,
						i18n.get(loc, "NPC_BOUNTY_MANAGER_GAMBLE", "%itemName%", item, "%count%", "500"));
				L2Htm.writeNpcButton(tb, null, /*"Chat", 5*/BountyRequest.GAMBLE, 1000, 250, 20, true,
						i18n.get(loc, "NPC_BOUNTY_MANAGER_GAMBLE", "%itemName%", item, "%count%", "1K"));
				item = it.getTemplate(GambleManager.COIN_OF_LUCK).getName();
				L2Htm.writeNpcButton(tb, null, /*"Chat", 11*/BountyRequest.GAMBLE, 90006, 250, 20, true,
						i18n.get(loc, "NPC_BOUNTY_MANAGER_GAMBLE", "%itemName%", item, "%count%", "1"));
			}
			L2Htm.writeFooter(tb);
			
			PAGES[0][i] = tb.toString();
			tb.setLength(0);
			
			// page 1
			L2Htm.writeHeader(tb, i18n.get(loc, "NPC_BOUNTY_MANAGER_TITLE"));
			tb.append(i18n.get(loc, "NPC_BOUNTY_MANAGER_ADD_BOUNTY_NAME"));
			tb.append("<br1>");
			L2Htm.writeTextField(tb, "name", 200, 16);
			tb.append("<br>");
			tb.append(i18n.get(loc, "NPC_BOUNTY_MANAGER_ADD_BOUNTY_AMOUNT"));
			tb.append("<br1>");
			L2Htm.writeTextField(tb, "name", 90, 5);
			tb.append("<br>");
			L2Htm.writeNpcButton(tb, null, BountyRequest.ADD_BOUNTY, "$name $cost", 250, 20, true,
					i18n.get(loc, "NPC_BOUNTY_MANAGER_ADD_BOUNTY_ADD"));
			L2Htm.writeFooter(tb);
			
			PAGES[1][i] = tb.toString();
			tb.setLength(0);
			
			// page 6
			L2Htm.writeHeader(tb, i18n.get(loc, "NPC_BOUNTY_MANAGER_TITLE"));
			tb.append(i18n.get(loc, "NPC_BOUNTY_MANAGER_BOUNTY_ADDED"));
			tb.append("<br><br>");
			tb.append(i18n.get(loc, "NPC_BOUNTY_MANAGER_GAMBLING_AD"));
			tb.append("<br>");
			L2Htm.writeNpcButton(tb, null, "Chat", 0, 250, 20, true, i18n.get(loc, "BACK"));
			L2Htm.writeFooter(tb);
			
			PAGES[6][i] = tb.toString();
			tb.setLength(0);
			
			// page 7
			L2Htm.writeHeader(tb, i18n.get(loc, "NPC_BOUNTY_MANAGER_TITLE"));
			tb.append(i18n.get(loc, "NPC_BOUNTY_MANAGER_WRONG_NAME"));
			tb.append("<br><br>");
			tb.append(i18n.get(loc, "NPC_BOUNTY_MANAGER_GAMBLING_AD"));
			tb.append("<br>");
			L2Htm.writeNpcButton(tb, null, "Chat", 0, 250, 20, true, i18n.get(loc, "BACK"));
			L2Htm.writeFooter(tb);
			
			PAGES[7][i] = tb.toString();
			tb.setLength(0);
			
			// page 8
			L2Htm.writeHeader(tb, i18n.get(loc, "NPC_BOUNTY_MANAGER_TITLE"));
			tb.append(i18n.get(loc, "NPC_BOUNTY_MANAGER_WRONG_AMOUNT"));
			tb.append("<br><br>");
			tb.append(i18n.get(loc, "NPC_BOUNTY_MANAGER_GAMBLING_AD"));
			tb.append("<br>");
			L2Htm.writeNpcButton(tb, null, "Chat", 0, 250, 20, true, i18n.get(loc, "BACK"));
			L2Htm.writeFooter(tb);
			
			PAGES[8][i] = tb.toString();
			tb.setLength(0);
			
			// page 9
			L2Htm.writeHeader(tb, i18n.get(loc, "NPC_BOUNTY_MANAGER_TITLE"));
			tb.append(i18n.get(loc, "NPC_BOUNTY_MANAGER_GAMBLING_LOST"));
			tb.append("<br>");
			L2Htm.writeNpcButton(tb, null, "Chat", 0, 250, 20, true, i18n.get(loc, "BACK"));
			L2Htm.writeFooter(tb);
			
			PAGES[9][i] = tb.toString();
			tb.setLength(0);
			
			// page 10
			L2Htm.writeHeader(tb, i18n.get(loc, "NPC_BOUNTY_MANAGER_TITLE"));
			tb.append(i18n.get(loc, "NPC_BOUNTY_MANAGER_BOUNTY_SELF"));
			tb.append("<br>");
			L2Htm.writeNpcButton(tb, null, "Chat", 0, 250, 20, true, i18n.get(loc, "BACK"));
			L2Htm.writeFooter(tb);
			
			PAGES[10][i] = tb.toString();
			tb.setLength(0);
		}
		tb.setLength(0);
		tb.moveToString();
	}
}
