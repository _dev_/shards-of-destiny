/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.model.actor.instance;

import org.sod.model.PlayerLookalike;

import com.l2jfree.gameserver.model.actor.instance.AdvertiserInstance.AdvertEquip;
import com.l2jfree.gameserver.model.base.ClassId;
import com.l2jfree.gameserver.network.serverpackets.AbstractNpcInfo;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;
import com.l2jfree.util.Rnd;

/**
 * TODO Auto-generated JavaDoc
 * @author savormix
 * @since 2010.09.29
 */
public class L2CaravanLeaderInstance extends L2MonsterInstance implements PlayerLookalike
{
	public static final AdvertEquip TANK_C_FP = new AdvertEquip(0, 2414, 2503, 2497, 2463, 356, 0, 2437, 0, 0, 0);
	public static final AdvertEquip DESTRO_C_FP = new AdvertEquip(0, 2414, 5286, 0, 2463, 356, 0, 2437, 0, 0, 0);
	public static final AdvertEquip TYRANT_C_DRAKE = new AdvertEquip(0, 2414, 266, 0, 2463, 401, 0, 2437, 0, 0, 0);
	public static final AdvertEquip ARCHER_C_DRAKE = new AdvertEquip(0, 2414, 286, 0, 2463, 401, 0, 2437, 0, 0, 0);
	public static final AdvertEquip ARBA_C_DRAKE = new AdvertEquip(0, 2414, 9300, 0, 2463, 401, 0, 2437, 0, 0, 0);
	public static final AdvertEquip ZERKER_C_DRAKE = new AdvertEquip(0, 2414, 9296, 0, 2463, 401, 0, 2437, 0, 0, 0);
	public static final AdvertEquip MAGE_C_DIVINE = new AdvertEquip(0, 2414, 0, 2497, 2463, 442, 473, 2437, 0, 0, 0);
	
	private final int _race;
	private final boolean _sex;
	private final int _class;
	private final double _cr;
	private final double _ch;
	private final int _hairStyle;
	private final int _hairColor;
	private final int _face;
	private AdvertEquip _equip;
	
	public L2CaravanLeaderInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
		_equip = AdvertiserInstance.UNK;
		_race = Rnd.get(6);
		if (getRace() == 4)
			_equip = TANK_C_FP;
		_sex = Rnd.nextBoolean();
		int c1, c2;
		double cr1, ch1, cr2, ch2;
		switch (getRace())
		{
		case 0:
			c1 = ClassId.HumanFighter.getId();
			if (getSex())
			{
				cr1 = 8;
				ch1 = 23.5;
			}
			else
			{
				cr1 = 9;
				ch1 = 23;
			}
			c2 = ClassId.HumanMystic.getId();
			if (getSex())
			{
				cr2 = 6.5;
				ch2 = 22.5;
			}
			else
			{
				cr2 = 7.5;
				ch2 = 22.8;
			}
			break;
		case 1:
			c1 = ClassId.ElvenFighter.getId();
			if (getSex())
			{
				cr1 = 7.5;
				ch1 = 23;
			}
			else
			{
				cr1 = 7.5;
				ch1 = 24;
			}
			c2 = ClassId.ElvenMystic.getId();
			cr2 = cr1;
			ch2 = ch1;
			break;
		case 2:
			c1 = ClassId.DarkFighter.getId();
			if (getSex())
			{
				cr1 = 7;
				ch1 = 23.5;
			}
			else
			{
				cr1 = 7.5;
				ch1 = 24;
			}
			c2 = ClassId.DarkMystic.getId();
			cr2 = cr1;
			ch2 = ch1;
			break;
		case 3:
			c1 = ClassId.OrcFighter.getId();
			if (getSex())
			{
				cr1 = 7;
				ch1 = 27;
			}
			else
			{
				cr1 = 11;
				ch1 = 28;
			}
			c2 = ClassId.OrcMystic.getId();
			if (getSex())
			{
				cr2 = 8;
				ch2 = 25.5;
			}
			else
			{
				cr2 = 7;
				ch2 = 27.5;
			}
			break;
		case 4:
			c1 = ClassId.DwarvenFighter.getId();
			if (getSex())
			{
				cr1 = 5;
				ch1 = 19;
			}
			else
			{
				cr1 = 9;
				ch1 = 18;
			}
			c2 = c1;
			cr2 = cr1;
			ch2 = ch1;
			break;
		//case 5:
		default:
			if (getSex())
			{
				c1 = ClassId.FemaleSoldier.getId();
				cr1 = 7.5;
				ch1 = 22;
			}
			else
			{
				c1 = ClassId.MaleSoldier.getId();
				cr1 = 7.5;
				ch1 = 24;
			}
			c2 = c1;
			cr2 = cr1;
			ch2 = ch1;
			break;
		}
		if (Rnd.nextBoolean())
		{
			_class = c1;
			_cr = cr1;
			_ch = ch1;
		}
		else
		{
			_class = c2;
			_cr = cr2;
			_ch = ch2;
		}
		boolean mage = false;
		for (ClassId ci : ClassId.values())
		{
			if (ci.getId() == getClassType())
			{
				mage = ci.isMage();
				break;
			}
		}
		if (getEquip() == AdvertiserInstance.UNK)
		{
			if (mage)
				_equip = MAGE_C_DIVINE;

			// dwarves taken care of
			if (getRace() == 3)
			{
				if (Rnd.nextBoolean())
					_equip = TYRANT_C_DRAKE;
				else
					_equip = DESTRO_C_FP;
			}
			else if (getRace() == 5)
			{
				if (getSex())
					_equip = ARBA_C_DRAKE;
				else
					_equip = ZERKER_C_DRAKE;
			}
			else
				_equip = ARCHER_C_DRAKE;
		}
		if (getSex())
			_hairStyle = Rnd.get(7);
		else
			_hairStyle = Rnd.get(5);
		_hairColor = Rnd.get(4);
		_face = Rnd.get(3);
	}
	
	@Override
	public int getRace()
	{
		return _race;
	}
	
	@Override
	public boolean getSex()
	{
		return _sex;
	}
	
	@Override
	public int getClassType()
	{
		return _class;
	}
	
	@Override
	public double getCr()
	{
		return _cr;
	}
	
	@Override
	public double getCh()
	{
		return _ch;
	}
	
	@Override
	public AdvertEquip getEquip()
	{
		return _equip;
	}
	
	@Override
	public int getHairStyle()
	{
		return _hairStyle;
	}
	
	@Override
	public int getHairColor()
	{
		return _hairColor;
	}
	
	@Override
	public int getFace()
	{
		return _face;
	}
	
	@Override
	protected boolean canReplaceAI()
	{
		return false;
	}
	
	@Override
	public boolean hasRandomAnimation()
	{
		return false;
	}
	
	@Override
	public void sendInfo(L2PcInstance activeChar)
	{
		activeChar.sendPacket(new AbstractNpcInfo.CaravanInfo(this));
	}
	
	@Override
	public void broadcastFullInfoImpl()
	{
		broadcastPacket(new AbstractNpcInfo.CaravanInfo(this));
	}
}
