/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.model.actor.knownlist;

import com.l2jfree.gameserver.model.L2Object;
import com.l2jfree.gameserver.model.actor.instance.L2FenceInstance;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;

/**
 * Fence NPC is in the middle of the rectangle, but fences must be seen even if they are
 * further away.
 * @author savormix
 * @since 2010.12.08
 */
public class FenceKnownList extends NpcKnownList
{
	public FenceKnownList(L2FenceInstance fence)
	{
		super(fence);
	}
	
	@Override
	public int getDistanceToForgetObject(L2Object object)
	{
		if (!(object instanceof L2PcInstance))
			return 0;
		return 8000;
	}
	
	@Override
	public int getDistanceToWatchObject(L2Object object)
	{
		if (!(object instanceof L2PcInstance))
			return 0;
		// maximum w/h of a fence is 10k
		// max range is sqrt(10k^2/2) ~= 7071
		// but we also need it to be seen from outside, so
		return 7500;
	}
}
