/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.model.zone;

import org.sod.model.event.global.impl.LureTheGremlin;

import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.instance.L2GremlinBallInstance;

/**
 * Scoring zone for Gremlin Ball event.
 * @author savormix
 * @since 2010.10.23
 */
public class L2GremlinBallZone extends L2Zone
{
	@Override
	protected void onEnter(L2Character character)
	{
		if (character instanceof L2GremlinBallInstance)
		{
			L2GremlinBallInstance npc = (L2GremlinBallInstance) character;
			LureTheGremlin evt = npc.getEvent();
			if (evt != null)
				evt.killGremlin(npc, getQuestZoneId());
		}
	}
}
