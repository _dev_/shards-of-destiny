/*
 * MISSING LICENSING INFO
 */
package com.l2jfree.gameserver.model.actor.instance;

import java.util.Locale;

import javolution.util.FastMap;

import org.sod.L2Htm;
import org.sod.i18n.Internationalization;
import org.sod.manager.GlobalEventManager;
import org.sod.manager.GlobalEventManager.EventGroup;
import org.sod.model.event.global.GlobalEvent;
import org.sod.tutorial.TutorialManager;

import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.network.client.packets.sendable.NpcHtmlMessage;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;
import com.l2jfree.lang.L2TextBuilder;

/**
 * @author savormix
 * 
 */
public class L2FakeManagerInstance extends L2Npc
{
	public L2FakeManagerInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}
	
	@Override
	public final void onBypassFeedback(L2PcInstance player, String command)
	{
		TutorialManager.getInstance().notifyJoinEvent(player);
	}
	
	@Override
	public final void showChatWindow(L2PcInstance player, int page)
	{
		TutorialManager.getInstance().notifyEventNpcAction(this, player);
	}
	
	public void showFakeVoteWindow(L2PcInstance player)
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, i18n.get(loc, "NPC_EVENT_MANAGER_TITLE"));
		tb.append(i18n.get(loc, "NPC_EVENT_MANAGER_AVAILABILITY_DISCLAIMER"));
		tb.append("<br>");
		tb.append(i18n.get(loc, "NPC_EVENT_MANAGER_VOTING_SELECT_EVENT"));
		tb.append("<br><table>");
		
		GlobalEventManager gem = GlobalEventManager.getInstance();
		int eventNo = 0;
		for (FastMap.Entry<Integer, EventGroup> entry = gem.getEvents().head(), end = gem.getEvents().tail(); (entry = entry
				.getNext()) != end;)
		{
			EventGroup eg = entry.getValue();
			if ((eventNo & 1) == 0)
				tb.append("<tr>");
			tb.append("<td>");
			L2Htm.writeDisabledButton(tb, 135, 30, eg.getName());
			tb.append("</td>");
			if ((eventNo & 1) == 1)
				tb.append("</tr>");
			eventNo++;
		}
		if ((eventNo & 1) == 0)
			tb.append("</tr>");
		tb.append("</table>");
		L2Htm.writeFooter(tb);
		
		NpcHtmlMessage htm = new NpcHtmlMessage(getObjectId(), tb.moveToString());
		player.sendPacket(htm);
	}
	
	public void showFakeEventWindow(L2PcInstance player, GlobalEvent event)
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, i18n.get(loc, "NPC_EVENT_MANAGER_TITLE"));
		tb.append(i18n.get(loc, "NPC_EVENT_MANAGER_CURRENT_EVENT", "%eventName%",
				"<font color=\"LEVEL\">" + event.getImplementationName() + "</font>"));
		tb.append("<br>");
		L2Htm.writeNpcButton(tb, getObjectId(), "join", null, 250, 40, true,
				i18n.get(loc, "NPC_EVENT_MANAGER_JOIN_EVENT"));
		L2Htm.writeFooter(tb);
		
		NpcHtmlMessage htm = new NpcHtmlMessage(getObjectId(), tb.moveToString());
		player.sendPacket(htm);
	}
}
