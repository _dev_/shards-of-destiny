/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.model.actor.instance;

import java.util.Locale;
import java.util.StringTokenizer;

import javolution.util.FastMap;

import org.sod.L2Htm;
import org.sod.i18n.Internationalization;
import org.sod.manager.RespawnManager;
import org.sod.manager.conquest.ConquestFactionManager;
import org.sod.manager.conquest.ConquestFactionManager.FactionTemplate;
import org.sod.manager.conquest.ConquestRestriction;
import org.sod.manager.conquest.ConquestZoneManager;
import org.sod.manager.conquest.ConquestZoneManager.ZoneData;

import com.l2jfree.gameserver.communitybbs.Manager.SettingsBBSManager;
import com.l2jfree.gameserver.model.Location;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.restriction.global.GlobalRestrictions;
import com.l2jfree.gameserver.model.zone.L2FactionZone;
import com.l2jfree.gameserver.model.zone.L2Zone.RestartType;
import com.l2jfree.gameserver.network.SystemMessageId;
import com.l2jfree.gameserver.network.client.packets.sendable.NpcHtmlMessage;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;
import com.l2jfree.lang.L2TextBuilder;

/**
 * A NPC instance that displays zone information and allows
 * teleportation to predefined areas in (around) the zone.
 * 
 * @author savormix
 * @since 2010.01.07
 */
public final class L2ZoneManagerInstance extends L2Npc
{
	public static volatile boolean SUSPEND = false;
	
	private static final int REFRESH_INTERVAL = 1100;
	private static final CachedChatHolder CCH = new CachedChatHolder();
	
	private static void append(CharSequence cs, L2TextBuilder... tbs)
	{
		for (L2TextBuilder tb : tbs)
			tb.append(cs);
	}
	
	private static void append(int i, L2TextBuilder... tbs)
	{
		for (L2TextBuilder tb : tbs)
			tb.append(i);
	}
	
	private static void writeGenericHeader(Locale loc, L2TextBuilder... tbs)
	{
		ConquestFactionManager cfm = ConquestFactionManager.getInstance();
		FactionTemplate a = cfm.getFactionTemplate(ConquestFactionManager.ANCIENTS);
		FactionTemplate d = cfm.getFactionTemplate(ConquestFactionManager.DIVINES);
		Internationalization i18n = Internationalization.getInstance();
		for (L2TextBuilder tb : tbs)
		{
			L2Htm.writeHeader(tb, i18n.get(loc, "CONQUEST_TITLE"));
			tb.append("<font color=\"");
			tb.append(Integer.toHexString(a.getColor()));
			tb.append("\">");
			tb.append(a.getName(loc));
			tb.append("</font> vs <font color=\"");
			tb.append(Integer.toHexString(d.getColor()));
			tb.append("\">");
			tb.append(d.getName(loc));
			tb.append("</font><br><br>");
		}
	}
	
	private static void writeGenericFooter(Locale loc, L2TextBuilder... tbs)
	{
		for (L2TextBuilder tb : tbs)
		{
			tb.append("<br1><img src=\"L2UI_CH3.herotower_deco\" width=\"256\" height=\"32\">");
			L2Htm.writeFooter(tb);
		}
	}
	
	public L2ZoneManagerInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}
	
	@Override
	public final void showChatWindow(L2PcInstance player, int page)
	{
		NpcHtmlMessage htm = new NpcHtmlMessage(getObjectId());
		final int loc = player.getSoDPlayer().getSettings().getLocaleIndex();
		if (SUSPEND)
		{
			htm.setHtml(CCH.getSuspended()[loc]);
			player.sendPacket(htm);
			return;
		}
		
		if (CCH.needsUpdate())
			buildMessages();
		
		htm.setHtml(CCH.getOverview()[player.getConquestFaction()][loc]);
		htm.replace("%objectId%", getObjectId());
		player.sendPacket(htm);
	}
	
	@Override
	public void onBypassFeedback(L2PcInstance player, String command)
	{
		StringTokenizer st = new StringTokenizer(command, " ");
		ConquestRequest cr = L2Htm.getRequest(st, ConquestRequest.values());
		if (cr == null)
			return;
		
		ConquestFactionManager cfm = ConquestFactionManager.getInstance();
		ConquestZoneManager czm = ConquestZoneManager.getInstance();
		
		if (cr == ConquestRequest.JOIN)
		{
			if (!czm.isActive())
			{
				showChatWindow(player);
				return;
			}
			
			if (GlobalRestrictions.isRestricted(player, ConquestRestriction.class))
				return;
			
			int pref = player.getSoDPlayer().getDualismFaction();
			try
			{
				pref = Integer.parseInt(st.nextToken());
			}
			catch (RuntimeException e)
			{
				// nothing
			}
			
			cfm.addMember(player.getSoDPlayer(), pref);
			return;
		}
		
		if (!cfm.isPlaying(player))
			return;
		
		if (SUSPEND)
		{
			showChatWindow(player);
			return;
		}
		
		switch (cr)
		{
		case VIEW_ALL:
			showChatWindow(player);
			break;
		case VIEW_ZONE:
			int id = 0;
			try
			{
				id = Integer.parseInt(st.nextToken());
			}
			catch (RuntimeException e)
			{
				return;
			}
			
			L2FactionZone zone = czm.getFactionZone(id);
			if (zone.isLocked())
			{
				sendLockedInfo(player);
				return;
			}
			
			if (cfm.isSameFaction(player, zone))
				sendZoneInfo(player, zone);
			else
				sendTeleInfo(player, zone);
			break;
		case TELEPORT:
			if (!GlobalRestrictions.canTeleport(player))
				return;
			
			int zoneId;
			try
			{
				zoneId = Integer.parseInt(st.nextToken());
			}
			catch (RuntimeException e)
			{
				return;
			}
			
			zone = czm.getFactionZone(zoneId);
			if (zone.isLocked())
			{
				sendLockedInfo(player);
				return;
			}
			
			Location loc = null;
			if (!cfm.isSameFaction(player, zone))
			{
				int teleId = 1;
				try
				{
					teleId = Integer.parseInt(st.nextToken());
				}
				catch (Exception e)
				{
					return;
				}
				Location[] l = zone.getRestartPoints(RestartType.CHAOTIC);
				if (l != null && teleId < l.length)
					loc = l[teleId];
			}
			else
				loc = zone.getRestartPoint(RestartType.OWNER);
			
			if (loc == null)
			{
				player.sendPacket(SystemMessageId.NOT_WORKING_PLEASE_TRY_AGAIN_LATER);
				return;
			}
			player.teleToLocation(loc);
			player.setInstanceId(0);
			break;
		case LEAVE:
			if (ConquestFactionManager.getInstance().removeMember(player.getSoDPlayer()))
				RespawnManager.getInstance().moveToSpawn(player);
			break;
		default:
			_log.warn("Unhandled zone manager's request: " + cr);
			break;
		}
	}
	
	private void sendLockedInfo(L2PcInstance player)
	{
		NpcHtmlMessage htm = new NpcHtmlMessage(getObjectId(), CCH.getLocked()[player.getSoDPlayer().getSettings()
				.getLocaleIndex()]);
		player.sendPacket(htm);
	}
	
	private void sendZoneInfo(L2PcInstance player, L2FactionZone z)
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		
		NpcHtmlMessage msg = new NpcHtmlMessage(getObjectId());
		L2TextBuilder tb = new L2TextBuilder();
		writeGenericHeader(loc, tb);
		tb.append("<font color=\"LEVEL\">");
		tb.append(z.getName());
		
		{
			ZoneData zd = z.getZoneData();
			tb.append("</font><br>");
			tb.append(i18n.get(loc, "CONQUEST_ZONE_CP_REGENERATION", "%cpReg%",
					String.valueOf(zd.getRegenLevel(ConquestZoneManager.REGEN_CP))));
			tb.append("<br>");
			tb.append(i18n.get(loc, "CONQUEST_ZONE_HP_REGENERATION", "%hpReg%",
					String.valueOf(zd.getRegenLevel(ConquestZoneManager.REGEN_HP))));
			tb.append("<br>");
			tb.append(i18n.get(loc, "CONQUEST_ZONE_MP_REGENERATION", "%mpReg%",
					String.valueOf(zd.getRegenLevel(ConquestZoneManager.REGEN_MP))));
		}
		tb.append("<br>");
		tb.append(i18n.get(loc, "CONQUEST_ZONE_TOTAL_PLAYERS", "%total%", String.valueOf(z.getPlayersInside())));
		tb.append("<br>");
		tb.append(z.getId());
		L2Htm.writeNpcAction(tb, getObjectId(), ConquestRequest.TELEPORT, z.getId(), true,
				i18n.get(loc, "CONQUEST_ZONE_TELEPORT_DEFEND"));
		tb.append("<br>");
		L2Htm.writeNpcAction(tb, getObjectId(), ConquestRequest.VIEW_ALL, null, true, i18n.get(loc, "BACK"));
		writeGenericFooter(loc, tb);
		msg.setHtml(tb.moveToString());
		player.sendPacket(msg);
	}
	
	private void sendTeleInfo(L2PcInstance player, L2FactionZone z)
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		
		NpcHtmlMessage msg = new NpcHtmlMessage(getObjectId());
		L2TextBuilder tb = new L2TextBuilder();
		writeGenericHeader(loc, tb);
		tb.append("<font color=\"LEVEL\">");
		tb.append(z.getName());
		tb.append("</font><br>");
		Location[] locs = z.getRestartPoints(RestartType.CHAOTIC);
		if (locs != null)
		{
			for (int i = 0; i < locs.length; i++)
			{
				L2Htm.writeNpcAction(tb, getObjectId(), ConquestRequest.TELEPORT, z.getId() + " " + i, true,
						i18n.get(loc, "CONQUEST_ZONE_TELEPORT_ATTACK", "%no%", String.valueOf(i + 1)));
			}
		}
		tb.append("<br>");
		L2Htm.writeNpcAction(tb, getObjectId(), ConquestRequest.VIEW_ALL, null, true, i18n.get(loc, "BACK"));
		writeGenericFooter(loc, tb);
		msg.setHtml(tb.moveToString());
		player.sendPacket(msg);
	}
	
	private static String getIndicator(L2FactionZone zone, Locale loc, boolean enemy)
	{
		Internationalization i18n = Internationalization.getInstance();
		StringBuilder sb = new StringBuilder(" (");
		if (zone.isLocked())
			sb.append(i18n.get(loc, "CONQUEST_ZONE_INDICATOR_LOCKED"));
		else if (zone.isSealing())
			sb.append(i18n.get(loc, enemy ? "CONQUEST_ZONE_INDICATOR_ATTACKING"
					: "CONQUEST_ZONE_INDICATOR_UNDER_ATTACK"));
		else if (zone.isPeaceful())
			sb.append(i18n.get(loc, "CONQUEST_ZONE_INDICATOR_PEACEFUL"));
		else
			sb.append(i18n.get(loc, "CONQUEST_ZONE_INDICATOR_PVP"));
		sb.append(')');
		
		return sb.toString();
	}
	
	private static final void buildMessages()
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale[] locs = SettingsBBSManager.getInstance().getAllLocales();
		ConquestZoneManager czm = ConquestZoneManager.getInstance();
		{
			L2TextBuilder tb = new L2TextBuilder();
			for (int i = 0; i < locs.length; i++)
			{
				Locale loc = locs[i];
				writeGenericHeader(loc, tb);
				if (czm.isActive())
				{
					tb.append(i18n.get(loc, "CONQUEST_ACTIVE"));
					L2Htm.writeNpcAction(tb, null, ConquestRequest.JOIN, null, true, i18n.get(loc, "CONQUEST_JOIN"));
				}
				else
					tb.append(i18n.get(loc, "CONQUEST_LOCKED"));
				tb.append("<br1>");
				writeGenericFooter(loc, tb);
				CCH.getOverview()[ConquestFactionManager.NONE][i] = tb.toString();
				tb.clear();
			}
			L2TextBuilder.recycle(tb);
		}
		
		ConquestFactionManager cfm = ConquestFactionManager.getInstance();
		FactionTemplate a = cfm.getFactionTemplate(ConquestFactionManager.ANCIENTS);
		FactionTemplate d = cfm.getFactionTemplate(ConquestFactionManager.DIVINES);
		
		L2TextBuilder tbe = new L2TextBuilder(), tbw = new L2TextBuilder();
		int eastZones = 0, westZones = 0;
		FastMap<Integer, L2FactionZone> zones = czm.getZoneMap();
		for (FastMap.Entry<Integer, L2FactionZone> entry = zones.head(), end = zones.tail(); (entry = entry.getNext()) != end;)
		{
			L2FactionZone zone = entry.getValue();
			switch (zone.getConquestFaction())
			{
			case ConquestFactionManager.ANCIENTS:
				eastZones++;
				break;
			case ConquestFactionManager.DIVINES:
				westZones++;
				break;
			default:
				break;
			}
		}
		
		final FastMap<Integer, L2FactionZone> map = czm.getZoneMap();
		for (int i = 0; i < locs.length; i++)
		{
			Locale loc = locs[i];
			writeGenericHeader(loc, tbe, tbw);
			StringBuilder sb = new StringBuilder("<font color=\"");
			sb.append(Integer.toHexString(a.getColor()));
			sb.append("\">");
			sb.append(a.getName(loc));
			sb.append("</font>");
			append(i18n.get(loc, "CONQUEST_OWNED_ZONES", "%factionName%", sb.toString()), tbe, tbw);
			append(" ", tbe, tbw);
			append(eastZones, tbe, tbw);
			append("<br1>", tbe, tbw);
			sb.setLength(0);
			sb.append("<font color=\"");
			sb.append(Integer.toHexString(d.getColor()));
			sb.append("\">");
			sb.append(d.getName(loc));
			sb.append("</font>");
			append(i18n.get(loc, "CONQUEST_OWNED_ZONES", "%factionName%", sb.toString()), tbe, tbw);
			append(" ", tbe, tbw);
			append(westZones, tbe, tbw);
			append("<br>", tbe, tbw);
			append(i18n.get(loc, "CONQUEST_SELECT_ZONE"));
			append("<br>", tbe, tbw);
			
			for (FastMap.Entry<Integer, L2FactionZone> entry = map.head(), end = map.tail(); (entry = entry.getNext()) != end;)
			{
				L2FactionZone zone = entry.getValue();
				if (zone.isLocked())
					continue;
				
				final int owner = zone.getConquestFaction();
				boolean writeE = owner == ConquestFactionManager.ANCIENTS;
				append("<font color=\"", tbe, tbw);
				FactionTemplate ft = cfm.getFactionTemplate(owner);
				if (ft != null)
					append(Integer.toHexString(ft.getColor()), tbe, tbw);
				else
					append("ffffff", tbe, tbw);
				append("\">", tbe, tbw);
				if (!zone.isLocked())
				{
					L2Htm.writeNpcAction(tbe, null, ConquestRequest.VIEW_ZONE, zone.getId(), true, zone.getName(),
							getIndicator(zone, loc, writeE));
					L2Htm.writeNpcAction(tbw, null, ConquestRequest.VIEW_ZONE, zone.getId(), true, zone.getName(),
							getIndicator(zone, loc, !writeE));
				}
				else
					append(zone.getName(), tbe, tbw);
				append("</font><br1>", tbe, tbw);
			}
			append("<br>", tbe, tbw);
			L2Htm.writeNpcAction(tbe, null, ConquestRequest.LEAVE, null, true, i18n.get(loc, "CONQUEST_LEAVE"));
			L2Htm.writeNpcAction(tbw, null, ConquestRequest.LEAVE, null, true, i18n.get(loc, "CONQUEST_LEAVE"));
			append("<br1>", tbe, tbw);
			writeGenericFooter(loc, tbe, tbw);
			
			CCH.getOverview()[ConquestFactionManager.ANCIENTS][i] = tbe.toString();
			CCH.getOverview()[ConquestFactionManager.DIVINES][i] = tbw.toString();
			tbe.clear();
			tbw.clear();
		}
		L2TextBuilder.recycle(tbe);
		L2TextBuilder.recycle(tbw);
	}
	
	private static class CachedChatHolder
	{
		private final String[] _suspended;
		private final String[] _locked;
		private final String[][] _overview;
		private volatile long _nextUpdate;
		
		public CachedChatHolder()
		{
			final int locs = SettingsBBSManager.getInstance().getAllLocales().length;
			_suspended = new String[locs];
			_locked = new String[locs];
			_overview = new String[ConquestFactionManager.DIVINES + 1][locs];
			buildStatic();
		}
		
		private void buildStatic()
		{
			L2TextBuilder tbs = new L2TextBuilder(), tbl = new L2TextBuilder();
			Internationalization i18n = Internationalization.getInstance();
			Locale[] locs = SettingsBBSManager.getInstance().getAllLocales();
			for (int i = 0; i < locs.length; i++)
			{
				Locale loc = locs[i];
				writeGenericHeader(loc, tbs, tbl);
				tbs.append(i18n.get(loc, "CONQUEST_RESET_IN_PROGRESS"));
				tbl.append(i18n.get(loc, "CONQUEST_THIS_ZONE_IS_LOCKED"));
				append("<br1>", tbs, tbl);
				writeGenericFooter(loc, tbs, tbl);
				_suspended[i] = tbs.toString();
				tbs.clear();
				_locked[i] = tbl.toString();
				tbl.clear();
			}
			L2TextBuilder.recycle(tbs);
			L2TextBuilder.recycle(tbl);
		}
		
		public boolean needsUpdate()
		{
			if (_nextUpdate > System.currentTimeMillis())
				return false;
			
			_nextUpdate = System.currentTimeMillis() + REFRESH_INTERVAL;
			return true;
		}
		
		public String[] getSuspended()
		{
			return _suspended;
		}
		
		public String[] getLocked()
		{
			return _locked;
		}
		
		public String[][] getOverview()
		{
			return _overview;
		}
	}
	
	private enum ConquestRequest
	{
		JOIN, LEAVE, VIEW_ZONE, VIEW_ALL, TELEPORT
	}
}
