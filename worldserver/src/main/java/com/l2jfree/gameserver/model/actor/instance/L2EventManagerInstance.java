/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.model.actor.instance;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.sod.L2Htm;
import org.sod.i18n.Internationalization;
import org.sod.manager.GlobalEventManager;
import org.sod.model.event.EventRequest;

import com.l2jfree.Config;
import com.l2jfree.gameserver.communitybbs.Manager.req.SetupRequest;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.network.client.packets.sendable.NpcHtmlMessage;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;
import com.l2jfree.lang.L2TextBuilder;

/**
 * TODO Auto-generated JavaDoc
 * 
 * @author savormix
 * @since 2010.12.10
 */
public class L2EventManagerInstance extends L2Npc
{
	public L2EventManagerInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}
	
	@Override
	public final void showChatWindow(L2PcInstance player, int page)
	{
		final int state = GlobalEventManager.getInstance().getState();
		switch (state)
		{
		case GlobalEventManager.STATUS_IDLE:
			showIdle(player);
			break;
		case GlobalEventManager.STATUS_VOTING:
			showVoting(player);
			break;
		case GlobalEventManager.STATUS_ACTIVE:
			showEvent(player);
			break;
		default:
			_log.warn("Unknown global event manager state: " + state);
			break;
		}
	}
	
	private void showIdle(L2PcInstance player)
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, i18n.get(loc, "NPC_EVENT_MANAGER_TITLE"));
		if (Config.AUTO_EVENTS)
			tb.append(i18n.get(loc, "NPC_EVENT_MANAGER_IDLE_WILL_RESTART"));
		else
			tb.append(i18n.get(loc, "NPC_EVENT_MANAGER_IDLE"));
		L2Htm.writeFooter(tb);
		
		NpcHtmlMessage htm = new NpcHtmlMessage(getObjectId(), tb.moveToString());
		player.sendPacket(htm);
	}
	
	private void showVoting(L2PcInstance player)
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, i18n.get(loc, "NPC_EVENT_MANAGER_TITLE"));
		tb.append(i18n.get(loc, "NPC_EVENT_MANAGER_VOTING_IN_PROGRESS"));
		tb.append("<br1>");
		L2Htm.writeButton(tb, "ge", EventRequest.SHOW_VOTING, 250, 30, true, i18n.get(loc, "NPC_EVENT_MANAGER_VOTE"));
		L2Htm.writeButton(tb, "_bbsgetfav;" + SetupRequest.GE_PRELIMINARY.ordinal(), null, 250, 30, true,
				i18n.get(loc, "NPC_EVENT_MANAGER_VOTING_RESULTS"));
		GlobalEventManager gem = GlobalEventManager.getInstance();
		if (gem.getWinners().contains(player.getObjectId()))
		{
			tb.append("<br>");
			tb.append(i18n.get(loc, "NPC_EVENT_MANAGER_CURRENT_EVENT", "%eventName%", "<font color=\"LEVEL\">"
					+ gem.getMiniEvent().getName() + "</font>"));
			tb.append("<br1>");
			L2Htm.writeButton(tb, "ge", EventRequest.JOIN_MINI_EVENT, 250, 30, true,
					i18n.get(loc, "NPC_EVENT_MANAGER_JOIN_EXCLUSIVE_EVENT"));
		}
		L2Htm.writeFooter(tb);
		
		NpcHtmlMessage htm = new NpcHtmlMessage(getObjectId(), tb.moveToString());
		player.sendPacket(htm);
	}
	
	private void showEvent(L2PcInstance player)
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		GlobalEventManager gem = GlobalEventManager.getInstance();
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, i18n.get(loc, "NPC_EVENT_MANAGER_TITLE"));
		tb.append(i18n.get(loc, "NPC_EVENT_MANAGER_CURRENT_EVENT", "%eventName%", "<font color=\"LEVEL\">"
				+ gem.getEvent().getName() + "</font>"));
		tb.append("<br>");
		{
			String left = i18n.get(loc, "NPC_EVENT_MANAGER_TIME_LEFT");
			int time = gem.getTimeLeft(TimeUnit.MINUTES);
			if (time > 0)
			{
				left = left.replace("%time%", String.valueOf(time));
				left = left.replace("%unit%", i18n.get(loc, "TIME_MINUTE"));
			}
			else
			{
				left = left.replace("%time%", String.valueOf(gem.getTimeLeft(TimeUnit.SECONDS)));
				left = left.replace("%unit%", i18n.get(loc, "TIME_SECOND"));
			}
			tb.append(left);
		}
		tb.append("<br>");
		L2Htm.writeButton(tb, "ge", EventRequest.JOIN_EVENT, 250, 40, true,
				i18n.get(loc, "NPC_EVENT_MANAGER_JOIN_EVENT"));
		L2Htm.writeFooter(tb);
		
		NpcHtmlMessage htm = new NpcHtmlMessage(getObjectId(), tb.moveToString());
		player.sendPacket(htm);
	}
}
