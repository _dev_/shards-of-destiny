/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.model.actor.instance;

import org.sod.manager.conquest.ConquestFactionManager;

import com.l2jfree.gameserver.ai.CtrlIntention;
import com.l2jfree.gameserver.model.L2Clan;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.status.CharStatus;
import com.l2jfree.gameserver.model.actor.status.SiegeFlagStatus;
import com.l2jfree.gameserver.network.SystemMessageId;
import com.l2jfree.gameserver.network.client.packets.sendable.ActionFail.InteractionFinished;
import com.l2jfree.gameserver.network.serverpackets.StatusUpdate;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;

public final class L2SiegeFlagInstance extends L2Npc
{
	private final L2Clan _clan;
	private final boolean _isAdvanced;
	private long _talkProtectionTime;

	public L2SiegeFlagInstance(L2PcInstance player, int objectId, L2NpcTemplate template, boolean advanced)
	{
		super(objectId, template);

		setIsInvul(false);

		_isAdvanced = advanced;
		_talkProtectionTime = 0;

		if (player == null || player.getClan() == null)
		{
			deleteMe();
			_clan = null;
			return;
		}
		else
			_clan = player.getClan();

		try
		{
			_clan.addFlag(this);
			ConquestFactionManager.getInstance().addHQ(this);
		}
		catch (Exception e)
		{
			deleteMe();
		}
	}

	@Override
	public boolean isAttackable()
	{
		return true;
	}

	@Override
	public boolean isAutoAttackable(L2Character attacker)
	{
		L2PcInstance player = attacker.getActingPlayer();
		if (player == null || player.getClan() != _clan)
			return true;
		return false;
	}

	@Override
	public boolean doDie(L2Character killer)
	{
		if (!super.doDie(killer))
			return false;

		_clan.removeFlag(this);
		ConquestFactionManager.getInstance().remHQ(this);
		return true;
	}

	@Override
	public void onForcedAttack(L2PcInstance player)
	{
		if (player == null || !canTarget(player))
			return;

		// Check if the L2PcInstance already target the L2NpcInstance
		if (this != player.getTarget())
		{
			// Set the target of the L2PcInstance player
			player.setTarget(this);

			// Send a Server->Client packet StatusUpdate of the L2NpcInstance to the L2PcInstance to update its HP bar
			StatusUpdate su = new StatusUpdate(getObjectId());
			su.addAttribute(StatusUpdate.CUR_HP, (int) getStatus().getCurrentHp());
			su.addAttribute(StatusUpdate.MAX_HP, getMaxHp());
			player.sendPacket(su);
		}
		else
		{
			if (Math.abs(player.getZ() - getZ()) < 100)
				player.getAI().setIntention(CtrlIntention.AI_INTENTION_ATTACK, this);
			else
			{
				// Send a Server->Client ActionFailed to the L2PcInstance in order to avoid that the client wait another packet
				player.sendPacket(InteractionFinished.PACKET);
			}
		}
	}

	@Override
	public void onAction(L2PcInstance player)
	{
		if (player == null || !canTarget(player))
			return;

		// Check if the L2PcInstance already target the L2NpcInstance
		if (this != player.getTarget())
		{
			// Set the target of the L2PcInstance player
			player.setTarget(this);

			// Send a Server->Client packet StatusUpdate of the L2NpcInstance to the L2PcInstance to update its HP bar
			StatusUpdate su = new StatusUpdate(getObjectId());
			su.addAttribute(StatusUpdate.CUR_HP, (int) getStatus().getCurrentHp());
			su.addAttribute(StatusUpdate.MAX_HP, getMaxHp());
			player.sendPacket(su);
		}
		else
		{
			if (isAutoAttackable(player) && Math.abs(player.getZ() - getZ()) < 100)
				player.getAI().setIntention(CtrlIntention.AI_INTENTION_ATTACK, this);
			else
			{
				// Send a Server->Client ActionFailed to the L2PcInstance in order to avoid that the client wait another packet
				player.sendPacket(InteractionFinished.PACKET);
			}
		}
	}

	public void flagAttacked()
	{
		// send warning to owners of headquarters that theirs base is under attack
		if (canTalk())
			_clan.broadcastToOnlineMembers(SystemMessageId.BASE_UNDER_ATTACK.getSystemMessage());
		
		_talkProtectionTime = System.currentTimeMillis() + 20000;
	}
	
	public boolean canTalk()
	{
		return System.currentTimeMillis() > _talkProtectionTime;
	}
	
	@Override
	protected CharStatus initStatus()
	{
		return new SiegeFlagStatus(this);
	}
	
	@Override
	public SiegeFlagStatus getStatus()
	{
		return (SiegeFlagStatus)_status;
	}
	
	public boolean isAdvanced()
	{
		return _isAdvanced;
	}
}
