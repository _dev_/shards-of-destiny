/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.model.zone;

import java.util.Locale;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.atomic.AtomicInteger;

import javolution.util.FastSet;

import org.sod.Skills;
import org.sod.i18n.Internationalization;
import org.sod.manager.RespawnManager;
import org.sod.manager.conquest.ConquestFactionManager;
import org.sod.manager.conquest.ConquestZoneManager;
import org.sod.manager.conquest.ConquestZoneManager.ZoneData;
import org.sod.model.conquest.ConquestFactionMember;

import com.l2jfree.gameserver.ThreadPoolManager;
import com.l2jfree.gameserver.ai.CtrlIntention;
import com.l2jfree.gameserver.datatables.NpcTable;
import com.l2jfree.gameserver.datatables.SkillTable;
import com.l2jfree.gameserver.instancemanager.CastleManager;
import com.l2jfree.gameserver.instancemanager.FortManager;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.L2Spawn;
import com.l2jfree.gameserver.model.Location;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.L2Playable;
import com.l2jfree.gameserver.model.actor.instance.L2ControllableMobInstance;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.actor.instance.L2ZoneArtefactInstance;
import com.l2jfree.gameserver.model.actor.instance.L2ZoneGuardInstance;
import com.l2jfree.gameserver.model.actor.instance.L2ZoneServitorInstance;
import com.l2jfree.gameserver.model.actor.instance.L2ZoneSpawnGuardInstance;
import com.l2jfree.gameserver.model.actor.status.PcStatus;
import com.l2jfree.gameserver.network.SystemMessageId;
import com.l2jfree.gameserver.network.client.packets.L2ServerPacket;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.Chat;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.ChatMessage;
import com.l2jfree.gameserver.network.client.packets.sendable.SystemMessagePacket.SystemMessage;
import com.l2jfree.gameserver.skills.AbnormalEffect;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;
import com.l2jfree.gameserver.util.Broadcast;
import com.l2jfree.util.Rnd;

/**
 * This class represents a faction-ownable zone. The zone manages it's
 * zone guard NPCs and deals with players currently in zone.
 * 
 * @see #checkForDamage(L2Character)
 * @see #onEnter(L2Character)
 * @see #onExit(L2Character)
 * @author savormix
 * @since 2009.12.01
 */
public class L2FactionZone extends L2DynamicZone implements ConquestFactionMember
{
	private static final int SECURITY_COOLDOWN = 60000;
	private static final int PEACE_COOLDOWN = 45000;
	private static final int ABNORMAL_CHANGING_DELAY = 10000;
	private static final int INTRUDER_WARN_DELAY = 15000;
	
	private ZoneData _data;
	private final FastSet<L2ZoneGuardInstance> _guards;
	private final FastSet<L2PcInstance> _donors;
	private final FastSet<L2PcInstance> _awakers;
	private final FastSet<Integer> _seal;
	private final AtomicInteger _players;
	private final AtomicInteger _defenders;
	
	private L2ZoneArtefactInstance _core;
	private L2ZoneGuardInstance _servitor;
	private transient volatile long _secured;
	private transient volatile long _peace;
	private volatile ScheduledFuture<?> _abnTask;
	private transient volatile long _nextAlert;
	private volatile boolean _locked;
	private volatile boolean _marked;
	
	public L2FactionZone()
	{
		_guards = FastSet.newInstance();
		_donors = FastSet.newInstance();
		_awakers = FastSet.newInstance();
		_seal = FastSet.newInstance();
		_players = new AtomicInteger();
		_defenders = new AtomicInteger();
		_servitor = null;
	}
	
	@Override
	protected void register()
	{
		_data = ConquestZoneManager.getInstance().register(this);
	}
	
	@Override
	protected void onEnter(L2Character character)
	{
		if (character instanceof L2ZoneGuardInstance)
			assign((L2ZoneGuardInstance) character);
		else if (character instanceof L2ZoneSpawnGuardInstance)
			((L2ZoneSpawnGuardInstance) character).setFactionZone(this);
		else if (character instanceof L2ZoneArtefactInstance)
		{
			_core = (L2ZoneArtefactInstance) character;
			_core.setFactionZone(this);
		}
		else if (character instanceof L2PcInstance)
		{
			_players.incrementAndGet();
			character.setInsideZone(FLAG_SIEGE, true);
			character.setInsideZone(FLAG_NO_HQ, true);
			L2PcInstance player = character.getActingPlayer();
			if (isEnemy(player))
			{
				if (isDangerous())
				{
					character.startAbnormalEffect(_data.getAbnormal());
					character.setInsideZone(FLAG_DANGER, true);
					character.setInsideZone(FLAG_ALTERED, true);
					player.sendEtcStatusUpdate();
				}
				// something if it's enemy
			}
			else
				_defenders.incrementAndGet();
		}
		else if (character instanceof L2Npc && character.getInstanceId() == 0
				&& !(character instanceof L2ControllableMobInstance))
		{
			character.setInstanceId(RespawnManager.getInstance().getBase().getInstanceId());
			
			L2Spawn s = ((L2Npc) character).getSpawn();
			for (int faction = ConquestFactionManager.ANCIENTS; faction <= ConquestFactionManager.DIVINES; faction++)
			{
				s.setInstanceId(ConquestFactionManager.getInstance().getFactionTemplate(faction).getInstanceId());
				s.doSpawn();
			}
			s.setInstanceId(0);
		}
		
		super.onEnter(character);
	}
	
	@Override
	protected void onExit(L2Character character)
	{
		if (!character.isDead() && character instanceof L2ZoneGuardInstance)
		{
			((L2ZoneGuardInstance) character).clearAggroList();
			// character.teleToLocation(getRestartPoint(RestartType.OWNER), true);
			character.teleToLocation(getRandomLocation(), false);
		}
		
		notifyTargetLeave(character);
		
		if (character instanceof L2PcInstance)
		{
			_players.decrementAndGet();
			character.setInsideZone(FLAG_SIEGE, false);
			character.setInsideZone(FLAG_NO_HQ, false);
			L2PcInstance player = character.getActingPlayer();
			if (isEnemy(player))
			{
				if (isDangerous())
				{
					character.stopAbnormalEffect(_data.getAbnormal());
					character.setInsideZone(FLAG_DANGER, false);
					character.setInsideZone(FLAG_ALTERED, false);
					player.sendEtcStatusUpdate();
				}
			}
			else
				_defenders.decrementAndGet();
		}
		
		super.onExit(character);
	}
	
	/**
	 * Each tick, this method is called for each character inside zone.<BR>
	 * <BR>
	 * If character is an enemy and HP/MP damage is enabled, then apply DOT.<BR>
	 * If character is from the owner faction and CP/HP/MP regen is enabled,
	 * apply the recovery.<BR>
	 * TODO: Since the tick is 3s, perhaps we don't need to send messages?
	 * 
	 * @param character
	 *            which is inside this zone
	 */
	@Override
	protected void checkForDamage(L2Character character)
	{
		if (isLocked() || !(character instanceof L2Playable))
			return;
		
		L2PcInstance owner = character.getActingPlayer();
		if (isEnemy(owner))
		{
			if (_servitor != null)
				_servitor.addDamageHate(character, (int) (500 - Math.log(_servitor.getPlanDistanceSq(character))), 1);
			for (FastSet.Record r = _guards.head(), end = _guards.tail(); (r = r.getNext()) != end;)
			{
				L2ZoneGuardInstance zg = _guards.valueOf(r);
				if (!zg.isDead())
					zg.addDamageHate(character, (int) (500 - Math.log(zg.getPlanDistanceSq(character))), 1);
			}
			
			if (!isDangerous())
				return;
			
			double dmg = Math.min(ConquestZoneManager.calculateDamage(_data, character), character.getCurrentHp() - 1);
			// SystemMessage sm;
			if (dmg > 0)
			{
				owner.reduceCurrentHp(dmg, character);
				// sm = new SystemMessage(SystemMessageId.C1_RECEIVED_DAMAGE_FROM_S2_THROUGH_FIRE_OF_MAGIC);
				// sm.addCharName(character);
				// sm.addNumber(dmg);
				// owner.sendPacket(sm);
			}
			dmg = Math.min(ConquestZoneManager.calculateMpDamage(_data, character), character.getCurrentMp() - 1);
			if (dmg > 0)
			{
				owner.reduceCurrentMp(dmg);
				// if (character instanceof L2PcInstance)
				// {
				// sm = new SystemMessage(SystemMessageId.MP_REDUCED_BY_S1);
				// sm.addNumber(dmg);
				// owner.sendPacket(sm);
				// }
			}
		}
		else
		{
			if (!isRegen())
				return;
			
			PcStatus status = owner.getStatus();
			double reg = ConquestZoneManager.calculateRegen(ConquestZoneManager.REGEN_CP, _data);
			if (reg > 0)
				status.setCurrentCp(status.getCurrentCp() + reg);
			reg = ConquestZoneManager.calculateRegen(ConquestZoneManager.REGEN_HP, _data);
			if (reg > 0)
			{
				status.increaseHp(reg);
				// if (character instanceof L2PcInstance)
				// owner.sendPacket(SystemMessageId.REJUVENATING_HP);
			}
			reg = ConquestZoneManager.calculateRegen(ConquestZoneManager.REGEN_MP, _data);
			if (reg > 0)
			{
				status.increaseMp(reg);
				// if (character instanceof L2PcInstance)
				// owner.sendPacket(SystemMessageId.REJUVENATING_MP);
			}
		}
	}
	
	@Override
	public int getConquestFaction()
	{
		return _data.getOwnerFaction();
	}
	
	/**
	 * Tests if the specified PlayerControllable's owner is not a member
	 * of the zone owner faction.
	 * 
	 * @param pc
	 *            a playable instance
	 * @return true if it's an enemy, false if friend
	 */
	public final boolean isEnemy(L2Character cha)
	{
		return !ConquestFactionManager.getInstance().isSameFaction(this, cha);
	}
	
	/** @return whether HP or MP damage is enabled */
	public boolean isDangerous()
	{
		return (_data.getDamageLevel(false) > 0 || _data.getDamageLevel(true) > 0);
	}
	
	/** @return whether CP, HP or MP recovery is enabled */
	private boolean isRegen()
	{
		for (int i = 0; i < 3; i++)
			if (_data.getRegenLevel(i) > 0)
				return true;
		return false;
	}
	
	/**
	 * Changes the zone's owner.<BR>
	 * <BR>
	 * Kill & respawn all zone guards (just in case they are stuck between walls)<BR>
	 * Teleport all enemies to their starter town<BR>
	 * Remove all bought features (dmg, regen)
	 * 
	 * @param newOwner
	 */
	public void changeOwner(int newOwner, boolean auto)
	{
		if (auto && getZoneData().getOwnerFaction() == newOwner)
			return;
		
		int oldAbn = _data.getAbnormal();
		_data.setOwnerFaction(newOwner);
		if (!auto)
		{
			Internationalization i18n = Internationalization.getInstance();
			ConquestFactionManager cfm = ConquestFactionManager.getInstance();
			for (int i = ConquestFactionManager.ANCIENTS; i <= ConquestFactionManager.DIVINES; i++)
			{
				FastSet<L2PcInstance> pcs = cfm.getFactionTemplate(i).getMembers();
				for (FastSet.Record r = pcs.head(), end = pcs.tail(); (r = r.getNext()) != end;)
				{
					L2PcInstance player = pcs.valueOf(r);
					Locale loc = player.getSoDPlayer().getSettings().getLocale();
					player.sendPacket(new ChatMessage(Chat.COMMANDER, getName(), i18n.get(loc,
							"CONQUEST_SEALING_COMPLETED", "%factionName%", cfm.getFactionTemplate(newOwner)
									.getName(loc))));
				}
			}
		}
		if (_servitor != null)
			_servitor.clearAggroList();
		for (FastSet.Record r = _guards.head(), end = _guards.tail(); (r = r.getNext()) != end;)
		{
			L2ZoneGuardInstance zg = _guards.valueOf(r);
			if (!zg.isDead())
			{
				zg.clearAggroList();
				zg.getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE);
				zg.getStatus().setCurrentHpMp(zg.getMaxHp(), zg.getMaxMp());
			}
			// Allow quick reclamation and prevent guard duping
			// else
			// zg.getSpawn().doSpawn();
		}
		RespawnManager rm = RespawnManager.getInstance();
		for (L2Character cha : getCharactersInsideActivated())
		{
			if (!(cha instanceof L2Playable))
				continue;
			L2PcInstance player = cha.getActingPlayer();
			if (isEnemy(player))
			{
				player.getSoDPlayer().sendLocalizedMessage("CONQUEST_FAILED_DEFENDING");
				rm.moveToHq(player);
			}
			else
			{
				cha.stopAbnormalEffect(oldAbn);
				cha.setInsideZone(FLAG_DANGER, false);
				if (cha instanceof L2PcInstance)
					player.sendEtcStatusUpdate();
				Location loc = getRestartPoint(RestartType.OWNER);
				if (loc != null)
					player.teleToLocation(loc.getX() + Rnd.get(-100, 100), loc.getY() + Rnd.get(-100, 100), loc.getZ(),
							false);
			}
		}
		ConquestZoneManager.getInstance().update(getId(), _data);
		if (getTownId() == -1)
		{
			int entity = getCastleId();
			if (entity > 0)
				CastleManager.getInstance().getCastleById(entity).spawnDoor();
			entity = getFortId();
			if (entity > 0)
				FortManager.getInstance().getFortById(entity).resetDoors();
		}
		if (!auto)
			ConquestZoneManager.getInstance().checkOwnedZones();
	}
	
	private void notifyTargetLeave(L2Character cha)
	{
		if (cha instanceof L2Playable)
		{
			if (_servitor != null)
				_servitor.reduceHate(cha, Integer.MAX_VALUE);
			for (FastSet.Record r = _guards.head(), end = _guards.tail(); (r = r.getNext()) != end;)
			{
				L2ZoneGuardInstance zg = _guards.valueOf(r);
				if (!zg.isDead())
					zg.reduceHate(cha, Integer.MAX_VALUE);
			}
		}
	}
	
	public void buyFeatureHostile(boolean mp, int level)
	{
		boolean danger = isDangerous();
		getZoneData().setDamageLevel(mp, level);
		if (!danger && isDangerous())
			changeAbnormal(getZoneData().getAbnormal());
		ConquestZoneManager.getInstance().update(getId(), getZoneData());
	}
	
	public boolean scheduleChangeAbnormal(final int mask)
	{
		if (_abnTask != null)
			return false;
		_abnTask = ThreadPoolManager.getInstance().schedule(new Runnable()
		{
			@Override
			public void run()
			{
				changeAbnormal(mask);
			}
		}, ABNORMAL_CHANGING_DELAY);
		return true;
	}
	
	public void changeAbnormal(int mask)
	{
		_abnTask = null;
		int oldAbn = getZoneData().getAbnormal();
		getZoneData().setAbnormal(mask);
		for (L2Character cha : getCharactersInsideActivated())
		{
			if (!(cha instanceof L2PcInstance))
				continue;
			if (!isEnemy(cha) || !isDangerous())
				continue;
			cha.stopAbnormalEffect(oldAbn);
			cha.startAbnormalEffect(mask);
		}
		ConquestZoneManager.getInstance().update(getId(), getZoneData());
	}
	
	public void buyFeatureFriendly(int type, int level)
	{
		getZoneData().setRegenLevel(type, level);
		ConquestZoneManager.getInstance().update(getId(), getZoneData());
	}
	
	public void servitorDied()
	{
		_servitor = null;
	}
	
	public void broadcastToOwners(L2ServerPacket packet)
	{
		for (L2Character cha : getCharactersInsideActivated())
		{
			if (!(cha instanceof L2PcInstance))
				continue;
			L2PcInstance player = cha.getActingPlayer();
			if (!isEnemy(player))
				player.sendPacket(packet);
		}
	}
	
	public void broadcastToEnemies(L2ServerPacket packet)
	{
		for (L2Character cha : getCharactersInsideActivated())
		{
			if (!(cha instanceof L2PcInstance))
				continue;
			L2PcInstance player = cha.getActingPlayer();
			if (isEnemy(player))
				player.sendPacket(packet);
		}
	}
	
	/** @return true when we have an active servitor, false otherwise */
	public boolean getServitorStatus()
	{
		return _servitor != null;
	}
	
	public final ZoneData getZoneData()
	{
		return _data;
	}
	
	private long getCooldownEnd()
	{
		return _secured + SECURITY_COOLDOWN;
	}
	
	public int getCooldown()
	{
		return (int) Math.max((getCooldownEnd() - System.currentTimeMillis()) / 1000, 0);
	}
	
	public void decontaminate()
	{
		Broadcast.broadcastMessage(getCharactersInside(), Chat.COMMANDER, "CONQUEST_FUSION_CONTROL_TITLE", true,
				"CONQUEST_FUSION_CONTROL_OVERLOAD");
		
		if (_servitor != null)
		{
			_servitor.doDie(null);
			Broadcast.broadcastMessage(_servitor.getKnownList().getKnownPlayers().values(), _servitor, Chat.LOCAL,
					"CONQUEST_SERVITOR_PURGED");
		}
		L2Skill sk = SkillTable.getInstance().getInfo(5256, 1);
		SystemMessage sm = new SystemMessage(SystemMessageId.YOU_FEEL_S1_EFFECT);
		sm.addSkill(sk);
		for (L2Character cha : getCharactersInsideActivated())
		{
			if (!(cha instanceof L2Playable))
				continue;
			sk.getEffects(cha, cha);
			if (cha instanceof L2PcInstance)
				cha.getActingPlayer().sendPacket(sm);
		}
	}
	
	private final FastSet<L2PcInstance> getEnergyDonors()
	{
		return _donors;
	}
	
	public void addEnergyDonor(L2PcInstance player)
	{
		if (player == null)
			return;
		
		getEnergyDonors().add(player);
		int abnormal = getTowerAbnormal();
		boolean start = (abnormal == AbnormalEffect.INVULNERABLE.getMask());
		L2ZoneArtefactInstance core = getCore();
		if (core != null)
		{
			if (core.getAbnormalEffect() != abnormal)
				core.startAbnormalEffect(abnormal);
			if (start)
				core.startAoE();
		}
		
		if (start)
		{
			if (getServitorStatus())
			{
				decontaminate();
				return;
			}
			_secured = System.currentTimeMillis();
			double nrg = 0;
			for (FastSet.Record r = _donors.head(), end = _donors.tail(); (r = r.getNext()) != end;)
			{
				L2PcInstance p = _donors.valueOf(r);
				nrg += p.getCurrentMp();
				p.getStatus().setCurrentMp(0);
			}
			getEnergyDonors().clear();
			int id;
			if (nrg < 5000)
				id = 9103;
			else if (nrg < 25000)
				id = 9104;
			else if (nrg < 60000)
				id = 9105;
			else
				id = 9106;
			L2Skill skill = SkillTable.getInstance().getInfo(id, 1);
			for (L2Character cha : getCharactersInsideActivated())
				if (isEnemy(cha))
					skill.getEffects(cha, cha);
		}
	}
	
	public void removeEnergyDonor(L2PcInstance player)
	{
		getEnergyDonors().remove(player);
		L2ZoneArtefactInstance core = getCore();
		if (core == null)
			return;
		int oldAbn = core.getAbnormalEffect();
		int newAbn = getTowerAbnormal();
		if (oldAbn != newAbn)
		{
			core.stopAbnormalEffect(oldAbn);
			core.startAbnormalEffect(newAbn);
		}
	}
	
	public final int getDonorCount()
	{
		return getEnergyDonors().size();
	}
	
	public double getNeededDonors()
	{
		return Math.max(1, _defenders.get() * 0.51);
	}
	
	public int getTowerAbnormal()
	{
		double ratio = getDonorCount() / getNeededDonors();
		if (ratio < 0.25)
			return 0;
		else if (ratio < 0.5)
			return AbnormalEffect.FEAR.getMask();
		else if (ratio < 0.75)
			return AbnormalEffect.VITALITY.getMask();
		else if (ratio < 1)
			return AbnormalEffect.IMPRISIONING_1.getMask();
		else
			return AbnormalEffect.INVULNERABLE.getMask();
	}
	
	public final L2ZoneArtefactInstance getCore()
	{
		return _core;
	}
	
	public final boolean isSealing()
	{
		return !_seal.isEmpty();
	}
	
	public void addSeal(L2PcInstance player)
	{
		_seal.add(player.getObjectId());
	}
	
	public void remSeal(L2PcInstance player)
	{
		_seal.remove(player.getObjectId());
	}
	
	public final int getDefenders()
	{
		return _defenders.get();
	}
	
	public final int getPlayersInside()
	{
		return _players.get();
	}
	
	private void assign(L2ZoneGuardInstance zg)
	{
		if (zg == null)
			return;
		if (zg instanceof L2ZoneServitorInstance)
			return;
		else
			_guards.add(zg);
		// zg.getSpawn().stopRespawn();
		zg.setFactionZone(this);
		zg.setTitle(getName());
		if (isLocked())
			zg.setIsPetrified(true);
	}
	
	public boolean isPeaceful()
	{
		return System.currentTimeMillis() > _peace;
	}
	
	@Override
	protected void onDieInside(L2Character character)
	{
		if (!(character instanceof L2Playable))
			return;
		
		_peace = System.currentTimeMillis() + PEACE_COOLDOWN;
		/*if (character instanceof L2PcInstance && !isEnemy(character)
				&& _servitor != null)
		{
			_servitor.clearAggroList();
			_servitor.teleToLocation(character.getLoc(), true);
		}*/
	}
	
	public void addAwaker(L2PcInstance player)
	{
		if (player == null)
			return;
		
		if (_awakers.add(player))
		{
			if (_awakers.size() == 1)
			{
				L2NpcTemplate temp = NpcTable.getInstance().getTemplate(90009);
				L2Spawn spawn = new L2Spawn(temp);
				spawn.setAmount(1);
				Location loc = getRestartPoint(RestartType.OWNER);
				spawn.setLocx(loc.getX());
				spawn.setLocy(loc.getY());
				spawn.setLocz(loc.getZ());
				spawn.setRespawnDelay(0);
				spawn.stopRespawn();
				_servitor = (L2ZoneGuardInstance) spawn.doSpawn();
				_servitor.setFactionZone(this);
				broadcastMessage(_servitor, Chat.SHOUT, "CONQUEST_SERVITOR_SUMMONED");
				return;
			}
			else if (_servitor == null)
			{
				_log.warn("No servitor??! Current awaker size: " + _awakers.size());
				return;
			}
			int level = 0;
			switch (_awakers.size())
			{
			case 3:
				broadcastMessage(_servitor, Chat.SHOUT, "CONQUEST_SERVITOR_STAGE_1_UP");
				level = 1;
				break;
			case 5:
				broadcastMessage(_servitor, Chat.SHOUT, "CONQUEST_SERVITOR_STAGE_2_UP");
				level = 2;
				break;
			case 10:
				broadcastMessage(_servitor, Chat.SHOUT, "CONQUEST_SERVITOR_STAGE_3_UP");
				level = 3;
				break;
			case 20:
				broadcastMessage(_servitor, Chat.SHOUT, "CONQUEST_SERVITOR_STAGE_4_UP");
				level = 4;
				break;
			default:
				break;
			}
			if (level > 0)
			{
				L2Skill skill = SkillTable.getInstance().getInfo(Skills.UNHOLY_POWER, level);
				skill.getEffects(_servitor, _servitor);
			}
		}
	}
	
	public void removeAwaker(L2PcInstance player)
	{
		if (_awakers.remove(player))
		{
			if (_servitor == null)
			{
				_log.info("Servitor is null, even though awaker size is " + _awakers.size());
				return;
			}
			int level = 0;
			switch (_awakers.size())
			{
			case 0:
				broadcastMessage(_servitor, Chat.SHOUT, "CONQUEST_SERVITOR_UNSUMMONED");
				_servitor.doDie(null);
				break;
			case 2:
				broadcastMessage(_servitor, Chat.SHOUT, "CONQUEST_SERVITOR_STAGE_1_DOWN");
				_servitor.getEffects().stopEffects(Skills.UNHOLY_POWER);
				break;
			case 4:
				broadcastMessage(_servitor, Chat.SHOUT, "CONQUEST_SERVITOR_STAGE_2_DOWN");
				level = 1;
				break;
			case 9:
				broadcastMessage(_servitor, Chat.SHOUT, "CONQUEST_SERVITOR_STAGE_3_DOWN");
				level = 2;
				break;
			case 19:
				broadcastMessage(_servitor, Chat.SHOUT, "CONQUEST_SERVITOR_STAGE_4_DOWN");
				level = 3;
				break;
			default:
				break;
			}
			if (level > 0)
			{
				L2Skill skill = SkillTable.getInstance().getInfo(Skills.UNHOLY_POWER, level);
				skill.getEffects(_servitor, _servitor);
			}
		}
	}
	
	public boolean tryAlert()
	{
		if (System.currentTimeMillis() > _nextAlert)
		{
			_nextAlert = System.currentTimeMillis() + INTRUDER_WARN_DELAY;
			return true;
		}
		else
			return false;
	}
	
	public final boolean isLocked()
	{
		return _locked;
	}
	
	/**
	 * Indicates this zone should be unlocked.
	 * 
	 * @return
	 */
	public final boolean isMarked()
	{
		return _marked;
	}
	
	public final void mark()
	{
		_marked = true;
	}
	
	public final void unmark()
	{
		_marked = false;
	}
	
	public void lock()
	{
		if (isLocked())
			return;
		_locked = true;
		
		if (ConquestZoneManager.getInstance().isActive())
			ConquestFactionManager.getInstance().broadcastToMembers(Chat.COMMANDER, getName(), "CONQUEST_ZONE_LOCKED");
		
		for (FastSet.Record r = _guards.head(), end = _guards.tail(); (r = r.getNext()) != end;)
		{
			L2ZoneGuardInstance zg = _guards.valueOf(r);
			zg.setIsPetrified(true);
		}
		for (L2Character cha : getCharactersInsideActivated())
		{
			if (!(cha instanceof L2PcInstance))
				continue;
			L2PcInstance player = cha.getActingPlayer();
			if (!isEnemy(player))
			{
				removeAwaker(player);
				removeEnergyDonor(player);
			}
		}
	}
	
	public void unlock()
	{
		if (!isLocked())
			return;
		for (FastSet.Record r = _guards.head(), end = _guards.tail(); (r = r.getNext()) != end;)
		{
			L2ZoneGuardInstance zg = _guards.valueOf(r);
			zg.setIsPetrified(false);
		}
		if (getTownId() == -1)
		{
			int entity = getCastleId();
			if (entity > 0)
				CastleManager.getInstance().getCastleById(entity).spawnDoor();
			entity = getFortId();
			if (entity > 0)
				FortManager.getInstance().getFortById(entity).resetDoors();
		}
		_locked = false;
		
		if (ConquestZoneManager.getInstance().isActive())
			ConquestFactionManager.getInstance()
					.broadcastToMembers(Chat.COMMANDER, getName(), "CONQUEST_ZONE_UNLOCKED");
	}
	
	@Override
	public L2Character getActingCharacter()
	{
		return null;
	}
}
