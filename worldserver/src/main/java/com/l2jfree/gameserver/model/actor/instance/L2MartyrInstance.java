/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.model.actor.instance;

import org.sod.model.ChallengeTemplate;

import com.l2jfree.gameserver.ThreadPoolManager;
import com.l2jfree.gameserver.ai.CtrlIntention;
import com.l2jfree.gameserver.datatables.NpcTable;
import com.l2jfree.gameserver.model.L2Spawn;
import com.l2jfree.gameserver.model.Location;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.L2Playable;
import com.l2jfree.gameserver.model.restriction.global.GlobalRestrictions;
import com.l2jfree.gameserver.network.serverpackets.MagicSkillUse;
import com.l2jfree.gameserver.network.serverpackets.AbstractNpcInfo.MartyrInfo;
import com.l2jfree.gameserver.skills.Stats;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;
import com.l2jfree.util.Rnd;

/**
 * TODO Auto-generated JavaDoc
 * @author savormix
 * @since 2010.04.11
 */
public class L2MartyrInstance extends L2Npc
{
	private static final int NPC = 90022;
	private static final int CHANCE = 1; // 1/100
	private static final int BASE_RADIUS = 400;
	private static final int BASE_DELAY = 3000;
	private static final int BASE_DAMAGE = 6000;
	private static final double DMG_MULTI_MAGE = 0.75;
	//private static final SkillUsageRequest SKILL = new SkillUsageRequest(
	//		SkillTable.getInstance().getInfo(Skills.MARTYR_EXPLOSION, 1), true, true);

	private L2PcInstance _owner;
	private int _radius;
	private double _damage;

	public L2MartyrInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
		onAction(null);
	}

	public final void setOwner(L2PcInstance player)
	{
		_owner = player;
		_radius = (int) _owner.calcStat(Stats.MARTYRDOM_RADIUS, BASE_RADIUS, null, null);
		_damage = _owner.calcStat(Stats.MARTYRDOM_DAMAGE, BASE_DAMAGE, null, null);
	}

	@Override
	public void broadcastFullInfoImpl()
	{
		// do nothing
	}

	@Override
	public void onAction(L2PcInstance player)
	{
		if (player != null && canTarget(player) && canInteract(player))
			player.getAI().setIntention(CtrlIntention.AI_INTENTION_INTERACT, this);
	}

	@Override
	public void onSpawn()
	{
		super.onSpawn();
		Iterable<L2PcInstance> near = getKnownList().getKnownPlayersInRadius(_radius);
		double max = BASE_DELAY;
		for (L2PcInstance pc : near)
		{
			double delay = pc.calcStat(Stats.MARTYRDOM_DELAY, BASE_DELAY, null, null);
			if (delay > max)
				max = delay;
		}
		ThreadPoolManager.getInstance().scheduleGeneral(new BlowUp(), (long) max);
	}

	@Override
	public void sendInfo(L2PcInstance activeChar)
	{
		activeChar.sendPacket(new MartyrInfo(this));
	}

	public static final void dropExplosive(L2PcInstance player)
	{
		if (player == null || Rnd.get(100) > CHANCE)
			return;
		L2NpcTemplate temp = NpcTable.getInstance().getTemplate(NPC);
		if (temp != null)
		{
			L2Spawn s = new L2Spawn(temp);
			s.setAmount(1);
			Location loc = player.getLoc();
			s.setLocx(loc.getX());
			s.setLocy(loc.getY());
			s.setLocz(loc.getZ());
			s.setHeading(loc.getHeading());
			s.setRespawnDelay(0);
			s.setLocation(0);
			s.setInstanceId(player.getInstanceId());
			s.init();
			((L2MartyrInstance) s.getLastSpawn()).setOwner(player);
			s.stopRespawn();
		}
		else
			_log.warn("Missing martyr NPC data: " + NPC);
	}

	private class BlowUp implements Runnable
	{
		@Override
		public void run()
		{
			getSpawn().stopRespawn();
			//getAI().setIntention(AI_INTENTION_CAST, SKILL);
			if (_owner != null)
			{
				broadcastPacket(new MagicSkillUse(L2MartyrInstance.this, 1171, 1, 1000, 0));
				Iterable<L2Playable> near = getKnownList().getKnownPlayableInRadius(_radius);
				for (L2Playable target : near)
				{
					if (target.isDead() || GlobalRestrictions.isProtected(_owner, target, null, false))
						continue;
					double dmg = _damage;
					L2PcInstance trgPlayer = null;
					if (target instanceof L2PcInstance)
						trgPlayer = target.getActingPlayer();
					if (trgPlayer != null &&
							(trgPlayer.getClassId().isMage() || trgPlayer.getClassId().isSummoner()))
						dmg *= DMG_MULTI_MAGE;
					dmg = Rnd.get((long) (_damage * 0.9), (long) (_damage * 1.1));
					target.reduceCurrentHp(dmg, L2MartyrInstance.this/*_owner*/);
					if (target.isDead())
						_owner.getSoDPlayer().tryAddChallengePoints(ChallengeTemplate.HOT_POTATO, 1);
					else if (trgPlayer != null)
						trgPlayer.getSoDPlayer().tryAddChallengePoints(ChallengeTemplate.CLOSE_SHAVE, 1);
				}
				_owner = null;
			}
			doDie(null);
		}
	}
}
