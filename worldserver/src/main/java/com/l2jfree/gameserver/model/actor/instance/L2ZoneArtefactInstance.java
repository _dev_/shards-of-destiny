/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.model.actor.instance;

import java.util.Arrays;

import org.sod.manager.conquest.ConquestFactionManager;
import org.sod.manager.conquest.ConquestZoneManager;
import org.sod.manager.conquest.ConquestZoneManager.ZoneData;
import org.sod.model.conquest.ConquestZoneMember;

import com.l2jfree.Config;
import com.l2jfree.gameserver.ThreadPoolManager;
import com.l2jfree.gameserver.ai.CtrlIntention;
import com.l2jfree.gameserver.model.Elementals;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.status.CharStatus;
import com.l2jfree.gameserver.model.actor.status.ImperviousNpcStatus;
import com.l2jfree.gameserver.model.itemcontainer.PcInventory;
import com.l2jfree.gameserver.model.zone.L2FactionZone;
import com.l2jfree.gameserver.network.client.packets.sendable.NpcHtmlMessage;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;

/**
 * This NPC is the core of each faction zone. Zone owners can purchase special
 * effects, like DOT for enemies, recovery for allies. They can also cast either
 * Awakening (summons an invincible <U>to damage</U> servitor which leeches
 * casters' (the more the people, the less MP each has to sacrifice per tick) MP
 * or Energy Ditch, which increases HP regen and activates zone towers.<BR>
 * Attackers can cast Seal of Ruler, which allows them to acquire the zone.
 * @author savormix
 * @since 2009.12.03
 */
public final class L2ZoneArtefactInstance extends L2Npc implements ConquestZoneMember
{
	private static final String OBJECT_ID = "%objectId%";
	private static final String PATH = "data/html/faction/artefact/";
	private static final String[] PAGES = {
		"chat.htm", "status.htm", "activate_rec.htm", "recovery.htm", "activate_dmg.htm",
		"damage.htm", "element.htm", "locked.htm"
	};
	private static final String RED = "FF0000";
	private static final String YELLOW = "FFFF00";
	private static final String GREEN = "00FF00";
	private static final String ACTIVE = "ACTIVE";
	private static final String INACTIVE = "INACTIVE";
	private static final String[] GM = {
		"%gmOption%", "<br1><a action=\"bypass -h npc_%objectId%_5\">Area Decontamination</a>", ""
	};
	
	private final String _oid;
	private L2FactionZone _zone;
	
	public L2ZoneArtefactInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
		_oid = String.valueOf(objectId);
		_zone = null;
	}
	
	@Override
	public final L2FactionZone getFactionZone()
	{
		return _zone;
	}
	
	private void showMessageWindow(L2PcInstance player, NpcHtmlMessage msg)
	{
		if (getFactionZone().isLocked() && !player.isGM())
			return;
		if (msg == null)
		{
			msg = new NpcHtmlMessage(getObjectId());
			msg.setFile(PATH + PAGES[0]);
		}
		try
		{
			if (player.isGM())
				msg.replace(GM[0], GM[1]);
			else
				msg.replace(GM[0], GM[2]);
			msg.replace(OBJECT_ID, _oid);
			player.sendPacket(msg);
		}
		catch (NullPointerException e)
		{
			_log.warn("player " + player + " message " + msg + " GM " + Arrays.toString(GM), e);
		}
	}
	
	private final String getStatusColor(int level)
	{
		if (level < 3)
			return RED;
		else if (level < 8)
			return YELLOW;
		else
			return GREEN;
	}
	
	private final String getStatusColor(double curr, double needed)
	{
		if (curr <= needed * 0.5)
			return RED;
		else
			return YELLOW;
	}
	
	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.model.actor.L2Npc#isAttackable()
	 */
	@Override
	public boolean isAttackable()
	{
		return false;
	}
	
	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.model.actor.L2Npc#onAction(com.l2jfree.gameserver.model.actor.instance.L2PcInstance)
	 */
	@Override
	public void onAction(L2PcInstance player)
	{
		if (!canTarget(player))
			return;
		
		if (this != player.getTarget())
			player.setTarget(this);
		else if (!canInteract(player))
			player.getAI().setIntention(CtrlIntention.AI_INTENTION_INTERACT, this);
		else if (player.isGM() || (!getFactionZone().isLocked() && !getFactionZone().isEnemy(player)))
			showMessageWindow(player, null);
	}
	
	@Override
	public void onBypassFeedback(L2PcInstance player, String command)
	{
		if ((getFactionZone().isLocked() || getFactionZone().isEnemy(player))
				&& !player.isGM())
			return;
		
		int request = -1;
		try
		{
			request = Integer.parseInt(command);
		}
		catch (Exception e)
		{
			showMessageWindow(player, null);
			return;
		}
		
		NpcHtmlMessage msg = new NpcHtmlMessage(getObjectId());
		switch (request)
		{
		case 1: // status
			msg.setFile(PATH + PAGES[1]);
			ZoneData zd = getFactionZone().getZoneData();
			int level = zd.getRegenLevel(ConquestZoneManager.REGEN_CP);
			msg.replace("%cRec%", level);
			msg.replace("%crCol%", getStatusColor(level));
			level = zd.getRegenLevel(ConquestZoneManager.REGEN_HP);
			msg.replace("%hRec%", level);
			msg.replace("%hrCol%", getStatusColor(level));
			level = zd.getRegenLevel(ConquestZoneManager.REGEN_MP);
			msg.replace("%mRec%", level);
			msg.replace("%mrCol%", getStatusColor(level));
			level = zd.getDamageLevel(false);
			msg.replace("%dmg%", level);
			msg.replace("%dCol%", getStatusColor(level));
			level = zd.getDamageLevel(true);
			msg.replace("%mDmg%", level);
			msg.replace("%mdCol%", getStatusColor(level));
			boolean on = getFactionZone().getServitorStatus();
			msg.replace("%srv%", on ? ACTIVE : INACTIVE);
			msg.replace("%sCol%", on ? GREEN : RED);
			String elem = Elementals.getElementName(zd.getElement());
			msg.replace("%elem%", elem);
			int cnt = getFactionZone().getDonorCount();
			msg.replace("%act%", cnt);
			msg.replace("%aCol%", getStatusColor(cnt, getFactionZone().getNeededDonors()));
			level = getFactionZone().getCooldown();
			msg.replace("%cool%", level);
			msg.replace("%cCol%", level > 0 ? RED : GREEN);
			break;
		case 2: // recovery activation
			msg.setFile(PATH + PAGES[2]);
			break;
		case 3: // damage activation
			msg.setFile(PATH + PAGES[4]);
			break;
		case 4: // element selection
			msg.setFile(PATH + PAGES[6]);
			break;
		case 5:
			getFactionZone().decontaminate();
			player.sendMessage("You have been affected as well!");
			break;
		case 10: // CP recovery activation
			msg.setFile(PATH + PAGES[3]);
			msg.replace("%stat%", "CP");
			msg.replace("%s%", 0);
			for (int i = 1; i < 10; i++)
				msg.replace("%a" + i + "%", Config.CONQUEST_ZONE_RECOVERY_COST[i]);
			break;
		case 11: // HP recovery activation
			msg.setFile(PATH + PAGES[3]);
			msg.replace("%stat%", "HP");
			msg.replace("%s%", 1);
			for (int i = 1; i < 10; i++)
				msg.replace("%a" + i + "%", Config.CONQUEST_ZONE_RECOVERY_COST[i]);
			break;
		case 12: // MP recovery activation
			msg.setFile(PATH + PAGES[3]);
			msg.replace("%stat%", "MP");
			msg.replace("%s%", 2);
			for (int i = 1; i < 10; i++)
				msg.replace("%a" + i + "%", Config.CONQUEST_ZONE_RECOVERY_COST[i]);
			break;
		case 13: // CP+HP damage activation
			msg.setFile(PATH + PAGES[5]);
			msg.replace("%s%", 0);
			for (int i = 1; i < 10; i++)
				msg.replace("%a" + i + "%", Config.CONQUEST_ZONE_DAMAGE_COST[i]);
			break;
		case 14: // MP damage activation
			msg.setFile(PATH + PAGES[5]);
			msg.replace("%s%", 1);
			for (int i = 1; i < 10; i++)
				msg.replace("%a" + i + "%", Config.CONQUEST_ZONE_DAMAGE_COST[i]);
			break;
		case 201: case 202: case 203: case 204: case 205:
		case 206: case 207: case 208: case 209:
		case 211: case 212: case 213: case 214: case 215:
		case 216: case 217: case 218: case 219:
		case 221: case 222: case 223: case 224: case 225:
		case 226: case 227: case 228: case 229:
			msg.setFile(PATH + PAGES[0]);
			L2FactionZone zone = getFactionZone();
			int type = request % 100 / 10;
			int currLvl = zone.getZoneData().getRegenLevel(type);
			int reqLvl = request % 10;
			if (reqLvl > currLvl)
			{
				if (player.destroyItemByItemId("Zone Regen Upgrade", PcInventory.ADENA_ID, Config.CONQUEST_ZONE_RECOVERY_COST[reqLvl], this, true))
					zone.buyFeatureFriendly(type, reqLvl);
			}
			else
				player.sendMessage("You may not downgrade the recovery system!");
			break;
		case 101: case 102: case 103: case 104: case 105:
		case 106: case 107: case 108: case 109:
		case 111: case 112: case 113: case 114: case 115:
		case 116: case 117: case 118: case 119:
		case 121: case 122: case 123: case 124: case 125:
		case 126: case 127: case 128: case 129:
			msg.setFile(PATH + PAGES[0]);
			zone = getFactionZone();
			boolean typ = request % 100 > 9;
			currLvl = zone.getZoneData().getDamageLevel(typ);
			reqLvl = request % 10;
			if (reqLvl > currLvl)
			{
				if (player.destroyItemByItemId("Zone Damage Upgrade", PcInventory.ADENA_ID, Config.CONQUEST_ZONE_DAMAGE_COST[reqLvl], this, true))
					zone.buyFeatureHostile(typ, reqLvl);
			}
			else
				player.sendMessage("You may not downgrade the damage system!");
			break;
		case 301:
		case 302:
		case 303:
		case 304:
		case 305:
			msg.setFile(PATH + PAGES[0]);
			if (getFactionZone().scheduleChangeAbnormal(getAbnormalReq(request)))
				player.sendMessage("Abnormal Elemental effect will be changed shortly.");
			else
				player.sendMessage("A change was already requested. Please try again later.");
			break;
		}
		
		showMessageWindow(player, msg);
	}
	
	public final int getAbnormalReq(int req)
	{
		switch (req)
		{
		case 301: return 0x0001;
		case 302: return 0x0002;
		case 303: return 0x0008;
		case 304: return 0x0010;
		case 305: return 0x4000;
		default: return 0;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.model.actor.L2Npc#onAction(com.l2jfree.gameserver.model.actor.instance.L2PcInstance)
	 */
	@Override
	public void onForcedAttack(L2PcInstance player)
	{
		onAction(player);
	}
	
	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.model.actor.L2Npc#initStatus()
	 */
	@Override
	protected CharStatus initStatus()
	{
		return new ImperviousNpcStatus(this);
	}
	
	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.model.actor.L2Npc#getStatus()
	 */
	@Override
	public ImperviousNpcStatus getStatus()
	{
		return (ImperviousNpcStatus) _status;
	}
	
	public final void startAoE()
	{
		ThreadPoolManager.getInstance().scheduleGeneral(new Runnable()
		{
			@Override
			public void run()
			{
				stopAbnormalEffect(getAbnormalEffect());
			}
		}, 5000);
	}
	
	@Override
	public int getConquestFaction()
	{
		if (_zone != null)
			return _zone.getConquestFaction();
		else
			return ConquestFactionManager.NONE;
	}
	
	@Override
	public void setFactionZone(L2FactionZone zone)
	{
		_zone = zone;
	}
}
