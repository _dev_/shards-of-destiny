/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.model.actor.instance;

import org.sod.manager.conquest.ConquestFactionManager;

import com.l2jfree.gameserver.network.SystemChatChannelId;
import com.l2jfree.gameserver.network.serverpackets.AbstractNpcInfo;
import com.l2jfree.gameserver.network.serverpackets.CreatureSay;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;
import com.l2jfree.util.Rnd;

/**
 * Zone servitor NPC instance, a.k.a. Zariche.
 * @author savormix
 * @since 2009.12.04
 */
public final class L2ZoneServitorInstance extends L2ZoneGuardInstance
{
	private static final int TALKER_INTERVAL = 10000;
	private static final String[] TALKER = {
		"Gotta move on.", "There's no time to chat!", "Please, continue fighting.",
		"Yes, war is never easy.", "Do you think I have time to chat?",
		"I'm on your side! Concentrate!", "Don't waste our time!", "Back off!",
		"There's no use in talking.", "Yes, I enjoy the fight too.",
		"I'd like to keep silent.", "Just don't be annoying...",
		"It's time to fight, not to chat!", "Hi and bye!"
	};
	private transient volatile long _lastTalk;

	public L2ZoneServitorInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
		setIsInvul(true);
	}

	@Override
	public final void showChatWindow(L2PcInstance player, int page)
	{
		if (!ConquestFactionManager.getInstance().isSameFaction(this, player)
				|| System.currentTimeMillis() < _lastTalk + TALKER_INTERVAL)
			return;
		_lastTalk = System.currentTimeMillis();
		CreatureSay talk = new CreatureSay(getObjectId(), SystemChatChannelId.Chat_Normal, getName(), TALKER[Rnd.get(TALKER.length)]);
		broadcastPacket(talk);
	}

	@Override
	public void sendInfo(L2PcInstance activeChar)
	{
		activeChar.sendPacket(new AbstractNpcInfo.TestInfo(this, true));
	}

	@Override
	public void broadcastFullInfoImpl()
	{
		broadcastPacket(new AbstractNpcInfo.TestInfo(this, true));
	}
}
