/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.skills.effects;

import com.l2jfree.gameserver.datatables.SkillTable;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.instance.L2ZoneArtefactInstance;
import com.l2jfree.gameserver.skills.Env;
import com.l2jfree.gameserver.templates.effects.EffectTemplate;

/**
 * @author savormix
 * @since 2010.01.15
 */
public final class EffectEnergyDonor extends EffectFusion
{
	public EffectEnergyDonor(Env env, EffectTemplate template)
	{
		super(env, template);
		_effect = 0;
		increaseEffect(getEffector());
	}

	@Override
	public void increaseEffect(L2Character caster)
	{
		L2ZoneArtefactInstance core = (L2ZoneArtefactInstance) getEffected();
		if (core.getFactionZone().isEnemy(caster))
		{
			SkillTable.getInstance().getInfo(1337, 1).getEffects(core, caster);
			SkillTable.getInstance().getInfo(1338, 1).getEffects(core, caster);
		}
		else
			core.getFactionZone().addEnergyDonor(caster.getActingPlayer());
		if (_effect < _maxEffect)
			_effect++;
	}

	@Override
	public void decreaseEffect(L2Character caster)
	{
		L2ZoneArtefactInstance core = (L2ZoneArtefactInstance) getEffected();
		if (core.getFactionZone().isEnemy(caster))
		{
			SkillTable.getInstance().getInfo(1169, 1).getEffects(core, caster);
		}
		else
			core.getFactionZone().removeEnergyDonor(caster.getActingPlayer());
		_effect--;
		if (_effect < 1)
			exit();
	}
}
