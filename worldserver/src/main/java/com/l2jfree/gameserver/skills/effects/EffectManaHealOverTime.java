/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.skills.effects;

import org.sod.Calc;

import com.l2jfree.gameserver.model.L2Effect;
import com.l2jfree.gameserver.skills.Env;
import com.l2jfree.gameserver.templates.effects.EffectTemplate;
import com.l2jfree.gameserver.templates.skills.L2EffectType;

public final class EffectManaHealOverTime extends L2Effect
{
	public EffectManaHealOverTime(Env env, EffectTemplate template)
	{
		super(env, template);
	}
	
	@Override
	public L2EffectType getEffectType()
	{
		return L2EffectType.MANA_HEAL_OVER_TIME;
	}
	
	@Override
	protected boolean onActionTime()
	{
		if (getEffected().isDead())
			return false;
		
		double oldmp = getEffected().getStatus().getCurrentMp();
		double maxmp = getEffected().getMaxMp();
		double mp = oldmp + calc();
		if (mp > maxmp)
		{
			mp = maxmp;
		}
		if (getEffector() != getEffected())
			Calc.rewardClerical(getEffector().getActingPlayer(), true, mp - oldmp);
		getEffected().getStatus().setCurrentMp(mp);
		return true;
	}
}
