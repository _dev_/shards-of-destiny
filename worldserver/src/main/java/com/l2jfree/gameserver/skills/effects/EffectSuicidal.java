/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.skills.effects;

import org.sod.model.ChallengeTemplate;

import com.l2jfree.gameserver.model.L2Effect;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.restriction.global.GlobalRestrictions;
import com.l2jfree.gameserver.network.serverpackets.MagicSkillUse;
import com.l2jfree.gameserver.skills.Env;
import com.l2jfree.gameserver.templates.effects.EffectTemplate;
import com.l2jfree.gameserver.templates.skills.L2EffectType;
import com.l2jfree.util.Rnd;

/**
 * TODO Auto-generated JavaDoc
 * @author savormix
 * @since 2010.04.27
 */
public class EffectSuicidal extends L2Effect
{
	private static final int BASE_RADIUS = 250;
	private static final int BASE_DAMAGE = 7500;
	private static final double DMG_MULTI_MAGE = 0.66;

	public EffectSuicidal(Env env, EffectTemplate template)
	{
		super(env, template);
	}

	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.model.L2Effect#onExit()
	 */
	@Override
	public void onExit()
	{
		L2Character caster = getEffector();
		caster.broadcastPacket(new MagicSkillUse(caster, 1171, 1, 1000, 0));
		Iterable<L2Character> near = caster.getKnownList().getKnownCharactersInRadius(BASE_RADIUS);
		caster.doDie(null);
		for (L2Character target : near)
		{
			if (target.isDead() || GlobalRestrictions.isProtected(caster, target, null, false))
				continue;
			L2PcInstance trgPlayer = null;
			if (target instanceof L2PcInstance) // ONLY player, not his pet/summon
				trgPlayer = target.getActingPlayer();
			double dmg = BASE_DAMAGE;
			if (trgPlayer != null && (trgPlayer.getClassId().isMage() || trgPlayer.getClassId().isSummoner()))
				dmg *= DMG_MULTI_MAGE;
			dmg = Rnd.get((long) (dmg * 0.5), (long) (dmg * 1.15));
			target.reduceCurrentHp(dmg, caster);
			if (target.isDead())
			{
				if (caster instanceof L2PcInstance)
					caster.getActingPlayer().getSoDPlayer().tryAddChallengePoints(ChallengeTemplate.FREQUENT_FRYER, 1);
			}
			else if (trgPlayer != null)
				trgPlayer.getSoDPlayer().tryAddChallengePoints(ChallengeTemplate.CLOSE_SHAVE, 1);
		}
	}

	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.model.L2Effect#getEffectType()
	 */
	@Override
	public L2EffectType getEffectType()
	{
		return L2EffectType.BUFF;
	}

	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.model.L2Effect#canBeStoredInDb()
	 */
	@Override
	public boolean canBeStoredInDb()
	{
		return false;
	}
}
