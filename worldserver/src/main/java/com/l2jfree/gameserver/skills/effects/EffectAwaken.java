/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.skills.effects;

import com.l2jfree.gameserver.instancemanager.ZoneManager;
import com.l2jfree.gameserver.model.zone.L2FactionZone;
import com.l2jfree.gameserver.model.zone.L2Zone.ZoneType;
import com.l2jfree.gameserver.skills.Env;
import com.l2jfree.gameserver.templates.effects.EffectTemplate;
import com.l2jfree.gameserver.templates.skills.L2EffectType;

@Deprecated
public final class EffectAwaken extends EffectDamOverTime
{
	private final L2FactionZone _zone;

	public EffectAwaken(Env env, EffectTemplate template)
	{
		super(env, template);
		_zone = (L2FactionZone) ZoneManager.getInstance().isInsideZone(ZoneType.Faction, getEffector().getX(), getEffector().getY());
	}

	@Override
	protected boolean onStart()
	{
		if (_zone == null)
			return false;

		_zone.addAwaker(getEffector().getActingPlayer());
		return true;
	}

	@Override
	protected void onExit()
	{
		if (_zone == null)
			return;
		_zone.removeAwaker(getEffector().getActingPlayer());
	}

	@Override
	public L2EffectType getEffectType()
	{
		return L2EffectType.BUFF;
	}
}
