/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.skills.effects;

import org.sod.model.L2PlayerData;

import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.actor.instance.L2ZoneArtefactInstance;
import com.l2jfree.gameserver.network.SystemMessageId;
import com.l2jfree.gameserver.network.client.packets.sendable.SystemMessagePacket.SystemMessage;
import com.l2jfree.gameserver.skills.Env;
import com.l2jfree.gameserver.templates.effects.EffectTemplate;

/**
 * TODO Auto-generated JavaDoc
 * @author savormix
 * @since 2010.04.06
 */
public class EffectSacrificeSoul extends EffectEradication
{
	public EffectSacrificeSoul(Env env, EffectTemplate template)
	{
		super(env, template);
	}

	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.model.L2Effect#onStart()
	 */
	@Override
	protected boolean onStart()
	{
		L2PcInstance caster = getEffector().getActingPlayer();
		L2ZoneArtefactInstance core;
		if (getEffector() instanceof L2PcInstance)
		{
			L2PlayerData dat = caster.getSoDPlayer();
			core = caster.getTarget(L2ZoneArtefactInstance.class);
			if (core == null || dat.getArtefact() != null)
				return false;
			dat.setArtefact(core);
			caster.sendPacket(new SystemMessage(SystemMessageId.YOU_FEEL_S1_EFFECT).addSkillName(5257));
			if (!core.getFactionZone().isEnemy(caster))
				core.getFactionZone().addAwaker(caster);
		}
		else
			return false;
		super.onStart();
		getEffector().doDie(null);
		return true;
	}

	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.model.L2Effect#onExit()
	 */
	@Override
	protected void onExit()
	{
		if (getEffector() instanceof L2PcInstance)
		{
			L2PcInstance caster = getEffector().getActingPlayer();
			L2PlayerData dat = caster.getSoDPlayer();
			L2ZoneArtefactInstance core = dat.getArtefact();
			dat.setArtefact(null);
			if (core != null && !core.getFactionZone().isEnemy(caster))
				core.getFactionZone().removeAwaker(caster);
		}
		super.onExit();
	}

	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.model.L2Effect#canBeStoredInDb()
	 */
	@Override
	public boolean canBeStoredInDb()
	{
		return false;
	}
}
