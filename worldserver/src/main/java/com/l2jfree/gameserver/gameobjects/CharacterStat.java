/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.gameobjects;

import com.l2jfree.ClientProtocolVersion;
import com.l2jfree.gameserver.gameobjects.components.interfaces.IElemental;
import com.l2jfree.network.mmocore.MMOBuffer;

/**
 * @author hex1r0
 * @author NB4L1
 */
public abstract class CharacterStat/* implements ICharacterStat*/
{
	public static enum Element
	{
		/**
		 * Not applicable:
		 * <UL>
		 * <LI>Item is not a weapon or player has no weapon equipped</LI>
		 * <LI>[Equipped] Weapon is not elementally enhanced</LI>
		 * </UL>
		 */
		NA(-2),
		/** Doesn't seem to be used. An in-depth analysis is required */
		NONE(-1), FIRE(0), WATER(1), WIND(2), EARTH(3), HOLY(4), DARK(5);
		
		private final int _value;
		
		Element(int value)
		{
			_value = value;
		}
		
		public int getValue()
		{
			return _value;
		}
		
		public static void writeElements(IElemental elemental, MMOBuffer buf, ClientProtocolVersion cpv)
		{
			if (cpv.isOlderThan(ClientProtocolVersion.GRACIA_FINAL))
			{
				buf.writeD(elemental.getAttackElementType().getValue()); // Attack element
				buf.writeD(elemental.getAttackElementPower()); // Attack element power
				buf.writeD(elemental.getDefenseElementPower(Element.FIRE)); // Fire defense
				buf.writeD(elemental.getDefenseElementPower(Element.WATER)); // Water defense
				buf.writeD(elemental.getDefenseElementPower(Element.WIND)); // Wind defense
				buf.writeD(elemental.getDefenseElementPower(Element.EARTH)); // Earth defense
				buf.writeD(elemental.getDefenseElementPower(Element.HOLY)); // Holy defense
				buf.writeD(elemental.getDefenseElementPower(Element.DARK)); // Dark defense
			}
			else
			{
				buf.writeH(elemental.getAttackElementType().getValue()); // Attack element
				buf.writeH(elemental.getAttackElementPower()); // Attack element power
				buf.writeH(elemental.getDefenseElementPower(Element.FIRE)); // Fire defense
				buf.writeH(elemental.getDefenseElementPower(Element.WATER)); // Water defense
				buf.writeH(elemental.getDefenseElementPower(Element.WIND)); // Wind defense
				buf.writeH(elemental.getDefenseElementPower(Element.EARTH)); // Earth defense
				buf.writeH(elemental.getDefenseElementPower(Element.HOLY)); // Holy defense
				buf.writeH(elemental.getDefenseElementPower(Element.DARK)); // Dark defense
			}
		}
	}
}
