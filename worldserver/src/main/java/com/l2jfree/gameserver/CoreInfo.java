/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.l2jfree.L2Config;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.client.packets.sendable.SystemMessagePacket.SystemMessage;
import com.l2jfree.mmocore.network.SelectorThread;
import com.l2jfree.versionning.Version;

/**
 * @author noctarius
 */
public final class CoreInfo
{
	private CoreInfo()
	{
	}
	
	private static final CoreVersion coreVersion = new CoreVersion(GameServer.class);
	private static final CoreVersion commonsVersion = new CoreVersion(L2Config.class);
	private static final CoreVersion mmocoreVersion = new CoreVersion(SelectorThread.class);
	private static final String serverPackCodeName = "Eudaimonia";
	private static SystemMessage ver;
	
	public static void showStartupInfo()
	{
		System.out.println("                      YBBB4BBBBBBBBBBBBB8BBBBBBBBBBBBBBBBX                     ");
		System.out.println("                    iBBBBB0N0PNkPkPSkFk15FfFUlUlUlUlU16BBBBj                   ");
		System.out.println("                  .BBBBBr                              ,BBBBBi                 ");
		System.out.println("                 PBBBBk      YyiririrrrrrrrrrrrrryY      23BBBB.               ");
		System.out.println("               YBBBBM    .   BBBBBBBBBBB4BBBBBBBBBB   i    SBBBBP              ");
		System.out.println("             :BBBBB:    BBM                          rBB;    BBBBBP            ");
		System.out.println("            MBBBBY    2BBBB   ,BEEPNqE   BOZGEOOB.   :BBBB.   iBBBBB:          ");
		System.out.println("          FBBBBN    ;BBBB  X,  BBBBBBB   BBBBBBBB   O: vBBBE    JBBBBM         ");
		System.out.println("        rBBBBB.   :BBBB: iBBB                      5BB8  qBBB     ZBBBBU       ");
		System.out.println("      ,BBBBB;     .BBL  BBB8    rBBBBBBBBBBBBBB     vBBBYBB        .BBBBBr     ");
		System.out.println("     GBBBBU           GBBB  JB.  MBBBBBBBBBBBB2   BB   BB.    .BB    iBBBBB.   ");
		System.out.println("   JBBBBM    :BBBB. XBBB; .BBBB                  JBBBLB.    .BOBBBU    5BBBBN  ");
		System.out.println(" iBBBBB:    MBBB: ;BBB2  MBBBY                    ,BBB     .BG  BBBBr    MBBBBP");
		System.out.println("eBBBB;   .MBBBU  BBBB  LBBBq     .7XBBBBBBBBFr      ;    . 0BBB7 :BBBBr   :BBBL");
		System.out.println("2BBB     LBBB..BBBB, ,BBBB.   .kBBBBBBBBBBBBBBBBl      vBBN  MBBB: qBBP     BBB");
		System.out.println("vBBB          BBBl  BBBB7   ,BBBBBBBBBBBBBBBBBBBBBG.   :BBBBY iBBB:         BB3");
		System.out.println("LBBB   jBBr        BBB0    EBBBBBBBBBBBBBBBBBBBBBBBBl    :BBBi       jBB    BBB");
		System.out.println("iBBB   JBBB BBN     ..    BBBBBBBBBBBBBBBBBBBBBBBBBBBB          .qB.:BBB    BB3");
		System.out.println("YBBB   7BBG BBB, E7.     BBBBBBBBBBBBBBBBBBBBBBBBBBBBBB     iSL BBB..BBB    BBB");
		System.out.println("lBBB   rBBZ BBB  BBB    BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBP   UBBB MBB  BBB    BBT");
		System.out.println("YBBB   7BBZ BBB  BBB   :BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB   LBB8 MBB  BBB    BBB");
		System.out.println("lBBB   rBBM BBB .BBB   0BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBL  2BBB OBB. BBB    BBB");
		System.out.println("JBBB   rBBB            BBB  Eudaimonia         CT2.3  BBE       MBB  BBB    BBB");
		System.out.println("UBBB   rBBB 7F0  XNS   BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB2BP       8BB. BBB    BBB");
		System.out.println("jBBB   rBBM BBB  BBB   kBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB7  UBBB 8BB  BBB    BBB");
		System.out.println("UBBB   rBBM BBB  BBB    BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB   uBBM GBB. BBB    BBB");
		System.out.println("uBBB   7BBM BBB  BBB    PBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB7   rBBB MBB. BBB    BBB");
		System.out.println("2BBB   7BBB BBB  r.      BBBBBBBBBBBBBBBBBBBBBBBBBBBBBq      :, BBB: BBB    BBB");
		System.out.println("uBBB   UBBB U7     .i     8BBBBBBBBBBBBBBBBBBBBBBBBBBl    ii     LO :BBB    BBB");
		System.out.println("lBBB   ,0:     :  BBBB:    7BBBBBBBBBBBBBBBBBBBBBBBB:   .BBBB  :     rqG    BBB");
		System.out.println("2BBB         MBBBr YBBBM     2BBBBBBBBBBBBBBBBBBBBv    8BBBJ .BBBZ          BBB");
		System.out.println("GBBB    :BBBJ :BBBB  GBBBj     i8BBBBBBBBBBBBBB0,    lBBBZ  5BBBi YBBBi     BBB");
		System.out.println("YBBBBU   JBBBB  YBBB.  BBBBi      .ruZMBBM0Ji      iBBBB  rBBBu  BBBBY    7BBBB");
		System.out.println(" .BBBBBi   YBBBq  B     rBBBB.                   iBBBB: .BBBZ  GBBBJ    ,BBBBB:");
		System.out.println("   7BBBBB.   ZBB.    BBB  lBBB                   BBBY  OBBB. LBBBE     8BBBBU  ");
		System.out.println("     2BBBBE   .B   . 2BBBL  B7  :BBBBB   BBBBB:  .k  2BBBi :BBBB.    5BBBBZ    ");
		System.out.println("       MBBBBY     BBB  ZBBB:    XBBBBB   BBBBBP    7BBBU  BBBBi    rBBBBB.     ");
		System.out.println("        :BBBBBi   8BBBY .BBB                      .BBB  NBBBY    :BBBBBr       ");
		System.out.println("          7BBBBB    BBBB, u,  YBBBBBBBBBBBBBBBBBY  ,r JBBBE     MBBBBu         ");
		System.out.println("            XBBBBP   :BBBB    YBBBBBBBBBBBBBBBBBY    BBBB.    2BBBBG           ");
		System.out.println("              MBBBBL   UBB                           BBi    7BBBBB,            ");
		System.out.println("               :BBBBB:      BBBBBBBBBB   BBBBBBBBB7       ,BBBBBi              ");
		System.out.println("                 YBBBBM     MBBBBBBBBB   BBBBBBBBBM      MBBBBu                ");
		System.out.println("                   kBBBBX                              5BBBB0                  ");
		System.out.println("                     BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB.                   ");
		System.out.println("                      .RSBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB,                     ");
		System.out.println("Namaste! [starting version: " + coreVersion.getVersionNumber() + "]");
		System.out.println("Dharma SecureLauncher by savormix initialized.");
	}
	
	public static final void versionInfo(L2PcInstance activeChar)
	{
		activeChar.sendPacket(ver);
	}
	
	public static String getVersionInfo()
	{
		return coreVersion.versionInfo;
	}
	
	public static String[] getFullVersionInfo()
	{
		return new String[] {
				"l2jfree-core :    " + coreVersion.fullVersionInfo,
				"l2j-commons  :    " + commonsVersion.fullVersionInfo,
				"l2j-mmocore  :    " + mmocoreVersion.fullVersionInfo };
	}

	private static final class CoreVersion extends Version
	{
		public final String versionInfo;
		public final String fullVersionInfo;
		
		public CoreVersion(Class<?> c)
		{
			super(c);
			
			versionInfo = String.format("%-6s [ %4s ]", getVersionNumber(), getRevisionNumber());
			fullVersionInfo = versionInfo + " - " + getBuildJdk() + " - " + new Date(getBuildTime());
		}
	}

	private static final String getLetter(int letter) {
		switch (letter)
		{
		case 2:
		case 3:
			return "a";
		case 4:
		case 5:
			return "b";
		case 6:
		case 7:
			return "c";
		case 8:
		case 9:
			return "d";
		}
		return "";
	}

	public static final void init()
	{
		int rev = 0;
		try
		{
			rev = Integer.parseInt(coreVersion.getRevisionNumber());
		}
		catch (Exception e)
		{	
		}
		if (rev == 0)
		{
			ver = SystemMessage.sendString(serverPackCodeName + " TEST SERVER");
		}
		else
		{
			rev += 1000;
			int letter = rev % 10;
			ver = SystemMessage.sendString(serverPackCodeName + " version " + rev / 1000 + "." + rev % 1000 / 10 +
					getLetter(letter) + " (" +
					new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z").format(new Date(coreVersion.getBuildTime())) + ")");
		}
	}
}
