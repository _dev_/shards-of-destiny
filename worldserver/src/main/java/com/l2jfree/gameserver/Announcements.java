/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.sod.i18n.Internationalization;

import com.l2jfree.gameserver.cache.HtmCache;
import com.l2jfree.gameserver.communitybbs.Manager.SettingsBBSManager;
import com.l2jfree.gameserver.model.L2World;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.SystemChatChannelId;
import com.l2jfree.gameserver.network.serverpackets.CreatureSay;
import com.l2jfree.gameserver.network.serverpackets.L2GameServerPacket;
import com.l2jfree.gameserver.network.client.packets.sendable.NpcHtmlMessage;
import com.l2jfree.gameserver.script.DateRange;
import com.l2jfree.lang.L2TextBuilder;

/**
 * This class ...
 * 
 * @version $Revision: 1.5.2.1.2.7 $ $Date: 2005/03/29 23:15:14 $
 */
public class Announcements
{
	private volatile Map<Locale, List<String>> _announcements;
	
	private Announcements()
	{
		_announcements = Collections.emptyMap();
		loadAnnouncements();
	}
	
	public void loadAnnouncements()
	{
		Internationalization i18n = Internationalization.getInstance();
		
		Map<Locale, List<String>> anno = new HashMap<Locale, List<String>>();
		for (Locale loc : SettingsBBSManager.getInstance().getAllLocales())
		{
			String[] all = i18n.get(loc, "ANNOUNCEMENTS").split("\0");
			anno.put(loc, Arrays.asList(all));
		}
		
		_announcements = anno;
		/*
		_announcements.clear();
		File file = new File(Config.DATAPACK_ROOT, "data/announcements.txt");
		if (file.exists())
		{
			readFromDisk(file);
		}
		else
		{
			_log.info("data/announcements.txt doesn't exist");
		}
		*/
	}
	
	public void showAnnouncements(L2PcInstance activeChar)
	{
		List<String> all = _announcements.get(activeChar.getSoDPlayer().getSettings().getLocale());
		for (String s : all)
			activeChar.sendPacket(new CreatureSay(0, SystemChatChannelId.Chat_Announce, "", s));
		/*
		for (int i = 0; i < _announcements.size(); i++)
		{
			CreatureSay cs = new CreatureSay(0, SystemChatChannelId.Chat_Announce, activeChar.getName(), _announcements
					.get(i).replace("%name%", activeChar.getName()));
			activeChar.sendPacket(cs);
		}
		
		Date currentDate = new Date();
		for (int i = 0; i < _eventAnnouncements.size(); i++)
		{
			List<Object> entry = _eventAnnouncements.get(i);
			
			DateRange validDateRange = (DateRange) entry.get(0);
			String[] msg = (String[]) entry.get(1);
			
			if (validDateRange.isValid() && validDateRange.isWithinRange(currentDate))
			{
				SystemMessage sm = new SystemMessage(SystemMessageId.S1);
				for (String element : msg)
					sm.addString(element);
				activeChar.sendPacket(sm);
			}
		}
		*/
	}
	
	public void addEventAnnouncement(DateRange validDateRange, String[] msg)
	{
		/*
		ArrayList<Object> entry = new ArrayList<Object>();
		entry.add(validDateRange);
		entry.add(msg);
		entry.trimToSize();
		_eventAnnouncements.add(entry);
		*/
	}
	
	public void listAnnouncements(L2PcInstance activeChar)
	{
		String content = HtmCache.getInstance().getHtmForce("data/html/admin/announce.htm");
		NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
		adminReply.setHtml(content);
		L2TextBuilder replyMSG = new L2TextBuilder();
		replyMSG.append("<br>");
		/*
		for (int i = 0; i < _announcements.size(); i++)
		{
			replyMSG.append("<table width=260><tr><td width=220>");
			replyMSG.append(_announcements.get(i));
			replyMSG.append("</td><td width=40><button value=\"Delete\" action=\"bypass -h admin_del_announcement ");
			replyMSG.append(i);
			replyMSG.append("\" width=60 height=15 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td></tr></table>");
		}
		*/
		replyMSG.append("Announcement modification is only allowed through localization.");
		adminReply.replace("%announces%", replyMSG.moveToString());
		activeChar.sendPacket(adminReply);
	}
	
	public void addAnnouncement(String text)
	{
		// _announcements.add(text);
		// saveToDisk();
	}
	
	public void delAnnouncement(int line)
	{
		// _announcements.remove(line);
		// saveToDisk();
	}
	
	public void announceToAll(String text)
	{
		CreatureSay cs = new CreatureSay(0, SystemChatChannelId.Chat_Announce, "", text);
		for (L2PcInstance player : L2World.getInstance().getAllPlayers())
		{
			player.sendPacket(cs);
		}
	}
	
	public void announceToInstance(L2GameServerPacket gsp, int instanceId)
	{
		for (L2PcInstance player : L2World.getInstance().getAllPlayers())
		{
			if (player.isSameInstance(instanceId))
				player.sendPacket(gsp);
		}
	}
	
	// Method fo handling announcements from admin
	public void handleAnnounce(String command, int lengthToTrim)
	{
		try
		{
			// Announce string to everyone on server
			String text = command.substring(lengthToTrim);
			announceToAll(text);
		}
		
		// No body cares!
		catch (StringIndexOutOfBoundsException e)
		{
			// empty message.. ignore
		}
	}
	
	/**
	 * Announce to players.<BR>
	 * <BR>
	 * 
	 * @param message
	 *            The String of the message to send to player
	 */
	public void announceToPlayers(String message)
	{
		// Get all players
		for (L2PcInstance player : L2World.getInstance().getAllPlayers())
			player.sendMessage(message);
	}
	
	public static Announcements getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final Announcements INSTANCE = new Announcements();
	}
}
