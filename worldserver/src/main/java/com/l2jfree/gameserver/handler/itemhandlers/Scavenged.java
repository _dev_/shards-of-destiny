/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.handler.itemhandlers;

import com.l2jfree.gameserver.handler.IItemHandler;
import com.l2jfree.gameserver.model.L2ItemInstance;
import com.l2jfree.gameserver.model.actor.L2Playable;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.skills.Stats;

/**
 * TODO Auto-generated JavaDoc
 * @author savormix
 * @since 2010.03.26
 */
public final class Scavenged implements IItemHandler
{
	private static final int[] ITEMS = new int[] { 90001 };

	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.handler.IItemHandler#getItemIds()
	 */
	@Override
	public int[] getItemIds()
	{
		return ITEMS;
	}

	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.handler.IItemHandler#useItem(com.l2jfree.gameserver.model.actor.L2Playable, com.l2jfree.gameserver.model.L2ItemInstance)
	 */
	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		L2PcInstance player = playable.getActingPlayer();
		double multi = player.calcStat(Stats.SCAVENGING, 0, null, null);
		player.getStatus().setCurrentCp(player.getCurrentCp() + player.getMaxCp() * multi);
		player.getStatus().setCurrentHpMp(player.getCurrentHp() + player.getMaxHp() * multi,
				player.getCurrentMp() + player.getMaxMp() * multi);
	}
}
