/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.handler.chathandlers;

import org.sod.manager.conquest.ConquestFactionManager;
import org.sod.manager.conquest.ConquestFactionManager.FactionTemplate;
import org.sod.model.event.global.GlobalEvent;
import org.sod.model.event.global.TeamedEvent;

import com.l2jfree.Config;
import com.l2jfree.gameserver.handler.IChatHandler;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.SystemChatChannelId;
import com.l2jfree.gameserver.network.serverpackets.CreatureSay;

public class ChatBattlefield implements IChatHandler
{
	private final SystemChatChannelId[] _chatTypes = { SystemChatChannelId.Chat_Battlefield };

	public SystemChatChannelId[] getChatTypes()
	{
		return _chatTypes;
	}

	public void useChatHandler(L2PcInstance activeChar, String target, SystemChatChannelId chatType, String text)
	{
		String name = (activeChar.isGM() && Config.GM_NAME_HAS_BRACELETS) ?
				"[GM]" + activeChar.getAppearance().getVisibleName() : activeChar.getName();

		CreatureSay cs = new CreatureSay(activeChar.getObjectId(), chatType, name, text);
		TeamedEvent<?> evt = GlobalEvent.getEvent(activeChar, TeamedEvent.class);
		if (evt != null)
		{
			evt.broadcastToParticipants(cs);
			return;
		}
		FactionTemplate ft = ConquestFactionManager.getInstance().getFactionTemplate(activeChar);
		if (ft != null)
			ft.broadcast(cs);
	}
}
