/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.handler.admincommandhandlers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.sod.manager.RespawnManager;
import org.sod.model.L2PlayerData;

import com.l2jfree.L2DatabaseFactory;
import com.l2jfree.gameserver.handler.IAdminCommandHandler;
import com.l2jfree.gameserver.model.L2World;
import com.l2jfree.gameserver.model.Location;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.zone.L2JailZone;

/**
 * This class handles following admin commands:
 * - jail charname [penalty_time] = jails character. Time specified in minutes. For ever if no time is specified.
 * - unjail charname = Unjails player, teleport him to Floran.
 * 
 * @version $Revision: 1.1.6.3 $ $Date: 2005/04/11 10:06:06 $
 */
public class AdminJail implements IAdminCommandHandler
{
	private static final String[]	ADMIN_COMMANDS	= { "admin_jail", "admin_unjail" };
	
	public static final String getXth(int num)
	{
		switch (num % 10)
		{
		case 1:
			return "st";
		case 2:
			return "nd";
		case 3:
			return "rd";
		default:
			return "th";
		}
	}
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance activeChar)
	{
		StringTokenizer st = new StringTokenizer(command);
		st.nextToken();
		if (command.startsWith("admin_jail"))
		{
			try
			{
				String player = st.nextToken();
				int delay = Integer.parseInt(st.nextToken());
				String reason = st.nextToken("\t");
				
				L2PcInstance playerObj = L2World.getInstance().getPlayer(player);
				if (playerObj != null)
				{
					L2PlayerData dat = playerObj.getSoDPlayer();
					int total = dat.getJailTotal() + 1;
					dat.setJailTotal(total);
					dat.setJailReason(reason);
					playerObj.setInJail(true, delay);
					playerObj.sendMessage("This is your " + total + getXth(total) + " time in jail.");
					activeChar.sendMessage("Character " + player + " jailed for " + (delay > 0 ? delay + " minutes." : "eternity!"));
				}
				else
					jailOfflinePlayer(activeChar, player, delay, reason);
			}
			catch (NoSuchElementException nsee)
			{
				activeChar.sendMessage("Usage: //jail <charname> <penalty_minutes> (0 - permanent) <reason>");
			}
			catch (NumberFormatException nfe)
			{
				activeChar.sendMessage("Usage: //jail <charname> <penalty_minutes> (0 - permanent) <reason>");
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		else if (command.startsWith("admin_unjail"))
		{
			try
			{
				String player = st.nextToken();
				L2PcInstance playerObj = L2World.getInstance().getPlayer(player);
				if (playerObj != null)
				{
					playerObj.setInJail(false, 0);
					activeChar.sendMessage(player + " removed from jail");
				}
				else
					unjailOfflinePlayer(activeChar, player);
			}
			catch (NoSuchElementException nsee)
			{
				activeChar.sendMessage("Specify a character name.");
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return true;
	}
	
	private void jailOfflinePlayer(L2PcInstance activeChar, String name, int delay, String reason)
	{
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection(con);
			
			PreparedStatement statement = con.prepareStatement("UPDATE characters SET x=?, y=?, z=?, in_jail=?, jail_timer=? WHERE char_name=?");
			statement.setInt(1, L2JailZone.JAIL_LOCATION.getX());
			statement.setInt(2, L2JailZone.JAIL_LOCATION.getY());
			statement.setInt(3, L2JailZone.JAIL_LOCATION.getZ());
			statement.setInt(4, 1);
			statement.setLong(5, delay * 60000L);
			statement.setString(6, name);
			
			statement.execute();
			int count = statement.getUpdateCount();
			statement.close();
			statement = con.prepareStatement("UPDATE character_jail SET total=total+1,reason=? WHERE charId=(SELECT charId FROM characters WHERE char_name=?)");
			statement.setString(1, reason);
			statement.setString(2, name);
			statement.executeUpdate();
			statement.close();
			
			if (count == 0)
				activeChar.sendMessage("Character not found!");
			else
				activeChar.sendMessage("Character " + name + " jailed offline for " + ((delay > 0) ? (" " + delay + " minutes.") : "eternity!"));
		}
		catch (SQLException se)
		{
			activeChar.sendMessage("SQLException while jailing player");
			se.printStackTrace();
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	private void unjailOfflinePlayer(L2PcInstance activeChar, String name)
	{
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection(con);
			PreparedStatement statement = con.prepareStatement("UPDATE characters SET x=?, y=?, z=?, in_jail=?, jail_timer=? WHERE char_name=?");
			Location loc = RespawnManager.getInstance().getBase().getLoc();
			statement.setInt(1, loc.getX());
			statement.setInt(2, loc.getY());
			statement.setInt(3, loc.getZ());
			statement.setInt(4, 0);
			statement.setLong(5, 0);
			statement.setString(6, name);
			
			statement.execute();
			int count = statement.getUpdateCount();
			statement.close();
			if (count == 0)
				activeChar.sendMessage("Character not found!");
			else
				activeChar.sendMessage("Character " + name + " removed from jail while offline.");
		}
		catch (SQLException se)
		{
			activeChar.sendMessage("SQLException while unjailing player");
			se.printStackTrace();
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
