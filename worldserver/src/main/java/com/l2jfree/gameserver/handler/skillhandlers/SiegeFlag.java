/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfree.gameserver.handler.skillhandlers;

import org.sod.manager.conquest.ConquestFactionManager;

import com.l2jfree.Config;
import com.l2jfree.gameserver.datatables.NpcTable;
import com.l2jfree.gameserver.handler.ISkillConditionChecker;
import com.l2jfree.gameserver.idfactory.IdFactory;
import com.l2jfree.gameserver.model.L2Clan;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.actor.instance.L2SiegeFlagInstance;
import com.l2jfree.gameserver.model.zone.L2Zone;
import com.l2jfree.gameserver.network.SystemMessageId;
import com.l2jfree.gameserver.skills.l2skills.L2SkillSiegeFlag;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;
import com.l2jfree.gameserver.templates.skills.L2SkillType;

public class SiegeFlag extends ISkillConditionChecker
{
	private static final L2SkillType[] SKILL_IDS = { L2SkillType.SIEGEFLAG };

	@Override
	public boolean checkConditions(L2Character activeChar, L2Skill skill)
	{
		if (!(activeChar instanceof L2PcInstance))
			return false;

		L2PcInstance player = activeChar.getActingPlayer();
		
		L2Clan clan = player.getClan();
		if (clan == null || !L2Clan.checkPrivileges(player, L2Clan.CP_CS_MANAGE_SIEGE))
			return false;
		else if (player.isInInstance())
		{
			player.sendPacket(SystemMessageId.NOT_SET_UP_BASE_HERE);
			return false;
		}
		else if (!ConquestFactionManager.getInstance().isPlaying(player))
			return false;
		else if (player.isInsideZone(L2Zone.FLAG_NO_HQ))
		{
			player.sendMessage("Enemy zone is too close!");
			return false;
		}
		else if (!ConquestFactionManager.getInstance().checkProximityToHq(player))
		{
			player.sendPacket(SystemMessageId.HEADQUARTERS_TOO_CLOSE);
			return false;
		}
		else if (clan.getNumFlags() >= Config.CONQUEST_MAX_CLAN_HEADQUARTERS)
		{
			player.sendPacket(SystemMessageId.NOT_ANOTHER_HEADQUARTERS);
			return false;
		}

		return super.checkConditions(activeChar, skill);
	}

	@Override
	public void useSkill(L2Character activeChar, L2Skill skill0, L2Character... targets)
	{
		L2SkillSiegeFlag skill = (L2SkillSiegeFlag)skill0;

		if (!(activeChar instanceof L2PcInstance))
			return;

		L2PcInstance player = activeChar.getActingPlayer();
		if (!checkConditions(activeChar, skill) || skill == null)
			return;

		L2NpcTemplate template = NpcTable.getInstance().getTemplate(35062);
		if (template != null)
		{
			// spawn a new flag
			L2SiegeFlagInstance flag = new L2SiegeFlagInstance(player, IdFactory.getInstance().getNextId(), template, skill.isAdvanced());
			flag.setTitle(player.getClan().getName());
			flag.getStatus().setCurrentHpMp(flag.getMaxHp(), flag.getMaxMp());
			flag.setHeading(player.getHeading());
			flag.spawnMe(player.getX(), player.getY(), player.getZ() + 50);
		}
	}

	@Override
	public L2SkillType[] getSkillIds()
	{
		return SKILL_IDS;
	}
}
