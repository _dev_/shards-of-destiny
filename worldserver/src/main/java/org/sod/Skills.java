/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod;

import com.l2jfree.gameserver.model.L2Clan;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;

/**
 * TODO Auto-generated JavaDoc
 * @author savormix
 * @since 2010.01.16
 */
public final class Skills
{
	public static final int SEAL_OF_RULER			= 246;
	public static final int BUILD_HEADQUARTERS		= 247;
	public static final int BUILD_ADV_HEADQUARTERS	= 326;

	public static final int ENERGY_DITCH			= 9100;
	public static final int SUMMON_SERVITOR			= 9107;
	public static final int UNHOLY_POWER			= 9109;
	public static final int SUICIDE_GRENADE			= 9116;
	public static final int CURSE_OF_ANOMALY		= 9120;
	public static final int TUTORIAL_SPEED			= 9127;

	private Skills()
	{
	}

	/**
	 * Gives a player all skills needed for a fluent SoD gameplay.
	 * <LI>Seal of Ruler - to capture a zone</LI>
	 * <LI>Energy Ditch - to activate the artefact</LI>
	 * <LI>TODO</LI>
	 * @param player
	 */
	public static final void addCustomSkills(L2PcInstance player)
	{
		player.addSkill(SEAL_OF_RULER, 1);
		player.addSkill(ENERGY_DITCH, 1);
		player.addSkill(SUMMON_SERVITOR, 1);
		clanStateChanged(player);
	}

	public static final void clanStateChanged(L2PcInstance player)
	{
		L2Clan clan = player.getClan();
		if (clan == null)
		{
			player.removeSkill(BUILD_HEADQUARTERS);
			player.removeSkill(BUILD_ADV_HEADQUARTERS);
		}
		else if (L2Clan.checkPrivileges(player, L2Clan.CP_CS_MANAGE_SIEGE))
		{
			player.addSkill(BUILD_HEADQUARTERS, 1);
			if (player.isNoble())
				player.addSkill(BUILD_ADV_HEADQUARTERS, 1);
		}
	}

	/**
	 * Removes all skills added by the previous method.
	 * @param player a player
	 * @see #addCustomSkills(L2PcInstance)
	 */
	public static final void removeCustomSkills(L2PcInstance player)
	{
		player.removeSkill(SEAL_OF_RULER);
		player.removeSkill(BUILD_HEADQUARTERS);
		player.removeSkill(BUILD_ADV_HEADQUARTERS);
		player.removeSkill(ENERGY_DITCH);
		player.removeSkill(SUMMON_SERVITOR);
	}
}
