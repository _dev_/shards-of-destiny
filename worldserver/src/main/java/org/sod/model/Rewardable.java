/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model;

import com.l2jfree.util.logging.L2Logger;


import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;

/**
 * A challenge reward superclass.
 * @author savormix
 * @since 2010.12.07
 */
public abstract class Rewardable
{
	protected static final L2Logger _log = L2Logger.getLogger(Rewardable.class);
	
	public static final Rewardable[] EMPTY_ARRAY = new Rewardable[0];
	
	protected Rewardable()
	{
	}
	
	/**
	 * Rewards the player
	 * @param player A player
	 * @param complementary false - give full reward (on achievement),
	 * true - give only lasting reward (on login)
	 */
	public abstract void giveReward(L2PcInstance player, boolean complementary);
	
	/**
	 * Validate if this reward can be used. Optionally print out an error message.
	 * @return whether this is a valid reward
	 */
	public abstract boolean validate();
}
