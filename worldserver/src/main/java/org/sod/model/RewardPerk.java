/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model;

import org.sod.manager.PerkManager;

import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;

/**
 * Constructs a perk challenge reward.
 * @author savormix
 * @since 2010.12.07
 */
public class RewardPerk extends Rewardable
{
	private final Integer _id;
	private final boolean _upgraded;
	
	public RewardPerk(int id, boolean upgraded)
	{
		_id = id;
		_upgraded = upgraded;
	}
	
	public Integer getId()
	{
		return _id;
	}
	
	public boolean isUpgraded()
	{
		return _upgraded;
	}
	
	/* (non-Javadoc)
	 * @see org.sod.model.Rewardable#giveReward(com.l2jfree.gameserver.model.actor.instance.L2PcInstance, boolean)
	 */
	@Override
	public void giveReward(L2PcInstance player, boolean complementary)
	{
		if (!complementary)
		{
			L2PlayerData dat = player.getSoDPlayer();
			dat.unlockPerk(getId());
			if (isUpgraded())
				dat.upgradePerk(getId(), Integer.MAX_VALUE);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.sod.model.Rewardable#validate()
	 */
	@Override
	public boolean validate()
	{
		if (PerkManager.getInstance().getTemplate(getId()) == null)
		{
			_log.warn("Invalid perk reward! ID: " + getId() + " upgrade: " + isUpgraded(),
					new IllegalArgumentException());
			return false;
		}
		return true;
	}
}
