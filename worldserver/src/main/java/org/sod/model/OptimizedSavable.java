/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model;

/**
 * This class allows simple management of often-updated models,
 * which (like Marathon perk) should not query the database on
 * each update, but still need to be saved during session.
 * @author savormix
 * @since 2010.01.26
 */
public abstract class OptimizedSavable
{
	public static final int		SAVING_INTERVAL = 10000;
	
	private volatile boolean	_changed;
	private volatile long		_nextSave;
	
	public boolean hasChanged()
	{
		return _changed;
	}
	
	public void setChanged()
	{
		_changed = true;
	}
	
	public long getNextSave()
	{
		return _nextSave;
	}
	
	public void setSaved()
	{
		_changed = false;
		_nextSave = System.currentTimeMillis() + SAVING_INTERVAL;
	}
	
	public boolean canSave()
	{
		return System.currentTimeMillis() > getNextSave();
	}
}
