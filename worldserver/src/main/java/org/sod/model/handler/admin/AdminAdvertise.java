/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.handler.admin;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javolution.util.FastMap;

import com.l2jfree.gameserver.datatables.NpcTable;
import com.l2jfree.gameserver.handler.IAdminCommandHandler;
import com.l2jfree.gameserver.model.L2Spawn;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.instance.AdvertiserInstance;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.client.packets.sendable.MagicSkillUse;
import com.l2jfree.gameserver.network.client.packets.sendable.SocialActionPacket.SocialAction;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;
import com.l2jfree.gameserver.util.Broadcast;
import com.l2jfree.lang.L2Math;

/**
 * TODO Auto-generated JavaDoc
 * 
 * @author savormix
 * @since 2010.09.03
 */
public class AdminAdvertise implements IAdminCommandHandler
{
	private static final String[] CMDS = new String[]
	{
			"admin_army_create", "admin_army_remove", "admin_army_action", "admin_army_race", "admin_army_sex",
			"admin_army_msu"
	};
	private static final FastMap<Integer, List<L2Spawn>> _spawns = FastMap.newInstance();
	private static final int NW = 0;
	private static final int NE = 1;
	private static final int SE = 2;
	private static final int SW = 3;
	
	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.handler.IAdminCommandHandler#useAdminCommand(java.lang.String, com.l2jfree.gameserver.model.actor.instance.L2PcInstance)
	 */
	@Override
	public boolean useAdminCommand(String command, L2PcInstance activeChar)
	{
		StringTokenizer st = new StringTokenizer(command, " ");
		command = st.nextToken();
		Integer id = null;
		if (command.equals(CMDS[1]))
		{
			try
			{
				id = Integer.valueOf(st.nextToken());
			}
			catch (Exception e)
			{
				activeChar.sendMessage("//army_remove id");
				return false;
			}
			List<L2Spawn> list = _spawns.remove(id);
			if (list == null)
			{
				activeChar.sendMessage("No army with ID " + id);
				return false;
			}
			for (L2Spawn sp : list)
			{
				sp.stopRespawn();
				L2Npc npc = sp.getLastSpawn();
				if (npc == null)
					continue;
				npc.deleteMe();
			}
			list.clear();
			activeChar.sendMessage("Removed army with ID " + id);
			return true;
		}
		else if (command.equals(CMDS[2]))
		{
			try
			{
				id = Integer.valueOf(st.nextToken());
			}
			catch (Exception e)
			{
				activeChar.sendMessage("//army_action id sa");
				return false;
			}
			List<L2Spawn> list = _spawns.get(id);
			if (list == null)
			{
				activeChar.sendMessage("No army with ID " + id);
				return false;
			}
			int sa = Integer.parseInt(st.nextToken());
			for (L2Spawn sp : list)
			{
				L2Npc npc = sp.getLastSpawn();
				if (npc == null)
					continue;
				SocialAction packet = new SocialAction(npc, sa);
				Broadcast.toKnownPlayers(npc, packet);
			}
		}
		else if (command.equals(CMDS[0]))
		{
			try
			{
				id = Integer.valueOf(st.nextToken());
				List<L2Spawn> list = _spawns.get(id);
				if (list != null)
				{
					activeChar.sendMessage("Already spawned army with ID " + id);
					return false;
				}
				list = new ArrayList<L2Spawn>();
				_spawns.put(id, list);
				int heading = Integer.parseInt(st.nextToken());
				boolean alongY = Integer.parseInt(st.nextToken()) == 1;
				int way = Integer.parseInt(st.nextToken());
				int length = Integer.parseInt(st.nextToken());
				int span = Integer.parseInt(st.nextToken());
				int count = Integer.parseInt(st.nextToken());
				int[] npcs = new int[count];
				int startX = activeChar.getX();
				int startY = activeChar.getY();
				int startZ = activeChar.getZ() + 100;
				for (int i = 0; i < count; i++)
				{
					try
					{
						npcs[i] = Integer.parseInt(st.nextToken());
					}
					catch (Exception e)
					{
						if (i == 0)
							npcs[i] = 90026;
						else
							npcs[i] = npcs[i - 1];
					}
					L2NpcTemplate temp = NpcTable.getInstance().getTemplate(npcs[i]);
					if (temp == null)
						continue;
					for (int j = 0; j < length; j++)
					{
						int a, x, b, y;
						if (alongY)
						{
							a = i;
							b = j;
						}
						else
						{
							a = j;
							b = i;
						}
						switch (way)
						{
						case NW:
							a *= -1;
							b *= -1;
							break;
						case NE:
							b *= -1;
							break;
						case SE:
							break;
						case SW:
						default:
							a *= -1;
							break;
						}
						x = startX + a * span;
						y = startY + b * span;
						L2Spawn sp = new L2Spawn(temp);
						sp.setAmount(1);
						sp.setLocx(x);
						sp.setLocy(y);
						sp.setLocz(startZ);
						sp.setHeading(heading);
						sp.setInstanceId(activeChar.getInstanceId());
						sp.init();
						list.add(sp);
					}
				}
			}
			catch (Exception e)
			{
				activeChar.sendMessage("//army_create id head NS(1)/WE(0) way ll ls lc <npc>");
				return false;
			}
		}
		else if (command.equals(CMDS[4]))
		{
			try
			{
				int sex = Integer.parseInt(st.nextToken());
				AdvertiserInstance.DEFAULT_SEX = sex;
			}
			catch (Exception e)
			{
				activeChar.sendMessage("//army_sex s");
				return false;
			}
			return true;
		}
		else if (command.equals(CMDS[3]))
		{
			String type;
			int race;
			try
			{
				type = st.nextToken();
				race = Integer.parseInt(st.nextToken());
			}
			catch (Exception e)
			{
				activeChar.sendMessage("//army_race t r");
				return false;
			}
			race = L2Math.limit(-1, race, 5);
			if (type.equals("tank"))
				AdvertiserInstance.TANK_RACE = race;
			else if (type.equals("mage"))
				AdvertiserInstance.MAGE_RACE = race;
			else if (type.equals("archer"))
				AdvertiserInstance.ARCHER_RACE = race;
			else
				AdvertiserInstance.DEFAULT_RACE = race;
			return true;
		}
		else if (command.equals(CMDS[5]))
		{
			try
			{
				id = Integer.valueOf(st.nextToken());
				List<L2Spawn> list = _spawns.get(id);
				if (list == null)
				{
					activeChar.sendMessage("No army with ID " + id);
					return false;
				}
				int skill = Integer.parseInt(st.nextToken());
				int level = Integer.parseInt(st.nextToken());
				int time = Integer.parseInt(st.nextToken());
				for (L2Spawn sp : list)
				{
					L2Npc npc = sp.getLastSpawn();
					if (npc == null)
						continue;
					
					MagicSkillUse msu = new MagicSkillUse(npc, skill, level, time);
					Broadcast.toKnownPlayers(npc, msu);
				}
			}
			catch (Exception e)
			{
				activeChar.sendMessage("//army_msu id sk lvl time");
				return false;
			}
		}
		return false;
	}
	
	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.handler.IAdminCommandHandler#getAdminCommandList()
	 */
	@Override
	public String[] getAdminCommandList()
	{
		return CMDS;
	}
}
