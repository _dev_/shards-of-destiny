/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.handler.admin;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import org.apache.commons.io.IOUtils;
import com.l2jfree.util.logging.L2Logger;


import com.l2jfree.gameserver.handler.IAdminCommandHandler;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.lang.L2TextBuilder;

/**
 * TODO Auto-generated JavaDoc
 * @author savormix
 * @since 2010.10.01
 */
public class AdminCaravan implements IAdminCommandHandler
{
	private static final L2Logger _log = L2Logger.getLogger(AdminCaravan.class);
	private static final String[] CMD = new String[] {
		"admin_cpos", "admin_cplist_close"
	};
	private BufferedWriter _bw = null;

	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.handler.IAdminCommandHandler#useAdminCommand(java.lang.String, com.l2jfree.gameserver.model.actor.instance.L2PcInstance)
	 */
	@Override
	public boolean useAdminCommand(String command, L2PcInstance activeChar)
	{
		if (command.equals(CMD[0]))
		{
			if (_bw == null)
			{
				File f = new File("caravan_" + System.currentTimeMillis() + ".txt");
				try
				{
					f.createNewFile();
					_bw = new BufferedWriter(new FileWriter(f));
				}
				catch (Exception e)
				{
					_log.warn("Could not create a writer for " + f.getName(), e);
				}
			}
			L2TextBuilder tb = new L2TextBuilder();
			tb.append("\t\tnew L2CharPosition(");
			tb.append(activeChar.getX());
			tb.append(", ");
			tb.append(activeChar.getY());
			tb.append(", ");
			tb.append(activeChar.getZ());
			tb.append(", ");
			tb.append(activeChar.getHeading());
			tb.append("),\n");
			try
			{
				_bw.write(tb.moveToString());
				//_bw.flush();
			}
			catch (Exception e)
			{
				_log.warn("Could not write caravan position!", e);
				return false;
			}
		}
		else
		{
			if (_bw == null)
				return false;
			IOUtils.closeQuietly(_bw);
			_bw = null;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.handler.IAdminCommandHandler#getAdminCommandList()
	 */
	@Override
	public String[] getAdminCommandList()
	{
		return CMD;
	}
}
