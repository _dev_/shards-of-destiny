/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.handler.item;

import org.sod.Skills;

import com.l2jfree.gameserver.datatables.SkillTable;
import com.l2jfree.gameserver.handler.IItemHandler;
import com.l2jfree.gameserver.model.L2ItemInstance;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.actor.L2Playable;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.SystemMessageId;
import com.l2jfree.gameserver.network.client.packets.sendable.SystemMessagePacket.SystemMessage;

/**
 * TODO Auto-generated JavaDoc
 * 
 * @author savormix
 * @since 2010.04.26
 */
public class SuicideGrenade implements IItemHandler
{
	private static final int[] ITEMS =
	{
		90005
	};
	
	@Override
	public int[] getItemIds()
	{
		return ITEMS;
	}
	
	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if (!(playable instanceof L2PcInstance))
			return;
		L2PcInstance player = playable.getActingPlayer();
		if (!player.getEffects().hasEffect(Skills.SUICIDE_GRENADE))
		{
			if (player.destroyItem("Suicide", item, 1, null, true))
			{
				L2Skill sk = SkillTable.getInstance().getInfo(Skills.SUICIDE_GRENADE, 1);
				sk.getEffects(player, player);
				player.sendPacket(new SystemMessage(SystemMessageId.YOU_FEEL_S1_EFFECT).addSkill(sk));
			}
		}
		else
			player.getSoDPlayer().sendLocalizedMessage("ITEM_SUICIDE_GRENADE_DISCLAIMER");
	}
}
