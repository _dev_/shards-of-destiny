/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.handler.skill;

import org.sod.model.ChallengeTemplate;
import org.sod.model.L2PlayerData;
import org.sod.model.event.global.GlobalEvent;

import com.l2jfree.gameserver.handler.ISkillConditionChecker;
import com.l2jfree.gameserver.model.L2Object;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.actor.instance.L2ZoneArtefactInstance;
import com.l2jfree.gameserver.model.zone.L2FactionZone;
import com.l2jfree.gameserver.model.zone.L2Zone;
import com.l2jfree.gameserver.network.SystemMessageId;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.Chat;
import com.l2jfree.gameserver.templates.skills.L2SkillType;
import com.l2jfree.gameserver.util.Broadcast;
import com.l2jfree.gameserver.util.Util;

/**
 * Seal of Ruler skill handler.
 * 
 * @author savormix
 * @since 2009.12.04
 */
public class TakeZone extends ISkillConditionChecker
{
	private static final L2SkillType[] TAKE_ZONE =
	{
		L2SkillType.TAKECASTLE
	};
	
	@Override
	public boolean checkConditions(L2Character activeChar, L2Skill skill)
	{
		if (activeChar == null || !(activeChar instanceof L2PcInstance))
			return false;
		L2PcInstance player = activeChar.getActingPlayer();
		L2Object target = player.getTarget();
		if (target == null || !(target instanceof L2ZoneArtefactInstance))
		{
			player.sendPacket(SystemMessageId.TARGET_IS_INCORRECT);
			return false;
		}
		if (!Util.checkIfInRange(200, player, target, true))
		{
			player.sendPacket(SystemMessageId.TARGET_TOO_FAR);
			return false;
		}
		
		L2FactionZone zone = ((L2ZoneArtefactInstance) target).getFactionZone();
		if (zone == null || zone.isLocked() || !zone.isEnemy(player))
		{
			player.sendPacket(SystemMessageId.TARGET_IS_INCORRECT);
			return false;
		}
		else if (((L2ZoneArtefactInstance) target).isInsideZone(L2Zone.FLAG_CASTLE)
				&& activeChar.getZ() < target.getZ())
		{
			player.sendPacket(SystemMessageId.CANT_SEE_TARGET);
			return false;
		}
		
		zone.addSeal(player);
		zone.broadcastToOwners(SystemMessageId.OPPONENT_STARTED_ENGRAVING.getSystemMessage());
		Broadcast.toAllOnlinePlayers(Chat.COMMANDER, zone.getName(), "CONQUEST_SEALING_STARTED", "%name%",
				player.getName());
		return true;
	}
	
	@Override
	public L2SkillType[] getSkillIds()
	{
		return TAKE_ZONE;
	}
	
	@Override
	public void useSkill(L2Character activeChar, L2Skill skill, L2Character... targets)
	{
		if (!(activeChar instanceof L2PcInstance))
			return;
		
		L2PcInstance player = activeChar.getActingPlayer();
		if (targets.length < 1 || !(targets[0] instanceof L2ZoneArtefactInstance))
			return;
		L2ZoneArtefactInstance art = (L2ZoneArtefactInstance) targets[0];
		L2FactionZone zone = art.getFactionZone();
		if (zone == null || zone.isLocked())
			return;
		L2PlayerData dat = player.getSoDPlayer();
		player.addItem("Sealed", GlobalEvent.EVENT_MEDAL, 2, null, true);
		player.addAdena("Sealed", 35, art, true);
		// for (L2PcInstance nearby : art.getKnownList().getKnownPlayersInRadius(1500))
		// if (nearby != player && art.getFactionZone().isEnemy(nearby))
		// nearby.addAdena("Near Sealer", 5, art, true);
		for (L2Character cha : zone.getCharactersInsideActivated())
			if (cha instanceof L2PcInstance && cha != player && zone.isEnemy(cha))
				cha.getActingPlayer().addAdena("Seal Helper", 15, art, true);
		
		int att = zone.getPlayersInside() - zone.getDefenders();
		if (zone.getDefenders() >= att * 5)
			dat.tryAddChallengePoints(ChallengeTemplate.NO_ODDS, 1);
		
		zone.changeOwner(dat.getConquestFaction(), false);
		dat.tryAddChallengePoints(ChallengeTemplate.WAR_HERO, 1);
	}
}
