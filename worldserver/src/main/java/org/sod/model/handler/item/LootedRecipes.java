/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.handler.item;

import org.sod.manager.LootedGoodsManager;

import com.l2jfree.gameserver.handler.IItemHandler;
import com.l2jfree.gameserver.model.L2ItemInstance;
import com.l2jfree.gameserver.model.actor.L2Playable;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;

/**
 * @author savormix
 * @since 2010.03.01
 */
public class LootedRecipes implements IItemHandler
{
	private static final int[] ITEM_IDS = {
		LootedGoodsManager.CHEST_RECIPES
	};
	
	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if (!(playable instanceof L2PcInstance))
			return;
		
		L2PcInstance activeChar = playable.getActingPlayer();
		if (activeChar.isAlikeDead())
			return;
		
		if (activeChar.destroyItem("Rec Chest - Consume", item, 1, null, true))
			activeChar.addItem("Rec Chest - Reward", LootedGoodsManager.getInstance().getRandomRecipe(), 1, item, true);
	}
	
	@Override
	public int[] getItemIds()
	{
		return ITEM_IDS;
	}
}
