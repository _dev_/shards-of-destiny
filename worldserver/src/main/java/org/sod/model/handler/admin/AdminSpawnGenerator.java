/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.handler.admin;

import javolution.text.TextBuilder;

import com.l2jfree.Config;
import com.l2jfree.gameserver.datatables.NpcTable;
import com.l2jfree.gameserver.datatables.SpawnTable;
import com.l2jfree.gameserver.handler.IAdminCommandHandler;
import com.l2jfree.gameserver.instancemanager.CastleManager;
import com.l2jfree.gameserver.instancemanager.FortManager;
import com.l2jfree.gameserver.instancemanager.MapRegionManager;
import com.l2jfree.gameserver.instancemanager.TownManager;
import com.l2jfree.gameserver.instancemanager.ZoneManager;
import com.l2jfree.gameserver.model.L2Spawn;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.entity.Castle;
import com.l2jfree.gameserver.model.entity.Fort;
import com.l2jfree.gameserver.model.zone.L2Zone;
import com.l2jfree.gameserver.model.zone.L2Zone.ZoneType;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;
import com.l2jfree.util.Rnd;

/**
 * TODO Auto-generated JavaDoc
 * @author savormix
 * @since 2010.01.29
 */
public final class AdminSpawnGenerator implements IAdminCommandHandler
{
	private static final String[] CMD = {
		"admin_spawn_t_guard_f1", "admin_spawn_t_guard_f2", "admin_spawn_t_guard_r", "admin_spawn_t_guard_m"
	};
	private static final int FIGHTER_POLE = 0;
	private static final int FIGHTER_SWORD = 1;
	private static final int ARCHER = 2;
	private static final int MAGE = 3;

	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.handler.IAdminCommandHandler#getAdminCommandList()
	 */
	@Override
	public String[] getAdminCommandList()
	{
		return CMD;
	}

	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.handler.IAdminCommandHandler#useAdminCommand(java.lang.String, com.l2jfree.gameserver.model.actor.instance.L2PcInstance)
	 */
	@Override
	public boolean useAdminCommand(String command, L2PcInstance activeChar)
	{
		L2Zone zone = ZoneManager.getInstance().isInsideZone(ZoneType.Faction, activeChar.getX(), activeChar.getY());
		if (zone == null)
		{
			activeChar.sendMessage("You are not in a faction zone.");
			return false;
		}
		int type;
		if (command.endsWith("1"))
			type = FIGHTER_POLE;
		else if (command.endsWith("2"))
			type = FIGHTER_SWORD;
		else if (command.endsWith("r"))
			type = ARCHER;
		else
			type = MAGE;
		int region = MapRegionManager.getInstance().getL2Region(activeChar);
		int npcId = -100;
		switch (region)
		{
		case 1: case 2: case 7: // Gludio
			switch (type)
			{
			case MAGE:
				if (Rnd.nextBoolean())
					npcId = 35067;
				else
					npcId = 35080;
				break;
			case FIGHTER_POLE:
				if (Rnd.nextBoolean())
					npcId = 35070;
				else
					npcId = 35083;
				break;
			case FIGHTER_SWORD:
				if (Rnd.nextBoolean())
					npcId = 35071;
				else
					npcId = 35084;
				break;
			case ARCHER:
				if (Rnd.nextBoolean())
					npcId = 35072;
				else
					npcId = 35085;
				break;
			}
			if (type == MAGE)
				npcId += Rnd.get(2);
			else
				npcId += (Rnd.get(3) * 3);
			break;
		case 3: case 4: case 10: case 11: // Oren
			switch (type)
			{
			case MAGE:
				if (Rnd.nextBoolean())
					npcId = 35193;
				else
					npcId = 35206;
				break;
			case FIGHTER_POLE:
				if (Rnd.nextBoolean())
					npcId = 35196;
				else
					npcId = 35209;
				break;
			case FIGHTER_SWORD:
				if (Rnd.nextBoolean())
					npcId = 35197;
				else
					npcId = 35210;
				break;
			case ARCHER:
				if (Rnd.nextBoolean())
					npcId = 35198;
				else
					npcId = 35211;
				break;
			}
			if (type == MAGE)
				npcId += Rnd.get(2);
			else
				npcId += (Rnd.get(3) * 3);
			break;
		case 5: // Dion
			switch (type)
			{
			case MAGE:
				if (Rnd.nextBoolean())
					npcId = 35109;
				else
					npcId = 35122;
				break;
			case FIGHTER_POLE:
				if (Rnd.nextBoolean())
					npcId = 35112;
				else
					npcId = 35125;
				break;
			case FIGHTER_SWORD:
				if (Rnd.nextBoolean())
					npcId = 35113;
				else
					npcId = 35126;
				break;
			case ARCHER:
				if (Rnd.nextBoolean())
					npcId = 35114;
				else
					npcId = 35127;
				break;
			}
			if (type == MAGE)
				npcId += Rnd.get(2);
			else
				npcId += (Rnd.get(3) * 3);
			break;
		case 6: // Giran
			switch (type)
			{
			case MAGE:
				if (Rnd.nextBoolean())
					npcId = 35151;
				else
					npcId = 35164;
				break;
			case FIGHTER_POLE:
				if (Rnd.nextBoolean())
					npcId = 35154;
				else
					npcId = 35167;
				break;
			case FIGHTER_SWORD:
				if (Rnd.nextBoolean())
					npcId = 35155;
				else
					npcId = 35168;
				break;
			case ARCHER:
				if (Rnd.nextBoolean())
					npcId = 35156;
				else
					npcId = 35169;
				break;
			}
			if (type == MAGE)
				npcId += Rnd.get(2);
			else
				npcId += (Rnd.get(3) * 3);
			break;
		case 9: // Schuttgart
			switch (type)
			{
			case MAGE:
				if (Rnd.nextBoolean())
					npcId = 35519;
				else
					npcId = 35532;
				break;
			case FIGHTER_POLE:
				if (Rnd.nextBoolean())
					npcId = 35522;
				else
					npcId = 35535;
				break;
			case FIGHTER_SWORD:
				if (Rnd.nextBoolean())
					npcId = 35523;
				else
					npcId = 35536;
				break;
			case ARCHER:
				if (Rnd.nextBoolean())
					npcId = 35524;
				else
					npcId = 35537;
				break;
			}
			if (type == MAGE)
				npcId += Rnd.get(2);
			else
				npcId += (Rnd.get(3) * 3);
			break;
		case 12: // Innadril
			switch (type)
			{
			case MAGE:
				if (Rnd.nextBoolean())
					npcId = 35283;
				else
					npcId = 35296;
				break;
			case FIGHTER_POLE:
				if (Rnd.nextBoolean())
					npcId = 35286;
				else
					npcId = 35299;
				break;
			case FIGHTER_SWORD:
				if (Rnd.nextBoolean())
					npcId = 35287;
				else
					npcId = 35300;
				break;
			case ARCHER:
				if (Rnd.nextBoolean())
					npcId = 35288;
				else
					npcId = 35301;
				break;
			}
			if (type == MAGE)
				npcId += Rnd.get(2);
			else
				npcId += (Rnd.get(3) * 3);
			break;
		case 13: // Aden
			switch (type)
			{
			case MAGE:
				if (Rnd.nextBoolean())
					npcId = 35236;
				else
					npcId = 35249;
				break;
			case FIGHTER_POLE:
				if (Rnd.nextBoolean())
					npcId = 35239;
				else
					npcId = 35252;
				break;
			case FIGHTER_SWORD:
				if (Rnd.nextBoolean())
					npcId = 35240;
				else
					npcId = 35253;
				break;
			case ARCHER:
				if (Rnd.nextBoolean())
					npcId = 35241;
				else
					npcId = 35254;
				break;
			}
			if (type == MAGE)
				npcId += Rnd.get(2);
			else
				npcId += (Rnd.get(3) * 3);
			break;
		case 14: // Rune
			switch (type)
			{
			case MAGE:
				if (Rnd.nextBoolean())
					npcId = 35472;
				else
					npcId = 35485;
				break;
			case FIGHTER_POLE:
				if (Rnd.nextBoolean())
					npcId = 35475;
				else
					npcId = 35488;
				break;
			case FIGHTER_SWORD:
				if (Rnd.nextBoolean())
					npcId = 35476;
				else
					npcId = 35489;
				break;
			case ARCHER:
				if (Rnd.nextBoolean())
					npcId = 35477;
				else
					npcId = 35490;
				break;
			}
			if (type == MAGE)
				npcId += Rnd.get(2);
			else
				npcId += (Rnd.get(3) * 3);
			break;
		case 15: // Goddard
			switch (type)
			{
			case MAGE:
				if (Rnd.nextBoolean())
					npcId = 35327;
				else
					npcId = 35340;
				break;
			case FIGHTER_POLE:
				if (Rnd.nextBoolean())
					npcId = 35330;
				else
					npcId = 35343;
				break;
			case FIGHTER_SWORD:
				if (Rnd.nextBoolean())
					npcId = 35331;
				else
					npcId = 35344;
				break;
			case ARCHER:
				if (Rnd.nextBoolean())
					npcId = 35332;
				else
					npcId = 35345;
				break;
			}
			if (type == MAGE)
				npcId += Rnd.get(2);
			else
				npcId += (Rnd.get(3) * 3);
			break;
		}

		L2NpcTemplate template = NpcTable.getInstance().getTemplate(npcId);
		if (template == null)
		{
			activeChar.sendMessage("NPC template ID " + npcId + " not found.");
			return false;
		}

		try
		{
			int x = activeChar.getX();
			int y = activeChar.getY();
			int z = activeChar.getZ();
			int heading = activeChar.getHeading();

			L2Spawn spawn = new L2Spawn(template);
			spawn.setCustom();
			spawn.setLocx(x);
			spawn.setLocy(y);
			spawn.setLocz(z);
			spawn.setAmount(1);
			spawn.setHeading(heading);
			spawn.setRespawnDelay(Config.STANDARD_RESPAWN_DELAY);
			spawn.setInstanceId(0);
			Castle c = CastleManager.getInstance().getCastle(activeChar);
			Fort f = FortManager.getInstance().getFort(activeChar);
			TextBuilder tb = new TextBuilder();
			if (c != null)
			{
				tb.append(c.getName());
				tb.append(" Castle - ");
			}
			else if (f != null)
			{
				tb.append(f.getName());
				tb.append(" Fort - ");
			}
			else
			{
				String s = TownManager.getInstance().getClosestTownName(activeChar);
				s = s.replace("Town of ", "");
				s = s.replace(" Township", "");
				s = s.replace(" Village", "");
				s = s.replace("Talking Island", "TI");
				s = s.replace("Dark Elven", "DE");
				tb.append(s);
				tb.append(" Town - ");
			}
			tb.append("Ter. Guard (");
			switch (type)
			{
			case MAGE: tb.append("Mage"); break;
			case ARCHER: tb.append("Archer"); break;
			case FIGHTER_POLE: tb.append("Pole"); break;
			case FIGHTER_SWORD: tb.append("Sword"); break;
			}
			tb.append(')');
			SpawnTable.getInstance().addNewSpawn(spawn, true, tb.toString());
			spawn.init();
			TextBuilder.recycle(tb);
		}
		catch (Exception e)
		{
		}
		return true;
	}
}
