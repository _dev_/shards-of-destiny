/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.handler.admin;

import org.sod.manager.GlobalEventManager;

import com.l2jfree.gameserver.handler.IAdminCommandHandler;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;

/**
 * This handler allows a GM to manually initiate voting for a global event.
 * @author savormix
 * @since 2010.09.08
 */
public class AdminGlobalEvent implements IAdminCommandHandler
{
	private static final String[] COMMANDS = {
		"admin_ge_start", "admin_ge_restart"
	};

	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.handler.IAdminCommandHandler#useAdminCommand(java.lang.String, com.l2jfree.gameserver.model.actor.instance.L2PcInstance)
	 */
	@Override
	public boolean useAdminCommand(String command, L2PcInstance activeChar)
	{
		if (command.equals(COMMANDS[0]))
		{
			boolean result = GlobalEventManager.getInstance().startTask();
			if (result)
				activeChar.sendMessage("Event selection started!");
			else
				activeChar.sendMessage("Event already in progress!");
			return result;
		}
		else
		{
			boolean result = GlobalEventManager.getInstance().restartTask();
			if (result)
				activeChar.sendMessage("Event/Voting progress was reset!");
			else
				activeChar.sendMessage("Event/Voting not in progress!");
			return result;
		}
	}

	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.handler.IAdminCommandHandler#getAdminCommandList()
	 */
	@Override
	public String[] getAdminCommandList()
	{
		return COMMANDS;
	}
}
