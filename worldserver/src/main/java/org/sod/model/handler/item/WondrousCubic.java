/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.handler.item;

import com.l2jfree.gameserver.handler.IItemHandler;
import com.l2jfree.gameserver.model.L2ItemInstance;
import com.l2jfree.gameserver.model.actor.L2Playable;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.util.Rnd;

/**
 * @author Horus
 * @version $1.0.0.0.0
 * WondrousCubic Handler (Reward differs on player's expertise)
 */
public class WondrousCubic implements IItemHandler
{
	// All the item IDs that this handler knows.
	private static final int[]	ITEM_IDS	=
											{ 10632 };

	@Override
	public void useItem(final L2Playable playable, final L2ItemInstance item)
	{		
		if (!(playable instanceof L2PcInstance))
			return;

		final L2PcInstance activeChar = (L2PcInstance) playable;
		if (activeChar.isDead())
			return;

		int chance = Rnd.get(0, 100);
		int reward = 0;

		if ((activeChar.getPkKills() + activeChar.getPvpKills()) >= 3000)
		{
			if (chance >= 0 && chance < 41)
				reward = 10647; //Fine Black Cubic Piece
			else if (chance >= 41 && chance < 71)
				reward = 10644; //Fine Green Cubic Piece
			else if (chance >= 71 && chance < 86)
				reward = 10642; //Fine Blue Cubic Piece
			else if (chance >= 86 && chance < 91)
				reward = 10647; //Fine Red Cubic Piece
			else if (chance >= 91 && chance < 98)
				reward = 10645; //Fine White Cubic Piece
			else //if(chance >= 98 && chance < 100)
				reward = 10643; //Fine Yellow Cubic Piece 
		}
		else //if ((activeChar.getPkKills() + activeChar.getPvpKills()) < 3000)
		{			
			if (chance >= 0 && chance < 41)
				reward = 10638; //Rough Black Cubic Piece
			else if (chance >= 41 && chance < 71)
				reward = 10635; //Rough Green Cubic Piece
			else if (chance >= 71 && chance < 86)
				reward = 10633; //Rough Blue Cubic Piece
			else if (chance >= 86 && chance < 91)
				reward = 10636; //Rough Red Cubic Piece
			else if (chance >= 91 && chance < 98)
				reward = 10637; //Rough White Cubic Piece
			else //if (chance >= 98 && chance < 100)
				reward = 10634; //Rough Yellow Cubic Piece 
		}

		activeChar.destroyItem("Wondrous Cubic - Consume", item, 1, activeChar, true);
		activeChar.addItem("Wondrous Cubic - Reward", reward, 1, activeChar, true);
	}

	@Override
	public int[] getItemIds()
	{
		return ITEM_IDS;
	}
}
