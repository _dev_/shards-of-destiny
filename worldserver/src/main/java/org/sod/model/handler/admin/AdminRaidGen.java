/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.handler.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.StringTokenizer;

import com.l2jfree.util.logging.L2Logger;


import com.l2jfree.L2DatabaseFactory;
import com.l2jfree.gameserver.handler.IAdminCommandHandler;
import com.l2jfree.gameserver.instancemanager.BossSpawnManager;
import com.l2jfree.gameserver.instancemanager.GrandBossSpawnManager;
import com.l2jfree.gameserver.instancemanager.RaidBossSpawnManager;
import com.l2jfree.gameserver.model.L2Spawn;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;

/**
 * TODO Auto-generated JavaDoc
 * @author savormix
 * @since 2010.08.18
 */
public class AdminRaidGen implements IAdminCommandHandler
{
	private static final L2Logger _log = L2Logger.getLogger(AdminRaidGen.class);
	private static final String[] CMD = {
		"admin_raid_add", "admin_raid_remove"
	};
	private static final String EXAMPLE_ADD = "//raid_add <id> <min respawn delay> [max respawn delay]";

	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.handler.IAdminCommandHandler#useAdminCommand(java.lang.String, com.l2jfree.gameserver.model.actor.instance.L2PcInstance)
	 */
	@Override
	public boolean useAdminCommand(String command, L2PcInstance activeChar)
	{
		StringTokenizer st = new StringTokenizer(command, " ");
		command = st.nextToken();
		
		int id = -1;
		try
		{
			id = Integer.parseInt(st.nextToken());
		}
		catch (Exception e)
		{
		}
		if (id == -1)
		{
			activeChar.sendMessage("You must specify a raidboss ID!");
			return false;
		}
		
		BossSpawnManager bsm = RaidBossSpawnManager.getInstance();
		L2NpcTemplate temp = bsm.getValidTemplate(id);
		if (temp == null)
		{
			bsm = GrandBossSpawnManager.getInstance();
			temp = bsm.getValidTemplate(id);
		}
		if (temp == null)
		{
			activeChar.sendMessage("Incorrect raid boss ID!");
			return false;
		}
		
		if (command.equals(CMD[0]))
		{
			int delayMin = -1, delayMax = -1;
			try
			{
				delayMin = Integer.parseInt(st.nextToken());
				delayMax = Integer.parseInt(st.nextToken());
			}
			catch (Exception e)
			{
				if (delayMin == -1)
				{
					activeChar.sendMessage(EXAMPLE_ADD);
					return false;
				}
				else
					delayMax = delayMin;
			}
			
			if (delayMin < 3600)
			{
				activeChar.sendMessage("Respawn delay must be at least 60 minutes!");
				return false;
			}
			else if (delayMax < delayMin)
			{
				activeChar.sendMessage("Invalid max respawn delay!");
				return false;
			}
			else if (bsm.isDefined(id))
			{
				activeChar.sendMessage("This raid boss is already managed.");
				return false;
			}
			
			Connection con = null;
			try
			{
				con = L2DatabaseFactory.getInstance().getConnection();
				PreparedStatement ps;
				if (bsm instanceof GrandBossSpawnManager)
					ps = con.prepareStatement("INSERT INTO grandboss_spawnlist VALUES (?,?,?,?,?,?,?,0,?,?)");
				else
					ps = con.prepareStatement("INSERT INTO raidboss_spawnlist VALUES (?,1,?,?,?,?,?,?,0,?,?)");
				ps.setInt(1, id);
				ps.setInt(2, activeChar.getX());
				ps.setInt(3, activeChar.getY());
				ps.setInt(4, activeChar.getZ());
				ps.setInt(5, activeChar.getHeading());
				ps.setInt(6, delayMin);
				ps.setInt(7, delayMax);
				ps.setInt(8, (int) temp.getBaseHpMax());
				ps.setInt(9, (int) temp.getBaseMpMax());
				ps.executeUpdate();
				ps.close();
				
				/*
				if (bsm instanceof GrandBossSpawnManager)
				{
					ps = con.prepareStatement("INSERT INTO grandboss_intervallist VALUES (?,0,0)");
					ps.setInt(1, id);
					ps.executeUpdate();
					ps.close();
				}
				*/
				
				L2Spawn sp = new L2Spawn(temp);
				sp.setLoc(activeChar);
				sp.setRespawnMinDelay(delayMin);
				sp.setRespawnMaxDelay(delayMax);
				bsm.addNewSpawn(sp, 0, temp.getBaseHpMax(), temp.getBaseMpMax(), false);
				if (!activeChar.isInMultiverse())
					activeChar.setInstanceId(sp.getInstanceId());
				return true;
			}
			catch (Exception e)
			{
				_log.warn("Could not add the raid boss to database!", e);
				return false;
			}
			finally
			{
				L2DatabaseFactory.close(con);
			}
		}
		else
		{
			if (!bsm.isDefined(id))
			{
				activeChar.sendMessage("This raid boss is not managed.");
				return false;
			}
			
			/*
			if (bsm instanceof GrandBossSpawnManager)
			{
				Connection con = null;
				try
				{
					con = L2DatabaseFactory.getInstance().getConnection();
					PreparedStatement ps = con.prepareStatement("INSERT INTO grandboss_intervallist VALUES (?,0,0)");
					ps.setInt(1, id);
					ps.executeUpdate();
					ps.close();
				}
				catch (Exception e)
				{
					_log.warn("Could not remove the raid boss from database!", e);
					return false;
				}
				finally
				{
					L2DatabaseFactory.close(con);
				}
			}
			*/
			
			L2Spawn sp = bsm.getSpawns().get(id);
			bsm.deleteSpawn(sp, true);
			activeChar.sendMessage("Raid boss removed!");
			return true;
		}
	}

	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.handler.IAdminCommandHandler#getAdminCommandList()
	 */
	@Override
	public String[] getAdminCommandList()
	{
		return CMD;
	}
}
