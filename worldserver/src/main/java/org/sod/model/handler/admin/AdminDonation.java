/*
 * MISSING LICENSING INFO
 */
package org.sod.model.handler.admin;

import org.sod.model.L2PlayerData;

import com.l2jfree.gameserver.handler.IAdminCommandHandler;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.SystemMessageId;

/**
 * @author savormix
 * 
 */
public final class AdminDonation implements IAdminCommandHandler
{
	private static final String[] CMDS =
	{
			"admin_donator", "admin_donator_tmp"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance activeChar)
	{
		L2PcInstance target = activeChar.getTarget(L2PcInstance.class);
		if (target == null)
		{
			activeChar.sendPacket(SystemMessageId.INCORRECT_TARGET);
			return false;
		}
		
		L2PlayerData dat = target.getSoDPlayer();
		boolean donator = dat.isDonator();
		if (donator)
			activeChar.sendMessage(target + " is no longer a donator.");
		else
			activeChar.sendMessage(target + " is now a donator.");
		
		if (!command.endsWith("_tmp"))
			dat.saveDonator();
		return false;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return CMDS;
	}
}
