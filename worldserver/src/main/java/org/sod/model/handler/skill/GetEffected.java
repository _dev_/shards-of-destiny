/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.handler.skill;

import com.l2jfree.gameserver.datatables.SkillTable;
import com.l2jfree.gameserver.handler.ISkillConditionChecker;
import com.l2jfree.gameserver.model.L2Object;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.actor.instance.L2ZoneArtefactInstance;
import com.l2jfree.gameserver.model.zone.L2FactionZone;
import com.l2jfree.gameserver.model.zone.L2Zone;
import com.l2jfree.gameserver.network.SystemMessageId;
import com.l2jfree.gameserver.skills.l2skills.L2SkillGetEffect;
import com.l2jfree.gameserver.templates.skills.L2SkillType;
import com.l2jfree.gameserver.util.Util;

/**
 * Skills that just add another skill(s) effect to the player.
 * 
 * @author savormix
 * @since 2009.12.04
 */
public class GetEffected extends ISkillConditionChecker
{
	private static final L2SkillType[] TYPE =
	{
		L2SkillType.GET_EFFECT
	};
	
	@Override
	public boolean checkConditions(L2Character activeChar, L2Skill skill)
	{
		if (activeChar == null || !(activeChar instanceof L2PcInstance))
			return false;
		L2PcInstance player = activeChar.getActingPlayer();
		L2Object target = player.getTarget();
		if (target == null || !(target instanceof L2ZoneArtefactInstance))
		{
			player.sendPacket(SystemMessageId.TARGET_IS_INCORRECT);
			return false;
		}
		if (!Util.checkIfInRange(skill.getCastRange(), player, target, true))
		{
			player.sendPacket(SystemMessageId.TARGET_TOO_FAR);
			return false;
		}
		
		L2FactionZone zone = ((L2ZoneArtefactInstance) target).getFactionZone();
		if (zone == null || zone.isLocked() || zone.isEnemy(player))
		{
			player.sendPacket(SystemMessageId.TARGET_IS_INCORRECT);
			return false;
		}
		else if (((L2ZoneArtefactInstance) target).isInsideZone(L2Zone.FLAG_CASTLE)
				&& activeChar.getZ() > target.getZ())
		{
			player.getSoDPlayer().sendLocalizedMessage("CONQUEST_CANNOT_AWAKEN_HERE");
			return false;
		}
		return true;
	}
	
	@Override
	public L2SkillType[] getSkillIds()
	{
		return TYPE;
	}
	
	@Override
	public void useSkill(L2Character activeChar, L2Skill skill, L2Character... targets)
	{
		if (!(activeChar instanceof L2PcInstance))
			return;
		
		L2PcInstance player = activeChar.getActingPlayer();
		if (targets.length < 1 || !(targets[0] instanceof L2ZoneArtefactInstance))
			return;
		L2FactionZone zone = ((L2ZoneArtefactInstance) targets[0]).getFactionZone();
		if (zone == null || zone.isLocked())
			return;
		L2SkillGetEffect used = (L2SkillGetEffect) skill;
		L2Skill sk = SkillTable.getInstance().getInfo(used.getSkillId(), used.getSkillLevel());
		sk.getEffects(player, player);
	}
}
