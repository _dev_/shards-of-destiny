/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.handler.admin;

import java.util.StringTokenizer;

import javolution.util.FastMap;

import org.sod.manager.GlobalEventManager;
import org.sod.model.event.global.GlobalEvent;
import org.sod.model.event.global.Team;
import org.sod.model.event.global.impl.CaptureTheFlag;
import org.sod.model.event.global.impl.CaptureTheFlag.FlagTeam;
import org.sod.model.event.global.impl.Deathmatch;
import org.sod.model.event.global.impl.GolemDispute;
import org.sod.model.event.global.impl.GolemDispute.DisputeTeam;
import org.sod.model.event.global.impl.LastManStanding;
import org.sod.model.event.global.impl.LureTheGremlin;
import org.sod.model.event.global.impl.LureTheGremlin.LurerTeam;
import org.sod.model.event.global.impl.Regicide;
import org.sod.model.event.global.impl.Regicide.GuardianTeam;
import org.sod.model.event.global.impl.Regicide.ZergTeam;
import org.sod.model.event.global.impl.SlaughterHouse;
import org.sod.model.event.global.impl.TagTheMob;
import org.sod.model.event.global.impl.TeamDeathmatch;
import org.sod.model.event.global.impl.TeamKingOfTheHill;

import com.l2jfree.gameserver.handler.IAdminCommandHandler;
import com.l2jfree.gameserver.handler.admincommandhandlers.AdminHelpPage;
import com.l2jfree.gameserver.model.L2World;
import com.l2jfree.gameserver.model.Location;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.client.packets.sendable.NpcHtmlMessage;
import com.l2jfree.lang.L2TextBuilder;
import com.l2jfree.util.logging.L2Logger;

/**
 * This handler allows GMs to manage all GlobalEvent subclasses
 * manually and independently from the automated system.<BR>
 * Commands handled: <LI>admin_sod <LI>admin_evt <LI>admin_[EVENT_ABBREV] [STATE] [EVENT_ID] [BYPASS]
 * 
 * @author savormix
 * @since 2010.05.19
 */
public final class AdminEvents implements IAdminCommandHandler
{
	protected static final L2Logger _log = L2Logger.getLogger(AdminEvents.class);
	private static final String[] COMMANDS =
	{
			"admin_sod", "admin_evt", "admin_lms", "admin_ffa", "admin_tdm", "admin_tctf", "admin_koth", "admin_ltg",
			"admin_gdi", "admin_ttm", "admin_reg", "admin_slh"
	};
	public static final int SHOW = 0;
	public static final int SELECT = 1;
	public static final int CREATE = 2;
	public static final int ADD = 3;
	public static final int REMOVE = 4;
	public static final int START = 5;
	public static final int END = 6;
	
	@Override
	public String[] getAdminCommandList()
	{
		return COMMANDS;
	}
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance activeChar)
	{
		StringTokenizer st = new StringTokenizer(command, " ");
		command = st.nextToken();
		int state = SHOW;
		try
		{
			if (st.hasMoreTokens())
				state = Integer.parseInt(st.nextToken());
		}
		catch (Exception e)
		{
		}
		if (command.equals(COMMANDS[0]))
		{
			AdminHelpPage.showHelpPage(activeChar, "sod/menu.htm");
			return true;
		}
		else if (command.equals(COMMANDS[1]))
		{
			AdminHelpPage.showHelpPage(activeChar, "sod/events.htm");
			return true;
		}
		else
		// if (command.equals(COMMANDS[2]))
		{
			GlobalEvent ge;
			L2TextBuilder tb = new L2TextBuilder("<html><title>Shards of Destiny - Event Control Panel</title><body>");
			switch (state)
			{
			case SELECT:
				ge = testBypass(st, activeChar);
				if (ge == null)
					return false;
				injectEvent(tb, ge, command);
				break;
			case START:
				ge = testBypass(st, activeChar);
				if (ge == null)
					return false;
				else if (ge.isStarted())
				{
					activeChar.sendMessage("Event has already been started.");
					return false;
				}
				ge.onStart();
				injectEvent(tb, ge, command);
				break;
			case END:
				ge = testBypass(st, activeChar);
				if (ge == null)
					return false;
				else if (!ge.isStarted())
				{
					activeChar.sendMessage("Event is not started.");
					return false;
				}
				ge.onFinish();
				return useAdminCommand(command, activeChar);
			case ADD:
				ge = testBypass(st, activeChar);
				if (ge == null)
					return false;
				while (st.hasMoreTokens())
				{
					String name = st.nextToken();
					L2PcInstance player = L2World.getInstance().getPlayer(name);
					if (player != null)
					{
						if (ge.add(player))
						{
							activeChar.sendMessage("Added " + name + " to " + ge.getImplementationName());
							player.sendMessage("You were registered to " + ge.getImplementationName());
						}
						else
							activeChar.sendMessage("Could not add " + name + " to " + ge.getImplementationName());
					}
					else
						activeChar.sendMessage(name + " is offline.");
				}
				injectEvent(tb, ge, command);
				break;
			case REMOVE:
				ge = testBypass(st, activeChar);
				if (ge == null)
					return false;
				while (st.hasMoreTokens())
				{
					String name = st.nextToken();
					L2PcInstance player = L2World.getInstance().getPlayer(name);
					if (player != null)
					{
						if (ge.remove(player))
						{
							activeChar.sendMessage("Removed " + name + " to " + ge.getImplementationName());
							player.sendMessage("You were removed from " + ge.getImplementationName());
						}
						else
							activeChar.sendMessage("Could not remove " + name + " to " + ge.getImplementationName());
					}
					else
						activeChar.sendMessage(name + " is offline.");
				}
				injectEvent(tb, ge, command);
				break;
			case CREATE:
				int points = 50;
				try
				{
					points = Integer.parseInt(st.nextToken());
				}
				catch (Exception e)
				{
					activeChar.sendMessage("Be aware that you can specify max points in the text field.");
				}
				if (command.equals(COMMANDS[2]))
					ge = new LastManStanding(true);
				else if (command.equals(COMMANDS[3]))
					ge = new Deathmatch(points, true);
				else if (command.equals(COMMANDS[4]))
					ge = new TeamDeathmatch(points, true, new Team(-79729, 111361, -4895), new Team(-75268, 111975,
							-4895));
				else if (command.equals(COMMANDS[5]))
					ge = new CaptureTheFlag(points, true, new FlagTeam(new Location(-80801, 86531, -5155),
							new Location(-81358, 86528, -5155)), new FlagTeam(new Location(-76337, 87139, -5155),
							new Location(-75992, 87133, -5155)));
				else if (command.equals(COMMANDS[6]))
					ge = new TeamKingOfTheHill(points, true, new Team(-87185, -50905, -10730), new Team(-81950, -54335,
							-10730), 90000);
				else if (command.equals(COMMANDS[7]))
					ge = new LureTheGremlin(points, true, new LurerTeam(new Location(11660, -49145, -3005),
							LureTheGremlin.COLOR_RED, 90001, LureTheGremlin.CORNER_NPC_RED), new LurerTeam(
							new Location(12660, -49150, -3005), LureTheGremlin.COLOR_BLUE, 90002,
							LureTheGremlin.CORNER_NPC_BLUE));
				else if (command.equals(COMMANDS[8]))
					ge = new GolemDispute(points, true,
							new DisputeTeam(new Location(-106750, 239405, -3640), GolemDispute.COLOR_PURPLE,
									GolemDispute.GOLEM_PURPLE, new Location(-106890, 239575, -3635)), new DisputeTeam(
									new Location(-105190, 237860, -3615), GolemDispute.COLOR_ORANGE,
									GolemDispute.GOLEM_ORANGE, new Location(-105040, 237725, -3600)),
							GolemDispute.GOLDEN_GOLEM_NPC, new Location(-105980, 238660, -3680));
				else if (command.equals(COMMANDS[9]))
					ge = new TagTheMob(points, true);
				else if (command.equals(COMMANDS[10]))
					ge = new Regicide(true, new GuardianTeam(Regicide.KING_NPC, new Location(147445, 22750, -1990),
							Regicide.ADEN_TEMPLE_SPAWN), new ZergTeam(Regicide.ADEN_TOWN_SPAWN), Regicide.ADEN_TOWN,
							Regicide.ADEN_TEMPLE);
				else
					// if (command.equals(COMMANDS[11]))
					ge = new SlaughterHouse(true);
				injectEvent(tb, ge, command);
				break;
			default:
				tb.append("Currently available:<br><table width=250>");
				FastMap<Integer, GlobalEvent> all = GlobalEvent.getEventInstances();
				for (FastMap.Entry<Integer, GlobalEvent> entry = all.head(), end = all.tail(); (entry = entry.getNext()) != end;)
				{
					if ((command.equals(COMMANDS[2]) && !(entry.getValue() instanceof LastManStanding))
							|| (command.equals(COMMANDS[3]) && !(entry.getValue() instanceof Deathmatch))
							|| (command.equals(COMMANDS[4]) && !(entry.getValue() instanceof TeamDeathmatch))
							|| (command.equals(COMMANDS[5]) && !(entry.getValue() instanceof CaptureTheFlag))
							|| (command.equals(COMMANDS[6]) && !(entry.getValue() instanceof TeamKingOfTheHill))
							|| (command.equals(COMMANDS[7]) && !(entry.getValue() instanceof LureTheGremlin))
							|| (command.equals(COMMANDS[8]) && !(entry.getValue() instanceof GolemDispute))
							|| (command.equals(COMMANDS[9]) && !(entry.getValue() instanceof TagTheMob))
							|| (command.equals(COMMANDS[10]) && !(entry.getValue() instanceof Regicide))
							|| (command.equals(COMMANDS[11]) && !(entry.getValue() instanceof SlaughterHouse)))
						continue;
					if (GlobalEventManager.getInstance().getEvents().containsKey(entry.getKey()))
						continue;
					tb.append("<tr><td>");
					tb.append(entry.getKey());
					tb.append(". </td><td><a action=\"bypass -h ");
					tb.append(command);
					tb.append(' ');
					tb.append(SELECT);
					tb.append(' ');
					tb.append(entry.getKey());
					tb.append("\">");
					tb.append(entry.getValue().getImplementationName());
					tb.append("</a></td></tr>");
				}
				tb.append("</table><br><edit var=\"pts\" width=150 height=15><br1>");
				tb.append("<a action=\"bypass -h ");
				tb.append(command);
				tb.append(' ');
				tb.append(CREATE);
				tb.append(" $pts\">Create new</a>");
				tb.append("<br>Team details cannot be specified yet.");
				break;
			}
			tb.append("</body></html>");
			showChat(activeChar, tb.moveToString());
			return true;
		}
	}
	
	private final void injectEvent(L2TextBuilder tb, GlobalEvent ge, String cmd)
	{
		tb.append("Managing event: <font color=\"LEVEL\">");
		tb.append(ge.getEventId());
		tb.append("</font> (");
		tb.append(ge.getImplementationName());
		tb.append(")<br><table width=250><tr><td>Instance:</td><td>");
		tb.append(ge.getInstanceId());
		tb.append("</td></tr><tr><td>Max. Score:</td><td>");
		tb.append(ge.getMaxPoints());
		tb.append("</td></tr><tr><td>Started:</td><td>");
		boolean started = ge.isStarted();
		tb.append(started);
		tb.append("</td></tr><tr><td>Players:</td><td>");
		tb.append(ge.getAllPlayers().size());
		tb.append(" / ");
		tb.append(ge.getSplitLimit());
		tb.append("</td></tr><tr><td>Split ratio:</td><td>");
		tb.append(ge.getSplitRatio());
		tb.append("</td></tr></table><br><edit var=\"add\" width=250 height=60><br1>");
		tb.append("<a action=\"bypass -h ");
		tb.append(cmd);
		tb.append(' ');
		tb.append(ADD);
		tb.append(' ');
		tb.append(ge.getEventId());
		tb.append(" $add\">Add player(s)</a><br1>");
		tb.append("<a action=\"bypass -h ");
		tb.append(cmd);
		tb.append(' ');
		tb.append(REMOVE);
		tb.append(' ');
		tb.append(ge.getEventId());
		tb.append(" $add\">Remove player(s)</a><br>");
		if (started)
			tb.append("NO LINK HERE!!!<br><br>");
		tb.append("<a action=\"bypass -h ");
		tb.append(cmd);
		tb.append(' ');
		if (started)
			tb.append(END);
		else
			tb.append(START);
		tb.append(' ');
		tb.append(ge.getEventId());
		if (started)
			tb.append(" $add\">End event</a>");
		else
			tb.append(" $add\">Start event</a>");
	}
	
	private final GlobalEvent testBypass(StringTokenizer st, L2PcInstance activeChar)
	{
		int evt;
		try
		{
			evt = Integer.parseInt(st.nextToken());
		}
		catch (Exception e)
		{
			activeChar.sendMessage("Malformed builder bypass!");
			return null;
		}
		GlobalEvent ge = GlobalEvent.getEventInstances().get(evt);
		if (ge == null)
			activeChar.sendMessage("This event already finished and has been removed.");
		return ge;
	}
	
	private final void showChat(L2PcInstance gm, CharSequence html)
	{
		gm.sendPacket(new NpcHtmlMessage(html));
	}
}
