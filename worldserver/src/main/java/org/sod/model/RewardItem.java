/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model;

import com.l2jfree.gameserver.datatables.ItemTable;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;

/**
 * An item challenge reward.
 * @author savormix
 * @since 2010.12.07
 */
public class RewardItem extends Rewardable
{
	private final int _id;
	private final long _count;
	
	/**
	 * Constructs an item reward.
	 * @param id Item ID
	 * @param count Item count
	 */
	public RewardItem(int id, long count)
	{
		_id = id;
		_count = count;
	}
	
	public int getId()
	{
		return _id;
	}
	
	public long getCount()
	{
		return _count;
	}
	
	/* (non-Javadoc)
	 * @see org.sod.model.Rewardable#giveReward(com.l2jfree.gameserver.model.actor.instance.L2PcInstance, boolean)
	 */
	@Override
	public void giveReward(L2PcInstance player, boolean complementary)
	{
		if (!complementary)
			player.addItem("Challenge", getId(), getCount(), null, true);
	}
	
	/* (non-Javadoc)
	 * @see org.sod.model.Rewardable#validate()
	 */
	@Override
	public boolean validate()
	{
		if (getCount() <= 0 || ItemTable.getInstance().getTemplate(getId()) == null)
		{
			_log.warn("Invalid item reward! ID: " + getId() + " count: " + getCount(),
					new IllegalArgumentException());
			return false;
		}
		return true;
	}
}
