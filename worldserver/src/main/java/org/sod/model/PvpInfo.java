/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model;

import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;

/**
 * Stores the count of times a player has been killed and the last kill time.
 * @author savormix
 * @since 2010.03.07
 */
public final class PvpInfo
{
	/** The minimum time amount required between consecutive kills */
	public static final int DEFAULT_KILL_INTERVAL = 15;

	private volatile int _total;
	private volatile long _last;

	/** Creates a new object with kill count equal to 1 and last kill time equal to now */
	public PvpInfo()
	{
		update();
	}

	/** Increases the kill count and updates the last kill time */
	public void update()
	{
		_total++;
		_last = System.currentTimeMillis();
	}

	/** @return the amount of times a player has been killed */
	public int getTotal()
	{
		return _total;
	}

	/** @return the last kill time */
	public long getLast()
	{
		return _last;
	}

	/**
	 * @deprecated Awaiting removal.
	 */
	@Deprecated
	public double getPenalty()
	{
		if (getLast() + getAllowedKillInterval(null, null) < System.currentTimeMillis())
			return 0;
		else
			return 1;
	}

	/**
	 * @deprecated Not implemented.
	 */
	@Deprecated
	public static int getAllowedKillInterval(L2PcInstance killer, L2PcInstance victim)
	{
		// wasted enough of time designing gameplay
		return DEFAULT_KILL_INTERVAL * 1000;
	}
}
