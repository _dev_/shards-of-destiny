/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.event.global.impl;

import java.util.concurrent.atomic.AtomicInteger;

import javolution.util.FastMap;
import javolution.util.FastSet;

import org.sod.model.ChallengeTemplate;
import org.sod.manager.GlobalEventManager;
import org.sod.model.L2PlayerData;
import org.sod.model.event.TeamVersusTeam;
import org.sod.model.event.global.EventProcedure;
import org.sod.model.event.global.GlobalEvent;
import org.sod.model.event.global.SplitProcedure;
import org.sod.model.event.global.Team;
import org.sod.model.event.global.TeamRewardProcedure;
import org.sod.model.event.global.TeamedEvent;
import org.sod.model.event.global.restriction.TeamDeathmatchRestriction;

import com.l2jfree.Config;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.base.ClassId;
import com.l2jfree.gameserver.network.client.packets.sendable.PlaySoundPacket.PlaySound;
import com.l2jfree.gameserver.network.client.packets.sendable.PlaySoundPacket.SoundType;

/**
 * Team Deathmatch a.k.a. Team Versus Team (TvT)
 * 
 * @author savormix
 * @since 2010.03.29
 */
public class TeamDeathmatch extends TeamedEvent<Team>
{
	private static final String NAME = "Team Deathmatch";
	static
	{
		TeamDeathmatchRestriction.getInstance().activate();
	}
	@SuppressWarnings("unchecked")
	private static final EventProcedure<TeamedEvent<Team>>[] START = new EventProcedure[]
	{
			PROC_LEAVE_PARTY, PROC_COLOR, PROC_MOVE_TO_TEAM, PROC_RECOVERY
	};
	@SuppressWarnings("unchecked")
	protected static final EventProcedure<TeamedEvent<Team>>[] MAINTENANCE = new EventProcedure[]
	{
		new EventProcedure<TeamedEvent<Team>>()
		{
			@Override
			public void performOn(L2PcInstance member, TeamedEvent<Team> event)
			{
				if (member.isDead())
				{
					PROC_ADD_PENDING_REVIVE.performOn(member, event);
					PROC_MOVE_TO_TEAM.performOn(member, event);
					PROC_RECOVERY.performOn(member, event);
				}
			}
		}
	};
	@SuppressWarnings("unchecked")
	private static final EventProcedure<TeamDeathmatch>[] END = new EventProcedure[]
	{
			PROC_LEAVE_PARTY, PROC_WASH_COLOR, PROC_RETURN, new EventProcedure<TeamDeathmatch>()
			{
				@Override
				public void performOn(L2PcInstance member, TeamDeathmatch event)
				{
					member.getSoDPlayer().sendLocalizedMessage("EVENT_YOU_MADE_KILLS", "%kills%",
							String.valueOf(event.getFragCount(member.getObjectId()).get()));
				}
			}, PROC_RECOVERY
	};
	private/* static */final SplitProcedure[] SPLIT = new SplitProcedure[]
	{
			PROC_UNSAFE_TRANSFER, PROC_SPLIT_NEW_BALANCED
	};
	
	private final FastMap<Integer, AtomicInteger> _frags;
	private L2PcInstance _best;
	
	/**
	 * Creates a Teamed DM event.
	 * 
	 * @param maxPoints
	 *            point limit
	 * @param autoRemove
	 *            mark as unused after end
	 * @param t1
	 *            Team 1
	 * @param t2
	 *            Team 2
	 */
	public TeamDeathmatch(int maxPoints, boolean autoRemove, Team t1, Team t2)
	{
		super(15000, 150, 2. / 3, maxPoints, -1, autoRemove, t1, t2);
		_frags = new FastMap<Integer, AtomicInteger>().setShared(true);
	}
	
	protected TeamDeathmatch(TeamDeathmatch main, Team t1, Team t2)
	{
		super(main, t1, t2);
		_frags = null;
	}
	
	public final void addFrag(Integer killerId)
	{
		if (getMainEvent() == null)
		{
			AtomicInteger pts = getFragCount(killerId);
			isOver(pts.incrementAndGet());
		}
		else
			getMainEvent().addFrag(killerId);
	}
	
	public final AtomicInteger getFragCount(Integer playerId)
	{
		if (getMainEvent() != null)
			return getMainEvent().getFragCount(playerId);
		
		AtomicInteger i = _frags.get(playerId);
		if (i == null)
		{
			i = new AtomicInteger();
			AtomicInteger old = _frags.putIfAbsent(playerId, i);
			if (old != null)
				return old;
		}
		return i;
	}
	
	@Override
	public TeamDeathmatch getMainEvent()
	{
		return (TeamDeathmatch) super.getMainEvent();
	}
	
	@Override
	protected double getCoef(Team team)
	{
		double result = 0;
		for (FastSet.Record r = team.getMembers().head(), end = team.getMembers().tail(); (r = r.getNext()) != end;)
		{
			L2PcInstance player = team.getMembers().valueOf(r);
			double temp = player.getLevel() - Config.STARTING_LEVEL + 1;
			ClassId prof = player.getClassId();
			if (prof.isSummoner())
				temp *= TeamVersusTeam.SUMMONER_LEVEL_MULTIPLIER;
			else if (prof.equalsOrChildOf(ClassId.AbyssWalker) || prof.equalsOrChildOf(ClassId.Plainswalker)
					|| prof.equalsOrChildOf(ClassId.TreasureHunter))
				temp *= TeamVersusTeam.DAGGER_LEVEL_MULTIPLIER;
			else if (prof.equalsOrChildOf(ClassId.Tyrant))
				temp *= TeamVersusTeam.TYRANT_LEVEL_MULTIPLIER;
			else if (prof.equalsOrChildOf(ClassId.Arbalester) || prof.equalsOrChildOf(ClassId.Hawkeye)
					|| prof.equalsOrChildOf(ClassId.PhantomRanger) || prof.equalsOrChildOf(ClassId.SilverRanger))
				temp *= TeamVersusTeam.ARCHER_LEVEL_MULTIPLIER;
			else if (prof.equalsOrChildOf(ClassId.Bishop) || prof.equalsOrChildOf(ClassId.ElvenElder)
					|| prof.equalsOrChildOf(ClassId.ShillienElder))
				temp *= TeamVersusTeam.HEALER_LEVEL_MULTIPLIER;
			else if (prof.isMage())
				temp *= TeamVersusTeam.MAGE_LEVEL_MULTIPLIER;
			result += temp;
		}
		return result;
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getEndingProcedure()
	{
		return END;
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getMaintenanceProcedure()
	{
		return MAINTENANCE;
	}
	
	@Override
	public String getImplementationName()
	{
		return NAME;
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getStartProcedure()
	{
		return START;
	}
	
	@Override
	protected void doBeforeEnd()
	{
		super.doBeforeEnd();
		FastSet<L2PcInstance> members;
		if (getWinner() == null)
			members = getAllPlayers();
		else
			members = getWinner().getMembers();
		int max = -1;
		for (FastSet.Record r = members.head(), end = members.tail(); (r = r.getNext()) != end;)
		{
			L2PcInstance best = members.valueOf(r);
			int i = getFragCount(best.getObjectId()).get();
			if (i > max)
			{
				max = i;
				_best = best;
			}
		}
	}
	
	@Override
	public void doAfterEnd()
	{
		final FastSet<Integer> winners = FastSet.newInstance();
		broadcastTeamScores(new TeamRewardProcedure<Team>()
		{
			@Override
			public void rewardMember(L2PcInstance member, Team t)
			{
				if (t == getWinner() && member != _best && getFragCount(member.getObjectId()).get() > 0)
				{
					rewardAdena(member, 25);
					rewardChallenge(member.getSoDPlayer(), ChallengeTemplate.COOPERATION, 1);
					winners.add(member.getObjectId());
				}
			}
		});
		if (_frags != null)
			_frags.clear();
		if (_best != null)
		{
			rewardItem(_best, EVENT_MEDAL, 5);
			rewardAdena(_best, 30);
			_best.sendPacket(new PlaySound(SoundType.MUSIC, "Rm01_S"));
			L2PlayerData dat = _best.getSoDPlayer();
			rewardChallenge(dat, ChallengeTemplate.COOPERATION, 1);
			dat.sendLocalizedMessage("EVENT_YOU_WERE_BEST");
			winners.add(_best.getObjectId());
			_best = null;
		}
		if (!GlobalEventManager.getInstance().setWinners(winners, this))
			FastSet.recycle(winners);
		
		super.doAfterEnd();
	}
	
	@Override
	public TeamedEvent<Team> newSplitEvent()
	{
		Team t1 = getTeam1();
		Team t2 = getTeam2();
		return new TeamDeathmatch(this, new Team(t1.getColor(), t1.getLoc()), new Team(t2.getColor(), t2.getLoc()));
	}
	
	@Override
	public SplitProcedure[] getSplitProcedure()
	{
		return SPLIT;
	}
}
