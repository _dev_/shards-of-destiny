/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.event.global.restriction;

import javolution.util.FastSet;

import org.sod.model.ChallengeTemplate;
import org.sod.model.event.global.GlobalEvent;
import org.sod.model.event.global.TeamedEvent;
import org.sod.model.event.global.impl.CaptureTheFlag;
import org.sod.model.event.global.impl.CaptureTheFlag.FlagTeam;

import com.l2jfree.gameserver.datatables.ItemTable;
import com.l2jfree.gameserver.handler.IItemHandler;
import com.l2jfree.gameserver.model.L2ItemInstance;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.L2Playable;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.actor.instance.L2PetInstance;
import com.l2jfree.gameserver.model.itemcontainer.Inventory;
import com.l2jfree.gameserver.model.itemcontainer.PcInventory;
import com.l2jfree.gameserver.model.restriction.global.GlobalRestrictions.CombatState;
import com.l2jfree.gameserver.model.restriction.global.RestrictionPriority;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.Chat;
import com.l2jfree.gameserver.network.client.packets.sendable.PlaySoundPacket.PlaySound;
import com.l2jfree.gameserver.network.client.packets.sendable.PlaySoundPacket.SoundType;
import com.l2jfree.gameserver.network.client.packets.sendable.UserInfo.MyPlayerInfo;

/**
 * TODO Auto-generated JavaDoc
 * 
 * @author savormix
 * @since 2010.04.01
 */
public class CaptureTheFlagRestriction extends GlobalEventRestriction<CaptureTheFlag>
{
	private final FastSet<L2Npc> _flagNPCs;
	private final FastSet<L2ItemInstance> _flagItems;
	
	private CaptureTheFlagRestriction()
	{
		_flagNPCs = FastSet.newInstance();
		_flagItems = FastSet.newInstance();
	}
	
	public final void addFlag(L2Npc flag)
	{
		_flagNPCs.add(flag);
	}
	
	public final void removeFlag(L2Npc flag)
	{
		_flagNPCs.remove(flag);
	}
	
	public final void addFlag(L2ItemInstance flag)
	{
		if (flag != null)
			_flagItems.add(flag);
	}
	
	public final void removeFlag(L2ItemInstance flag)
	{
		if (flag != null)
			_flagItems.remove(flag);
	}
	
	@Override
	public Class<CaptureTheFlag> getEventClass()
	{
		return CaptureTheFlag.class;
	}
	
	@Override
	public boolean canInviteToParty(L2PcInstance activeChar, L2PcInstance target)
	{
		GlobalEvent evt1 = GlobalEvent.getEvent(activeChar, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target, getEventClass());
		if (evt1 == null && evt2 == null)
			return true;
		else
			return getCombatState(activeChar, target) == CombatState.FRIEND;
	}
	
	@Override
	public boolean canPickUp(L2PcInstance activeChar, L2ItemInstance item, L2PetInstance pet)
	{
		if (!_flagItems.contains(item))
			return true;
		CaptureTheFlag evt = GlobalEvent.getEvent(activeChar, getEventClass());
		if (evt == null || pet != null)
			return false;
		FlagTeam self = evt.getTeam(activeChar);
		if (item == self.getFlagItem())
			return true;
		FlagTeam enemy = evt.getEnemyTeam(activeChar);
		return item == enemy.getFlagItem();
	}
	
	@Override
	public boolean canRequestRevive(L2PcInstance activeChar)
	{
		if (GlobalEvent.getEvent(activeChar, getEventClass()) != null)
			return false;
		else
			return true;
	}
	
	@Override
	public boolean canTeleport(L2PcInstance activeChar)
	{
		return canRequestRevive(activeChar);
	}
	
	@Override
	public boolean canUseItemHandler(Class<? extends IItemHandler> clazz, int itemId, L2Playable activeChar,
			L2ItemInstance item, L2PcInstance player)
	{
		if (itemId == CaptureTheFlag.COMBAT_FLAG_ITEM)
			return false;
		else if (player != null
				&& item != null
				&& item.isWeapon()
				&& player.getInventory().getPaperdollItemId(PcInventory.PAPERDOLL_RHAND) == CaptureTheFlag.COMBAT_FLAG_ITEM)
			return false;
		else
			return true;
	}
	
	@Override
	public boolean canDestroyItem(L2PcInstance activeChar, int itemId, L2ItemInstance item)
	{
		if (itemId == CaptureTheFlag.COMBAT_FLAG_ITEM && GlobalEvent.getEvent(activeChar, getEventClass()) != null)
		{
			activeChar.getSoDPlayer().sendLocalizedMessage("EVENT_CTF_CANNOT_DESTROY_FLAG");
			activeChar.doDie(null);
		}
		// return false;
		// else
		return true;
	}
	
	@Override
	public CombatState getCombatState(L2PcInstance activeChar, L2PcInstance target)
	{
		TeamedEvent<?> evt1 = GlobalEvent.getEvent(activeChar, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target, getEventClass());
		if (evt1 != null && evt2 != null && evt1 == evt2)
		{
			if (evt1.isSameTeam(activeChar, target))
				return CombatState.FRIEND;
			else
				return CombatState.ENEMY;
		}
		
		return CombatState.NEUTRAL;
	}
	
	@Override
	public boolean isProtected(L2Character activeChar, L2Character target, L2Skill skill, boolean sendMessage,
			L2PcInstance attacker_, L2PcInstance target_, boolean isOffensive)
	{
		if (attacker_ == null || attacker_ == target_)
			return false;
		
		TeamedEvent<?> evt1 = GlobalEvent.getEvent(attacker_, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target_, getEventClass());
		if (evt1 != null || evt2 != null)
		{
			if (evt1 != evt2)
				return true;
			TeamedEvent<?> evt = evt1;
			if (evt.isSameTeam(attacker_, target_))
				return isOffensive;
			else
				return !isOffensive;
		}
		return false;
	}
	
	@Override
	@RestrictionPriority(RestrictionPriority.DEFAULT_PRIORITY + 0.1)
	public void playerDisconnected(L2PcInstance activeChar)
	{
		dropFlag(activeChar, null, GlobalEvent.getEvent(activeChar, getEventClass()));
	}
	
	@Override
	public boolean playerDied(L2PcInstance activeChar)
	{
		return playerKilled(null, activeChar, null);
	}
	
	@Override
	public boolean playerKilled(L2Character activeChar, L2PcInstance target, L2PcInstance killer)
	{
		CaptureTheFlag evt1 = GlobalEvent.getEvent(target, getEventClass());
		if (evt1 != null)
		{
			GlobalEvent evt2 = GlobalEvent.getEvent(killer, getEventClass());
			if (evt1 == evt2 && target != killer)
			{
				// evt1.broadcastToParticipants(new CreatureSay(0, SystemChatChannelId.Chat_Alliance,
				// evt1.getName(), killer.getName() + " has killed " + target.getName()));
				evt1.getScore(killer.getObjectId()).incKilled();
			}
			dropFlag(target, killer, evt1);
		}
		return false;
	}
	
	@Override
	public boolean onAction(L2Npc npc, L2PcInstance activeChar)
	{
		if (!_flagNPCs.contains(npc))
			return false;
		
		CaptureTheFlag evt = GlobalEvent.getEvent(activeChar, getEventClass());
		if (evt != null)
		{
			int no = evt.getTeamNo(activeChar);
			FlagTeam own = evt.getTeamByNo(no);
			FlagTeam enemy = evt.getEnemyTeam(activeChar);
			if (npc.getSpawn().equals(own.getFlagSpawn()) && enemy.getFlagItem() != null)
			{
				PcInventory inv = activeChar.getInventory();
				L2ItemInstance wpn = inv.getPaperdollItem(Inventory.PAPERDOLL_RHAND);
				if (wpn != null && wpn.getItemId() == CaptureTheFlag.COMBAT_FLAG_ITEM)
				{
					activeChar.useEquippableItem(wpn, true);
					if (activeChar.destroyItemByItemId(evt.getImplementationName(), CaptureTheFlag.COMBAT_FLAG_ITEM, 1,
							npc, true))
					{
						enemy.setFlagItem(null);
						// activeChar.stopSpecialEffect(SpecialEffect.S_AIR_ROOT);
						
						evt.broadcastMessageToTeam(own, Chat.PRIVATE, "EVENT_CTF_CAPTURED_ENEMY_FLAG", "%name%",
								activeChar.getName());
						evt.broadcastMessageToTeam(enemy, Chat.PRIVATE, "EVENT_CTF_CAPTURED_YOUR_FLAG", "%name%",
								activeChar.getName());
						CaptureTheFlag.broadcastToTeam(own, new PlaySound(SoundType.MUSIC, "BS11_D"));
						CaptureTheFlag.broadcastToTeam(enemy, new PlaySound(SoundType.MUSIC, "BS03_A"));
						
						enemy.spawnFlag(evt.getInstanceId());
						evt.addCapture(activeChar.getObjectId());
						evt.rewardChallenge(activeChar.getSoDPlayer(), ChallengeTemplate.FLAG_BEARER, 1);
						FlagTeam t;
						if (evt.getMainEvent() != null)
							t = evt.getMainEvent().getTeamByNo(no);
						else
							t = evt.getTeamByNo(no);
						evt.isOver(t.addPoint());
					}
				}
			}
			else if (npc.getSpawn().equals(enemy.getFlagSpawn()) && enemy.getFlagItem() == null)
			{
				enemy.unspawnFlag();
				L2ItemInstance flag = ItemTable.getInstance().createItem(evt.getImplementationName(),
						CaptureTheFlag.COMBAT_FLAG_ITEM, 1, activeChar);
				equipFlag(activeChar, flag, evt);
			}
		}
		return true;
	}
	
	public final void equipFlag(L2PcInstance activeChar, L2ItemInstance flag, CaptureTheFlag event)
	{
		FlagTeam enemy = event.getEnemyTeam(activeChar);
		enemy.setFlagItem(flag);
		activeChar.addItem(event.getImplementationName(), flag, null, true);
		activeChar.useEquippableItem(flag, true);
		activeChar.sendPacket(MyPlayerInfo.PACKET);
		// activeChar.startSpecialEffect(SpecialEffect.S_AIR_ROOT);
		
		event.broadcastMessageToTeam(event.getTeam(activeChar), Chat.PRIVATE, "EVENT_CTF_TOOK_ENEMY_FLAG", "%name%",
				activeChar.getName());
		event.broadcastMessageToTeam(enemy, Chat.PRIVATE, "EVENT_CTF_TOOK_YOUR_FLAG", "%name%", activeChar.getName());
	}
	
	private void dropFlag(L2PcInstance activeChar, L2PcInstance killer, CaptureTheFlag event)
	{
		L2ItemInstance wpn = activeChar.getInventory().getPaperdollItem(Inventory.PAPERDOLL_RHAND);
		if (wpn != null && wpn.getItemId() == CaptureTheFlag.COMBAT_FLAG_ITEM)
		{
			activeChar.useEquippableItem(wpn, true);
			activeChar.sendPacket(MyPlayerInfo.PACKET);
			if (activeChar.dropItem("Capture the Flag", wpn, killer, true))
			{
				if (event != null)
				{
					event.broadcastMessageToParticipants(Chat.ALLIANCE, "EVENT_CTF_DROPPED_THE_FLAG", "%name%",
							activeChar.getName());
					event.broadcastToParticipants(new PlaySound(SoundType.MUSIC, "BS04_D"));
					if (killer != null)
						event.rewardChallenge(killer.getSoDPlayer(), ChallengeTemplate.DEMOTIVATOR, 1);
				}
				// activeChar.stopSpecialEffect(SpecialEffect.S_AIR_ROOT);
			}
		}
	}
	
	public static final CaptureTheFlagRestriction getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static final class SingletonHolder
	{
		private static final CaptureTheFlagRestriction INSTANCE = new CaptureTheFlagRestriction();
	}
}
