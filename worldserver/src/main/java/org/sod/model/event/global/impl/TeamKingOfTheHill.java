/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.event.global.impl;

import org.sod.model.event.global.Team;
import org.sod.model.event.global.TeamedEvent;

import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.zone.L2Zone;

/**
 * Team King of the Hill event.
 * 
 * @author savormix
 * @since 2010.03.30
 */
public class TeamKingOfTheHill extends TeamDeathmatch
{
	private static final String NAME = "Team King of the Hill";
	
	private final L2Zone _zone;
	
	/**
	 * Creates a teamed KoTH event
	 * 
	 * @param maxPoints
	 *            point limit
	 * @param autoRemove
	 *            mark as unused after end
	 * @param t1
	 *            Team 1
	 * @param t2
	 *            Team 2
	 * @param uniqueZoneId
	 *            The hill zone
	 */
	public TeamKingOfTheHill(int maxPoints, boolean autoRemove, Team t1, Team t2, int uniqueZoneId)
	{
		super(maxPoints, autoRemove, t1, t2);
		_zone = registerEventZone(uniqueZoneId);
	}
	
	protected TeamKingOfTheHill(TeamKingOfTheHill main, Team t1, Team t2)
	{
		super(main, t1, t2);
		_zone = registerEventZone(main.getZoneId());
	}
	
	public int getZoneId()
	{
		if (_zone == null)
			return 0;
		else
			return _zone.getId();
	}
	
	@Override
	public String getImplementationName()
	{
		return NAME;
	}
	
	@Override
	protected void doBeforeMaintenance()
	{
		super.doBeforeMaintenance();
		
		for (L2Character cha : _zone.getCharactersInsideActivated())
		{
			L2PcInstance player = cha.getActingPlayer();
			int no = getTeamNo(player);
			if (no == 0)
				return;
			Team t;
			if (getMainEvent() != null)
				t = getMainEvent().getTeamByNo(no);
			else
				t = getTeamByNo(no);
			player.getSoDPlayer().sendLocalizedMessage("EVENT_TKOTH_SCORED");
			isOver(t.addPoint());
		}
	}
	
	@Override
	public TeamedEvent<Team> newSplitEvent()
	{
		Team t1 = getTeam1();
		Team t2 = getTeam2();
		return new TeamKingOfTheHill(this, new Team(t1.getColor(), t1.getLoc()), new Team(t2.getColor(), t2.getLoc()));
	}
}
