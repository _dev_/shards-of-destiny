/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.event.global;

import org.sod.i18n.Internationalization;

import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.Chat;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.ChatMessage;

/**
 * TODO Auto-generated JavaDoc
 * 
 * @author savormix
 * @since 2010.12.10
 */
public class ProcInfoMessage implements EventProcedure<GlobalEvent>
{
	private final String _key;
	
	public ProcInfoMessage(String key)
	{
		_key = key;
	}
	
	@Override
	public void performOn(L2PcInstance member, GlobalEvent event)
	{
		ChatMessage cs = new ChatMessage(Chat.PRIVATE, event.getImplementationName(), Internationalization
				.getInstance().get(member.getSoDPlayer().getSettings().getLocale(), _key));
		member.sendPacket(cs);
	}
}
