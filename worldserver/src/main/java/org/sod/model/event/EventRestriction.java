/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.event;

import java.util.Calendar;

import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.restriction.global.AbstractRestriction;
import com.l2jfree.gameserver.model.restriction.global.GlobalRestriction;
import com.l2jfree.gameserver.model.restriction.global.GlobalRestrictions;
import com.l2jfree.gameserver.model.restriction.global.GlobalRestrictions.CombatState;
import com.l2jfree.gameserver.taskmanager.AttackStanceTaskManager;

/**
 * TODO Auto-generated JavaDoc
 * 
 * @author savormix
 * @since 2010.03.26
 */
public class EventRestriction extends AbstractRestriction
{
	private EventRestriction()
	{
	}
	
	@Override
	public boolean isRestricted(L2PcInstance activeChar, Class<? extends GlobalRestriction> callingRestriction)
	{
		if (callingRestriction == getClass())
			return false;
		
		if (TeamVersusTeam.getInstance().isPlaying(activeChar))
		{
			activeChar.getSoDPlayer().sendLocalizedMessage("EVENT_OPEN_TVT_ALREADY_PARTICIPATING");
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean canInviteToParty(L2PcInstance activeChar, L2PcInstance target)
	{
		if (TeamVersusTeam.getInstance().isPlaying(activeChar, target))
			return getCombatState(activeChar, target) == CombatState.FRIEND;
		return true;
	}
	
	@Override
	public boolean canRequestRevive(L2PcInstance activeChar)
	{
		return !TeamVersusTeam.getInstance().isPlaying(activeChar);
	}
	
	@Override
	public boolean canTeleport(L2PcInstance activeChar)
	{
		return canRequestRevive(activeChar);
	}
	
	@Override
	public void playerDisconnected(L2PcInstance activeChar)
	{
		// not a global event, so needed
		TeamVersusTeam.getInstance().remove(activeChar);
	}
	
	@Override
	public CombatState getCombatState(L2PcInstance activeChar, L2PcInstance target)
	{
		if (TeamVersusTeam.getInstance().isPlaying(activeChar, target))
		{
			if (TeamVersusTeam.getInstance().isSameTeam(activeChar, target))
				return CombatState.FRIEND;
			else
				return CombatState.ENEMY;
		}
		
		return CombatState.NEUTRAL;
	}
	
	@Override
	public boolean isProtected(L2Character activeChar, L2Character target, L2Skill skill, boolean sendMessage,
			L2PcInstance attacker_, L2PcInstance target_, boolean isOffensive)
	{
		if (attacker_ == null || attacker_ == target_)
			return false;
		
		boolean att = TeamVersusTeam.getInstance().isPlaying(attacker_);
		boolean trg = TeamVersusTeam.getInstance().isPlaying(target_);
		if (att || trg)
		{
			if (att == trg)
			{
				if (getCombatState(attacker_, target_) == CombatState.FRIEND)
					return isOffensive;
				else
					return !isOffensive;
			}
			else
				return true;
		}
		return false;
	}
	
	@Override
	public boolean useVoicedCommand(String command, L2PcInstance activeChar, String target)
	{
		if (command.equals("jointvt"))
		{
			if (GlobalRestrictions.isRestricted(activeChar, getClass()))
				return false;
			
			if (AttackStanceTaskManager.getInstance().getAttackStanceTask(activeChar))
			{
				activeChar.getSoDPlayer().sendLocalizedMessage("EVENT_OPEN_TVT_NOT_IN_COMBAT");
				return false;
			}
			int weekday = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
			if (weekday != Calendar.SATURDAY && weekday != Calendar.SUNDAY)
			{
				activeChar.getSoDPlayer().sendLocalizedMessage("EVENT_OPEN_TVT_CONDITION");
				return false;
			}
			TeamVersusTeam.getInstance().add(activeChar);
			return true;
		}
		else if (command.equals("leavetvt"))
		{
			playerDisconnected(activeChar);
			return true;
		}
		return false;
	}
	
	public static final EventRestriction getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static final class SingletonHolder
	{
		private static final EventRestriction INSTANCE = new EventRestriction();
	}
}
