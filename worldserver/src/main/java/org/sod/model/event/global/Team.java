/*
 * MISSING LICENSING INFO
 */
package org.sod.model.event.global;

import java.util.concurrent.atomic.AtomicInteger;

import javolution.util.FastSet;

import org.sod.Calc;

import com.l2jfree.gameserver.model.Location;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;

/**
 * @author savormix
 * 
 */
public class Team
{
	protected final AtomicInteger _points;
	private final Location[] _loc;
	private final FastSet<L2PcInstance> _members;
	private final boolean _randColor;
	private int _color;
	private volatile double _power;
	
	public Team(int color, Location... loc)
	{
		_points = new AtomicInteger();
		_loc = loc;
		_members = FastSet.newInstance();
		_randColor = (color == -1);
		setColor(color);
	}
	
	public Team(Location... loc)
	{
		this(-1, loc);
	}
	
	public Team(int x, int y, int z)
	{
		this(new Location(x, y, z));
	}
	
	/** @return team's spawn locations */
	public final Location[] getLoc()
	{
		return _loc;
	}
	
	/** @return team's spawn location */
	public final Location getLocToSpawn()
	{
		return Calc.getRandomFromArray(getLoc());
	}
	
	/** @return team's members */
	public final FastSet<L2PcInstance> getMembers()
	{
		return _members;
	}
	
	public final boolean isRandColor()
	{
		return _randColor;
	}
	
	/** @return team's color */
	public final int getColor()
	{
		return _color;
	}
	
	public final int getRed()
	{
		return (getColor() >> 16) & 0xFF;
	}
	
	public final int getGreen()
	{
		return (getColor() >> 8) & 0xFF;
	}
	
	public final int getBlue()
	{
		return getColor() & 0xFF;
	}
	
	/**
	 * Set a new name color for this team.
	 * 
	 * @param color
	 *            New color
	 */
	public final void setColor(int color)
	{
		_color = color;
	}
	
	/**
	 * Set a new name color for this team by giving
	 * explicit RGB values.
	 * 
	 * @param r
	 *            Red
	 * @param g
	 *            Green
	 * @param b
	 *            Blue
	 */
	public final void setColor(int r, int g, int b)
	{
		setColor(r << 16 + g << 8 + b);
	}
	
	/** @return team's power coefficient */
	public final double getPower()
	{
		return _power;
	}
	
	/**
	 * Update the team's strength.
	 * 
	 * @param power
	 *            the newly calculated power
	 */
	public final void setPower(double power)
	{
		_power = power;
	}
	
	/** @return team's score */
	public final int getPoints()
	{
		return _points.get();
	}
	
	/**
	 * Add a point to team's score.
	 * 
	 * @param limit
	 *            point limit
	 * @return true if points after addition >= limit
	 */
	public final int addPoint()
	{
		return _points.incrementAndGet();
	}
	
	/** Reset team's score */
	public final void resetPoints()
	{
		_points.set(0);
	}
}
