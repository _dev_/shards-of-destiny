/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.event.global.impl;

import java.util.concurrent.atomic.AtomicInteger;

import javolution.util.FastMap;
import javolution.util.FastSet;

import org.sod.model.ChallengeTemplate;
import org.sod.model.L2PlayerData;
import org.sod.model.event.global.EventProcedure;
import org.sod.model.event.global.GlobalEvent;
import org.sod.model.event.global.SplitProcedure;
import org.sod.model.event.global.restriction.DeathmatchRestriction;

import com.l2jfree.gameserver.model.Location;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.client.packets.sendable.PlaySoundPacket.PlaySound;
import com.l2jfree.gameserver.network.client.packets.sendable.PlaySoundPacket.SoundType;
import com.l2jfree.util.Rnd;

/**
 * A simple deathmatch (FFA style).
 * 
 * @author savormix
 * @since 2010.03.29
 */
@SuppressWarnings("unchecked")
public class Deathmatch extends GlobalEvent
{
	private static final String NAME = "Deathmatch (Free For All)";
	private static final Location[] SPAWNS = new Location[]
	{
			new Location(17720, 114205, -11670), new Location(16710, 115770, -11845),
			new Location(18750, 115770, -11845), new Location(15145, 117870, -12090),
			new Location(13965, 116710, -12085), new Location(14955, 118940, -12085),
			new Location(13660, 120540, -12085), new Location(11535, 119950, -12085),
			new Location(11535, 117325, -12085), new Location(16105, 113130, -11980),
			new Location(16100, 115165, -11980), new Location(16360, 114155, -11850),
			new Location(13990, 111660, -12085), new Location(15150, 110485, -12085),
			new Location(14300, 108220, -12085), new Location(11665, 109640, -12085),
			new Location(11740, 108220, -12085), new Location(11735, 111105, -12085),
			new Location(18700, 112525, -11975), new Location(16735, 112525, -11975),
			new Location(17710, 112780, -11975), new Location(17710, 107925, -11845),
			new Location(21455, 111560, -12085), new Location(20295, 110420, -12085),
			new Location(20875, 108370, -12085), new Location(22345, 111558, -12085),
			new Location(23770, 108420, -12085), new Location(23725, 110975, -12085),
			new Location(19360, 113210, -11965), new Location(19345, 115045, -11960),
			new Location(19035, 114150, -11910), new Location(20300, 117885, -12085),
			new Location(21435, 116725, -12085), new Location(20635, 119935, -12085),
			new Location(23735, 117380, -12085), new Location(23735, 120000, -12085),
	};
	static
	{
		DeathmatchRestriction.getInstance().activate();
	}
	private static final EventProcedure<GlobalEvent>[] START = new EventProcedure[]
	{
			PROC_LEAVE_PARTY, new EventProcedure<GlobalEvent>()
			{
				@Override
				public void performOn(L2PcInstance member, GlobalEvent event)
				{
					Location loc = SPAWNS[Rnd.get(SPAWNS.length)];
					member.teleToLocation(loc, true);
				}
			}, PROC_RECOVERY
	};
	private static final EventProcedure<GlobalEvent>[] MAINTENANCE = new EventProcedure[]
	{
		new EventProcedure<GlobalEvent>()
		{
			@Override
			public void performOn(L2PcInstance member, GlobalEvent event)
			{
				if (member.isDead())
				{
					PROC_ADD_PENDING_REVIVE.performOn(member, event);
					START[1].performOn(member, event);
					PROC_RECOVERY.performOn(member, event);
				}
			}
		}
	};
	private static final EventProcedure<Deathmatch>[] END = new EventProcedure[]
	{
			PROC_LEAVE_PARTY, PROC_RETURN, new EventProcedure<Deathmatch>()
			{
				@Override
				public void performOn(L2PcInstance member, Deathmatch event)
				{
					member.getSoDPlayer().sendLocalizedMessage("EVENT_YOU_MADE_KILLS", "%kills%",
							String.valueOf(event.getFragCount(member.getObjectId()).get()));
				}
			}, PROC_RECOVERY
	};
	private/*static*/final SplitProcedure[] SPLIT = new SplitProcedure[]
	{
		PROC_UNSAFE_TRANSFER
	};
	
	private final FastMap<Integer, AtomicInteger> _frags;
	private L2PcInstance _best;
	private L2PcInstance _second;
	
	/**
	 * Creates a FFA DM event.
	 * 
	 * @param maxPoints
	 *            point limit
	 * @param autoRemove
	 *            mark as unused after end
	 */
	public Deathmatch(int maxPoints, boolean autoRemove)
	{
		super(2500, 80, 0.75, maxPoints, -1, autoRemove);
		_frags = new FastMap<Integer, AtomicInteger>().setShared(true);
	}
	
	protected Deathmatch(Deathmatch main)
	{
		super(main);
		_frags = null;
	}
	
	public final void addFrag(Integer killerId)
	{
		if (getMainEvent() == null)
		{
			AtomicInteger pts = getFragCount(killerId);
			isOver(pts.incrementAndGet());
		}
		else
			getMainEvent().addFrag(killerId);
	}
	
	public final AtomicInteger getFragCount(Integer playerId)
	{
		if (getMainEvent() != null)
			return getMainEvent().getFragCount(playerId);
		
		AtomicInteger i = _frags.get(playerId);
		if (i == null)
		{
			i = new AtomicInteger();
			AtomicInteger old = _frags.putIfAbsent(playerId, i);
			if (old != null)
				return old;
		}
		return i;
	}
	
	@Override
	public Deathmatch getMainEvent()
	{
		return (Deathmatch) super.getMainEvent();
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getEndingProcedure()
	{
		return END;
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getMaintenanceProcedure()
	{
		return MAINTENANCE;
	}
	
	@Override
	public String getImplementationName()
	{
		return NAME;
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getStartProcedure()
	{
		return START;
	}
	
	@Override
	public void doBeforeEnd()
	{
		super.doBeforeEnd();
		int topFrags = -1, maxFrags = -1;
		for (FastSet.Record r = getAllPlayers().head(), end = getAllPlayers().tail(); (r = r.getNext()) != end;)
		{
			L2PcInstance participant = getAllPlayers().valueOf(r);
			int frags = getFragCount(participant.getObjectId()).get();
			if (frags > maxFrags)
			{
				if (frags > topFrags)
				{
					maxFrags = topFrags;
					_second = _best;
					topFrags = frags;
					_best = participant;
				}
				else
				{
					maxFrags = frags;
					_second = participant;
				}
			}
		}
	}
	
	@Override
	public void doAfterEnd()
	{
		super.doAfterEnd();
		
		if (_frags != null)
			_frags.clear();
		
		if (_best != null)
		{
			rewardItem(_best, GlobalEvent.EVENT_MEDAL, 5);
			rewardAdena(_best, 30);
			_best.sendPacket(new PlaySound(SoundType.MUSIC, "Rm01_S"));
			{
				L2PlayerData dat = _best.getSoDPlayer();
				rewardChallenge(dat, ChallengeTemplate.MASSACRE, 1);
				dat.sendLocalizedMessage("EVENT_YOU_WERE_BEST");
			}
			_best = null;
			if (_second != null)
			{
				rewardItem(_second, GlobalEvent.EVENT_MEDAL, 1);
				rewardAdena(_second, 20);
				_second.getSoDPlayer().sendLocalizedMessage("EVENT_YOU_WERE_SECOND_BEST");
				_second = null;
			}
		}
	}
	
	@Override
	public SplitProcedure[] getSplitProcedure()
	{
		return SPLIT;
	}
	
	@Override
	public Deathmatch newSplitEvent()
	{
		return new Deathmatch(this);
	}
}
