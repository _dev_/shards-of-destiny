/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.event.global.impl;

import org.sod.Calc;
import org.sod.model.ChallengeTemplate;
import org.sod.model.event.global.EventProcedure;
import org.sod.model.event.global.GlobalEvent;

import com.l2jfree.gameserver.instancemanager.TransformationManager;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.Location;
import com.l2jfree.gameserver.model.actor.L2Playable;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.client.packets.sendable.PlaySoundPacket.PlaySound;
import com.l2jfree.gameserver.network.client.packets.sendable.PlaySoundPacket.SoundType;

/**
 * Elimination (InfL2-style) based event.
 * 
 * @author savormix
 * @since 2010.12.25
 */
public class SlaughterHouse extends LastManStanding
{
	private static final String NAME = "Slaughterhouse";
	private static final int[] TRANSFORMATIONS =
	{
			252, 253, 254, 255, 256, 257, 258
	};
	private static final int SLAUGHTER_SKILL = 9126;
	private static final Location[] SPAWNS = new Location[]
	{
			new Location(-11560, 278725, -10495), new Location(-11605, 280610, -10495),
			new Location(-13445, 280610, -10495), new Location(-13420, 278775, -10495),
	};
	
	@SuppressWarnings("unchecked")
	private static final EventProcedure<GlobalEvent>[] START = new EventProcedure[]
	{
			PROC_LEAVE_PARTY, new EventProcedure<GlobalEvent>()
			{
				@Override
				public void performOn(L2PcInstance member, GlobalEvent event)
				{
					member.stopTransformation(true);
					TransformationManager.getInstance().transformPlayer(Calc.getRandomFromArray(TRANSFORMATIONS),
							member);
					member.teleToLocation(Calc.getRandomFromArray(SPAWNS));
					member.addSkill(SLAUGHTER_SKILL, 1);
				}
			}, PROC_RECOVERY
	};
	@SuppressWarnings("unchecked")
	private static final EventProcedure<GlobalEvent>[] END = new EventProcedure[]
	{
			PROC_LEAVE_PARTY, new EventProcedure<GlobalEvent>()
			{
				@Override
				public void performOn(L2PcInstance member, GlobalEvent event)
				{
					member.removeSkill(SLAUGHTER_SKILL);
					member.stopTransformation(true);
				}
			}, PROC_RETURN, PROC_RECOVERY
	};
	
	/**
	 * Creates a Slaughterhouse event.
	 * 
	 * @param autoRemove
	 *            mark as unused after end
	 */
	public SlaughterHouse(boolean autoRelease)
	{
		super(autoRelease);
	}
	
	@Override
	public boolean canUseSkill(L2Playable character, L2PcInstance participant, L2Skill skill)
	{
		if (skill == null)
			return true;
		
		participant.getSoDPlayer().sendLocalizedMessage("EVENT_SH_SKILLS_ARE_DISABLED");
		return false;
	}
	
	@Override
	protected void doAfterEnd()
	{
		super.doAfterEnd();
		
		if (_survivor != null)
		{
			rewardItem(_survivor, GlobalEvent.EVENT_MEDAL, 2);
			rewardAdena(_survivor, 15);
			_survivor.sendPacket(new PlaySound(SoundType.MUSIC, "Rm01_S"));
			rewardChallenge(_survivor.getSoDPlayer(), ChallengeTemplate.SECTOR_CLEAR, 1);
			_survivor = null;
		}
		_phonomania = null;
	}
	
	@Override
	public String getImplementationName()
	{
		return NAME;
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getStartProcedure()
	{
		return START;
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getEndingProcedure()
	{
		return END;
	}
}
