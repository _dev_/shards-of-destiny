/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.event.global.restriction;

import org.sod.model.event.global.GlobalEvent;
import org.sod.model.event.global.TeamedEvent;
import org.sod.model.event.global.impl.Regicide;

import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.quest.Quest;
import com.l2jfree.gameserver.model.restriction.global.GlobalRestrictions.CombatState;

/**
 * TODO Auto-generated JavaDoc
 * 
 * @author savormix
 * @since 2010.12.20
 */
public class RegicideRestriction extends GlobalEventRestriction<Regicide>
{
	private RegicideRestriction()
	{
		new NpcHandler();
	}
	
	@Override
	public Class<Regicide> getEventClass()
	{
		return Regicide.class;
	}
	
	@Override
	public boolean canInviteToParty(L2PcInstance activeChar, L2PcInstance target)
	{
		GlobalEvent evt1 = GlobalEvent.getEvent(activeChar, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target, getEventClass());
		if (evt1 == null && evt2 == null)
			return true;
		else
			return getCombatState(activeChar, target) == CombatState.FRIEND;
	}
	
	@Override
	public boolean canRequestRevive(L2PcInstance activeChar)
	{
		if (GlobalEvent.getEvent(activeChar, getEventClass()) != null)
			return false;
		else
			return true;
	}
	
	@Override
	public boolean canTeleport(L2PcInstance activeChar)
	{
		return canRequestRevive(activeChar);
	}
	
	@Override
	public CombatState getCombatState(L2PcInstance activeChar, L2PcInstance target)
	{
		TeamedEvent<?> evt1 = GlobalEvent.getEvent(activeChar, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target, getEventClass());
		if (evt1 != null && evt2 != null && evt1 == evt2)
		{
			if (evt1.isSameTeam(activeChar, target))
				return CombatState.FRIEND;
			else
				return CombatState.ENEMY;
		}
		
		return CombatState.NEUTRAL;
	}
	
	@Override
	public boolean isProtected(L2Character activeChar, L2Character target, L2Skill skill, boolean sendMessage,
			L2PcInstance attacker_, L2PcInstance target_, boolean isOffensive)
	{
		if (attacker_ == null || attacker_ == target_)
			return false;
		
		Regicide evt1 = GlobalEvent.getEvent(attacker_, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target_, getEventClass());
		if (evt1 != null || evt2 != null)
		{
			if (evt1 != evt2 && !(target instanceof L2Npc))
				return true;
			
			Regicide evt = evt1;
			if (target instanceof L2Npc && (!isOffensive || evt.getGuardTeam().getMembers().contains(attacker_)))
				return ((L2Npc) target).getNpcId() == Regicide.KING_NPC;
			else if (evt.isSameTeam(attacker_, target_))
				return isOffensive;
			else
				return !isOffensive;
		}
		return false;
	}
	
	@Override
	public boolean playerDied(L2PcInstance activeChar)
	{
		return playerKilled(null, activeChar, null);
	}
	
	@Override
	public boolean playerKilled(L2Character activeChar, L2PcInstance target, L2PcInstance killer)
	{
		Regicide evt1 = GlobalEvent.getEvent(killer, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target, getEventClass());
		if (evt1 != null && evt1 == evt2 && !evt1.getGuardTeam().getMembers().contains(target))
		{
			target.setIsPendingRevive(true);
			target.teleToLocation(evt1.getTeam2().getLocToSpawn());
		}
		return false;
	}
	
	private final class NpcHandler extends Quest
	{
		private NpcHandler()
		{
			super(-1, "Regicide", "ai");
			addKillId(Regicide.KING_NPC);
		}
		
		@Override
		public String onKill(L2Npc npc, L2PcInstance killer, boolean isPet)
		{
			Regicide evt = GlobalEvent.getEvent(killer, getEventClass());
			if (evt != null)
				evt.onRegicide(killer);
			return null;
		}
	}
	
	public static RegicideRestriction getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		private static final RegicideRestriction INSTANCE = new RegicideRestriction();
	}
}
