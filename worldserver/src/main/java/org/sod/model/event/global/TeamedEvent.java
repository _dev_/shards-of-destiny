/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.event.global;

import java.util.Locale;

import javolution.util.FastSet;

import org.sod.Calc;
import org.sod.i18n.Internationalization;

import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.SystemMessageId;
import com.l2jfree.gameserver.network.client.packets.L2ServerPacket;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.Chat;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.ChatMessage;
import com.l2jfree.gameserver.network.client.packets.sendable.SystemMessagePacket.SystemMessage;
import com.l2jfree.gameserver.util.Broadcast;

/**
 * This class represents the base for a two-team event.
 * 
 * @author savormix
 * @since 2010.03.27
 */
public abstract class TeamedEvent<TeamType extends Team> extends GlobalEvent
{
	/** General formula would be 256 * 2 / [team count] */
	private static final int NOTICEABLE_COLOR_DIFFERENCE = 256;
	private static final int[] COLORS = new int[]
	{
			/*0xFF0000, 0x00FF00, 0x0000FF, 0xFFFF00, 0xFF00FF, 0x00FFFF, 0x000000,*/
			0x800000, 0x008000, 0x000080, 0x808000, 0x800080, 0x008080, 0x808080, 0xC00000, 0x00C000, 0x0000C0,
			0xC0C000, 0xC000C0, 0x00C0C0, 0xC0C0C0, 0x400000, 0x004000, 0x000040, 0x404000, 0x400040, 0x004040,
			0x404040, 0x200000, 0x002000, 0x000020, 0x202000, 0x200020, 0x002020, 0x202020, 0x600000, 0x006000,
			0x000060, 0x606000, 0x600060, 0x006060, 0x606060, 0xA00000, 0x00A000, 0x0000A0, 0xA0A000, 0xA000A0,
			0x00A0A0, 0xA0A0A0, 0xE00000, 0x00E000, 0x0000E0, 0xE0E000, 0xE000E0, 0x00E0E0, 0xE0E0E0,
	};
	
	private final TeamType _team1;
	private final TeamType _team2;
	private final int _colorStat;
	private TeamType _winner;
	
	protected TeamedEvent(int maintenanceInterval, int splitLimit, double splitRatio, int maxPoints, int fencedArea,
			boolean autoRemove, TeamType t1, TeamType t2)
	{
		super(maintenanceInterval, splitLimit, splitRatio, maxPoints, fencedArea, autoRemove);
		_team1 = t1;
		_team2 = t2;
		_colorStat = (t1.isRandColor() ? 1 : 0) + (t2.isRandColor() ? 2 : 0);
		recalculate();
	}
	
	protected TeamedEvent(TeamedEvent<TeamType> main, TeamType t1, TeamType t2)
	{
		super(main);
		_team1 = t1;
		_team2 = t2;
		_colorStat = main.getColorStat();
		recalculate();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public TeamedEvent<TeamType> getMainEvent()
	{
		return (TeamedEvent<TeamType>) super.getMainEvent();
	}
	
	/** Updates both team strength coefficients */
	protected void recalculate()
	{
		getTeam1().setPower(getCoef(getTeam1()));
		getTeam2().setPower(getCoef(getTeam2()));
	}
	
	/* (non-Javadoc)
	 * @see GlobalEvent#add(com.l2jfree.gameserver.model.actor.instance.L2PcInstance)
	 */
	@Override
	public boolean add(L2PcInstance player)
	{
		if (!super.addTest(player))
			return false;
		
		if (getTeam1().getPower() > getTeam2().getPower())
			getTeam2().getMembers().add(player);
		else
			getTeam1().getMembers().add(player);
		recalculate();
		return addDirect(player);
	}
	
	/* (non-Javadoc)
	 * @see GlobalEvent#remove(com.l2jfree.gameserver.model.actor.instance.L2PcInstance)
	 */
	@Override
	public boolean remove(L2PcInstance player)
	{
		if (!super.remove(player))
			return false;
		
		getTeam1().getMembers().remove(player);
		getTeam2().getMembers().remove(player);
		recalculate();
		return true;
	}
	
	@Override
	public abstract TeamedEvent<? extends TeamType> newSplitEvent();
	
	/**
	 * Moves players from this event to a new event in the same order and team
	 * (removed from team before split leave and added after split enter) as they
	 * were added originally, thus the same balance is ensured in the new event.
	 * The balance of teams in this event is undefined.<BR>
	 * If overridden, this method must not depend on split procedures.
	 * 
	 * @param event
	 *            the new event
	 */
	@Override
	protected void onSplit(GlobalEvent event)
	{
		super.onSplit(event);
		recalculate();
	}
	
	/* (non-Javadoc)
	 * @see org.sod.model.event.global.GlobalEvent#doBeforeStart()
	 */
	@Override
	protected void doBeforeStart()
	{
		super.doBeforeStart();
		correctColors();
	}
	
	/* (non-Javadoc)
	 * @see org.sod.model.event.global.GlobalEvent#doBeforeEnd()
	 */
	@Override
	protected void doBeforeEnd()
	{
		super.doBeforeEnd();
		if (_winner == null)
		{
			if (getTeam1().getPoints() > getTeam2().getPoints())
				_winner = getTeam1();
			else if (getTeam2().getPoints() > getTeam1().getPoints())
				_winner = getTeam2();
		}
	}
	
	/* (non-Javadoc)
	 * @see org.sod.model.event.global.GlobalEvent#doAfterEnd()
	 */
	@Override
	protected void doAfterEnd()
	{
		super.doAfterEnd();
		getTeam1().resetPoints();
		getTeam2().resetPoints();
		getTeam1().getMembers().clear();
		getTeam2().getMembers().clear();
		recalculate();
	}
	
	/**
	 * Looks up the given player's team number in this event.
	 * 
	 * @param player
	 *            A player
	 * @return the team number (0 for non-participant)
	 */
	public final int getTeamNo(L2PcInstance player)
	{
		if (player != null)
		{
			if (getTeam1().getMembers().contains(player))
				return 1;
			else if (getTeam2().getMembers().contains(player))
				return 2;
		}
		return 0;
	}
	
	/**
	 * Looks up the given player's team in this event.
	 * 
	 * @param player
	 *            A player
	 * @return the <CODE>Team</CODE> object or <CODE>null</CODE>
	 */
	public final TeamType getTeam(L2PcInstance player)
	{
		return getTeamByNo(getTeamNo(player));
	}
	
	/**
	 * Looks up the given player's enemy team in this event.
	 * 
	 * @param player
	 *            A player
	 * @return the <CODE>Team</CODE> object or <CODE>null</CODE>
	 */
	public final TeamType getEnemyTeam(L2PcInstance player)
	{
		switch (getTeamNo(player))
		{
		case 1:
			return getTeam2();
		case 2:
			return getTeam1();
		default:
			return null;
		}
	}
	
	/**
	 * Returns a team specified by a number.
	 * 
	 * @param number
	 *            team number
	 * @return <CODE>Team</CODE> object or null
	 */
	public final TeamType getTeamByNo(int number)
	{
		switch (number)
		{
		case 1:
			return getTeam1();
		case 2:
			return getTeam2();
		default:
			return null;
		}
	}
	
	/**
	 * Checks if two players are in the same team.
	 * 
	 * @param player1
	 *            A player
	 * @param player2
	 *            Another player
	 * @return both players are in the same team OR both are teamless
	 */
	public final boolean isSameTeam(L2PcInstance player1, L2PcInstance player2)
	{
		if (player1 == player2)
			return true;
		else
			return getTeamNo(player1) == getTeamNo(player2);
	}
	
	protected ChatMessage createEventMessage(String message)
	{
		return new ChatMessage(Chat.ALLIANCE, getImplementationName(), message);
	}
	
	protected void broadcastTeamScores(TeamRewardProcedure<TeamType> rewardProc)
	{
		Internationalization i18n = Internationalization.getInstance();
		TeamType t1 = getTeam1(), t2 = getTeam2();
		FastSet<L2PcInstance> members;
		
		members = t1.getMembers();
		for (FastSet.Record r = members.head(), end = members.tail(); (r = r.getNext()) != end;)
		{
			L2PcInstance player = members.valueOf(r);
			Locale loc = player.getSoDPlayer().getSettings().getLocale();
			player.sendPacket(createEventMessage(i18n.get(loc, "EVENT_TEAMED_YOUR_TEAM_SCORED", "%points%",
					String.valueOf(t1.getPoints()))));
			player.sendPacket(createEventMessage(i18n.get(loc, "EVENT_TEAMED_ENEMY_TEAM_SCORED", "%points%",
					String.valueOf(t2.getPoints()))));
			rewardProc.rewardMember(player, t1);
		}
		members = t2.getMembers();
		for (FastSet.Record r = members.head(), end = members.tail(); (r = r.getNext()) != end;)
		{
			L2PcInstance player = members.valueOf(r);
			Locale loc = player.getSoDPlayer().getSettings().getLocale();
			player.sendPacket(createEventMessage(i18n.get(loc, "EVENT_TEAMED_YOUR_TEAM_SCORED", "%points%",
					String.valueOf(t2.getPoints()))));
			player.sendPacket(createEventMessage(i18n.get(loc, "EVENT_TEAMED_ENEMY_TEAM_SCORED", "%points%",
					String.valueOf(t1.getPoints()))));
			rewardProc.rewardMember(player, t2);
		}
	}
	
	public void broadcastMessageToTeam(TeamType team, Chat channel, String key, String... repVals)
	{
		Broadcast.broadcastMessage(team.getMembers(), channel, getImplementationName(), false, key, repVals);
	}
	
	/** @return the first team */
	public final TeamType getTeam1()
	{
		return _team1;
	}
	
	/** @return the second team */
	public final TeamType getTeam2()
	{
		return _team2;
	}
	
	/** @return random color identifier */
	public final int getColorStat()
	{
		return _colorStat;
	}
	
	/** @return the winner team */
	public final TeamType getWinner()
	{
		return _winner;
	}
	
	/**
	 * Saves the event winner
	 * 
	 * @param winner
	 *            the winner team
	 */
	protected final void setWinner(TeamType winner)
	{
		_winner = winner;
	}
	
	protected void correctColors()
	{
		Team fixed, random;
		switch (getColorStat())
		{
		case 1:
			fixed = getTeam1();
			random = getTeam2();
			break;
		case 2:
		case 3:
			fixed = getTeam2();
			random = getTeam1();
			break;
		default:
			return;
		}
		if (fixed.isRandColor())
			fixed.setColor(Calc.getRandomFromArray(COLORS));
		for (int color : COLORS)
		{
			random.setColor(color);
			if ((Math.abs(fixed.getRed() - random.getRed()) + Math.abs(fixed.getGreen() - random.getGreen()) + Math
					.abs(fixed.getBlue() - random.getBlue())) < NOTICEABLE_COLOR_DIFFERENCE)
				break;
		}
		/*
		if (fixed.isRandColor())
			fixed.setColor(Rnd.get(256), Rnd.get(256), Rnd.get(256));
		random.setColor(Rnd.get(256), Rnd.get(256), Rnd.get(256));
		while ((Math.abs(fixed.getRed() - random.getRed()) +
				Math.abs(fixed.getGreen() - random.getGreen()) +
				Math.abs(fixed.getBlue() - random.getBlue())) < NOTICEABLE_COLOR_DIFFERENCE)
			random.setColor(Rnd.get(256), Rnd.get(256), Rnd.get(256));
		 */
	}
	
	/**
	 * Send a packet to all team members
	 * 
	 * @param packet
	 *            A server packet
	 */
	public static void broadcastToTeam(Team t, L2ServerPacket... packets)
	{
		for (FastSet.Record r = t.getMembers().head(), end = t.getMembers().tail(); (r = r.getNext()) != end;)
			for (L2ServerPacket packet : packets)
				t.getMembers().valueOf(r).sendPacket(packet);
	}
	
	/**
	 * Send a static system message to team event members
	 * 
	 * @param smsg
	 *            A <CODE>SystemMessageId</CODE> enumerated value
	 */
	public static final void broadcastToTeam(Team t, SystemMessageId smsg)
	{
		broadcastToTeam(t, smsg.getSystemMessage());
	}
	
	/**
	 * Send a message to all team members
	 * 
	 * @param message
	 *            A message
	 */
	public static final void broadcastToTeam(Team t, String message)
	{
		broadcastToTeam(t, SystemMessage.sendString(message));
	}
	
	/**
	 * Get team's strength coefficient. The higher this coefficient is, the stronger the team is.<BR>
	 * This method is abstract because for different events <I>(e.g. TvT and CTF)</I>, same structure
	 * teams may perform differently <I>(DDs are superior in TvT, while Tanks & Healers are superior
	 * in CTF)</I>
	 * 
	 * @param team
	 *            A <CODE>Team</CODE> object
	 * @return team's strength
	 */
	protected abstract double getCoef(TeamType team);
	
	protected static final EventProcedure<TeamedEvent<Team>> PROC_COLOR = new EventProcedure<TeamedEvent<Team>>()
	{
		@Override
		public void performOn(L2PcInstance member, TeamedEvent<Team> event)
		{
			Team t = event.getTeam(member);
			if (t != null)
				setNameColor(member, t.getColor());
		}
	};
	protected static final EventProcedure<GlobalEvent> PROC_WASH_COLOR = new EventProcedure<GlobalEvent>()
	{
		@Override
		public void performOn(L2PcInstance member, GlobalEvent event)
		{
			setNameColor(member, -1);
		}
	};
	protected static final EventProcedure<TeamedEvent<Team>> PROC_MOVE_TO_TEAM = new EventProcedure<TeamedEvent<Team>>()
	{
		@Override
		public void performOn(L2PcInstance member, TeamedEvent<Team> event)
		{
			Team t = event.getTeam(member);
			if (t != null)
				member.teleToLocation(t.getLocToSpawn(), true);
		}
	};
	
	protected/* static */final SplitProcedure PROC_SPLIT_NEW_BALANCED = new SplitProcedure()
	{
		@Override
		public void performOn(L2PcInstance member, GlobalEvent event)
		{
			TeamedEvent<?> evt = (TeamedEvent<?>) event;
			boolean one = false;
			if (getTeam1().getMembers().remove(member))
				one = true;
			else if (getTeam2().getMembers().remove(member) || getTeam1().getPower() > getTeam2().getPower())
				one = false;
			else
				one = true;
			if (one)
				evt.getTeam1().getMembers().add(member);
			else
				evt.getTeam2().getMembers().add(member);
		}
	};
}
