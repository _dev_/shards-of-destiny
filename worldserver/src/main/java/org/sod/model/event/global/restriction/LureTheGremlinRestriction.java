/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.event.global.restriction;

import org.sod.model.event.global.GlobalEvent;
import org.sod.model.event.global.TeamedEvent;
import org.sod.model.event.global.impl.LureTheGremlin;
import org.sod.model.event.global.impl.LureTheGremlin.LurerTeam;

import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.instance.L2GremlinBallInstance;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.restriction.global.GlobalRestrictions.CombatState;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.Chat;

/**
 * Restrictions for the Gremlin Ball event.
 * 
 * @author savormix
 * @since 2010.10.24
 */
public class LureTheGremlinRestriction extends GlobalEventRestriction<LureTheGremlin>
{
	private LureTheGremlinRestriction()
	{
	}
	
	@Override
	public Class<LureTheGremlin> getEventClass()
	{
		return LureTheGremlin.class;
	}
	
	@Override
	public boolean canCreateEffect(L2Character activeChar, L2Character target, L2Skill skill)
	{
		boolean cannot = (target instanceof L2GremlinBallInstance);
		if (cannot)
			activeChar.sendResistedMyEffectMessage(target, skill);
		return !cannot;
	}
	
	@Override
	public boolean canInviteToParty(L2PcInstance activeChar, L2PcInstance target)
	{
		GlobalEvent evt1 = GlobalEvent.getEvent(activeChar, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target, getEventClass());
		if (evt1 == null && evt2 == null)
			return true;
		else
			return getCombatState(activeChar, target) == CombatState.FRIEND;
	}
	
	@Override
	public boolean canRequestRevive(L2PcInstance activeChar)
	{
		if (GlobalEvent.getEvent(activeChar, getEventClass()) != null)
			return false;
		else
			return true;
	}
	
	@Override
	public boolean canTeleport(L2PcInstance activeChar)
	{
		return canRequestRevive(activeChar);
	}
	
	@Override
	public CombatState getCombatState(L2PcInstance activeChar, L2PcInstance target)
	{
		TeamedEvent<?> evt1 = GlobalEvent.getEvent(activeChar, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target, getEventClass());
		if (evt1 != null && evt2 != null && evt1 == evt2)
		{
			if (evt1.isSameTeam(activeChar, target))
				return CombatState.FRIEND;
			else
				return CombatState.ENEMY;
		}
		
		return CombatState.NEUTRAL;
	}
	
	@Override
	public boolean isProtected(L2Character activeChar, L2Character target, L2Skill skill, boolean sendMessage,
			L2PcInstance attacker_, L2PcInstance target_, boolean isOffensive)
	{
		if (attacker_ == null || attacker_ == target_ || target_ == null)
			return false;
		
		LureTheGremlin evt1 = GlobalEvent.getEvent(attacker_, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target_, getEventClass());
		if (evt1 != null || evt2 != null)
		{
			if (evt1 != evt2)
				return true;
			LureTheGremlin evt = evt1;
			if (evt.isSameTeam(attacker_, target_))
				return isOffensive;
			else
				return !isOffensive;
		}
		return false;
	}
	
	@Override
	public boolean playerKilled(L2Character activeChar, L2PcInstance target, L2PcInstance killer)
	{
		LureTheGremlin evt1 = GlobalEvent.getEvent(killer, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target, getEventClass());
		if (evt1 != null && evt1 == evt2)
		{
			int no = evt1.getTeamNo(killer);
			LurerTeam t;
			if (evt1.getMainEvent() != null)
				t = evt1.getMainEvent().getTeamByNo(no);
			else
				t = evt1.getTeamByNo(no);
			t.removePoint();
			evt1.broadcastMessageToParticipants(Chat.ALLIANCE, "EVENT_LTG_TEAM_LOSES_A_POINT", "%killerName%",
					killer.getName(), "%victimName%", target.getName());
			evt1.addOffender(killer);
		}
		return false;
	}
	
	public static LureTheGremlinRestriction getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static final class SingletonHolder
	{
		private static final LureTheGremlinRestriction INSTANCE = new LureTheGremlinRestriction();
	}
}
