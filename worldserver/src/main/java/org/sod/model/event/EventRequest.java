/*
 * MISSING LICENSING INFO
 */
package org.sod.model.event;

/**
 * @author savormix
 * 
 */
public enum EventRequest
{
	SHOW_VOTING, CAST_VOTE, JOIN_EVENT, JOIN_MINI_EVENT;
}
