/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.event.global.restriction;

import org.sod.model.event.global.GlobalEvent;

import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.restriction.global.AbstractRestriction;
import com.l2jfree.gameserver.model.restriction.global.GlobalRestriction;

/**
 * Simplifies event restriction code reuse.
 * 
 * @author savormix
 * @since 2010.12.12
 */
public abstract class GlobalEventRestriction<T extends GlobalEvent> extends AbstractRestriction
{
	public abstract Class<T> getEventClass();
	
	@Override
	public boolean isRestricted(L2PcInstance activeChar, Class<? extends GlobalRestriction> callingRestriction)
	{
		if (callingRestriction == getClass())
			return false;
		
		GlobalEvent ge = GlobalEvent.getEvent(activeChar, getEventClass());
		if (ge != null)
		{
			activeChar.getSoDPlayer().sendLocalizedMessage("NPC_EVENT_MANAGER_ALREADY_IN_EVENT", "%eventName%",
					ge.getImplementationName());
			return true;
		}
		
		return false;
	}
}
