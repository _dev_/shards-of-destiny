/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.event.global.restriction;

import org.sod.model.event.global.GlobalEvent;
import org.sod.model.event.global.impl.Deathmatch;

import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.restriction.global.GlobalRestrictions.CombatState;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.Chat;

/**
 * TODO Auto-generated JavaDoc
 * 
 * @author savormix
 * @since 2010.03.29
 */
public class DeathmatchRestriction extends GlobalEventRestriction<Deathmatch>
{
	private DeathmatchRestriction()
	{
	}
	
	@Override
	public Class<Deathmatch> getEventClass()
	{
		return Deathmatch.class;
	}
	
	@Override
	public boolean canInviteToParty(L2PcInstance activeChar, L2PcInstance target)
	{
		GlobalEvent evt1 = GlobalEvent.getEvent(activeChar, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target, getEventClass());
		if (evt1 == null && evt2 == null)
			return true;
		else
			return false;
	}
	
	@Override
	public boolean canRequestRevive(L2PcInstance activeChar)
	{
		if (GlobalEvent.getEvent(activeChar, getEventClass()) != null)
			return false;
		else
			return true;
	}
	
	@Override
	public boolean canTeleport(L2PcInstance activeChar)
	{
		return canRequestRevive(activeChar);
	}
	
	@Override
	public CombatState getCombatState(L2PcInstance activeChar, L2PcInstance target)
	{
		GlobalEvent evt1 = GlobalEvent.getEvent(activeChar, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target, getEventClass());
		if (evt1 != null || evt2 != null)
			if (evt1 == evt2)
				return CombatState.ENEMY;
		
		return CombatState.NEUTRAL;
	}
	
	@Override
	public boolean isProtected(L2Character activeChar, L2Character target, L2Skill skill, boolean sendMessage,
			L2PcInstance attacker_, L2PcInstance target_, boolean isOffensive)
	{
		if (attacker_ == null || attacker_ == target_)
			return false;
		
		GlobalEvent evt1 = GlobalEvent.getEvent(attacker_, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target_, getEventClass());
		if (evt1 != null || evt2 != null)
		{
			if (evt1 == evt2)
				return !isOffensive;
			else
				return true;
		}
		return false;
	}
	
	@Override
	public boolean playerKilled(L2Character activeChar, L2PcInstance target, L2PcInstance killer)
	{
		Deathmatch evt1 = GlobalEvent.getEvent(killer, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target, getEventClass());
		if (evt1 != null && evt1 == evt2)
		{
			Integer kOid = killer.getObjectId();
			evt1.broadcastMessageToParticipants(Chat.PARTY, "EVENT_DM_PLAYER_WAS_KILLED", "%killerName%",
					killer.getName(), "%kKills%", String.valueOf(evt1.getFragCount(kOid).get()), "%victimName%",
					target.getName(), "%vKills%", String.valueOf(evt1.getFragCount(target.getObjectId()).get()));
			evt1.addFrag(kOid);
		}
		return false;
	}
	
	public static final DeathmatchRestriction getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static final class SingletonHolder
	{
		private static final DeathmatchRestriction INSTANCE = new DeathmatchRestriction();
	}
}
