/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.event.global.restriction;

import javolution.util.FastSet;

import org.sod.model.ChallengeTemplate;
import org.sod.model.event.global.GlobalEvent;
import org.sod.model.event.global.TeamedEvent;
import org.sod.model.event.global.impl.GolemDispute;
import org.sod.model.event.global.impl.GolemDispute.DisputeTeam;

import com.l2jfree.gameserver.datatables.ItemTable;
import com.l2jfree.gameserver.handler.IItemHandler;
import com.l2jfree.gameserver.model.L2ItemInstance;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.L2Spawn;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.L2Playable;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.itemcontainer.Inventory;
import com.l2jfree.gameserver.model.itemcontainer.PcInventory;
import com.l2jfree.gameserver.model.quest.Quest;
import com.l2jfree.gameserver.model.restriction.global.GlobalRestrictions.CombatState;
import com.l2jfree.gameserver.model.restriction.global.RestrictionPriority;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.Chat;
import com.l2jfree.gameserver.network.client.packets.sendable.UserInfo.MyPlayerInfo;
import com.l2jfree.gameserver.skills.SpecialEffect;

/**
 * Restrictions for the Goblin Dispute event.
 * 
 * @author Horus
 * @author savormix
 * @since 2010.10.24
 */
public class GolemDisputeRestriction extends GlobalEventRestriction<GolemDispute>
{
	private final FastSet<L2Npc> _goldenNpcs;
	
	private GolemDisputeRestriction()
	{
		_goldenNpcs = FastSet.newInstance();
		new NpcHandler();
	}
	
	public final void addGolden(L2Npc golem)
	{
		_goldenNpcs.add(golem);
	}
	
	public final void removeGolden(L2Npc golem)
	{
		_goldenNpcs.remove(golem);
	}
	
	@Override
	public Class<GolemDispute> getEventClass()
	{
		return GolemDispute.class;
	}
	
	@Override
	public boolean canInviteToParty(L2PcInstance activeChar, L2PcInstance target)
	{
		GlobalEvent evt1 = GlobalEvent.getEvent(activeChar, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target, getEventClass());
		if (evt1 == null && evt2 == null)
			return true;
		else
			return getCombatState(activeChar, target) == CombatState.FRIEND;
	}
	
	@Override
	public boolean canRequestRevive(L2PcInstance activeChar)
	{
		if (GlobalEvent.getEvent(activeChar, getEventClass()) != null)
			return false;
		else
			return true;
	}
	
	@Override
	public boolean canTeleport(L2PcInstance activeChar)
	{
		return canRequestRevive(activeChar);
	}
	
	@Override
	public boolean canUseItemHandler(Class<? extends IItemHandler> clazz, int itemId, L2Playable activeChar,
			L2ItemInstance item, L2PcInstance player)
	{
		if (itemId == GolemDispute.GOLDEN_GOLEM_ITEM)
			return false;
		else if (player != null
				&& item != null
				&& item.isWeapon()
				&& player.getInventory().getPaperdollItemId(PcInventory.PAPERDOLL_RHAND) == GolemDispute.GOLDEN_GOLEM_ITEM)
			return false;
		else
			return true;
	}
	
	@Override
	public boolean canDestroyItem(L2PcInstance activeChar, int itemId, L2ItemInstance item)
	{
		if (itemId == GolemDispute.GOLDEN_GOLEM_ITEM && GlobalEvent.getEvent(activeChar, getEventClass()) != null)
		{
			activeChar.getSoDPlayer().sendLocalizedMessage("EVENT_GD_CANNOT_DESTROY_STICK");
			activeChar.doDie(null);
		}
		return true;
	}
	
	@Override
	public CombatState getCombatState(L2PcInstance activeChar, L2PcInstance target)
	{
		TeamedEvent<?> evt1 = GlobalEvent.getEvent(activeChar, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target, getEventClass());
		if (evt1 != null && evt2 != null && evt1 == evt2)
		{
			if (evt1.isSameTeam(activeChar, target))
				return CombatState.FRIEND;
			else
				return CombatState.ENEMY;
		}
		
		return CombatState.NEUTRAL;
	}
	
	@Override
	public boolean isProtected(L2Character activeChar, L2Character target, L2Skill skill, boolean sendMessage,
			L2PcInstance attacker_, L2PcInstance target_, boolean isOffensive)
	{
		if (attacker_ == null || attacker_ == target_ || target_ == null)
			return false;
		
		GolemDispute evt1 = GlobalEvent.getEvent(attacker_, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target_, getEventClass());
		if (evt1 != null || evt2 != null)
		{
			if (target instanceof L2Npc)
			{
				L2Npc npc = (L2Npc) target;
				if (evt1 != null && evt1.getTeam(npc.getNpcId()) == evt1.getTeam(attacker_))
					return true;
			}
			
			if (evt1 != evt2)
				return true;
			GolemDispute evt = evt1;
			if (evt.isSameTeam(attacker_, target_))
				return isOffensive;
			else
				return !isOffensive;
		}
		return false;
	}
	
	@Override
	@RestrictionPriority(RestrictionPriority.DEFAULT_PRIORITY + 0.1)
	public void playerDisconnected(L2PcInstance activeChar)
	{
		onDeathCarrier(activeChar, null, GlobalEvent.getEvent(activeChar, getEventClass()), false);
	}
	
	@Override
	public boolean playerDied(L2PcInstance activeChar)
	{
		return playerKilled(null, activeChar, null);
	}
	
	@Override
	public boolean playerKilled(L2Character activeChar, L2PcInstance target, L2PcInstance killer)
	{
		onDeathCarrier(target, killer, GlobalEvent.getEvent(target, getEventClass()), true);
		return false;
	}
	
	private void onDeathCarrier(L2PcInstance activeChar, L2PcInstance killer, GolemDispute event, boolean killed)
	{
		L2ItemInstance wpn = activeChar.getInventory().getPaperdollItem(Inventory.PAPERDOLL_RHAND);
		if (wpn != null && wpn.getItemId() == GolemDispute.GOLDEN_GOLEM_ITEM)
		{
			activeChar.useEquippableItem(wpn, true);
			activeChar.sendPacket(MyPlayerInfo.PACKET);
			if (activeChar.destroyItem("Golden Golem", wpn, killer, true))
			{
				if (event != null)
				{
					DisputeTeam enemy = event.getEnemyTeam(activeChar);
					enemy.unspawnGolem();
					event.getGm().spawnNpc(event.getInstanceId());
					if (killed)
						event.broadcastMessageToParticipants(Chat.ALLIANCE, "EVENT_GD_GOLDEN_CARRIER_KILLED");
					event.broadcastMessageToParticipants(Chat.ALLIANCE, "EVENT_GD_GOLDEN_GOLEM_RESPAWNED");
					if (killer != null)
						event.rewardChallenge(killer.getSoDPlayer(), ChallengeTemplate.REVENGINEERING, 1);
				}
				if (activeChar.getTransformationId() == 116)
					activeChar.untransform();
				activeChar.stopSpecialEffect(SpecialEffect.S_AIR_ROOT);
			}
		}
	}
	
	public void onDeathGolemGolden(L2PcInstance killer)
	{
		GolemDispute event = GlobalEvent.getEvent(killer, getEventClass());
		if (event == null)
			return;
		L2ItemInstance stick = ItemTable.getInstance().createItem(event.getImplementationName(),
				GolemDispute.GOLDEN_GOLEM_ITEM, 1, killer);
		event.getGm().unspawnNpc(killer);
		killer.addItem(event.getImplementationName(), stick, null, true);
		killer.useEquippableItem(stick, true);
		killer.sendPacket(MyPlayerInfo.PACKET);
		killer.startSpecialEffect(SpecialEffect.S_AIR_ROOT);
		event.broadcastMessageToParticipants(Chat.ALLIANCE, "EVENT_GD_GOLDEN_CARRIER_NEW", "%name%", killer.getName());
		event.rewardChallenge(killer.getSoDPlayer(), ChallengeTemplate.WRENCH_IS_MINE, 1);
		DisputeTeam kill = event.getTeam(killer);
		if (event.isOver(kill.addPoint()))
			return;
		
		DisputeTeam enemy = event.getEnemyTeam(killer);
		enemy.spawnGolem(event.getInstanceId());
		event.broadcastMessageToTeam(enemy, Chat.PRIVATE, "EVENT_GD_GOLDEN_CARRIER_KILL");
		event.broadcastMessageToTeam(event.getTeam(killer), Chat.PRIVATE, "EVENT_GD_GOLDEN_CARRIER_PROTECT");
	}
	
	private final class NpcHandler extends Quest
	{
		private NpcHandler()
		{
			super(-1, "GoldenGolem", "ai");
			addKillId(GolemDispute.GOLDEN_GOLEM_NPC);
			addKillId(GolemDispute.GOLEM_PURPLE);
			addKillId(GolemDispute.GOLEM_ORANGE);
		}
		
		@Override
		public String onKill(L2Npc npc, L2PcInstance killer, boolean isPet)
		{
			GolemDispute event = GlobalEvent.getEvent(killer, getEventClass());
			if (event == null)
			{
				npc.deleteMe();
				L2Spawn sp = npc.getSpawn();
				sp.doSpawn();
				return null;
			}
			DisputeTeam dt = event.getTeam(npc.getNpcId());
			if (dt != null)
			{
				DisputeTeam killTeam = event.getTeam(killer);
				event.broadcastMessageToParticipants(Chat.ALLIANCE, "EVENT_GD_BONUS_POINTS", "%points%",
						String.valueOf(GolemDispute.TEAMED_GOLEM_POINTS));
				GolemDisputeRestriction.getInstance().onDeathCarrier(event.getGm().getCarrier(), null, event, false);
				event.rewardChallenge(killer.getSoDPlayer(), ChallengeTemplate.THAT_STUPID_ROCK, 1);
				
				for (int i = 1; i < GolemDispute.TEAMED_GOLEM_POINTS; i++)
					killTeam.addPoint();
				event.isOver(killTeam.addPoint());
			}
			else
				GolemDisputeRestriction.getInstance().onDeathGolemGolden(killer);
			return null;
		}
	}
	
	public static GolemDisputeRestriction getInstance()
	{
		return SingletonHolder._instance;
	}
	
	private static final class SingletonHolder
	{
		private static final GolemDisputeRestriction _instance = new GolemDisputeRestriction();
	}
}
