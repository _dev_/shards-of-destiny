/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.event.global.impl;

import java.util.concurrent.atomic.AtomicInteger;

import javolution.util.FastMap;
import javolution.util.FastSet;

import org.sod.model.ChallengeTemplate;
import org.sod.manager.GlobalEventManager;
import org.sod.model.L2PlayerData;
import org.sod.model.event.TeamVersusTeam;
import org.sod.model.event.global.EventProcedure;
import org.sod.model.event.global.GlobalEvent;
import org.sod.model.event.global.ProcInfoMessage;
import org.sod.model.event.global.SplitProcedure;
import org.sod.model.event.global.Team;
import org.sod.model.event.global.TeamRewardProcedure;
import org.sod.model.event.global.TeamedEvent;
import org.sod.model.event.global.impl.LureTheGremlin.LurerTeam;
import org.sod.model.event.global.restriction.LureTheGremlinRestriction;

import com.l2jfree.Config;
import com.l2jfree.gameserver.ai.CtrlIntention;
import com.l2jfree.gameserver.datatables.NpcTable;
import com.l2jfree.gameserver.instancemanager.ZoneManager;
import com.l2jfree.gameserver.model.L2Spawn;
import com.l2jfree.gameserver.model.Location;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.instance.L2GremlinBallInstance;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.base.ClassId;
import com.l2jfree.gameserver.model.zone.L2Zone;
import com.l2jfree.gameserver.model.zone.L2Zone.RestartType;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.Chat;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;

/**
 * Gremlin Ball event, originally from Hyperion L2, idea adaptation by shin.
 * 
 * @author savormix
 * @since 2010.10.23
 */
public class LureTheGremlin extends TeamedEvent<LurerTeam>
{
	private static final String NAME = "Lure the Gremlin";
	private static final int GREMLIN = 90063;
	/*
	private static final int AGGRO_POINTS = 5;
	// this includes fair DPS
	private static final int LURING_POINTS = 4;
	private static final int DPS_POINTS = 3;
	private static final int LOW_AGGRO_POINTS = 2;
	 */
	public static final int CORNER_NPC_RED = 90064;
	public static final int CORNER_NPC_BLUE = 90065;
	public static final int COLOR_RED = 0x4444FF;
	public static final int COLOR_BLUE = 0xFF4444;
	static
	{
		LureTheGremlinRestriction.getInstance().activate();
	}
	@SuppressWarnings("unchecked")
	private static final EventProcedure<TeamedEvent<Team>>[] START = new EventProcedure[]
	{
			PROC_LEAVE_PARTY, PROC_COLOR, PROC_MOVE_TO_TEAM, PROC_RECOVERY,
			new ProcInfoMessage("EVENT_LTG_INSTRUCTIONS"),
	};
	@SuppressWarnings("unchecked")
	private static final EventProcedure<TeamedEvent<Team>>[] MAINTENANCE = new EventProcedure[]
	{
		new EventProcedure<TeamedEvent<Team>>()
		{
			@Override
			public void performOn(L2PcInstance member, TeamedEvent<Team> event)
			{
				if (member.isDead())
				{
					PROC_ADD_PENDING_REVIVE.performOn(member, event);
					PROC_MOVE_TO_TEAM.performOn(member, event);
					PROC_RECOVERY.performOn(member, event);
				}
			}
		}
	};
	@SuppressWarnings("unchecked")
	private static final EventProcedure<GlobalEvent>[] END = new EventProcedure[]
	{
			PROC_LEAVE_PARTY, PROC_WASH_COLOR, PROC_RETURN, PROC_RECOVERY
	};
	private/*static*/final SplitProcedure[] SPLIT = new SplitProcedure[]
	{
			PROC_UNSAFE_TRANSFER, PROC_SPLIT_NEW_BALANCED
	};
	
	private final L2Spawn _gremlin;
	private final FastSet<L2Npc> _npcs;
	private final FastMap<Integer, AtomicInteger> _offenders;
	
	/**
	 * Creates a Gremlin Ball event.
	 * 
	 * @param maxPoints
	 *            point limit
	 * @param autoRemove
	 *            mark as unused after end
	 * @param t1
	 *            Team 1
	 * @param t2
	 *            Team 2
	 */
	public LureTheGremlin(int maxPoints, boolean autoRemove, LurerTeam t1, LurerTeam t2)
	{
		super(15000, 20, 0.5, maxPoints, -1, autoRemove, t1, t2);
		_gremlin = new L2Spawn(GREMLIN);
		_gremlin.setLocx(12145);
		_gremlin.setLocy(-49150);
		_gremlin.setLocz(-3005);
		_gremlin.setInstanceId(getInstanceId());
		_gremlin.stopRespawn();
		_npcs = FastSet.newInstance();
		_offenders = new FastMap<Integer, AtomicInteger>().setShared(true);
	}
	
	protected LureTheGremlin(LureTheGremlin main, LurerTeam t1, LurerTeam t2)
	{
		super(main, t1, t2);
		_gremlin = new L2Spawn(GREMLIN);
		_gremlin.setLocx(12145);
		_gremlin.setLocy(-49150);
		_gremlin.setLocz(-3005);
		_gremlin.setInstanceId(getInstanceId());
		_gremlin.stopRespawn();
		_npcs = FastSet.newInstance();
		_offenders = new FastMap<Integer, AtomicInteger>().setShared(true);
	}
	
	public void spawnGremlin()
	{
		L2Npc ball = _gremlin.spawnOne(false);
		if (ball instanceof L2GremlinBallInstance)
			((L2GremlinBallInstance) ball).setEvent(this);
		else
			_log.warn("Invalid type of " + GREMLIN + " NPC, should be L2GremlinBall!", new IllegalArgumentException());
		_npcs.add(ball);
		ball.getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE);
	}
	
	public synchronized void killGremlin(L2GremlinBallInstance ball, int zoneId)
	{
		if (!_npcs.contains(ball))
			return;
		ball.setEvent(null);
		ball.doDie(null);
		LurerTeam lurer;
		if (getMainEvent() != null)
			lurer = getMainEvent().getTeamByNo(zoneId % 10);
		else
			lurer = getTeamByNo(zoneId % 10);
		if (lurer != null)
		{
			broadcastMessageToParticipants(Chat.ALLIANCE, "EVENT_LTG_TEAM_SCORES_A_POINT", "%team%",
					String.valueOf(zoneId % 10));
			isOver(lurer.addPoint());
		}
		else
			_log.warn("Invalid Gremlin Ball event scoring zone ID!" + zoneId, new IllegalArgumentException());
		ball.deleteMe();
		spawnGremlin();
	}
	
	private void spawnZoneMarkers()
	{
		for (int i = 1; i <= 2; i++)
		{
			LurerTeam t = getTeamByNo(i);
			L2Zone z = t.getZone();
			if (z == null)
				continue;
			Location[] locs = z.getRestartPoints(RestartType.OWNER);
			L2NpcTemplate temp = NpcTable.getInstance().getTemplate(t.getCornerNpcId());
			if (temp == null)
			{
				_log.warn("Cannot find corner NPC " + t.getCornerNpcId(), new IllegalArgumentException());
				continue;
			}
			for (Location loc : locs)
			{
				L2Spawn sp = new L2Spawn(temp);
				sp.setLoc(loc);
				sp.setInstanceId(getInstanceId());
				sp.stopRespawn();
				_npcs.add(sp.spawnOne(false));
			}
		}
	}
	
	private void unspawnEventNpcs()
	{
		for (FastSet.Record r = _npcs.head(), end = _npcs.tail(); (r = r.getNext()) != end;)
		{
			L2Npc npc = _npcs.valueOf(r);
			npc.doDie(null);
			npc.deleteMe();
		}
		_npcs.clear();
	}
	
	private AtomicInteger getOffenses(L2PcInstance player)
	{
		AtomicInteger count = _offenders.get(player.getObjectId());
		if (count == null)
		{
			count = new AtomicInteger();
			
			AtomicInteger old = _offenders.putIfAbsent(player.getObjectId(), count);
			if (old != null)
				return old;
		}
		return count;
	}
	
	public void addOffender(L2PcInstance player)
	{
		AtomicInteger count = getOffenses(player);
		if (count.incrementAndGet() == 3)
		{
			_offenders.remove(player.getObjectId());
			remove(player);
			L2PlayerData dat = player.getSoDPlayer();
			dat.setJailTotal(dat.getJailTotal() + 1);
			dat.setJailReason("Event crashing");
			player.setInJail(true, 15);
		}
	}
	
	@Override
	public TeamedEvent<LurerTeam> newSplitEvent()
	{
		LurerTeam t1 = getTeam1();
		LurerTeam t2 = getTeam2();
		return new LureTheGremlin(this, new LurerTeam(t1.getLoc()[0], t1.getColor(), t1.getZone().getQuestZoneId(),
				t1.getCornerNpcId()), new LurerTeam(t2.getLoc()[0], t2.getColor(), t2.getZone().getQuestZoneId(),
				t2.getCornerNpcId()));
	}
	
	@Override
	protected double getCoef(LurerTeam team)
	{
		double result = 0;
		for (FastSet.Record r = team.getMembers().head(), end = team.getMembers().tail(); (r = r.getNext()) != end;)
		{
			L2PcInstance player = team.getMembers().valueOf(r);
			double temp = player.getLevel() - Config.STARTING_LEVEL + 1;
			ClassId prof = player.getClassId();
			if (prof.isSummoner())
				temp *= TeamVersusTeam.SUMMONER_LEVEL_MULTIPLIER;
			else if (prof.equalsOrChildOf(ClassId.AbyssWalker) || prof.equalsOrChildOf(ClassId.Plainswalker)
					|| prof.equalsOrChildOf(ClassId.TreasureHunter))
				temp *= TeamVersusTeam.DAGGER_LEVEL_MULTIPLIER;
			else if (prof.equalsOrChildOf(ClassId.Tyrant))
				temp *= TeamVersusTeam.TYRANT_LEVEL_MULTIPLIER;
			else if (prof.equalsOrChildOf(ClassId.Arbalester) || prof.equalsOrChildOf(ClassId.Hawkeye)
					|| prof.equalsOrChildOf(ClassId.PhantomRanger) || prof.equalsOrChildOf(ClassId.SilverRanger))
				temp *= TeamVersusTeam.ARCHER_LEVEL_MULTIPLIER;
			else if (prof.equalsOrChildOf(ClassId.Bishop) || prof.equalsOrChildOf(ClassId.ElvenElder)
					|| prof.equalsOrChildOf(ClassId.ShillienElder))
				temp *= TeamVersusTeam.HEALER_LEVEL_MULTIPLIER;
			else if (prof.isMage())
				temp *= TeamVersusTeam.MAGE_LEVEL_MULTIPLIER;
			result += temp;
			/*
			ClassId prof = player.getClassId();
			if (prof.equalsOrChildOf(ClassId.HumanKnight) ||
					prof.equalsOrChildOf(ClassId.ElvenKnight) ||
					prof.equalsOrChildOf(ClassId.ShillienKnight))
				result += AGGRO_POINTS;
			else if (prof.equalsOrChildOf(ClassId.Arbalester) ||
					prof.equalsOrChildOf(ClassId.TreasureHunter) ||
					prof.equalsOrChildOf(ClassId.AbyssWalker) ||
					prof.equalsOrChildOf(ClassId.Plainswalker))
				result += LURING_POINTS;
			else if (player.getRace() == Race.Kamael ||
					prof.equalsOrChildOf(ClassId.Sorceror) ||
					prof.equalsOrChildOf(ClassId.Necromancer) ||
					prof.equalsOrChildOf(ClassId.Spellsinger) ||
					prof.equalsOrChildOf(ClassId.Spellhowler) ||
					prof.equalsOrChildOf(ClassId.OrcFighter))
				result += DPS_POINTS;
			else if (prof.equalsOrChildOf(ClassId.Warlord) ||
					prof.equalsOrChildOf(ClassId.Gladiator) ||
					prof.equalsOrChildOf(ClassId.Swordsinger) ||
					prof.equalsOrChildOf(ClassId.Bladedancer) ||
					prof.equalsOrChildOf(ClassId.OrcMystic))
				result += LOW_AGGRO_POINTS;
			else
				result += 1;
			 */
		}
		return result;
	}
	
	@Override
	public String getImplementationName()
	{
		return NAME;
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getStartProcedure()
	{
		return START;
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getMaintenanceProcedure()
	{
		return MAINTENANCE;
	}
	
	@Override
	public SplitProcedure[] getSplitProcedure()
	{
		return SPLIT;
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getEndingProcedure()
	{
		return END;
	}
	
	@Override
	public void doBeforeStart()
	{
		super.doBeforeStart();
		spawnZoneMarkers();
		spawnGremlin();
	}
	
	@Override
	public void doAfterEnd()
	{
		unspawnEventNpcs();
		
		final FastSet<Integer> winners = FastSet.newInstance();
		broadcastTeamScores(new TeamRewardProcedure<LureTheGremlin.LurerTeam>()
		{
			@Override
			public void rewardMember(L2PcInstance member, LurerTeam t)
			{
				if (t == getWinner())
				{
					rewardAdena(member, 25);
					rewardChallenge(member.getSoDPlayer(), ChallengeTemplate.TEMPTATION, 1);
					winners.add(member.getObjectId());
				}
			}
		});
		if (!GlobalEventManager.getInstance().setWinners(winners, this))
			FastSet.recycle(winners);
		
		super.doAfterEnd();
	}
	
	public static final class LurerTeam extends Team
	{
		private final L2Zone _zone;
		private final int _cornerNpcId;
		
		public LurerTeam(Location loc, int color, int zoneId, int cornerNpcId)
		{
			super(color, loc);
			_zone = ZoneManager.getInstance().getZoneById(zoneId);
			if (_zone == null)
				_log.warn("Non-existent unique zone " + zoneId, new IllegalArgumentException());
			_cornerNpcId = cornerNpcId;
		}
		
		public L2Zone getZone()
		{
			return _zone;
		}
		
		public int getCornerNpcId()
		{
			return _cornerNpcId;
		}
		
		public void removePoint()
		{
			_points.decrementAndGet();
		}
	}
}
