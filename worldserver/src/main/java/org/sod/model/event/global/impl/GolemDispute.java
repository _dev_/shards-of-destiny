/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.event.global.impl;

import javolution.util.FastSet;

import org.sod.manager.EventFenceManager;
import org.sod.manager.GlobalEventManager;
import org.sod.model.event.TeamVersusTeam;
import org.sod.model.event.global.EventProcedure;
import org.sod.model.event.global.GlobalEvent;
import org.sod.model.event.global.ProcInfoMessage;
import org.sod.model.event.global.SplitProcedure;
import org.sod.model.event.global.Team;
import org.sod.model.event.global.TeamRewardProcedure;
import org.sod.model.event.global.TeamedEvent;
import org.sod.model.event.global.impl.GolemDispute.DisputeTeam;
import org.sod.model.event.global.restriction.GolemDisputeRestriction;

import com.l2jfree.Config;
import com.l2jfree.gameserver.model.L2ItemInstance;
import com.l2jfree.gameserver.model.L2Spawn;
import com.l2jfree.gameserver.model.Location;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.base.ClassId;
import com.l2jfree.gameserver.model.itemcontainer.Inventory;
import com.l2jfree.gameserver.network.client.packets.sendable.UserInfo.MyPlayerInfo;
import com.l2jfree.gameserver.skills.SpecialEffect;

/**
 * Goblin Dispute event, originally from L2Hunter.
 * 
 * @author savormix
 * @since 2010.10.23
 */
public class GolemDispute extends TeamedEvent<DisputeTeam>
{
	private static final String NAME = "Golden Golem";
	private static final int FENCED_AREA_TI = 1;
	public static final int GOLDEN_GOLEM_ITEM = 91002;
	public static final int GOLDEN_GOLEM_NPC = 90066;
	public static final int GOLEM_PURPLE = 90067;
	public static final int GOLEM_ORANGE = 90068;
	public static final int COLOR_PURPLE = 0x9900FF;
	public static final int COLOR_ORANGE = 0xFF9900;
	public static final int TEAMED_GOLEM_POINTS = 5;
	static
	{
		GolemDisputeRestriction.getInstance().activate();
	}
	@SuppressWarnings("unchecked")
	private static final EventProcedure<GlobalEvent>[] START = new EventProcedure[]
	{
			PROC_LEAVE_PARTY, PROC_COLOR, PROC_MOVE_TO_TEAM, PROC_RECOVERY,
			new ProcInfoMessage("EVENT_GD_INSTRUCTIONS")
	};
	@SuppressWarnings("unchecked")
	private static final EventProcedure<TeamedEvent<Team>>[] MAINTENANCE = new EventProcedure[]
	{
		new EventProcedure<TeamedEvent<Team>>()
		{
			@Override
			public void performOn(L2PcInstance member, TeamedEvent<Team> event)
			{
				if (member.isDead())
				{
					PROC_ADD_PENDING_REVIVE.performOn(member, event);
					PROC_MOVE_TO_TEAM.performOn(member, event);
					PROC_RECOVERY.performOn(member, event);
				}
			}
		}
	};
	@SuppressWarnings("unchecked")
	private static final EventProcedure<GlobalEvent>[] END = new EventProcedure[]
	{
			PROC_LEAVE_PARTY, PROC_WASH_COLOR, PROC_RETURN, PROC_RECOVERY
	};
	private/*static*/final SplitProcedure[] SPLIT = new SplitProcedure[]
	{
			PROC_UNSAFE_TRANSFER, PROC_SPLIT_NEW_BALANCED
	};
	
	private final GoldenMember _gm;
	
	/**
	 * Creates a GoblinDispute event.
	 * 
	 * @param maxPoints
	 *            point limit
	 * @param autoRemove
	 *            mark as unused after end
	 * @param t1
	 *            Team 1
	 * @param t2
	 *            Team 2
	 */
	public GolemDispute(int maxPoints, boolean autoRemove, DisputeTeam t1, DisputeTeam t2, int goldenNpcId,
			Location goldenNpcLoc)
	{
		super(15000, 20, 0.5, maxPoints, FENCED_AREA_TI, autoRemove, t1, t2);
		_gm = new GoldenMember(goldenNpcId, goldenNpcLoc);
	}
	
	protected GolemDispute(GolemDispute main, DisputeTeam t1, DisputeTeam t2)
	{
		super(main, t1, t2);
		_gm = new GoldenMember(main.getGm().getNpcId(), main.getGm().getNpcLoc());
	}
	
	public GoldenMember getGm()
	{
		return _gm;
	}
	
	public DisputeTeam getTeam(int npcId)
	{
		DisputeTeam t = getTeam1();
		if (t.getGolemId() == npcId)
			return t;
		t = getTeam2();
		if (t.getGolemId() == npcId)
			return t;
		return null;
	}
	
	@Override
	public GolemDispute newSplitEvent()
	{
		DisputeTeam t1 = getTeam1();
		DisputeTeam t2 = getTeam2();
		return new GolemDispute(this,
				new DisputeTeam(t1.getLoc()[0], t1.getColor(), t1.getGolemId(), t1.getGolemLoc()), new DisputeTeam(
						t2.getLoc()[0], t2.getColor(), t2.getGolemId(), t2.getGolemLoc()));
	}
	
	@Override
	protected double getCoef(DisputeTeam team)
	{
		double temp, result = 0;
		for (FastSet.Record r = team.getMembers().head(), end = team.getMembers().tail(); (r = r.getNext()) != end;)
		{
			L2PcInstance player = team.getMembers().valueOf(r);
			temp = player.getLevel() - Config.STARTING_LEVEL + 1;
			ClassId prof = player.getClassId();
			if (prof.isSummoner())
				temp *= TeamVersusTeam.SUMMONER_LEVEL_MULTIPLIER;
			else if (prof.equalsOrChildOf(ClassId.AbyssWalker) || prof.equalsOrChildOf(ClassId.Plainswalker)
					|| prof.equalsOrChildOf(ClassId.TreasureHunter))
				temp *= TeamVersusTeam.DAGGER_LEVEL_MULTIPLIER;
			else if (prof.equalsOrChildOf(ClassId.Tyrant))
				temp *= TeamVersusTeam.TYRANT_LEVEL_MULTIPLIER;
			else if (prof.equalsOrChildOf(ClassId.Arbalester) || prof.equalsOrChildOf(ClassId.Hawkeye)
					|| prof.equalsOrChildOf(ClassId.PhantomRanger) || prof.equalsOrChildOf(ClassId.SilverRanger))
				temp *= TeamVersusTeam.ARCHER_LEVEL_MULTIPLIER;
			else if (prof.equalsOrChildOf(ClassId.Bishop) || prof.equalsOrChildOf(ClassId.ElvenElder)
					|| prof.equalsOrChildOf(ClassId.ShillienElder))
				temp *= TeamVersusTeam.HEALER_LEVEL_MULTIPLIER;
			else if (prof.isMage())
				temp *= TeamVersusTeam.MAGE_LEVEL_MULTIPLIER;
			result += temp;
		}
		return result;
	}
	
	@Override
	public String getImplementationName()
	{
		return NAME;
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getStartProcedure()
	{
		return START;
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getMaintenanceProcedure()
	{
		return MAINTENANCE;
	}
	
	@Override
	public SplitProcedure[] getSplitProcedure()
	{
		return SPLIT;
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getEndingProcedure()
	{
		return END;
	}
	
	@Override
	public void doBeforeStart()
	{
		super.doBeforeStart();
		getGm().spawnNpc(getInstanceId());
	}
	
	@Override
	public void doBeforeEnd()
	{
		super.doBeforeEnd();
		
		L2PcInstance carrier = getGm().getCarrier();
		if (carrier != null)
		{
			L2ItemInstance wpn = carrier.getInventory().getPaperdollItem(Inventory.PAPERDOLL_RHAND);
			if (wpn != null && wpn.getItemId() == GolemDispute.GOLDEN_GOLEM_ITEM)
			{
				carrier.useEquippableItem(wpn, true);
				carrier.sendPacket(MyPlayerInfo.PACKET);
				if (carrier.destroyItem(getImplementationName(), wpn, null, true))
				{
					if (carrier.getTransformationId() == 116)
						carrier.untransform();
					carrier.stopSpecialEffect(SpecialEffect.S_AIR_ROOT);
				}
			}
		}
	}
	
	@Override
	public void doAfterEnd()
	{
		getGm().unspawnNpc(null);
		
		final FastSet<Integer> winners = FastSet.newInstance();
		broadcastTeamScores(new TeamRewardProcedure<GolemDispute.DisputeTeam>()
		{
			@Override
			public void rewardMember(L2PcInstance member, DisputeTeam t)
			{
				if (t == getWinner())
				{
					rewardAdena(member, 25);
					winners.add(member.getObjectId());
				}
			}
		});
		if (!GlobalEventManager.getInstance().setWinners(winners, this))
			FastSet.recycle(winners);
		
		super.doAfterEnd();
	}
	
	public static final class GoldenMember
	{
		private final int _npcId;
		private final Location _npcLoc;
		private final L2Spawn _npcSpawn;
		private L2PcInstance _carrier;
		
		private GoldenMember(int npcId, Location loc)
		{
			_npcId = npcId;
			_npcLoc = loc;
			_npcSpawn = new L2Spawn(npcId);
			_npcSpawn.setLoc(loc);
			_carrier = null;
		}
		
		/** @return the golden NPC template's ID */
		public int getNpcId()
		{
			return _npcId;
		}
		
		/** @return the NPC's location */
		public Location getNpcLoc()
		{
			return _npcLoc;
		}
		
		/** @return the NPC's <CODE>L2Spawn</CODE> object */
		public L2Spawn getNpcSpawn()
		{
			return _npcSpawn;
		}
		
		/**
		 * Spawns the NPC in a given instance.
		 * 
		 * @param instanceId
		 *            Event's instance
		 */
		public void spawnNpc(int instanceId)
		{
			if (_npcSpawn == null)
				return;
			_npcSpawn.setInstanceId(instanceId);
			setCarrier(null);
			GolemDisputeRestriction.getInstance().addGolden(_npcSpawn.doSpawn());
		}
		
		/** Removes the NPC */
		public void unspawnNpc(L2PcInstance killer)
		{
			if (_npcSpawn == null || _npcSpawn.getLastSpawn() == null)
				return;
			L2Npc golem = _npcSpawn.getLastSpawn();
			golem.deleteMe();
			GolemDisputeRestriction.getInstance().removeGolden(golem);
			setCarrier(killer);
		}
		
		public L2PcInstance getCarrier()
		{
			return _carrier;
		}
		
		private void setCarrier(L2PcInstance player)
		{
			_carrier = player;
		}
	}
	
	public static final class DisputeTeam extends Team
	{
		private final int _golemId;
		private final Location _golemLoc;
		private final L2Spawn _golemSpawn;
		
		public DisputeTeam(Location loc, int color, int golemId, Location golemLoc)
		{
			super(color, loc);
			_golemId = golemId;
			_golemLoc = golemLoc;
			_golemSpawn = new L2Spawn(golemId);
			_golemSpawn.setLoc(golemLoc);
		}
		
		public int getGolemId()
		{
			return _golemId;
		}
		
		public Location getGolemLoc()
		{
			return _golemLoc;
		}
		
		public L2Spawn getGolemSpawn()
		{
			return _golemSpawn;
		}
		
		/**
		 * Spawns the NPC in a given instance.
		 * 
		 * @param instanceId
		 *            Event's instance
		 */
		public void spawnGolem(int instanceId)
		{
			if (_golemSpawn == null)
				return;
			_golemSpawn.setInstanceId(instanceId);
			EventFenceManager.getInstance().addCharacterToArea(FENCED_AREA_TI, _golemSpawn.doSpawn());
		}
		
		/** Removes the NPC */
		public void unspawnGolem()
		{
			if (_golemSpawn == null || _golemSpawn.getLastSpawn() == null)
				return;
			L2Npc golem = _golemSpawn.getLastSpawn();
			golem.deleteMe();
			EventFenceManager.getInstance().removeCharacterFromArea(FENCED_AREA_TI, golem);
		}
	}
}
