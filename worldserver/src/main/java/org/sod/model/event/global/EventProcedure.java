/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.event.global;

import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;

/**
 * Represents a general task declaration.<BR>
 * <BR>
 * To generalize events and to reduce loop count, one should declare tasks to be
 * performed on each participant by implementing this interface and overriding a
 * specific get*Procedure() method.<BR>
 * Although this method will be called for each participant, there is no reason why
 * you cannot just check, for example, if a participant is alive and return without
 * doing anything.
 * 
 * @see GlobalEvent#getEndingProcedure()
 * @see GlobalEvent#getMaintenanceProcedure()
 * @see GlobalEvent#getStartProcedure()
 * @author savormix
 * @since 2010.03.27
 */
public interface EventProcedure<T extends GlobalEvent>
{
	/**
	 * This method should alter the given player instance if needed criteria are met.
	 * 
	 * @param member
	 *            an event participant
	 */
	public abstract void performOn(L2PcInstance member, T event);
}
