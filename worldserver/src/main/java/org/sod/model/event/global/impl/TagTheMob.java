/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.event.global.impl;

import java.util.ArrayList;
import java.util.List;

import javolution.util.FastMap;
import javolution.util.FastSet;

import org.sod.Calc;
import org.sod.i18n.Internationalization;
import org.sod.model.ChallengeTemplate;
import org.sod.manager.EventFenceManager;
import org.sod.model.event.global.EventProcedure;
import org.sod.model.event.global.GlobalEvent;
import org.sod.model.event.global.SplitProcedure;
import org.sod.model.event.global.restriction.TagTheMobRestriction;

import com.l2jfree.gameserver.datatables.ItemTable;
import com.l2jfree.gameserver.model.L2ItemInstance;
import com.l2jfree.gameserver.model.L2Spawn;
import com.l2jfree.gameserver.model.Location;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.itemcontainer.Inventory;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.Chat;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.ChatMessage;
import com.l2jfree.gameserver.network.client.packets.sendable.PlaySoundPacket.PlaySound;
import com.l2jfree.gameserver.network.client.packets.sendable.PlaySoundPacket.SoundType;
import com.l2jfree.gameserver.network.client.packets.sendable.UserInfo.MyPlayerInfo;
import com.l2jfree.util.Rnd;

/**
 * Tag the monsters event.
 * 
 * @author savormix
 * @since 2010.12.12
 */
@SuppressWarnings("unchecked")
public class TagTheMob extends GlobalEvent
{
	private static final String NAME = "Tag the Mob";
	private static final int FENCED_AREA_ELMORE = 3;
	public static final int TAG_WEAPON_ITEM = 91003;
	private static final Location SPAWN_PLAYER = new Location(178012, -211792, -3600);
	private static final Location SPAWN_MONSTER = new Location(178012, -211792, -3600);
	private static final int MONSTER_SPAWN_RADIUS = 600;
	public static final int[] MONSTERS = new int[14];
	static
	{
		for (int i = 0; i < MONSTERS.length; i++)
			MONSTERS[i] = 90076 + i;
		TagTheMobRestriction.getInstance().activate();
	}
	private static final EventProcedure<GlobalEvent>[] START = new EventProcedure[]
	{
			PROC_LEAVE_PARTY, new EventProcedure<GlobalEvent>()
			{
				@Override
				public void performOn(L2PcInstance member, GlobalEvent event)
				{
					member.teleToLocation(SPAWN_PLAYER, true);
				}
			}, PROC_RECOVERY, new EventProcedure<GlobalEvent>()
			{
				@Override
				public void performOn(L2PcInstance member, GlobalEvent event)
				{
					L2ItemInstance stick = ItemTable.getInstance().createItem(NAME, TAG_WEAPON_ITEM, 1, member);
					member.addItem(NAME, stick, null, true);
					member.useEquippableItem(stick, true);
					member.sendPacket(MyPlayerInfo.PACKET);
				}
			}
	};
	private static final EventProcedure<TagTheMob>[] END = new EventProcedure[]
	{
			new EventProcedure<GlobalEvent>()
			{
				@Override
				public void performOn(L2PcInstance member, GlobalEvent event)
				{
					L2ItemInstance wpn = member.getInventory().getPaperdollItem(Inventory.PAPERDOLL_RHAND);
					if (wpn != null && wpn.getItemId() == TAG_WEAPON_ITEM)
					{
						member.useEquippableItem(wpn, true);
						member.sendPacket(MyPlayerInfo.PACKET);
						member.destroyItem(NAME, wpn, null, true);
					}
				}
			}, PROC_LEAVE_PARTY, PROC_RETURN, PROC_RECOVERY, new EventProcedure<TagTheMob>()
			{
				@Override
				public void performOn(L2PcInstance member, TagTheMob event)
				{
					FastSet<Integer> mobs = event.getTags().get(member.getObjectId());
					int size = 0;
					if (mobs != null)
						size = mobs.size();
					member.getSoDPlayer().sendLocalizedMessage("EVENT_TTM_YOU_TAGGED_MONSTERS", "%tagged%",
							String.valueOf(size), "%total%", String.valueOf(event.getMaxPoints()));
				}
			}
	};
	private/*static*/final SplitProcedure[] SPLIT = new SplitProcedure[]
	{
		PROC_UNSAFE_TRANSFER
	};
	
	private final FastMap<Integer, FastSet<Integer>> _tags;
	private final List<L2Npc> _mobs;
	private L2PcInstance _winner;
	
	/**
	 * Creates a Tag the Mob event.
	 * 
	 * @param maxPoints
	 *            point limit
	 * @param autoRelease
	 *            mark as unused after end
	 */
	public TagTheMob(int maxPoints, boolean autoRelease)
	{
		// never split
		super(0, Integer.MAX_VALUE, 1, maxPoints, FENCED_AREA_ELMORE, autoRelease);
		_tags = new FastMap<Integer, FastSet<Integer>>().setShared(true);
		_mobs = new ArrayList<L2Npc>(getMaxPoints());
	}
	
	public FastMap<Integer, FastSet<Integer>> getTags()
	{
		TagTheMob main = getMainEvent();
		if (main == null)
			return _tags;
		else
			return main.getTags();
	}
	
	public FastSet<Integer> getTags(Integer tagger)
	{
		FastSet<Integer> mobs = getTags().get(tagger);
		if (mobs == null)
		{
			mobs = FastSet.newInstance();
			FastSet<Integer> old = getTags().putIfAbsent(tagger, mobs);
			if (old != null)
			{
				FastSet.recycle(mobs);
				return old;
			}
		}
		return mobs;
	}
	
	public void mobTagged(L2Npc npc, L2PcInstance tagger)
	{
		FastSet<Integer> mobs = getTags(tagger.getObjectId());
		if (mobs.add(npc.getObjectId()))
		{
			ChatMessage cs = new ChatMessage(Chat.PRIVATE, getImplementationName(), Internationalization.getInstance()
					.get(tagger.getSoDPlayer().getSettings().getLocale(), "EVENT_TTM_STATUS_UPDATE", "%tagged%",
							String.valueOf(mobs.size()), "%total%", String.valueOf(getMaxPoints())));
			tagger.sendPacket(cs);
			isOver(mobs.size());
		}
	}
	
	@Override
	public void doBeforeStart()
	{
		super.doBeforeStart();
		
		L2Spawn sp = new L2Spawn(Calc.getRandomFromArray(MONSTERS));
		sp.setInstanceId(getInstanceId());
		for (int i = 0; i < getMaxPoints(); i++)
		{
			int signX = (Rnd.nextInt(2) == 0) ? -1 : 1;
			int signY = (Rnd.nextInt(2) == 0) ? -1 : 1;
			int randX = Rnd.nextInt(MONSTER_SPAWN_RADIUS);
			int randY = Rnd.nextInt(MONSTER_SPAWN_RADIUS);
			sp.setLocx(SPAWN_MONSTER.getX() + signX * randX);
			sp.setLocy(SPAWN_MONSTER.getY() + signY * randY);
			sp.setLocz(SPAWN_MONSTER.getZ());
			sp.setHeading(Rnd.get(1 << 16));
			sp.stopRespawn();
			L2Npc npc = sp.spawnOne(false);
			_mobs.add(npc);
			// should not leave the fenced area
			EventFenceManager.getInstance().addCharacterToArea(getFencedArea(), npc);
			// should not fight back
			npc.disableCoreAI(true);
			// should not pick the same "random" spot and gang there
			npc.setIsNoRndWalk(true);
		}
	}
	
	@Override
	protected void doBeforeEnd()
	{
		for (L2Npc npc : _mobs)
		{
			npc.doDie(null);
			npc.deleteMe();
			EventFenceManager.getInstance().removeCharacterFromArea(getFencedArea(), npc);
		}
		
		super.doBeforeEnd();
		
		int maxTags = 1;
		for (FastSet.Record r = getAllPlayers().head(), end = getAllPlayers().tail(); (r = r.getNext()) != end;)
		{
			L2PcInstance participant = getAllPlayers().valueOf(r);
			FastSet<Integer> tags = getTags().get(participant.getObjectId());
			if (tags == null)
				continue;
			
			if (tags.size() > maxTags)
			{
				maxTags = tags.size();
				_winner = participant;
			}
		}
		
		if (_winner != null)
		{
			if (maxTags == getMaxPoints())
				broadcastMessageToParticipants(Chat.ALLIANCE, "EVENT_TTM_TAGGED_ALL", "%name%", _winner.getName());
			else
				broadcastMessageToParticipants(Chat.ALLIANCE, "EVENT_TTM_TAGGED_MOST", "%name%", _winner.getName(),
						"%tagged%", String.valueOf(maxTags));
		}
	}
	
	@Override
	public void doAfterEnd()
	{
		super.doAfterEnd();
		
		if (_tags != null)
		{
			for (FastMap.Entry<Integer, FastSet<Integer>> e = _tags.head(), end = _tags.tail(); (e = e.getNext()) != end;)
				FastSet.recycle(e.getValue());
			_tags.clear();
		}
		
		if (_winner != null)
		{
			rewardItem(_winner, GlobalEvent.EVENT_MEDAL, 2);
			rewardAdena(_winner, 15);
			_winner.sendPacket(new PlaySound(SoundType.MUSIC, "Rm01_S"));
			rewardChallenge(_winner.getSoDPlayer(), ChallengeTemplate.TAG_AND_FRAG, 1);
			_winner = null;
		}
	}
	
	@Override
	public TagTheMob getMainEvent()
	{
		return (TagTheMob) super.getMainEvent();
	}
	
	@Override
	public String getImplementationName()
	{
		return NAME;
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getStartProcedure()
	{
		return START;
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getMaintenanceProcedure()
	{
		return PROC_NONE;
	}
	
	@Override
	public SplitProcedure[] getSplitProcedure()
	{
		return SPLIT;
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getEndingProcedure()
	{
		return END;
	}
	
	@Override
	public GlobalEvent newSplitEvent()
	{
		return new TagTheMob(getMaxPoints(), true);
	}
}
