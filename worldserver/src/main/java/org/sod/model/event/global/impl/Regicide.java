/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.event.global.impl;

import javolution.util.FastSet;

import org.sod.model.ChallengeTemplate;
import org.sod.manager.GlobalEventManager;
import org.sod.model.L2PlayerData;
import org.sod.model.event.global.EventProcedure;
import org.sod.model.event.global.GlobalEvent;
import org.sod.model.event.global.SplitProcedure;
import org.sod.model.event.global.Team;
import org.sod.model.event.global.TeamedEvent;
import org.sod.model.event.global.restriction.RegicideRestriction;

import com.l2jfree.gameserver.model.L2Spawn;
import com.l2jfree.gameserver.model.Location;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.L2Playable;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.zone.L2GlobalEventZone;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.Chat;

/**
 * Regicide/Zergling rush.
 * This event is not split as the whole Aden town is dedicated to it.
 * 
 * @author savormix
 * @since 2010.12.14
 */
public class Regicide extends TeamedEvent<Team>
{
	public static final int KING_NPC = 90090;
	public static final int ADEN_TOWN = 90003;
	public static final int ADEN_TEMPLE = 90004;
	public static final Location[] ADEN_TEMPLE_SPAWN = new Location[]
	{
			new Location(147450, 23720, -1990), new Location(146995, 23745, -1990), new Location(147895, 23752, -1990),
	};
	public static final Location[] ADEN_TOWN_SPAWN = new Location[]
	{
			new Location(147455, 30445, -2460), new Location(150510, 27490, -2265), new Location(144715, 27690, -2265),
			new Location(147465, 20840, -2115),
	};
	
	private static final String NAME = "Regicide";
	private static final int GUARD_SKILL = 9124;
	private static final int ZERG_SKILL = 9125;
	private static final int GUARD_PLAYER = 8;
	private static final int ZERG_PLAYER = 1;
	static
	{
		RegicideRestriction.getInstance().activate();
	}
	@SuppressWarnings("unchecked")
	private static final EventProcedure<TeamedEvent<Team>>[] START = new EventProcedure[]
	{
			PROC_LEAVE_PARTY, PROC_COLOR, PROC_MOVE_TO_TEAM, PROC_RECOVERY
	};
	@SuppressWarnings("unchecked")
	private static final EventProcedure<GlobalEvent>[] END = new EventProcedure[]
	{
			PROC_LEAVE_PARTY, PROC_WASH_COLOR, PROC_RETURN, PROC_RECOVERY
	};
	
	private final int _eventZoneId;
	private final int _guardZoneId;
	private L2PcInstance _murderer;
	
	public Regicide(boolean autoRemove, GuardianTeam t1, ZergTeam t2, int eventZoneId, int guardZoneId)
	{
		super(0, Integer.MAX_VALUE, 1, 1, -1, autoRemove, t1, t2);
		_eventZoneId = eventZoneId;
		_guardZoneId = guardZoneId;
		registerEventZone(_eventZoneId);
		registerEventZone(_guardZoneId);
	}
	
	public Regicide(Regicide main, GuardianTeam t1, ZergTeam t2)
	{
		super(main, t1, t2);
		_eventZoneId = main.getZoneId(false);
		_guardZoneId = main.getZoneId(true);
		registerEventZone(_eventZoneId);
		registerEventZone(_guardZoneId);
	}
	
	public int getZoneId(boolean guard)
	{
		if (guard)
			return _guardZoneId;
		else
			return _eventZoneId;
	}
	
	public GuardianTeam getGuardTeam()
	{
		return (GuardianTeam) getTeam1();
	}
	
	public void onRegicide(L2PcInstance killer)
	{
		_murderer = killer;
		onFinish();
	}
	
	@Override
	public void zoneStateChanged(L2GlobalEventZone zone, L2Playable walker, L2PcInstance participant, boolean inside)
	{
		if (zone.getQuestZoneId() == getZoneId(false))
		{
			if (inside)
			{
				if (!getGuardTeam().getMembers().contains(participant))
					walker.addSkill(ZERG_SKILL, 1);
			}
			else
			{
				if (!getGuardTeam().getMembers().contains(participant))
					walker.removeSkill(ZERG_SKILL);
				if (participant == walker)
					PROC_MOVE_TO_TEAM.performOn(participant, this);
			}
		}
		else
		{
			if (!getGuardTeam().getMembers().contains(participant))
				return;
			if (inside)
				walker.addSkill(GUARD_SKILL, 1);
			else
				walker.removeSkill(GUARD_SKILL);
		}
	}
	
	@Override
	public Regicide newSplitEvent()
	{
		GuardianTeam gt = getGuardTeam();
		GuardianTeam t1 = new GuardianTeam(gt.getKingId(), gt.getKingLoc(), gt.getLoc());
		ZergTeam t2 = new ZergTeam(getTeam2().getLoc());
		return new Regicide(this, t1, t2);
	}
	
	@Override
	protected double getCoef(Team team)
	{
		double c = team.getMembers().size();
		final double mul;
		if (team == getGuardTeam())
			mul = GUARD_PLAYER;
		else
			mul = ZERG_PLAYER;
		return c * mul;
	}
	
	@Override
	protected void doBeforeStart()
	{
		super.doBeforeStart();
		
		getGuardTeam().spawnKing(getInstanceId());
		broadcastMessageToTeam(getTeam2(), Chat.PRIVATE, "EVENT_REGICIDE_INSTRUCTIONS");
	}
	
	@Override
	protected void doBeforeEnd()
	{
		super.doBeforeEnd();
		getGuardTeam().unspawnKing();
	}
	
	@Override
	protected void doAfterEnd()
	{
		FastSet<Integer> winners = FastSet.newInstance();
		if (_murderer != null)
		{
			for (FastSet.Record r = getTeam2().getMembers().head(), end = getTeam2().getMembers().tail(); (r = r
					.getNext()) != end;)
			{
				L2PcInstance member = getTeam2().getMembers().valueOf(r);
				if (member != _murderer)
					rewardAdena(member, 15);
				winners.add(member.getObjectId());
			}
			rewardItem(_murderer, EVENT_MEDAL, 5);
			rewardAdena(_murderer, 30);
			L2PlayerData dat = _murderer.getSoDPlayer();
			dat.sendLocalizedMessage("EVENT_REGICIDE_KING_IS_DEAD");
			rewardChallenge(dat, ChallengeTemplate.ZERGLING_RUSH, 1);
			_murderer = null;
		}
		else
		{
			for (FastSet.Record r = getTeam1().getMembers().head(), end = getTeam1().getMembers().tail(); (r = r
					.getNext()) != end;)
			{
				L2PcInstance member = getTeam1().getMembers().valueOf(r);
				rewardAdena(member, 25);
				rewardChallenge(member.getSoDPlayer(), ChallengeTemplate.ANGEL, 1);
			}
		}
		if (!GlobalEventManager.getInstance().setWinners(winners, this))
			FastSet.recycle(winners);
		
		super.doAfterEnd();
	}
	
	@Override
	public String getImplementationName()
	{
		return NAME;
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getStartProcedure()
	{
		return START;
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getMaintenanceProcedure()
	{
		return PROC_NONE;
	}
	
	@Override
	public SplitProcedure[] getSplitProcedure()
	{
		return null;
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getEndingProcedure()
	{
		return END;
	}
	
	public static class GuardianTeam extends Team
	{
		private final int _kingId;
		private final Location _kingLoc;
		private final L2Spawn _kingSpawn;
		
		public GuardianTeam(int kingId, Location kingLoc, Location... loc)
		{
			super(loc);
			_kingId = kingId;
			_kingLoc = kingLoc;
			_kingSpawn = new L2Spawn(kingId);
			_kingSpawn.setLoc(kingLoc);
		}
		
		public void spawnKing(int instanceId)
		{
			_kingSpawn.setInstanceId(instanceId);
			_kingSpawn.doSpawn();
		}
		
		public void unspawnKing()
		{
			L2Npc king = _kingSpawn.getLastSpawn();
			if (king != null)
				king.deleteMe();
		}
		
		public int getKingId()
		{
			return _kingId;
		}
		
		public Location getKingLoc()
		{
			return _kingLoc;
		}
	}
	
	public static class ZergTeam extends Team
	{
		public ZergTeam(Location... loc)
		{
			super(loc);
		}
	}
}
