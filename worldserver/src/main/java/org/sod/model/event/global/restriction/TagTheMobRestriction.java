/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.event.global.restriction;

import java.util.Arrays;

import org.sod.model.event.global.GlobalEvent;
import org.sod.model.event.global.impl.TagTheMob;

import com.l2jfree.gameserver.handler.IItemHandler;
import com.l2jfree.gameserver.model.L2ItemInstance;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.actor.L2Attackable;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.L2Playable;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.itemcontainer.PcInventory;
import com.l2jfree.gameserver.model.quest.Quest;
import com.l2jfree.gameserver.model.restriction.global.GlobalRestrictions.CombatState;
import com.l2jfree.gameserver.network.SystemMessageId;

/**
 * Restrictions for the Tag the Mob event.
 * 
 * @author savormix
 * @since 2010.12.12
 */
public class TagTheMobRestriction extends GlobalEventRestriction<TagTheMob>
{
	private TagTheMobRestriction()
	{
		new NpcHandler();
	}
	
	@Override
	public Class<TagTheMob> getEventClass()
	{
		return TagTheMob.class;
	}
	
	@Override
	public boolean canInviteToParty(L2PcInstance activeChar, L2PcInstance target)
	{
		GlobalEvent evt1 = GlobalEvent.getEvent(activeChar, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target, getEventClass());
		if (evt1 == null && evt2 == null)
			return true;
		else
			return false;
	}
	
	@Override
	public boolean canRequestRevive(L2PcInstance activeChar)
	{
		if (GlobalEvent.getEvent(activeChar, getEventClass()) != null)
			return false;
		else
			return true;
	}
	
	@Override
	public boolean canTeleport(L2PcInstance activeChar)
	{
		return canRequestRevive(activeChar);
	}
	
	@Override
	public boolean canUseItemHandler(Class<? extends IItemHandler> clazz, int itemId, L2Playable activeChar,
			L2ItemInstance item, L2PcInstance player)
	{
		if (itemId == TagTheMob.TAG_WEAPON_ITEM)
			return false;
		else if (player != null && item != null && item.isWeapon()
				&& player.getInventory().getPaperdollItemId(PcInventory.PAPERDOLL_RHAND) == TagTheMob.TAG_WEAPON_ITEM)
			return false;
		else
			return true;
	}
	
	@Override
	public boolean canDestroyItem(L2PcInstance activeChar, int itemId, L2ItemInstance item)
	{
		if (itemId == TagTheMob.TAG_WEAPON_ITEM && GlobalEvent.getEvent(activeChar, getEventClass()) != null)
		{
			activeChar.sendPacket(SystemMessageId.COPYRIGHT);
			return false;
		}
		return true;
	}
	
	@Override
	public CombatState getCombatState(L2PcInstance activeChar, L2PcInstance target)
	{
		GlobalEvent evt1 = GlobalEvent.getEvent(activeChar, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target, getEventClass());
		if (evt1 != null || evt2 != null)
			if (evt1 == evt2)
				return CombatState.FRIEND;
		
		return CombatState.NEUTRAL;
	}
	
	@Override
	public boolean isProtected(L2Character activeChar, L2Character target, L2Skill skill, boolean sendMessage,
			L2PcInstance attacker_, L2PcInstance target_, boolean isOffensive)
	{
		if (attacker_ == null || attacker_ == target_)
			return false;
		
		GlobalEvent evt1 = GlobalEvent.getEvent(attacker_, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target_, getEventClass());
		if (evt1 != null || evt2 != null)
		{
			if (evt1 == evt2)
				return isOffensive;
			else if (target instanceof L2Npc)
				return Arrays.binarySearch(TagTheMob.MONSTERS, ((L2Npc) target).getNpcId()) < 0;
			else
				return true;
		}
		return false;
	}
	
	private final class NpcHandler extends Quest
	{
		private NpcHandler()
		{
			super(-1, "TagTheMob", "ai");
			for (int i : TagTheMob.MONSTERS)
				addAttackId(i);
		}
		
		@Override
		public String onAttack(L2Npc npc, L2PcInstance attacker, int damage, boolean isPet, L2Skill skill)
		{
			L2Attackable actor = (L2Attackable) npc;
			TagTheMob event = GlobalEvent.getEvent(attacker, getEventClass());
			if (event != null)
				event.mobTagged(actor, attacker);
			return null;
		}
	}
	
	public static TagTheMobRestriction getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		private static final TagTheMobRestriction INSTANCE = new TagTheMobRestriction();
	}
}
