/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.event.global;

import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.atomic.AtomicInteger;

import javolution.util.FastMap;
import javolution.util.FastSet;

import org.sod.manager.EventFenceManager;
import org.sod.manager.GlobalEventManager;
import org.sod.manager.PooledInstanceManager;
import org.sod.manager.RespawnManager;
import org.sod.manager.conquest.ConquestFactionManager;
import org.sod.model.ChallengeTemplate;
import org.sod.model.L2PlayerData;
import org.sod.model.event.global.restriction.GlobalEventRestriction;
import org.sod.tutorial.TutorialManager;

import com.l2jfree.gameserver.ThreadPoolManager;
import com.l2jfree.gameserver.instancemanager.ZoneManager;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.actor.L2Playable;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.actor.status.PcStatus;
import com.l2jfree.gameserver.model.restriction.global.GlobalRestrictions;
import com.l2jfree.gameserver.model.zone.L2GlobalEventZone;
import com.l2jfree.gameserver.model.zone.L2Zone;
import com.l2jfree.gameserver.network.SystemMessageId;
import com.l2jfree.gameserver.network.client.packets.L2ServerPacket;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.Chat;
import com.l2jfree.gameserver.network.client.packets.sendable.SystemMessagePacket.SystemMessage;
import com.l2jfree.gameserver.util.Broadcast;
import com.l2jfree.util.logging.L2Logger;

/**
 * This class represents the base for an event.
 * Originally designed for L2SoD global event system.
 * 
 * @author savormix
 * @since 2010.03.27
 */
@SuppressWarnings(
{
		"unchecked", "rawtypes"
})
public abstract class GlobalEvent
{
	protected static final L2Logger _log = L2Logger.getLogger(GlobalEvent.class);
	
	private static final FastMap<Integer, GlobalEvent> _instances = new FastMap<Integer, GlobalEvent>().setShared(true);
	private static final FastMap<Integer, Integer> _nameColor = new FastMap<Integer, Integer>().setShared(true);
	private static final AtomicInteger _uniquity = new AtomicInteger(Integer.MIN_VALUE);
	
	public static final int EVENT_MEDAL = 6392;
	
	private final int _uniqueId;
	private final int _instanceId;
	private final int _splitLimit;
	private final double _splitRatio;
	private final int _maxPoints;
	private final int _fencedArea;
	private final int _maintenanceInterval;
	private ScheduledFuture<?> _mainTask;
	private final boolean _autoRelease;
	private final FastSet<L2PcInstance> _allPlayers;
	private final Queue<L2PcInstance> _removal;
	private volatile boolean _started;
	private final GlobalEvent _mainEvent;
	
	private GlobalEvent(int maintenanceInterval, int splitLimit, double splitRatio, int maxPoints, int fencedArea,
			boolean autoRelease, GlobalEvent main)
	{
		_uniqueId = _uniquity.getAndIncrement();
		_mainEvent = main;
		_instanceId = PooledInstanceManager.getInstance().createInstance();
		_splitLimit = splitLimit;
		_splitRatio = splitRatio;
		_maxPoints = maxPoints;
		_fencedArea = fencedArea;
		int moved = (int) (getSplitLimit() * getSplitRatio());
		if (_splitLimit > 0 && moved <= 0)
			_log.error(getImplementationName() + ": bad split ratio(" + getSplitRatio() + ")/limit(" + getSplitLimit()
					+ "), no players will be moved!", new IllegalArgumentException());
		_maintenanceInterval = maintenanceInterval;
		_autoRelease = autoRelease;
		_allPlayers = FastSet.newInstance();
		_removal = new ConcurrentLinkedQueue<L2PcInstance>();
		_started = false;
		_log.info("Global event " + getImplementationName() + " initialized. (Temporary: " + autoRelease + ")");
		getEventInstances().put(getEventId(), this);
	}
	
	/**
	 * Creates a global event with given parameters.
	 * 
	 * @param maintenanceInterval
	 *            How often to perform maintenance procedures
	 * @param splitLimit
	 *            Player count when the event is split to two
	 * @param splitRatio
	 *            Ratio defining how many players are moved to the split event
	 * @param autoRelease
	 *            calls {@link #release(GlobalEvent)} in {@link #doAfterEnd()}
	 */
	protected GlobalEvent(int maintenanceInterval, int splitLimit, double splitRatio, int maxPoints, int fencedArea,
			boolean autoRelease)
	{
		this(maintenanceInterval, splitLimit, splitRatio, maxPoints, fencedArea, autoRelease, null);
	}
	
	protected GlobalEvent(GlobalEvent main)
	{
		this(main.getMaintenanceInterval(), main.getSplitLimit(), main.getSplitRatio(), main.getMaxPoints(), main
				.getFencedArea(), true, main);
	}
	
	public static FastMap<Integer, GlobalEvent> getEventInstances()
	{
		return _instances;
	}
	
	/**
	 * Unregister a global event and allow it to be collected by GC.<BR>
	 * The instance is also destroyed.
	 * 
	 * @param ge
	 *            An event that already served it's use
	 */
	public static void release(int eventId)
	{
		GlobalEvent ge = getEventInstances().remove(eventId);
		if (ge != null)
			PooledInstanceManager.getInstance().destroyInstance(ge.getInstanceId());
	}
	
	/**
	 * Check if a player is registered in any event.
	 * 
	 * @param player
	 *            A player
	 * @return is it an event member
	 */
	public static boolean isParticipating(L2PcInstance player)
	{
		return getEvent(player) != null;
	}
	
	/**
	 * Get the event in which a player is registered.
	 * 
	 * @param player
	 *            A player
	 * @return an event or <CODE>null</CODE>
	 */
	public static GlobalEvent getEvent(L2PcInstance player)
	{
		return getEvent(player, GlobalEvent.class);
	}
	
	/**
	 * Get the event in which a player is registered.
	 * 
	 * @param player
	 *            A player
	 * @return an event or <CODE>null</CODE>
	 */
	public static <Event extends GlobalEvent> Event getEvent(L2PcInstance player, Class<Event> type)
	{
		if (player == null)
			return null;
		for (FastMap.Entry<Integer, GlobalEvent> entry = getEventInstances().head(), end = getEventInstances().tail(); (entry = entry
				.getNext()) != end;)
		{
			GlobalEvent ge = entry.getValue();
			if (ge.isPlaying(player))
			{
				if (type.isInstance(ge))
					return type.cast(ge);
				else
					return null;
			}
		}
		return null;
	}
	
	/** @return event has started and hasn't finished */
	public final boolean isStarted()
	{
		return _started;
	}
	
	/** @return the unique event ID */
	public final int getEventId()
	{
		return _uniqueId;
	}
	
	/** @return the instance where the event takes place */
	public final int getInstanceId()
	{
		return _instanceId;
	}
	
	/** @return point count which indicates event should end */
	public final int getMaxPoints()
	{
		return _maxPoints;
	}
	
	public final boolean isOver(int points)
	{
		if (getMaxPoints() < 1)
			return false;
		if (points < getMaxPoints())
			return false;
		
		onFinish();
		return true;
	}
	
	/** @return fenced area ID */
	public final int getFencedArea()
	{
		return _fencedArea;
	}
	
	/** @return how often the maintenance task is done (ms) */
	public final int getMaintenanceInterval()
	{
		return _maintenanceInterval;
	}
	
	/**
	 * Returns the player count, after which is reached, the event
	 * is split to two instances (events).
	 * 
	 * @return the player count limit
	 * @see #getSplitRatio()
	 */
	public final int getSplitLimit()
	{
		return _splitLimit;
	}
	
	/**
	 * Returns the ratio that defines the size of (player count in) the <U>new</U> event.<BR>
	 * If this event is registered in the <CODE>GlobalEventManager</CODE>, players will still be
	 * able to join <U>this</U> event and not the new event.<BR>
	 * <BR>
	 * Example: 0,67 means that approximately 2 / 3 players will be moved to the newly
	 * created event. 1 means that everyone will be moved to the new event.
	 * 
	 * @return the split ratio
	 * @see #getSplitLimit()
	 */
	public final double getSplitRatio()
	{
		return _splitRatio;
	}
	
	/** @return whether to remove this event and it's instance after end */
	public final boolean isAutoRelease()
	{
		return _autoRelease;
	}
	
	/** @return the father event (using recursion) */
	public GlobalEvent getMainEvent()
	{
		if (_mainEvent != null && _mainEvent.getMainEvent() != null)
			return _mainEvent.getMainEvent();
		else
			return _mainEvent;
	}
	
	/** @return all players registered to this event */
	public final FastSet<L2PcInstance> getAllPlayers()
	{
		return _allPlayers;
	}
	
	/**
	 * Tests if this player is registered for/to this event
	 * 
	 * @param player
	 *            A player
	 * @return if player is a participant
	 */
	public final boolean isPlaying(L2PcInstance player)
	{
		if (!isStarted())
			return false;
		else
			return getAllPlayers().contains(player);
	}
	
	/**
	 * Check if player is eligible for participation.
	 * 
	 * @param player
	 *            A player
	 * @return if registration would be successful
	 */
	protected boolean addTest(L2PcInstance player)
	{
		if (player == null || isParticipating(player))
			return false;
		
		// XXX: decide about this
		if (GlobalRestrictions.isRestricted(player, GlobalEventRestriction.class)
				|| !GlobalRestrictions.canTeleport(player))
			return false;
		
		if (ConquestFactionManager.getInstance().isPlaying(player))
		{
			player.getSoDPlayer().sendLocalizedMessage("NPC_EVENT_MANAGER_ALREADY_IN_CONQUEST");
			return false;
		}
		
		return !getAllPlayers().contains(player);
	}
	
	/**
	 * Register a player to this event.<BR>
	 * If the event is already started, will change instance and
	 * perform starting procedures on the player.
	 * 
	 * @param player
	 *            A player
	 * @return if registration is be successful
	 */
	public boolean add(L2PcInstance player)
	{
		if (!addTest(player))
			return false;
		return addDirect(player);
	}
	
	/**
	 * Register a player to this event bypassing all checks.
	 * 
	 * @param player
	 *            A player
	 * @return true, unless the player is already added
	 */
	protected boolean addDirect(L2PcInstance player)
	{
		if (!getAllPlayers().add(player))
			return false;
		
		if (isStarted())
		{
			player.setInstanceId(getInstanceId());
			EventFenceManager.getInstance().addCharacterToArea(getFencedArea(), player);
			for (EventProcedure ep : getStartProcedure())
			{
				try
				{
					ep.performOn(player, this);
				}
				catch (Exception e)
				{
					_log.warn(getImplementationName() + " could not perform join task on " + player, e);
				}
			}
		}
		
		if (getAllPlayers().size() >= getSplitRatio())
		{
			onSplit();
			return getAllPlayers().contains(player);
		}
		return true;
	}
	
	/**
	 * Unregister/Remove a player from this event.<BR>
	 * This will also perform all ending procedures on the player!<BR>
	 * <BR>
	 * <B><FONT COLOR="RED">DO NOT CALL FROM PROCEDURES!</FONT></B>
	 * <I>(or iterations over {@link #getAllPlayers()})</I>
	 * 
	 * @param player
	 *            A player
	 * @return if removal is successful
	 * @see #addPendingRemoval(L2PcInstance)
	 * @see #sweep()
	 */
	public boolean remove(L2PcInstance player)
	{
		if (!getAllPlayers().remove(player))
			return false;
		
		if (isStarted())
		{
			EventFenceManager.getInstance().removeCharacterFromArea(getFencedArea(), player);
			for (EventProcedure ep : getEndingProcedure())
			{
				try
				{
					ep.performOn(player, this);
				}
				catch (Exception e)
				{
					_log.warn(getImplementationName() + " could not perform leave task on " + player, e);
				}
			}
		}
		return true;
	}
	
	/**
	 * Add a player to the sweeper list.
	 * 
	 * @param player
	 *            A participant
	 * @see #sweep()
	 */
	/*protected*/public void addPendingRemoval(L2PcInstance player)
	{
		_removal.add(player);
	}
	
	/**
	 * Oust all players in the sweeper list from the event.<BR>
	 * <BR>
	 * <B><FONT COLOR="RED">DO NOT CALL FROM PROCEDURES!</FONT></B>
	 * <I>(or iterations over {@link #getAllPlayers()})</I>
	 * 
	 * @see #addPendingRemoval(L2PcInstance)
	 */
	protected void sweep()
	{
		L2PcInstance pending;
		while ((pending = _removal.poll()) != null)
			remove(pending);
	}
	
	public void broadcastMessageToParticipants(Chat channel, String key, String... repVals)
	{
		Broadcast.broadcastMessage(getAllPlayers(), channel, getImplementationName(), false, key, repVals);
	}
	
	/**
	 * Send a packet to all event members
	 * 
	 * @param packet
	 *            A server packet
	 */
	public void broadcastToParticipants(L2ServerPacket... packets)
	{
		for (FastSet.Record r = getAllPlayers().head(), end = getAllPlayers().tail(); (r = r.getNext()) != end;)
			for (L2ServerPacket packet : packets)
				getAllPlayers().valueOf(r).sendPacket(packet);
	}
	
	/**
	 * Send a static system message to all event members
	 * 
	 * @param smsg
	 *            A <CODE>SystemMessageId</CODE> enumerated value
	 */
	public final void broadcastToParticipants(SystemMessageId smsg)
	{
		broadcastToParticipants(smsg.getSystemMessage());
	}
	
	/**
	 * Send a message to all event members
	 * 
	 * @param message
	 *            A message
	 */
	public final void broadcastToParticipants(String message)
	{
		broadcastToParticipants(SystemMessage.valueOf(message));
	}
	
	/**
	 * Perform procedures at the beginning of the event.<BR>
	 * Instance is changed automatically.
	 * 
	 * @see #getStartProcedure()
	 */
	public final void onStart()
	{
		_started = true;
		try
		{
			doBeforeStart();
		}
		catch (Exception e)
		{
			_log.error(getImplementationName() + ": pre-starting failure!", e);
		}
		for (FastSet.Record r = getAllPlayers().head(), end = getAllPlayers().tail(); (r = r.getNext()) != end;)
		{
			L2PcInstance player = getAllPlayers().valueOf(r);
			player.setInstanceId(getInstanceId());
			EventFenceManager.getInstance().addCharacterToArea(getFencedArea(), player);
			for (EventProcedure ep : getStartProcedure())
			{
				try
				{
					ep.performOn(player, this);
				}
				catch (Exception e)
				{
					_log.warn(getImplementationName() + " could not perform start task on " + player, e);
				}
			}
		}
		if (_maintenanceInterval > 0)
			_mainTask = ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(new Maintainer(),
					_maintenanceInterval, _maintenanceInterval);
		else
			_mainTask = null;
		try
		{
			doAfterStart();
		}
		catch (Exception e)
		{
			_log.error(getImplementationName() + ": post-starting failure!", e);
		}
	}
	
	/** Method to be called whenever split has to be performed */
	protected final synchronized void onSplit()
	{
		/* Removed, should be revised
		if (getAllPlayers().size() >= getSplitLimit())
			onSplit(newSplitEvent());
		 */
	}
	
	/**
	 * Performs <B>Split Leave</B> procedures of <B>this event</B> and
	 * then <B>Split Enter</B> procedures of the given event.<BR>
	 * If overridden, bear in mind that this method <U>must</U> be
	 * compatible with {@link #PROC_UNSAFE_JOIN} and {@link #PROC_UNSAFE_REMOVAL},
	 * that is an indirect iteration must be used.
	 * 
	 * @param event
	 *            The new event where a part of players must be moved
	 */
	protected void onSplit(GlobalEvent event)
	{
		// TODO: why not use #addPendingRemoval()?
		event.onStart();
		ArrayList<L2PcInstance> list = new ArrayList<L2PcInstance>();
		list.addAll(getAllPlayers());
		int max = (int) (list.size() - list.size() * getSplitRatio());
		for (int i = 0; i < max; i++)
		{
			L2PcInstance player = list.get(i);
			for (SplitProcedure sp : getSplitProcedure())
			{
				try
				{
					sp.performOn(player, event);
				}
				catch (Exception e)
				{
					_log.warn(getImplementationName() + " could not perform split task on " + player, e);
				}
			}
		}
		list.clear();
	}
	
	/**
	 * Perform things at the end of the event.<BR>
	 * Players are moved back to faction home automatically.
	 * 
	 * @see #getEndingProcedure()
	 */
	public final void onFinish()
	{
		if (getMainEvent() != null)
			getMainEvent().onFinish();
		
		if (!isStarted())
			return;
		_started = false;
		if (_mainTask != null)
			_mainTask.cancel(true);
		try
		{
			doBeforeEnd();
		}
		catch (Exception e)
		{
			_log.error(getImplementationName() + ": pre-ending failure!", e);
		}
		for (FastSet.Record r = getAllPlayers().head(), end = getAllPlayers().tail(); (r = r.getNext()) != end;)
		{
			L2PcInstance player = getAllPlayers().valueOf(r);
			EventFenceManager.getInstance().removeCharacterFromArea(getFencedArea(), player);
			for (EventProcedure ep : getEndingProcedure())
			{
				try
				{
					ep.performOn(player, this);
				}
				catch (Exception e)
				{
					_log.warn(getImplementationName() + " could not perform ending task on " + player, e);
				}
			}
		}
		try
		{
			doAfterEnd();
		}
		catch (Exception e)
		{
			_log.error(getImplementationName() + ": post-ending failure!", e);
		}
		GlobalEventManager.getInstance().notifyEventComplete(this);
	}
	
	/** Global things to do before starting procedures are done on participants */
	protected void doBeforeStart()
	{
		GlobalEventManager.getInstance().getWinners().clear();
		EventFenceManager.getInstance().spawnFence(getFencedArea(), getInstanceId());
	}
	
	/** Global things to do after starting procedures are done on participants */
	protected void doAfterStart()
	{
	}
	
	/** Global things to do before maintenance procedures are done on participants */
	protected void doBeforeMaintenance()
	{
	}
	
	/** Global things to do after maintenance procedures are done on participants */
	protected void doAfterMaintenance()
	{
	}
	
	/** Global things to do before ending procedures are done on participants */
	protected void doBeforeEnd()
	{
	}
	
	/** Global things to do after ending procedures are done on participants */
	protected void doAfterEnd()
	{
		EventFenceManager.getInstance().unspawnFence(getFencedArea(), getInstanceId());
		getAllPlayers().clear();
		if (isAutoRelease())
			release(getEventId());
	}
	
	protected void rewardAdena(L2PcInstance player, long count)
	{
		if (TutorialManager.getInstance().isInTutorial(player))
			return;
		
		player.addAdena(getImplementationName(), count, null, true);
	}
	
	protected void rewardItem(L2PcInstance player, int id, long count)
	{
		if (TutorialManager.getInstance().isInTutorial(player))
			return;
		
		player.addItem(getImplementationName(), id, count, null, true);
	}
	
	protected void rewardExpSp(L2PcInstance player, long exp, int sp)
	{
		if (TutorialManager.getInstance().isInTutorial(player))
			return;
		
		player.addExpAndSp(exp, sp);
	}
	
	public void rewardChallenge(L2PlayerData dat, ChallengeTemplate chall, int points)
	{
		if (TutorialManager.getInstance().isInTutorial(dat.getActingPlayer()))
			return;
		
		dat.tryAddChallengePoints(chall, points);
	}
	
	/**
	 * Attempt to register a zone listener for
	 * {@link #zoneStateChanged(L2GlobalEventZone, L2Playable, L2PcInstance, boolean)}.<BR>
	 * Registration is successful if returned zone is an instance of L2GlobalEventZone.
	 * 
	 * @param uniqueId
	 *            Zone ID
	 * @return the zone
	 */
	protected L2Zone registerEventZone(int uniqueId)
	{
		L2Zone zone = ZoneManager.getInstance().getZoneById(uniqueId);
		if (zone == null)
			_log.warn("Non-existent zone with UID: " + uniqueId, new IllegalArgumentException());
		else if (zone instanceof L2GlobalEventZone)
			((L2GlobalEventZone) zone).register(this);
		return zone;
	}
	
	/**
	 * Things to be done when a participant enters/leaves an event zone.
	 * 
	 * @param zone
	 *            A zone
	 * @param walker
	 *            The character that entered/left the zone
	 * @param participant
	 *            Player which either is or controls walker
	 * @param inside
	 *            true if entered, false if left
	 */
	public void zoneStateChanged(L2GlobalEventZone zone, L2Playable walker, L2PcInstance participant, boolean inside)
	{
	}
	
	/**
	 * A method to disallow skill cast. Sending a message is optional.
	 * 
	 * @param character
	 *            the player or his servitor
	 * @param participant
	 *            the player
	 * @param skill
	 *            the skill
	 * @return true to allow, false to disallow
	 */
	public boolean canUseSkill(L2Playable character, L2PcInstance participant, L2Skill skill)
	{
		return true;
	}
	
	private class Maintainer implements Runnable
	{
		@Override
		public void run()
		{
			try
			{
				doBeforeMaintenance();
			}
			catch (Exception e)
			{
				_log.error(getImplementationName() + ": pre-maintenance failure!", e);
			}
			for (FastSet.Record r = getAllPlayers().head(), end = getAllPlayers().tail(); (r = r.getNext()) != end;)
			{
				L2PcInstance player = getAllPlayers().valueOf(r);
				for (EventProcedure ep : getMaintenanceProcedure())
				{
					try
					{
						ep.performOn(player, GlobalEvent.this);
					}
					catch (Exception e)
					{
						_log.warn(getImplementationName() + " could not perform maintenance task on " + player, e);
					}
				}
			}
			try
			{
				doAfterMaintenance();
			}
			catch (Exception e)
			{
				_log.error(getImplementationName() + ": post-maintenance failure!", e);
			}
		}
	}
	
	public static int getNameColor(L2PcInstance player)
	{
		if (player == null)
			return -1;
		Integer color = _nameColor.get(player.getObjectId());
		if (color == null)
			return -1;
		else
			return color.intValue();
	}
	
	public static void setNameColor(L2PcInstance player, int color)
	{
		if (player == null)
			return;
		else if (color == -1)
			_nameColor.remove(player.getObjectId());
		else
			_nameColor.put(player.getObjectId(), color);
		player.broadcastFullInfo();
	}
	
	/**
	 * This method should return the type of this event, the
	 * basic implementation name, which should be shared
	 * between event instances (static).
	 * This name is intended to be used for class-wide error logging,
	 * item add/removal logging, information messages during an event
	 * and to be displayed in the GM event management menu.
	 * 
	 * @return the event name
	 */
	public abstract String getImplementationName();
	
	/**
	 * @return procedures to be done:<br />
	 *         a) on every added player (if event is in progress)<br />
	 *         b) on all players when event starts
	 */
	public abstract EventProcedure<? extends GlobalEvent>[] getStartProcedure();
	
	/** @return procedures to be done on all players (e.g. respawn dead) when maintenance task is executed */
	public abstract EventProcedure<? extends GlobalEvent>[] getMaintenanceProcedure();
	
	/** @return procedures to be done on players which should be moved when event is split */
	public abstract SplitProcedure[] getSplitProcedure();
	
	/**
	 * @return procedures to be done:<br />
	 *         a) on every removed player (if event is in progress)<br />
	 *         b) on all players when event ends
	 */
	public abstract EventProcedure<? extends GlobalEvent>[] getEndingProcedure();
	
	public abstract GlobalEvent newSplitEvent();
	
	/** An empty procedure array. */
	protected static final EventProcedure<GlobalEvent>[] PROC_NONE = new EventProcedure[0];
	
	/** Recovers CP, HP and MP to the maximum values */
	protected static final EventProcedure<GlobalEvent> PROC_RECOVERY = new EventProcedure<GlobalEvent>()
	{
		@Override
		public void performOn(L2PcInstance member, GlobalEvent event)
		{
			PcStatus ps = member.getStatus();
			ps.setCurrentCp(member.getMaxCp());
			ps.setCurrentHpMp(member.getMaxHp(), member.getMaxMp());
		}
	};
	
	/** Prepares a player for revival, if he is dead. */
	protected static final EventProcedure<GlobalEvent> PROC_ADD_PENDING_REVIVE = new EventProcedure<GlobalEvent>()
	{
		@Override
		public void performOn(L2PcInstance member, GlobalEvent event)
		{
			member.setIsPendingRevive(member.isDead());
		}
	};
	
	/** Returns player to the base location, reviving if necessary */
	protected static final EventProcedure<GlobalEvent> PROC_RETURN = new EventProcedure<GlobalEvent>()
	{
		@Override
		public void performOn(L2PcInstance member, GlobalEvent event)
		{
			PROC_ADD_PENDING_REVIVE.performOn(member, event);
			RespawnManager.getInstance().moveToSpawn(member);
		}
	};
	
	/** Forces player to leave party */
	protected static final EventProcedure<GlobalEvent> PROC_LEAVE_PARTY = new EventProcedure<GlobalEvent>()
	{
		@Override
		public void performOn(L2PcInstance member, GlobalEvent event)
		{
			member.leaveParty();
		}
	};
	
	/** A default split procedure */
	protected/*static*/final SplitProcedure PROC_UNSAFE_TRANSFER = new SplitProcedure()
	{
		@Override
		public void performOn(L2PcInstance member, GlobalEvent event)
		{
			member.leaveParty();
			getAllPlayers().remove(member);
			event.getAllPlayers().add(member);
			member.setInstanceId(event.getInstanceId());
		}
	};
}
