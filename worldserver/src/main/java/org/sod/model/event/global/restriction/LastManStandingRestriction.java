/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.event.global.restriction;

import org.sod.model.event.global.GlobalEvent;
import org.sod.model.event.global.impl.LastManStanding;

import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.restriction.global.GlobalRestrictions.CombatState;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.Chat;

/**
 * TODO Auto-generated JavaDoc
 * 
 * @author savormix
 * @since 2010.03.28
 */
public final class LastManStandingRestriction extends GlobalEventRestriction<LastManStanding>
{
	private LastManStandingRestriction()
	{
	}
	
	@Override
	public Class<LastManStanding> getEventClass()
	{
		return LastManStanding.class;
	}
	
	@Override
	public boolean canInviteToParty(L2PcInstance activeChar, L2PcInstance target)
	{
		GlobalEvent evt1 = GlobalEvent.getEvent(activeChar, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target, getEventClass());
		if (evt1 == null && evt2 == null)
			return true;
		else
			return false;
	}
	
	@Override
	public boolean canRequestRevive(L2PcInstance activeChar)
	{
		if (GlobalEvent.getEvent(activeChar, getEventClass()) != null)
			return false;
		else
			return true;
	}
	
	@Override
	public boolean canTeleport(L2PcInstance activeChar)
	{
		return canRequestRevive(activeChar);
	}
	
	@Override
	public CombatState getCombatState(L2PcInstance activeChar, L2PcInstance target)
	{
		GlobalEvent evt1 = GlobalEvent.getEvent(activeChar, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target, getEventClass());
		if (evt1 != null || evt2 != null)
			if (evt1 == evt2)
				return CombatState.ENEMY;
		
		return CombatState.NEUTRAL;
	}
	
	@Override
	public boolean isProtected(L2Character activeChar, L2Character target, L2Skill skill, boolean sendMessage,
			L2PcInstance attacker_, L2PcInstance target_, boolean isOffensive)
	{
		if (attacker_ == null || attacker_ == target_)
			return false;
		
		GlobalEvent evt1 = GlobalEvent.getEvent(attacker_, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target_, getEventClass());
		if (evt1 != null || evt2 != null)
		{
			if (evt1 == evt2)
				return !isOffensive;
			else
				return true;
		}
		return false;
	}
	
	@Override
	public boolean playerKilled(L2Character activeChar, L2PcInstance target, L2PcInstance killer)
	{
		GlobalEvent evt1 = GlobalEvent.getEvent(killer, getEventClass());
		GlobalEvent evt2 = GlobalEvent.getEvent(target, getEventClass());
		if (evt1 != null && evt1 == evt2)
			evt1.broadcastMessageToParticipants(Chat.PARTY, "EVENT_LMS_ELIMINATED", "%victimName%", target.getName(),
					"%killerName%", killer.getName());
		return false;
	}
	
	public static final LastManStandingRestriction getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static final class SingletonHolder
	{
		private static final LastManStandingRestriction INSTANCE = new LastManStandingRestriction();
	}
}
