/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.event.global.impl;

import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

import javolution.util.FastMap;
import javolution.util.FastSet;

import org.sod.i18n.Internationalization;
import org.sod.manager.GlobalEventManager;
import org.sod.model.event.global.EventProcedure;
import org.sod.model.event.global.GlobalEvent;
import org.sod.model.event.global.SplitProcedure;
import org.sod.model.event.global.Team;
import org.sod.model.event.global.TeamRewardProcedure;
import org.sod.model.event.global.TeamedEvent;
import org.sod.model.event.global.impl.CaptureTheFlag.FlagTeam;
import org.sod.model.event.global.restriction.CaptureTheFlagRestriction;

import com.l2jfree.Config;
import com.l2jfree.gameserver.datatables.ItemTable;
import com.l2jfree.gameserver.datatables.NpcTable;
import com.l2jfree.gameserver.model.L2ItemInstance;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.L2Spawn;
import com.l2jfree.gameserver.model.L2World;
import com.l2jfree.gameserver.model.Location;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.L2Playable;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.base.ClassId;
import com.l2jfree.gameserver.model.itemcontainer.PcInventory;
import com.l2jfree.gameserver.network.client.packets.sendable.PlaySoundPacket.PlaySound;
import com.l2jfree.gameserver.network.client.packets.sendable.PlaySoundPacket.SoundType;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;

/**
 * Capture the flag event.
 * 
 * @author savormix
 * @since 2010.03.30
 */
public class CaptureTheFlag extends TeamedEvent<FlagTeam>
{
	public static final int COMBAT_FLAG_ITEM = 91001;
	public static final int COMBAT_FLAG_NPC = 90021;
	
	private static final String NAME = "Capture the Flag";
	
	private static final double TANK_LEVEL_MULTIPLIER = 1.5;
	private static final double HEALER_LEVEL_MULTIPLIER = 1.5;
	private static final double DESTROYER_LEVEL_MULTIPLIER = 1.4;
	private static final double SKNIGHT_LEVEL_MULTIPLIER = 1.25;
	private static final double GLADIATOR_LEVEL_MULTIPLIER = 1.35;
	private static final double MAGE_LEVEL_MULTIPLIER = 1.15;
	private static final double SUMMONER_LEVEL_MULTIPLIER = 0.95;
	static
	{
		CaptureTheFlagRestriction.getInstance().activate();
	}
	
	@SuppressWarnings("unchecked")
	private static final EventProcedure<TeamedEvent<Team>>[] START = new EventProcedure[]
	{
			PROC_LEAVE_PARTY, PROC_COLOR, PROC_MOVE_TO_TEAM, PROC_RECOVERY
	};
	@SuppressWarnings("unchecked")
	protected static final EventProcedure<TeamedEvent<Team>>[] MAINTENANCE = new EventProcedure[]
	{
		new EventProcedure<TeamedEvent<Team>>()
		{
			@Override
			public void performOn(L2PcInstance member, TeamedEvent<Team> event)
			{
				if (member.isDead())
				{
					PROC_ADD_PENDING_REVIVE.performOn(member, event);
					PROC_MOVE_TO_TEAM.performOn(member, event);
					PROC_RECOVERY.performOn(member, event);
				}
			}
		}
	};
	@SuppressWarnings("unchecked")
	private static final EventProcedure<CaptureTheFlag>[] END = new EventProcedure[]
	{
			PROC_LEAVE_PARTY, PROC_WASH_COLOR, PROC_RETURN, new EventProcedure<CaptureTheFlag>()
			{
				@Override
				public void performOn(L2PcInstance member, CaptureTheFlag event)
				{
					CtfScore cs = event.getScore(member.getObjectId());
					if (cs.getCaptured() > 0)
					{
						Locale loc = member.getSoDPlayer().getSettings().getLocale();
						member.sendMessage(Internationalization.getInstance().get(loc, "EVENT_CTF_YOU_CAPTURED_TIMES",
								"%times%", String.valueOf(cs.getCaptured())));
						event.rewardAdena(member, 5);
					}
				}
			}, PROC_RECOVERY
	};
	
	private/*static*/final SplitProcedure[] SPLIT = new SplitProcedure[]
	{
			PROC_UNSAFE_TRANSFER, PROC_SPLIT_NEW_BALANCED
	};
	
	private final FastMap<Integer, CtfScore> _points;
	private L2PcInstance _best;
	
	/**
	 * Creates a CTF event
	 * 
	 * @param maxPoints
	 *            point limit
	 * @param autoRemove
	 *            mark as unused after end
	 * @param t1
	 *            Team 1
	 * @param t2
	 *            Team 2
	 */
	public CaptureTheFlag(int maxPoints, boolean autoRemove, FlagTeam t1, FlagTeam t2)
	{
		super(15000, 60, 0.75, maxPoints, -1, autoRemove, t1, t2);
		_points = new FastMap<Integer, CtfScore>().setShared(true);
	}
	
	protected CaptureTheFlag(CaptureTheFlag main, FlagTeam t1, FlagTeam t2)
	{
		super(main, t1, t2);
		_points = null;
	}
	
	@Override
	public boolean canUseSkill(L2Playable character, L2PcInstance participant, L2Skill skill)
	{
		if (character == participant
				&& !skill.isToggle()
				&& participant.getInventory().getPaperdollItemId(PcInventory.PAPERDOLL_RHAND) == CaptureTheFlag.COMBAT_FLAG_ITEM)
			return false;
		else
			return true;
	}
	
	public final void addCapture(Integer playerId)
	{
		if (getMainEvent() == null)
			getScore(playerId).incCaptured();
		else
			getMainEvent().addCapture(playerId);
	}
	
	public final CtfScore getScore(Integer playerId)
	{
		if (getMainEvent() != null)
			return getMainEvent().getScore(playerId);
		
		CtfScore cs = _points.get(playerId);
		if (cs == null)
		{
			cs = new CtfScore();
			CtfScore old = _points.putIfAbsent(playerId, cs);
			if (old != null)
				return old;
		}
		return cs;
	}
	
	@Override
	public CaptureTheFlag getMainEvent()
	{
		return (CaptureTheFlag) super.getMainEvent();
	}
	
	@Override
	protected double getCoef(FlagTeam team)
	{
		double temp, result = 0;
		for (FastSet.Record r = team.getMembers().head(), end = team.getMembers().tail(); (r = r.getNext()) != end;)
		{
			L2PcInstance player = team.getMembers().valueOf(r);
			temp = player.getLevel() - Config.STARTING_LEVEL + 1;
			ClassId prof = player.getClassId();
			if (prof.isSummoner())
				temp *= SUMMONER_LEVEL_MULTIPLIER;
			else if (prof.equalsOrChildOf(ClassId.Destroyer))
				temp *= DESTROYER_LEVEL_MULTIPLIER;
			else if (prof.equalsOrChildOf(ClassId.ShillienKnight))
				temp *= SKNIGHT_LEVEL_MULTIPLIER;
			else if (prof.equalsOrChildOf(ClassId.Gladiator))
				temp *= GLADIATOR_LEVEL_MULTIPLIER;
			else if (prof.equalsOrChildOf(ClassId.HumanKnight) || prof.equalsOrChildOf(ClassId.Warlord)
					|| prof.equalsOrChildOf(ClassId.TempleKnight))
				temp *= TANK_LEVEL_MULTIPLIER;
			else if (prof.equalsOrChildOf(ClassId.Bishop) || prof.equalsOrChildOf(ClassId.ElvenElder)
					|| prof.equalsOrChildOf(ClassId.ShillienElder))
				temp *= HEALER_LEVEL_MULTIPLIER;
			else if (prof.isMage())
				temp *= MAGE_LEVEL_MULTIPLIER;
			result += temp;
		}
		return result;
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getEndingProcedure()
	{
		return END;
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getMaintenanceProcedure()
	{
		return MAINTENANCE;
	}
	
	@Override
	public String getImplementationName()
	{
		return NAME;
	}
	
	@Override
	public SplitProcedure[] getSplitProcedure()
	{
		return SPLIT;
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getStartProcedure()
	{
		return START;
	}
	
	@Override
	public TeamedEvent<FlagTeam> newSplitEvent()
	{
		// TODO: instance problems?
		FlagTeam t = getTeam1();
		FlagTeam t1 = new FlagTeam(t.getLoc()[0], t.getColor(), t.getFlagId(), t.getFlagLoc());
		L2ItemInstance flag = t.getFlagItem();
		t.setFlagItem(null);
		t.spawnFlag(getInstanceId());
		t1.setFlagItem(flag);
		t = getTeam2();
		FlagTeam t2 = new FlagTeam(t.getLoc()[0], t.getColor(), t.getFlagId(), t.getFlagLoc());
		flag = t.getFlagItem();
		t.setFlagItem(null);
		t.spawnFlag(getInstanceId());
		t2.setFlagItem(flag);
		return new CaptureTheFlag(this, t1, t2);
	}
	
	@Override
	protected void doBeforeStart()
	{
		super.doBeforeStart();
		
		if (getTeam1().getFlagItem() == null)
			getTeam1().spawnFlag(getInstanceId());
		if (getTeam2().getFlagItem() == null)
			getTeam2().spawnFlag(getInstanceId());
	}
	
	@Override
	protected void doBeforeEnd()
	{
		super.doBeforeEnd();
		L2ItemInstance flag = getTeam1().getFlagItem();
		if (flag != null)
		{
			L2PcInstance owner = L2World.getInstance().getPlayer(flag.getOwnerId());
			if (owner == null || !owner.destroyItem(getImplementationName(), flag, null, true))
				ItemTable.getInstance().destroyItem(getImplementationName(), flag, owner, null);
			// if (owner != null)
			// owner.stopSpecialEffect(SpecialEffect.S_AIR_ROOT);
		}
		getTeam1().unspawnFlag();
		flag = getTeam2().getFlagItem();
		if (flag != null)
		{
			L2PcInstance owner = L2World.getInstance().getPlayer(flag.getOwnerId());
			if (owner == null || !owner.destroyItem(getImplementationName(), flag, null, true))
				ItemTable.getInstance().destroyItem(getImplementationName(), flag, owner, null);
			// if (owner != null)
			// owner.stopSpecialEffect(SpecialEffect.S_AIR_ROOT);
		}
		getTeam2().unspawnFlag();
		FastSet<L2PcInstance> members;
		if (getWinner() == null)
			members = getAllPlayers();
		else
			members = getWinner().getMembers();
		int max = -1;
		for (FastSet.Record r = members.head(), end = members.tail(); (r = r.getNext()) != end;)
		{
			L2PcInstance best = members.valueOf(r);
			CtfScore cs = getScore(best.getObjectId());
			if (cs.getPoints() > max)
			{
				max = cs.getPoints();
				_best = best;
			}
		}
	}
	
	@Override
	public void doAfterEnd()
	{
		final FastSet<Integer> winners = FastSet.newInstance();
		
		broadcastTeamScores(new TeamRewardProcedure<CaptureTheFlag.FlagTeam>()
		{
			@Override
			public void rewardMember(L2PcInstance member, FlagTeam t)
			{
				if (t == getWinner() && member != _best && getScore(member.getObjectId()).getPoints() > 0)
				{
					rewardAdena(member, 25);
					member.sendPacket(new PlaySound(SoundType.MUSIC, "BS08_D"));
					winners.add(member.getObjectId());
				}
			}
		});
		
		if (_points != null)
			_points.clear();
		if (_best != null)
		{
			rewardItem(_best, GlobalEvent.EVENT_MEDAL, 5);
			rewardAdena(_best, 30);
			_best.sendPacket(new PlaySound(SoundType.MUSIC, "Rm01_S"));
			Locale loc = _best.getSoDPlayer().getSettings().getLocale();
			_best.sendMessage(Internationalization.getInstance().get(loc, "EVENT_YOU_WERE_BEST"));
			winners.add(_best.getObjectId());
			_best = null;
		}
		
		if (!GlobalEventManager.getInstance().setWinners(winners, this))
			FastSet.recycle(winners);
		
		super.doAfterEnd();
	}
	
	public static final class CtfScore
	{
		private final AtomicInteger _captured;
		private final AtomicInteger _killed;
		
		protected CtfScore()
		{
			_captured = new AtomicInteger();
			_killed = new AtomicInteger();
		}
		
		private int getCaptured()
		{
			return _captured.get();
		}
		
		public void incCaptured()
		{
			_captured.incrementAndGet();
		}
		
		public int getKilled()
		{
			return _killed.get();
		}
		
		public void incKilled()
		{
			_killed.incrementAndGet();
		}
		
		private int getPoints()
		{
			return getCaptured() * 25 + getKilled();
		}
	}
	
	/**
	 * A simple extension that also defines the flag coordinates.
	 * 
	 * @author savormix
	 * @since 2010.03.31
	 */
	public static final class FlagTeam extends Team
	{
		private final int _flagId;
		private final Location _flagLoc;
		private final L2Spawn _flagSpawn;
		private volatile L2ItemInstance _flagItem;
		
		public FlagTeam(Location loc, int color, int flagId, Location flagLoc)
		{
			super(color, loc);
			_flagId = flagId;
			_flagLoc = flagLoc;
			L2NpcTemplate temp = NpcTable.getInstance().getTemplate(flagId);
			if (temp == null)
			{
				_log.warn("Invalid flag NPC: " + flagId, new IllegalArgumentException());
				_flagSpawn = null;
				return;
			}
			_flagSpawn = new L2Spawn(temp);
			_flagSpawn.setLoc(loc);
		}
		
		public FlagTeam(Location loc, int color, Location flagLoc)
		{
			this(loc, color, COMBAT_FLAG_NPC, flagLoc);
		}
		
		public FlagTeam(Location loc, Location flagLoc)
		{
			this(loc, -1, flagLoc);
		}
		
		/** @return the flag npc template's ID */
		public final int getFlagId()
		{
			return _flagId;
		}
		
		/** @return the flag's location */
		public final Location getFlagLoc()
		{
			return _flagLoc;
		}
		
		/** @return the flag's <CODE>L2Spawn</CODE> object */
		public final L2Spawn getFlagSpawn()
		{
			return _flagSpawn;
		}
		
		/**
		 * Spawns the flag in a given instance.
		 * 
		 * @param instanceId
		 *            Event's instance
		 */
		public final void spawnFlag(int instanceId)
		{
			if (_flagSpawn == null)
				return;
			_flagSpawn.setInstanceId(instanceId);
			CaptureTheFlagRestriction.getInstance().addFlag(_flagSpawn.doSpawn());
		}
		
		/** Removes the flag's NPC */
		public final void unspawnFlag()
		{
			if (_flagSpawn == null || _flagSpawn.getLastSpawn() == null)
				return;
			L2Npc flag = _flagSpawn.getLastSpawn();
			flag.deleteMe();
			CaptureTheFlagRestriction.getInstance().removeFlag(flag);
		}
		
		/** @return the flag's item; <CODE>null</CODE> if flag is spawned */
		public final L2ItemInstance getFlagItem()
		{
			return _flagItem;
		}
		
		/**
		 * Registers this team's flag as this item. The old item is unregistered
		 * from the restriction and the given item is then registered.
		 * 
		 * @param flagItem
		 *            the flagItem to set
		 */
		public final void setFlagItem(L2ItemInstance flagItem)
		{
			CaptureTheFlagRestriction.getInstance().removeFlag(_flagItem);
			_flagItem = flagItem;
			CaptureTheFlagRestriction.getInstance().addFlag(_flagItem);
		}
	}
}
