/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.event.global.impl;

import java.util.concurrent.atomic.AtomicInteger;

import javolution.util.FastMap;
import javolution.util.FastSet;

import org.sod.Calc;
import org.sod.model.ChallengeTemplate;
import org.sod.model.L2PlayerData;
import org.sod.model.event.global.EventProcedure;
import org.sod.model.event.global.GlobalEvent;
import org.sod.model.event.global.SplitProcedure;
import org.sod.model.event.global.restriction.LastManStandingRestriction;

import com.l2jfree.gameserver.model.Location;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.client.packets.sendable.PlaySoundPacket.PlaySound;
import com.l2jfree.gameserver.network.client.packets.sendable.PlaySoundPacket.SoundType;

/**
 * The Last Man Standing event.
 * 
 * @author savormix
 * @since 2010.03.28
 */
@SuppressWarnings("unchecked")
public class LastManStanding extends GlobalEvent
{
	private static final String NAME = "Last Man Standing";
	/** All over Elven Ruins, but not in the gallery. */
	private static final Location[] SPAWNS = new Location[]
	{
			new Location(49234, 248458, -5975), new Location(47967, 248625, -6360), new Location(47973, 248288, -6360),
			new Location(46767, 248793, -6360), new Location(47114, 249117, -6360), new Location(47359, 248829, -6360),
			new Location(47344, 249564, -6360), new Location(47844, 249553, -6360), new Location(47912, 248838, -6360),
			new Location(46408, 248176, -6360), new Location(46817, 248018, -6360), new Location(46797, 247744, -6360),
			new Location(47346, 247886, -6380), new Location(47760, 247583, -6545), new Location(47478, 247303, -6615),
			new Location(47747, 246984, -6615), new Location(47488, 246561, -6615), new Location(47986, 246624, -6615),
			new Location(47999, 247290, -6615), new Location(45886, 248072, -6360), new Location(46044, 248769, -6360),
			new Location(45710, 249423, -6360), new Location(44812, 249430, -6360), new Location(44857, 248929, -6360),
			new Location(45053, 248319, -6410), new Location(43610, 248482, -6495), new Location(43334, 246889, -6460),
			new Location(42595, 246942, -6455), new Location(43106, 246926, -6460), new Location(42581, 247666, -6460),
			new Location(42174, 247341, -6460), new Location(41660, 247117, -6460), new Location(41894, 246794, -6460),
			new Location(41462, 246809, -6460), new Location(41419, 245964, -6460), new Location(41851, 245927, -6460),
			new Location(41659, 245660, -6460), new Location(42081, 245365, -6460), new Location(42433, 245134, -6460),
			new Location(42395, 245593, -6460), new Location(43356, 245091, -6460), new Location(43344, 245578, -6460),
			new Location(43263, 246201, -6460), new Location(43991, 246036, -6460), new Location(43952, 245192, -6460),
			new Location(44357, 245593, -6460), new Location(43617, 244353, -6495), new Location(44826, 243861, -6460),
			new Location(44816, 243388, -6460), new Location(44848, 242833, -6460), new Location(45968, 242854, -6460),
			new Location(45986, 243391, -6460), new Location(45778, 244251, -6510), new Location(46787, 244030, -6560),
			new Location(47593, 242831, -6560), new Location(47593, 243363, -6560), new Location(46978, 243134, -6560),
			new Location(47309, 244350, -6560), new Location(47864, 244585, -6560), new Location(47772, 244100, -6560),
			new Location(48001, 243365, -6610),
	};
	static
	{
		LastManStandingRestriction.getInstance().activate();
	}
	protected static final EventProcedure<GlobalEvent> PROC_MOVE_TO_SPAWN = new EventProcedure<GlobalEvent>()
	{
		@Override
		public void performOn(L2PcInstance member, GlobalEvent event)
		{
			member.teleToLocation(Calc.getRandomFromArray(SPAWNS), true);
		}
	};
	private static final EventProcedure<GlobalEvent>[] START = new EventProcedure[]
	{
			PROC_LEAVE_PARTY, PROC_MOVE_TO_SPAWN, PROC_RECOVERY
	};
	private static final EventProcedure<GlobalEvent>[] MAINTENANCE = new EventProcedure[]
	{
		new EventProcedure<GlobalEvent>()
		{
			@Override
			public void performOn(L2PcInstance member, GlobalEvent event)
			{
				if (member.isDead())
					event.addPendingRemoval(member);
			}
		}
	};
	private static final EventProcedure<GlobalEvent>[] END = new EventProcedure[]
	{
			PROC_LEAVE_PARTY, PROC_RETURN, PROC_RECOVERY
	};
	
	private final FastMap<Integer, LmsScore> _banned;
	protected L2PcInstance _survivor;
	protected L2PcInstance _phonomania;
	
	/**
	 * Creates a LMS event.
	 * 
	 * @param autoRemove
	 *            mark as unused after end
	 */
	public LastManStanding(boolean autoRemove)
	{
		// never split
		super(5000, Integer.MAX_VALUE, 1, Integer.MAX_VALUE, -1, autoRemove);
		_banned = new FastMap<Integer, LmsScore>().setShared(true);
	}
	
	@Override
	public boolean add(L2PcInstance player)
	{
		if (!super.addTest(player))
			return false;
		
		if (_banned.containsKey(player.getObjectId()))
		{
			player.getSoDPlayer().sendLocalizedMessage("EVENT_LMS_CANNOT_REJOIN");
			return false;
		}
		else
			_banned.put(player.getObjectId(), new LmsScore());
		return addDirect(player);
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getEndingProcedure()
	{
		return END;
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getMaintenanceProcedure()
	{
		return MAINTENANCE;
	}
	
	@Override
	public String getImplementationName()
	{
		return NAME;
	}
	
	@Override
	public EventProcedure<? extends GlobalEvent>[] getStartProcedure()
	{
		return START;
	}
	
	@Override
	protected void doBeforeEnd()
	{
		super.doBeforeEnd();
		if (_survivor != null)
			return;
		
		L2PcInstance surv = null, killer = null;
		long survLength = Long.MAX_VALUE, killLength = Long.MAX_VALUE;
		int kills = 0;
		for (FastSet.Record r = getAllPlayers().head(), end = getAllPlayers().tail(); (r = r.getNext()) != end;)
		{
			L2PcInstance participant = getAllPlayers().valueOf(r);
			LmsScore ls = _banned.get(participant.getObjectId());
			if (participant.isDead() || ls == null)
				continue;
			if (ls.getJoinTime() < survLength)
			{
				survLength = ls.getJoinTime();
				surv = participant;
			}
			if (ls.getKilled() == kills && ls.getJoinTime() < killLength)
				kills--;
			if (ls.getKilled() > kills)
			{
				kills = ls.getKilled();
				killLength = ls.getJoinTime();
				killer = participant;
			}
		}
		_survivor = surv;
		_phonomania = killer;
	}
	
	@Override
	protected void doAfterEnd()
	{
		super.doAfterEnd();
		
		if (!(this instanceof SlaughterHouse))
		{
			if (_survivor != null)
			{
				rewardItem(_survivor, GlobalEvent.EVENT_MEDAL, 3);
				rewardAdena(_survivor, 20);
				_survivor.sendPacket(new PlaySound(SoundType.MUSIC, "Rm01_S"));
				L2PlayerData dat = _survivor.getSoDPlayer();
				rewardChallenge(dat, ChallengeTemplate.SURVIVOR, 1);
				LmsScore ls = _banned.get(_survivor.getObjectId());
				if (ls != null && ls.getKilled() == 0)
					rewardChallenge(dat, ChallengeTemplate.HARMLESS, 1);
				_survivor = null;
			}
			if (_phonomania != null)
			{
				rewardItem(_phonomania, GlobalEvent.EVENT_MEDAL, 3);
				rewardAdena(_phonomania, 40);
				if (_phonomania != _survivor)
					_phonomania.sendPacket(new PlaySound(SoundType.MUSIC, "Rm01_S"));
				rewardChallenge(_phonomania.getSoDPlayer(), ChallengeTemplate.EXTRA_CRUELTY, 1);
				_phonomania = null;
			}
		}
		_banned.clear();
	}
	
	@Override
	protected void doAfterMaintenance()
	{
		super.doAfterMaintenance();
		
		sweep();
		/*
		 * With few people online, there is no need to have this event
		 * run full-time. Especially because you can't rejoin once you die/leave.
		 */
		if (getAllPlayers().size() == 1 && _banned.size() > 7)
		{
			_survivor = getAllPlayers().iterator().next();
			LmsScore ls = _banned.get(_survivor.getObjectId());
			if (ls != null && ls.getKilled() > 0)
				_phonomania = _survivor;
			onFinish();
		}
	}
	
	@Override
	public SplitProcedure[] getSplitProcedure()
	{
		return null;
	}
	
	@Override
	public GlobalEvent newSplitEvent()
	{
		return null;
	}
	
	public static class LmsScore
	{
		private final long _joinTime;
		private AtomicInteger _killed;
		
		protected LmsScore()
		{
			_joinTime = System.currentTimeMillis();
			_killed = new AtomicInteger();
		}
		
		public final long getJoinTime()
		{
			return _joinTime;
		}
		
		public int getKilled()
		{
			return _killed.get();
		}
		
		public void incKilled()
		{
			_killed.incrementAndGet();
		}
	}
}
