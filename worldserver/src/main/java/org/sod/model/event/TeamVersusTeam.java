/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.event;

import javolution.util.FastSet;

import com.l2jfree.util.logging.L2Logger;

import org.sod.manager.RespawnManager;

import com.l2jfree.Config;
import com.l2jfree.gameserver.ThreadPoolManager;
import com.l2jfree.gameserver.instancemanager.InstanceManager;
import com.l2jfree.gameserver.model.Location;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.base.ClassId;
import com.l2jfree.gameserver.model.restriction.global.GlobalRestrictions;

/**
 * Free TvT engine.
 * 
 * @author savormix
 * @since 2010.03.26
 */
public final class TeamVersusTeam
{
	private static final L2Logger _log = L2Logger.getLogger(TeamVersusTeam.class);
	
	private static final int RESPAWN_DELAY = 10000;
	public static final double MAGE_LEVEL_MULTIPLIER = 1.5;
	public static final double DAGGER_LEVEL_MULTIPLIER = 1.2;
	public static final double ARCHER_LEVEL_MULTIPLIER = 1.15;
	public static final double TYRANT_LEVEL_MULTIPLIER = 1.33;
	public static final double SUMMONER_LEVEL_MULTIPLIER = 0.8;
	public static final double HEALER_LEVEL_MULTIPLIER = 1.1;
	
	private final int _instance;
	private final Location _1;
	private final Location _2;
	private final FastSet<L2PcInstance> _team1;
	private final FastSet<L2PcInstance> _team2;
	private volatile double _coef1;
	private volatile double _coef2;
	private boolean _empty;
	
	@SuppressWarnings("deprecation")
	private TeamVersusTeam()
	{
		EventRestriction.getInstance().activate();
		// The specific instance ID is not really needed
		_instance = 0xA0B0C0;
		InstanceManager.getInstance().createInstance(_instance);
		_1 = new Location(-114790, -178260, -6755, 0x00FFFF);
		_team1 = new FastSet<L2PcInstance>();
		_2 = new Location(-114780, -181965, -6755, 0xFFFF00);
		_team2 = new FastSet<L2PcInstance>();
		recalculate();
		ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(new Respawner(), 60000, RESPAWN_DELAY);
		_log.info("Team versus Team initialized.");
	}
	
	private void recalculate()
	{
		_coef1 = getCoef(_team1);
		_coef2 = getCoef(_team2);
		_empty = (_coef1 == 0 && _coef2 == 0);
	}
	
	private double getCoef(FastSet<L2PcInstance> team)
	{
		double temp, result = 0;
		for (FastSet.Record r = team.head(), end = team.tail(); (r = r.getNext()) != end;)
		{
			L2PcInstance player = team.valueOf(r);
			temp = player.getLevel() - Config.STARTING_LEVEL + 1;
			ClassId prof = player.getClassId();
			if (prof.isSummoner())
				temp *= SUMMONER_LEVEL_MULTIPLIER;
			else if (prof.equalsOrChildOf(ClassId.AbyssWalker) || prof.equalsOrChildOf(ClassId.Plainswalker)
					|| prof.equalsOrChildOf(ClassId.TreasureHunter))
				temp *= DAGGER_LEVEL_MULTIPLIER;
			else if (prof.equalsOrChildOf(ClassId.Tyrant))
				temp *= TYRANT_LEVEL_MULTIPLIER;
			else if (prof.equalsOrChildOf(ClassId.Arbalester) || prof.equalsOrChildOf(ClassId.Hawkeye)
					|| prof.equalsOrChildOf(ClassId.PhantomRanger) || prof.equalsOrChildOf(ClassId.SilverRanger))
				temp *= ARCHER_LEVEL_MULTIPLIER;
			else if (prof.equalsOrChildOf(ClassId.Bishop) || prof.equalsOrChildOf(ClassId.ElvenElder)
					|| prof.equalsOrChildOf(ClassId.ShillienElder))
				temp *= HEALER_LEVEL_MULTIPLIER;
			else if (prof.isMage())
				temp *= MAGE_LEVEL_MULTIPLIER;
			result += temp;
		}
		return result;
	}
	
	public void add(L2PcInstance player)
	{
		if (isPlaying(player) || !GlobalRestrictions.canTeleport(player))
			return;
		
		if (_coef1 > _coef2)
			_team2.add(player);
		else
			_team1.add(player);
		recalculate();
	}
	
	public void remove(L2PcInstance player)
	{
		if (_empty || !isPlaying(player))
			return;
		_team1.remove(player);
		_team2.remove(player);
		// player.getAppearance().setNameColor(0xFFFFFF);
		player.setIsPendingRevive(player.isDead());
		RespawnManager.getInstance().moveToSpawn(player);
		recalculate();
	}
	
	public boolean isPlaying(L2PcInstance... player)
	{
		for (L2PcInstance pc : player)
			if (getTeam(pc) == null)
				return false;
		return true;
	}
	
	private FastSet<L2PcInstance> getTeam(L2PcInstance player)
	{
		if (player == null)
			return null;
		else if (_team1.contains(player))
			return _team1;
		else if (_team2.contains(player))
			return _team2;
		else
			return null;
	}
	
	public boolean isSameTeam(L2PcInstance player1, L2PcInstance player2)
	{
		return getTeam(player1) == getTeam(player2);
	}
	
	private class Respawner implements Runnable
	{
		@Override
		public void run()
		{
			relocateDeceased(_team1, _1);
			relocateDeceased(_team2, _2);
		}
		
		private void relocateDeceased(FastSet<L2PcInstance> team, Location loc)
		{
			for (FastSet.Record r = team.head(), end = team.tail(); (r = r.getNext()) != end;)
			{
				L2PcInstance player = team.valueOf(r);
				if (!player.isDead())
					continue;
				player.setIsPendingRevive(player.isDead());
				player.setInstanceId(_instance);
				player.getAppearance().setNameColor(loc.getHeading());
				player.teleToLocation(loc, true);
				player.getStatus().setCurrentCp(player.getMaxCp());
				player.getStatus().setCurrentHpMp(player.getMaxHp(), player.getMaxMp());
			}
		}
	}
	
	public static TeamVersusTeam getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final TeamVersusTeam INSTANCE = new TeamVersusTeam();
	}
}
