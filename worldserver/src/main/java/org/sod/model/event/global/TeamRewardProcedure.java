/*
 * MISSING LICENSING INFO
 */
package org.sod.model.event.global;

import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;

/**
 * @author savormix
 * 
 */
public interface TeamRewardProcedure<TeamType extends Team>
{
	public void rewardMember(L2PcInstance member, TeamType t);
}
