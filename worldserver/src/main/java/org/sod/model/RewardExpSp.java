/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model;

import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;

/**
 * An experience and/or skill points challenge reward.
 * @author savormix
 * @since 2010.12.07
 */
public class RewardExpSp extends Rewardable
{
	private final long _exp;
	private final int _sp;
	
	/**
	 * Constructs an experience and skill point reward.
	 * @param exp Experience points
	 * @param sp Skill points
	 */
	public RewardExpSp(long exp, int sp)
	{
		_exp = Math.max(exp, 0);
		_sp = Math.max(sp, 0);
	}
	
	/**
	 * Constructs an experience reward.
	 * @param exp Experience points
	 */
	public RewardExpSp(long exp)
	{
		this(exp, 0);
	}
	
	/**
	 * Constructs a skill points reward.
	 * @param sp Skill points
	 */
	public RewardExpSp(int sp)
	{
		this(0, sp);
	}
	
	public long getExp()
	{
		return _exp;
	}
	
	public int getSp()
	{
		return _sp;
	}
	
	/* (non-Javadoc)
	 * @see org.sod.model.Rewardable#giveReward(com.l2jfree.gameserver.model.actor.instance.L2PcInstance, boolean)
	 */
	@Override
	public void giveReward(L2PcInstance player, boolean complementary)
	{
		if (!complementary)
			player.addExpAndSp(getExp(), getSp());
	}
	
	/* (non-Javadoc)
	 * @see org.sod.model.Rewardable#validate()
	 */
	@Override
	public boolean validate()
	{
		if (getExp() == 0 && getSp() == 0)
		{
			_log.warn("Invalid XP or SP reward! XP: " + getExp() + " SP: " + getSp(),
					new IllegalArgumentException());
			return false;
		}
		return true;
	}
}
