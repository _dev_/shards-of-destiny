/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model;

import com.l2jfree.gameserver.datatables.SkillTable;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;

/**
 * A skill challenge reward.
 * @author savormix
 * @since 2010.12.07
 */
public class RewardSkill extends Rewardable
{
	private final int _id;
	private final int _level;
	
	/**
	 * Constructs a skill reward.
	 * @param id Skill ID
	 * @param level Skill level
	 */
	public RewardSkill(int id, int level)
	{
		_id = id;
		_level = level;
	}
	
	public int getId()
	{
		return _id;
	}
	
	public int getLevel()
	{
		return _level;
	}
	
	/* (non-Javadoc)
	 * @see org.sod.model.Rewardable#giveReward(com.l2jfree.gameserver.model.actor.instance.L2PcInstance, boolean)
	 */
	@Override
	public void giveReward(L2PcInstance player, boolean complementary)
	{
		player.addSkill(getId(), getLevel());
	}
	
	/* (non-Javadoc)
	 * @see org.sod.model.Rewardable#validate()
	 */
	@Override
	public boolean validate()
	{
		if (SkillTable.getInstance().getInfo(getId(), getLevel()) == null)
		{
			_log.warn("Invalid skill reward! ID: " + getId() + " level: " + getLevel(),
					new IllegalArgumentException());
			return false;
		}
		return true;
	}
}
