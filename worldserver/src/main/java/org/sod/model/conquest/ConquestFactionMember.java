/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.conquest;

import com.l2jfree.gameserver.model.actor.L2Character;

/**
 * Special Aden Conquest interface, defining side of an instance.
 * @author savormix
 * @since 2010.02.04
 */
public interface ConquestFactionMember
{
	public L2Character getActingCharacter();
	/**
	 * A method to determine the Conquest faction of this instance.
	 * This method generally should not use
	 * {@link org.sod.manager.conquest.ConquestFactionManager#getFaction(Object)}
	 * to return a result.
	 * @return Conquest faction ID
	 */
	public int getConquestFaction();
}
