/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model.conquest;

import com.l2jfree.gameserver.model.zone.L2FactionZone;

/**
 * Special Aden Conquest interface used on NPCs
 * that are directly related to a faction zone.
 * @author savormix
 * @since 2010.02.04
 */
public interface ConquestZoneMember extends ConquestFactionMember
{
	public void setFactionZone(L2FactionZone zone);
	public L2FactionZone getFactionZone();
}
