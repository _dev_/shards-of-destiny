package org.sod.model;

import static org.sod.manager.ChallengeManager.CONQUEST;
import static org.sod.manager.ChallengeManager.EVENT;
import static org.sod.manager.ChallengeManager.GENERAL;
import static org.sod.manager.ChallengeManager.PVP;
import static org.sod.manager.ChallengeManager.SOCIAL;
import static org.sod.manager.ChallengeManager.SUPPORT;

import org.sod.model.event.global.GlobalEvent;

import com.l2jfree.gameserver.model.base.Experience;
import com.l2jfree.gameserver.model.itemcontainer.PcInventory;

public enum ChallengeTemplate implements PointTemplate
{
	// @formatter:off
	FRAGILE(20, GENERAL, "Fragile", "Die 1500 times.", 1500, "Permanent +15% maximum HP", new RewardSkill(9301, 1)),
	POWER_LEVEL(26, GENERAL, "Powerleveling", "Reach level 85.", 85, "Top-Grade Life Stone [Level 84] (2)", new RewardItem(14169, 2)),
	JURASSIC_PARK(29, GENERAL, "Jurassic Park", "Complete Area 3 subquest.", 1, "Medium Experience", false, new RewardExpSp(Experience.LEVEL[75] / 700)),
	L2WALKER(31, GENERAL, "L2Walker", "Walk (instead of running) 50 kilometers", 50000, "+10 Movement speed", new RewardSkill(9303, 1)),
	TRADER(44, GENERAL, "Trader", "Sell an item using the auction system.", 1, "Low Experience", false, new RewardExpSp(Experience.LEVEL[75] / 1000)),

	CHEAT_DEATH(10, SUPPORT, "Cheating Death", "Resurrect 4000 players.", 4000, "High Experience, Event Medal (1)", new RewardItem(GlobalEvent.EVENT_MEDAL, 1), new RewardExpSp(Experience.LEVEL[75] / 375)),
	FIELD_MEDIC(45, SUPPORT, "Field Medic", "Heal 10 million health or combat points.", 10000000, "High Experience", new RewardExpSp(Experience.LEVEL[75] / 375)),
	MANA_WELL(46, SUPPORT, "Mana Well", "Recover a million mana points.", 1000000, "High Experience", new RewardExpSp(Experience.LEVEL[75] / 375)),
	PUBLIC_DANCER(47, SUPPORT, "Public Dancer", "Dance the Berserker's dance on a full party 50 times.", 10, "High Experience, Adena (150)", new RewardItem(PcInventory.ADENA_ID, 150), new RewardExpSp(Experience.LEVEL[75] / 375)),
	POPSTAR(48, SUPPORT, "Popstar", "Sing the Purification song on a full party 50 times.", 10, "High Experience, Adena (150)", new RewardItem(PcInventory.ADENA_ID, 150), new RewardExpSp(Experience.LEVEL[75] / 375)),

	EXTRA_CRUELTY(1, EVENT, "Extreme Cruelty", "Get the most kills during Last Man Standing. You do not have to win the event.", 1, "High Experience", new RewardExpSp(Experience.LEVEL[75] / 375)),
	SURVIVOR(53, EVENT, "Survivor", "Win Last Man Standing.", 1, "Medium Experience", new RewardExpSp(Experience.LEVEL[75] / 700)),
	HARMLESS(54, EVENT, "Harmless", "Win Last Man Standing without killing anyone.", 1, "High Experience, Adena (100)", new RewardExpSp(Experience.LEVEL[75] / 375), new RewardItem(PcInventory.ADENA_ID, 100)),
	MASSACRE(2, EVENT, "Massacre", "Win Deathmatch.", 1, "High Experience", new RewardExpSp(Experience.LEVEL[75] / 375)),
	//RAPEFEST(28, EVENT, "Rapefest", "Win Team Deathmatch by getting max points while the enemy team has 10 times less points.", 1, "Undecided"),
	WRENCH_IS_MINE(32, EVENT, "The Wrench", "Kill the Golden Golem 10 times.", 10, "Medium Experience", new RewardExpSp(Experience.LEVEL[75] / 700)),
	THAT_STUPID_ROCK(33, EVENT, "Piece of Rock", "Kill the enemy team's golem.", 1, "Medium Experience", new RewardExpSp(Experience.LEVEL[75] / 700)),
	REVENGINEERING(34, EVENT, "Revengineering", "Kill the Golden Carrier 10 times.", 10, "Medium Experience", new RewardExpSp(Experience.LEVEL[75] / 700)),
	TEMPTATION(35, EVENT, "Temptation", "Win Lure the Gremlin 5 times.", 5, "Medium Experience", new RewardExpSp(Experience.LEVEL[75] / 700)),
	FLAG_BEARER(36, EVENT, "Flag Bearer", "Capture the flag.", 1, "Medium Experience", new RewardExpSp(Experience.LEVEL[75] / 700)),
	DEMOTIVATOR(37, EVENT, "Demotivator", "Cause 5 flag drops.", 5, "Medium Experience", new RewardExpSp(Experience.LEVEL[75] / 700)),
	COOPERATION(38, EVENT, "Cooperative", "Win Team Deathmatch or Team King of the Hill 5 times.", 5, "Medium Experience", new RewardExpSp(Experience.LEVEL[75] / 700)),
	TAG_AND_FRAG(49, EVENT, "Tag&Frag", "Win Tag the Monster.", 1, "Medium Experience", new RewardExpSp(Experience.LEVEL[75] / 700)),
	ZERGLING_RUSH(50, EVENT, "Zergling Rush", "Win Regicide by killing the king.", 1, "High Experience", new RewardExpSp(Experience.LEVEL[75] / 375)),
	ANGEL(51, EVENT, "Guardian Angel", "Prevent the king from dying in Regicide.", 1, "High Experience", new RewardExpSp(Experience.LEVEL[75] / 375)),
	SECTOR_CLEAR(52, EVENT, "Sector clear", "Win Slaughterhouse.", 1, "Medium Experience", new RewardExpSp(Experience.LEVEL[75] / 700)),

	BRINK(0, PVP, "The Brink", "Get a three-kill streak while near death (red flashing HP bar).", 3, "Low Experience", new RewardExpSp(Experience.LEVEL[75] / 1000)),
	HOT_POTATO(3, PVP, "Hot Potato", "Kill 600 enemies with Martyrdom Pulse Energy.", 600, "Medium Experience", new RewardExpSp(Experience.LEVEL[75] / 700)),
	SPRAY_N_PRAY(4, PVP, "Spray and Pray", "Kill 15 enemies in a row.", 15, "Medium Experience, Cold-Blooded Perk", new RewardExpSp(Experience.LEVEL[75] / 700), new RewardPerk(8, false)),
	RIOT_CONTROL(5, PVP, "Riot Control", "Kill 50 enemies in a row.", 50, "Extra slot in NPC buffer.", new RewardSkill(9300, 1)),
	FREQUENT_FRYER(6, PVP, "Frequent Fryer", "Kill 1500 enemies with the Suicide Grenades.", 1500, "Event Medal (1), Danger Close Pro Perk", new RewardItem(GlobalEvent.EVENT_MEDAL, 1), new RewardPerk(9, true)),
	REMF(7, PVP, "REMF", "Assist 1000 kills.", 1000, "Top-Grade Life Stone [Level 76] (1)", new RewardItem(8762, 1L)),
	SURGEON(8, PVP, "The Surgeon", "Kill 4000 enemies with a close-range attack.", 4000, "High Experience, Event Medal (1), One Man Army Perk", new RewardItem(GlobalEvent.EVENT_MEDAL, 1), new RewardExpSp(Experience.LEVEL[75] / 375), new RewardPerk(4, false)),
	BLOODY_TOSSER(9, PVP, "The Bloody Tosser", "Kill 4000 enemies with a long-range attack.", 4000, "High Experience, Event Medal (1), One Man Army Perk", new RewardItem(GlobalEvent.EVENT_MEDAL, 1), new RewardExpSp(Experience.LEVEL[75] / 375), new RewardPerk(4, false)),
	ROUGH_ECONOMY(12, PVP, "Rough Economy", "Kill 3 enemies in 6 seconds.", 3, "Event Medal (2), Hardline Pro Perk", new RewardItem(GlobalEvent.EVENT_MEDAL, 2), new RewardPerk(7, true)),
	CLOSE_SHAVE(13, PVP, "Close Shave", "Survive a Martyrdom or Dynamite Explosion.", 1, "Low Experience", new RewardExpSp(Experience.LEVEL[75] / 1000)),
	DOWN_NOT_OUT(14, PVP, "Down but not out", "Complete 'The Brink' Challenge 30 times.", 30 * 3, "High Experience, Commando Pro Perk", new RewardExpSp(Experience.LEVEL[75] / 375), new RewardPerk(10, true)),
	//NO_SECRETS(18, PVP, "No Secrets", "Call in the Radar a total of 50 times.", 50, "Low Experience", new RewardExpSp(Experience.LEVEL[75] / 1000)),
	SPECIAL_DELIVERY(19, PVP, "Special Delivery", "Kill 200 Players who had a 50 or more Bounty on their head.", 200, "Event Medal (1)", new RewardItem(GlobalEvent.EVENT_MEDAL, 1)),
	DEMIGOD(27, PVP, "Demigod", "Kill 20 people in 30 seconds without using AoE skills.", 20, "Undecided"),
	FIRST_BLOOD(39, PVP, "First Blood", "Kill someone.", 1, "Adena (10)", new RewardItem(PcInventory.ADENA_ID, 10)),
	PAPERMAKING(40, PVP, "Papermaking", "Receive an enchant scroll.", 1, "Adena (30)", new RewardItem(PcInventory.ADENA_ID, 30)),
	POWERUP(41, PVP, "Power-up", "Get a weapon enchant.", 1, "Adena (100)", new RewardItem(PcInventory.ADENA_ID, 100)),
	BEASTMASTER(42, PVP, "Beastmaster", "Attract a NPC helper by getting 15 kills without dying.", 1, "Medium Experience", new RewardExpSp(Experience.LEVEL[75] / 700)),
	GRAVE_ROBBER(43, PVP, "Grave Robber", "Sweep a spoiled player or summon corpse.", 1, "Low Experience", new RewardExpSp(Experience.LEVEL[75] / 1000)),

	SPARTAN(11, CONQUEST, "The Spartan", "Kill 6000 guards.", 6000, "Medium Experience", false, new RewardExpSp(Experience.LEVEL[75] / 700)),
	RABID(15, CONQUEST, "Rabid", "Kill 150 enemies that were Sealing.", 150, "Medium Experience", false, new RewardExpSp(Experience.LEVEL[75] / 700)),
	IRON_FIST(16, CONQUEST, "Iron Fist", "Kill 800 enemies that were Sealing.", 800, "High Experience", false, new RewardExpSp(Experience.LEVEL[75] / 375)),
	WAR_HERO(17, CONQUEST, "War Hero", "Seal 400 zones.", 400, "High Experience", false, new RewardExpSp(Experience.LEVEL[75] / 375)),
	NO_ODDS(30, CONQUEST, "Against all odds", "Seal a zone while being outnumbered.", 1, "High Experience", false, new RewardExpSp(Experience.LEVEL[75] / 375)),

	SAD_NOOB(21, SOCIAL, "Sad Noob", "Die while doing social sorrow 700 times.", 700, "+2 Evasion, +5 Move Speed, +5% M.Def., Sobbing Agathion 30 Day Pack", false, new RewardItem(14230, 1), new RewardSkill(9302, 1)),
	//LAUGHING_DEATH(22, SOCIAL, "Laughing Death", "Use social laugh before dealing the finishing hit 500 times.", 500, "Undecided"),
	//ARTISTIC_INTRO(23, SOCIAL, "Artistic Intro", "Kill 3 enemies within 2 minutes. Use social charge before first attack and social victory after each kill.", 3, "Undecided"),
	//ARTISTIC_PERFORMANCE(24, SOCIAL, "Artistic Performance", "Undecided", 10, "Undecided"),
	//MUDKIP_DANCE(25, SOCIAL, "Liek a Mudkip", "Applaud nearby enemies. Respond with social unaware to first damage. " +
	//		"Do social dance, then social charm. If receiving damage, this time respond with social " +
	//		"shyness. Perform social bow. Kill an enemy that had attacked you during your performance " +
	//		"and then use social waiting.", 1, "Undecided"),
	;
	// @formatter:on
	
	private final int _id;
	private final String _cat;
	private final String _name;
	private final String _fullName;
	private final String _desc;
	private final int _unlockPoints;
	private String _reward;
	private boolean _enabled;
	private Rewardable[] _rewards;
	
	private ChallengeTemplate(int id, String cat, String name, String desc, int unlockPoints, String reward,
			boolean enabled, Rewardable... rewards)
	{
		_id = id;
		_cat = cat;
		_name = name;
		_fullName = cat + ": " + name;
		_desc = desc;
		_unlockPoints = unlockPoints;
		_reward = reward;
		_enabled = enabled;
		setRewards(rewards);
	}
	
	private ChallengeTemplate(int id, String cat, String name, String desc, int unlockPoints, String reward,
			Rewardable... rewards)
	{
		this(id, cat, name, desc, unlockPoints, reward, true, rewards);
	}
	
	public final int getId()
	{
		return _id;
	}
	
	protected final String getCat()
	{
		return _cat;
	}
	
	protected final String getName()
	{
		return _name;
	}
	
	public final String getFullName()
	{
		return _fullName;
	}
	
	public final String getDesc()
	{
		return _desc;
	}
	
	public final String getReward()
	{
		return _reward;
	}
	
	public void setReward(String reward)
	{
		_reward = reward;
	}
	
	public Rewardable[] getRewards()
	{
		return _rewards;
	}
	
	public void setRewards(Rewardable[] rewards)
	{
		boolean invalid = false;
		if (rewards.length == 0)
			invalid = true;
		for (Rewardable r : rewards)
		{
			if (!r.validate())
			{
				invalid = true;
				break;
			}
		}
		_rewards = rewards;
		if (invalid)
			_enabled = false;
	}
	
	public final boolean isEnabled()
	{
		return _enabled;
	}
	
	@Override
	public int getNeededPoints()
	{
		return _unlockPoints;
	}
	
	public static final ChallengeTemplate getTemplate(int id)
	{
		ChallengeTemplate[] temp = values();
		for (ChallengeTemplate ct : temp)
			if (ct.getId() == id)
				return ct;
		return null;
	}
}
