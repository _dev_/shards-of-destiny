/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.ScheduledFuture;

import javolution.util.FastList;
import javolution.util.FastMap;

import org.sod.Calc;
import org.sod.Skills;
import org.sod.buffs.BuffManager;
import org.sod.buffs.PlayerBuffs;
import org.sod.i18n.Internationalization;
import org.sod.manager.ChallengeManager.Challenge;
import org.sod.model.ChallengeTemplate;
import org.sod.manager.GlobalEventManager;
import org.sod.manager.PerkManager;
import org.sod.manager.PerkManager.Perk;
import org.sod.manager.PerkManager.PerkTemplate;
import org.sod.manager.RespawnManager;
import org.sod.manager.TitleManager;
import org.sod.manager.conquest.ConquestFactionManager;
import org.sod.model.conquest.ConquestFactionPlayable;
import org.sod.model.event.global.GlobalEvent;

import com.l2jfree.Config;
import com.l2jfree.L2DatabaseFactory;
import com.l2jfree.gameserver.ThreadPoolManager;
import com.l2jfree.gameserver.ai.L2ControllableMobAI;
import com.l2jfree.gameserver.communitybbs.Manager.SettingsBBSManager;
import com.l2jfree.gameserver.datatables.NpcTable;
import com.l2jfree.gameserver.idfactory.IdFactory;
import com.l2jfree.gameserver.instancemanager.BossSpawnManager;
import com.l2jfree.gameserver.instancemanager.BossSpawnManager.StatusEnum;
import com.l2jfree.gameserver.instancemanager.GrandBossSpawnManager;
import com.l2jfree.gameserver.instancemanager.RaidBossSpawnManager;
import com.l2jfree.gameserver.model.L2Clan;
import com.l2jfree.gameserver.model.L2ItemInstance;
import com.l2jfree.gameserver.model.L2Party;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.L2World;
import com.l2jfree.gameserver.model.MobGroup;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.L2Playable;
import com.l2jfree.gameserver.model.actor.instance.L2ControllableMobInstance;
import com.l2jfree.gameserver.model.actor.instance.L2MartyrInstance;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.actor.instance.L2RaidTeleporterInstance;
import com.l2jfree.gameserver.model.actor.instance.L2ZoneArtefactInstance;
import com.l2jfree.gameserver.model.actor.instance.L2ZoneSpawnGuardInstance;
import com.l2jfree.gameserver.model.itemcontainer.Inventory;
import com.l2jfree.gameserver.network.SystemMessageId;
import com.l2jfree.gameserver.network.client.packets.L2ServerPacket;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.Chat;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.ChatMessage;
import com.l2jfree.gameserver.network.client.packets.sendable.SystemMessagePacket.SystemMessage;
import com.l2jfree.gameserver.network.client.packets.sendable.TutorialShowHtml.ShowTutorialHtml;
import com.l2jfree.gameserver.network.client.packets.sendable.TutorialShowQuestionMarkPacket.ShowTutorialMark;
import com.l2jfree.gameserver.network.serverpackets.ExShowScreenMessage;
import com.l2jfree.gameserver.skills.SpecialEffect;
import com.l2jfree.gameserver.skills.Stats;
import com.l2jfree.gameserver.templates.chars.L2NpcTemplate;
import com.l2jfree.gameserver.templates.item.L2Item;
import com.l2jfree.gameserver.templates.item.L2WeaponType;
import com.l2jfree.gameserver.templates.skills.L2SkillType;
import com.l2jfree.gameserver.util.Broadcast;
import com.l2jfree.lang.L2TextBuilder;
import com.l2jfree.util.logging.L2Logger;

/**
 * Contains all SoD customs that would be accessed from L2PcInstance.
 * 
 * @author savormix
 * @since 2009.12.01
 */
public class L2PlayerData implements ConquestFactionPlayable
{
	// @formatter:off
	private static final L2Logger							_log					= L2Logger.getLogger(L2PlayerData.class);
	
	private volatile ScheduledFuture<?>					_perkEquip;
	private volatile boolean							_nearDeath;
	private volatile ScheduledFuture<?>					_lastStand;
	private volatile L2PcInstance						_resProp;
	private volatile long								_lastAoE;
	private volatile L2ZoneArtefactInstance				_artefact;
	private volatile boolean							_deleted;
	
	private final L2PcInstance							_player;
	private final FastMap<Integer, Perk>				_earnedPerks;
	private final Perk[]								_activePerks;
	private final Perk[]								_nextPerks;
	private final KillStreak							_killStreak;
	private final FastMap<ChallengeTemplate, Challenge>	_challenges;
	private final ArrayList<Integer>					_questMarks;
	private FastMap<Integer, RaidRequest>				_raidRequests;
	private final MobGroup[]							_followers;
	private final PcAuction								_auction;
	private final PlayerBuffs							_buffs;
	private final PlayerSettings						_settings;
	
	private int											_dualismFactionId;
	private int											_conquestFactionId;
	private boolean										_donator;
	private boolean										_superDonator;
	private int											_jailTotal;
	private String										_jailReason;
	private boolean										_godlike;
	private Record										_records;
	private long										_lastAction;
	private int											_inactivityTicks;
	private boolean										_inGhostMode;
	// @formatter:on
	
	/**
	 * Creates a class to store add-on data.
	 * 
	 * @param L2PcInstance
	 *            player
	 */
	public L2PlayerData(L2PcInstance player)
	{
		_player = player;
		_earnedPerks = new FastMap<Integer, Perk>().setShared(true);
		_activePerks = new Perk[4];
		_nextPerks = new Perk[4];
		_killStreak = new KillStreak();
		_challenges = new FastMap<ChallengeTemplate, Challenge>().setShared(true);
		_questMarks = new ArrayList<Integer>();
		_raidRequests = null;
		_followers = new MobGroup[3];
		_auction = new PcAuction(_player);
		_dualismFactionId = ConquestFactionManager.NONE;
		load();
		_buffs = BuffManager.getInstance().loadTemplates(getObjectId());
		_settings = loadSettings();
		markAction();
	}
	
	public final Integer getObjectId()
	{
		return _player.getObjectId();
	}
	
	@Override
	public final L2PcInstance getActingPlayer()
	{
		return _player;
	}
	
	private final void load()
	{
		loadSettings();
		loadJail();
		loadRecords();
		loadDonatorStatus();
		loadChallenges();
		loadPerks();
		loadDualism();
		getAuction().restore();
	}
	
	private void loadPerks()
	{
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con
					.prepareStatement("SELECT perkId,points,active FROM character_perks WHERE charId=?");
			ps.setInt(1, getObjectId());
			ResultSet rs = ps.executeQuery();
			while (rs.next())
			{
				Integer id = Integer.valueOf(rs.getInt("perkId"));
				Perk perk = new Perk(id, rs.getInt("points"));
				_earnedPerks.put(id, perk);
				if (rs.getBoolean("active"))
					activatePerk(id);
			}
			rs.close();
			ps.close();
		}
		catch (Exception e)
		{
			_log.error("Couldn't restore char's (" + getObjectId() + ") perks data!", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	private void loadJail()
	{
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection(con);
			PreparedStatement ps = con.prepareStatement("SELECT total,reason FROM character_jail WHERE charId=?");
			ps.setInt(1, getObjectId());
			ResultSet rs = ps.executeQuery();
			if (rs.next())
			{
				setJailTotal(rs.getInt("total"));
				setJailReason(rs.getString("reason"));
			}
			else
				_log.warn("Missing jail state for " + getActingPlayer());
			rs.close();
			ps.close();
		}
		catch (SQLException e)
		{
			_log.error("Couldn't restore char's (" + getObjectId() + ") jail data!", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	private PlayerSettings loadSettings()
	{
		PlayerSettings pset = new PlayerSettings();
		
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection(con);
			PreparedStatement ps = con
					.prepareStatement("SELECT waypoint,messages,vote,ghost,rates,iso639 FROM character_settings WHERE charId=?");
			ps.setInt(1, getObjectId());
			ResultSet rs = ps.executeQuery();
			if (rs.next())
			{
				pset.setMessages(rs.getBoolean("messages"));
				pset.setShowVoting(rs.getBoolean("vote"));
				pset.setWaypoint(rs.getBoolean("waypoint"));
				pset.setGhostMode(rs.getBoolean("ghost"));
				pset.setSuccessRates(rs.getInt("rates"));
				String iso639 = rs.getString("iso639");
				Locale loc = Locale.ENGLISH;
				for (Locale l : SettingsBBSManager.getInstance().getAllLocales())
				{
					if (l.getLanguage().equals(iso639))
					{
						loc = l;
						break;
					}
				}
				pset.setLocale(loc);
			}
			else
				_log.warn("Missing settings for " + getActingPlayer());
			rs.close();
			ps.close();
		}
		catch (SQLException e)
		{
			_log.error("Couldn't restore char's (" + getObjectId() + ") settings!", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
		
		return pset;
	}
	
	private void loadDonatorStatus()
	{
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection(con);
			PreparedStatement ps = con.prepareStatement("SELECT donator,superDonator FROM donators WHERE charId=?");
			ps.setInt(1, getObjectId());
			ResultSet rs = ps.executeQuery();
			if (rs.next())
			{
				setDonator(rs.getInt("donator") > 0);
				setSuperDonator(rs.getInt("superDonator") > 0);
			}
			rs.close();
			ps.close();
		}
		catch (SQLException e)
		{
			_log.error("Couldn't restore char's (" + getObjectId() + ") donator status!", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	public void saveDonator()
	{
		if (isDonator())
			makeDonator();
		else
			unmakeDonator();
	}
	
	private void makeDonator()
	{
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection(con);
			PreparedStatement ps = con.prepareStatement("INSERT INTO donators (charId) VALUES (?)");
			ps.setInt(1, getObjectId());
			ps.executeUpdate();
			ps.close();
		}
		catch (SQLException e)
		{
			_log.error("Couldn't save char's (" + getObjectId() + ") donation data!", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	private void unmakeDonator()
	{
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection(con);
			PreparedStatement ps = con.prepareStatement("DELETE FROM donators WHERE charId=?");
			ps.setInt(1, getObjectId());
			ps.executeUpdate();
			ps.close();
		}
		catch (SQLException e)
		{
			_log.error("Couldn't save char's (" + getObjectId() + ") donation data!", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	private void loadDualism()
	{
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection(con);
			PreparedStatement ps = con.prepareStatement("SELECT faction FROM character_factions WHERE charId=?");
			ps.setInt(1, getObjectId());
			ResultSet rs = ps.executeQuery();
			if (rs.next())
				setDualismFaction(rs.getInt("faction"));
			rs.close();
			ps.close();
		}
		catch (SQLException e)
		{
			_log.error("Couldn't restore char's (" + getObjectId() + ") faction data!", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	public Perk[] getActivePerks()
	{
		return _activePerks;
	}
	
	public Perk[] getNextPerks()
	{
		return _nextPerks;
	}
	
	/**
	 * Return the active perk ID in the specified slot.
	 * 
	 * @param slot
	 *            Perk slot ID (0 - 2, inclusive); 3 for death streak perk
	 * @return Perk or null
	 */
	public Perk getPerkInSlot(int slot)
	{
		return _activePerks[slot];
	}
	
	/**
	 * Activate a perk for this player.
	 * 
	 * @param id
	 *            Perk ID
	 */
	public void activatePerk(Integer id)
	{
		Perk p = _earnedPerks.get(id);
		if (p == null)
			return;
		
		int slot = p.getTemplate().getSlot();
		Perk old = _activePerks[slot];
		if (old == p)
			return;
		
		removePerkBonus(old);
		_activePerks[slot] = p;
		applyPerkBonus(p, false);
		
		updatePerk(p, false);
	}
	
	/**
	 * To be used if added point calculation would cause noticeable overhead.
	 * Can also be used to test if a char has an non-upgraded perk equipped.
	 * 
	 * @param id
	 *            Perk ID
	 * @return true if {@link #tryUpgradePerk(Integer, int)} should follow, false otherwise
	 */
	public final boolean canUpgradePerk(Integer id)
	{
		Perk p = _earnedPerks.get(id);
		if (p == null) // doesn't have
			return false;
		else if (_activePerks[p.getTemplate().getSlot()] != p) // not equipped
			return false;
		else if (p.isUpgraded()) // already upgraded
			return false;
		return true;
	}
	
	/**
	 * This method should be used to add points to perk if the point gain conditions are met,
	 * as this method calls {@link #canUpgradePerk(Integer)} to determine if player's conditions
	 * are met.
	 * 
	 * @param id
	 *            Perk ID
	 * @param points
	 *            points to add
	 */
	public void tryUpgradePerk(Integer id, int points)
	{
		if (canUpgradePerk(id))
			upgradePerk(id, points);
	}
	
	/**
	 * Updates a given perk without checking player's conditions.
	 * Does nothing if player doesn't have a perk with given ID altogether.
	 * Method was designed to remove point calculation overhead (e.g.
	 * calculate distance and convert to points without knowing if player
	 * meets the requirements). Use with caution.
	 * 
	 * @param id
	 *            Perk ID
	 * @param points
	 *            points to add
	 */
	public void upgradePerk(Integer id, int points)
	{
		Perk p = _earnedPerks.get(id);
		if (p == null)
			return;
		p.setPoints(p.getPoints() + points);
		if (getPerkInSlot(p.getTemplate().getSlot()) == p)
			applyPerkBonus(p, true);
		updatePerk(p, false);
	}
	
	private final void applyPerkBonus(Perk p, boolean upgradeOnly)
	{
		if (p == null)
			return;
		p.setActive(true);
		L2PcInstance player = getActingPlayer();
		if (p.isUpgraded())
		{
			player.addSkill(p.getTemplate().getSkillId(), 2);
			int upSkill = p.getTemplate().getUpgradeSkillId();
			if (upSkill > 0)
				player.addSkill(upSkill, 1);
			if (upgradeOnly)
				showQuestionMark(ShowTutorialMark.PERK_UPGRADE_OFFSET + p.getTemplate().getId());
		}
		else if (!upgradeOnly)
			player.addSkill(p.getTemplate().getSkillId(), 1);
	}
	
	private final void removePerkBonus(Perk p)
	{
		if (p != null)
		{
			L2PcInstance player = getActingPlayer();
			player.removeSkill(p.getTemplate().getSkillId());
			player.removeSkill(p.getTemplate().getUpgradeSkillId());
			p.setActive(false);
		}
	}
	
	public void updateAppearance()
	{
		// L2PcInstance p = getActingPlayer();
		// TitleManager.getInstance().updateTitle(p, true);
		// managed by GRs now
		// p.getAppearance().setTitleColor(FactionManager.getFactionColor(getFaction()));
		// p.broadcastFullInfo();
	}
	
	/** @return the donator status */
	public final boolean isDonator()
	{
		return _donator;
	}
	
	/**
	 * Sets the donator status.
	 * 
	 * @param donator
	 *            donator status
	 */
	public final void setDonator(boolean donator)
	{
		_donator = donator;
	}
	
	/** @return the super donator status */
	public final boolean isSuperDonator()
	{
		return _superDonator;
	}
	
	/**
	 * Sets the super donator status.
	 * 
	 * @param superDonator
	 *            super donator status
	 */
	public final void setSuperDonator(boolean superDonator)
	{
		_superDonator = superDonator;
	}
	
	/** @return how many times this player has been jailed */
	public final int getJailTotal()
	{
		return _jailTotal;
	}
	
	/**
	 * Sets the jailed counter.
	 * 
	 * @param jailTotal
	 *            total times jailed
	 */
	public final void setJailTotal(int jailTotal)
	{
		_jailTotal = jailTotal;
	}
	
	/** @return the jail reason */
	public final String getJailReason()
	{
		return _jailReason;
	}
	
	/**
	 * Set the last jail reason.
	 * 
	 * @param jailReason
	 *            the jail reason to set
	 */
	public final void setJailReason(String jailReason)
	{
		_jailReason = jailReason;
		updateJail();
	}
	
	/** @return the godlike status */
	public final boolean isGodlike()
	{
		return _godlike;
	}
	
	/**
	 * Sets the godlike status.
	 * 
	 * @param godlike
	 *            status
	 */
	public final void setGodlike(boolean godlike)
	{
		_godlike = godlike;
	}
	
	/**
	 * Returns a L2PlayerData's KillStreak class.
	 * 
	 * @return
	 */
	public final KillStreak getKillStreak()
	{
		return _killStreak;
	}
	
	/**
	 * Contains the received damage map and killed players along with their last kill time.
	 * 
	 * @author savormix
	 * @since 2010.03.07
	 */
	public class KillStreak
	{
		private final FastMap<Integer, Integer> _damageByPlayer;
		private final FastMap<Integer, PvpInfo> _penalty;
		private final FastList<KillTime> _sessionKills;
		
		public KillStreak()
		{
			_damageByPlayer = new FastMap<Integer, Integer>().setShared(true);
			_penalty = new FastMap<Integer, PvpInfo>().setShared(true);
			_sessionKills = new FastList<KillTime>();
		}
		
		/**
		 * Adds the damage made by another player.
		 * 
		 * @param attacker
		 *            the player that dealt the damage
		 */
		public void onHit(L2Character attacker, int dmg)
		{
			if (!(attacker instanceof L2Playable) || attacker == getActingPlayer() || getActingPlayer().isDead())
				return;
			Integer id = attacker.getActingPlayer().getObjectId();
			Integer currDmg = _damageByPlayer.remove(id);
			if (currDmg == null)
				currDmg = 0;
			_damageByPlayer.put(id, currDmg + dmg);
		}
		
		/**
		 * Distribute rewards to nearby players that caused damage to this player.<BR>
		 * To prevent advanced farming, killer's penalty multiplier affects everyone.
		 * (Means someone just keeps dealing the last hit and another guy keeps dealing
		 * 90% damage to farm rewards).
		 * 
		 * @param killer
		 *            Player that made the last hit.
		 * @param penaltyRate
		 *            Killer's penalty multiplier.
		 */
		public final void distributeRewards(L2PcInstance killer, double penaltyRate, int targetKc)
		{
			if (penaltyRate > 0)
			{
				double totalDmg = 0;
				Iterable<L2Playable> chars = getActingPlayer().getKnownList().getKnownPlayableInRadius(2500);
				for (L2Playable c : chars)
				{
					L2PcInstance player = c.getActingPlayer();
					if (player.isDead() || !_damageByPlayer.containsKey(player.getObjectId()))
						continue;
					totalDmg += _damageByPlayer.get(player.getObjectId());
				}
				for (L2Playable c : chars)
				{
					L2PcInstance player = c.getActingPlayer();
					if (player.isDead() || !_damageByPlayer.containsKey(player.getObjectId()))
						continue;
					Calc.rewardKiller(player, getActingPlayer(), (_damageByPlayer.get(player.getObjectId()) / totalDmg)
							* penaltyRate, player == killer, targetKc);
				}
			}
			_damageByPlayer.clear();
		}
		
		public final PvpInfo getPenalty(Integer victimId)
		{
			return _penalty.get(victimId);
		}
		
		public final void addPenalty(Integer victimId, PvpInfo pen)
		{
			_penalty.put(victimId, pen);
			long now = System.currentTimeMillis();
			_sessionKills.addLast(new KillTime(victimId, now));
			int re = 0, demi = 0;
			for (FastList.Node<KillTime> n = _sessionKills.head(), end = _sessionKills.tail(); (n = n.getNext()) != end;)
			{
				KillTime kt = n.getValue();
				if (now - kt.getTimestamp() < 6000)
					re++;
				if (getLastAoE() - kt.getTimestamp() < 30000)
					demi++;
			}
			trySetChallengePoints(ChallengeTemplate.ROUGH_ECONOMY, re);
			trySetChallengePoints(ChallengeTemplate.DEMIGOD, demi);
		}
		
		public final void deleteMe()
		{
			_damageByPlayer.clear();
			_penalty.clear();
			_sessionKills.clear();
		}
	}
	
	public static class KillTime
	{
		private final Integer _objectId;
		private final long _timestamp;
		
		private KillTime(Integer objectId, long timestamp)
		{
			_objectId = objectId;
			_timestamp = timestamp;
		}
		
		public Integer getObjectId()
		{
			return _objectId;
		}
		
		public long getTimestamp()
		{
			return _timestamp;
		}
	}
	
	private static final ExShowScreenMessage KJ = new ExShowScreenMessage("Killjoy !", 3000);
	
	public void doDie(L2Character killer)
	{
		L2PcInstance player = getActingPlayer();
		int streak = player.getPvpKills();
		if (killer instanceof L2ZoneSpawnGuardInstance)
		{
			long lost = Calc.calcAdenaLost(player);
			if (lost > 0)
			{
				player.reduceAdena("Spawn Guard", lost, killer, false);
				player.sendMessage("You have lost " + lost + " adena by dying to a veteran!");
			}
		}
		else if (killer instanceof L2ControllableMobInstance)
		{
			L2Character owner = ((L2ControllableMobAI) killer.getAI()).getOwner();
			L2PcInstance ownerPc = null;
			if (owner != null)
				ownerPc = owner.getActingPlayer();
			if (ownerPc != null)
				ownerPc.setPkKills(ownerPc.getPkKills() + 1);
		}
		else if (killer instanceof L2Playable)
		{
			L2PcInstance killOwner = killer.getActingPlayer();
			if (!killOwner.isGM() && !player.isGM() && player.getPvpKills() > 4)
			{
				ChatMessage cs = new ChatMessage(Chat.COMMANDER, Calc.PVP, player.getName() + " was stopped by "
						+ killOwner.getName() + " after " + streak + " kills.");
				Broadcast.toAllOnlinePlayers(cs);
				killOwner.sendPacket(KJ);
			}
		}
		setLastStand(null);
		L2MartyrInstance.dropExplosive(player);
		player.setPkKills(player.getPkKills() + streak);
		player.setPvpKills(0);
		player.stopSpecialEffect(SpecialEffect.S_UNKNOWN9);
		Record rec = getRecords();
		rec.increaseDeathCount();
		rec.setMaxStreak(streak);
		updateRecords(false);
		removeFollowers();
		activatePerks();
		trySetChallengePoints(ChallengeTemplate.RIOT_CONTROL, 0);
		tryAddChallengePoints(ChallengeTemplate.FRAGILE, 1);
		player.broadcastUserInfo();
	}
	
	private void removeFollowers()
	{
		for (int i = 0; i < getFollowers().length; i++)
		{
			MobGroup mg = getFollowers()[i];
			if (mg != null)
			{
				mg.setIdleMode();
				mg.unspawnGroup();
				getFollowers()[i] = null;
			}
		}
	}
	
	public int lastHit(L2PcInstance attacker)
	{
		L2PcInstance player = getActingPlayer();
		int res = (int) player.getStat().calcStat(Stats.IGNORE_DEATH_TIME, 0, null, null);
		if (res < 1 && attacker != null)
		{
			L2PlayerData dat = attacker.getSoDPlayer();
			L2Skill sk = attacker.getAI().getCastSkill();
			if (sk != null)
			{
				if (sk.getCastRange() < 200)
				{
					dat.tryAddChallengePoints(ChallengeTemplate.SURGEON, 1);
					dat.tryUpgradePerk(13, 1); // Ninja
				}
				else
					dat.tryAddChallengePoints(ChallengeTemplate.BLOODY_TOSSER, 1);
			}
			else
			{
				L2ItemInstance weapon = attacker.getInventory().getPaperdollItem(Inventory.PAPERDOLL_RHAND);
				if (weapon != null && weapon.getItemType() != L2WeaponType.BOW
						&& weapon.getItemType() != L2WeaponType.CROSSBOW)
				{
					dat.tryAddChallengePoints(ChallengeTemplate.SURGEON, 1);
					dat.tryUpgradePerk(13, 1); // Ninja
				}
				else
					dat.tryAddChallengePoints(ChallengeTemplate.BLOODY_TOSSER, 1);
			}
			sk = player.getAI().getCastSkill();
			if (sk != null && sk.getSkillType() == L2SkillType.TAKECASTLE)
			{
				dat.tryAddChallengePoints(ChallengeTemplate.RABID, 1);
				dat.tryAddChallengePoints(ChallengeTemplate.IRON_FIST, 1);
			}
		}
		return res;
	}
	
	public void onKill(L2PcInstance target, int targetKc)
	{
		L2PcInstance player = getActingPlayer();
		if (target == player)
			return;
		PvpInfo inf = getKillStreak().getPenalty(target.getObjectId());
		
		// double penalty = 1;
		if (inf == null)
		{
			inf = new PvpInfo();
			getKillStreak().addPenalty(target.getObjectId(), inf);
		}
		// else
		// penalty = inf.getPenalty();
		
		target.getSoDPlayer().getKillStreak().distributeRewards(player, /*penalty*/1, targetKc);
		/*
		if (penalty == 0)
			return;
		else
		 */
		inf.update();
		player.setPvpKills(player.getPvpKills() + 1);
		TitleManager.getInstance().updateTitle(player, false);
		tryAddChallengePoints(ChallengeTemplate.FIRST_BLOOD, 1);
		trySetChallengePoints(ChallengeTemplate.SPRAY_N_PRAY, player.getPvpKills());
		trySetChallengePoints(ChallengeTemplate.RIOT_CONTROL, player.getPvpKills());
		if (isNearDeath())
		{
			tryAddChallengePoints(ChallengeTemplate.BRINK, 1);
			tryAddChallengePoints(ChallengeTemplate.DOWN_NOT_OUT, 1);
		}
		tryUpgradePerk(2, 1); // Sleight of Hand
		tryUpgradePerk(4, 1); // One Man Army
		tryUpgradePerk(5, 1); // Stopping Power
		tryUpgradePerk(11, 1); // Steady Aim
		if (player.getPvpKills() % 5 == 0)
		{
			player.startSpecialEffect(SpecialEffect.S_UNKNOWN9);
			Broadcast.toAllOnlinePlayers(new ChatMessage(Chat.COMMANDER, Calc.PVP, player.getAppearance()
					.getVisibleName() + " is on a " + player.getPvpKills() + " kill spree!"));
			if (player.getPvpKills() > 10)
				addFollower();
		}
		processItemKills(player);
		PerkTemplate pt = PerkManager.getInstance().getTemplate(3); // Scavenger
		Perk p = getActivePerks()[pt.getSlot()];
		if (p != null && p.getTemplate() == pt)
		{
			L2ItemInstance scav = new L2ItemInstance(IdFactory.getInstance().getNextId(), 90001);
			scav.setScavengedId(target.getObjectId());
			scav.dropMe(target, target.getX(), target.getY(), target.getZ());
		}
		L2Clan clan = player.getClan();
		L2Clan loser = target.getClan();
		if (clan != null && loser != null && clan.isAtWarWith(loser.getClanId()) && loser.isAtWarWith(clan.getClanId()))
		{
			if (loser.getReputationScore() > 0)
				clan.setReputationScore(clan.getReputationScore() + Config.REPUTATION_SCORE_PER_KILL, true);
			if (clan.getReputationScore() > 0)
				loser.setReputationScore(loser.getReputationScore() - Config.REPUTATION_SCORE_PER_KILL, true);
		}
	}
	
	public void processItemKills(L2PcInstance player)
	{
		L2ItemInstance weapon = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_RHAND);
		if (weapon != null)
		{
			int grade = weapon.getItem().getItemGrade();
			if (grade > L2Item.CRYSTAL_S)
			{
				player.sendPacket(SystemMessageId.ITEM_ENCHANT_VALUE_STRANGE);
				return;
			}
			int enchant = weapon.getEnchantLevel();
			weapon.incKills();
			if (Calc.calcHaveEnoughKills(enchant, weapon.getKills(), grade, true))
			{
				weapon.resetKills();
				if (enchant < Config.ENCHANT_SAFE_MAX)
					enchantItem(weapon);
				else
					player.addItem(Calc.PVP, Calc.calcEnchantScroll(grade, true), 1, weapon, true);
			}
			else
				player.sendMessage("Your weapon has made " + weapon.getKills() + " kills since last reward.");
		}
		L2ItemInstance chest = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_CHEST);
		if (chest != null)
		{
			int grade = chest.getItem().getItemGrade();
			if (grade > L2Item.CRYSTAL_S)
			{
				player.sendPacket(SystemMessageId.ITEM_ENCHANT_VALUE_STRANGE);
				return;
			}
			int enchant = Calc.calcMaxArmorEnchant(player, grade);
			chest.incKills();
			if (Calc.calcHaveEnoughKills(enchant, chest.getKills(), grade, false))
			{
				chest.resetKills();
				player.addItem(Calc.PVP, Calc.calcEnchantScroll(grade, false), 1, chest, true);
				tryAddChallengePoints(ChallengeTemplate.PAPERMAKING, 1);
			}
			else
				player.sendMessage("Your armor was stained with blood " + chest.getKills()
						+ " times since last reward.");
		}
	}
	
	private final void enchantItem(L2ItemInstance item)
	{
		int level = item.getEnchantLevel();
		SystemMessage sm;
		if (level > 0)
		{
			sm = new SystemMessage(SystemMessageId.S1_S2_SUCCESSFULLY_ENCHANTED);
			sm.addInt(level);
		}
		else
			sm = new SystemMessage(SystemMessageId.S1_SUCCESSFULLY_ENCHANTED);
		sm.addItem(item);
		item.setEnchantLevel(level + 1);
		item.setLastChange(L2ItemInstance.MODIFIED);
		getActingPlayer().sendPacket(sm);
		updateInventory(item);
		tryAddChallengePoints(ChallengeTemplate.POWERUP, 1);
	}
	
	private void updateInventory(L2ItemInstance item)
	{
		getActingPlayer().getInventory().updateInventory(item);
	}
	
	private void updateJail()
	{
		if (isDeleted())
			return;
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection(con);
			PreparedStatement statement = con
					.prepareStatement("UPDATE character_jail SET total=?,reason=? WHERE charId=?");
			statement.setInt(1, getJailTotal());
			statement.setString(2, getJailReason());
			statement.setInt(3, getObjectId());
			statement.executeUpdate();
			statement.close();
		}
		catch (SQLException e)
		{
			_log.info("Couldn't update char's (" + getObjectId() + ") jail state!", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	public final void updateDualism(boolean deletion)
	{
		if (!deletion && isDeleted())
			return;
		
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection(con);
			PreparedStatement ps = con.prepareStatement("UPDATE character_factions SET faction=? WHERE charId=?");
			ps.setInt(1, getDualismFaction());
			ps.setInt(2, getObjectId());
			ps.executeUpdate();
			ps.close();
		}
		catch (SQLException e)
		{
			_log.error("Couldn't update char's (" + getObjectId() + ") settings!", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	public final void deleteMe()
	{
		if (isDeleted())
			return;
		else
			setDeleted();
		
		_resProp = null;
		setLastStand(null);
		if (_perkEquip != null)
			_perkEquip.cancel(true);
		
		L2PcInstance player = getActingPlayer();
		RespawnManager.getInstance().removePlayer(player);
		
		GlobalEventManager.getInstance().removeVote(getObjectId());
		GlobalEvent evt = GlobalEvent.getEvent(player);
		if (evt != null)
			evt.remove(player);
		
		ConquestFactionManager.getInstance().removeMember(this);
		if (player.getPvpKills() > 0)
		{
			player.setPkKills(player.getPkKills() + player.getPvpKills());
			player.setPvpKills(0);
		}
		
		updateSettings(true);
		updateRecords(true);
		updateDualism(true);
		removeFollowers();
		activatePerks();
		for (FastMap.Entry<Integer, Perk> entry = _earnedPerks.head(), end = _earnedPerks.tail(); (entry = entry
				.getNext()) != end;)
			updatePerk(entry.getValue(), true);
		for (FastMap.Entry<ChallengeTemplate, Challenge> entry = _challenges.head(), end = _challenges.tail(); (entry = entry
				.getNext()) != end;)
			updateChallenge(entry.getValue(), true);
		_earnedPerks.clear();
		_challenges.clear();
		_questMarks.clear();
		getKillStreak().deleteMe();
		_auction.deleteMe();
		invalidateRaidRequests();
	}
	
	public final void updateSettings(boolean deletion)
	{
		if (!deletion && isDeleted())
			return;
		
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection(con);
			PreparedStatement ps = con
					.prepareStatement("UPDATE character_settings SET waypoint=?,messages=?,vote=?,ghost=?,rates=?,iso639=? WHERE charId=?");
			ps.setInt(1, getSettings().showWaypoint() ? 1 : 0);
			ps.setInt(2, getSettings().showMessages() ? 1 : 0);
			ps.setInt(3, getSettings().showVoting() ? 1 : 0);
			ps.setInt(4, getSettings().isGhostMode() ? 1 : 0);
			ps.setInt(5, getSettings().showSuccessRates());
			ps.setString(6, getSettings().getLocale().getLanguage());
			ps.setInt(7, getObjectId());
			ps.executeUpdate();
			ps.close();
		}
		catch (SQLException e)
		{
			_log.error("Couldn't update char's (" + getObjectId() + ") settings!", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	private final void updatePerk(Perk p, boolean force)
	{
		if (p == null || !p.hasChanged())
			return;
		if (!force && isDeleted())
			return;
		if (!force && !p.canSave())
			return;
		
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con
					.prepareStatement("UPDATE character_perks SET points=?,active=? WHERE charId=? AND perkId=?");
			ps.setInt(1, p.getPoints());
			ps.setInt(2, p.isActive() ? 1 : 0);
			ps.setInt(3, getObjectId());
			ps.setInt(4, p.getTemplate().getId());
			ps.executeUpdate();
			ps.close();
			p.setSaved();
		}
		catch (Exception e)
		{
			_log.error("Couldn't update char's (" + getObjectId() + ") perk " + p + "!", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	/**
	 * Unlocks a perk for this player. Does nothing if the perk is already unlocked.
	 * Newly unlocked perk will be automatically equipped if the relevant perk slot is empty.
	 * 
	 * @param id
	 *            Perk ID
	 */
	public final void unlockPerk(Integer id)
	{
		if (_earnedPerks.containsKey(id) || isDeleted())
			return;
		
		Perk p = new Perk(id, 0);
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con
					.prepareStatement("INSERT INTO character_perks (charId,perkId,active) VALUES (?,?,?)");
			ps.setInt(1, getObjectId());
			ps.setInt(2, p.getTemplate().getId());
			ps.setInt(3, p.isActive() ? 1 : 0);
			ps.executeUpdate();
			ps.close();
			_earnedPerks.put(id, p);
			showQuestionMark(ShowTutorialMark.PERK_UNLOCK_OFFSET + id);
			// Equip newly gained perk instantly if applicable
			if (getPerkInSlot(p.getTemplate().getSlot()) == null)
				activatePerk(id);
		}
		catch (Exception e)
		{
			_log.error("Couldn't add unlocked perk " + p + " to char (" + getObjectId() + ")!", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	public final FastMap<Integer, Perk> getEarnedPerks()
	{
		return _earnedPerks;
	}
	
	private final void activatePerks()
	{
		if (_perkEquip != null)
		{
			_perkEquip.cancel(false);
			_perkEquip = null;
		}
		for (int i = 0; i < getNextPerks().length; i++)
		{
			Perk p = getNextPerks()[i];
			if (p != null)
			{
				getNextPerks()[i] = null;
				activatePerk(p.getTemplate().getId());
			}
		}
	}
	
	public final boolean schedulePerkEquip()
	{
		if (_perkEquip != null)
			return true;
		int delay = (int) (getActingPlayer().getStat().calcStat(Stats.PERK_CHANGING_DELAY, 0, null, null));
		if (delay > 0)
		{
			_perkEquip = ThreadPoolManager.getInstance().scheduleGeneral(new Runnable()
			{
				@Override
				public final void run()
				{
					activatePerks();
				}
			}, delay * 1000);
			return true;
		}
		else
			return false;
	}
	
	public final void onChangeClassBegin()
	{
		if (_perkEquip != null)
		{
			_perkEquip.cancel(true);
			_perkEquip = null;
		}
		Skills.removeCustomSkills(getActingPlayer());
		// System.arraycopy(getActivePerks(), 0, getNextPerks(), 0, getNextPerks().length);
		for (int i = 0; i < _activePerks.length; i++)
		{
			removePerkBonus(_activePerks[i]);
			// _activePerks[i] = null;
		}
	}
	
	public final void onChangeClassFinish()
	{
		L2PcInstance player = getActingPlayer();
		Skills.addCustomSkills(player);
		// activatePerks();
		for (int i = 0; i < _activePerks.length; i++)
			applyPerkBonus(_activePerks[i], false);
		if (player.getLevel() < Config.STARTING_LEVEL)
			player.getStat().addLevel((byte) (Config.STARTING_LEVEL - player.getLevel()));
		BuffManager.getInstance().buff(this);
	}
	
	public final FastMap<ChallengeTemplate, Challenge> getChallenges()
	{
		return _challenges;
	}
	
	public final void tryAddChallengePoints(ChallengeTemplate chall, int points)
	{
		Challenge ch = getChallenges().get(chall);
		trySetChallengePoints(chall, points + (ch == null ? 0 : ch.getPoints()));
	}
	
	public final void trySetChallengePoints(ChallengeTemplate chall, int newPoints)
	{
		if (!chall.isEnabled())
			return;
		Challenge ch = getChallenges().get(chall);
		if (ch == null)
		{
			ch = new Challenge(chall, newPoints);
			getChallenges().put(chall, ch);
			storeChallenge(ch);
		}
		else if (!ch.isUpgraded())
		{
			ch.setPoints(newPoints);
			updateChallenge(ch, false);
		}
		else
			return;
		if (ch.isUpgraded())
			rewardForChallenge(chall, false);
	}
	
	private final void rewardForChallenge(ChallengeTemplate ct, boolean skillOnly)
	{
		if (!ct.isEnabled())
			return;
		Challenge ch = getChallenges().get(ct);
		if (ch == null || !ch.isUpgraded())
			return;
		
		L2PcInstance player = getActingPlayer();
		for (Rewardable r : ct.getRewards())
			r.giveReward(player, skillOnly);
	}
	
	private void loadChallenges()
	{
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con
					.prepareStatement("SELECT challenge,points FROM character_challenges WHERE charId=?");
			ps.setInt(1, getObjectId());
			ResultSet rs = ps.executeQuery();
			while (rs.next())
			{
				ChallengeTemplate temp = ChallengeTemplate.getTemplate(rs.getInt("challenge"));
				Challenge chall = new Challenge(temp, rs.getInt("points"));
				getChallenges().put(temp, chall);
				rewardForChallenge(temp, true);
			}
			rs.close();
			ps.close();
		}
		catch (Exception e)
		{
			_log.error("Couldn't restore char's (" + getObjectId() + ") challenge data!", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	private void storeChallenge(Challenge chall)
	{
		if (isDeleted())
			return;
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con
					.prepareStatement("INSERT INTO character_challenges (charId,challenge,points) VALUES (?,?,?)");
			ps.setInt(1, getObjectId());
			ps.setInt(2, chall.getTemplate().getId());
			ps.setInt(3, chall.getPoints());
			ps.executeUpdate();
			ps.close();
			chall.setSaved();
		}
		catch (Exception e)
		{
			_log.error("Couldn't store char's (" + getObjectId() + ") new challenge data!", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	private void updateChallenge(Challenge chall, boolean force)
	{
		if (chall == null || !chall.hasChanged())
			return;
		if (!force && isDeleted())
			return;
		if (!force && !chall.canSave())
			return;
		
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con
					.prepareStatement("UPDATE character_challenges SET points=? WHERE charId=? AND challenge=?");
			ps.setInt(1, chall.getPoints());
			ps.setInt(2, getObjectId());
			ps.setInt(3, chall.getTemplate().getId());
			ps.executeUpdate();
			ps.close();
			chall.setSaved();
		}
		catch (Exception e)
		{
			_log.error("Couldn't update char's (" + getObjectId() + ") challenge " + chall + "!", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	/** @return whether player is low on HP */
	public final boolean isNearDeath()
	{
		return _nearDeath;
	}
	
	/**
	 * Marks this player as low on HP
	 * 
	 * @param nearDeath
	 *            HP bar flashing status
	 */
	public final void setNearDeath(boolean nearDeath)
	{
		_nearDeath = nearDeath;
		if (!nearDeath)
		{
			trySetChallengePoints(ChallengeTemplate.BRINK, 0);
			Challenge dno = getChallenges().get(ChallengeTemplate.DOWN_NOT_OUT);
			if (dno != null)
			{
				int pts = dno.getPoints();
				trySetChallengePoints(ChallengeTemplate.DOWN_NOT_OUT, pts - pts % 3);
			}
		}
	}
	
	/** @return whether this player is in last stand */
	public final boolean isLastStand()
	{
		return _lastStand != null;
	}
	
	/**
	 * Set the postponed death task.
	 * 
	 * @param lastStand
	 *            the last stand end task
	 */
	public final void setLastStand(ScheduledFuture<?> task)
	{
		if (_lastStand != null)
			_lastStand.cancel(false);
		_lastStand = task;
	}
	
	public final void showQuestionMark(Integer id)
	{
		synchronized (_questMarks)
		{
			if (_questMarks.isEmpty())
				getActingPlayer().sendPacket(new ShowTutorialMark(id));
			_questMarks.add(id);
		}
	}
	
	public final void questMarkViewed(Integer id)
	{
		synchronized (_questMarks)
		{
			_questMarks.remove(id);
			if (!_questMarks.isEmpty())
				ThreadPoolManager.getInstance().scheduleGeneral(
						new DelayedPacket(new ShowTutorialMark(_questMarks.get(_questMarks.size() - 1))),
						ShowTutorialMark.QUEUED_DELAY);
		}
	}
	
	private final class DelayedPacket implements Runnable
	{
		private final L2ServerPacket _packet;
		
		private DelayedPacket(L2ServerPacket packet)
		{
			_packet = packet;
		}
		
		@Override
		public final void run()
		{
			getActingPlayer().sendPacket(_packet);
		}
	}
	
	private final void addFollower()
	{
		int max = getFollowers().length;
		for (int i = 0; i < max; i++)
		{
			if (getFollowers()[i] != null)
				continue;
			L2NpcTemplate temp = NpcTable.getInstance().getTemplate(90013);
			MobGroup mg = new MobGroup(getObjectId().intValue() * max + i, temp, 1);
			getFollowers()[i] = mg;
			mg.spawnGroup(getActingPlayer());
			((L2ControllableMobAI) mg.getRandomMob().getAI()).setOwner(getActingPlayer());
			tryAddChallengePoints(ChallengeTemplate.BEASTMASTER, 1);
			break;
		}
	}
	
	public final MobGroup[] getFollowers()
	{
		return _followers;
	}
	
	public final void setReviveProposer(L2PcInstance player)
	{
		_resProp = player;
	}
	
	public final L2PcInstance getReviveProposer()
	{
		return _resProp;
	}
	
	public final void onReviveProposal(L2PcInstance proposer)
	{
		if (proposer == null || proposer == getActingPlayer())
			return;
		setReviveProposer(proposer);
	}
	
	public long getLastAoE()
	{
		return _lastAoE;
	}
	
	public void setLastAoE(long lastAoE)
	{
		_lastAoE = lastAoE;
	}
	
	public L2ZoneArtefactInstance getArtefact()
	{
		return _artefact;
	}
	
	public void setArtefact(L2ZoneArtefactInstance artefact)
	{
		_artefact = artefact;
	}
	
	public PcAuction getAuction()
	{
		return _auction;
	}
	
	public PlayerBuffs getBuffs()
	{
		return _buffs;
	}
	
	public PlayerSettings getSettings()
	{
		return _settings;
	}
	
	/**
	 * If this object is marked as obsolete, it does no longer represent
	 * a player object. In a narrow sense, the database should not be
	 * queried based on this object's data.
	 * 
	 * @return the deletion status
	 */
	private boolean isDeleted()
	{
		return _deleted;
	}
	
	/** Marks this data object as obsolete. */
	private void setDeleted()
	{
		_deleted = true;
	}
	
	private static class RaidRequest
	{
		private final int _playerId;
		private final int _raidId;
		private final boolean _party;
		
		private RaidRequest(int playerId, int raidId, boolean party)
		{
			_playerId = playerId;
			_raidId = raidId;
			_party = party;
		}
		
		/**
		 * @return the playerId
		 */
		public int getPlayerId()
		{
			return _playerId;
		}
		
		/**
		 * @return the raidId
		 */
		public int getRaidId()
		{
			return _raidId;
		}
		
		/**
		 * @return the party
		 */
		public boolean isParty()
		{
			return _party;
		}
	}
	
	public void addRaidRequest(L2PcInstance player, L2Party party, int raid)
	{
		if (_raidRequests == null)
		{
			_raidRequests = FastMap.newInstance();
			_raidRequests.setShared(true);
		}
		_raidRequests.put(player.getObjectId(), new RaidRequest(player.getObjectId(), raid, party != null));
		showQuestionMark(ShowTutorialMark.RAID_REQUEST);
	}
	
	public void showRaidRequest()
	{
		L2RaidTeleporterInstance.remPendingRequest(getObjectId(), false);
		L2PcInstance leader = getActingPlayer();
		if (_raidRequests == null || _raidRequests.isEmpty())
		{
			leader.sendPacket(L2RaidTeleporterInstance.INVALID_REQUEST);
			return;
		}
		Integer pid = _raidRequests.keySet().iterator().next();
		RaidRequest rr = _raidRequests.remove(pid);
		if (_raidRequests.isEmpty())
		{
			FastMap<?, ?> re = _raidRequests;
			_raidRequests = null;
			FastMap.recycle(re);
		}
		L2PcInstance player = L2World.getInstance().findPlayer(pid);
		if (player == null)
		{
			leader.sendPacket(L2RaidTeleporterInstance.INVALID_REQUEST);
			return;
		}
		L2Party party = player.getParty();
		if (rr.isParty() && (party == null || !party.isLeader(player)))
		{
			leader.sendPacket(L2RaidTeleporterInstance.INVALID_REQUEST);
			player.getSoDPlayer().showQuestionMark(ShowTutorialMark.RAID_REQUEST_INVALIDATED);
			return;
		}
		else if (!rr.isParty() && party != null)
		{
			leader.sendPacket(L2RaidTeleporterInstance.INVALID_REQUEST);
			player.getSoDPlayer().showQuestionMark(ShowTutorialMark.RAID_REQUEST_INVALIDATED);
			return;
		}
		BossSpawnManager bsm = RaidBossSpawnManager.getInstance();
		if (!bsm.isDefined(rr.getRaidId()))
			bsm = GrandBossSpawnManager.getInstance();
		if (bsm.getRaidBossStatusId(rr.getRaidId()) != StatusEnum.ALIVE)
		{
			leader.sendPacket(L2RaidTeleporterInstance.INVALID_REQUEST);
			player.getSoDPlayer().showQuestionMark(ShowTutorialMark.RAID_REQUEST_INVALIDATED);
			return;
		}
		L2Npc npc = bsm.getBosses().get(rr.getRaidId());
		double percent = npc.getCurrentHp() * 100 / npc.getMaxHp();
		if (percent < 5)
		{
			leader.sendPacket(L2RaidTeleporterInstance.INVALID_REQUEST);
			player.getSoDPlayer().showQuestionMark(ShowTutorialMark.RAID_REQUEST_INVALIDATED);
			return;
		}
		L2TextBuilder sb = new L2TextBuilder();
		sb.append("<html><title>Raid Manager</title><body><center><img ");
		sb.append("src=\"L2UI_CH3.herotower_deco\" width=256 height=32><br>A ");
		if (rr.isParty())
			sb.append("party");
		else
			sb.append("player");
		sb.append(" requested to participate in the raid.<br>");
		if (rr.isParty())
			sb.append("Leader");
		else
			sb.append("Name");
		sb.append(": <font color=\"LEVEL\">");
		sb.append(player.getName());
		sb.append("</font>");
		if (party != null)
		{
			sb.append("<br1>Members:");
			for (L2PcInstance p : party.getPartyMembersWithoutLeader())
			{
				sb.append(' ');
				sb.append(p.getName());
			}
		}
		sb.append("<br><a action=\"bypass -h raidman_allow ");
		sb.append(rr.getRaidId());
		sb.append(' ');
		sb.append(rr.getPlayerId());
		sb.append("\">Accept.</a><br><a action=\"bypass -h raidman_deny ");
		sb.append(rr.getRaidId());
		sb.append(' ');
		sb.append(rr.getPlayerId());
		sb.append("\">Refuse.</a><br><img src=\"L2UI_CH3.herotower_deco\" width=256 height=32></center></body></html>");
		L2RaidTeleporterInstance.addPendingRequest(getObjectId(), pid);
		leader.sendPacket(new ShowTutorialHtml(sb.moveToString()));
	}
	
	public boolean isRaidRequested(L2PcInstance requester)
	{
		if (_raidRequests == null || _raidRequests.isEmpty())
			return false;
		else
			return _raidRequests.containsKey(requester.getObjectId());
	}
	
	public void invalidateRaidRequests()
	{
		L2RaidTeleporterInstance.remPendingRequest(getObjectId(), true);
		if (_raidRequests != null && !_raidRequests.isEmpty())
		{
			for (Integer pid : _raidRequests.keySet())
			{
				L2PcInstance requester = L2World.getInstance().findPlayer(pid);
				if (requester != null)
					requester.sendPacket(L2RaidTeleporterInstance.INVALIDATED_REQUEST);
			}
			FastMap.recycle(_raidRequests);
			_raidRequests = null;
		}
	}
	
	public static class Record extends OptimizedSavable
	{
		private volatile int _deathCount;
		private volatile int _maxStreak;
		
		private Record(int deathCount, int maxStreak)
		{
			_deathCount = deathCount;
			_maxStreak = maxStreak;
		}
		
		public int getDeathCount()
		{
			return _deathCount;
		}
		
		public void increaseDeathCount()
		{
			_deathCount++;
			setChanged();
		}
		
		public int getMaxStreak()
		{
			return _maxStreak;
		}
		
		public void setMaxStreak(int maxStreak)
		{
			if (getMaxStreak() < maxStreak)
			{
				_maxStreak = maxStreak;
				setChanged();
			}
		}
	}
	
	/**
	 * @return the records
	 */
	public Record getRecords()
	{
		return _records;
	}
	
	private void loadRecords()
	{
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection(con);
			PreparedStatement ps = con
					.prepareStatement("SELECT deaths,maxStreak FROM character_records WHERE charId=?");
			ps.setInt(1, getObjectId());
			ResultSet rs = ps.executeQuery();
			if (rs.next())
				_records = new Record(rs.getInt("deaths"), rs.getInt("maxStreak"));
			else
				_log.warn("Missing records for " + getActingPlayer());
			rs.close();
			ps.close();
		}
		catch (SQLException e)
		{
			_log.error("Couldn't restore char's (" + getObjectId() + ") jail data!", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	public void updateRecords(boolean force)
	{
		Record rec = getRecords();
		if (rec == null || !rec.hasChanged())
			return;
		if (!force && isDeleted())
			return;
		if (!force && !rec.canSave())
			return;
		
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con
					.prepareStatement("UPDATE character_records SET deaths=?, maxStreak=? WHERE charId=?");
			ps.setInt(1, rec.getDeathCount());
			ps.setInt(2, rec.getMaxStreak());
			ps.setInt(3, getObjectId());
			ps.executeUpdate();
			ps.close();
			rec.setSaved();
		}
		catch (Exception e)
		{
			_log.error("Couldn't update char's (" + getObjectId() + ") records!", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	/** @return time of last player's action */
	private long getLastActionTime()
	{
		return _lastAction;
	}
	
	/** @return whether this player is afk */
	public boolean isAway()
	{
		return System.currentTimeMillis() > (getLastActionTime() + Config.CONQUEST_AWAY_THRESHOLD);
	}
	
	/** Stores action time. */
	public void markAction()
	{
		_lastAction = System.currentTimeMillis();
		_inactivityTicks = 0;
		ConquestFactionManager.getInstance().reactivate(this);
	}
	
	/** @return Inactivator's tick count while being inactive */
	public int getInactivityTicks()
	{
		return _inactivityTicks;
	}
	
	/** Increase inactivity tick count */
	public void tickInactive()
	{
		_inactivityTicks++;
	}
	
	/** @return whether the player is in ghost mode */
	public boolean isInGhostMode()
	{
		return _inGhostMode;
	}
	
	/**
	 * Set player's movie mode status.
	 * 
	 * @param inGhostMode
	 *            whether the player is in ghost mode
	 */
	public void setInGhostMode(boolean inGhostMode)
	{
		_inGhostMode = inGhostMode;
	}
	
	@Override
	public L2Character getActingCharacter()
	{
		return getActingPlayer();
	}
	
	public void setDualismFaction(int faction)
	{
		_dualismFactionId = faction;
	}
	
	public int getDualismFaction()
	{
		return _dualismFactionId;
	}
	
	public void setConquestFaction(int faction)
	{
		_conquestFactionId = faction;
	}
	
	@Override
	public int getConquestFaction()
	{
		return _conquestFactionId;
	}
	
	public void sendLocalizedMessage(String key, String... repVals)
	{
		final String s = Internationalization.getInstance().get(getSettings().getLocale(), key, repVals);
		getActingPlayer().sendMessage(s);
	}
	
	public void sendLocalizedScreenMessage(int pos, boolean small, boolean deco, int duration, String key,
			String... repVals)
	{
		PlayerSettings pset = getSettings();
		if (!pset.showMessages())
			return;
		
		ExShowScreenMessage sm = new ExShowScreenMessage(true, null, pos, 0, small, 0, 1, deco, duration, false,
				Internationalization.getInstance().get(pset.getLocale(), key, repVals));
		getActingPlayer().sendPacket(sm);
	}
	
	// STATIC METHODS BELOW
	
	/**
	 * Creates all needed database entries for a newly created character.
	 * 
	 * @param charId
	 *            character's object ID
	 */
	public static void createRecord(int charId)
	{
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement("INSERT INTO character_jail (charId) VALUES (?)");
			ps.setInt(1, charId);
			ps.executeUpdate();
			ps.close();
			ps = con.prepareStatement("INSERT INTO character_settings (charId) VALUES (?)");
			ps.setInt(1, charId);
			ps.executeUpdate();
			ps.close();
			ps = con.prepareStatement("INSERT INTO character_perks (charId,perkId,active) VALUES (?,?,?)");
			ps.setInt(1, charId);
			ps.setInt(3, 1);
			ps.setInt(2, 1); // Marathon
			ps.executeUpdate();
			ps.setInt(2, 5); // Stopping power
			ps.executeUpdate();
			ps.setInt(2, 10); // Commando
			ps.executeUpdate();
			ps.close();
			ps = con.prepareStatement("INSERT INTO character_records (charId) VALUES (?)");
			ps.setInt(1, charId);
			ps.executeUpdate();
			ps = con.prepareStatement("INSERT INTO character_tutorial (charId) VALUES (?)");
			ps.setInt(1, charId);
			ps.executeUpdate();
			ps = con.prepareStatement("INSERT INTO character_factions (charId) VALUES (?)");
			ps.setInt(1, charId);
			ps.executeUpdate();
			ps.close();
		}
		catch (Exception e)
		{
			_log.error("Failed creating SoD data record for " + charId, e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
}
