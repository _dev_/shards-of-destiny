/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model;

import com.l2jfree.lang.L2Math;

/**
 * TODO Auto-generated JavaDoc
 * @author savormix
 * @since 2010.01.26
 */
public abstract class PointCollectable extends OptimizedSavable
{
	private final PointTemplate	_template;
	private volatile int		_points;
	private volatile boolean	_upgraded;

	protected PointCollectable(PointTemplate template, int points)
	{
		_template = template;
		_points = points;
		refreshUpgraded();
	}

	/** @return currently acquired point count */
	public int getPoints()
	{
		return _points;
	}

	/**
	 * Set the new point count
	 * @param points new point count
	 */
	public void setPoints(int points)
	{
		if (_points != points)
		{
			_points = L2Math.limit(0, points, getTemplate().getNeededPoints());
			refreshUpgraded();
			setChanged();
		}
	}

	public boolean isUpgraded()
	{
		return _upgraded;
	}

	protected void setUpgraded(boolean upgraded)
	{
		_upgraded = upgraded;
	}

	protected void refreshUpgraded()
	{
		setUpgraded(getPoints() >= getTemplate().getNeededPoints());
	}

	public PointTemplate getTemplate()
	{
		return _template;
	}
}
