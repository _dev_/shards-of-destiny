/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.model;

import com.l2jfree.gameserver.model.L2ItemInstance.ItemLocation;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.itemcontainer.ItemContainer;

/**
 * @author savormix
 */
public final class PcAuction extends ItemContainer
{
	private final L2PcInstance _owner;

	public PcAuction(L2PcInstance owner)
	{
		_owner = owner;
	}

	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.model.itemcontainer.ItemContainer#getBaseLocation()
	 */
	@Override
	protected ItemLocation getBaseLocation()
	{
		return ItemLocation.AUCTION;
	}

	/* (non-Javadoc)
	 * @see com.l2jfree.gameserver.model.itemcontainer.ItemContainer#getOwner()
	 */
	@Override
	protected L2Character getOwner()
	{
		return _owner;
	}
}
