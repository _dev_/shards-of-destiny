/*
 * MISSING LICENSING INFO
 */
package org.sod.buffs;

import java.util.Collection;

import javolution.util.FastSet;

/**
 * @author savormix
 * 
 */
public class BuffTemplate
{
	private final BuffTemplateType _type;
	private final Collection<BuffSkill> _buffs;
	
	private String _name;
	
	public BuffTemplate(int type, String name)
	{
		_type = BuffTemplateType.values()[type];
		_buffs = FastSet.newInstance();
		
		_name = name;
	}
	
	boolean addBuff(BuffSkill skill)
	{
		return getBuffs().add(skill);
	}
	
	boolean addBuff(int skill, int level)
	{
		return getBuffs().add(BuffSkill.newInstance(skill, level));
	}
	
	void removeBuff(int skill, int level)
	{
		// there are alternatives to this
		getBuffs().remove(BuffSkill.newInstance(skill, level));
	}
	
	public int getDanceSongCount()
	{
		// used only when player is actively modifying the templates
		int ds = 0;
		for (BuffSkill bs : getBuffs())
			if (bs.getSkill().isDanceOrSong())
				ds++;
		return ds;
	}
	
	public int getNormalBuffCount()
	{
		return getBuffs().size() - getDanceSongCount();
	}
	
	public BuffTemplateType getType()
	{
		return _type;
	}
	
	public Collection<BuffSkill> getBuffs()
	{
		return _buffs;
	}
	
	public String getName()
	{
		return _name;
	}
}
