/*
 * MISSING LICENSING INFO
 */
package org.sod.buffs;

import java.util.Collection;

/**
 * @author savormix
 * 
 */
public class PlayerBuffs
{
	private final Collection<Integer> _templates;
	private final Integer[] _active;
	
	public PlayerBuffs(Collection<Integer> templates, Integer[] active)
	{
		_templates = templates;
		_active = active;
	}
	
	public Collection<Integer> getTemplates()
	{
		return _templates;
	}
	
	public Integer getActive(BuffTemplateType type)
	{
		return getActive()[type.ordinal()];
	}
	
	public Integer[] getActive()
	{
		return _active;
	}
}
