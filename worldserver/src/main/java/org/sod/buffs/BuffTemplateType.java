/*
 * MISSING LICENSING INFO
 */
package org.sod.buffs;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.sod.i18n.Internationalization;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.l2jfree.Config;
import com.l2jfree.util.logging.L2Logger;

/**
 * @author savormix
 * 
 */
public enum BuffTemplateType
{
	FIGHTER(13, 5, 4), MYSTIC(11, 4, 3), SERVITOR(10, 4, 1);
	
	private final int _buffSlots;
	private final int _danceSongSlots;
	private final int _sharedSlots;
	private final List<BuffSkill> _buffs;
	private final List<BuffSkill> _danceSongs;
	
	private BuffTemplateType(int buffSlots, int danceSongSlots, int sharedSlots)
	{
		Collection<BuffSkill> buffs = new HashSet<BuffSkill>();
		Collection<BuffSkill> danceSongs = new HashSet<BuffSkill>();
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try
		{
			db = dbf.newDocumentBuilder();
			Document doc = db.parse(new File(Config.DATAPACK_ROOT, "data/stats/buffs.xml"));
			for (Node list = doc.getFirstChild(); list != null; list = list.getNextSibling())
			{
				if (list.getNodeName().equals("list"))
				{
					for (Node n = list.getFirstChild(); n != null; n = n.getNextSibling())
					{
						if (n.getNodeName().toUpperCase().equals(toString()))
						{
							for (Node cl = n.getFirstChild(); cl != null; cl = cl.getNextSibling())
							{
								if (cl.getNodeName().equals("choices"))
								{
									for (Node skill = cl.getFirstChild(); skill != null; skill = skill.getNextSibling())
									{
										if (skill.getNodeName().equals("skill"))
										{
											NamedNodeMap attr = skill.getAttributes();
											int id = Integer.parseInt(attr.getNamedItem("id").getNodeValue());
											int lvl = Integer.parseInt(attr.getNamedItem("level").getNodeValue());
											BuffSkill bs = BuffSkill.newInstance(id, lvl);
											if (bs.getSkill().isDanceOrSong())
												danceSongs.add(bs);
											else
												buffs.add(bs);
										}
									}
								}
								else if (cl.getNodeName().equals("slots"))
								{
									NamedNodeMap attr = cl.getAttributes();
									buffSlots = Integer.parseInt(attr.getNamedItem("normal").getNodeValue());
									danceSongSlots = Integer.parseInt(attr.getNamedItem("special").getNodeValue());
									sharedSlots = Integer.parseInt(attr.getNamedItem("shared").getNodeValue());
								}
							}
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			L2Logger.getLogger(getClass()).error("Cannot load buff template configuration!", e);
		}
		
		_buffs = new ArrayList<BuffSkill>(buffs);
		_danceSongs = new ArrayList<BuffSkill>(danceSongs);
		
		_buffSlots = buffSlots;
		_danceSongSlots = danceSongSlots;
		_sharedSlots = sharedSlots;
		L2Logger.getLogger(getClass()).info(
				"Loaded " + getBuffs().size() + " buffs, " + getDanceSongs().size() + " dances/songs for " + this
						+ " templates.");
		L2Logger.getLogger(getClass()).info(
				"Available slots: " + getBuffSlots() + " normal, " + getDanceSongSlots() + " special and "
						+ getSharedSlots() + " shared.");
	}
	
	public int getBuffSlots()
	{
		return _buffSlots;
	}
	
	public int getDanceSongSlots()
	{
		return _danceSongSlots;
	}
	
	public int getSharedSlots()
	{
		return _sharedSlots;
	}
	
	public List<BuffSkill> getBuffs()
	{
		return _buffs;
	}
	
	public List<BuffSkill> getDanceSongs()
	{
		return _danceSongs;
	}
	
	public String toString(Locale loc)
	{
		return Internationalization.getInstance().get(loc, "NPC_BUFFER_TEMPLATE_TYPE_" + this);
	}
}
