/*
 * MISSING LICENSING INFO
 */
package org.sod.buffs;

import java.lang.ref.WeakReference;
import java.util.Map;

import javolution.util.FastMap;

import com.l2jfree.gameserver.datatables.SkillTable;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.lang.L2System;

/**
 * Skill access wrapper that supports skill reloads.
 * 
 * @author savormix
 */
public final class BuffSkill
{
	private final static Map<Integer, WeakReference<BuffSkill>> INSTANCES = new FastMap<Integer, WeakReference<BuffSkill>>()
			.setShared(true);
	
	private final int _id;
	private final int _level;
	
	private BuffSkill(int id, int level)
	{
		_id = id;
		_level = level;
	}
	
	public L2Skill getSkill()
	{
		return SkillTable.getInstance().getInfo(_id, _level);
	}
	
	int getId()
	{
		return _id;
	}
	
	int getLevel()
	{
		return _level;
	}

	@Override
	public int hashCode()
	{
		return L2System.hash(SkillTable.getSkillUID(_id, _level));
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (o instanceof BuffSkill || o instanceof L2Skill)
			return hashCode() == o.hashCode(); // TEST ME
		else
			return false;
	}
	
	public static BuffSkill newInstance(int id, int level)
	{
		Integer key = SkillTable.getSkillUID(id, level);
		WeakReference<BuffSkill> ref = INSTANCES.get(key);
		BuffSkill bs = ref != null ? ref.get() : null;
		if (bs == null)
		{
			bs = new BuffSkill(id, level);
			INSTANCES.put(key, new WeakReference<BuffSkill>(bs));
		}
		return bs;
	}
}
