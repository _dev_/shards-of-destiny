/*
 * MISSING LICENSING INFO
 */
package org.sod.buffs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javolution.util.FastMap;
import javolution.util.FastSet;

import org.sod.manager.RespawnManager;
import org.sod.model.L2PlayerData;

import com.l2jfree.L2DatabaseFactory;
import com.l2jfree.gameserver.model.L2Effect;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.effects.CharEffects;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.SystemMessageId;
import com.l2jfree.gameserver.network.client.packets.sendable.SystemMessagePacket.SystemMessage;
import com.l2jfree.util.logging.L2Logger;

/**
 * @author savormix
 */
public final class BuffManager
{
	private static final L2Logger _log = L2Logger.getLogger(BuffManager.class);
	
	private final AtomicInteger _oneWayCounter;
	private final Map<Integer, BuffTemplate> _loadedTemplates;
	
	private BuffManager()
	{
		_oneWayCounter = new AtomicInteger();
		_loadedTemplates = new FastMap<Integer, BuffTemplate>().setShared(true);
		
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT MAX(id) AS id FROM character_buff_templates");
			ResultSet rs = ps.executeQuery();
			if (rs.next())
				_oneWayCounter.set(rs.getInt("id"));
			_log.info("BuffManager initialized.");
			rs.close();
			ps.close();
		}
		catch (SQLException e)
		{
			_log.error("Cannot initialize the buff template counter!", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	public PlayerBuffs loadTemplates(Integer charId)
	{
		Collection<Integer> temps = FastSet.newInstance();
		PlayerBuffs pb = new PlayerBuffs(temps, new Integer[BuffTemplateType.values().length]);
		final long[] last = new long[pb.getActive().length];
		
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con
					.prepareStatement("SELECT id, target, name, lastSelection FROM character_buff_templates WHERE charId=?");
			ps.setInt(1, charId);
			
			ResultSet rs = ps.executeQuery();
			PreparedStatement bps = con.prepareStatement("SELECT skill, level FROM character_buffs WHERE id=?");
			while (rs.next())
			{
				Integer id = rs.getInt("id");
				temps.add(id);
				
				int type = rs.getInt("target");
				BuffTemplate bt = new BuffTemplate(type, rs.getString("name"));
				long last_ = rs.getLong("lastSelection");
				if (last_ > last[type])
				{
					last[type] = last_;
					pb.getActive()[type] = id;
				}
				bps.setInt(1, id);
				ResultSet brs = bps.executeQuery();
				while (brs.next())
					bt.addBuff(brs.getInt("skill"), brs.getInt("level"));
				brs.close();
				
				_loadedTemplates.put(id, bt);
			}
			bps.close();
			rs.close();
			// SELECT id, target, MAX(lastSelection) FROM character_buff_templates WHERE charId=? GROUP BY target;
			ps.close();
		}
		catch (SQLException e)
		{
			_log.error("Cannot load buff templates for character " + charId, e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
		return pb;
	}
	
	public void unloadTemplates(Collection<Integer> temps)
	{
		for (Integer id : temps)
			_loadedTemplates.remove(id);
	}
	
	public void setActive(L2PlayerData dat, Integer template)
	{
		PlayerBuffs pb = dat.getBuffs();
		if (template == null || !pb.getTemplates().contains(template))
			return;
		
		BuffTemplate bt = getTemplate(template);
		pb.getActive()[bt.getType().ordinal()] = template;
		
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con
					.prepareStatement("UPDATE character_buff_templates SET lastSelection=? WHERE id=?");
			ps.setLong(1, System.currentTimeMillis());
			ps.setInt(2, template);
			ps.executeUpdate();
			ps.close();
		}
		catch (SQLException e)
		{
			_log.error("Cannot set active buff template " + template + " for " + dat.getActingPlayer(), e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	public void buff(L2PlayerData dat)
	{
		L2PcInstance player = dat.getActingPlayer();
		PlayerBuffs pb = dat.getBuffs();
		buff(pb.getActive(player.isMageClass() ? BuffTemplateType.MYSTIC : BuffTemplateType.FIGHTER), player);
		buff(pb.getActive(BuffTemplateType.SERVITOR), player.getActingSummon());
	}
	
	public void buff(Integer template, L2Character target)
	{
		if (template == null || target == null)
			return;
		
		BuffTemplate bt = _loadedTemplates.get(template);
		if (bt == null)
		{
			_log.warn("Buffing with an unloaded template: " + template + ", " + target);
			return;
		}
		
		CharEffects ce = target.getEffects();
		for (L2Effect e : ce.getAllEffects())
		{
			int skill = e.getId();
			if (skill >= 9060 && skill <= 9090)
				e.exit();
			else if (skill >= 9501 && skill <= 9539)
				e.exit();
		}
		
		L2PcInstance player = target.getActingPlayer();
		for (BuffSkill bs : bt.getBuffs())
		{
			L2Skill sk = bs.getSkill();
			for (L2Effect e; (e = ce.getFirstEffect(sk)) != null;)
				e.exit();
			bs.getSkill().getEffects(target, target);
			if (player != null)
				player.sendPacket(new SystemMessage(SystemMessageId.YOU_FEEL_S1_EFFECT).addSkill(sk));
		}
		
		if (RespawnManager.getInstance().isInsideBase(target))
			target.getStatus().setCurrentHpMp(target.getMaxHp(), target.getMaxMp());
		
		target.broadcastFullInfo();
	}
	
	public Integer add(int charId, BuffTemplateType type, String name)
	{
		BuffTemplate bt = new BuffTemplate(type.ordinal(), name);
		Integer id = _oneWayCounter.incrementAndGet();
		_loadedTemplates.put(id, bt);
		
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con
					.prepareStatement("INSERT INTO character_buff_templates (id,charId,target,name) VALUES (?,?,?,?)");
			ps.setInt(1, id);
			ps.setInt(2, charId);
			ps.setInt(3, type.ordinal());
			ps.setString(4, name);
			ps.executeUpdate();
			ps.close();
		}
		catch (SQLException e)
		{
			_log.error("Cannot create buff template " + name + "[" + id + "] for " + charId, e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
		return id;
	}
	
	public boolean reset(Integer template)
	{
		BuffTemplate bt = _loadedTemplates.get(template);
		if (bt == null)
		{
			_log.warn("Resetting an unloaded template: " + template);
			return false;
		}
		if (bt.getBuffs().isEmpty())
			return true; // already empty
		bt.getBuffs().clear();
		
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement("DELETE FROM character_buffs WHERE id=?");
			ps.setInt(1, template);
			ps.executeUpdate();
			ps.close();
		}
		catch (SQLException e)
		{
			_log.error("Cannot reset buff template " + template, e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
		return true;
	}
	
	public boolean remove(Integer template)
	{
		BuffTemplate bt = _loadedTemplates.remove(template);
		// this is not a pool; recover only if possible
		_oneWayCounter.compareAndSet(template, template - 1);
		
		if (bt == null)
		{
			_log.warn("Removing an unloaded template: " + template);
			return false;
		}
		
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement("DELETE FROM character_buff_templates WHERE id=?");
			ps.setInt(1, template);
			ps.executeUpdate();
			ps.close();
			ps = con.prepareStatement("DELETE FROM character_buffs WHERE id=?");
			ps.setInt(1, template);
			ps.executeUpdate();
			ps.close();
		}
		catch (SQLException e)
		{
			_log.error("Cannot remove buff template " + template, e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
		return true;
	}
	
	public boolean add(Integer template, BuffSkill skill)
	{
		BuffTemplate bt = _loadedTemplates.get(template);
		if (bt == null)
		{
			_log.warn("Modifying an unloaded template: " + template);
			return false;
		}
		if (!bt.addBuff(skill))
			return true; // already in
			
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement("INSERT INTO character_buffs (id,skill,level) VALUES (?,?,?)");
			ps.setInt(1, template);
			ps.setInt(2, skill.getId());
			ps.setInt(3, skill.getLevel());
			ps.executeUpdate();
			ps.close();
		}
		catch (SQLException e)
		{
			_log.error("Cannot modify buff template " + template, e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
		return true;
	}
	
	public boolean remove(Integer template, int skill, int level)
	{
		BuffTemplate bt = _loadedTemplates.get(template);
		if (bt == null)
		{
			_log.warn("Modifying an unloaded template: " + template);
			return false;
		}
		bt.removeBuff(skill, level);
		
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con
					.prepareStatement("DELETE FROM character_buffs WHERE id=? AND skill=? AND level=?");
			ps.setInt(1, template);
			ps.setInt(2, skill);
			ps.setInt(3, level);
			ps.executeUpdate();
			ps.close();
		}
		catch (SQLException e)
		{
			_log.error("Cannot modify buff template " + template, e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
		return true;
	}
	
	public BuffTemplate getTemplate(Integer id)
	{
		if (id == null)
			return null;
		else
			return _loadedTemplates.get(id);
	}
	
	public static BuffManager getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static final class SingletonHolder
	{
		private static final BuffManager INSTANCE = new BuffManager();
	}
}
