/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod;

import java.util.List;

import org.sod.manager.BountyManager;
import org.sod.model.ChallengeTemplate;
import org.sod.manager.LootedGoodsManager;
import org.sod.manager.RespawnManager;
import org.sod.manager.TitleManager;
import org.sod.model.L2PlayerData;

import com.l2jfree.Config;
import com.l2jfree.gameserver.datatables.ItemTable;
import com.l2jfree.gameserver.instancemanager.ZoneManager;
import com.l2jfree.gameserver.model.L2ItemInstance;
import com.l2jfree.gameserver.model.L2Party;
import com.l2jfree.gameserver.model.actor.L2Attackable.RewardItem;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.base.Experience;
import com.l2jfree.gameserver.model.itemcontainer.Inventory;
import com.l2jfree.gameserver.model.zone.L2Zone;
import com.l2jfree.gameserver.model.zone.L2Zone.ZoneType;
import com.l2jfree.gameserver.templates.item.L2Item;
import com.l2jfree.lang.L2Math;
import com.l2jfree.util.Rnd;

/**
 * SoD Related Formulas (Calculations)
 * 
 * @author savormix
 */
public final class Calc
{
	public static final int STARTER_LEVEL = 75;
	public static final String PVP = "PvP";
	private static final String LAST_HIT = "PvP - Last Hit";
	private static final int[] KILLS_FOR_LEVEL =
	{
			100, 250, 600, 1200, 5000, 7900, 13666, 17445, 21000, 33676, 50000
	};
	private static final int RES_KILL_MULTIPLIER = 5;
	// 3-7, 8-12, 13-16
	public static final int[] MAX_ENCHANT_PER_KILL_COUNT =
	{
			8, 13, 17
	};
	public static final int[] KILLS_FOR_ENCHANT_C_WEAPON =
	{
			15, 30, 60
	};
	public static final int[] KILLS_FOR_ENCHANT_C_ARMOR =
	{
			7, 14, 30
	}; // little bonus
	public static final int[] KILLS_FOR_ENCHANT_B_WEAPON =
	{
			30, 60, 120
	};
	public static final int[] KILLS_FOR_ENCHANT_B_ARMOR =
	{
			15, 30, 60
	};
	public static final int[] KILLS_FOR_ENCHANT_A_WEAPON =
	{
			60, 120, 240
	};
	public static final int[] KILLS_FOR_ENCHANT_A_ARMOR =
	{
			30, 60, 120
	};
	public static final int[] KILLS_FOR_ENCHANT_S_WEAPON =
	{
			100, 200, 300
	};
	public static final int[] KILLS_FOR_ENCHANT_S_ARMOR =
	{
			50, 100, 150
	};
	private static final int ENCHANT_C_WEAPON = 951;
	private static final int ENCHANT_C_ARMOR = 952;
	private static final int ENCHANT_B_WEAPON = 947;
	private static final int ENCHANT_B_ARMOR = 948;
	private static final int ENCHANT_A_WEAPON = 729;
	private static final int ENCHANT_A_ARMOR = 730;
	private static final int ENCHANT_S_WEAPON = 959;
	private static final int ENCHANT_S_ARMOR = 960;
	
	private static final int METRIC_UNIT = 30;
	private static final double EXP_TO_SP_RATIO = 2.7;
	// private static final int KILL_AS_HEALTH = 20000;
	// private static final int KILL_AS_MANA = 18000;
	
	public static final double BLOW_DAMAGE = 0.556;
	public static final double PDAM_DAMAGE = 0.741;
	public static final int BASE_CRIT_RATE = 5;
	
	public static void rewardKiller(L2PcInstance killer, L2PcInstance killed, double dmgRatio, boolean last,
			int targetKc)
	{
		if (killer == null || killed == null || dmgRatio == 0)
			return;
		
		// victim's owned adena (must be >=1 to enable lvldiff multiplier)
		long victimsAdena = Math.max(1, killed.getAdena() + killed.getWarehouse().getAdena());
		int level = killer.getLevel();
		// lvlDiff is using squared values to ensure a higher diff produces a real effect
		// we set a max limit to it, since 25x is already enough
		// min limit is 0, because low lvls tend to have little adena, no need
		// to strip adena earned by high levels; and the current exp
		// system gives no need for stripping exp when killing lower leveled players
		int lvlDiff = L2Math.limit(0, killed.getLevel() - level, 5);
		// now, let's calculate base xp earned.
		double zealExp = Experience.LEVEL[level + 1] - Experience.LEVEL[level];
		zealExp /= KILLS_FOR_LEVEL[level - STARTER_LEVEL];
		zealExp *= dmgRatio;
		if (lvlDiff > 0)
		{
			// square the difference, no real need if difference is 0
			lvlDiff *= lvlDiff;
			// increase the rewards according to difference
			victimsAdena *= lvlDiff;
			zealExp *= lvlDiff;
		}
		int sp = calcSp(zealExp);
		long currAdena = killer.getAdena() + killer.getWarehouse().getAdena();
		// can't earn more than 2/3 that you own; so it's better to have cash than to spend it
		// but must earn at least 1 adena per kill
		int killerKc = killer.getPvpKills() < 5 ? 0 : killer.getPvpKills();
		targetKc = (targetKc < 5 ? 0 : targetKc);
		int kcBoost = L2Math.limit(0, targetKc + killerKc, 50);
		dmgRatio *= (1 + kcBoost / 100D);
		long rewardAdena = (long) limit(1, Math.log(victimsAdena * dmgRatio), currAdena / 1.5D);
		long rewardExp = (long) zealExp;
		
		L2PlayerData dat = killer.getSoDPlayer();
		if (dat.isDonator())
		{
			rewardAdena = (long) (rewardAdena * Config.DONATOR_MULTI_ADENA);
			rewardExp = (long) (rewardExp * Config.DONATOR_MULTI_XP_SP);
		}
		
		L2Party party = killer.getParty();
		if (rewardExp > 0) // must earn exp in order to earn sp
		{
			if (party != null)
				party.distributeXpAndSp(rewardExp, sp, killer, killed);
			else
				killer.addExpAndSp(rewardExp, sp);
		}
		if (party != null)
			party.distributeAdena(killer, rewardAdena, killed);
		else
			killer.addAdena(PVP, rewardAdena, killed, true);
		if (last)
		{
			// last hitter gets exp and adena bonus; perks and challenges
			if (dat.isLastStand())
				dat.tryUpgradePerk(14, 1); // Last Stand
			killer.sendMessage("Last hit bonus!");
			// add 25% XP bonus and double the SP earned if possible
			if (rewardExp > 3)
				killer.addExpAndSp(rewardExp / 4, sp);
			// add the adena bonus
			killer.addAdena(LAST_HIT, (long) (rewardAdena * 1.5D), killed, true);
			int loot = LootedGoodsManager.getInstance().getLoot();
			if (loot > 0)
			{
				if (party != null)
				{
					RewardItem ri = new RewardItem(loot, getItemDisplayId(loot), 1);
					party.distributeItem(killer, ri, false, killed);
				}
				else
					killer.addItem(LAST_HIT, loot, 1, killed, true);
			}
			long bounty = BountyManager.getInstance().removeBounty(killed.getObjectId());
			if (bounty > 0)
			{
				if (party != null)
					party.distributeAdena(killer, bounty, killed);
				else
					killer.addAdena("Bounty Reward", bounty, killed, true);
				TitleManager.getInstance().updateTitle(killed, true);
				if (bounty >= 50)
					dat.tryAddChallengePoints(ChallengeTemplate.SPECIAL_DELIVERY, 1);
			}
		}
		else
			dat.tryAddChallengePoints(ChallengeTemplate.REMF, 1);
	}
	
	public static final int calcSp(double exp)
	{
		return (int) (exp / EXP_TO_SP_RATIO);
	}
	
	public static final long calcAdenaLost(L2PcInstance lurker)
	{
		return Math.min(5, lurker.getAdena());
	}
	
	public static int calcMaxArmorEnchant(L2PcInstance player, int grade)
	{
		int result = 0;
		for (int i = 0; i < Inventory.PAPERDOLL_TOTALSLOTS; i++)
		{
			if (i == Inventory.PAPERDOLL_LRHAND || i == Inventory.PAPERDOLL_RHAND)
				continue;
			L2ItemInstance item = player.getInventory().getPaperdollItem(i);
			if (item == null || item.getItem().getItemGrade() != grade)
				continue;
			if (item.getEnchantLevel() > result)
				result = item.getEnchantLevel();
		}
		return result;
	}
	
	public static int calcEnchantScroll(int grade, boolean weapon)
	{
		switch (grade)
		{
		case L2Item.CRYSTAL_C:
			if (weapon)
				return ENCHANT_C_WEAPON;
			else
				return ENCHANT_C_ARMOR;
		case L2Item.CRYSTAL_B:
			if (weapon)
				return ENCHANT_B_WEAPON;
			else
				return ENCHANT_B_ARMOR;
		case L2Item.CRYSTAL_A:
			if (weapon)
				return ENCHANT_A_WEAPON;
			else
				return ENCHANT_A_ARMOR;
		case L2Item.CRYSTAL_S:
			if (weapon)
				return ENCHANT_S_WEAPON;
			else
				return ENCHANT_S_ARMOR;
		default:
			return 0;
		}
	}
	
	public static int calcNeededKills(int enchant, int grade, boolean weapon)
	{
		int needed;
		if (enchant < MAX_ENCHANT_PER_KILL_COUNT[0])
			needed = 0;
		else if (enchant < MAX_ENCHANT_PER_KILL_COUNT[1])
			needed = 1;
		else
			needed = 2;
		switch (grade)
		{
		case L2Item.CRYSTAL_C:
			if (weapon)
				needed = KILLS_FOR_ENCHANT_C_WEAPON[needed];
			else
				needed = KILLS_FOR_ENCHANT_C_ARMOR[needed];
			break;
		case L2Item.CRYSTAL_B:
			if (weapon)
				needed = KILLS_FOR_ENCHANT_B_WEAPON[needed];
			else
				needed = KILLS_FOR_ENCHANT_B_ARMOR[needed];
			break;
		case L2Item.CRYSTAL_A:
			if (weapon)
				needed = KILLS_FOR_ENCHANT_A_WEAPON[needed];
			else
				needed = KILLS_FOR_ENCHANT_A_ARMOR[needed];
			break;
		case L2Item.CRYSTAL_S:
			if (weapon)
				needed = KILLS_FOR_ENCHANT_S_WEAPON[needed];
			else
				needed = KILLS_FOR_ENCHANT_S_ARMOR[needed];
			break;
		default:
			return 0;
		}
		return needed;
	}
	
	public static final boolean calcHaveEnoughKills(int enchant, int kills, int grade, boolean weapon)
	{
		return kills == calcNeededKills(enchant, grade, weapon);
	}
	
	/**
	 * Returns a value between <CODE>min</CODE> and <CODE>max</CODE> for given <CODE>value</CODE>.<BR>
	 * If <CODE>min</CODE> > <CODE>max</CODE>, returns <CODE>min</CODE>.
	 * 
	 * @param min
	 *            minimal value
	 * @param value
	 *            value to be calculated
	 * @param max
	 *            maximum value
	 * @return
	 */
	public static final double limit(double min, double value, double max)
	{
		return Math.max(min, Math.min(value, max));
	}
	
	public static final int calcDistancePoints(int x1, int y1, int x2, int y2)
	{
		return (int) (L2Math.calculateDistance(x1, y1, x2, y2) / METRIC_UNIT);
	}
	
	public static void rewardReviver(L2PcInstance proposer, L2PcInstance revived)
	{
		if (proposer == null)
			return;
		else
			revived.getSoDPlayer().setReviveProposer(null);
		int level = proposer.getLevel();
		double zealExp = Experience.LEVEL[level + 1] - Experience.LEVEL[level];
		zealExp /= (KILLS_FOR_LEVEL[level - STARTER_LEVEL] * RES_KILL_MULTIPLIER);
		int lvl = revived.getLevel();
		if (lvl > 83)
			zealExp *= 2.5;
		else if (lvl > 79)
			zealExp *= 1.5;
		int sp = calcSp(zealExp);
		proposer.addExpAndSp((long) zealExp, sp);
		long adena = revived.getAdena();
		if (adena > 10)
			proposer.addAdena("Resurrection", (long) Math.log(adena), revived, true);
		L2PlayerData dat = proposer.getSoDPlayer();
		dat.tryAddChallengePoints(ChallengeTemplate.CHEAT_DEATH, 1);
		if (Rnd.get(3) == 0)
			dat.processItemKills(proposer);
	}
	
	public static void rewardClerical(L2PcInstance healer, boolean mp, double val)
	{
		if (healer == null || val < 1)
			return;
		// Healing at home
		if (RespawnManager.getInstance().isInsideBase(healer))
			return;
		// Healing without any enemies around
		L2Zone zone = ZoneManager.getInstance().isInsideZone(ZoneType.Faction, healer.getX(), healer.getY());
		if (zone != null && zone.isPeace())
			return;
		// Needs rework
		/*
		double multi = Math.max((mp ? KILL_AS_MANA : KILL_AS_HEALTH) - val, 500);
		int level = healer.getLevel();
		double zealExp = Experience.LEVEL[level + 1] - Experience.LEVEL[level];
		zealExp /= (KILLS_FOR_LEVEL[level - STARTER_LEVEL] * multi);
		int sp = calcSp(zealExp);
		healer.addExpAndSp((long) zealExp, sp);
		 */
		L2PlayerData dat = healer.getSoDPlayer();
		if (mp)
			dat.tryAddChallengePoints(ChallengeTemplate.MANA_WELL, (int) val);
		else
			dat.tryAddChallengePoints(ChallengeTemplate.FIELD_MEDIC, (int) val);
		
		double zealExp;
		zealExp = (val / 8);
		if (zealExp > 0)
			healer.addExpAndSp((long) zealExp, calcSp(zealExp));
	}
	
	public static final <T> T getRandomFromList(List<T> list)
	{
		if (list == null || list.isEmpty())
			return null;
		else
			return list.get(Rnd.get(list.size()));
	}
	
	public static final <T> T getRandomFromArray(T[] array)
	{
		if (array == null || array.length == 0)
			return null;
		else
			return array[Rnd.get(array.length)];
	}
	
	public static final int getRandomFromArray(int[] array)
	{
		if (array == null || array.length == 0)
			return -1;
		else
			return array[Rnd.get(array.length)];
	}
	
	public static int getSkillFromDisplayId(int magicId)
	{
		switch (magicId)
		{
		case 5712:
			return Skills.ENERGY_DITCH;
		case 5753:
			return Skills.SUMMON_SERVITOR;
		case 5519:
			return Skills.SUMMON_SERVITOR;
		default:
			return magicId;
		}
	}
	
	public static int getItemDisplayId(int itemId)
	{
		L2Item it = ItemTable.getInstance().getTemplate(itemId);
		if (it == null)
			return itemId;
		else
			return it.getItemDisplayId();
	}
}
