/*
 * MISSING LICENSING INFO
 */
package org.sod;

import com.l2jfree.Config;
import com.l2jfree.gameserver.ThreadPoolManager;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.util.IllegalPlayerAction;

/**
 * @author savormix
 * 
 */
public final class Punishment
{
	private Punishment()
	{
		// utility class
	}
	
	public static void handleIllegalPlayerAction(L2PcInstance actor, String message)
	{
		handleIllegalPlayerAction(actor, message, Config.DEFAULT_PUNISH);
	}
	
	public static void handleIllegalPlayerAction(L2PcInstance actor, String message, int punishment)
	{
		actor.setIllegalWaiting(true);
		ThreadPoolManager.getInstance().scheduleGeneral(new IllegalPlayerAction(actor, message, punishment), 5000);
	}
}
