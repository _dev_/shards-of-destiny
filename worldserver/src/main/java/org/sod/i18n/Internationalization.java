/*
 * MISSING LICENSING INFO
 */
package org.sod.i18n;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.IOUtils;

import com.l2jfree.Config;
import com.l2jfree.gameserver.network.client.packets.sendable.NpcHtmlMessage;
import com.l2jfree.util.logging.L2Logger;

/**
 * @author savormix
 * 
 */
public final class Internationalization
{
	private static final L2Logger _log = L2Logger.getLogger(Internationalization.class);
	
	private Map<Locale, Properties> _extStrings;
	
	private Internationalization()
	{
		_extStrings = Collections.emptyMap();
		load();
	}
	
	public String localize(String str, Locale loc, String... localizables)
	{
		for (String s : localizables)
			str = str.replace(s, get(loc, s.substring(1, s.length() - 1)));
		return str;
	}
	
	public void localize(NpcHtmlMessage htm, Locale loc, String... localizables)
	{
		for (String s : localizables)
			htm.replace(s, get(loc, s.substring(1, s.length() - 1)));
	}
	
	public String get(Locale loc, String key, String... repVals)
	{
		if (loc == null)
			loc = Locale.ENGLISH;
		
		String val = null;
		Properties map = _extStrings.get(loc);
		if (map != null)
			val = map.getProperty(key);
		
		if (val == null)
			return key; // for testing
		// return _extStrings.get(Locale.ENGLISH).getProperty(key, key);
		
		try
		{
			for (int i = 0; i < repVals.length; i += 2)
				val = val.replace(repVals[i], repVals[i + 1]);
		}
		catch (RuntimeException e)
		{
			_log.error("Invalid call!", e);
		}
		
		return val;
	}
	
	@SuppressWarnings("resource")
	public void load()
	{
		File dir = new File(Config.DATAPACK_ROOT, "data/i18n");
		
		Map<Locale, Properties> extStrings = new HashMap<Locale, Properties>();
		
		BufferedReader br = null;
		try
		{
			br = new BufferedReader(new FileReader(new File(dir, "i18n.list")));
			for (String line; (line = br.readLine()) != null;)
			{
				Properties p = new Properties();
				Reader r = null;
				try
				{
					p.load(r = new FileReader(new File(dir, line + ".properties")));
				}
				catch (IOException e)
				{
					_log.error("Failed loading " + line, e);
					continue;
				}
				finally
				{
					IOUtils.closeQuietly(r);
				}
				Locale loc;
				extStrings.put(loc = new Locale(line), p);
				
				_log.info("Internationalization: " + p.size() + " " + loc.getDisplayLanguage()
						+ " externalized strings");
			}
		}
		catch (IOException e)
		{
			_log.error("Failed parsing i18n list!", e);
		}
		finally
		{
			IOUtils.closeQuietly(br);
		}
		
		_extStrings = extStrings;
	}
	
	public static Internationalization getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static final class SingletonHolder
	{
		private static final Internationalization INSTANCE = new Internationalization();
	}
}
