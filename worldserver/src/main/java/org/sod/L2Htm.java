/*
 * MISSING LICENSING INFO
 */
package org.sod;

import java.util.StringTokenizer;

import com.l2jfree.Config;
import com.l2jfree.lang.L2TextBuilder;

/**
 * @author savormix
 * 
 */
public final class L2Htm
{
	private L2Htm()
	{
		// utility class
	}
	
	public static void writeHeader(L2TextBuilder tb, String title)
	{
		tb.append("<html>");
		if (title != null)
		{
			tb.append("<title>");
			tb.append(title);
			tb.append("</title>");
		}
		tb.append("<body><center><font color=\"LEVEL\">");
		tb.append(Config.SERVER_NAME);
		tb.append("</font><br>");
	}
	
	public static void writeFooter(L2TextBuilder tb)
	{
		tb.append("</center></body></html>");
	}
	
	public static void writeImage(L2TextBuilder tb, String image)
	{
		writeImage(tb, image, 32, 32);
	}
	
	public static void writeImage(L2TextBuilder tb, String image, int w, int h)
	{
		tb.append("<img src=\"");
		tb.append(image);
		tb.append("\" width=\"");
		tb.append(w);
		tb.append("\" height=\"");
		tb.append(h);
		tb.append("\">");
	}
	
	public static void _writeImage(L2TextBuilder tb, String image, int w, int h)
	{
		tb.append("<img src=\"");
		tb.append(image);
		tb.append("\" width=");
		tb.append(w);
		tb.append(" height=");
		tb.append(h);
		tb.append('>');
	}
	
	public static void writeTextField(L2TextBuilder tb, String var, int w, int maxLen)
	{
		writeTextField(tb, var, w, 0, maxLen);
	}
	
	public static void writeTextField(L2TextBuilder tb, String var, int w, int h, int maxLen)
	{
		tb.append("<edit var=\"");
		tb.append(var);
		tb.append("\" width=\"");
		tb.append(w);
		if (h > 0)
		{
			tb.append("\" height=\"");
			tb.append(h);
		}
		tb.append("\" length=\"");
		tb.append(maxLen);
		tb.append("\">");
	}
	
	private static void writeNpcEvent(L2TextBuilder tb, Object npcOid, Object event, Object subEvent)
	{
		tb.append("npc_");
		if (npcOid == null)
			tb.append("%objectId%");
		else
			tb.append(npcOid);
		tb.append('_');
		tb.append(event);
		if (subEvent != null)
		{
			tb.append(' ');
			tb.append(subEvent);
		}
	}
	
	public static void writeButton(L2TextBuilder tb, String event, Enum<?> subEvent, int w, int h, boolean hide,
			String... value)
	{
		writeButton(tb, event, subEvent != null ? subEvent.ordinal() : null, w, h, hide, value);
	}
	
	public static void writeButton(L2TextBuilder tb, String event, Object subEvent, int w, int h, boolean hide,
			String... value)
	{
		tb.append("<button value=\"");
		for (String val : value)
			tb.append(val);
		tb.append("\" action=\"bypass ");
		if (hide)
			tb.append("-h ");
		tb.append(event);
		if (subEvent != null)
		{
			tb.append(' ');
			tb.append(subEvent);
		}
		tb.append("\" width=\"");
		tb.append(w);
		tb.append("\" height=\"");
		tb.append(h);
		tb.append("\" back=\"L2UI_ct1.button_df_down\" fore=\"L2UI_ct1.button_df\">");
	}
	
	public static void writeTutorialCloseButton(L2TextBuilder tb, int w, int h, String... value)
	{
		tb.append("<button value=\"");
		for (String val : value)
			tb.append(val);
		tb.append("\" action=\"link close\" width=\"");
		tb.append(w);
		tb.append("\" height=\"");
		tb.append(h);
		tb.append("\" back=\"L2UI_ct1.button_df_down\" fore=\"L2UI_ct1.button_df\">");
	}
	
	public static void writeDisabledButton(L2TextBuilder tb, int w, int h, String... value)
	{
		tb.append("<button value=\"");
		for (String val : value)
			tb.append(val);
		tb.append("\" width=\"");
		tb.append(w);
		tb.append("\" height=\"");
		tb.append(h);
		tb.append("\" back=\"L2UI_ct1.button_df_down\" fore=\"L2UI_ct1.button_df_disable\">");
	}
	
	public static void writeNpcAction(L2TextBuilder tb, Object npcOid, Enum<?> event, Object subEvent, boolean hide,
			String... value)
	{
		tb.append("<a action=\"bypass ");
		if (hide)
			tb.append("-h ");
		writeNpcEvent(tb, npcOid, event.ordinal(), subEvent);
		tb.append("\">");
		for (String val : value)
			tb.append(val);
		tb.append("</a>");
	}
	
	public static void writeNpcButton(L2TextBuilder tb, Object npcOid, Enum<?> event, Object subEvent, int w, int h,
			boolean hide, String... value)
	{
		writeNpcButton(tb, npcOid, event.ordinal(), subEvent, w, h, hide, value);
	}
	
	public static void writeNpcButton(L2TextBuilder tb, Object npcOid, Object event, Object subEvent, int w, int h,
			boolean hide, String... value)
	{
		tb.append("<button value=\"");
		for (String val : value)
			tb.append(val);
		tb.append("\" action=\"bypass ");
		if (hide)
			tb.append("-h ");
		writeNpcEvent(tb, npcOid, event, subEvent);
		tb.append("\" width=\"");
		tb.append(w);
		tb.append("\" height=\"");
		tb.append(h);
		tb.append("\" back=\"L2UI_ct1.button_df_down\" fore=\"L2UI_ct1.button_df\">");
	}
	
	public static void writeQuestButton(L2TextBuilder tb, String quest, Enum<?> event, int w, int h, boolean hide,
			String... value)
	{
		writeQuestButton(tb, quest, event.ordinal(), w, h, hide, value);
	}
	
	public static void writeQuestButton(L2TextBuilder tb, String quest, Object event, int w, int h, boolean hide,
			String... value)
	{
		tb.append("<button value=\"");
		for (String val : value)
			tb.append(val);
		tb.append("\" action=\"bypass ");
		if (hide)
			tb.append("-h Quest ");
		tb.append(quest);
		tb.append(' ');
		tb.append(event);
		tb.append("\" width=\"");
		tb.append(w);
		tb.append("\" height=\"");
		tb.append(h);
		tb.append("\" back=\"L2UI_ct1.button_df_down\" fore=\"L2UI_ct1.button_df\">");
	}
	
	public static void writeExchangeButton(L2TextBuilder tb, Object npcOid, int multisell, int w, int h, boolean hide,
			String... value)
	{
		writeNpcButton(tb, npcOid, "multisell", multisell, w, h, hide, value);
	}
	
	public static <T extends Enum<T>> T getRequest(StringTokenizer st, T[] values)
	{
		return getRequest(st.nextToken(), values);
	}
	
	public static <T extends Enum<T>> T getRequest(String s, T[] values)
	{
		int idx;
		try
		{
			idx = Integer.parseInt(s);
		}
		catch (RuntimeException e)
		{
			return null;
		}
		
		if (idx < 0 || idx >= values.length)
			return null;
		
		return values[idx];
	}
}
