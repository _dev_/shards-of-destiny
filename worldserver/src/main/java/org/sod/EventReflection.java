/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod;

import org.sod.buffs.BuffManager;
import org.sod.model.ChallengeTemplate;
import org.sod.manager.GambleManager;
import org.sod.manager.GlobalEventManager;
import org.sod.manager.RespawnManager;
import org.sod.model.L2PlayerData;
import org.sod.model.event.TeamVersusTeam;
import org.sod.model.event.global.GlobalEvent;

import com.l2jfree.Config;
import com.l2jfree.gameserver.handler.IItemHandler;
import com.l2jfree.gameserver.model.L2ItemInstance;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.L2Playable;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.base.Experience;
import com.l2jfree.gameserver.model.olympiad.Olympiad;
import com.l2jfree.gameserver.model.restriction.global.AbstractRestriction;
import com.l2jfree.gameserver.model.zone.L2Zone;
import com.l2jfree.gameserver.skills.Stats;
import com.l2jfree.gameserver.templates.effects.EffectTemplate;
import com.l2jfree.gameserver.templates.skills.L2SkillType;

/**
 * General server restrictions.
 * 
 * @author savormix
 * @since 2010.01.01
 */
public final class EventReflection extends AbstractRestriction
{
	private EventReflection()
	{
	}
	
	@Override
	public boolean canUseItemHandler(Class<? extends IItemHandler> clazz, int itemId, L2Playable activeChar,
			L2ItemInstance item, L2PcInstance player)
	{
		if (clazz == null)
		{
			if (player != null && player.getSoDPlayer().isLastStand())
			{
				double swap = activeChar.getStat().calcStat(Stats.IGNORE_DEATH_SWAP, 0, null, null);
				if (!item.isEquipable() || swap < 1)
				{
					player.sendMessage("Cannot use items during last stand.");
					return false;
				}
			}
		}
		return true;
	}
	
	@Override
	public int getNameColor(L2PcInstance activeChar)
	{
		return GlobalEvent.getNameColor(activeChar);
	}
	
	@Override
	public Boolean isInsideZone(L2Character activeChar, byte zone)
	{
		if (activeChar instanceof L2Playable)
		{
			switch (zone)
			{
			case L2Zone.FLAG_PEACE:
				return RespawnManager.getInstance().isInsideBase(activeChar);
			case L2Zone.FLAG_PVP:
				return !RespawnManager.getInstance().isInsideBase(activeChar);
			case L2Zone.FLAG_NOSTORE:
				return !activeChar.isInsideZone(L2Zone.FLAG_STORE);
			}
		}
		return null;
	}
	
	@Override
	public void levelChanged(L2PcInstance activeChar)
	{
		L2PlayerData dat = activeChar.getSoDPlayer();
		if (dat == null)
			return; // we set level 75 for newly created chars before they log in
		// it looks bad, but this way we can handle overleveling (by GM menu, for ex)
		if (activeChar.getLevel() >= 76)
		{
			dat.unlockPerk(2); // Sleight of Hand
			dat.unlockPerk(7); // Hardline
			dat.unlockPerk(11); // Steady Aim
			if (activeChar.getLevel() >= 77)
			{
				dat.unlockPerk(6); // Lightweight
				if (activeChar.getLevel() >= 78)
				{
					dat.unlockPerk(3); // Scavenger
					dat.unlockPerk(9); // Danger Close
					if (activeChar.getLevel() >= 79)
					{
						dat.unlockPerk(13); // Ninja
						if (activeChar.getLevel() >= 80)
						{
							dat.unlockPerk(14); // Last Stand
						}
					}
				}
			}
		}
		dat.trySetChallengePoints(ChallengeTemplate.POWER_LEVEL, activeChar.getLevel());
	}
	
	@Override
	public void playerLoggedIn(L2PcInstance activeChar)
	{
		if (activeChar.getPvpKills() > 0)
		{
			activeChar.setPkKills(activeChar.getPkKills() + activeChar.getPvpKills());
			activeChar.setPvpKills(0);
		}
		if (!activeChar.isGM())
		{
			if (activeChar.isDead())
				activeChar.setIsPendingRevive(true);
			RespawnManager.getInstance().moveToSpawn(activeChar);
		}
		GlobalEventManager.getInstance().askVote(activeChar);
		GlobalEventManager.getInstance().startTask();
		
		if (Config.NOBLE_EVERYONE && !activeChar.isNoble())
		{
			activeChar.setNoble(true);
			activeChar.addItem("Noblesse", 7694, 1, null, true);
			activeChar.broadcastFullInfo(); // show noble symbol in status
		}
	}
	
	@Override
	public void playerDisconnected(L2PcInstance activeChar)
	{
		L2PlayerData dat = activeChar.getSoDPlayer();
		if (dat != null)
			dat.deleteMe();
	}
	
	@Override
	public boolean playerDied(L2PcInstance activeChar)
	{
		return playerKilled(null, activeChar, null);
	}
	
	@Override
	public boolean playerKilled(L2Character activeChar, L2PcInstance target, L2PcInstance killer)
	{
		if (TeamVersusTeam.getInstance().isPlaying(killer, target))
			return false;
		if (Olympiad.isPlaying(target) || Olympiad.isPlaying(killer))
			return false;
		
		// target is not null, since it was killed.
		// activeChar is not null, see L2PcInstance#doDie(L2Character)
		int kc = target.getPvpKills();
		L2PlayerData dat = target.getSoDPlayer();
		dat.doDie(activeChar);
		if (killer != null)
			killer.getSoDPlayer().onKill(target, kc);
		return false;
	}
	
	@Override
	public void playerRevived(L2PcInstance player)
	{
		RespawnManager.getInstance().removePlayer(player);
		L2PlayerData dat = player.getSoDPlayer();
		Calc.rewardReviver(dat.getReviveProposer(), player);
		BuffManager.getInstance().buff(dat);
		// CP is not restored for resurrected players (in field)
		player.getStatus().setCurrentHpMp(player.getMaxHp(), player.getMaxMp());
		if (dat.isInGhostMode())
			RespawnManager.getInstance().leaveMovieMode(dat);
	}
	
	@Override
	// @DisabledRestriction
	public boolean useVoicedCommand(String command, L2PcInstance activeChar, String target)
	{
		if (!Config.PUBLIC_TEST_SERVER)
			return false;
		
		long trgt;
		try
		{
			trgt = Long.parseLong(target);
		}
		catch (Exception e)
		{
			return false;
		}
		if (command.equals("level"))
		{
			if (trgt < Config.STARTING_LEVEL || trgt > Experience.MAX_LEVEL)
				return false;
			activeChar.getStat().addLevel((byte) (trgt - activeChar.getLevel()));
			return true;
		}
		else if (command.equals("adena"))
		{
			if (trgt < 1 || trgt > Integer.MAX_VALUE * 2L)
				return false;
			activeChar.addAdena("PTS", trgt, null, true);
			return true;
		}
		else if (command.equals("luck"))
		{
			if (trgt < 1 || trgt > Integer.MAX_VALUE * 2L)
				return false;
			activeChar.addItem("PTS", GambleManager.COIN_OF_LUCK, trgt, null, true);
			return true;
		}
		return false;
	}
	
	@Override
	public boolean canCreateEffect(L2Character activeChar, L2Character target, L2Skill skill)
	{
		if (activeChar == target)
			return true;
		
		L2PcInstance affected = target.getActingPlayer();
		if (affected == null)
			return true;
		
		L2PcInstance player = activeChar.getActingPlayer();
		if (player == null || player.getLevel() >= Config.MASS_BUFF_MIN_LEVEL || player == affected)
			return true;
		
		if (skill.isOffensive() || !skill.hasEffects() || !skill.isBuff())
			return true;
		
		switch (skill.getTargetType())
		{
		case TARGET_ONE:
		case TARGET_CLAN:
		case TARGET_PARTY_CLAN:
		case TARGET_PARTY:
		case TARGET_PARTY_MEMBER:
			if (skill.getEffectType() != L2SkillType.BUFF)
				return true;
			if (skill.isDanceOrSong())
			{
				player.getSoDPlayer().sendLocalizedMessage("BUFFING_PLAYERS_NOT_ALLOWED", "%level%",
						String.valueOf(Config.MASS_BUFF_MIN_LEVEL));
				return false;
			}
			for (EffectTemplate tmp : skill.getEffectTemplates())
				if (tmp.period >= 300)
				{
					player.getSoDPlayer().sendLocalizedMessage("BUFFING_PLAYERS_NOT_ALLOWED", "%level%",
							String.valueOf(Config.MASS_BUFF_MIN_LEVEL));
					return false;
				}
		}
		
		return true;
	}
	
	public static EventReflection getInstance()
	{
		return SingletonHolder._instance;
	}
	
	private static final class SingletonHolder
	{
		private static final EventReflection _instance = new EventReflection();
	}
}
