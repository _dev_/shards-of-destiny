/*
 * MISSING LICENSING INFO
 */
package org.sod.tutorial;

import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.instance.L2MonsterInstance;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.restriction.global.AbstractRestriction;
import com.l2jfree.gameserver.model.restriction.global.GlobalRestrictions.CombatState;
import com.l2jfree.gameserver.model.restriction.global.RestrictionPriority;

/**
 * @author savormix
 * 
 */
public class TutorialRestriction extends AbstractRestriction
{
	private TutorialRestriction()
	{
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public double calcDamage(L2Character activeChar, L2Character target, double damage, L2Skill skill)
	{
		if (!TutorialManager.getInstance().isInTutorial(activeChar.getActingPlayer()))
			return damage;
		
		if (target instanceof L2MonsterInstance)
		{
			L2MonsterInstance npc = (L2MonsterInstance) target;
			if (npc.getNpcId() >= 90066 && npc.getNpcId() <= 90068)
				return 9001;
		}
		
		return damage;
	}
	
	@Override
	public boolean canInviteToParty(L2PcInstance activeChar, L2PcInstance target)
	{
		if (TutorialManager.getInstance().isInTutorial(activeChar)
				|| TutorialManager.getInstance().isInTutorial(target))
			return false;
		
		return true;
	}
	
	@Override
	public boolean isInvul(L2Character activeChar, L2Character target, L2Skill skill, boolean sendMessage,
			L2PcInstance attacker_, L2PcInstance target_, boolean isOffensive)
	{
		if (TutorialManager.getInstance().isInTutorial(target_))
			return isOffensive;
		
		return false;
	}
	
	@Override
	public CombatState getCombatState(L2PcInstance activeChar, L2PcInstance target)
	{
		if (TutorialManager.getInstance().isInTutorial(activeChar)
				|| TutorialManager.getInstance().isInTutorial(target))
			return CombatState.FRIEND;
		
		return CombatState.NEUTRAL;
	}
	
	@RestrictionPriority(RestrictionPriority.DEFAULT_PRIORITY + 0.3)
	@Override
	public void playerLoggedIn(L2PcInstance activeChar)
	{
		TutorialManager.getInstance().loadTutorial(activeChar);
	}
	
	@Override
	public void playerDisconnected(L2PcInstance activeChar)
	{
		TutorialManager.getInstance().cleanUp(activeChar);
	}
	
	public static TutorialRestriction getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static final class SingletonHolder
	{
		private static final TutorialRestriction INSTANCE = new TutorialRestriction();
	}
}
