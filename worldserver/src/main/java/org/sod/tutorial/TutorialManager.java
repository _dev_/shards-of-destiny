/*
 * MISSING LICENSING INFO
 */
package org.sod.tutorial;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;

import javolution.util.FastMap;

import org.sod.Calc;
import org.sod.L2Htm;
import org.sod.Skills;
import org.sod.i18n.Internationalization;
import org.sod.manager.EventFenceManager;
import org.sod.manager.PooledInstanceManager;
import org.sod.manager.RespawnManager;
import org.sod.manager.RespawnManager.BaseInfo;
import org.sod.manager.conquest.ConquestFactionManager;
import org.sod.model.event.global.GlobalEvent;
import org.sod.model.event.global.Team;
import org.sod.model.event.global.impl.CaptureTheFlag;
import org.sod.model.event.global.impl.CaptureTheFlag.FlagTeam;
import org.sod.model.event.global.impl.GolemDispute;
import org.sod.model.event.global.impl.GolemDispute.DisputeTeam;
import org.sod.model.event.global.impl.LureTheGremlin;
import org.sod.model.event.global.impl.LureTheGremlin.LurerTeam;
import org.sod.model.event.global.impl.TeamKingOfTheHill;

import com.l2jfree.Config;
import com.l2jfree.L2DatabaseFactory;
import com.l2jfree.gameserver.communitybbs.Manager.SettingsBBSManager;
import com.l2jfree.gameserver.idfactory.IdFactory;
import com.l2jfree.gameserver.instancemanager.MapRegionManager;
import com.l2jfree.gameserver.model.L2Spawn;
import com.l2jfree.gameserver.model.Location;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.instance.L2DoorInstance;
import com.l2jfree.gameserver.model.actor.instance.L2FakeManagerInstance;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.base.Experience;
import com.l2jfree.gameserver.network.client.packets.sendable.TutorialCloseHtmlPacket.HideTutorialHtml;
import com.l2jfree.gameserver.network.client.packets.sendable.TutorialShowHtml.ShowTutorialHtml;
import com.l2jfree.gameserver.templates.StatsSet;
import com.l2jfree.gameserver.templates.chars.L2CharTemplate;
import com.l2jfree.lang.L2TextBuilder;
import com.l2jfree.util.logging.L2Logger;

/**
 * @author savormix
 * 
 */
public class TutorialManager
{
	private static final L2Logger _log = L2Logger.getLogger(TutorialManager.class);
	private static final int POINTS_FOR_KOTH = 2;
	private static final int POINTS_FOR_LTG = 2;
	
	private final Map<Integer, TutorialInfo> _newBees;
	private final Location _base;
	private final Location _hall;
	private final String _welcome;
	
	private TutorialManager()
	{
		_newBees = new FastMap<Integer, TutorialInfo>().setShared(true);
		_base = new Location(-23775, -8960, -5385);
		_hall = new Location(83400, 145330, -3390);
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, null);
		SettingsBBSManager.getInstance().writeLanguageSelect(tb, Locale.ENGLISH, true);
		L2Htm.writeFooter(tb);
		_welcome = tb.moveToString();
		
		TutorialRestriction.getInstance().activate();
	}
	
	public boolean isInTutorial(L2PcInstance player)
	{
		if (player == null)
			return false;
		
		return getNewBees().containsKey(player.getObjectId());
	}
	
	public BaseInfo getSpawn(L2PcInstance player)
	{
		if (player == null)
			return null;
		
		TutorialInfo ti = getNewBees().get(player.getObjectId());
		if (ti == null)
			return null;
		
		return new BaseInfo(ti.getSpawn(), ti.getInstance(), -1);
	}
	
	public void loadTutorial(L2PcInstance player)
	{
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT state FROM character_tutorial WHERE charId=?");
			ps.setInt(1, player.getObjectId());
			ResultSet rs = ps.executeQuery();
			if (rs.next())
			{
				TutorialState ts = TutorialState.values()[rs.getInt("state")];
				if (ts != TutorialState.COMPLETED && ts != TutorialState.SKIPPED)
				{
					TutorialInfo ti = new TutorialInfo(PooledInstanceManager.getInstance().createInstance(), ts);
					getNewBees().put(player.getObjectId(), ti);
					
					player.addSkill(Skills.TUTORIAL_SPEED, 1);
					
					// show something based on the state they are in
					stateChanged(player, ts);
				}
			}
			rs.close();
			ps.close();
		}
		catch (SQLException e)
		{
			_log.error("Failed loading tutorial state for " + player);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	private void stateChanged(L2PcInstance player, TutorialState state)
	{
		switch (state)
		{
		case SELECT_LANGUAGE:
			player.sendPacket(new ShowTutorialHtml(_welcome));
			break;
		case CHOOSE_TO_SKIP:
			showChoiceToSkip(player);
			break;
		case EVENT_INTRO:
			showEventIntro(player);
			break;
		case EVENTS_KILL_TO_WIN:
			showK2w(player);
			break;
		case EVENT_KOTH:
			showKothIntro(player);
			break;
		case EVENT_CTF:
			showCtfIntro(player);
			break;
		case EVENT_LTG:
			showLtgIntro(player);
			break;
		case EVENT_GD:
			showGdIntro(player);
			break;
		case CONQUEST_INTRO:
		case CONQUEST_SECURITY:
		case CONQUEST_AWAKENING:
		case CONQUEST_ENERGY_TRANSFER:
		case CONQUEST_SEAL:
			showConquest(player, false);
			break;
		case CONQUEST_DOORS:
			showConquestDoors(player);
			break;
		case SKIPPED:
		case COMPLETED:
			endTutorial(player);
			break;
		default:
			_log.warn("Unhandled tutorial state: " + state);
			break;
		}
	}
	
	public boolean notifyLanguageSelected(L2PcInstance player)
	{
		TutorialInfo ti = getNewBees().get(player.getObjectId());
		if (ti == null || ti.getState() != TutorialState.SELECT_LANGUAGE)
			return false;
		
		updateState(player, TutorialState.CHOOSE_TO_SKIP);
		return true;
	}
	
	public void notifyEventNpcAction(L2FakeManagerInstance npc, L2PcInstance player)
	{
		TutorialInfo ti = getNewBees().get(player.getObjectId());
		if (ti == null)
			return;
		
		switch (ti.getState())
		{
		case EVENT_INTRO:
		case EVENTS_KILL_TO_WIN:
			npc.showFakeVoteWindow(player);
			updateState(player, TutorialState.EVENTS_KILL_TO_WIN);
			break;
		case EVENT_KOTH:
		case EVENT_CTF:
		case EVENT_LTG:
		case EVENT_GD:
		case EVENT_REGICIDE:
			npc.showFakeEventWindow(player, ti.getEvent());
			break;
		default:
			_log.warn("Unhandled event NPC state: " + ti.getState());
			break;
		}
	}
	
	public void notifyJoinEvent(L2PcInstance player)
	{
		TutorialInfo ti = getNewBees().get(player.getObjectId());
		if (ti == null)
			return;
		
		switch (ti.getState())
		{
		case EVENT_KOTH:
			showKothOrders(player);
			break;
		case EVENT_CTF:
			showCtfOrders(player);
			break;
		case EVENT_LTG:
			showLtgOrders(player);
			break;
		case EVENT_GD:
			showGdOrders(player);
			break;
		default:
			_log.warn("Unhandled event state: " + ti.getState());
			break;
		}
		
		GlobalEvent ge = ti.getEvent();
		ge.add(player);
		ge.onStart();
	}
	
	private void setUpEventGuide(L2PcInstance player)
	{
		TutorialInfo ti = getNewBees().get(player.getObjectId());
		if (ti == null || ti.getNpc() != null)
			return;
		
		EventFenceManager.getInstance().spawnFence(2, ti.getInstance());
		L2Spawn sp = new L2Spawn(90093);
		sp.setLoc(new Location(82944, 148640, -3464, 31710));
		sp.setInstanceId(ti.getInstance());
		ti.setNpc(sp.doSpawn());
	}
	
	private void showChoiceToSkip(L2PcInstance player)
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, i18n.get(loc, "TUTORIAL_TITLE"));
		tb.append(i18n.get(loc, "TUTORIAL_INTRO", "%serverName%", Config.SERVER_NAME));
		tb.append("<br1>");
		L2Htm.writeButton(tb, "tut", TutorialState.EVENT_INTRO, 150, 30, false, i18n.get(loc, "TUTORIAL_CONTINUE"));
		tb.append("<br>");
		tb.append(i18n.get(loc, "TUTORIAL_SKIP_DISCLAIMER"));
		tb.append("<br1>");
		L2Htm.writeButton(tb, "tut", TutorialState.SKIPPED, 150, 30, false, i18n.get(loc, "TUTORIAL_SKIP"));
		L2Htm.writeFooter(tb);
		
		player.sendPacket(new ShowTutorialHtml(tb.moveToString()));
	}
	
	private void showEventIntro(L2PcInstance player)
	{
		TutorialInfo ti = getNewBees().get(player.getObjectId());
		if (ti == null)
			return;
		
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, i18n.get(loc, "TUTORIAL_TITLE"));
		tb.append(i18n.get(loc, "TUTORIAL_EVENT_INTRO", "%serverName%", Config.SERVER_NAME));
		tb.append("<br><font color=\"LEVEL\">");
		tb.append(i18n.get(loc, "TUTORIAL_EVENT_INTRO_COND"));
		tb.append("</font>");
		L2Htm.writeFooter(tb);
		
		player.sendPacket(new ShowTutorialHtml(tb.moveToString()));
		
		setUpEventGuide(player);
		RespawnManager.getInstance().moveToSpawn(player);
	}
	
	private void showK2w(L2PcInstance player)
	{
		TutorialInfo ti = getNewBees().get(player.getObjectId());
		if (ti == null)
			return;
		
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, i18n.get(loc, "TUTORIAL_TITLE"));
		tb.append(i18n.get(loc, "TUTORIAL_EVENT_KILL_TO_WIN"));
		tb.append("<br>");
		L2Htm.writeButton(tb, "tut", TutorialState.EVENT_KOTH, 150, 30, false, i18n.get(loc, "TUTORIAL_CONTINUE"));
		L2Htm.writeFooter(tb);
		
		player.sendPacket(new ShowTutorialHtml(tb.moveToString()));
		
		setUpEventGuide(player);
	}
	
	private void showKothIntro(final L2PcInstance player)
	{
		TutorialInfo ti = getNewBees().get(player.getObjectId());
		if (ti == null)
			return;
		
		{
			GlobalEvent ge = new TeamKingOfTheHill(POINTS_FOR_KOTH, true, new Team(
					new Location(-13750, 278440, -11935), new Location(-11250, 280950, -11935)), new Team(new Location(
					-11240, 278420, -11935), new Location(-13760, 280945, -11935)), 90005)
			{
				@Override
				public void doAfterEnd()
				{
					boolean ok = getAllPlayers().contains(player);
					super.doAfterEnd();
					if (ok)
						updateState(player, TutorialState.EVENT_CTF);
				}
			};
			ti.setEvent(ge);
		}
		
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, i18n.get(loc, "TUTORIAL_TITLE"));
		tb.append(i18n.get(loc, "TUTORIAL_EVENT_TKOTH_INTRO"));
		tb.append("<br><font color=\"LEVEL\">");
		tb.append(i18n.get(loc, "TUTORIAL_EVENT_TKOTH_INTRO_COND"));
		tb.append("</font>");
		L2Htm.writeFooter(tb);
		
		player.sendPacket(new ShowTutorialHtml(tb.moveToString()));
		
		setUpEventGuide(player);
	}
	
	private void showKothOrders(L2PcInstance player)
	{
		TutorialInfo ti = getNewBees().get(player.getObjectId());
		if (ti == null)
			return;
		
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, i18n.get(loc, "TUTORIAL_TITLE"));
		tb.append(i18n.get(loc, "TUTORIAL_EVENT_TKOTH_ORDERS"));
		tb.append("<br><font color=\"LEVEL\">");
		tb.append(i18n.get(loc, "TUTORIAL_EVENT_TKOTH_ORDERS_COND", "%points%", String.valueOf(POINTS_FOR_KOTH)));
		tb.append("</font>");
		L2Htm.writeFooter(tb);
		
		player.sendPacket(new ShowTutorialHtml(tb.moveToString()));
	}
	
	private void showCtfIntro(final L2PcInstance player)
	{
		TutorialInfo ti = getNewBees().get(player.getObjectId());
		if (ti == null)
			return;
		
		{
			GlobalEvent ge = new CaptureTheFlag(1, true, new FlagTeam(new Location(-80801, 86531, -5155), 0x660000,
					new Location(-81358, 86528, -5155)), new FlagTeam(new Location(-76337, 87139, -5155), 0x66FF,
					new Location(-75992, 87133, -5155)))
			{
				@Override
				public void doAfterEnd()
				{
					boolean ok = getAllPlayers().contains(player);
					super.doAfterEnd();
					if (ok)
						updateState(player, TutorialState.EVENT_LTG);
				}
			};
			ti.setEvent(ge);
		}
		
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, i18n.get(loc, "TUTORIAL_TITLE"));
		tb.append(i18n.get(loc, "TUTORIAL_EVENT_CTF_INTRO"));
		tb.append("<br><font color=\"LEVEL\">");
		tb.append(i18n.get(loc, "TUTORIAL_EVENT_CTF_INTRO_COND"));
		tb.append("</font>");
		L2Htm.writeFooter(tb);
		
		player.sendPacket(new ShowTutorialHtml(tb.moveToString()));
		
		setUpEventGuide(player);
	}
	
	private void showCtfOrders(L2PcInstance player)
	{
		TutorialInfo ti = getNewBees().get(player.getObjectId());
		if (ti == null)
			return;
		
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, i18n.get(loc, "TUTORIAL_TITLE"));
		tb.append(i18n.get(loc, "TUTORIAL_EVENT_CTF_ORDERS"));
		tb.append("<br><font color=\"LEVEL\">");
		tb.append(i18n.get(loc, "TUTORIAL_EVENT_CTF_ORDERS_COND"));
		tb.append("</font>");
		L2Htm.writeFooter(tb);
		
		player.sendPacket(new ShowTutorialHtml(tb.moveToString()));
	}
	
	private void showLtgIntro(final L2PcInstance player)
	{
		TutorialInfo ti = getNewBees().get(player.getObjectId());
		if (ti == null)
			return;
		
		{
			GlobalEvent ge = new LureTheGremlin(POINTS_FOR_LTG, true, new LurerTeam(new Location(11660, -49145, -3005),
					LureTheGremlin.COLOR_RED, 90001, LureTheGremlin.CORNER_NPC_RED), new LurerTeam(new Location(12660,
					-49150, -3005), LureTheGremlin.COLOR_BLUE, 90002, LureTheGremlin.CORNER_NPC_BLUE))
			{
				@Override
				public void doAfterEnd()
				{
					boolean ok = getAllPlayers().contains(player);
					super.doAfterEnd();
					if (ok)
						updateState(player, TutorialState.EVENT_GD);
				}
			};
			ti.setEvent(ge);
		}
		
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, i18n.get(loc, "TUTORIAL_TITLE"));
		tb.append(i18n.get(loc, "TUTORIAL_EVENT_LTG_INTRO"));
		tb.append("<br><font color=\"LEVEL\">");
		tb.append(i18n.get(loc, "TUTORIAL_EVENT_LTG_INTRO_COND"));
		tb.append("</font>");
		L2Htm.writeFooter(tb);
		
		player.sendPacket(new ShowTutorialHtml(tb.moveToString()));
		
		setUpEventGuide(player);
	}
	
	private void showLtgOrders(L2PcInstance player)
	{
		TutorialInfo ti = getNewBees().get(player.getObjectId());
		if (ti == null)
			return;
		
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, i18n.get(loc, "TUTORIAL_TITLE"));
		tb.append(i18n.get(loc, "TUTORIAL_EVENT_LTG_ORDERS"));
		tb.append("<br><font color=\"LEVEL\">");
		tb.append(i18n.get(loc, "TUTORIAL_EVENT_LTG_ORDERS_COND", "%points%", String.valueOf(POINTS_FOR_LTG)));
		tb.append("</font>");
		L2Htm.writeFooter(tb);
		
		player.sendPacket(new ShowTutorialHtml(tb.moveToString()));
	}
	
	private void showGdIntro(final L2PcInstance player)
	{
		TutorialInfo ti = getNewBees().get(player.getObjectId());
		if (ti == null)
			return;
		
		{
			GlobalEvent ge = new GolemDispute(6, true, new DisputeTeam(new Location(-106750, 239405, -3640),
					GolemDispute.COLOR_PURPLE, GolemDispute.GOLEM_PURPLE, new Location(-106890, 239575, -3635)),
					new DisputeTeam(new Location(-105190, 237860, -3615), GolemDispute.COLOR_ORANGE,
							GolemDispute.GOLEM_ORANGE, new Location(-105040, 237725, -3600)),
					GolemDispute.GOLDEN_GOLEM_NPC, new Location(-105980, 238660, -3680))
			{
				@Override
				public void doAfterEnd()
				{
					boolean ok = getAllPlayers().contains(player);
					super.doAfterEnd();
					if (ok)
						updateState(player, TutorialState.CONQUEST_INTRO);
				}
			};
			ti.setEvent(ge);
		}
		
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, i18n.get(loc, "TUTORIAL_TITLE"));
		tb.append(i18n.get(loc, "TUTORIAL_EVENT_GD_INTRO"));
		tb.append("<br><font color=\"LEVEL\">");
		tb.append(i18n.get(loc, "TUTORIAL_EVENT_GD_INTRO_COND"));
		tb.append("</font>");
		L2Htm.writeFooter(tb);
		
		player.sendPacket(new ShowTutorialHtml(tb.moveToString()));
		
		setUpEventGuide(player);
	}
	
	private void showGdOrders(L2PcInstance player)
	{
		TutorialInfo ti = getNewBees().get(player.getObjectId());
		if (ti == null)
			return;
		
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, i18n.get(loc, "TUTORIAL_TITLE"));
		tb.append(i18n.get(loc, "TUTORIAL_EVENT_GD_ORDERS"));
		tb.append("<br><font color=\"LEVEL\">");
		tb.append(i18n.get(loc, "TUTORIAL_EVENT_GD_ORDERS_COND"));
		tb.append("</font>");
		L2Htm.writeFooter(tb);
		
		player.sendPacket(new ShowTutorialHtml(tb.moveToString()));
	}
	
	private void showConquest(L2PcInstance player, boolean cond)
	{
		TutorialInfo ti = getNewBees().get(player.getObjectId());
		if (ti == null)
			return;
		
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, i18n.get(loc, "TUTORIAL_TITLE"));
		tb.append(i18n.get(loc, "TUTORIAL_" + ti.getState().name()));
		tb.append("<br>");
		if (cond)
		{
			tb.append("<font color=\"LEVEL\">");
			tb.append(i18n.get(loc, "TUTORIAL_" + ti.getState().name() + "_COND"));
			tb.append("</font>");
		}
		else
		{
			TutorialState next;
			if (ti.getState() == TutorialState.CONQUEST_SEAL)
				next = TutorialState.COMPLETED;
			else
				next = TutorialState.values()[ti.getState().ordinal() + 1];
			L2Htm.writeButton(tb, "tut", next, 150, 30, false, i18n.get(loc, "TUTORIAL_CONTINUE"));
		}
		L2Htm.writeFooter(tb);
		
		player.sendPacket(new ShowTutorialHtml(tb.moveToString()));
	}
	
	private void showConquestDoors(final L2PcInstance player)
	{
		final TutorialInfo ti = getNewBees().get(player.getObjectId());
		if (ti == null)
			return;
		
		StatsSet npcDat = new StatsSet();
		npcDat.set("baseSTR", 0);
		npcDat.set("baseCON", 0);
		npcDat.set("baseDEX", 0);
		npcDat.set("baseINT", 0);
		npcDat.set("baseWIT", 0);
		npcDat.set("baseMEN", 0);
		
		npcDat.set("baseHpMax", 1000000);
		npcDat.set("baseCpMax", 0);
		npcDat.set("baseMpMax", 0);
		npcDat.set("baseHpReg", 3.e-3f);
		npcDat.set("baseMpReg", 3.e-3f);
		
		npcDat.set("basePAtk", 0);
		npcDat.set("baseMAtk", 0);
		npcDat.set("basePDef", 50000);
		npcDat.set("baseMDef", 50000);
		
		npcDat.set("basePAtkSpd", 0);
		npcDat.set("baseMAtkSpd", 0);
		
		npcDat.set("baseShldDef", 0);
		npcDat.set("baseAtkRange", 0);
		npcDat.set("baseShldRate", 0);
		npcDat.set("baseCritRate", 38);
		npcDat.set("baseRunSpd", 0);
		npcDat.set("baseWalkSpd", 0);
		
		int collisionRadius; // (max) radius for movement checks
		int rangeXMin, rangeYMin, rangeZMin;
		int rangeXMax, rangeYMax, rangeZMax;
		// giran_clanhall_002;22220011;83618;145382;-3360; 83614;145330;-3417; 83623;145383;-2967;158250;644;518;False
		rangeXMin = 83614;
		rangeYMin = 145330;
		rangeZMin = -3417;
		rangeXMax = 83623;
		rangeYMax = 145383;
		rangeZMax = -2967;
		if (rangeXMax - rangeXMin > rangeYMax - rangeYMin)
			collisionRadius = rangeYMax - rangeYMin;
		else
			collisionRadius = rangeXMax - rangeXMin;
		
		npcDat.set("collision_radius", collisionRadius);
		npcDat.set("collision_height", rangeZMax - rangeZMin & 0xfff0);
		
		L2CharTemplate template;
		L2DoorInstance door;
		
		template = new L2CharTemplate(npcDat);
		door = (ti.getDoors()[0] = new L2DoorInstance(IdFactory.getInstance().getNextId(), template, 22220011,
				"tutorial_door_1", false)
		{
			@Override
			public void onActionShift(L2PcInstance initiator)
			{
				if (initiator != player)
					return;
				
				if (isOpen())
				{
					closeMe();
					ti.setFirstDoorTested();
					
					if (ti.areDoorsTested())
						updateState(player, TutorialState.CONQUEST_SECURITY);
				}
				else
					openMe();
			}
		});
		door.setRange(rangeXMin, rangeYMin, rangeZMin, rangeXMax, rangeYMax, rangeZMax);
		door.getStatus().setCurrentHpMp(door.getMaxHp(), door.getMaxMp());
		door.setOpen(false);
		door.getPosition().setXYZInvisible(83618, 145382, -3360);
		door.setMapRegion(MapRegionManager.getInstance().getRegion(83618, 145382));
		door.setInstanceId(ti.getInstance());
		door.spawnMe();
		
		// giran_clanhall_001;22220012;83618;145382;-3360; 83614;145280;-3417; 83622;145332;-2967;158250;644;518;False
		rangeXMin = 83614;
		rangeYMin = 145280;
		rangeZMin = -3417;
		rangeXMax = 83622;
		rangeYMax = 145332;
		rangeZMax = -2967;
		if (rangeXMax - rangeXMin > rangeYMax - rangeYMin)
			collisionRadius = rangeYMax - rangeYMin;
		else
			collisionRadius = rangeXMax - rangeXMin;
		
		npcDat.set("collision_radius", collisionRadius);
		npcDat.set("collision_height", rangeZMax - rangeZMin & 0xfff0);
		
		template = new L2CharTemplate(npcDat);
		door = (ti.getDoors()[1] = new L2DoorInstance(IdFactory.getInstance().getNextId(), template, 22220012,
				"tutorial_door_2", false)
		{
			@Override
			public void onActionShift(L2PcInstance initiator)
			{
				if (initiator != player)
					return;
				
				if (isOpen())
				{
					closeMe();
					ti.setSecondDoorTested();
					
					if (ti.areDoorsTested())
						updateState(player, TutorialState.CONQUEST_SECURITY);
				}
				else
					openMe();
			}
		});
		door.setRange(rangeXMin, rangeYMin, rangeZMin, rangeXMax, rangeYMax, rangeZMax);
		door.getStatus().setCurrentHpMp(door.getMaxHp(), door.getMaxMp());
		door.setOpen(false);
		door.getPosition().setXYZInvisible(83618, 145382, -3360);
		door.setMapRegion(MapRegionManager.getInstance().getRegion(83618, 145382));
		door.setInstanceId(ti.getInstance());
		door.spawnMe();
		
		showConquest(player, true);
		RespawnManager.getInstance().moveToSpawn(player);
	}
	
	private void endTutorial(L2PcInstance player)
	{
		TutorialInfo ti = getNewBees().get(player.getObjectId());
		if (ti == null)
			return;
		
		long exp = Experience.LEVEL[75] / 700;
		int sp = Calc.calcSp(exp);
		
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, i18n.get(loc, "TUTORIAL_TITLE"));
		tb.append(i18n.get(loc, "TUTORIAL_COMPLETED", "%exp%", String.valueOf(exp), "%sp%", String.valueOf(sp)));
		tb.append("<br>");
		L2Htm.writeTutorialCloseButton(tb, 150, 30, i18n.get(loc, "CLOSE"));
		L2Htm.writeFooter(tb);
		
		player.sendPacket(new ShowTutorialHtml(tb.moveToString()));
		
		player.addExpAndSp(exp, sp);
		
		cleanUp(player);
		
		if (Config.CONQUEST_DUALISM)
			ConquestFactionManager.getInstance().showChooseSide(player);
		RespawnManager.getInstance().moveToSpawn(player);
	}
	
	public void cleanUp(L2PcInstance player)
	{
		if (player == null)
			return;
		
		TutorialInfo ti = getNewBees().remove(player.getObjectId());
		if (ti == null)
			return;
		
		player.removeSkill(Skills.TUTORIAL_SPEED);
		
		GlobalEvent ge = ti.getEvent();
		if (ge != null)
		{
			ge.remove(player);
			ge.onFinish();
		}
		
		L2Npc npc = ti.getNpc();
		if (npc != null)
			npc.deleteMe();
		
		for (L2DoorInstance door : ti.getDoors())
		{
			if (door != null)
			{
				door.doDie(null);
				door.decayMe();
			}
		}
		
		EventFenceManager.getInstance().unspawnFence(2, ti.getInstance());
		PooledInstanceManager.getInstance().destroyInstance(ti.getInstance());
	}
	
	public void updateState(L2PcInstance player, TutorialState state)
	{
		TutorialInfo ti = getNewBees().get(player.getObjectId());
		if (ti == null)
			return;
		ti.setState(state);
		
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement("UPDATE character_tutorial SET state=? WHERE charId=?");
			ps.setInt(1, state.ordinal());
			ps.setInt(2, player.getObjectId());
			ps.executeUpdate();
			ps.close();
		}
		catch (SQLException e)
		{
			_log.error("Cannot update tutorial state for " + player);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
		
		stateChanged(player, state);
	}
	
	public void handleBypass(L2PcInstance player, String command)
	{
		player.sendPacket(HideTutorialHtml.PACKET);
		
		StringTokenizer st = new StringTokenizer(command, " ");
		st.nextToken(); // prefix
		
		int request;
		try
		{
			request = Integer.parseInt(st.nextToken());
		}
		catch (RuntimeException e)
		{
			return;
		}
		
		if (request < 0 || request > TutorialState.values().length)
			return;
		
		TutorialState next = TutorialState.values()[request];
		updateState(player, next);
	}
	
	private Map<Integer, TutorialInfo> getNewBees()
	{
		return _newBees;
	}
	
	public Location getBase()
	{
		return _base;
	}
	
	public Location getHall()
	{
		return _hall;
	}
	
	public static TutorialManager getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static final class SingletonHolder
	{
		private static final TutorialManager INSTANCE = new TutorialManager();
	}
}
