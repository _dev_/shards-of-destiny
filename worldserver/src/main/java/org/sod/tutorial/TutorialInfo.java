/*
 * MISSING LICENSING INFO
 */
package org.sod.tutorial;

import org.sod.manager.RespawnManager;
import org.sod.model.event.global.GlobalEvent;

import com.l2jfree.gameserver.model.Location;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.instance.L2DoorInstance;

/**
 * @author savormix
 * 
 */
public class TutorialInfo
{
	private final int _instance;
	private TutorialState _state;
	private L2Npc _npc;
	private GlobalEvent _event;
	private L2DoorInstance[] _doors;
	private int _doorState;
	
	public TutorialInfo(int instance, TutorialState state)
	{
		_instance = instance;
		_state = state;
		_doors = new L2DoorInstance[2];
	}
	
	public int getInstance()
	{
		return _instance;
	}
	
	public TutorialState getState()
	{
		return _state;
	}
	
	public void setState(TutorialState state)
	{
		_state = state;
	}
	
	public GlobalEvent getEvent()
	{
		return _event;
	}
	
	public void setEvent(GlobalEvent event)
	{
		_event = event;
	}
	
	public L2Npc getNpc()
	{
		return _npc;
	}
	
	public void setNpc(L2Npc npc)
	{
		_npc = npc;
	}
	
	public L2DoorInstance[] getDoors()
	{
		return _doors;
	}
	
	public boolean areDoorsTested()
	{
		return (_doorState & 3) == 3;
	}
	
	public void setFirstDoorTested()
	{
		_doorState |= 1;
	}
	
	public void setSecondDoorTested()
	{
		_doorState |= 2;
	}
	
	public Location getSpawn()
	{
		switch (getState())
		{
		case SELECT_LANGUAGE:
		case CHOOSE_TO_SKIP:
			return TutorialManager.getInstance().getBase();
		case CONQUEST_DOORS:
			return TutorialManager.getInstance().getHall();
		default:
			return RespawnManager.getInstance().getBase().getLoc();
		}
	}
}
