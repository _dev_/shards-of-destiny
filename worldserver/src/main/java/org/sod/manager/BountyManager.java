/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.manager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.concurrent.atomic.AtomicLong;

import javolution.util.FastMap;
import javolution.util.FastMap.Entry;

import com.l2jfree.util.logging.L2Logger;


import com.l2jfree.L2DatabaseFactory;
import com.l2jfree.gameserver.model.L2World;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;

/**
 * TODO Auto-generated JavaDoc
 * 
 * @author savormix
 * @since 2010.01.31
 */
public final class BountyManager
{
	private static final L2Logger _log = L2Logger.getLogger(BountyManager.class);
	private static final String LOAD_BOUNTY = "SELECT * FROM character_bounty";
	private static final String ADD_BOUNTY = "INSERT INTO character_bounty VALUES (?,?)";
	private static final String UPDATE_BOUNTY = "UPDATE character_bounty SET bounty=? WHERE charId=?";
	private static final String REMOVE_BOUNTY = "DELETE FROM character_bounty WHERE charId=?";
	
	private final FastMap<Integer, AtomicLong> _bounty;
	private volatile Integer _mostWanted;
	
	private BountyManager()
	{
		_bounty = new FastMap<Integer, AtomicLong>().setShared(true);
		load();
		_log.info("BountyManager: loaded " + _bounty.size() + " wanted characters.");
	}
	
	private void load()
	{
		Connection con = null;
		long maxBounty = 0;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(LOAD_BOUNTY);
			while (rs.next())
			{
				int charId = rs.getInt("charId");
				long bounty = rs.getLong("bounty");
				_bounty.put(charId, new AtomicLong(bounty));
				if (bounty > maxBounty)
				{
					maxBounty = bounty;
					_mostWanted = charId;
				}
			}
			rs.close();
			st.close();
		}
		catch (Exception e)
		{
			_log.warn("Cannot load bounty data!", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	public Integer getMostWanted()
	{
		return _mostWanted;
	}
	
	public FastMap<Integer, AtomicLong> getBounties()
	{
		return _bounty;
	}
	
	private AtomicLong getCurrentBounty(Integer target)
	{
		AtomicLong cb = getBounties().get(target);
		if (cb == null)
		{
			cb = new AtomicLong();
			AtomicLong old = getBounties().putIfAbsent(target, cb);
			if (old != null)
				return old;
		}
		return cb;
	}
	
	public void increaseBounty(L2PcInstance employer, Integer target, long bounty)
	{
		AtomicLong current = getCurrentBounty(target);
		if (current.addAndGet(bounty) == bounty)
			storeBounty(target);
		else
			updateBounty(target);
		
		Integer mw = getMostWanted();
		AtomicLong mwBounty = null;
		if (mw != null)
			mwBounty = getBounties().get(mw);
		if (mwBounty == null || current.get() > mwBounty.get())
			_mostWanted = target;
		L2PcInstance player = L2World.getInstance().findPlayer(target);
		if (player != null)
		{
			TitleManager.getInstance().updateTitle(player, true);
			player.getSoDPlayer().sendLocalizedMessage("BOUNTY_ADDED", "%name%", employer.getName(), "%bounty%",
					String.valueOf(bounty));
		}
	}
	
	private void storeBounty(Integer target)
	{
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(ADD_BOUNTY);
			ps.setInt(1, target);
			ps.setLong(2, getCurrentBounty(target).get());
			ps.executeUpdate();
			ps.close();
		}
		catch (Exception e)
		{
			_log.error("Cannot store bounty for " + target, e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	private void updateBounty(Integer target)
	{
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(UPDATE_BOUNTY);
			ps.setLong(1, getCurrentBounty(target).get());
			ps.setInt(2, target);
			ps.executeUpdate();
			ps.close();
		}
		catch (Exception e)
		{
			_log.error("Cannot update bounty for " + target, e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	public long removeBounty(Integer player)
	{
		AtomicLong bounty = getBounties().remove(player);
		if (bounty == null)
			return 0;
		
		if (player == _mostWanted)
		{
			if (!getBounties().isEmpty())
			{
				long maxBounty = 0;
				for (Entry<Integer, AtomicLong> entry = _bounty.head(), end = _bounty.tail(); (entry = entry.getNext()) != end;)
				{
					if (entry.getValue().get() > maxBounty)
					{
						maxBounty = entry.getValue().get();
						_mostWanted = entry.getKey();
					}
				}
			}
			else
				_mostWanted = null;
		}
		
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(REMOVE_BOUNTY);
			ps.setInt(1, player);
			ps.executeUpdate();
			ps.close();
		}
		catch (Exception e)
		{
			_log.error("Cannot remove bounty for " + player, e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
		return bounty.get();
	}
	
	public static BountyManager getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final BountyManager INSTANCE = new BountyManager();
	}
}
