/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.manager;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;

import javax.xml.parsers.DocumentBuilderFactory;

import javolution.util.FastMap;

import com.l2jfree.util.logging.L2Logger;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.l2jfree.Config;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;

/**
 * Manages player titles.
 * 
 * @author savormix
 * @since 2010.01.19
 */
public final class TitleManager
{
	private static final L2Logger _log = L2Logger.getLogger(TitleManager.class);
	
	private final FastMap<Integer, String> _titles;
	private Integer[] _titleChanges;
	
	private TitleManager()
	{
		_titles = new FastMap<Integer, String>().setShared(true);
		reload();
	}
	
	public String getTitle(int points)
	{
		int size = _titleChanges.length;
		for (int i = 0; i < size; i++)
			if (points < _titleChanges[i])
				return _titles.get(_titleChanges[i - 1]);
		return _titles.get(_titleChanges[size - 1]);
	}
	
	@SuppressWarnings("unused")
	private boolean isTitleChange(Integer points)
	{
		return _titles.containsKey(points);
	}
	
	public void updateTitle(L2PcInstance player, boolean force)
	{
		if (player.isGM() && !player.getTitle().isEmpty())
			return;
		if (!force/* && !isTitleChange(player.getPvpKills())*/)
			return;
		AtomicLong bounty = BountyManager.getInstance().getBounties().get(player.getObjectId());
		if (bounty != null)
			player.setTitle("Bounty: " + bounty.get());
		else
			player.setTitle(Config.SERVER_NAME);
		// player.setTitle(getTitle(player.getPvpKills() + player.getPkKills()));
		player.broadcastTitleInfo();
	}
	
	public void reload()
	{
		File xml = new File(Config.DATAPACK_ROOT, "data/stats/titles.xml");
		try
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			factory.setIgnoringComments(true);
			Document doc = factory.newDocumentBuilder().parse(xml);
			parseDocument(doc);
		}
		catch (Exception e)
		{
			_log.error("Cannot load titles!", e);
		}
		_log.info("TitleManager: Loaded " + _titles.size() + " title(s).");
	}
	
	private void parseDocument(Document doc) throws Exception
	{
		ArrayList<Integer> list = new ArrayList<Integer>();
		for (Node n = doc.getFirstChild(); n != null; n = n.getNextSibling())
			if ("list".equalsIgnoreCase(n.getNodeName()))
				for (Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
					if ("title".equalsIgnoreCase(d.getNodeName()))
					{
						Integer pts = Integer.valueOf(d.getAttributes().getNamedItem("points").getNodeValue());
						_titles.put(pts, d.getTextContent());
						list.add(pts);
					}
		_titleChanges = list.toArray(new Integer[list.size()]);
	}
	
	public static TitleManager getInstance()
	{
		return SingletonHolder._instance;
	}
	
	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final TitleManager _instance = new TitleManager();
	}
}
