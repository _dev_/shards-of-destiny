/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.manager;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import javolution.util.FastMap;
import javolution.util.FastSet;

import org.sod.Calc;
import org.sod.L2Htm;
import org.sod.i18n.Internationalization;
import org.sod.model.L2PlayerData;
import org.sod.model.PlayerSettings;
import org.sod.model.event.EventRequest;
import org.sod.model.event.global.GlobalEvent;
import org.sod.tutorial.TutorialManager;

import com.l2jfree.Config;
import com.l2jfree.gameserver.ThreadPoolManager;
import com.l2jfree.gameserver.communitybbs.Manager.SettingsBBSManager;
import com.l2jfree.gameserver.communitybbs.Manager.req.SetupRequest;
import com.l2jfree.gameserver.model.L2World;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.Chat;
import com.l2jfree.gameserver.network.client.packets.sendable.NpcHtmlMessage;
import com.l2jfree.gameserver.util.Broadcast;
import com.l2jfree.lang.L2TextBuilder;
import com.l2jfree.util.logging.L2Logger;

/**
 * This manager provides an easy interface to managing & integrating global events.
 * 
 * @author savormix
 * @since 2010.03.27
 */
public final class GlobalEventManager
{
	private static final L2Logger _log = L2Logger.getLogger(GlobalEventManager.class);
	
	/** Voting system inactive */
	public static final int STATUS_IDLE = 0;
	/** Voting in progress, perhaps a mini-event in progress */
	public static final int STATUS_VOTING = 1;
	/** An event in progress */
	public static final int STATUS_ACTIVE = 2;
	
	private static final int WINDOW_UPDATE_INTERVAL = 1100;
	
	private final FastMap<Integer, EventGroup> _events;
	private final FastMap<Integer, EventGroup> _miniEvents;
	private final FastMap<Integer, Integer> _votes;
	private final ResultWindow _resWin;
	private volatile int _status;
	private AtomicInteger _totalVotes;
	private Event _mostVoted;
	private Event _miniEvent;
	private ScheduledFuture<?> _eventTask;
	private FastSet<Integer> _winners;
	
	private GlobalEventManager()
	{
		_events = new FastMap<Integer, EventGroup>().setShared(true);
		_miniEvents = new FastMap<Integer, EventGroup>().setShared(true);
		_votes = new FastMap<Integer, Integer>().setShared(true);
		_resWin = new ResultWindow();
		
		_status = STATUS_IDLE;
		_totalVotes = new AtomicInteger();
		_winners = FastSet.newInstance();
		_log.info("GlobalEventManager: initialized.");
	}
	
	/** @return all registered events */
	public FastMap<Integer, EventGroup> getEvents()
	{
		return _events;
	}
	
	/** @return all registered mini-events */
	public FastMap<Integer, EventGroup> getMiniEvents()
	{
		return _miniEvents;
	}
	
	/** @return last event winner object IDs, if last event was a teamed event */
	public FastSet<Integer> getWinners()
	{
		return _winners;
	}
	
	/**
	 * Report event winners (they will be allowed to
	 * participate in a mini-event). Should be called
	 * from teamed events only.
	 * 
	 * @param winners
	 *            a set of winner object IDs
	 */
	public boolean setWinners(FastSet<Integer> winners, GlobalEvent event)
	{
		if (getState() != STATUS_ACTIVE)
			return false;
		if (getEvent().getEvent() != event)
			return false;
		
		if (getWinners() != winners)
		{
			FastSet.recycle(_winners);
			_winners = winners;
		}
		return true;
	}
	
	/**
	 * Start the voting system. Unless {@link Config#AUTOMATIC_EVENTS} is enabled, the system will stop after one cycle.
	 * 
	 * @return true, unless a system cycle is already in progress
	 */
	public synchronized boolean startTask()
	{
		if (_eventTask != null)
			return false;
		_eventTask = ThreadPoolManager.getInstance().scheduleGeneral(new EventTask(), 1000);
		return true;
	}
	
	/**
	 * Cancel the current cycle and restart the voting system.
	 * Unless {@link Config#AUTOMATIC_EVENTS} is enabled, the
	 * system will stop after one cycle.
	 * 
	 * @return true, unless the voting system is idle
	 */
	public synchronized boolean restartTask()
	{
		if (_eventTask != null)
		{
			_eventTask.cancel(false);
			_eventTask = null;
		}
		else
			return false;
		if (_status == STATUS_VOTING)
			endVoting();
		if (_status == STATUS_ACTIVE)
			endEvent(true);
		return startTask();
	}
	
	/**
	 * Attempts to send an event election bulletin to the player.
	 * 
	 * @param player
	 *            A player
	 */
	public void askVote(L2PcInstance player)
	{
		if (_status == STATUS_VOTING && !_votes.containsKey(player.getObjectId()))
			showVoteWindow(false, player);
	}
	
	private void register(EventGroup eg, FastMap<Integer, EventGroup> map)
	{
		if (eg != null)
			map.put(eg.getId(), eg);
	}
	
	/**
	 * Register an event group to the voting system.
	 * If the group ID is already in use, this group
	 * will replace the previously registered group.
	 * 
	 * @param eg
	 *            An event group
	 * @param mini
	 *            Mini-event
	 */
	public void registerEventGroup(EventGroup eg, boolean mini)
	{
		register(eg, mini ? getMiniEvents() : getEvents());
	}
	
	/**
	 * Register an event to the voting system.
	 * 
	 * @param e
	 *            An event
	 * @param mini
	 *            Mini-event
	 */
	public void registerStandaloneEvent(Event e, boolean mini)
	{
		registerEventGroup(new EventGroup(e), mini);
	}
	
	/**
	 * Attempts to remove all event and mini-event registrations.
	 * 
	 * @return true if system is idle, false otherwise
	 */
	public boolean unregisterAllEvents()
	{
		if (_eventTask != null || _status != STATUS_IDLE)
			return false;
		_events.clear();
		_miniEvents.clear();
		_winners.clear();
		_mostVoted = null;
		_miniEvent = null;
		return true;
	}
	
	/**
	 * Get the vote count of a registered event group.
	 * 
	 * @param group
	 *            Event group ID
	 * @return group vote count
	 */
	public int getVotes(Integer group)
	{
		EventGroup eg = getEvents().get(group);
		if (eg == null)
			return 0;
		else
			return eg.getVotes();
	}
	
	/**
	 * Register a vote for a registered event group.
	 * 
	 * @param player
	 *            Player object ID
	 * @param group
	 *            Event group ID
	 */
	public void addVote(Integer player, Integer group)
	{
		if (_status != STATUS_VOTING)
			return;
		
		removeVote(player);
		
		EventGroup eg = getEvents().get(group);
		if (eg != null)
		{
			_votes.put(player, group);
			eg.addVote();
			_totalVotes.incrementAndGet();
		}
	}
	
	/**
	 * Remove a vote for a registered event group.
	 * 
	 * @param player
	 *            Player object ID
	 */
	public void removeVote(Integer player)
	{
		Integer group = _votes.remove(player);
		if (group != null)
		{
			EventGroup eg = getEvents().get(group);
			if (eg != null)
			{
				eg.removeVote();
				_totalVotes.decrementAndGet();
			}
		}
	}
	
	/**
	 * Handles global event chat bypasses.
	 * 
	 * @param command
	 *            Bypass string
	 * @param player
	 *            A player
	 */
	public void handleBypass(String command, L2PcInstance player)
	{
		StringTokenizer st = new StringTokenizer(command, " ");
		st.nextToken(); // prefix
		
		EventRequest er = L2Htm.getRequest(st, EventRequest.values());
		if (er == null)
			return;
		
		switch (er)
		{
		case JOIN_EVENT:
			if (getState() == STATUS_ACTIVE)
				getEvent().getEvent().add(player);
			break;
		case JOIN_MINI_EVENT:
			Event e = getMiniEvent();
			if (e != null && getState() == STATUS_VOTING)
				e.getEvent().add(player);
			break;
		case SHOW_VOTING:
			if (getState() == STATUS_VOTING)
				showVoteWindow(true, player);
			break;
		case CAST_VOTE:
			Integer group;
			try
			{
				group = Integer.valueOf(st.nextToken());
			}
			catch (RuntimeException ex)
			{
				return;
			}
			addVote(player.getObjectId(), group);
			SettingsBBSManager.getInstance().showGeInfo(player, 0);
			break;
		default:
			_log.warn("Unhandled event request: " + er);
			break;
		}
	}
	
	/**
	 * Get current voting results.
	 * 
	 * @param page
	 *            Page No.
	 * @return voting results page
	 */
	public String getPrelimResults(int page, L2PcInstance viewer)
	{
		return _resWin.getResults(page, viewer);
	}
	
	/**
	 * Notifies the voting system that an event ended prematurely
	 * because the point limit was reached.
	 * 
	 * @param ge
	 *            An event
	 */
	public void notifyEventComplete(GlobalEvent ge)
	{
		if (_status != STATUS_ACTIVE || !getEvents().containsKey(ge.getEventId()))
			return;
		if (getEvent().getEvent() != ge)
			return;
		
		if (_eventTask != null)
		{
			_eventTask.cancel(true);
			_eventTask = null;
		}
		endEvent(false);
		startTask();
	}
	
	private void startVoting()
	{
		_status = STATUS_VOTING;
		_log.info("GlobalEventManager: Voting started");
		for (L2PcInstance player : L2World.getInstance().getAllPlayers())
			showVoteWindow(false, player);
		
		if (getWinners().size() < 2)
			return;
		
		int op = L2World.getInstance().getAllPlayersCount();
		List<EventGroup> list = new LinkedList<EventGroup>();
		for (EventGroup eg : getMiniEvents().values())
			if (op >= eg.getMinOnline())
				list.add(eg);
		if (!list.isEmpty())
		{
			_miniEvent = Calc.getRandomFromList(list).getEvent();
			Broadcast.toAllOnlinePlayers(Chat.COMMANDER, _miniEvent.getName(), "EVENT_OPEN_FOR_WINNERS");
			_log.info("GlobalEventManager: Started mini-event " + _miniEvent.getName());
		}
	}
	
	private boolean endVoting()
	{
		if (_miniEvent != null)
		{
			Broadcast.toAllOnlinePlayers(Chat.COMMANDER, _miniEvent.getName(), "EVENT_FINISHED");
			_miniEvent.getEvent().onFinish();
			_log.info("GlobalEventManager: Ended mini-event " + _miniEvent.getName());
			_miniEvent = null;
		}
		
		_status = STATUS_IDLE;
		if (_totalVotes.get() == 0 && _votes.isEmpty())
		{
			Broadcast.toAllOnlinePlayers(Chat.COMMANDER, "EVENT_ELECTION_TITLE", true, "EVENT_NOT_VOTED");
			_log.info("GlobalEventManager: Voting session will continue");
			return false;
		}
		
		int votes = -1;
		List<EventGroup> best = new LinkedList<EventGroup>();
		for (FastMap.Entry<Integer, EventGroup> entry = getEvents().head(), end = getEvents().tail(); (entry = entry
				.getNext()) != end;)
		{
			EventGroup eg = entry.getValue();
			_log.info("GlobalEventManager: Group " + eg.getName() + " received " + eg.getVotes() + " votes");
			if (eg.getVotes() == votes)
				best.add(eg);
			else if (eg.getVotes() > votes)
			{
				best.clear();
				best.add(eg);
				votes = eg.getVotes();
			}
			eg.resetVotes();
		}
		_votes.clear();
		_totalVotes.set(0);
		if (best.isEmpty())
			return false; // no events registered
			
		_log.info("GlobalEventManager: An event will be picked from " + best.size() + " group(s)");
		
		EventGroup group = Calc.getRandomFromList(best);
		_log.info("GlobalEventManager: Selected group " + group.getName());
		_mostVoted = group.getEvent();
		if (_mostVoted == null)
		{
			Broadcast.toAllOnlinePlayers(Chat.COMMANDER, group.getName(), "EVENT_VOTED_INVALID");
			_log.info("GlobalEventManager: Voters have left the game...");
			return false;
		}
		
		_status = STATUS_ACTIVE;
		_mostVoted.getEvent().onStart();
		Broadcast.toAllOnlinePlayers(Chat.COMMANDER, _mostVoted.getName(), "EVENT_READY");
		_log.info("GlobalEventManager: Started event " + _mostVoted.getName());
		return true;
	}
	
	private void endEvent(boolean timeout)
	{
		_status = STATUS_IDLE;
		Broadcast.toAllOnlinePlayers(Chat.COMMANDER, _mostVoted.getName(), "EVENT_FINISHED");
		if (timeout)
			_mostVoted.getEvent().onFinish();
		_log.info("GlobalEventManager: Ended event " + _mostVoted.getName());
		_mostVoted = null;
	}
	
	public int getState()
	{
		return _status;
	}
	
	public Event getMiniEvent()
	{
		return _miniEvent;
	}
	
	public Event getEvent()
	{
		return _mostVoted;
	}
	
	public int getTimeLeft(TimeUnit unit)
	{
		return (int) _eventTask.getDelay(unit);
	}
	
	private final class EventTask implements Runnable
	{
		@Override
		public void run()
		{
			switch (_status)
			{
			case STATUS_IDLE:
				startVoting();
				_eventTask = ThreadPoolManager.getInstance().scheduleGeneral(this, Config.AUTO_EVENT_SELECTION_PERIOD);
				break;
			case STATUS_VOTING:
				if (endVoting())
					_eventTask = ThreadPoolManager.getInstance()
							.scheduleGeneral(this, Config.AUTO_EVENT_PLAYING_PERIOD);
				else
					run();
				break;
			case STATUS_ACTIVE:
				endEvent(true);
				if (Config.AUTO_EVENTS && L2World.getInstance().getAllPlayersCount() > 0)
					run();
				else
					_eventTask = null;
				break;
			default:
				_log.error("Unhandled manager state: " + _status);
				break;
			}
		}
	}
	
	private void showVoteWindow(boolean force, L2PcInstance player)
	{
		if (player == null || TutorialManager.getInstance().isInTutorial(player))
			return;
		
		L2PlayerData dat = player.getSoDPlayer();
		if (dat == null)
			return;
		PlayerSettings pset = dat.getSettings();
		if (!force && !pset.showVoting())
			return;
		
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = pset.getLocale();
		
		final int online = L2World.getInstance().getAllPlayersCount();
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, i18n.get(loc, "NPC_EVENT_MANAGER_TITLE"));
		tb.append(i18n.get(loc, "NPC_EVENT_MANAGER_AVAILABILITY_DISCLAIMER"));
		tb.append("<br>");
		tb.append(i18n.get(loc, "NPC_EVENT_MANAGER_VOTING_SELECT_EVENT"));
		tb.append("<br><table>");
		int eventNo = 0;
		for (FastMap.Entry<Integer, EventGroup> entry = getEvents().head(), end = getEvents().tail(); (entry = entry
				.getNext()) != end;)
		{
			EventGroup eg = entry.getValue();
			if ((eventNo & 1) == 0)
				tb.append("<tr>");
			tb.append("<td>");
			if (online >= eg.getMinOnline())
				L2Htm.writeButton(tb, "ge " + EventRequest.CAST_VOTE.ordinal(), eg.getId(), 135, 30, true, eg.getName());
			else
				L2Htm.writeDisabledButton(tb, 135, 30, eg.getName());
			tb.append("</td>");
			if ((eventNo & 1) == 1)
				tb.append("</tr>");
			eventNo++;
		}
		if ((eventNo & 1) == 0)
			tb.append("</tr>");
		tb.append("</table>");
		L2Htm.writeFooter(tb);
		
		NpcHtmlMessage htm = new NpcHtmlMessage(tb.moveToString());
		player.sendPacket(htm);
	}
	
	private final class ResultWindow
	{
		private static final int SQUARES = 10;
		private static final int EVENTS_PER_PAGE = 5;
		
		private String[][] _results = new String[SettingsBBSManager.getInstance().getAllLocales().length][];
		private volatile long _nextUpdate;
		
		private String getResults(int page, L2PcInstance player)
		{
			PlayerSettings ps = player.getSoDPlayer().getSettings();
			long now = System.currentTimeMillis();
			if (now > _nextUpdate)
			{
				_results[ps.getLocaleIndex()] = new String[getEvents().size() / EVENTS_PER_PAGE
						+ ((getEvents().size() % EVENTS_PER_PAGE > 0) ? 1 : 0)];
				_nextUpdate = now + WINDOW_UPDATE_INTERVAL;
				L2TextBuilder[] tbs = new L2TextBuilder[_results[ps.getLocaleIndex()].length];
				for (int i = 0; i < tbs.length; i++)
					tbs[i] = new L2TextBuilder("<table width=300>");
				double total = _totalVotes.get();
				int current = 0;
				for (FastMap.Entry<Integer, EventGroup> entry = getEvents().head(), end = getEvents().tail(); (entry = entry
						.getNext()) != end;)
				{
					int num = current++ / EVENTS_PER_PAGE;
					EventGroup eg = entry.getValue();
					int parts = (int) Math.round(eg.getVotes() / total * SQUARES);
					tbs[num].append("<tr><td>");
					tbs[num].append(eg.getName());
					tbs[num].append("</td>");
					for (int i = 0; i < parts; i++)
						tbs[num].append("<td><img src=\"L2UI_ct1.gauge_df_large_weight_center1\" width=10 height=16></td>");
					for (int i = parts; i < SQUARES; i++)
						tbs[num].append("<td><img src=\"L2UI_ct1.gauge_df_large_weight_bg_center1\" width=10 height=16></td>");
					tbs[num].append("</tr>");
				}
				Internationalization i18n = Internationalization.getInstance();
				for (int i = 0; i < tbs.length; i++)
				{
					tbs[i].append("</table><table><tr><td>");
					if (i > 0)
						L2Htm.writeButton(tbs[i],
								"_bbsgetfav;" + SetupRequest.GE_PRELIMINARY.ordinal() + ";" + (i - 1), null, 100, 30,
								false, i18n.get(ps.getLocale(), "PREVIOUS"));
					tbs[i].append("</td><td>");
					if (i < tbs.length - 1)
						L2Htm.writeButton(tbs[i],
								"_bbsgetfav;" + SetupRequest.GE_PRELIMINARY.ordinal() + ";" + (i + 1), null, 100, 30,
								false, i18n.get(ps.getLocale(), "NEXT"));
					tbs[i].append("</td></tr></table>");
					_results[ps.getLocaleIndex()][i] = tbs[i].moveToString();
				}
			}
			return _results[ps.getLocaleIndex()][page];
		}
	}
	
	/**
	 * This class represents an event interface to be used with the
	 * event voting system.
	 * 
	 * @author savormix
	 * @since 2011.01.25
	 */
	public static class Event
	{
		private final GlobalEvent _event;
		private final int _preferedOnline;
		private final String _name;
		
		/**
		 * Create an event wrapper to be used with the voting system.
		 * 
		 * @param preferedOnline
		 *            Required online players to allow voting/starting
		 * @param name
		 *            Event instance name (user-friendly, e.g. includes the location name)
		 * @param event
		 *            The event instance
		 */
		public Event(int preferedOnline, String name, GlobalEvent event)
		{
			_preferedOnline = preferedOnline;
			_name = name;
			_event = event;
		}
		
		/**
		 * Create an event wrapper to be used with the voting system.
		 * 
		 * @param name
		 *            Event instance name (user-friendly, e.g. includes the location name)
		 * @param event
		 *            The event instance
		 */
		public Event(String name, GlobalEvent event)
		{
			this(0, name, event);
		}
		
		/**
		 * Create an event wrapper to be used with the voting system.
		 * 
		 * @param preferedOnline
		 *            Required online players to allow voting/starting
		 * @param event
		 *            The event instance
		 */
		public Event(int preferedOnline, GlobalEvent event)
		{
			this(preferedOnline, event.getImplementationName(), event);
		}
		
		/**
		 * Create an event wrapper to be used with the voting system.
		 * 
		 * @param event
		 *            The event instance
		 */
		public Event(GlobalEvent event)
		{
			this(0, event);
		}
		
		/** @return event instance name */
		public String getName()
		{
			return _name;
		}
		
		/** @return player count to allow selection */
		public int getPreferedOnline()
		{
			return _preferedOnline;
		}
		
		/** @return the event instance */
		public GlobalEvent getEvent()
		{
			return _event;
		}
	}
	
	public static class EventGroup
	{
		private static final AtomicInteger ID = new AtomicInteger(0);
		private final int _id;
		private final String _name;
		private final List<Event> _events;
		private int _minOnline;
		private AtomicInteger _votes;
		
		private EventGroup(int id, String name, int minOnline)
		{
			_id = id;
			_name = name;
			_events = new LinkedList<Event>();
			_minOnline = minOnline;
			_votes = new AtomicInteger();
		}
		
		/**
		 * Creates an event group.
		 * 
		 * @param id
		 *            Group ID, must be > 0
		 * @param name
		 *            Group name
		 */
		public EventGroup(int id, String name)
		{
			this(id, name, Integer.MAX_VALUE);
		}
		
		private EventGroup(Event event)
		{
			this(ID.decrementAndGet(), event.getName());
		}
		
		/**
		 * Adds an event wrapper to the event group.
		 * 
		 * @param event
		 *            An event wrapper
		 */
		public void add(Event event)
		{
			_events.add(event);
			if (event.getPreferedOnline() < _minOnline)
				_minOnline = event.getPreferedOnline();
		}
		
		/** @return the group ID */
		public int getId()
		{
			return _id;
		}
		
		/** @return the group name */
		public String getName()
		{
			return _name;
		}
		
		/**
		 * Picks a random event from the group that meets the online player criteria.
		 * 
		 * @return an event wrapper
		 */
		public Event getEvent()
		{
			int op = L2World.getInstance().getAllPlayersCount();
			List<Event> list = new LinkedList<Event>();
			for (Event e : _events)
				if (op >= e.getPreferedOnline())
					list.add(e);
			return Calc.getRandomFromList(list);
		}
		
		/** @return player count to allow voting for this group */
		public int getMinOnline()
		{
			return _minOnline;
		}
		
		/** @return current vote count */
		public int getVotes()
		{
			return _votes.get();
		}
		
		/** adds a single vote */
		public void addVote()
		{
			_votes.incrementAndGet();
		}
		
		/** removes a single vote */
		public void removeVote()
		{
			_votes.decrementAndGet();
		}
		
		/** removes all votes */
		public void resetVotes()
		{
			_votes.set(0);
		}
	}
	
	public static GlobalEventManager getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final GlobalEventManager INSTANCE = new GlobalEventManager();
	}
}
