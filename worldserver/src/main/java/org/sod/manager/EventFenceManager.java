/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.manager;

import java.io.File;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.xml.parsers.DocumentBuilderFactory;

import javolution.util.FastMap;

import com.l2jfree.util.logging.L2Logger;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.l2jfree.Config;
import com.l2jfree.gameserver.model.L2Spawn;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.instance.L2FenceInstance;

/**
 * Manages fences, fenced areas and fence geodata.
 * @author savormix
 * @since 2010.11.28
 */
public class EventFenceManager
{
	private static final L2Logger _log = L2Logger.getLogger(EventFenceManager.class);
	private static final int FENCE_NPC = 90069;
	
	public static final int TYPE_NO_FENCE = 0;
	public static final int TYPE_CORNERS = 1;
	public static final int TYPE_FULL_FENCE = 2;
	
	private final FastMap<Integer, FenceInfo> _fences;
	private final List<L2FenceInstance> _spawnedFences;
	private final FastMap<L2Character, FenceInfo> _fencedCharacters;
	
	private EventFenceManager()
	{
		_fences = new FastMap<Integer, FenceInfo>().setShared(true);
		// NOTE: we can spawn several fences in order to have a tall fence
		_spawnedFences = new CopyOnWriteArrayList<L2FenceInstance>();
		_fencedCharacters = new FastMap<L2Character, FenceInfo>().setShared(true);
		reload();
	}
	
	public void reload()
	{
		getFences().clear();
		File xml = new File(Config.DATAPACK_ROOT, "data/stats/fences.xml");
		try
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			factory.setIgnoringComments(true);
			Document doc = factory.newDocumentBuilder().parse(xml);
			parseDocument(doc);
		}
		catch (Exception e)
		{
			_log.error("Cannot load fences!", e);
		}
		_log.info("EventFenceManager: Loaded " + _fences.size() + " fenced area(s).");
	}
	
	private void parseDocument(Document doc) throws Exception
	{
		for (Node n = doc.getFirstChild(); n != null; n = n.getNextSibling())
			if ("list".equalsIgnoreCase(n.getNodeName()))
				for (Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
					if ("fence".equalsIgnoreCase(d.getNodeName()))
					{
						int id, t, x, y, z, w, h, minZ, maxZ;
						String name;
						id = Integer.parseInt(d.getAttributes().getNamedItem("id").getNodeValue());
						name = d.getAttributes().getNamedItem("name").getNodeValue();
						t = Integer.parseInt(d.getAttributes().getNamedItem("type").getNodeValue());
						x = Integer.parseInt(d.getAttributes().getNamedItem("x").getNodeValue());
						y = Integer.parseInt(d.getAttributes().getNamedItem("y").getNodeValue());
						z = Integer.parseInt(d.getAttributes().getNamedItem("z").getNodeValue());
						w = Integer.parseInt(d.getAttributes().getNamedItem("width").getNodeValue());
						h = Integer.parseInt(d.getAttributes().getNamedItem("height").getNodeValue());
						minZ = Integer.parseInt(d.getAttributes().getNamedItem("minZ").getNodeValue());
						maxZ = Integer.parseInt(d.getAttributes().getNamedItem("maxZ").getNodeValue());
						FenceInfo fi = new FenceInfo(id, name, t, x, y, z, w, h, minZ, maxZ);
						getFences().put(id, fi);
					}
	}
	
	/**
	 * Spawn a visual representation of the fence in the world.
	 * @param id Fence ID
	 * @param instanceId Instance ID
	 */
	public void spawnFence(Integer id, int instanceId)
	{
		FenceInfo fi = getFences().get(id);
		if (fi == null)
			return;
		L2Spawn fs = new L2Spawn(FENCE_NPC);
		fs.setLocx(fi.getMiddleX());
		fs.setLocy(fi.getMiddleY());
		fs.setLocz(fi.getZ());
		fs.setInstanceId(instanceId);
		fs.stopRespawn();
		L2FenceInstance fence = (L2FenceInstance) fs.spawnOne(false);
		fence.setFence(fi);
		getSpawnedFences().add(fence);
		fence.broadcastFullInfo();
	}
	
	/**
	 * Remove the visual representation of a fence from the world.
	 * @param id Fence ID
	 * @param instanceId Instance ID
	 */
	public void unspawnFence(int id, int instanceId)
	{
		if (!getFences().containsKey(id))
			return;
		
		L2FenceInstance fence = null;
		for (L2FenceInstance f : getSpawnedFences())
		{
			if (f.getFence().getId() == id && f.getInstanceId() == instanceId)
			{
				fence = f;
				break;
			}
		}
		if (fence == null)
			return;

		fence.deleteMe();
		getSpawnedFences().remove(fence);
	}
	
	public boolean isInFencedArea(int x, int y, int z, FenceInfo fi)
	{
		if (fi == null)
			return true;
		else if (z < fi.getMinZ() || z > fi.getMaxZ())
			return false;
		else if (x < fi.getMinX() || x > fi.getMaxX())
			return false;
		else if (y < fi.getMinY() || y > fi.getMaxY())
			return false;
		else
			return true;
	}
	
	public boolean canMove(L2Character actor, int x, int y, int z)
	{
		FenceInfo fi = getFencedCharacters().get(actor);
		if (fi == null)
			return true;
		else
			return isInFencedArea(x, y, z, fi);
	}
	
	public void addCharacterToArea(Integer id, L2Character actor)
	{
		FenceInfo nf = getFences().get(id);
		if (nf == null || actor == null)
			return;
		/*FenceInfo fi = */getFencedCharacters().put(actor, nf);
		/*if (fi != null && fi.getId() != nf.getId())
			_log.warn(actor + " was in fenced area of " + fi.getName() + " and was moved to fenced area of " + nf.getName());
		 */
	}
	
	public void removeCharacterFromArea(int id, L2Character actor)
	{
		if (actor == null)
			return;
		FenceInfo fi = getFencedCharacters().get(actor);
		if (fi != null && fi.getId() == id)
			getFencedCharacters().remove(actor);
	}
	
	public FastMap<Integer, FenceInfo> getFences()
	{
		return _fences;
	}
	
	public List<L2FenceInstance> getSpawnedFences()
	{
		return _spawnedFences;
	}
	
	public FastMap<L2Character, FenceInfo> getFencedCharacters()
	{
		return _fencedCharacters;
	}
	
	public class FenceInfo
	{
		private final int _id;
		private final String _name;
		private final int _type;
		private final int _middleX;
		private final int _middleY;
		private final int _z;
		private final int _width;
		private final int _height;
		
		private final int _minZ;
		private final int _maxZ;
		private final int _minX;
		private final int _maxX;
		private final int _minY;
		private final int _maxY;
		
		public FenceInfo(int id, String name, int type, int x, int y, int z,
				int width, int height, int minZ, int maxZ)
		{
			_id = id;
			_name = name;
			_type = type;
			_middleX = x;
			_middleY = y;
			_z = z;
			_width = width;
			_height = height;
			
			_minZ = minZ;
			_maxZ = maxZ;
			int dev = width / 2 + 5;
			_minX = x - dev;
			_maxX = x + dev;
			dev = height / 2 + 5;
			_minY = y - dev;
			_maxY = y + dev;
		}
		
		public int getId()
		{
			return _id;
		}
		
		public String getName()
		{
			return _name;
		}
		
		public int getType()
		{
			return _type;
		}
		
		public int getMiddleX()
		{
			return _middleX;
		}
		
		public int getMiddleY()
		{
			return _middleY;
		}
		
		public int getZ()
		{
			return _z;
		}
		
		public int getMinZ()
		{
			return _minZ;
		}
		
		public int getMaxZ()
		{
			return _maxZ;
		}
		
		public int getWidth()
		{
			return _width;
		}
		
		public int getHeight()
		{
			return _height;
		}
		
		public int getMinX()
		{
			return _minX;
		}
		
		public int getMaxX()
		{
			return _maxX;
		}
		
		public int getMinY()
		{
			return _minY;
		}
		
		public int getMaxY()
		{
			return _maxY;
		}
	}
	
	public static EventFenceManager getInstance()
	{
		return SingletonHolder._instance;
	}
	
	private static class SingletonHolder
	{
		private static final EventFenceManager _instance = new EventFenceManager();
	}
}
