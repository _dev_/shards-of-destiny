/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.manager;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javolution.util.FastSet;

import com.l2jfree.util.logging.L2Logger;

import org.sod.EventReflection;
import org.sod.manager.conquest.ConquestFactionManager;
import org.sod.manager.conquest.ConquestFactionManager.FactionTemplate;
import org.sod.manager.conquest.ConquestZoneManager;
import org.sod.model.L2PlayerData;
import org.sod.tutorial.TutorialManager;

import com.l2jfree.Config;
import com.l2jfree.gameserver.GameServer;
import com.l2jfree.gameserver.GameServer.StartupHook;
import com.l2jfree.gameserver.ThreadPoolManager;
import com.l2jfree.gameserver.model.L2Clan;
import com.l2jfree.gameserver.model.L2Spawn;
import com.l2jfree.gameserver.model.Location;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.clientpackets.ConfirmDlgAnswer.AnswerHandler;
import com.l2jfree.gameserver.network.serverpackets.CameraMode;
import com.l2jfree.gameserver.network.serverpackets.ExShowScreenMessage;
import com.l2jfree.gameserver.network.serverpackets.Revive;
import com.l2jfree.gameserver.network.serverpackets.ValidateLocation;
import com.l2jfree.util.Rnd;

/**
 * Manages player respawns in Conquest.
 * 
 * @author savormix
 * @since 2010.01.01
 */
public final class RespawnManager
{
	private static final L2Logger _log = L2Logger.getLogger(RespawnManager.class);
	
	public static final int SPAWN_CYCLE = 30000;
	private static final int MESSAGE_TIME = 2500;
	private static final int STATUS_WAIT = 0;
	private static final int STATUS_INSTANT = 1;
	
	private final BaseInfo _base;
	private final ShopInfo[] _shops;
	// private final FastSet<L2PcInstance> _deadHome;
	private final FastSet<L2PcInstance> _deadHq;
	private final ScheduledFuture<?> _task;
	private volatile int _status;
	
	private RespawnManager()
	{
		_base = new BaseInfo(new Location(81515, 148610, -3460), PooledInstanceManager.getInstance().createInstance(),
				2);
		_shops = new ShopInfo[]
		{
			new ShopInfo(new Location(80430, 152190, -3525, 31710), new Location(79865, 151845, -3525),
					PooledInstanceManager.getInstance().createInstance(), 4),
		};
		EventReflection.getInstance().activate();
		// _deadHome = FastSet.newInstance();
		_deadHq = FastSet.newInstance();
		_task = ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(new Respawner(), SPAWN_CYCLE, SPAWN_CYCLE);
		_log.info("RespawnManager: initialized.");
		
		GameServer.addStartupHook(new StartupHook()
		{
			@Override
			public void onStartup()
			{
				EventFenceManager efm = EventFenceManager.getInstance();
				efm.spawnFence(_base.getFenceId(), _base.getInstanceId());
				
				L2Spawn sp = new L2Spawn(90091);
				for (ShopInfo si : getShops())
				{
					efm.spawnFence(si.getFenceId(), si.getInstanceId());
					sp.setLoc(si.getTeleporter());
					sp.setInstanceId(si.getInstanceId());
					sp.doSpawn();
				}
			}
		});
	}
	
	/**
	 * Add a [dead] player to be managed.
	 * 
	 * @param deceased
	 *            A [dead] player
	 * @param hq
	 *            Respawn to clan headquarters
	 */
	public void addPlayer(L2PcInstance deceased, boolean hq)
	{
		if (_status == STATUS_WAIT || hq)
		{
			// if ((hq ? _deadHq.add(deceased) : _deadHome.add(deceased)))
			if (_deadHq.add(deceased))
			{
				L2PlayerData dat = deceased.getSoDPlayer();
				if (dat.getSettings().isGhostMode())
					enterMovieMode(dat);
				else
					deceased.sendPacket(new ExShowScreenMessage("You will respawn in " + getNextSpawnAfter()
							+ " second(s).", MESSAGE_TIME));
			}
		}
		else
			revive(deceased, hq);
	}
	
	/**
	 * Remove a player from the managed list.
	 * 
	 * @param deceased
	 *            A player
	 * @return whether the player was removed
	 */
	public boolean removePlayer(L2PcInstance deceased)
	{
		if (_status == STATUS_WAIT)
			return (/*_deadHome.remove(deceased) || */_deadHq.remove(deceased));
		else
			return false;
	}
	
	private void revive(L2PcInstance player, boolean hq)
	{
		if (player == null || !player.isDead())
			return;
		player.setIsPendingRevive(true);
		if (hq)
			moveToHq(player);
		else
			moveToSpawn(player);
	}
	
	/**
	 * Move a player to their original spawn (base location).
	 * In conquest, player is moved to their faction's preparation area.
	 * 
	 * @param player
	 *            A player
	 */
	public void moveToSpawn(L2PcInstance player)
	{
		if (player == null || player.isInJail())
			return;
		
		TutorialManager tm = TutorialManager.getInstance();
		BaseInfo bi = tm.getSpawn(player);
		int df = player.getSoDPlayer().getDualismFaction();
		if (bi == null && Config.CONQUEST_DUALISM && df == ConquestFactionManager.NONE)
			bi = new BaseInfo(tm.getBase(), getBase().getInstanceId(), -1);
		if (bi == null)
			bi = ConquestFactionManager.getInstance().getFactionTemplate(player);
		if (bi == null && Config.CONQUEST_DUALISM)
			bi = ConquestFactionManager.getInstance().getFactionTemplate(df);
		if (bi == null)
			bi = getBase();
		moveToBase(bi, player);
	}
	
	private void moveToBase(BaseInfo bi, L2PcInstance player)
	{
		if (bi == null || player == null)
			return;
		
		EventFenceManager.getInstance().addCharacterToArea(bi.getFenceId(), player);
		player.setInstanceId(bi.getInstanceId());
		
		Location loc = bi.getLoc();
		if (loc == null)
			loc = getBase().getLoc();
		player.teleToLocation(loc);
		player.getStatus().setCurrentHpMp(player.getMaxHp(), player.getMaxMp());
	}
	
	/**
	 * If a player is participating in Conquest and it's clan has
	 * a headquarters, they are moved to HQ. Otherwise, they are moved
	 * to their original spawn.
	 * 
	 * @param player
	 */
	public void moveToHq(L2PcInstance player)
	{
		if (player == null || player.isInJail())
			return;
		
		L2Clan clan = player.getClan();
		Location loc = null;
		if (clan != null)
			loc = clan.getClosestFlag(player);
		if (loc != null)
		{
			player.setInstanceId(0);
			player.teleToLocation(loc, true);
		}
		else
			moveToSpawn(player);
	}
	
	/**
	 * Revives the player client side and opens first-person view.
	 * 
	 * @param dat
	 *            A [dead] player
	 */
	public void enterMovieMode(L2PlayerData dat)
	{
		L2PcInstance dead = dat.getActingPlayer();
		dead.setTarget(null);
		dead.setIsPetrified(true);
		dead.sendPacket(CameraMode.FIRST_PERSON);
		dead.sendPacket(new Revive(dead));
		dat.setInGhostMode(true);
	}
	
	/**
	 * Returns player to their body location and opens
	 * third-person view.
	 * 
	 * @param dat
	 *            A [revived] player
	 */
	public void leaveMovieMode(L2PlayerData dat)
	{
		dat.setInGhostMode(false);
		L2PcInstance dead = dat.getActingPlayer();
		dead.setIsPetrified(false);
		dead.sendPacket(new ValidateLocation(dead));
		dead.sendPacket(CameraMode.THIRD_PERSON);
	}
	
	public BaseInfo getBase()
	{
		return _base;
	}
	
	public ShopInfo[] getShops()
	{
		return _shops;
	}
	
	public boolean isInsideBase(L2Character cha)
	{
		if (cha == null || cha.isInMultiverse())
			return false;
		
		if (cha.isSameInstance(getBase().getInstanceId()))
			return true;
		
		for (BaseInfo bi : getShops())
			if (cha.isSameInstance(bi.getInstanceId()))
				return true;
		
		if (ConquestZoneManager.getInstance().isActive())
		{
			ConquestFactionManager cfm = ConquestFactionManager.getInstance();
			for (int i = ConquestFactionManager.ANCIENTS; i <= ConquestFactionManager.DIVINES; i++)
			{
				FactionTemplate ft = cfm.getFactionTemplate(i);
				if (cha.isSameInstance(ft.getInstanceId()))
					return true;
			}
		}
		
		return false;
	}
	
	public int getShopAreaId(L2Character cha)
	{
		if (cha != null && !cha.isInMultiverse())
		{
			for (int i = 0; i < getShops().length; i++)
			{
				BaseInfo bi = getShops()[i];
				if (cha.isSameInstance(bi.getInstanceId()))
					return i;
			}
		}
		return -1;
	}
	
	public void moveToShopArea(L2PcInstance player, int id)
	{
		if (id < getShops().length)
			moveToBase(getShops()[id], player);
	}
	
	private final class Respawner implements Runnable
	{
		@Override
		public void run()
		{
			_status = STATUS_INSTANT;
			for (FastSet.Record r = _deadHq.head(), end = _deadHq.tail(); (r = r.getNext()) != end;)
				revive(_deadHq.valueOf(r), true);
			_deadHq.clear();
			/*
			for (FastSet.Record r = _deadHome.head(), end = _deadHome.tail(); (r = r.getNext()) != end;)
				revive(_deadHome.valueOf(r), false);
			_deadHome.clear();
			 */
			_status = STATUS_WAIT;
		}
	}
	
	public final long getNextSpawnAfter()
	{
		return _task.getDelay(TimeUnit.SECONDS);
	}
	
	public static class ShopTransferHandler extends AnswerHandler
	{
		private final L2PcInstance _player;
		
		public ShopTransferHandler(L2PcInstance player)
		{
			_player = player;
		}
		
		@Override
		public void handle(boolean answer)
		{
			if (answer)
				getInstance().moveToShopArea(_player, Rnd.get(getInstance().getShops().length));
		}
	}
	
	public static class ShopInfo extends BaseInfo
	{
		private final Location _teleporter;
		
		protected ShopInfo(Location teleporter, Location loc, int instanceId, int fenceId)
		{
			super(loc, instanceId, fenceId);
			
			_teleporter = teleporter;
		}
		
		public Location getTeleporter()
		{
			return _teleporter;
		}
	}
	
	public static class BaseInfo
	{
		private Location _loc;
		private final int _instanceId;
		private int _fenceId;
		
		public BaseInfo(Location loc, int instanceId, int fenceId)
		{
			_loc = loc;
			_instanceId = instanceId;
			_fenceId = fenceId;
		}
		
		public Location getLoc()
		{
			return _loc;
		}
		
		public void setLoc(Location loc)
		{
			_loc = loc;
		}
		
		public int getInstanceId()
		{
			return _instanceId;
		}
		
		public int getFenceId()
		{
			return _fenceId;
		}
		
		public void setFenceId(int fenceId)
		{
			_fenceId = fenceId;
		}
	}
	
	public static final RespawnManager getInstance()
	{
		return SingletonHolder._instance;
	}
	
	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final RespawnManager _instance = new RespawnManager();
	}
}
