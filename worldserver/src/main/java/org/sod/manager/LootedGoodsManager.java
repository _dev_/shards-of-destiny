/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.manager;

import com.l2jfree.util.logging.L2Logger;

import org.sod.Calc;

import com.l2jfree.util.Rnd;

/**
 * This manager:
 * <LI>defines material categories
 * <LI>defines craftable A/S grade parts and recipes
 * <LI>handles distribution of treasure chests in PvP
 * <LI>handles extraction of treasure chests
 * @author savormix
 * @since 2010.02.28
 */
public class LootedGoodsManager
{
	private static final L2Logger _log = L2Logger.getLogger(LootedGoodsManager.class);
	
	/** No loot */
	public static final int NOTHING				= -1;
	public static final int GATEKEEPER_TOKEN	= 1659;
	/** Jade treasure chest */
	public static final int CHEST_MATERIALS		= 90002;
	/** White treasure chest */
	public static final int CHEST_PARTS			= 90003;
	/** Red treasure chest */
	public static final int CHEST_RECIPES		= 90004;
	
	/** Does array size equality checks */
	private LootedGoodsManager()
	{
		int l1, l2;
		if ((l1 = _recipesArmorA60.length) != (l2 = _recipesArmorA100.length))
			_log.warn("A grade armor recipes - 60%: " + l1 + " 100%: " + l2);
		if (l1 != (l2 = _partsArmorA.length))
			_log.warn("A grade armor recipes: " + l1 + " parts: " + l2);
		if ((l1 = _recipesArmorS60.length) != (l2 = _recipesArmorS100.length))
			_log.warn("S grade armor recipes - 60%: " + l1 + " 100%: " + l2);
		if (l1 != (l2 = _partsArmorS.length))
			_log.warn("S grade armor recipes: " + l1 + " parts: " + l2);
		if ((l1 = _recipesWeaponA_C3C5_60.length) != (l2 = _recipesWeaponA_C3C5_100.length))
			_log.warn("A grade weapon [C3-C5] recipes - 60%: " + l1 + " 100%: " + l2);
		if (l1 != (l2 = _partsWeaponA_C3C5_.length))
			_log.warn("A grade weapon [C3-C5] recipes: " + l1 + " parts: " + l2);
		if ((l1 = _recipesWeaponA_Int_60.length) != (l2 = _recipesWeaponA_Int_100.length))
			_log.warn("A grade weapon [Int] recipes - 60%: " + l1 + " 100%: " + l2);
		if (l1 != (l2 = _partsWeaponA_Int_.length))
			_log.warn("A grade weapon [Int] recipes: " + l1 + " parts: " + l2);
		if ((l1 = _recipesWeaponS60.length) != (l2 = _recipesWeaponS100.length - 1))
			_log.warn("S grade weapon recipes - 60%: " + l1 + " 100%: " + l2);
		if (l1 != (l2 = _partsWeaponS.length))
			_log.warn("S grade weapon recipes: " + l1 + " parts: " + l2);
	}
	
	// MATERIALS
	private final int[] _materialsLow = {
		1864, // Stem (0)
		1865, // Varnish (0)
		1866, // Suede (0)
		1867, // Animal Skin (0)
		1868, // Thread (0)
		1869, // Iron Ore (0)
		1870, // Coal (0)
		1871, // Charcoal (0)
		1872, // Animal Bone (0)
		1873, // Silver Nugget (0)
		1874, // Oriharukon Ore (0)
		1875, // Stone of Purity (0)
		1876, // Mithril Ore (0)
		1877, // Adamantite Nugget (0)
		1878, // Braided Hemp (1)
		1879, // Cokes (1)
		1880, // Steel (1)
		1881, // Coarse Bone Powder (1)
		1882, // Leather (1)
		1883, // Steel Mold (2)
		1884, // Cord (2)
		1885, // High Grade Suede (2)
		1886, // Silver Mold (2)
		1887, // Varnish of Purity (2)
		1888, // Synthetic Cokes (2)
		1889, // Compound Braid (2)
	};
	private final int[] _materialsMid = {
		4039, // Mold Glue (0)
		4040, // Mold Lubricant (0)
		4041, // Mold Hardener (0)
		4042, // Enria (0)
		4043, // Asofe (0)
		4044, // Thons (0)
		1890, // Mithril Alloy (4)
		1891, // Artisan's Frame (4)
		1892, // Blacksmith's Frame (4)
		1893, // Oriharukon (4)
		1894, // Crafted Leather (4)
		1895, // Metallic Fiber (4)
		5220, // Metal Hardener (4)
		5549, // Metallic Thread (4)
		5550, // Durable Metal Plate (4)
	};
	private final int[] _materialsHigh = {
		5551, // Leolin's Mold (5)
		4045, // Maestro Holder (6)
		4046, // Maestro Anvil Lock (6)
		4047, // Craftsman Mold (6)
		4048, // Maestro Mold (6)
		5552, // Warsmith's Mold (6)
		5553, // Arcsmith's Anvil (6)
		5554, // Warsmith's Holder (6)
	};
	
	// PARTS
	private final int[] _partsArmorA = {
		5478, // Sealed Dark Crystal Leather Armor Pattern
		5479, // Sealed Tallum Leather Armor Pattern
		5480, // Sealed Leather Armor of Nightmare Fabric
		5481, // Sealed Majestic Leather Armor Fabric
		5482, // Sealed Dark Crystal Leggings Design
		5485, // Sealed Tallum Tunic Texture
		5486, // Sealed Dark Crystal Robe Fabric
		5487, // Sealed Nightmare Robe Fabric
		5488, // Sealed Majestic Robe Fabric
		5489, // Sealed Tallum Stockings Fabric
		5494, // Sealed Dark Crystal Shield Fragment
		5495, // Sealed Shield of Nightmare Fragment
		5496, // Sealed Dark Crystal Boots Lining
		5497, // Sealed Tallum Boots Lining
		5502, // Sealed Boots of Nightmare Lining
		5503, // Sealed Majestic Boots Lining
		5508, // Sealed Dark Crystal Gloves Design
		5509, // Sealed Tallum Gloves Design
		5514, // Sealed Gauntlets of Nightmare Design
		5515, // Sealed Majestic Gauntlets Design
		5520, // Sealed Dark Crystal Breastplate Pattern
		5521, // Sealed Tallum Plate Armor Pattern
		5522, // Sealed Armor of Nightmare Pattern
		5523, // Sealed Majestic Plate Armor Pattern
		5524, // Sealed Dark Crystal Gaiters Pattern
		5525, // Sealed Dark Crystal Helmet Design
		5526, // Sealed Tallum Helm Design
		5527, // Sealed Helm of Nightmare Design
		5528, // Sealed Majestic Circlet Design
		6341, // Sealed Phoenix Earring Gemstone
		6342, // Sealed Majestic Earring Gemstone
		6343, // Sealed Phoenix Necklace Beads
		6344, // Sealed Majestic Necklace Beads
		6345, // Sealed Phoenix Ring Gemstone
		6346, // Sealed Majestic Ring Gemstone
	};
	private final int[] _partsArmorS = {
		6698, // Sealed Tateossian Earring Part
		6699, // Sealed Tateossian Ring Gem
		6700, // Sealed Tateossian Necklace Chain
		6701, // Sealed Imperial Crusader Breastplate Part
		6702, // Sealed Imperial Crusader Gaiters Pattern
		6703, // Sealed Imperial Crusader Gauntlets Design
		6704, // Sealed Imperial Crusader Boots Design
		6705, // Sealed Imperial Crusader Shield Part
		6706, // Sealed Imperial Crusader Helmet Pattern
		6707, // Sealed Draconic Leather Armor Part
		6708, // Sealed Draconic Leather Gloves Fabric
		6709, // Sealed Draconic Leather Boots Design
		6710, // Sealed Draconic Leather Helmet Pattern
		6711, // Sealed Major Arcana Robe Part
		6712, // Sealed Major Arcana Gloves fabric
		6713, // Sealed Major Arcana Boots Design
		6714, // Sealed Major Arcana Circlet Pattern
		13099, // Sealed Arcana Sigil Piece
	};
	private final int[] _partsWeaponA_C3C5_ = {
		// C3
		5529, // Dragon Slayer Edge
		5532, // Meteor Shower Head
		5533, // Elysian Head
		5534, // Soul Bow Stave
		5535, // Carnage Bow Stave
		5536, // Bloody Orchid Head
		5537, // Soul Separator Head
		5538, // Dragon Grinder Edge
		5539, // Blood Tornado Edge
		5540, // Orcish Halberd Edge
		5541, // Tallum Glaive Edge
		5542, // Halberd Edge
		5543, // Dasparion's Staff Edge
		5544, // Branch of The Mother Tree Head
		5545, // Dark Legion's Edge Blade
		5546, // Sword of Miracles Edge
		5547, // Elemental Sword Edge
		5548, // Tallum Blade Edge
		// C5
		8331, // Infernal Master Blade
		8341, // Spiritual Eye Piece
		8342, // Flaming Dragon Skull Piece
		8346, // Hammer Piece of Destroyer
		8349, // Doom Crusher Head
	};
	private final int[] _partsWeaponA_Int_ = {
		8712, // Sirra's Blade Edge
		8713, // Sword of Ipos Blade
		8714, // Barakiel's Axe Piece
		8715, // Behemoth's Tuning Fork Piece
		8716, // Naga Storm Piece
		8717, // Tiphon's Spear Edge
		8718, // Shyeed's Bow Shaft
		8719, // Sobekk's Hurricane Edge
		8720, // Themis' Tongue Piece
		8721, // Cabrio's Hand Head
		8722, // Daimon Crystal Fragment
	};
	private final int[] _partsWeaponS = {
		6688, // Forgotten Blade Edge
		6689, // Basalt Battlehammer Head
		6690, // Imperial Staff Head
		6691, // Angel Slayer Blade
		6692, // Shining Bow Shaft
		6693, // Dragon Hunter Axe Blade
		6694, // Saint Spear Blade
		6695, // Demon Splinter Blade
		6696, // Heavens Divider Edge
		6697, // Arcana Mace Head
		7579, // Draconic Bow Shaft
	};
	
	// RECIPES
	private final int[] _recipesArmorA60 = {
		5332, // Recipe: Sealed Dark Crystal Leather Armor(60%)
		5334, // Recipe: Sealed Tallum Leather Armor(60%)
		5336, // Recipe: Sealed Leather Armor of Nightmare(60%)
		5338, // Recipe: Sealed Majestic Leather Armor(60%)
		5340, // Recipe: Sealed Dark Crystal Leggings(60%)
		5346, // Recipe: Sealed Tallum Tunic(60%)
		5348, // Recipe: Sealed Dark Crystal Robe(60%)
		5350, // Recipe: Sealed Nightmare Robe(60%)
		5352, // Recipe: Sealed Majestic Robe(60%)
		5354, // Recipe: Sealed Tallum Stockings(60%)
		5364, // Recipe: Sealed Dark Crystal Shield(60%)
		5366, // Recipe: Sealed Shield of Nightmare(60%)
		5368, // Recipe: Sealed Dark Crystal Boots(60%)
		5370, // Recipe: Sealed Tallum Boots(60%)
		5380, // Recipe: Sealed Boots of Nightmare(60%)
		5382, // Recipe: Sealed Majestic Boots(60%)
		5392, // Recipe: Sealed Dark Crystal Gloves(60%)
		5394, // Recipe: Sealed Tallum Gloves(60%)
		5404, // Recipe: Sealed Gauntlets of Nightmare(60%)
		5406, // Recipe: Sealed Majestic Gauntlets(60%)
		5416, // Recipe: Sealed Dark Crystal Breastplate(60%)
		5418, // Recipe: Sealed Tallum Plate Armor(60%)
		5420, // Recipe: Sealed Armor of Nightmare(60%)
		5422, // Recipe: Sealed Majestic Plate Armor(60%)
		5424, // Recipe: Sealed Dark Crystal Gaiters(60%)
		5426, // Recipe: Sealed Dark Crystal Helmet(60%)
		5428, // Recipe: Sealed Tallum Helmet(60%)
		5430, // Recipe: Sealed Helm of Nightmare(60%)
		5432, // Recipe: Sealed Majestic Circlet (60%)
		6329, // Recipe: Sealed Phoenix Necklace(70%)
		6331, // Recipe: Sealed Phoenix Earring(70%)
		6333, // Recipe: Sealed Phoenix Ring(70%)
		6335, // Recipe: Sealed Majestic Necklace(70%)
		6337, // Recipe: Sealed Majestic Earring(70%)
		6339, // Recipe: Sealed Majestic Ring(70%)
	};
	private final int[] _recipesArmorA100 = {
		5333, // Recipe: Sealed Dark Crystal Leather Armor(100%)
		5335, // Recipe: Sealed Tallum Leather Armor(100%)
		5337, // Recipe: Sealed Leather Armor of Nightmare(100%)
		5339, // Recipe: Sealed Majestic Leather Armor(100%)
		5341, // Recipe: Sealed Dark Crystal Leggings(100%)
		5347, // Recipe: Sealed Tallum Tunic(100%)
		5349, // Recipe: Sealed Dark Crystal Robe(100%)
		5351, // Recipe: Sealed Nightmare Robe(100%)
		5353, // Recipe: Sealed Majestic Robe(100%)
		5355, // Recipe: Sealed Tallum Stockings(100%)
		5365, // Recipe: Sealed Dark Crystal Shield(100%)
		5367, // Recipe: Sealed Shield of Nightmare(100%)
		5369, // Recipe: Sealed Dark Crystal Boots(100%)
		5371, // Recipe: Sealed Tallum Boots(100%)
		5381, // Recipe: Sealed Boots of Nightmare(100%)
		5383, // Recipe: Sealed Majestic Boots(100%)
		5393, // Recipe: Sealed Dark Crystal Gloves(100%)
		5395, // Recipe: Sealed Tallum Gloves(100%)
		5405, // Recipe: Sealed Gauntlets of Nightmare(100%)
		5407, // Recipe: Sealed Majestic Gauntlets(100%)
		5417, // Recipe: Sealed Dark Crystal Breastplate(100%)
		5419, // Recipe: Sealed Tallum Plate Armor(100%)
		5421, // Recipe: Sealed Armor of Nightmare(100%)
		5423, // Recipe: Sealed Majestic Plate Armor(100%)
		5425, // Recipe: Sealed Dark Crystal Gaiters(100%)
		5427, // Recipe: Sealed Dark Crystal Helmet(100%)
		5429, // Recipe: Sealed Tallum Helmet(100%)
		5431, // Recipe: Sealed Helm of Nightmare(100%)
		5433, // Recipe: Sealed Majestic Circlet (100%)
		6330, // Recipe: Sealed Phoenix Necklace(100%)
		6332, // Recipe: Sealed Phoenix Earring(100%)
		6334, // Recipe: Sealed Phoenix Ring(100%)
		6336, // Recipe: Sealed Majestic Necklace(100%)
		6338, // Recipe: Sealed Majestic Earring(100%)
		6340, // Recipe: Sealed Majestic Ring(100%)	
	};
	private final int[] _recipesArmorS60 = {
		6847, // Recipe: Sealed Tateossian Earring (70%)
		6849, // Recipe: Sealed Tateossian Ring(70%)
		6851, // Recipe: Sealed Tateossian Necklace(70%)
		6853, // Recipe: Sealed Imperial Crusader Breastplate (60%)
		6855, // Recipe: Sealed Imperial Crusader Gaiters (60%)
		6857, // Recipe: Sealed Imperial Crusader Gauntlets (60%)
		6859, // Recipe: Sealed Imperial Crusader Boots (60%)
		6861, // Recipe: Sealed Imperial Crusader Shield (60%)
		6863, // Recipe: Sealed Imperial Crusader Helmet (60%)
		6865, // Recipe: Sealed Draconic Leather Armor (60%)
		6867, // Recipe: Sealed Draconic Leather Gloves (60%)
		6869, // Recipe: Sealed Draconic Leather Boots (60%)
		6871, // Recipe: Sealed Draconic Leather Helmet (60%)
		6873, // Recipe: Sealed Major Arcana Robe (60%)
		6875, // Recipe: Sealed Major Arcana Gloves (60%)
		6877, // Recipe: Sealed Major Arcana Boots (60%)
		6879, // Recipe: Sealed Major Arcana Circlet (60%)
		13100, // Recipe - Sealed Arcana Sigil (60%)
	};
	private final int[] _recipesArmorS100 = {
		6848, // Recipe: Sealed Tateossian Earring (100%)
		6850, // Recipe: Sealed Tateossian Ring(100%)
		6852, // Recipe: Sealed Tateossian Necklace(100%)
		6854, // Recipe: Sealed Imperial Crusader Breastplate (100%)
		6856, // Recipe: Sealed Imperial Crusader Gaiters (100%)
		6858, // Recipe: Sealed Imperial Crusader Gauntlets (100%)
		6860, // Recipe: Sealed Imperial Crusader Boots (100%)
		6862, // Recipe: Sealed Imperial Crusader Shield (100%)
		6864, // Recipe: Sealed Imperial Crusader Helmet (100%)
		6866, // Recipe: Sealed Draconic Leather Armor (100%)
		6868, // Recipe: Sealed Draconic Leather Gloves (100%)
		6870, // Recipe: Sealed Draconic Leather Boots (100%)
		6872, // Recipe: Sealed Draconic Leather Helmet (100%)
		6874, // Recipe: Sealed Major Arcana Robe (100%)
		6876, // Recipe: Sealed Major Arcana Gloves (100%)
		6878, // Recipe: Sealed Major Arcana Boots (100%)
		6880, // Recipe: Sealed Major Arcana Circlet (100%)
		13101, // Recipe - Sealed Arcana Sigil (100%)
	};
	private final int[] _recipesWeaponA_C3C5_60 = {
		// C3
		5434, // Recipe: Dragon Slayer(60%)
		5438, // Recipe: Meteor Shower(60%)
		5440, // Recipe: Elysian(60%)
		5442, // Recipe: Soul Bow(60%)
		5444, // Recipe: Carnage Bow(60%)
		5446, // Recipe: Bloody Orchid(60%)
		5448, // Recipe: Soul Separator(60%)
		5450, // Recipe: Dragon Grinder(60%)
		5452, // Recipe: Blood Tornado(60%)
		5454, // Recipe: Orcish Halberd(60%)
		5456, // Recipe: Tallum Glaive(60%)
		5458, // Recipe: Halberd(60%)
		5460, // Recipe: Dasparion's Staff(60%)
		5462, // Recipe: Branch of The Mother Tree(60%)
		5464, // Recipe: Dark Legion's Edge(60%)
		5466, // Recipe: Sword of Miracles(60%)
		5468, // Recipe: Elemental Sword(60%)
		5470, // Recipe: Tallum Blade(60%)
		// C5
		8300, // Recipe: Infernal Master (60%)
		8314, // Recipe: Spiritual Eye (60%)
		8316, // Recipe: Flaming Dragon Skull (60%)
		8326, // Recipe: Doom Crusher (60%)
		8487, // Recipe: Destroyer Hammer (60%)
	};
	private final int[] _recipesWeaponA_C3C5_100 = {
		// C3
		5435, // Recipe: Dragon Slayer(100%)
		5439, // Recipe: Meteor Shower(100%)
		5441, // Recipe: Elysian(100%)
		5443, // Recipe: Soul Bow(100%)
		5445, // Recipe: Carnage Bow(100%)
		5447, // Recipe: Bloody Orchid(100%)
		5449, // Recipe: Soul Separator(100%)
		5451, // Recipe: Dragon Grinder(100%)
		5453, // Recipe: Blood Tornado(100%)
		5455, // Recipe: Orcish Halberd(100%)
		5457, // Recipe: Tallum Glaive(100%)
		5459, // Recipe: Halberd(100%)
		5461, // Recipe: Dasparion's Staff(100%)
		5463, // Recipe: Branch of The Mother Tree(100%)
		5465, // Recipe: Dark Legion's Edge(100%)
		5467, // Recipe: Sword of Miracles(100%)
		5469, // Recipe: Elemental Sword(100%)
		5471, // Recipe: Tallum Blade(100%)
		// C5
		8299, // Recipe: Infernal Master (100%)
		8313, // Recipe: Spiritual Eye (100%)
		8315, // Recipe: Flaming Dragon Skull (100%)
		8320, // Recipe: Destroyer Hammer (100%)
		8325, // Recipe: Doom Crusher (100%)
	};
	private final int[] _recipesWeaponA_Int_60 = {
		8690, // Recipe: Sirra's Blade (60%)
		8692, // Recipe: Sword of Ipos (60%)
		8694, // Recipe: Barakiel's Axe (60%)
		8696, // Recipe: Behemoth's Tuning Fork (60%)
		8698, // Recipe: Naga Storm (60%)
		8700, // Recipe: Tiphon's Spear (60%)
		8702, // Recipe: Shyeed's Bow (60%)
		8704, // Recipe: Sobekk's Hurricane (60%)
		8706, // Recipe: Themis' Tongue (60%)
		8708, // Recipe: Cabrio's Hand (60%)
		8710, // Recipe: Daimon Crystal (60%)
	};
	private final int[] _recipesWeaponA_Int_100 = {
		8691, // Recipe: Sirra's Blade (100%)
		8693, // Recipe: Sword of Ipos (100%)
		8695, // Recipe: Barakiel's Axe (100%)
		8697, // Recipe: Behemoth's Tuning Fork (100%)
		8699, // Recipe: Naga Storm (100%)
		8701, // Recipe: Tiphon's Spear (100%)
		8703, // Recipe: Shyeed's Bow (100%)
		8705, // Recipe: Sobekk's Hurricane (100%)
		8707, // Recipe: Themis' Tongue (100%)
		8709, // Recipe: Cabrio's Hand (100%)
		8711, // Recipe: Daimon Crystal (100%)
	};
	private final int[] _recipesWeaponS60 = {
		6881, // Recipe: Forgotten Blade (60%)
		6883, // Recipe: Basalt Battlehammer (60%)
		6885, // Recipe: Imperial Staff (60%)
		6887, // Recipe: Angel Slayer (60%)
		6889, // Recipe: Shining Bow (60%)
		6891, // Recipe: Dragon Hunter Axe (60%)
		6893, // Recipe: Saint Spear (60%)
		6895, // Recipe: Demon Splinter (60%)
		6897, // Recipe: Heavens Divider (60%)
		6899, // Recipe: Arcana Mace (60%)
		7580, // Recipe: Draconic Bow (60%)
	};
	private final int[] _recipesWeaponS100 = {
		6882, // Recipe: Forgotten Blade (100%)
		6884, // Recipe: Basalt Battlehammer (100%)
		6886, // Recipe: Imperial Staff(100%)
		6888, // Recipe: Angel Slayer (100%)
		6890, // Recipe: Shining Bow (100%)
		6892, // Recipe: Dragon Hunter Axe (100%)
		6894, // Recipe: Saint Spear (100%)
		6896, // Recipe: Demon Splinter (100%)
		6898, // Recipe: Heavens Divider (100%)
		6900, // Recipe: Arcana Mace (100%)
		7581, // Recipe: Draconic Bow (100%)
		// Special prize
		3957, // Recipe: Blessed Spiritshot S
	};
	
	/**
	 * Used to extract {@link #CHEST_MATERIALS}.
	 * @return
	 * <LI>3 low(0-2 lvl) material IDs at 50% chance
	 * <LI>2 mid(0,4 lvl) material IDs at 37,5% chance
	 * <LI>1 high(5+ lvl) material ID at 12,5% chance
	 */
	public int[] getRandomMaterials()
	{
		int chance = Rnd.get(8);
		if (chance < 4) // 1/2
		{
			return new int[] {
				Calc.getRandomFromArray(_materialsLow),
				Calc.getRandomFromArray(_materialsLow),
				Calc.getRandomFromArray(_materialsLow),
			};
		}
		else if (chance < 7) // 3/8
		{
			return new int[] {
				Calc.getRandomFromArray(_materialsMid),
				Calc.getRandomFromArray(_materialsMid),
			};
		}
		else // 1/8
			return new int[] { Calc.getRandomFromArray(_materialsHigh) };
	}
	
	/**
	 * Used to extract {@link #CHEST_PARTS}.
	 * @return
	 * <LI>A grade armor part at 58% chance
	 * <LI>A grade weapon [C3-C5] part at 25% chance
	 * <LI>A grade weapon [Int] part at 11% chance
	 * <LI>S grade armor part at 4% chance
	 * <LI>S grade weapon part at 2% chance
	 */
	public int getRandomPart()
	{
		int chance = Rnd.get(100);
		if (chance < 58)
			return Calc.getRandomFromArray(_partsArmorA);
		else if (chance < 83)
			return Calc.getRandomFromArray(_partsWeaponA_C3C5_);
		else if (chance < 94)
			return Calc.getRandomFromArray(_partsWeaponA_Int_);
		else if (chance < 98)
			return Calc.getRandomFromArray(_partsArmorS);
		else
			return Calc.getRandomFromArray(_partsWeaponS);
	}
	
	/**
	 * Used to extract {@link #CHEST_RECIPES}.<BR>
	 * General rule is to favor 60% recipes, so the players
	 * would be willing/forced to risk such craft.<BR>
	 * Also, S grade recipes should not be handed out often.
	 * @return
	 * <LI>A grade armor recipe (60%) at 35% chance
	 * <LI>A grade armor recipe (100%) at 14% chance
	 * <LI>A grade weapon [C3-C5] recipe (60%) at 19% chance
	 * <LI>A grade weapon [C3-C5] recipe (100%) at 8% chance
	 * <LI>A grade weapon [Int] recipe (60%) at 9% chance
	 * <LI>A grade weapon [Int] recipe (100%) at 5% chance
	 * <LI>S grade armor recipe (60%) at 5% chance
	 * <LI>S grade armor recipe (100%) at 2% chance
	 * <LI>S grade weapon recipe (60%) at 2% chance
	 * <LI>S grade weapon recipe (100%) at 1% chance
	 */
	public int getRandomRecipe()
	{
		int chance = Rnd.get(100);
		if (chance < 35)
			return Calc.getRandomFromArray(_recipesArmorA60);
		else if (chance < 49)
			return Calc.getRandomFromArray(_recipesArmorA100);
		else if (chance < 68)
			return Calc.getRandomFromArray(_recipesWeaponA_C3C5_60);
		else if (chance < 76)
			return Calc.getRandomFromArray(_recipesWeaponA_C3C5_100);
		else if (chance < 85)
			return Calc.getRandomFromArray(_recipesWeaponA_Int_60);
		else if (chance < 90)
			return Calc.getRandomFromArray(_recipesWeaponA_Int_100);
		else if (chance < 95)
			return Calc.getRandomFromArray(_recipesArmorS60);
		else if (chance < 97)
			return Calc.getRandomFromArray(_recipesArmorS100);
		else if (chance < 99)
			return Calc.getRandomFromArray(_recipesWeaponS60);
		else
			return Calc.getRandomFromArray(_recipesWeaponS100);
	}
	
	/**
	 * Used to determine last hit item reward in PvP.
	 * @return
	 * <LI>{@link #GATEKEEPER_TOKEN} at 2% chance
	 * <LI>{@link #CHEST_RECIPES} at 2% chance
	 * <LI>{@link #CHEST_PARTS} at 10% chance
	 * <LI>{@link #CHEST_MATERIALS} at 50% chance
	 * <LI>{@value #NOTHING} at 36% chance
	 */
	public int getLoot()
	{
		int chance = Rnd.get(50);
		if (chance < 1)
			return GATEKEEPER_TOKEN; // for raids, 1/50
		else if (chance < 2)
			return CHEST_RECIPES; // 1/50
		else if (chance < 7)
			return CHEST_PARTS; // 1/10
		else if (chance < 32)
			return CHEST_MATERIALS; // 1/2
		else
			return NOTHING;
	}
	
	public static LootedGoodsManager getInstance()
	{
		return SingletonHolder._instance;
	}
	
	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final LootedGoodsManager _instance = new LootedGoodsManager();
	}
}
