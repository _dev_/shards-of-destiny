/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.manager;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.l2jfree.util.logging.L2Logger;


import com.l2jfree.gameserver.instancemanager.InstanceManager;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.entity.Instance;

/**
 * TODO Auto-generated JavaDoc
 * 
 * @author savormix
 * @since 2010.10.18
 */
public final class PooledInstanceManager
{
	private static final L2Logger _log = L2Logger.getLogger(PooledInstanceManager.class);
	
	private final InstanceManager _im;
	private final Queue<Integer> _pool;
	
	private PooledInstanceManager()
	{
		_im = InstanceManager.getInstance();
		_pool = new ConcurrentLinkedQueue<Integer>();
	}
	
	private void addToPool(int instanceId)
	{
		final Integer id = instanceId;
		if (_pool.contains(id))
		{
			_log.warn("Multiply destroyed instance " + instanceId, new Exception());
			return;
		}
		else
			_pool.add(id);
	}
	
	private Integer getFromPool()
	{
		return _pool.poll();
	}
	
	public Instance getDynamicInstance(L2PcInstance player)
	{
		return _im.getDynamicInstance(player);
	}
	
	public void destroyInstance(int instanceId)
	{
		_im.destroyInstance(instanceId);
		if (instanceId > 0)
			addToPool(instanceId);
	}
	
	public Instance getInstance(int instanceId)
	{
		return _im.getInstance(instanceId);
	}
	
	private int reserveFreeId()
	{
		Integer id = getFromPool();
		if (id != null)
			return id;
		else
			return _im.reserveFreeId();
	}
	
	@SuppressWarnings("deprecation")
	public int createInstance()
	{
		int id = reserveFreeId();
		_im.createInstance(id);
		return id;
	}
	
	public static PooledInstanceManager getInstance()
	{
		return SingletonHolder._instance;
	}
	
	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final PooledInstanceManager _instance = new PooledInstanceManager();
	}
}
