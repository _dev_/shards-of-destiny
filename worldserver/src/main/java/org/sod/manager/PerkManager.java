/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.manager;

import java.io.File;

import javax.xml.parsers.DocumentBuilderFactory;

import javolution.util.FastMap;

import com.l2jfree.util.logging.L2Logger;

import org.sod.model.PointCollectable;
import org.sod.model.PointTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.l2jfree.Config;

/**
 * Manages perk loading and stores the needed classes.
 * @author savormix
 * @since 2010.01.21
 */
public final class PerkManager
{
	private static final L2Logger _log = L2Logger.getLogger(PerkManager.class);

	private final FastMap<Integer, PerkTemplate> _perks;

	private PerkManager()
	{
		_perks = new FastMap<Integer, PerkTemplate>().setShared(true);
		load();
	}

	public PerkTemplate getTemplate(Integer perkId)
	{
		return _perks.get(perkId);
	}

	private void load()
	{
		File xml = new File(Config.DATAPACK_ROOT, "data/stats/perks.xml");
		Document doc = null;
		try
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			factory.setIgnoringComments(true);
			doc = factory.newDocumentBuilder().parse(xml);
		}
		catch (Exception e)
		{
			_log.fatal("Cannot load perks!", e);
		}
		try
		{
			parseDocument(doc);
		}
		catch (Exception e)
		{
			_log.fatal("Cannot parse perks!", e);
		}
		_log.info("PerkManager: Loaded " + _perks.size() + " perk(s).");
	}

	private void parseDocument(Document doc) throws Exception
	{
		for (Node n = doc.getFirstChild(); n != null; n = n.getNextSibling())
			if ("list".equalsIgnoreCase(n.getNodeName()))
				for (Node e = n.getFirstChild(); e != null; e = e.getNextSibling())
					if ("perk".equalsIgnoreCase(e.getNodeName()))
					{
						PerkTemplate perk = parsePerk(e);
						_perks.put(perk.getId(), perk);
					}
	}

	public PerkTemplate parsePerk(Node n) throws Exception
	{
		Node id = n.getAttributes().getNamedItem("id");
		Node name = n.getAttributes().getNamedItem("name");
		Node buy = n.getAttributes().getNamedItem("buyable");
		Node slot = n.getAttributes().getNamedItem("slot");
		Node skill = n.getAttributes().getNamedItem("skill");
		Node upgrade = n.getAttributes().getNamedItem("upgradePoints");
		Node upSkill = n.getAttributes().getNamedItem("upgradeSkill");
		int upSkillId = -1;
		if (upSkill != null)
			upSkillId = Integer.parseInt(upSkill.getNodeValue());

		return new PerkTemplate(Integer.parseInt(id.getNodeValue()), name.getNodeValue(),
				Boolean.parseBoolean(buy.getNodeValue()), Integer.parseInt(slot.getNodeValue()),
				Integer.parseInt(skill.getNodeValue()), Integer.parseInt(upgrade.getNodeValue()),
				upSkillId);
	}

	public static class Perk extends PointCollectable
	{
		private volatile boolean _active;

		public Perk(Integer perkId, int points)
		{
			super(PerkManager.getInstance().getTemplate(perkId), points);
		}

		/** @return the perk's base data */
		@Override
		public PerkTemplate getTemplate()
		{
			return (PerkTemplate) super.getTemplate();
		}

		/**
		 * @return if this perk is equipped
		 */
		public boolean isActive()
		{
			return _active;
		}

		/**
		 * @param active is perk equipped
		 */
		public void setActive(boolean active)
		{
			if (_active != active)
			{
				_active = active;
				setChanged();
			}
		}

		@Override
		public String toString()
		{
			return getTemplate().getName() + " [Upgraded: " + isUpgraded() + ", points: " + getPoints() + "]";
		}
	}

	/**
	 * Class defining a perk template.
	 * @author savormix
	 * @since 2010.01.19
	 */
	public static class PerkTemplate implements PointTemplate
	{
		private final int			_id;
		private final String		_name;
		private final boolean		_buyable;
		private final int			_slot;
		private final int			_skillId;
		private final int			_upgradePoints;
		private final int			_upgradeSkillId;

		private PerkTemplate(int id, String name, boolean buyable, int slot, int skill, int points, int upSkill)
		{
			_id = id;
			_name = name;
			_buyable = buyable;
			// array indexing starts at 0
			_slot = (slot - 1);
			_skillId = skill;
			_upgradePoints = points;
			_upgradeSkillId = upSkill;
		}

		/**
		 * @return the buyable
		 */
		public boolean isBuyable()
		{
			return _buyable;
		}

		/**
		 * @return the id
		 */
		public int getId()
		{
			return _id;
		}

		/**
		 * @return the name
		 */
		public String getName()
		{
			return _name;
		}

		/**
		 * @return the slot
		 */
		public int getSlot()
		{
			return _slot;
		}

		/**
		 * @return the skillId
		 */
		public int getSkillId()
		{
			return _skillId;
		}

		/**
		 * @return the upgradeSkillId
		 */
		public int getUpgradeSkillId()
		{
			return _upgradeSkillId;
		}

		@Override
		public int getNeededPoints()
		{
			return _upgradePoints;
		}
	}

	public static PerkManager getInstance()
	{
		return SingletonHolder._instance;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final PerkManager _instance = new PerkManager();
	}
}
