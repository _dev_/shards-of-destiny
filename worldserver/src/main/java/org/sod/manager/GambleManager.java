/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.manager;

import java.util.ArrayList;
import java.util.List;

import com.l2jfree.gameserver.model.itemcontainer.PcInventory;
import com.l2jfree.lang.L2Math;
import com.l2jfree.util.Rnd;

import javolution.util.FastMap;

/**
 * Manages gambling categories and rewards.
 * @author savormix
 * @since 2010.05.12
 */
public final class GambleManager
{
	private static final int MAX_CHANCE = 1000000;
	public static final int COIN_OF_LUCK = 90006;

	private final FastMap<Integer, List<GambleReward>> _options;

	private GambleManager()
	{
		_options = new FastMap<Integer, List<GambleReward>>().setShared(true);
		fillOptions();
	}

	private final void fillOptions()
	{
		ArrayList<GambleReward> list = new ArrayList<GambleReward>();
		_options.put(10, list);
		//   10 ADENA =====
		list.add(new GambleReward(PcInventory.ADENA_ID, 20, MAX_CHANCE / 2));
		list.add(new GambleReward(13124, 1, MAX_CHANCE / 50)); // color name 1-pack
		list.add(new GambleReward(13279, 1, MAX_CHANCE / 80)); // feather 1-pack
		list.add(new GambleReward(2125, 1, MAX_CHANCE / 20)); // phoenix feather piece
		list.add(new GambleReward(20616, 1, MAX_CHANCE / 200)); // headphone (love theme)
		list.add(new GambleReward(20617, 1, MAX_CHANCE / 200)); // headphone (hero theme)
		list.add(new GambleReward(20618, 1, MAX_CHANCE / 200)); // headphone (mark theme)
		list.add(new GambleReward(COIN_OF_LUCK, 1, MAX_CHANCE / 1000));
		// ===============
		list.trimToSize();
		list = new ArrayList<GambleReward>();
		_options.put(100, list);
		//  100 ADENA =====
		list.add(new GambleReward(PcInventory.ADENA_ID, 200, MAX_CHANCE / 3));
		list.add(new GambleReward(2125, 1, MAX_CHANCE / 10)); // phoenix feather piece
		list.add(new GambleReward(13098, 1, MAX_CHANCE / 50)); // color name 3-pack
		list.add(new GambleReward(13082, 1, MAX_CHANCE / 80)); // feather 3-pack
		list.add(new GambleReward(5595, 1, MAX_CHANCE / 100)); // 100k SP scroll
		list.add(new GambleReward(14678, 1, MAX_CHANCE / 200)); // saerom B
		list.add(new GambleReward(COIN_OF_LUCK, 1, MAX_CHANCE / 800));
		// ===============
		list.trimToSize();
		list = new ArrayList<GambleReward>();
		_options.put(500, list);
		//  500 ADENA =====
		list.add(new GambleReward(PcInventory.ADENA_ID, 1000, MAX_CHANCE / 4));
		list.add(new GambleReward(2125, 3, MAX_CHANCE / 10)); // phoenix feather piece
		list.add(new GambleReward(13098, 2, MAX_CHANCE / 50)); // color name 3-pack
		list.add(new GambleReward(13082, 2, MAX_CHANCE / 80)); // feather 3-pack
		list.add(new GambleReward(14678, 1, MAX_CHANCE / 150)); // saerom B
		list.add(new GambleReward(2363, 1, MAX_CHANCE / 200)); // phoenix feather recipe
		list.add(new GambleReward(9898, 1, MAX_CHANCE / 200)); // 500k SP scroll
		list.add(new GambleReward(14679, 1, MAX_CHANCE / 300)); // saerom A
		list.add(new GambleReward(20207, 1, MAX_CHANCE / 230)); // Zaken's Earring (3 day)
		list.add(new GambleReward(20204, 1, MAX_CHANCE / 300)); // QA Ring (3 day)
		list.add(new GambleReward(COIN_OF_LUCK, 1, MAX_CHANCE / 700));
		// ===============
		list.trimToSize();
		list = new ArrayList<GambleReward>();
		_options.put(1000, list);
		// 1000 ADENA =====
		list.add(new GambleReward(PcInventory.ADENA_ID, 2000, MAX_CHANCE / 5));
		list.add(new GambleReward(2125, 10, MAX_CHANCE / 20)); // phoenix feather piece
		list.add(new GambleReward(13098, 3, MAX_CHANCE / 50)); // color name 3-pack
		list.add(new GambleReward(13082, 3, MAX_CHANCE / 60)); // feather 3-pack
		list.add(new GambleReward(2363, 1, MAX_CHANCE / 100)); // phoenix feather recipe
		list.add(new GambleReward(9898, 2, MAX_CHANCE / 100)); // 500k SP scroll
		list.add(new GambleReward(20207, 1, MAX_CHANCE / 230)); // Zaken's Earring (3 day)
		list.add(new GambleReward(20204, 1, MAX_CHANCE / 300)); // QA Ring (3 day)
		list.add(new GambleReward(14678, 1, MAX_CHANCE / 100)); // saerom B
		list.add(new GambleReward(14679, 1, MAX_CHANCE / 250)); // saerom A
		list.add(new GambleReward(14679, 1, MAX_CHANCE / 400)); // saerom S
		list.add(new GambleReward(COIN_OF_LUCK, 1, MAX_CHANCE / 500));
		// ===============
		list.trimToSize();
		list = new ArrayList<GambleReward>();
		_options.put(COIN_OF_LUCK, list);
		// Coin of Luck =====
		list.add(new GambleReward(2363, 1, MAX_CHANCE / 3)); // phoenix feather recipe
		list.add(new GambleReward(PcInventory.ADENA_ID, 5000, MAX_CHANCE / 10));
		list.add(new GambleReward(14678, 2, MAX_CHANCE / 10)); // saerom B
		list.add(new GambleReward(10315, 1, MAX_CHANCE / 50)); // purple viking circlet
		list.add(new GambleReward(10321, 1, MAX_CHANCE / 50)); // golden viking circlet
		list.add(new GambleReward(14679, 1, MAX_CHANCE / 50)); // saerom A
		list.add(new GambleReward(14679, 1, MAX_CHANCE / 100)); // saerom S
		list.add(new GambleReward(PcInventory.ADENA_ID, 30000, MAX_CHANCE / 100));
		list.add(new GambleReward(14280, 1, MAX_CHANCE / 100)); // weapon enhance B
		list.add(new GambleReward(14281, 1, MAX_CHANCE / 100)); // weapon enhance A
		list.add(new GambleReward(14285, 1, MAX_CHANCE / 100)); // armor enhance B
		list.add(new GambleReward(14286, 1, MAX_CHANCE / 100)); // armor enhance A
		list.add(new GambleReward(14248, 1, MAX_CHANCE / 175)); // HQ Hunting helper 1-pack
		// 100% enchant scrolls
		list.add(new GambleReward(22018, 1, MAX_CHANCE / 2500)); // divine enchant weapon B
		list.add(new GambleReward(22019, 1, MAX_CHANCE / 2500)); // divine enchant weapon A
		list.add(new GambleReward(22020, 1, MAX_CHANCE / 2500)); // divine enchant weapon B
		list.add(new GambleReward(22021, 1, MAX_CHANCE / 2500)); // divine enchant armor A
		// S grade masterwork
		list.add(new GambleReward(11483, 1, MAX_CHANCE / 10000)); // mw draconic gloves
		list.add(new GambleReward(11484, 1, MAX_CHANCE / 10000)); // mw draconic boots
		list.add(new GambleReward(11485, 1, MAX_CHANCE / 10000)); // mw draconic armor
		list.add(new GambleReward(11486, 1, MAX_CHANCE / 10000)); // mw draconic helmet
		list.add(new GambleReward(11487, 1, MAX_CHANCE / 10000)); // mw arcana gloves
		list.add(new GambleReward(11488, 1, MAX_CHANCE / 10000)); // mw arcana boots
		list.add(new GambleReward(11489, 1, MAX_CHANCE / 10000)); // mw arcana armor
		list.add(new GambleReward(11490, 1, MAX_CHANCE / 10000)); // mw arcana circlet
		list.add(new GambleReward(11505, 1, MAX_CHANCE / 10000)); // mw imperial gaiters
		list.add(new GambleReward(11506, 1, MAX_CHANCE / 10000)); // mw imperial gloves
		list.add(new GambleReward(11507, 1, MAX_CHANCE / 10000)); // mw imperial boots
		list.add(new GambleReward(11508, 1, MAX_CHANCE / 10000)); // mw imperial shield
		list.add(new GambleReward(11509, 1, MAX_CHANCE / 10000)); // mw imperial helmet
		list.add(new GambleReward(11510, 1, MAX_CHANCE / 10000)); // mw imperial breastplate
		// ===============
		list.trimToSize();
	}

	public GambleReward getGambleReward(int bet)
	{
		List<GambleReward> list = _options.get(bet);
		if (list != null)
			for (GambleReward gr : list)
				if (gr.getChance() > Rnd.get(MAX_CHANCE))
					return gr;
		return null;
	}

	public static class GambleReward
	{
		private final int _itemId;
		private final long _count;
		private final int _chance;

		private GambleReward(int itemId, long count, int chance)
		{
			_itemId = itemId;
			_count = count;
			_chance = L2Math.limit(1, chance, MAX_CHANCE);
		}

		/**
		 * @return the itemId
		 */
		public int getItemId()
		{
			return _itemId;
		}

		/**
		 * @return the count
		 */
		public long getCount()
		{
			return _count;
		}

		/**
		 * 1000000 <=> 100%
		 * @return the chance
		 */
		public int getChance()
		{
			return _chance;
		}
	}

	public static GambleManager getInstance()
	{
		return SingletonHolder._instance;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final GambleManager _instance = new GambleManager();
	}
}
