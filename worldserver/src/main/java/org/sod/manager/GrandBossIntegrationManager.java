package org.sod.manager;

import java.util.Locale;

import javolution.util.FastMap;

import org.sod.Skills;
import org.sod.i18n.Internationalization;
import org.sod.model.L2PlayerData;

import com.l2jfree.gameserver.datatables.SkillTable;
import com.l2jfree.gameserver.instancemanager.BossSpawnManager;
import com.l2jfree.gameserver.instancemanager.grandbosses.BossLair;
import com.l2jfree.gameserver.model.L2Party;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.L2World;
import com.l2jfree.gameserver.model.Location;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.actor.instance.L2RaidTeleporterInstance;
import com.l2jfree.gameserver.network.client.packets.sendable.NpcHtmlMessage;
import com.l2jfree.lang.L2TextBuilder;
import com.l2jfree.util.logging.L2Logger;

public final class GrandBossIntegrationManager
{
	private static final L2Logger _log = L2Logger.getLogger(GrandBossIntegrationManager.class);
	private static final String[] PARTS =
	{
			"", "<tr>", // 1
			"</tr>", // 2
			"<td>", // 3
			"</td>", // 4
			"<td width=\"180\">", // 5
			"<button value=\"Go\" action=\"bypass -h raidman_tele ", // 6
			"\" width=40 height=20 back=\"L2UI_CT1.Gauge_DF_SSQ_Dawn_bg\" fore=\"L2UI_CT1.Gauge_DF_SSQ_Dawn\">", // 7
			"\" width=40 height=20 back=\"L2UI_CT1.Gauge_DF_SSQ_Dusk_bg\" fore=\"L2UI_CT1.Gauge_DF_SSQ_Dusk\">", // 8
			"<font color=\"red\">Dead</font>", // 9
			"", // 10
			"<font color=\"red\">Sealed</font>", // 11
	};
	private static final String[] ASK =
	{
			"<html><title>Raid Manager</title><body><center><img src=\"L2UI_CH3.herotower_deco\" "
					+ "width=256 height=32><br>Hey listen, <font color=\"LEVEL\">you can't go</font> there. "
					+ "The current raiding party paid me a good sum of adena to get rid of all useless competitors, "
					+ "if you get what I mean.<br1>However, I can arrange my assistant (that good-for-nothing boy "
					+ "is gone again!) to <font color=\"LEVEL\">send a message to the current raiding party leader</font>"
					+ ". If he approves your presence in the battlefield, I'll send you there.<br1>Just remember, "
					+ "the anomalous beast will not tolerate such interference, so you are likely to be "
					+ "<font color=\"LEVEL\">disabled and attacked at once</font> when you get there.<br>"
					+ "<a action=\"bypass -h raidman_ask ",
			"\">Send a message.</a><br><img src=\"L2UI_CH3.herotower_deco\" width=256 height=32>"
					+ "</center></body></html>"
	};
	
	private final FastMap<Integer, GrandInfo> _mgb;
	
	private GrandBossIntegrationManager()
	{
		_mgb = new FastMap<Integer, GrandInfo>().setShared(true);
		_log.info("GrandBossIntegrationManager: initialized.");
	}
	
	public void register(BossLair manager, String name, int grandId)
	{
		_mgb.put(grandId, new GrandInfo(manager, name));
	}
	
	public void insertManagedBosses(L2TextBuilder htm)
	{
		for (FastMap.Entry<Integer, GrandInfo> entry = _mgb.head(), end = _mgb.tail(); (entry = entry.getNext()) != end;)
		{
			int mgb = entry.getKey();
			GrandInfo gi = entry.getValue();
			BossLair bl = gi.getManager();
			htm.append(PARTS[1]);
			htm.append(PARTS[5]);
			htm.append(gi.getName());
			htm.append(PARTS[4]);
			htm.append(PARTS[3]);
			htm.append(L2RaidTeleporterInstance.getNeededItemCount(mgb, null));
			htm.append(PARTS[4]);
			for (int i = 1; i < 3; i++)
			{
				htm.append(PARTS[3]);
				switch (bl.getState())
				{
				case DEAD:
					htm.append(PARTS[9]);
					break;
				case INTERVAL:
					htm.append(PARTS[11]);
					break;
				case ALIVE:
				case NOTSPAWN:
					htm.append(PARTS[6]);
					htm.append(mgb);
					htm.append(' ');
					htm.append(i);
					if (bl.isEnableEnterToLair())
					{
						L2PcInstance leader = BossLair.getLeader(mgb);
						L2Npc npc = gi.getSpawn();
						if (npc != null)
						{
							double percent = npc.getCurrentHp() * 100 / npc.getMaxHp();
							if (percent > 90 || leader == null)
								htm.append(PARTS[7]);
							else
								htm.append(PARTS[8]);
						}
						else if (bl.getTeleport() != null)
							htm.append(PARTS[7]);
						else
							htm.append(PARTS[8]);
					}
					else
						htm.append(PARTS[8]);
					break;
				}
				htm.append(PARTS[4]);
			}
			htm.append(PARTS[2]);
		}
	}
	
	public boolean requestEntry(L2PcInstance player, L2Party party, Integer raidId)
	{
		GrandInfo gi = _mgb.get(raidId);
		if (gi == null)
			return false;
		L2PcInstance leader = BossLair.getLeader(raidId);
		if (leader == null)
		{
			player.sendMessage("There is no leader to request.");
			return true;
		}
		L2PlayerData dat = leader.getSoDPlayer();
		L2Npc npc = gi.getSpawn();
		if (npc != null)
		{
			double percent = npc.getCurrentHp() * 100 / npc.getMaxHp();
			if (percent < 5)
			{
				player.sendMessage("Currently one or more parties are finishing off the raid boss, so you may not enter.");
			}
			else if (percent < 90)
			{
				if (!dat.isRaidRequested(player))
					dat.addRaidRequest(player, party, raidId);
				else
				{
					L2TextBuilder tb = new L2TextBuilder();
					tb.append("<html><title>Raid Manager</title><body><center><img src=\"L2UI_CH3.herotower_deco\" ");
					tb.append("width=256 height=32><br>Do I look retarded to you? ");
					tb.append("<font color=\"LEVEL\">The raid leader did not answer yet!</font>");
					tb.append("<br><img src=\"L2UI_CH3.herotower_deco\" width=256 height=32></center></body></html>");
					player.sendPacket(new NpcHtmlMessage(tb.moveToString()));
				}
			}
			else
				player.sendMessage("You may attempt entrance without any requests.");
		}
		else
		{
			if (!dat.isRaidRequested(player))
				dat.addRaidRequest(player, party, raidId);
		}
		return true;
	}
	
	public void teleByRequest(L2PcInstance insider, Integer raid, int partyLeader)
	{
		GrandInfo gi = _mgb.get(raid);
		if (gi == null || !gi.getManager().isEnableEnterToLair())
			return;
		L2PcInstance leader = BossSpawnManager.getLeader(raid);
		if (insider != leader)
			return;
		L2Npc npc = gi.getSpawn();
		Location loc;
		if (npc != null)
		{
			double percent = npc.getCurrentHp() * 100 / npc.getMaxHp();
			if (percent < 5)
				loc = null;
			else
				loc = npc.getLoc();
		}
		else
			loc = gi.getManager().getTeleport();
		
		if (loc == null)
			return;
		
		L2PcInstance requestor = L2World.getInstance().findPlayer(partyLeader);
		if (requestor == null)
			return;
		L2Party party = requestor.getParty();
		if (party != null && !party.isLeader(requestor))
			return;
		if (!requestor.destroyItemByItemId("Raid Teleport", LootedGoodsManager.GATEKEEPER_TOKEN,
				L2RaidTeleporterInstance.getNeededItemCount(raid, party), insider, true))
			return;
		
		if (npc == null)
			gi.getManager().prepareBoss();
		
		int left = gi.getManager().getTimeToAppearance();
		Internationalization i18n = Internationalization.getInstance();
		
		L2Skill sk = SkillTable.getInstance().getInfo(Skills.CURSE_OF_ANOMALY, 1);
		if (party != null)
		{
			BossSpawnManager.addRaider(raid, party);
			for (L2PcInstance member : party.getPartyMembers())
			{
				member.teleToLocation(loc, true);
				if (sk != null)
					sk.getEffects(member, member);
				member.setInstanceId(gi.getManager().getInstanceId());
				
				if (left > 0)
				{
					Locale l = member.getSoDPlayer().getSettings().getLocale();
					String time, unit;
					if (left < 60)
					{
						time = String.valueOf(left);
						unit = i18n.get(l, "TIME_SECOND");
					}
					else
					{
						time = String.valueOf(left / 60);
						unit = i18n.get(l, "TIME_MINUTE");
					}
					member.getSoDPlayer().sendLocalizedScreenMessage(2, false, false, Math.min(15, left),
							"NPC_RAID_MANAGER_TIME_TO_APPEAR", "%boss%", gi.getName(), "%time%", time, "%unit%", unit);
					member.getSoDPlayer().sendLocalizedMessage("NPC_RAID_MANAGER_TIME_TO_APPEAR", "%boss%",
							gi.getName(), "%time%", time, "%unit%", unit);
				}
			}
		}
		else
		{
			requestor.teleToLocation(loc, true);
			if (sk != null)
				sk.getEffects(requestor, requestor);
			requestor.setInstanceId(gi.getManager().getInstanceId());
			
			if (left > 0)
			{
				Locale l = requestor.getSoDPlayer().getSettings().getLocale();
				String time, unit;
				if (left < 60)
				{
					time = String.valueOf(left);
					unit = i18n.get(l, "TIME_SECOND");
				}
				else
				{
					time = String.valueOf(left / 60);
					unit = i18n.get(l, "TIME_MINUTE");
				}
				requestor.getSoDPlayer().sendLocalizedScreenMessage(2, false, false, Math.min(15, left),
						"NPC_RAID_MANAGER_TIME_TO_APPEAR", "%boss%", gi.getName(), "%time%", time, "%unit%", unit);
				requestor.getSoDPlayer().sendLocalizedMessage("NPC_RAID_MANAGER_TIME_TO_APPEAR", "%boss%",
						gi.getName(), "%time%", time, "%unit%", unit);
			}
		}
	}
	
	public void tele(L2PcInstance player, L2Party party, Integer raid)
	{
		GrandInfo gi = _mgb.get(raid);
		if (gi == null || !gi.getManager().isEnableEnterToLair())
			return;
		
		L2Npc transObj = player.getLastFolkNPC();
		L2PcInstance leader = BossLair.getLeader(raid);
		L2Npc npc = gi.getSpawn();
		Location loc;
		if (npc != null)
		{
			double percent = npc.getCurrentHp() * 100 / npc.getMaxHp();
			if (percent < 5 && leader != null)
			{
				player.sendMessage("Currently a party is finishing off the raid boss, so you may not enter.");
				return;
			}
			else if (percent < 90 && leader != null)
			{
				NpcHtmlMessage msg = new NpcHtmlMessage(transObj);
				L2TextBuilder sb = new L2TextBuilder(ASK[0]);
				sb.append(raid);
				sb.append(ASK[1]);
				msg.setMessage(sb.moveToString());
				player.sendPacket(msg);
				return;
			}
			loc = npc.getLoc();
		}
		else
			loc = gi.getManager().getTeleport();
		if (loc == null)
			return;
		
		if (!player.destroyItemByItemId("Raid Teleport", LootedGoodsManager.GATEKEEPER_TOKEN,
				L2RaidTeleporterInstance.getNeededItemCount(raid, party), transObj, true))
			return;
		
		if (npc == null)
			gi.getManager().prepareBoss();
		
		int left = gi.getManager().getTimeToAppearance();
		Internationalization i18n = Internationalization.getInstance();
		
		int x = loc.getX(), y = loc.getY(), z = loc.getZ();
		if (party != null)
		{
			BossSpawnManager.addRaider(raid, party);
			for (L2PcInstance member : party.getPartyMembers())
			{
				member.teleToLocation(x, y, z, true);
				member.setInstanceId(BossSpawnManager.BOSS_INSTANCE_OFFSET + raid);
				
				if (left > 0)
				{
					Locale l = member.getSoDPlayer().getSettings().getLocale();
					String time, unit;
					if (left < 60)
					{
						time = String.valueOf(left);
						unit = i18n.get(l, "TIME_SECOND");
					}
					else
					{
						time = String.valueOf(left / 60);
						unit = i18n.get(l, "TIME_MINUTE");
					}
					member.getSoDPlayer().sendLocalizedScreenMessage(2, false, false, Math.min(15, left),
							"NPC_RAID_MANAGER_TIME_TO_APPEAR", "%boss%", gi.getName(), "%time%", time, "%unit%", unit);
					member.getSoDPlayer().sendLocalizedMessage("NPC_RAID_MANAGER_TIME_TO_APPEAR", "%boss%",
							gi.getName(), "%time%", time, "%unit%", unit);
				}
			}
		}
		else
		{
			player.teleToLocation(x, y, z, true);
			player.setInstanceId(BossSpawnManager.BOSS_INSTANCE_OFFSET + raid);
			
			if (left > 0)
			{
				Locale l = player.getSoDPlayer().getSettings().getLocale();
				String time, unit;
				if (left < 60)
				{
					time = String.valueOf(left);
					unit = i18n.get(l, "TIME_SECOND");
				}
				else
				{
					time = String.valueOf(left / 60);
					unit = i18n.get(l, "TIME_MINUTE");
				}
				player.getSoDPlayer().sendLocalizedScreenMessage(2, false, false, Math.min(15, left),
						"NPC_RAID_MANAGER_TIME_TO_APPEAR", "%boss%", gi.getName(), "%time%", time, "%unit%", unit);
				player.getSoDPlayer().sendLocalizedMessage("NPC_RAID_MANAGER_TIME_TO_APPEAR", "%boss%", gi.getName(),
						"%time%", time, "%unit%", unit);
			}
		}
	}
	
	public static class GrandInfo
	{
		private final BossLair _manager;
		private final String _name;
		
		public GrandInfo(BossLair manager, String name)
		{
			_manager = manager;
			_name = name;
		}
		
		public L2Npc getSpawn()
		{
			return _manager.getBossSpawn();
		}
		
		public BossLair getManager()
		{
			return _manager;
		}
		
		public String getName()
		{
			return _name;
		}
	}
	
	public static GrandBossIntegrationManager getInstance()
	{
		return SingletonHolder._instance;
	}
	
	@SuppressWarnings("synthetic-access")
	private static final class SingletonHolder
	{
		private static final GrandBossIntegrationManager _instance = new GrandBossIntegrationManager();
	}
}
