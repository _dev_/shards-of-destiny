/*
 * MISSING LICENSING INFO
 */
package org.sod.manager.conquest;

import org.sod.model.L2PlayerData;

import com.l2jfree.Config;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.restriction.global.AbstractRestriction;
import com.l2jfree.gameserver.model.restriction.global.RestrictionPriority;
import com.l2jfree.gameserver.util.Util;

/**
 * @author savormix
 * 
 */
public class DualismRestriction extends AbstractRestriction
{
	private DualismRestriction()
	{
	}
	
	@Override
	@RestrictionPriority(RestrictionPriority.DEFAULT_PRIORITY - 0.1)
	public int getTitleColor(L2PcInstance activeChar)
	{
		if (!Config.CONQUEST_DUALISM || !activeChar.isGM())
			return -1;
		
		L2PlayerData dat = activeChar.getSoDPlayer();
		if (dat == null)
			return -1;
		
		int faction = dat.getDualismFaction();
		if (faction == ConquestFactionManager.NONE)
			return -1;
		
		return Util.reverseRGBChanels(ConquestFactionManager.getInstance().getFactionTemplate(faction).getColor());
	}
	
	@Override
	@RestrictionPriority(RestrictionPriority.DEFAULT_PRIORITY - 0.1)
	public int getNameColor(L2PcInstance activeChar)
	{
		if (!Config.CONQUEST_DUALISM || activeChar.isGM())
			return -1;
		
		L2PlayerData dat = activeChar.getSoDPlayer();
		if (dat == null)
			return -1;
		
		int faction = dat.getDualismFaction();
		if (faction == ConquestFactionManager.NONE)
			return -1;
		
		return Util.reverseRGBChanels(ConquestFactionManager.getInstance().getFactionTemplate(faction).getColor());
	}
	
	@Override
	@RestrictionPriority(RestrictionPriority.DEFAULT_PRIORITY - 0.1)
	public void playerLoggedIn(L2PcInstance activeChar)
	{
		if (!Config.CONQUEST_DUALISM)
			return;
		
		ConquestFactionManager.getInstance().showChooseSide(activeChar);
	}
	
	public static DualismRestriction getInstance()
	{
		return SingletonHolder._instance;
	}
	
	private static final class SingletonHolder
	{
		private static final DualismRestriction _instance = new DualismRestriction();
	}
}
