/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.manager.conquest;

import java.util.Locale;
import java.util.StringTokenizer;

import javolution.util.FastMap;
import javolution.util.FastSet;

import org.sod.L2Htm;
import org.sod.Skills;
import org.sod.i18n.Internationalization;
import org.sod.manager.EventFenceManager;
import org.sod.manager.PooledInstanceManager;
import org.sod.manager.RespawnManager;
import org.sod.manager.RespawnManager.BaseInfo;
import org.sod.model.L2PlayerData;
import org.sod.model.conquest.ConquestFactionMember;
import org.sod.model.conquest.ConquestFactionPlayable;
import org.sod.tutorial.TutorialManager;

import com.l2jfree.Config;
import com.l2jfree.gameserver.GameServer;
import com.l2jfree.gameserver.GameServer.StartupHook;
import com.l2jfree.gameserver.ThreadPoolManager;
import com.l2jfree.gameserver.model.Location;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.SystemMessageId;
import com.l2jfree.gameserver.network.client.packets.L2ServerPacket;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.Chat;
import com.l2jfree.gameserver.network.client.packets.sendable.SystemMessagePacket.SystemMessage;
import com.l2jfree.gameserver.network.client.packets.sendable.TutorialCloseHtmlPacket.HideTutorialHtml;
import com.l2jfree.gameserver.network.client.packets.sendable.TutorialShowHtml.ShowTutorialHtml;
import com.l2jfree.gameserver.util.Broadcast;
import com.l2jfree.lang.L2TextBuilder;
import com.l2jfree.util.Rnd;
import com.l2jfree.util.logging.L2Logger;

/**
 * Manages conquest factions.
 * 
 * @author savormix
 * @since 2009.12.07
 */
public final class ConquestFactionManager
{
	private static final L2Logger _log = L2Logger.getLogger(ConquestFactionManager.class);
	
	public static final int NONE = 0;
	public static final int ANCIENTS = 1;
	public static final int DIVINES = 2;
	
	private final FastMap<Integer, FactionTemplate> _factions;
	// shared FastMap in this specific javolution version is what we need
	private final FastMap<Integer, L2PlayerData> _inactive;
	private final FastSet<L2Npc> _headquarters;
	
	private ConquestFactionManager()
	{
		_factions = new FastMap<Integer, FactionTemplate>().setShared(true);
		_factions.put(ANCIENTS,
				new FactionTemplate(ANCIENTS, Config.CONQUEST_COLOR_ANCIENTS, "CONQUEST_ANCIENTS", null));
		_factions.put(DIVINES, new FactionTemplate(DIVINES, Config.CONQUEST_COLOR_DIVINES, "CONQUEST_DIVINES", null));
		_inactive = new FastMap<Integer, L2PlayerData>().setShared(true);
		_headquarters = FastSet.newInstance();
		
		ConquestRestriction.getInstance().activate();
		DualismRestriction.getInstance().activate();
		GameServer.addStartupHook(new StartupHook()
		{
			@Override
			public void onStartup()
			{
				EventFenceManager efm = EventFenceManager.getInstance();
				for (int i = ANCIENTS; i <= DIVINES; i++)
				{
					FactionTemplate ft = getFactionTemplate(i);
					efm.spawnFence(ft.getFenceId(), ft.getInstanceId());
				}
			}
		});
		
		ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(new Inactivator(), Config.CONQUEST_INACTIVATOR_TICK,
				Config.CONQUEST_INACTIVATOR_TICK);
		_log.info("ConquestFactionManager: initialized.");
	}
	
	public FactionTemplate getFactionTemplate(int id)
	{
		return _factions.get(id);
	}
	
	public FactionTemplate getFactionTemplate(Object obj)
	{
		return getFactionTemplate(getFaction(obj));
	}
	
	/**
	 * Checks if player is in one of Conquest's factions.
	 * 
	 * @param cfp
	 *            A player
	 * @return whether player is playing Conquest
	 */
	public boolean isPlaying(ConquestFactionPlayable... cfp)
	{
		for (ConquestFactionPlayable player : cfp)
			if (getFaction(player) == NONE)
				return false;
		return true;
	}
	
	/**
	 * Remove a player from Conquest. This method does not
	 * move player anywhere.
	 * 
	 * @param dat
	 *            A player
	 * @return whether player was in conquest
	 */
	public boolean removeMember(L2PlayerData dat)
	{
		L2PcInstance player = dat.getActingPlayer();
		FactionTemplate ft = getFactionTemplate(dat);
		if (ft != null)
		{
			player.leaveParty();
			ft.getMembers().remove(player);
			if (_inactive.remove(dat.getObjectId()) == null)
				ft.getActiveMembers().remove(player);
			dat.setConquestFaction(NONE);
			return true;
		}
		return false;
	}
	
	private void addMember(L2PlayerData dat)
	{
		FactionTemplate ft = getFactionTemplate(dat);
		if (ft != null)
		{
			L2PcInstance player = dat.getActingPlayer();
			player.leaveParty();
			ft.getMembers().add(player);
			ft.getActiveMembers().add(player);
			Skills.addCustomSkills(player);
		}
	}
	
	/**
	 * Adds a player to Conquest. This method does not
	 * move player anywhere.
	 * 
	 * @param dat
	 *            A player
	 * @param prefFaction
	 *            Player's preferred faction
	 */
	public void addMember(L2PlayerData dat, int prefFaction)
	{
		if (dat == null)
			return;
		
		int myFaction = dat.getConquestFaction();
		if (myFaction != NONE)
			return; // already in faction
			
		int trgFaction = getScabFaction();
		if (trgFaction == NONE)
			trgFaction = prefFaction;
		if (trgFaction == NONE)
			trgFaction = Rnd.get(DIVINES) + 1;
		dat.setConquestFaction(trgFaction);
		addMember(dat);
		dat.updateAppearance();
		dat.sendLocalizedScreenMessage(2, true, true, 4000, "CONQUEST_JOINED_FACTION", "%factionName%",
				getFactionTemplate(trgFaction).getName(dat.getSettings().getLocale()));
		RespawnManager.getInstance().moveToSpawn(dat.getActingPlayer());
	}
	
	/**
	 * Scabs a partyless player into a faction that has less
	 * active people online.
	 * 
	 * @param dat
	 *            player data
	 */
	public void scabMember(L2PlayerData dat)
	{
		int myFaction = dat.getConquestFaction();
		if (myFaction == NONE)
			return; // not in faction, no scabbing
			
		int trgFaction = getScabFaction();
		if (trgFaction == NONE || trgFaction == myFaction)
			return; // player cannot contribute to faction balancing
			
		L2PcInstance player = dat.getActingPlayer();
		if (player.isInParty() || player.isGM())
			return; // player is bound to current faction
			
		removeMember(dat);
		dat.setConquestFaction(trgFaction);
		addMember(dat);
		dat.updateAppearance();
		dat.sendLocalizedScreenMessage(2, true, true, 4000, "CONQUEST_BALANCED_FACTION", "%factionName%",
				getFactionTemplate(trgFaction).getName(dat.getSettings().getLocale()));
	}
	
	public void broadcastToMembers(Chat channel, String name, String key, String... repVals)
	{
		for (FastMap.Entry<Integer, FactionTemplate> e = _factions.head(), end = _factions.tail(); (e = e.getNext()) != end;)
			e.getValue().broadcast(channel, name, key, repVals);
	}
	
	/**
	 * Send a packet to all faction members.
	 * 
	 * @param gsp
	 *            A packet
	 */
	public void broadcastToMembers(L2ServerPacket gsp)
	{
		for (FastMap.Entry<Integer, FactionTemplate> e = _factions.head(), end = _factions.tail(); (e = e.getNext()) != end;)
			e.getValue().broadcast(gsp);
	}
	
	public final void broadcastToMembers(SystemMessageId sm)
	{
		broadcastToMembers(sm.getSystemMessage());
	}
	
	public final void broadcastToMembers(String message)
	{
		broadcastToMembers(SystemMessage.valueOf(message));
	}
	
	/**
	 * Adds a flag to proximity checks.
	 * 
	 * @param flag
	 *            Clan HQ NPC
	 */
	public void addHQ(L2Npc flag)
	{
		if (flag != null)
			_headquarters.add(flag);
	}
	
	/**
	 * Removes a flag from proximity checks.
	 * 
	 * @param flag
	 *            Clan HQ NPC
	 */
	public void remHQ(L2Npc flag)
	{
		_headquarters.remove(flag);
	}
	
	/**
	 * @param obj
	 *            A player trying to build clan headquarters
	 * @return there are no headquarters in range 1500
	 */
	public boolean checkProximityToHq(L2PcInstance obj)
	{
		if (obj == null)
			return false;
		
		for (FastSet.Record r = _headquarters.head(), end = _headquarters.tail(); (r = r.getNext()) != end;)
		{
			if (obj.isInsideRadius(_headquarters.valueOf(r), 1500, false, false))
				return false;
		}
		return true;
	}
	
	/**
	 * Returns ID of the faction that has less people online.<br>
	 * If both factions are evenly matched, returns {@link #NONE}.
	 * 
	 * @return faction ID
	 */
	public final int getScabFaction()
	{
		FactionTemplate ft1 = getFactionTemplate(ANCIENTS);
		FactionTemplate ft2 = getFactionTemplate(DIVINES);
		int c1 = ft1.getActiveMembers().size(), c2 = ft2.getActiveMembers().size();
		if ((c1 == 0 || c2 == 0) && Math.abs(c1 - c2) == 1)
			return NONE; // do not scab the only player
		if (c1 > c2 * Config.CONQUEST_MAX_VARIATION)
			return ft2.getId();
		else if (c2 > c1 * Config.CONQUEST_MAX_VARIATION)
			return ft1.getId();
		else
			return NONE;
	}
	
	/**
	 * Tests if two objects share the same conquest faction.
	 * 
	 * @param obj1
	 *            An object
	 * @param obj2
	 *            An object
	 * @return
	 *         <LI>false, if either object does not belong to a faction <LI>false, if objects belong to different
	 *         factions <LI>true, if objects belong to the same faction
	 */
	public boolean isSameFaction(Object obj1, Object obj2)
	{
		int f1 = getFaction(obj1);
		int f2 = getFaction(obj2);
		if (f1 == NONE || f2 == NONE)
			return false;
		return (f1 == f2);
	}
	
	/**
	 * Fail-safe method to know the faction of an object.<BR>
	 * If faction is impossible to determine, {@link #NULL} is returned.
	 * 
	 * @param obj
	 *            An object
	 * @return object's faction
	 */
	public int getFaction(Object obj)
	{
		if (obj instanceof ConquestFactionMember)
			return ((ConquestFactionMember) obj).getConquestFaction();
		else
			return NONE;
	}
	
	public void showChooseSide(L2PcInstance player)
	{
		if (TutorialManager.getInstance().isInTutorial(player))
			return;
		
		L2PlayerData dat = player.getSoDPlayer();
		if (dat.getDualismFaction() != ConquestFactionManager.NONE)
			return;
		
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = dat.getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, i18n.get(loc, "CONQUEST_TITLE"));
		tb.append(i18n.get(loc, "CONQUEST_SELECT_FACTION"));
		tb.append("<br>");
		L2Htm.writeButton(tb, "fact", ConquestFactionManager.ANCIENTS, 150, 30, false,
				i18n.get(loc, "CONQUEST_ANCIENTS"));
		L2Htm.writeButton(tb, "fact", ConquestFactionManager.DIVINES, 150, 30, false, i18n.get(loc, "CONQUEST_DIVINES"));
		L2Htm.writeFooter(tb);
		
		player.sendPacket(new ShowTutorialHtml(tb.moveToString()));
		
		RespawnManager.getInstance().moveToSpawn(player);
	}
	
	public void handleBypass(L2PcInstance player, String command)
	{
		player.sendPacket(HideTutorialHtml.PACKET);
		
		StringTokenizer st = new StringTokenizer(command, " ");
		st.nextToken(); // prefix
		
		int faction;
		try
		{
			faction = Integer.parseInt(st.nextToken());
		}
		catch (RuntimeException e)
		{
			return;
		}
		
		if (faction < ConquestFactionManager.ANCIENTS || faction > ConquestFactionManager.DIVINES)
			return;
		
		L2PlayerData dat = player.getSoDPlayer();
		if (dat.getDualismFaction() == ConquestFactionManager.NONE)
		{
			dat.setDualismFaction(faction);
			player.broadcastFullInfo();
			RespawnManager.getInstance().moveToSpawn(player);
			dat.updateDualism(false);
		}
	}
	
	/**
	 * Add player back to their faction's active players.
	 * Does nothing if player was already active.
	 * 
	 * @param dat
	 *            A player
	 */
	public void reactivate(L2PlayerData dat)
	{
		if (_inactive.remove(dat.getObjectId()) == null)
			return;
		FactionTemplate ft = getFactionTemplate(dat);
		ft.getActiveMembers().add(dat.getActingPlayer());
	}
	
	private class Inactivator implements Runnable
	{
		@Override
		public void run()
		{
			for (FastMap.Entry<Integer, FactionTemplate> e = _factions.head(), end = _factions.tail(); (e = e.getNext()) != end;)
			{
				FactionTemplate ft = e.getValue();
				FastSet<L2PcInstance> members = ft.getMembers();
				for (FastSet.Record r = members.head(), endr = members.tail(); (r = r.getNext()) != endr;)
				{
					L2PcInstance player = members.valueOf(r);
					L2PlayerData dat = player.getSoDPlayer();
					if (dat.isAway())
					{
						if (player.isCastingNow() || player.isCastingSimultaneouslyNow())
							continue; // do not tick
							
						if (ft.getActiveMembers().remove(player))
							_inactive.put(dat.getObjectId(), dat);
						else
							dat.tickInactive();
					}
				}
			}
			for (FastMap.Entry<Integer, L2PlayerData> e = _inactive.head(), end = _inactive.tail(); (e = e.getNext()) != end;)
			{
				L2PlayerData dat = e.getValue();
				if (dat.getInactivityTicks() >= Config.CONQUEST_INACTIVITY_TICKS_TO_KICK)
				{
					if (removeMember(dat))
					{
						L2PcInstance player = dat.getActingPlayer();
						dat.sendLocalizedMessage("CONQUEST_KICKED_AFK");
						RespawnManager.getInstance().moveToSpawn(player);
					}
				}
			}
		}
	}
	
	public static class FactionTemplate extends BaseInfo
	{
		private final int _id;
		private int _color;
		private final String _name;
		private final FastSet<L2PcInstance> _members;
		private final FastSet<L2PcInstance> _active;
		
		private FactionTemplate(int id, int color, String name, Location loc)
		{
			super(loc, PooledInstanceManager.getInstance().createInstance(), 2);
			
			_id = id;
			_color = color;
			_name = name;
			_members = FastSet.newInstance();
			_active = FastSet.newInstance();
		}
		
		public int getId()
		{
			return _id;
		}
		
		public int getColor()
		{
			return _color;
		}
		
		public void setColor(int color)
		{
			_color = color;
		}
		
		public String getName(Locale loc)
		{
			return Internationalization.getInstance().get(loc, _name);
		}
		
		public FastSet<L2PcInstance> getMembers()
		{
			return _members;
		}
		
		public FastSet<L2PcInstance> getActiveMembers()
		{
			return _active;
		}
		
		public void broadcast(Chat channel, String name, String key, String... repVals)
		{
			Broadcast.broadcastMessage(getMembers(), channel, name, false, key, repVals);
		}
		
		public void broadcast(L2ServerPacket gsp)
		{
			for (FastSet.Record r = getMembers().head(), end = getMembers().tail(); (r = r.getNext()) != end;)
				getMembers().valueOf(r).sendPacket(gsp);
		}
		
		public final void broadcast(SystemMessageId sm)
		{
			broadcast(sm.getSystemMessage());
		}
		
		public final void broadcast(String message)
		{
			broadcast(SystemMessage.valueOf(message));
		}
	}
	
	public static ConquestFactionManager getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final ConquestFactionManager INSTANCE = new ConquestFactionManager();
	}
}
