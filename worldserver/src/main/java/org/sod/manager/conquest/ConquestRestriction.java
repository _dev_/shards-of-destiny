/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.manager.conquest;

import org.sod.manager.conquest.ConquestFactionManager.FactionTemplate;
import org.sod.model.conquest.ConquestFactionMember;

import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.L2Skill.SkillTargetType;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.L2Playable;
import com.l2jfree.gameserver.model.actor.instance.L2DoorInstance;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.actor.instance.L2ZoneArtefactInstance;
import com.l2jfree.gameserver.model.actor.instance.L2ZoneGuardInstance;
import com.l2jfree.gameserver.model.actor.instance.L2ZoneSpawnGuardInstance;
import com.l2jfree.gameserver.model.restriction.global.AbstractRestriction;
import com.l2jfree.gameserver.model.restriction.global.GlobalRestrictions.CombatState;
import com.l2jfree.gameserver.model.restriction.global.RestrictionPriority;
import com.l2jfree.gameserver.model.zone.L2Zone;
import com.l2jfree.gameserver.network.SystemMessageId;
import com.l2jfree.gameserver.util.Util;

/**
 * Restrictions for Conquest players.
 * @author savormix
 * @since 2010.10.19
 */
public final class ConquestRestriction extends AbstractRestriction
{
	private ConquestRestriction()
	{
	}
	
	@Override
	public boolean canInviteToParty(L2PcInstance activeChar, L2PcInstance target)
	{
		if (!ConquestFactionManager.getInstance().isPlaying(activeChar, target))
			return true;
		else
			return getCombatState(activeChar, target) == CombatState.FRIEND;
	}
	
	@Override
	public CombatState getCombatState(L2PcInstance activeChar, L2PcInstance target)
	{
		if (!ConquestFactionManager.getInstance().isPlaying(activeChar, target))
			return CombatState.NEUTRAL;
		else
			return (ConquestFactionManager.getInstance().isSameFaction(activeChar, target)
					? CombatState.FRIEND : CombatState.ENEMY);
	}
	
	@Override
	public int getTitleColor(L2PcInstance activeChar)
	{
		if (activeChar.isGM() && ConquestFactionManager.getInstance().isPlaying(activeChar))
			return Util.reverseRGBChanels(ConquestFactionManager.getInstance().getFactionTemplate(activeChar)
					.getColor());
		else
			return -1;
	}
	
	@Override
	public int getNpcTitleColor(L2Npc npc)
	{
		if (npc instanceof ConquestFactionMember)
		{
			ConquestFactionManager cfm = ConquestFactionManager.getInstance();
			FactionTemplate ft = cfm.getFactionTemplate(cfm.getFaction(npc));
			if (ft == null)
				return -1;
			else
				return Util.reverseRGBChanels(ft.getColor());
		}
		return -1;
	}
	
	@Override
	public int getNameColor(L2PcInstance activeChar)
	{
		if (activeChar.isGM() || !ConquestFactionManager.getInstance().isPlaying(activeChar))
			return -1;
		else
			return Util.reverseRGBChanels(ConquestFactionManager.getInstance().getFactionTemplate(activeChar)
					.getColor());
	}
	
	@Override
	@RestrictionPriority(RestrictionPriority.DEFAULT_PRIORITY + 0.1)
	public Boolean isInsideZone(L2Character activeChar, byte zone)
	{
		if (activeChar instanceof L2Playable &&
				ConquestFactionManager.getInstance().isPlaying(activeChar.getActingPlayer()))
		{
			switch (zone)
			{
			case L2Zone.FLAG_PEACE:
				return Boolean.FALSE;
			case L2Zone.FLAG_PVP:
				return Boolean.TRUE;
			}
		}
		return null;
	}
	
	@Override
	public boolean isProtected(L2Character activeChar, L2Character target, L2Skill skill, boolean sendMessage,
			L2PcInstance attacker_, L2PcInstance target_, boolean isOffensive)
	{
		if (activeChar == null || activeChar == target || target instanceof L2ZoneArtefactInstance)
			return false;
		else if (target instanceof L2ZoneSpawnGuardInstance)
			return true;
		else if (target instanceof L2DoorInstance && target.isInsideZone(L2Zone.FLAG_TOWN))
			return true;
		else if (target instanceof L2ZoneGuardInstance && attacker_ == null)
			return true;
		
		if (!ConquestFactionManager.getInstance().isPlaying(attacker_, target_))
			return false;
		
		if (ConquestFactionManager.getInstance().isSameFaction(activeChar, target))
		{
			if (isOffensive)
			{
				if (sendMessage)
					activeChar.sendPacket(SystemMessageId.TARGET_IS_INCORRECT);
				return true;
			}
		}
		else
		{
			if (!isOffensive)
			{
				if (skill != null && skill.getTargetType() == SkillTargetType.TARGET_CORPSE_PLAYER)
					return false;
				if (sendMessage)
					activeChar.sendPacket(SystemMessageId.TARGET_IS_INCORRECT);
				return true;
			}
		}
		return false;
	}
	
	@Override
	public void playerLoggedIn(L2PcInstance activeChar)
	{
		ConquestZoneManager.getInstance().lockUnlock(false, activeChar);
	}
	
	@Override
	public void playerDisconnected(L2PcInstance activeChar)
	{
		ConquestZoneManager.getInstance().lockUnlock(false, activeChar);
	}
	
	@Override
	public boolean playerDied(L2PcInstance activeChar)
	{
		return playerKilled(null, activeChar, null);
	}
	
	@Override
	public boolean playerKilled(L2Character activeChar, L2PcInstance target, L2PcInstance killer)
	{
		if (target != null)
			ConquestFactionManager.getInstance().scabMember(target.getSoDPlayer());
		return false;
	}
	
	public static ConquestRestriction getInstance()
	{
		return SingletonHolder._instance;
	}
	
	private static final class SingletonHolder
	{
		private static final ConquestRestriction _instance = new ConquestRestriction();
	}
}
