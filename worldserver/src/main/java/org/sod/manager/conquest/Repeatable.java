/*
 * MISSING LICENSING INFO
 */
package org.sod.manager.conquest;

/**
 * @author savormix
 */
public abstract class Repeatable implements Runnable {
	private final int _totalTicks;
	private int _currentTick;
	
	protected Repeatable(int totalTicks) {
		_totalTicks = totalTicks;
	}
	
	protected boolean nextTick() {
		return ++_currentTick < _totalTicks;
	}
}
