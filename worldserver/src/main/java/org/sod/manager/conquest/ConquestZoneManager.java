/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.manager.conquest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javolution.util.FastMap;
import javolution.util.FastMap.Entry;
import javolution.util.FastSet;

import org.sod.manager.RespawnManager;
import org.sod.manager.conquest.ConquestFactionManager.FactionTemplate;
import org.sod.model.event.global.GlobalEvent;

import com.l2jfree.Config;
import com.l2jfree.L2DatabaseFactory;
import com.l2jfree.gameserver.ThreadPoolManager;
import com.l2jfree.gameserver.model.Elementals;
import com.l2jfree.gameserver.model.L2World;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.actor.instance.L2ZoneManagerInstance;
import com.l2jfree.gameserver.model.zone.L2FactionZone;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.Chat;
import com.l2jfree.gameserver.util.Broadcast;
import com.l2jfree.lang.L2Math;
import com.l2jfree.util.logging.L2Logger;

/**
 * The faction zone manager does only two things:<BR>
 * 1. Load all zone data from the DB when server starts<BR>
 * 2. Update zone status in the DB when the owner changes
 * 
 * @author savormix
 * @since 2009.12.01
 */
public final class ConquestZoneManager
{
	private static final L2Logger _log = L2Logger.getLogger(ConquestZoneManager.class);
	
	private static final String RESTORE_DATA = "SELECT * FROM faction_zones";
	private static final String UPDATE_DATA = "UPDATE faction_zones SET ownerId=?,recoverCpLevel=?,recoverHpLevel=?,recoverMpLevel=?,damageLevel=?,damageMpLevel=?,damageAbnormal=?,recoverCpExpire=?,recoverHpExpire=?,recoverMpExpire=?,damageHpExpire=?,damageMpExpire=? WHERE zoneId=?";
	
	public static final int REGEN_CP = 0;
	public static final int REGEN_HP = 1;
	public static final int REGEN_MP = 2;
	
	private static final int BASE_REGEN_CP = 5;
	private static final int BASE_REGEN_HP = 8;
	private static final int BASE_REGEN_MP = 12;
	
	private static final int BASE_DAMAGE = 30;
	private static final int BASE_DAMAGE_MP = 10;
	
	private static final int[] ELEMENTAL_POWER =
	{
			0, 10, 20, 30, 45, 60, 80, 105, 145, 200
	};
	
	private final FastMap<Integer, ZoneData> _data;
	private final FastMap<Integer, L2FactionZone> _zones;
	private final ZoneData _default;
	private volatile int _open;
	
	private final Lock _conquestLock;
	private ScheduledFuture<?> _activation;
	private ScheduledFuture<?> _lockdown;
	private volatile boolean _active;
	
	private ConquestZoneManager()
	{
		_data = new FastMap<Integer, ZoneData>().setShared(true);
		_zones = new FastMap<Integer, L2FactionZone>().setShared(true);
		_default = new ZoneData(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		_open = 0;
		load();
		
		_conquestLock = new ReentrantLock();
	}
	
	public final void saveAll()
	{
		for (Entry<Integer, ZoneData> entry = _data.head(); (entry = entry.getNext()) != _data.tail();)
			update(entry.getKey(), entry.getValue());
	}
	
	/** Loads faction zone data from `faction_zones` */
	private void load()
	{
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(RESTORE_DATA);
			while (rs.next())
			{
				int id = rs.getInt("zoneId");
				_data.put(
						id,
						new ZoneData(rs.getInt("ownerId"), rs.getInt("defaultOwnerId"), rs.getInt("weight"), rs
								.getInt("innerLevel"), rs.getInt("recoverCpLevel"), rs.getInt("recoverHpLevel"), rs
								.getInt("recoverMpLevel"), rs.getInt("damageLevel"), rs.getInt("damageMpLevel"), rs
								.getInt("damageAbnormal"), rs.getInt("recoverCpExpire"), rs.getInt("recoverHpExpire"),
								rs.getInt("recoverMpExpire"), rs.getInt("damageHpExpire"), rs.getInt("damageMpExpire")));
			}
		}
		catch (Exception e)
		{
			_log.error("Couldn't restore faction zone data!", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
		_log.info("FactionZoneManager: loaded " + _data.size() + " zone(s).");
	}
	
	/**
	 * <S>Registers the actual L2FactionZone and</S> returns the loaded data for that zone.
	 * 
	 * @param zone
	 *            a faction zone
	 * @return ZoneData object containing zone's data
	 */
	public ZoneData register(L2FactionZone zone)
	{
		Integer id = zone.getId();
		ZoneData dat = _data.get(id);
		_zones.put(id, zone);
		zone.lock();
		// lockUnlock();
		if (dat == null)
		{
			_log.warn("Missing faction zone data for zone " + id);
			return _default;
		}
		else
			return dat;
	}
	
	private void startLockdown()
	{
		_lockdown = ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(
				new Repeatable(Config.CONQUEST_LOCK_TICKS)
				{
					@Override
					public void run()
					{
						try
						{
							_conquestLock.lock();
							
							if (L2World.getInstance().getAllPlayersCount() >= Config.CONQUEST_LOCK_CANCEL_PLAYERS)
							{
								_lockdown.cancel(false);
								_lockdown = null;
								Broadcast.toAllOnlinePlayers(Chat.COMMANDER, "CONQUEST_FUSION_CONTROL_TITLE", true,
										"CONQUEST_FUSION_CONTROL_PLAYERS_REJOINED");
								_log.info("Conquest lockdown cancelled.");
								return;
							}
							
							if (nextTick())
							{
								Broadcast.toAllOnlinePlayers(Chat.COMMANDER, "CONQUEST_FUSION_CONTROL_TITLE", true,
										"CONQUEST_FUSION_CONTROL_LOCKDOWN_IN_PROGRESS");
							}
							else
							{
								setActive(false);
								_lockdown.cancel(false);
								_lockdown = null;
								Broadcast.toAllOnlinePlayers(Chat.COMMANDER, "CONQUEST_FUSION_CONTROL_TITLE", true,
										"CONQUEST_FUSION_CONTROL_LOCKDOWN_SUCCESS");
								_log.info("Conquest is now locked.");
							}
						}
						finally
						{
							_conquestLock.unlock();
						}
					}
				}, Config.CONQUEST_LOCK_TICK, Config.CONQUEST_LOCK_TICK);
		_log.info("Conquest lockdown initiated.");
	}
	
	private void startActivation()
	{
		_activation = ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(
				new Repeatable(Config.CONQUEST_PREP_TICKS)
				{
					@Override
					public void run()
					{
						try
						{
							_conquestLock.lock();
							
							if (L2World.getInstance().getAllPlayersCount() < Config.CONQUEST_PREP_PLAYERS)
							{
								Broadcast.toAllOnlinePlayers(Chat.COMMANDER, "CONQUEST_FUSION_CONTROL_TITLE", true,
										"CONQUEST_FUSION_CONTROL_INIT_FAILED");
								_activation.cancel(false);
								_activation = null;
								_log.info("Conquest activation cancelled.");
								return;
							}
							
							if (nextTick())
							{
								Broadcast.toAllOnlinePlayers(Chat.COMMANDER, "CONQUEST_FUSION_CONTROL_TITLE",
										"CONQUEST_FUSION_CONTROL_INIT_IN_PROGRESS");
							}
							else
							{
								setActive(true);
								_activation.cancel(false);
								_activation = null;
								Broadcast.toAllOnlinePlayers(Chat.COMMANDER, "CONQUEST_FUSION_CONTROL_TITLE",
										"CONQUEST_FUSION_CONTROL_INIT_SUCCESS");
								_log.info("Conquest is now active.");
							}
						}
						finally
						{
							_conquestLock.unlock();
						}
					}
				}, Config.CONQUEST_PREP_TICK, Config.CONQUEST_PREP_TICK);
		_log.info("Conquest activation initiated.");
	}
	
	public void lockUnlock(boolean capture, L2PcInstance player)
	{
		int online;
		
		if (player != null)
		{
			try
			{
				_conquestLock.lock();
				
				online = L2World.getInstance().getAllPlayersCount();
				if (player.getOnlineState() == L2PcInstance.ONLINE_STATE_DELETED)
					online--;
				
				if (isActive())
				{
					if (online <= Config.CONQUEST_LOCK_PLAYERS && _lockdown == null)
						startLockdown();
				}
				else
				{
					if (online >= Config.CONQUEST_PREP_PLAYERS && _activation == null)
						startActivation();
				}
			}
			finally
			{
				_conquestLock.unlock();
			}
		}
		else
			online = L2World.getInstance().getAllPlayersCount();
		
		int open = (online / Config.CONQUEST_ZONE_NEEDED_PLAYERS) + 1;
		int a = 0, d = 0;
		for (Entry<Integer, L2FactionZone> entry = getZoneMap().head(); (entry = entry.getNext()) != getZoneMap()
				.tail();)
		{
			L2FactionZone zone = entry.getValue();
			if (zone.getConquestFaction() == ConquestFactionManager.ANCIENTS)
				a++;
			else if (zone.getConquestFaction() == ConquestFactionManager.DIVINES)
				d++;
			zone.unmark();
		}
		open = L2Math.min(open, a, d);
		if (!capture && _open == open)
			return;
		_open = open;
		for (int i = 0; i < open; i++)
		{
			L2FactionZone zone = getOutermostUnmarkedZone(ConquestFactionManager.ANCIENTS);
			if (zone != null)
				zone.mark();
			zone = getOutermostUnmarkedZone(ConquestFactionManager.DIVINES);
			if (zone != null)
				zone.mark();
		}
		for (Entry<Integer, L2FactionZone> entry = getZoneMap().head(); (entry = entry.getNext()) != getZoneMap()
				.tail();)
		{
			L2FactionZone zone = entry.getValue();
			if (zone.isMarked())
				zone.unlock();
			else
				zone.lock();
		}
	}
	
	private L2FactionZone getOutermostUnmarkedZone(int faction)
	{
		L2FactionZone zone = null;
		for (Entry<Integer, L2FactionZone> entry = getZoneMap().head(); (entry = entry.getNext()) != getZoneMap()
				.tail();)
		{
			L2FactionZone z = entry.getValue();
			if (z.getConquestFaction() != faction)
				continue;
			else if (!z.isMarked()
					&& (zone == null || z.getZoneData().getInnerLevel() < zone.getZoneData().getInnerLevel()))
				zone = z;
		}
		return zone;
	}
	
	public L2FactionZone getFactionZone(int id)
	{
		return _zones.get(id);
	}
	
	public int getLoadedCount()
	{
		return _zones.size();
	}
	
	public FastMap<Integer, L2FactionZone> getZoneMap()
	{
		return _zones;
	}
	
	public boolean isActive()
	{
		return _active;
	}
	
	private void setActive(boolean active)
	{
		_active = active;
		
		if (!isActive())
		{
			ConquestFactionManager cfm = ConquestFactionManager.getInstance();
			RespawnManager rm = RespawnManager.getInstance();
			for (L2PcInstance pc : L2World.getInstance().getAllPlayers())
				if (cfm.removeMember(pc.getSoDPlayer()))
					rm.moveToSpawn(pc);
		}
	}
	
	/**
	 * Update the specified zone's data in the database.
	 * 
	 * @param id
	 *            Zone's ID, specified in the XML file
	 * @param zd
	 *            ZoneData object containing the zone's data
	 */
	public void update(int id, ZoneData zd)
	{
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(UPDATE_DATA);
			ps.setInt(1, zd.getOwnerFaction());
			ps.setInt(2, zd.getRegenLevel(REGEN_CP));
			ps.setInt(3, zd.getRegenLevel(REGEN_HP));
			ps.setInt(4, zd.getRegenLevel(REGEN_MP));
			ps.setInt(5, zd.getDamageLevel(false));
			ps.setInt(6, zd.getDamageLevel(true));
			ps.setInt(7, zd.getAbnormal());
			ps.setInt(8, zd.getRegenExpiryMs(REGEN_CP));
			ps.setInt(9, zd.getRegenExpiryMs(REGEN_HP));
			ps.setInt(10, zd.getRegenExpiryMs(REGEN_MP));
			ps.setInt(11, zd.getDamageExpiryMs(false));
			ps.setInt(12, zd.getDamageExpiryMs(true));
			ps.setInt(13, id);
			ps.executeUpdate();
			ps.close();
		}
		catch (Exception e)
		{
			_log.error("Couldn't update faction zone data!", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	public void checkOwnedZones()
	{
		int a = 0, d = 0;
		for (Entry<Integer, L2FactionZone> entry = _zones.head(); (entry = entry.getNext()) != _zones.tail();)
		{
			switch (entry.getValue().getConquestFaction())
			{
			case ConquestFactionManager.ANCIENTS:
				a++;
				break;
			case ConquestFactionManager.DIVINES:
				d++;
				break;
			}
		}
		if (a != 0 && d != 0)
		{
			lockUnlock(true, null);
			return;
		}
		L2ZoneManagerInstance.SUSPEND = true;
		for (int i = ConquestFactionManager.ANCIENTS; i <= ConquestFactionManager.DIVINES; i++)
		{
			FactionTemplate ft = ConquestFactionManager.getInstance().getFactionTemplate(i);
			FastSet<L2PcInstance> players = ft.getMembers();
			for (FastSet.Record r = players.head(), end = players.tail(); (r = r.getNext()) != end;)
			{
				L2PcInstance player = players.valueOf(r);
				if (player.isDead())
					player.setIsPendingRevive(true);
				RespawnManager.getInstance().moveToSpawn(player);
				if ((a == 0 && i == ConquestFactionManager.DIVINES) || (d == 0 && i == ConquestFactionManager.ANCIENTS))
				{
					player.addItem("All Zones", GlobalEvent.EVENT_MEDAL, 5, null, true);
					player.addAdena("All Zones", 100, null, true);
				}
			}
		}
		for (Entry<Integer, L2FactionZone> entry = _zones.head(); (entry = entry.getNext()) != _zones.tail();)
		{
			L2FactionZone zone = entry.getValue();
			zone.changeOwner(zone.getZoneData().getDefFaction(), true);
		}
		L2ZoneManagerInstance.SUSPEND = false;
	}
	
	/**
	 * This class represents all data needed for a L2FactionZone. <LI>Owner faction</LI> <LI>Zone weight</LI> <LI>CP
	 * recovery level</LI> <LI>HP recovery level</LI> <LI>MP recovery level</LI> <LI>CP+HP damage level</LI> <LI>MP
	 * damage level</LI> <LI>Abnormal effect / damage elemental attribute</LI>
	 * 
	 * @author savormix
	 * @since 2009.12.01
	 */
	public class ZoneData
	{
		private static final int SYSTEM_UPGRADE_TIMEOUT = 3600000;
		
		private int _faction;
		private final int _defFaction;
		private final int _weight;
		private final int _innerLevel;
		private int[] _recovery;
		private int[] _damage;
		private int _abnormal;
		private final ScheduledFuture<?>[] _recTask;
		private final ScheduledFuture<?>[] _dmgTask;
		
		private ZoneData(int faction, int defFaction, int weight, int innerLevel, int cpRe, int hpRe, int mpRe,
				int hpDam, int mpDam, int abnormal, int cpExp, int hpExp, int mpExp, int hpDamExp, int mpDamExp)
		{
			_recTask = new ScheduledFuture<?>[3];
			_dmgTask = new ScheduledFuture<?>[2];
			setOwnerFaction(faction);
			_defFaction = defFaction;
			_weight = weight;
			_innerLevel = innerLevel;
			setRegenLevel(REGEN_CP, cpRe, cpExp);
			setRegenLevel(REGEN_HP, hpRe, hpExp);
			setRegenLevel(REGEN_MP, mpRe, mpExp);
			setDamageLevel(false, hpDam, hpDamExp);
			setDamageLevel(true, mpDam, mpDamExp);
			_abnormal = abnormal;
			if (_abnormal == 0)
				_abnormal = 1;
		}
		
		/** @return owner faction's ID */
		public int getOwnerFaction()
		{
			return _faction;
		}
		
		/**
		 * Changes zone owner and resets the buyable stats.
		 * 
		 * @param faction
		 *            new owner
		 */
		public void setOwnerFaction(int faction)
		{
			_faction = faction;
			reset();
		}
		
		/** @return the original owner, intended by builder */
		public int getDefFaction()
		{
			return _defFaction;
		}
		
		/** @return TODO: Gained faction points multiplier? */
		public int getWeight()
		{
			return _weight;
		}
		
		/**
		 * Returns a value to define how far inside a faction's
		 * domain this zone is. Lower values - closer to border,
		 * higher values - further from border.
		 * 
		 * @return
		 */
		public int getInnerLevel()
		{
			if (getOwnerFaction() != getDefFaction())
				return -_innerLevel;
			else
				return _innerLevel;
		}
		
		/** @return Abnormal effect type */
		public int getAbnormal()
		{
			return _abnormal;
		}
		
		/** @return Damage element type */
		public byte getElement()
		{
			switch (getAbnormal())
			{
			case 1:
				return Elementals.NONE;
			case 2:
				return Elementals.DARK;
			case 8:
				return Elementals.WATER;
			case 16:
				return Elementals.WIND;
			case 16384:
				return Elementals.FIRE;
			default:
				return -2;
			}
		}
		
		/** @return damage level (0-9) */
		public int getDamageLevel(boolean mp)
		{
			return _damage[mp ? 1 : 0];
		}
		
		/** @return recovery level (0-9) */
		public int getRegenLevel(int type)
		{
			return _recovery[type];
		}
		
		public void setAbnormal(int mask)
		{
			_abnormal = mask;
		}
		
		public void setDamageLevel(boolean mp, int level)
		{
			setDamageLevel(mp, level, SYSTEM_UPGRADE_TIMEOUT);
		}
		
		private void setDamageLevel(boolean mp, int level, int delay)
		{
			int index = (mp ? 1 : 0);
			_damage[index] = level;
			if (_dmgTask[index] != null)
			{
				_dmgTask[index].cancel(false);
				_dmgTask[index] = null;
			}
			if (level > 0)
				_dmgTask[index] = ThreadPoolManager.getInstance().scheduleGeneral(new UpgradeReset(mp), delay);
		}
		
		public void setRegenLevel(int type, int level)
		{
			setRegenLevel(type, level, SYSTEM_UPGRADE_TIMEOUT);
		}
		
		private void setRegenLevel(int type, int level, int delay)
		{
			_recovery[type] = level;
			if (_recTask[type] != null)
			{
				_recTask[type].cancel(false);
				_recTask[type] = null;
			}
			if (level > 0)
				_recTask[type] = ThreadPoolManager.getInstance().scheduleGeneral(new UpgradeReset(type), delay);
		}
		
		public int getRegenExpiryMs(int recType)
		{
			ScheduledFuture<?> task = _recTask[recType];
			if (task == null)
				return 0;
			else
				return (int) task.getDelay(TimeUnit.MILLISECONDS);
		}
		
		public int getDamageExpiryMs(boolean mp)
		{
			ScheduledFuture<?> task = _dmgTask[mp ? 1 : 0];
			if (task == null)
				return 0;
			else
				return (int) task.getDelay(TimeUnit.MILLISECONDS);
		}
		
		private final void reset()
		{
			_recovery = new int[3];
			_damage = new int[2];
			_abnormal = 1;
			for (ScheduledFuture<?> task : _recTask)
				if (task != null)
					task.cancel(true);
			for (ScheduledFuture<?> task : _dmgTask)
				if (task != null)
					task.cancel(true);
		}
		
		private final class UpgradeReset implements Runnable
		{
			@SuppressWarnings("hiding")
			private final boolean _recovery;
			private final int _recType;
			private final boolean _dmgMana;
			
			private UpgradeReset(int recType)
			{
				_recovery = true;
				_recType = recType;
				_dmgMana = false;
			}
			
			private UpgradeReset(boolean mp)
			{
				_recovery = false;
				_recType = 0;
				_dmgMana = mp;
			}
			
			@Override
			public void run()
			{
				Integer i = null;
				for (Integer key : _data.keySet())
					if (_data.get(key) == ZoneData.this)
					{
						i = key;
						break;
					}
				L2FactionZone zone = _zones.get(i);
				if (zone == null)
					return;
				if (_recovery)
					setRegenLevel(_recType, 0);
				else
					setDamageLevel(_dmgMana, 0);
				if (!zone.isDangerous())
					zone.changeAbnormal(0);
			}
		}
	}
	
	/**
	 * Calculates CP/HP/MP increase based on the recovery level.<BR>
	 * <BR>
	 * The formula is <CODE>BASE_REGEN_?P</CODE> multiplied by the recovery
	 * level.
	 * 
	 * @param type
	 *            Recovery type (<CODE>REGEN_?P</CODE> in this class)
	 * @param zd
	 *            ZoneData object
	 * @return CP/HP/MP increase
	 */
	public static int calculateRegen(int type, ZoneData zd)
	{
		int base = 0;
		switch (type)
		{
		case 0:
			base = BASE_REGEN_CP;
			break;
		case 1:
			base = BASE_REGEN_HP;
			break;
		case 2:
			base = BASE_REGEN_MP;
		}
		return base * zd.getRegenLevel(type);
	}
	
	/**
	 * Behaves exactly same as <CODE>calculateDamage(false, zd, cha)</CODE>.
	 * 
	 * @param zd
	 *            ZoneData object
	 * @param cha
	 *            character receiving the damage
	 * @return Maximum HP damage to be inflicted
	 */
	public static double calculateDamage(ZoneData zd, L2Character cha)
	{
		return calculateDamage(false, zd, cha);
	}
	
	/**
	 * Behaves exactly same as <CODE>calculateDamage(true, zd, cha)</CODE>.
	 * 
	 * @param zd
	 *            ZoneData object
	 * @param cha
	 *            character receiving the damage
	 * @return Maximum MP damage to be inflicted
	 */
	public static double calculateMpDamage(ZoneData zd, L2Character cha)
	{
		return calculateDamage(true, zd, cha);
	}
	
	/**
	 * Calculates damage to be inflicted on a character.<BR>
	 * <BR>
	 * If damage has an elemental attribute, the damage multiplier depends
	 * on zone's elemental power (the higher the damage level, the higher
	 * the power) and character's elemental defense.<BR>
	 * Examples:<BR>
	 * If the difference between power and defense is +35, the damage will
	 * be multiplied by 1.2<BR>
	 * if the difference is -175, the damage will be halved.<BR>
	 * <BR>
	 * If the damage doesn't have an elemental attribute (is physical), then
	 * the damage is simply multiplied by 1.5
	 * 
	 * @param mp
	 *            true for MP damage, false for CP/HP damage
	 * @param zd
	 *            ZoneData object containing zone damage level & type
	 * @param cha
	 *            character receiving the damage
	 * @return Maximum damage to be inflicted
	 */
	private static double calculateDamage(boolean mp, ZoneData zd, L2Character cha)
	{
		int base = (mp ? BASE_DAMAGE_MP : BASE_DAMAGE);
		final byte element = zd.getElement();
		if (element > -1)
		{
			int att = ELEMENTAL_POWER[zd.getDamageLevel(mp)];
			int def = cha.getDefenseElementValue(element);
			return base * zd.getDamageLevel(mp) * (1 + (att - def) / 175D);
		}
		else
			return base * zd.getDamageLevel(mp) * 1.5D;
	}
	
	/** @return the manager */
	public static ConquestZoneManager getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final ConquestZoneManager INSTANCE = new ConquestZoneManager();
	}
}
