/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.manager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.RandomAccess;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import javolution.util.FastSet;

import com.l2jfree.util.logging.L2Logger;

import org.sod.model.L2PlayerData;

import com.l2jfree.L2DatabaseFactory;
import com.l2jfree.gameserver.ThreadPoolManager;
import com.l2jfree.gameserver.datatables.ItemTable;
import com.l2jfree.gameserver.idfactory.IdFactory;
import com.l2jfree.gameserver.model.L2ItemInstance;
import com.l2jfree.gameserver.model.L2ItemInstance.ItemLocation;
import com.l2jfree.gameserver.model.L2World;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.itemcontainer.PcInventory;
import com.l2jfree.gameserver.network.SystemMessageId;
import com.l2jfree.gameserver.network.serverpackets.ExItemAuctionInfo;
import com.l2jfree.gameserver.network.serverpackets.L2GameServerPacket.ElementalOwner;

/**
 * Based on Infinite L2's implementation.<BR>
 * Still needs a review.
 * @author savormix
 */
public final class ItemAuctionManager
{
	private static final L2Logger _log = L2Logger.getLogger(ItemAuctionManager.class);
	
	private static final String DECREASE_ITEM_COUNT = "UPDATE items SET count=(count-?) WHERE object_id=?";
	private static final String CHANGE_ITEM_OWNER = "UPDATE items SET owner_id=? WHERE object_id=?";
	
	private final AtomicInteger _auctionIds;
	private final AuctionHolder _holder;
	
	private ItemAuctionManager()
	{
		_auctionIds = new AtomicInteger();
		_holder = new AuctionHolder();
		_log.info("ItemAuctionManager: initialized.");
		load();
		ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(new AuctionTicker(), 5000, 1500);
	}
	
	private AtomicInteger getAuctionIds()
	{
		return _auctionIds;
	}
	
	public AuctionHolder getHolder()
	{
		return _holder;
	}
	
	private void load()
	{
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM bbs_memo_auction");
			ResultSet rs = ps.executeQuery();
			while (rs.next())
			{
				int itemId = rs.getInt("itemId");
				L2ItemInstance temp = ItemTable.getInstance().createDummyItem(itemId);
				getHolder().addAuction(
						new ItemReflection(
								rs.getLong("endDate"), rs.getLong("bid"), rs.getInt("bidderId"),
								rs.getInt("ownerId"), rs.getInt("objectId"), itemId,
								temp.getItemDisplayId(), rs.getLong("count"), temp.getItemName(),
								temp.getItem().getType2(), rs.getInt("cType1"),
								temp.getItem().getBodyPart(), rs.getInt("enchant"),
								rs.getInt("cType2"), rs.getInt("aug"), rs.getInt("mana"),
								rs.getInt("elPow"), rs.getByte("elType"), rs.getInt("elDef0"),
								rs.getInt("elDef1"), rs.getInt("elDef2"), rs.getInt("elDef3"),
								rs.getInt("elDef4"), rs.getInt("elDef5"), rs.getInt("time")
								)
						);
			}
		}
		catch (Exception e)
		{
			_log.error("Cannot restore auctions!", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
		_log.info("ItemAuctionManager: loaded " + getHolder().getAuctions().size() + " items for up for auction!");
	}
	
	public long getClaimableCount(L2ItemInstance item)
	{
		if (item == null)
			return 0;
		long total = item.getCount();
		if (getHolder().getItems().contains(item.getObjectId()))
		{
			if (item.isStackable())
			{
				for (int i = 0; i < getHolder().getAuctions().size(); i++)
				{
					ItemReflection ir = getHolder().getAuctions().get(i);
					if (ir != null && ir.getObjectId() == item.getObjectId())
						total -= ir.getCount();
				}
			}
			else
				return 0;
		}
		return total;
	}
	
	public void addToAuction(L2ItemInstance item, long count, int minutes, long bid)
	{
		// TESTING ONLY
		//if (minutes == 60)
		//	minutes = 1;
		// TESTING END
		ItemReflection ir = new ItemReflection(item, count, minutes, bid);
		getHolder().addAuction(ir);
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con
					.prepareStatement("INSERT INTO bbs_memo_auction VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			ps.setLong(1, ir.getEndDate());
			ps.setLong(2, ir.getBid());
			ps.setInt(3, ir.getBidderId());
			ps.setInt(4, ir.getOwnerId());
			ps.setInt(5, ir.getObjectId());
			ps.setInt(6, ir.getItemId());
			ps.setLong(7, ir.getCount());
			ps.setInt(8, ir.getEnchant());
			ps.setInt(9, ir.getCType1());
			ps.setInt(10, ir.getCType2());
			ps.setInt(11, ir.getAug());
			ps.setInt(12, ir.getMana());
			ps.setInt(13, ir.getTimeRemaining());
			ps.setInt(14, ir.getAttackElementPower());
			ps.setByte(15, ir.getAttackElementType());
			for (byte i = 0; i < 6; i++)
				ps.setInt(16 + i, ir.getElementDefAttr(i));
			ps.executeUpdate();
			ps.close();
		}
		catch (Exception e)
		{
			_log.warn("Failed saving auction data!", e);
			getHolder().removeAuction(ir);
			item.setLocation(ItemLocation.INVENTORY);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
	}
	
	// this method is called from a single thread, so not synchronized
	public void endAuction(ItemReflection ir)
	{
		// Remove from auction state table
		Connection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement ps = con
					.prepareStatement("DELETE FROM bbs_memo_auction WHERE endDate=? AND objectId=? AND count=? LIMIT 1");
			ps.setLong(1, ir.getEndDate());
			ps.setInt(2, ir.getObjectId());
			ps.setLong(3, ir.getCount());
			ps.executeUpdate();
			ps.close();
		}
		catch (Exception e)
		{
			_log.fatal("Could not remove auction from the DB!", e);
		}
		finally
		{
			L2DatabaseFactory.close(con);
		}
		
		L2PcInstance owner = L2World.getInstance().getPlayer(ir.getOwnerId());
		L2PcInstance bidder = L2World.getInstance().getPlayer(ir.getBidderId());
		// Original owner online
		if (owner != null)
		{
			// Someone has bid on the auction
			if (ir.getOwnerId() != ir.getBidderId())
			{
				owner.addAdena("Auction end", ir.getBid(), null, true);
				L2PlayerData dat = owner.getSoDPlayer();
				// Highest bidder online
				if (bidder != null)
				{
					// Just transfer between online players
					transferToOnlineBidder(owner, bidder, ir);
				}
				// Highest bidder offline
				else
				{
					// remove from former owner and database
					// (this does not generate a new item)
					L2ItemInstance item = dat.getAuction().destroyItem("Auction end", ir.getObjectId(), ir.getCount(), owner, bidder);
					try
					{
						con = L2DatabaseFactory.getInstance().getConnection();
						// item is stackable
						if (item.isStackable())
						{
							// check if bidder has such stackable item
							int objectId = getItemObjectId(con, ir.getBidderId(), ir.getItemId());
							if (objectId != -1)
							{
								// increase the stackable's count (note the minus)
								PreparedStatement ps = con.prepareStatement(DECREASE_ITEM_COUNT);
								ps.setLong(1, -ir.getCount());
								ps.setInt(2, objectId);
								ps.executeUpdate();
								ps.close();
							}
							// generate a new item in the database
							else
								insertWhileOffline(con, ir);
						}
						// item is not stackable
						else
							insertWhileOffline(con, ir);
					}
					catch (Exception e)
					{
						_log.fatal("Item was not successfully transfered in DB!", e);
					}
					finally
					{
						L2DatabaseFactory.close(con);
					}
				}
			}
			// Noone bid in the auction
			else
				owner.sendMessage("Your " + ir.getCount() + " " + ir.getName() + " did not sell in the auction.");
		}
		// Original owner offline
		else
		{
			try
			{
				L2ItemInstance item = ItemTable.getInstance().createDummyItem(ir.getItemId());
				con = L2DatabaseFactory.getInstance().getConnection();
				// item is stackable
				if (item.isStackable())
				{
					// get item count (object ID guarantees to be in AUCTION)
					long count = getItemCount(con, ir.getObjectId());
					if (count < ir.getCount())
						_log.warn("Possible exploit/scam by player " + ir.getOwnerId());
					PreparedStatement ps;
					// exact count, remove, can reuse object ID
					if (count <= ir.getCount())
					{
						// removed directly from the database
						// related entries will be deleted on next restart, as well as
						// auto-release will be done
						ps = con.prepareStatement("DELETE FROM items WHERE object_id=?");
						ps.setInt(1, ir.getObjectId());
					}
					// more on auction, so just decrease
					else
					{
						ps = con.prepareStatement(DECREASE_ITEM_COUNT);
						ps.setLong(1, ir.getCount());
						ps.setInt(2, ir.getObjectId());
					}
					ps.executeUpdate();
					ps.close();
					// add the bid to owner
					int adenaId = getItemObjectId(con, ir.getOwnerId(), PcInventory.ADENA_ID);
					if (adenaId != -1)
					{
						ps = con.prepareStatement(DECREASE_ITEM_COUNT);
						ps.setLong(1, -ir.getBid());
						ps.setInt(2, adenaId);
						ps.executeUpdate();
						ps.close();
					}
					else
					{
						ps = con.prepareStatement("INSERT INTO items VALUES (?,?,?,?,?,'INVENTORY',0,?,?,?,?,0)");
						ps.setInt(1, ir.getOwnerId());
						ps.setInt(2, IdFactory.getInstance().getNextId());
						ps.setInt(3, PcInventory.ADENA_ID);
						ps.setLong(4, ir.getBid());
						ps.setInt(5, 0);
						ps.setInt(6, 0);
						ps.setInt(7, 0);
						ps.setInt(8, -1);
						ps.setInt(9, -1);
						ps.executeUpdate();
						ps.close();
					}
					// add the item to bidder
					if (bidder == null)
						insertWhileOffline(con, ir);
				}
				// item is not stackable
				else
				{
					// Highest bidder offline, just swap owner in database
					if (bidder == null)
						swapOwner(con, ir);
				}
				// Highest bidder is online, makes no difference how we removed the item
				// Just add a new one
				if (bidder != null)
					transferToOnlineBidder(owner, bidder, ir);
			}
			catch (Exception e)
			{
				_log.fatal("Item was not successfully transfered in the DB!", e);
			}
			finally
			{
				L2DatabaseFactory.close(con);
			}
		}
	}
	
	private final void transferToOnlineBidder(L2PcInstance owner, L2PcInstance bidder, ItemReflection ir)
	{
		bidder.getSoDPlayer().getAuction().addItem("Auction end", ir.getItemId(), ir.getCount(), bidder, owner);
		bidder.sendMessage("You bought " + ir.getCount() + " " + ir.getName() + " in the auction.");
	}
	
	private void insertWhileOffline(Connection con, ItemReflection ir) throws Exception
	{
		PreparedStatement ps = con.prepareStatement("INSERT INTO items VALUES (?,?,?,?,?,'AUCTION',0,?,?,?,?,0)");
		ps.setInt(1, ir.getBidderId());
		ps.setInt(2, IdFactory.getInstance().getNextId());
		ps.setInt(3, ir.getItemId());
		ps.setLong(4, ir.getCount());
		ps.setInt(5, ir.getEnchant());
		ps.setInt(6, ir.getCType1());
		ps.setInt(7, ir.getCType2());
		ps.setInt(8, ir.getMana());
		ps.setInt(9, ir.getTimeRemaining());
		ps.executeUpdate();
		ps.close();
	}
	
	private void swapOwner(Connection con, ItemReflection ir) throws Exception
	{
		PreparedStatement ps = con.prepareStatement(CHANGE_ITEM_OWNER);
		ps.setInt(1, ir.getBidderId());
		ps.setInt(2, ir.getObjectId());
		ps.executeUpdate();
		ps.close();
	}
	
	private long getItemCount(Connection con, int objectId) throws Exception
	{
		PreparedStatement ps = con.prepareStatement("SELECT count FROM items WHERE object_id=?");
		ps.setInt(1, objectId);
		ResultSet rs = ps.executeQuery();
		try
		{
			if (!rs.next())
				return 0;
			else
				return rs.getLong("count");
		}
		finally
		{
			rs.close();
			ps.close();
		}
	}
	
	private int getItemObjectId(Connection con, int ownerId, int itemId) throws Exception
	{
		PreparedStatement ps = con.prepareStatement("SELECT object_id FROM items WHERE owner_id=? AND item_id=? AND loc LIKE 'INVENTORY'");
		ps.setInt(1, ownerId);
		ps.setInt(2, itemId);
		ResultSet rs = ps.executeQuery();
		try
		{
			if (!rs.next())
				return -1;
			else
				return rs.getInt("object_id");
		}
		finally
		{
			rs.close();
			ps.close();
		}
	}
	
	public void openBid(int id, L2PcInstance player)
	{
		ItemReflection ir = getAuction(id);
		if (ir != null)
			player.sendPacket(new ExItemAuctionInfo(ir, true));
		else
			player.sendPacket(SystemMessageId.NO_AUCTION_PERIOD);
	}
	
	public ItemReflection getAuction(int id)
	{
		for (int i = 0; i < getHolder().getAuctions().size(); i++)
		{
			ItemReflection ir = getHolder().getAuctions().get(i);
			if (ir != null && ir.getId() == id)
				return ir;
		}
		return null;
	}
	
	public static ItemAuctionManager getInstance()
	{
		return SingletonHolder._instance;
	}
	
	@SuppressWarnings("synthetic-access")
	private static final class SingletonHolder
	{
		private static final ItemAuctionManager _instance = new ItemAuctionManager();
	}
	
	private final class AuctionTicker implements Runnable
	{
		@Override
		public void run()
		{
			try
			{
				for (int i = 0; i < getHolder().getAuctions().size(); i++)
				{
					ItemReflection ir = getHolder().getAuctions().get(i);
					if (ir != null && ir.getState() == 0)
					{
						getHolder().removeAuction(ir);
						endAuction(ir);
					}
				}
			}
			catch (Exception e)
			{
				_log.warn("Failed during auction tick!", e);
				// continues operation on next tick
			}
		}
	}
	
	/**
	 * We could just do a cross-query for this item's attributes.
	 * Just would that be better? RAM is cheaper than ultra-fast HDDs.
	 * @author savormix
	 */
	public class ItemReflection implements ElementalOwner
	{
		// auction related
		private final int _id;
		private final long _endDate;
		private volatile long _bid;
		private volatile int _bidderId;
		
		// item related
		private final int _ownerId;
		private final Integer _objectId;
		private final int _itemId;
		private final int _displayId;
		private final long _count;
		// for lists
		private final String _name;
		
		// item properties
		private final int _type2;
		private final int _cType1;
		private final int _bodypart;
		private final int _enchant;
		private final int _cType2;
		private final int _aug;
		private final int _mana;
		
		private final int _elPow;
		private final byte _elType;
		private final int[] _elDef;
		
		private final int _remTime;
		
		private ItemReflection(long endDate, long bid, int bidderId, int ownerId, Integer objectId,
				int itemId, int displayId, long count, String name, int type2, int cType1,
				int bodypart, int enchant, int cType2, int aug, int mana, int elPow, byte elType,
				int elDef0, int elDef1, int elDef2, int elDef3, int elDef4, int elDef5, int remTime)
		{
			_id = getAuctionIds().getAndIncrement();
			_endDate = endDate;
			_bid = bid;
			_bidderId = bidderId;
			_ownerId = ownerId;
			_objectId = objectId;
			_itemId = itemId;
			_displayId = displayId;
			_count = count;
			_name = name;
			_type2 = type2;
			_cType1 = cType1;
			_bodypart = bodypart;
			_enchant = enchant;
			_cType2 = cType2;
			_aug = aug;
			_mana = mana;
			_elPow = elPow;
			_elType = elType;
			_elDef = new int[] {
					elDef0, elDef1, elDef2, elDef3, elDef4, elDef5
			};
			_remTime = remTime;
		}
		
		public ItemReflection(L2ItemInstance item, long count, int length, long bid)
		{
			this(System.currentTimeMillis() + length * 60000, bid, item.getOwnerId(),
					item.getOwnerId(), item.getObjectId(), item.getItemId(),
					item.getItemDisplayId(), count, item.getItemName(), item.getItem().getType2(),
					item.getCustomType1(), item.getItem().getBodyPart(), item.getEnchantLevel(),
					item.getCustomType2(),
					item.isAugmented() ? item.getAugmentation().getAugmentationId() : 0,
							item.getMana(), item.getAttackElementPower(), item.getAttackElementType(),
							item.getElementDefAttr((byte) 0), item.getElementDefAttr((byte) 1),
							item.getElementDefAttr((byte) 2), item.getElementDefAttr((byte) 3),
							item.getElementDefAttr((byte) 4), item.getElementDefAttr((byte) 5),
							item.isTimeLimitedItem() ? (int) (item.getRemainingTime() / 1000) : -1);
		}
		
		public int getId()
		{
			return _id;
		}
		
		public long getEndDate()
		{
			return _endDate;
		}
		
		public int getTimeLeft()
		{
			return (int) ((getEndDate() - System.currentTimeMillis()) / 1000);
		}
		
		public long getBid()
		{
			return _bid;
		}
		
		public int getBidderId()
		{
			return _bidderId;
		}
		
		public synchronized void setBid(long bid, int bidderId)
		{
			int prevBidder = getBidderId();
			long prevBid = getBid();
			if (prevBidder != getOwnerId()) // owner paid 10% fee, did not actually bid
			{
				L2PcInstance loser = L2World.getInstance().getPlayer(prevBidder);
				if (loser == null)
				{
					Connection con = null;
					try
					{
						con = L2DatabaseFactory.getInstance().getConnection();
						PreparedStatement ps = con.prepareStatement("UPDATE items SET count=(count+?) WHERE owner_id=? AND item_id=?");
						ps.setLong(1, prevBid);
						ps.setInt(2, prevBidder);
						ps.setInt(3, PcInventory.ADENA_ID);
						if (ps.executeUpdate() == 0)
						{
							ps.close();
							ps = con.prepareStatement("INSERT INTO items VALUES (?,?,?,?,0,'INVENTORY',0,NULL,0,0,-1,-1,0)");
							ps.setInt(1, prevBidder);
							ps.setInt(2, IdFactory.getInstance().getNextId());
							ps.setInt(3, PcInventory.ADENA_ID);
							ps.setLong(4, prevBid);
							ps.executeUpdate();
						}
						ps.close();
					}
					catch (Exception e)
					{
						_log.error("Cannot recover smaller bid!", e);
					}
					finally
					{
						L2DatabaseFactory.close(con);
					}
				}
				else
				{
					if (prevBidder == bidderId)
						loser.sendMessage("You recover your previous bid.");
					else
						loser.sendPacket(SystemMessageId.YOU_HAVE_BEEN_OUTBID);
					loser.addAdena("Auction Outbid", prevBid, null, false);
				}
			}
			/* bid on own item
			else if (bidderId == getOwnerId())
			{
				loser.addAdena("Original bid", prevBid, null, false);
			}
			 */
			_bid = bid;
			_bidderId = bidderId;
		}
		
		public int getOwnerId()
		{
			return _ownerId;
		}
		
		public Integer getObjectId()
		{
			return _objectId;
		}
		
		public int getItemId()
		{
			return _itemId;
		}
		
		public int getDisplayId()
		{
			return _displayId;
		}
		
		public long getCount()
		{
			return _count;
		}
		
		public String getName()
		{
			return _name;
		}
		
		public int getState()
		{
			if (getTimeLeft() > 0)
				return 1;
			else
				return 0;
		}
		
		public int getType2()
		{
			return _type2;
		}
		
		public int getCType1()
		{
			return _cType1;
		}
		
		public int getBodypart()
		{
			return _bodypart;
		}
		
		public int getEnchant()
		{
			return _enchant;
		}
		
		public int getCType2()
		{
			return _cType2;
		}
		
		public int getAug()
		{
			return _aug;
		}
		
		public int getMana()
		{
			return _mana;
		}
		
		@Override
		public int getAttackElementPower()
		{
			return _elPow;
		}
		
		@Override
		public byte getAttackElementType()
		{
			return _elType;
		}
		
		@Override
		public int getElementDefAttr(byte element)
		{
			return _elDef[element];
		}
		
		public int getTimeRemaining()
		{
			return _remTime;
		}
	}
	
	public static class AuctionHolder
	{
		private final List<ItemReflection> _auctions;
		private final Set<Integer> _items;
		
		private AuctionHolder()
		{
			//_auctions = Collections.synchronizedList(new ArrayList<ItemReflection>());
			_auctions = new CopyOnWriteArrayList<ItemReflection>();
			if (!(_auctions instanceof RandomAccess))
				_log.warn("ItemAuctionEngine: degraded performance!");
			_items = FastSet.newInstance();
		}
		
		public List<ItemReflection> getAuctions()
		{
			return _auctions;
		}
		
		private Set<Integer> getItems()
		{
			return _items;
		}
		
		private void addAuction(ItemReflection ir)
		{
			getItems().add(ir.getObjectId());
			getAuctions().add(ir);
		}
		
		private void removeAuction(ItemReflection ir)
		{
			getItems().remove(ir.getObjectId());
			getAuctions().remove(ir);
		}
	}
}
