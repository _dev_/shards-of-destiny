/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.sod.manager;

import java.io.File;

import javax.xml.parsers.DocumentBuilderFactory;

import javolution.util.FastSet;

import org.apache.commons.lang3.ArrayUtils;
import org.sod.model.ChallengeTemplate;
import org.sod.model.PointCollectable;
import org.sod.model.RewardExpSp;
import org.sod.model.RewardItem;
import org.sod.model.RewardPerk;
import org.sod.model.RewardSkill;
import org.sod.model.Rewardable;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.l2jfree.Config;
import com.l2jfree.util.logging.L2Logger;

/**
 * TODO Auto-generated JavaDoc
 * 
 * @author savormix
 * @since 2010.01.01
 */
public final class ChallengeManager
{
	private static final L2Logger _log = L2Logger.getLogger(ChallengeManager.class);
	// Challenge categories
	public static final String GENERAL = "General";
	public static final String SUPPORT = "Support";
	public static final String PVP = "PvP";
	public static final String EVENT = "Events";
	public static final String CONQUEST = "Conquest";
	public static final String SOCIAL = "Emote";
	
	private ChallengeManager()
	{
		FastSet<Integer> valid = FastSet.newInstance();
		int count = 0;
		for (ChallengeTemplate ct : ChallengeTemplate.values())
		{
			if (ct.isEnabled())
				count++;
			if (!valid.add(ct.getId()))
				_log.warn("Duplicate challenge ID: " + ct.getId(), new IllegalArgumentException());
		}
		_log.info("ChallengeManager: loaded " + count + " challenges.");
		loadRewards();
	}
	
	public void loadRewards()
	{
		File xml = new File(Config.DATAPACK_ROOT, "data/stats/ch_rewards.xml");
		Document doc = null;
		try
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			factory.setIgnoringComments(true);
			doc = factory.newDocumentBuilder().parse(xml);
		}
		catch (Exception e)
		{
			_log.fatal("Cannot load challenge rewards!", e);
		}
		int total = 0;
		try
		{
			total = parseDocument(doc);
		}
		catch (Exception e)
		{
			_log.fatal("Cannot parse challenge rewards!", e);
		}
		_log.info("ChallengeManager: Loaded " + total + " custom challenge reward(s).");
		main(null);
	}
	
	private int parseDocument(Document doc) throws Exception
	{
		int total = 0;
		for (Node n = doc.getFirstChild(); n != null; n = n.getNextSibling())
			if ("list".equalsIgnoreCase(n.getNodeName()))
				for (Node e = n.getFirstChild(); e != null; e = e.getNextSibling())
					if ("challenge".equalsIgnoreCase(e.getNodeName()))
					{
						parseChallenge(e);
						total++;
					}
		return total;
	}
	
	public void parseChallenge(Node n) throws Exception
	{
		Node id = n.getAttributes().getNamedItem("id");
		Node desc = n.getAttributes().getNamedItem("reward_description");
		
		Rewardable[] all = Rewardable.EMPTY_ARRAY;
		for (Node reward = n.getFirstChild(); reward != null; reward = reward.getNextSibling())
		{
			if ("item".equalsIgnoreCase(reward.getNodeName()))
			{
				Node item = reward.getAttributes().getNamedItem("id");
				Node count = reward.getAttributes().getNamedItem("count");
				Rewardable re = new RewardItem(Integer.parseInt(item.getNodeValue()), Integer.parseInt(count
						.getNodeValue()));
				all = ArrayUtils.add(all, re);
			}
			else if ("skill".equalsIgnoreCase(reward.getNodeName()))
			{
				Node skill = reward.getAttributes().getNamedItem("id");
				Node level = reward.getAttributes().getNamedItem("level");
				Rewardable re = new RewardSkill(Integer.parseInt(skill.getNodeValue()), Integer.parseInt(level
						.getNodeValue()));
				all = ArrayUtils.add(all, re);
			}
			else if ("perk".equalsIgnoreCase(reward.getNodeName()))
			{
				Node perk = reward.getAttributes().getNamedItem("id");
				Node up = reward.getAttributes().getNamedItem("upgraded");
				Rewardable re = new RewardPerk(Integer.parseInt(perk.getNodeValue()), Boolean.parseBoolean(up
						.getNodeValue()));
				all = ArrayUtils.add(all, re);
			}
			else if ("expsp".equalsIgnoreCase(reward.getNodeName()))
			{
				Node xp = reward.getAttributes().getNamedItem("xp");
				Node sp = reward.getAttributes().getNamedItem("sp");
				Rewardable re = new RewardExpSp(Integer.parseInt(xp.getNodeValue()), sp != null ? Integer.parseInt(sp
						.getNodeValue()) : 0);
				all = ArrayUtils.add(all, re);
			}
		}
		
		int ch = Integer.parseInt(id.getNodeValue());
		ChallengeTemplate ct = ChallengeTemplate.getTemplate(ch);
		ct.setReward(desc.getNodeValue());
		ct.setRewards(all);
	}
	
	public static class Challenge extends PointCollectable
	{
		public Challenge(ChallengeTemplate template, int points)
		{
			super(template, points);
		}
		
		@Override
		public ChallengeTemplate getTemplate()
		{
			return (ChallengeTemplate) super.getTemplate();
		}
		
		@Override
		protected void refreshUpgraded()
		{
			int need = getTemplate().getNeededPoints();
			if (need > -1)
				super.refreshUpgraded();
			else
				setUpgraded(getPoints() == need);
		}
	}
	
	public static ChallengeManager getInstance()
	{
		return SingletonHolder._instance;
	}
	
	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final ChallengeManager _instance = new ChallengeManager();
	}
	
	public static void main(String[] args)
	{
		// remove reward validation to use this
		StringBuilder sb = new StringBuilder("<list>\r\n");
		for (ChallengeTemplate ct : ChallengeTemplate.values())
		{
			sb.append("  <challenge id=\"");
			sb.append(ct.getId());
			sb.append("\" reward_description=\"");
			sb.append(ct.getReward());
			sb.append("\"> <!-- ");
			sb.append(ct.getFullName());
			sb.append(" -->\r\n");
			for (Rewardable r : ct.getRewards())
			{
				sb.append("    ");
				if (r instanceof RewardItem)
				{
					RewardItem re = (RewardItem) r;
					sb.append("<item id=\"");
					sb.append(re.getId());
					sb.append("\" count=\"");
					sb.append(re.getCount());
					sb.append("\" />");
				}
				else if (r instanceof RewardSkill)
				{
					RewardSkill re = (RewardSkill) r;
					sb.append("<skill id=\"");
					sb.append(re.getId());
					sb.append("\" level=\"");
					sb.append(re.getLevel());
					sb.append("\" />");
				}
				else if (r instanceof RewardPerk)
				{
					RewardPerk re = (RewardPerk) r;
					sb.append("<perk id=\"");
					sb.append(re.getId());
					sb.append("\" upgraded=\"");
					sb.append(re.isUpgraded());
					sb.append("\" />");
				}
				else if (r instanceof RewardExpSp)
				{
					RewardExpSp re = (RewardExpSp) r;
					sb.append("<expsp xp=\"");
					sb.append(re.getExp());
					sb.append("\" ");
					if (re.getSp() > 0)
					{
						sb.append("sp=\"");
						sb.append(re.getSp());
						sb.append("\" ");
					}
					sb.append("/>");
				}
				sb.append("\r\n");
			}
			sb.append("  </challenge>\r\n");
		}
		sb.append("</list>");
		System.out.println(sb);
	}
}
