#!/bin/bash

err=1
until [ $err == 0 ];
do
	. ./setenv.sh
	[ -d log/ ] || mkdir log/
	[ -f log/stdout.log ] && mv log/stdout.log "log/stdout/`date +%Y-%m-%d_%H-%M-%S`_stdout.log"
# For developers mostly (1. line gc logrotate, 2. line parameters for gc logging):
#	[ -f log/gc.log ] && mv log/gc.log "log/gc/`date +%Y-%m-%d_%H-%M-%S`_gc.log"
#	-verbose:gc -Xloggc:log/gc.log -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:+PrintHeapAtGC -XX:+PrintTenuringDistribution
# Good old Solaris box
#	java -Xmn384m -Xms3g -Xmx3g -server -XX:+UseNUMA -d64 -XX:+UseCompressedOops org.sod.security.SecureLauncher 20673434a > log/stdout.log 2>&1
# Current config
# The box is auto Server x64, but include anyway
	java -Djava.net.preferIPv4Stack=true -Xmn384m -Xms3g -Xmx3g -server -d64 -XX:+UseNUMA -XX:+UseCompressedOops -XX:+UseFastAccessorMethods -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalMode -XX:+PrintGCDetails -XX:+PrintGCTimeStamps org.sod.security.SecureLauncher 1a8a36676 > log/stdout.log 2>&1
	err=$?
	sleep 10
done