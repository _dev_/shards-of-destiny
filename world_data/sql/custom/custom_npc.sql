CREATE TABLE IF NOT EXISTS `custom_npc` (
  `id` DECIMAL(11,0) NOT NULL DEFAULT 0,
  `idTemplate` INT(11) NOT NULL DEFAULT 0,
  `name` VARCHAR(200) DEFAULT NULL,
  `serverSideName` INT(1) DEFAULT 0,
  `title` VARCHAR(45) DEFAULT '',
  `serverSideTitle` INT(1) DEFAULT 0,
  `class` VARCHAR(200) DEFAULT NULL,
  `collision_radius` DECIMAL(5,2) DEFAULT NULL,
  `collision_height` DECIMAL(5,2) DEFAULT NULL,
  `level` DECIMAL(2,0) DEFAULT NULL,
  `sex` VARCHAR(6) DEFAULT NULL,
  `type` VARCHAR(21) DEFAULT NULL,
  `attackrange` INT(11) DEFAULT NULL,
  `hp` DECIMAL(8,0) DEFAULT NULL,
  `mp` DECIMAL(8,0) DEFAULT NULL,
  `hpreg` DECIMAL(8,2) DEFAULT NULL,
  `mpreg` DECIMAL(5,2) DEFAULT NULL,
  `str` DECIMAL(7,0) DEFAULT NULL,
  `con` DECIMAL(7,0) DEFAULT NULL,
  `dex` DECIMAL(7,0) DEFAULT NULL,
  `int` DECIMAL(7,0) DEFAULT NULL,
  `wit` DECIMAL(7,0) DEFAULT NULL,
  `men` DECIMAL(7,0) DEFAULT NULL,
  `exp` DECIMAL(9,0) DEFAULT NULL,
  `sp` DECIMAL(8,0) DEFAULT NULL,
  `patk` DECIMAL(5,0) DEFAULT NULL,
  `pdef` DECIMAL(5,0) DEFAULT NULL,
  `matk` DECIMAL(5,0) DEFAULT NULL,
  `mdef` DECIMAL(5,0) DEFAULT NULL,
  `atkspd` DECIMAL(3,0) DEFAULT NULL,
  `aggro` DECIMAL(6,0) DEFAULT NULL,
  `matkspd` DECIMAL(4,0) DEFAULT NULL,
  `rhand` DECIMAL(8,0) DEFAULT NULL,
  `lhand` DECIMAL(8,0) DEFAULT NULL,
  `armor` DECIMAL(1,0) DEFAULT NULL,
  `walkspd` DECIMAL(3,0) DEFAULT NULL,
  `runspd` DECIMAL(3,0) DEFAULT NULL,
  `faction_id` VARCHAR(40) DEFAULT NULL,
  `faction_range` DECIMAL(4,0) DEFAULT NULL,
  `isUndead` INT(11) DEFAULT 0,
  `absorb_level` DECIMAL(2,0) DEFAULT 0,
  `absorb_type` enum('FULL_PARTY','LAST_HIT','PARTY_ONE_RANDOM') DEFAULT 'LAST_HIT' NOT NULL,
  `ss` int(4) DEFAULT 0,
  `bss` int(4) DEFAULT 0,
  `ss_rate` INT(3) DEFAULT 0,
  `AI` VARCHAR(8) DEFAULT "fighter",
  `drop_herbs` enum('true','false') DEFAULT 'false' NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

TRUNCATE TABLE `custom_npc`;

-- testing stuff
-- INSERT INTO `custom_npc` VALUES
-- (75001, 31675, 'Faction Manager', 1, 'East', 1, 'NPC.a_royal_guardA_MHuman', 7, 24, 70, 'male', 'L2FactionManager', 40, 3862, 1493, 11.85, 2.78, 40, 43, 30, 21, 20, 10, 0, 0, 1314, 470, 780, 382, 278, 0, 333, 128, 632, 0, 50, 120, 'NULL', 0, 1, 0, 'LAST_HIT', 0, 0, 0, 'fighter', 'false'),
-- (75002, 31675, 'Faction Manager', 1, 'West', 1, 'NPC.a_royal_guardA_MHuman', 7, 24, 70, 'male', 'L2FactionManager', 40, 3862, 1493, 11.85, 2.78, 40, 43, 30, 21, 20, 10, 0, 0, 1314, 470, 780, 382, 278, 0, 333, 128, 632, 0, 50, 120, 'NULL', 0, 1, 0, 'LAST_HIT', 0, 0, 0, 'fighter', 'false');

-- SoD NPCs
INSERT INTO `custom_npc` VALUES
-- (90000, 30192, 'Katia', 1, 'NPC Buffer', 1, 'NPC.a_fighterguild_teacher_FDarkElf',8,23,70,'female', 'L2AiOBuffer', 40, 3862, 1493, 11.85, 2.78, 40, 43, 30, 21, 35, 10, 5879, 590, 1314, 470, 780, 382, 278, 0, 253, 0, 0, 0, 80, 120, '', 0, 0, 0, 'LAST_HIT', 0, 0, 0, 'balanced', 'false'),
(90001, 30010, 'Molotov', 1, 'Weapons', 1, 'NPC.a_fighterguild_teacher_MHuman',8,23,70,'male', 'L2Merchant', 40, 3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,780,382,278,0,333,161,0,0,50,120,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),
(90002, 30029, 'Olga', 1, 'Armor', 1, 'NPC.a_fighterguild_teacher_FHuman',9.5,22.4,70,'female', 'L2Merchant', 40, 3862, 1493, 11.85, 2.78, 40, 43, 30, 21, 35, 10, 5879, 590, 1314, 470, 780, 382, 278, 0, 253, 0, 0, 0, 80, 120, '', 0, 0, 0, 'LAST_HIT', 0, 0, 0, 'fighter', 'false'),
(90003, 31275, 'Osiris', 1, 'Jewels', 1, 'NPC.a_teleporter_FHuman', 8, 25, 85, 'female', 'L2Merchant', 40, 3862, 1493, 11.85, 2.78, 40, 43, 30, 21, 35, 10, 5879, 590, 1314, 470, 780, 382, 278, 0, 253, 0, 0, 0, 80, 120, '', 0, 0, 0, 'LAST_HIT', 0, 0, 0, 'balanced', 'false'),
(90004, 30633, 'Strelcov', 1, 'Miscellaneous', 1, 'NPC.a_common_peopleA_MHuman',8,21.5,70,'male', 'L2Merchant', 40, 3862, 1493, 11.85, 2.78, 40, 43, 30, 21, 35, 10, 5879, 590, 1314, 470, 780, 382, 278, 0, 253, 0, 0, 0, 80, 120, '', 0, 0, 0, 'LAST_HIT', 0, 0, 0, 'balanced', 'false'),
(90005, 32546, 'Natascha', 1, 'Event Manager', 1, 'LineageMonster4.death_scout_088p',14.4,22,70,'female', 'L2EventManager', 40, 3862, 1493, 11.75, 2.78, 40, 43, 30, 21, 35, 10, 5879, 590, 1314, 470, 780, 382, 278, 0, 253, 0, 0, 0, 80, 120, '', null, 0, 0, 'LAST_HIT', 0, 0, 0, 'balanced', 'false'),
(90006, 32319, 'Teleport Device', 1, 'Zone Manager', 1, 'LineageNpcEV.warp_gate_e', 16, 31, 85, 'male', 'L2ZoneManager', 40, 3862, 1493, 11.75, 2.78, 40, 43, 30, 21, 35, 10, 5879, 590, 1314, 470, 780, 382, 278, 0, 253, 0, 0, 0, 80, 120, '', null, 0, 0, 'LAST_HIT', 0, 0, 0, 'balanced', 'false'),
(90007, 35420, 'Veteran', 1, 'Spawn Guard', 1, 'Monster.human_ghost', 8, 22, 70, 'male', 'L2ZoneSpawnGuard', 500, 3862,1493,11.85,2.78,80,83,70,61,60,50,0,0,8000,470,9000,382,666,500,999,14568,0,0,0,0,'zone_owner_servant',1000,1,0,'LAST_HIT',9999,9999,100,'balanced','false'),
(90008, 31756, 'Drug Kochka', 1, 'Class Changer', 1, 'Monster.cat_the_cat', 9, 16, 70, 'male', 'L2ClassChanger', 40, 3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,780,382,278,0,333,0,0,0,0,0,'NULL',0,0,0,'LAST_HIT',0,0,0,'fighter','false'),
(90009, 31675, 'Zariche', 1, 'Unholy Servitor', 1, 'NPC.a_royal_guardA_MHuman', 7, 24, 70, 'male', 'L2ZoneServitor', 40, 3862, 1493, 11.85, 2.78, 99, 99, 99, 99, 99, 99, 0, 0, 1500, 470, 1780, 382, 400, 0, 333, 128, 632, 0, 150, 375, 'zone_owner_servant', 0, 1, 0, 'LAST_HIT', 9999, 9999, 100, 'fighter', 'false'),
(90010, 32534, 'Jakunin', 1, 'Bounty Manager', 1, 'LineageMonster4.disciple_silen', 19, 44, 70, 'male', 'L2BountyHunter', 40, 2444, 2444, 0, 0, 10, 10, 10, 10, 10, 10, 0, 0, 500, 500, 500, 500, 278, 0, 333, 0, 0, NULL, 73, 170, '', 0, 0, 0, 'LAST_HIT', 0, 0, 0, 'balanced', 'false'),
(90011, 32253, 'Harkilgamed', 1, 'East', 1, 'LineageNPC2.k_hero_bigsword', 11, 25, 70, 'male', 'L2Npc', 40, 2444,2444,0,0,10,10,10,10,10,10,0,0,500,500,500,500,253,0,253,9284,0,NULL,80,120,'',0,0,0,'LAST_HIT',0,0,0,'balanced','false'),
(90012, 32263, 'Rodenpicula', 1, 'West', 1, 'LineageNPC2.k_hero_bowgun', 9, 23, 70, 'male', 'L2Npc', 40, 2444, 2444,0,0,10,10,10,10,10,10,0,0,500,500,500,500,253,0,253,9300,0,NULL,80,120,'',0,0,0,'LAST_HIT',0,0,0,'balanced','false'),
(90013, 25188, 'Flying Serpent', 1, 'Follower', 1, 'Monster.lienrik', 12, 50, 80, 'male', 'L2Follower', 40, 35564, 763, 155.08, 5.72, 60, 57, 73, 76, 70, 80, 0, 0, 2315, 1423, 1925, 2229, 433, 1200, 3819, 0, 0, 0, 162, 305, 'NULL', 0, 0, 0, 'LAST_HIT', 0, 0, 0, 'balanced', 'false'),
(90014, 32602, 'Temporary Teleporter', 0, '', 0, 'LineageNpcEV.teleport_device', 16, 42, 70, 'male', 'L2Teleporter', 40, 2444, 2444, 0, 0, 10, 10, 10, 10, 10, 10, 0, 0, 500, 500, 500, 500, 278, 0, 333, 0, 0, NULL, 60, 60, '', 0, 0, 0, 'LAST_HIT', 0, 0, 0, 'balanced', 'false'),
(90015, 32602, 'Temporary Teleporter', 0, '', 0, 'LineageNpcEV.teleport_device', 16, 42, 70, 'male', 'L2Teleporter', 40, 2444, 2444, 0, 0, 10, 10, 10, 10, 10, 10, 0, 0, 500, 500, 500, 500, 278, 0, 333, 0, 0, NULL, 60, 60, '', 0, 0, 0, 'LAST_HIT', 0, 0, 0, 'balanced', 'false'),
(90016, 30086, 'Parme', 1, 'Remarketer', 1, 'NPC.a_warehouse_keeper_MDwarf', 8, 17, 70, 'male', 'L2Merchant', 40, 3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,780,382,278,0,333,0,0,0,50,120,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),
(90017, 32272, 'Zhukov', 1, 'Skill/Item upgrades', 1, 'LineageNPC.a_traderA_MDwarf', 8, 17, 70, 'male', 'L2Merchant', 40, 2444, 2444,0,0,10,10,10,10,10,10,0,0,500,500,500,500,253,0,253,0,0,NULL,80,120,'',0,0,0,'LAST_HIT',0,0,0,'balanced','false'),
(90018, 32635, 'Saerom', 1, 'Masterwork', 1, 'LineageNPC2.event_comic_masteryogy', 9, 16.7, 70, 'male', 'L2Merchant', 40, 2444,2444,0,0,10,10,10,10,10,10,0,0,500,500,500,500,278,0,333,0,0,NULL,60,60,'',0,0,0,'LAST_HIT',0,0,0,'balanced','false'),
(90019, 35102, 'Greenspan', 0, 'Mercenary Manager', 0, 'NPC.a_common_peopleA_MHuman', 8, 21.5, 70, 'male', 'L2Merchant',40,3862,1493,11.85,2.78,40,43,30,21,20,10,490,10,1335,470,780,382,278,0,333,0,0,0,50,120,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),
(90020, 35437, 'Sylanth', 1, 'Raid Manager', 1, 'Monster.oel_mahum', 20, 37, 56, 'male', 'L2RaidTeleporter',40,2725,1019,10.27,2.45,40,43,30,21,20,10,313,10,706,331,382,268,282,0,333,0,0,0,77,132,'NULL',600,0,0,'LAST_HIT',0,0,0,'fighter','false'),
(90021, 35422, 'Flag',0,'',0,'Deco.azit_a',100,72,56,'etc','L2Npc',40,2725,1019,10.27,2.45,40,43,30,21,20,10,313,10,706,331,382,268,282,0,333,0,0,0,55,132,'dion_siege_clan',600,1,0,'LAST_HIT',0,0,0,'fighter','false'),
(90022, 30734, 'Martyr', 0, '', 0, 'NPC.box', 18,14,70,'etc','L2Martyr',40,3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,1780,382,278,0,333,0,0,0,55,132,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),
(90023, 30699, 'Historian', 1, 'Noblesse Manager', 1, 'NPC.a_guild_master_MDarkElf', 8,23,70,'male','L2Npc',40,3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,780,382,278,0,333,0,0,0,50,120,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),
(90024, 30734, 'Tank Player B', 0, '', 0, 'NPC.box', 18,14,70,'etc','Advertiser',40,3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,1780,382,278,0,333,0,0,0,55,132,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),
(90025, 30734, 'Mage Player B', 0, '', 0, 'NPC.box', 18,14,70,'etc','Advertiser',40,3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,1780,382,278,0,333,0,0,0,55,132,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),
(90026, 30734, 'Any Player B', 0, '', 0, 'NPC.box', 18,14,70,'etc','Advertiser',40,3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,1780,382,278,0,333,0,0,0,55,132,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),
(90027, 30734, 'Tank Player A', 0, '', 0, 'NPC.box', 18,14,70,'etc','Advertiser',40,3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,1780,382,278,0,333,0,0,0,55,132,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),
(90028, 30734, 'Mage Player A', 0, '', 0, 'NPC.box', 18,14,70,'etc','Advertiser',40,3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,1780,382,278,0,333,0,0,0,55,132,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),
(90029, 30734, 'Any Player A', 0, '', 0, 'NPC.box', 18,14,70,'etc','Advertiser',40,3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,1780,382,278,0,333,0,0,0,55,132,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),
(90030, 30734, 'Ranger Player B', 0, '', 0, 'NPC.box', 18,14,70,'etc','Advertiser',40,3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,1780,382,278,0,333,0,0,0,55,132,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),
(90031, 30734, 'Ranger Player A', 0, '', 0, 'NPC.box', 18,14,70,'etc','Advertiser',40,3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,1780,382,278,0,333,0,0,0,55,132,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),
(90032, 31645, 'Venn', 1, 'Coordinator', 1, 'NPC.a_traderD_Mhuman', 8,25.3,70,'male','L2Npc',40,3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,780,382,278,0,333,195,0,0,55,132,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),
(90033, 31364, 'Ugmak', 1, 'Teleporter', 1, 'NPC.e_guard_MOrc',8,27,70,'male','L2Npc',40,3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,780,382,278,0,333,203,0,0,50,120,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),
(90034, 31439, 'Meriik', 1, 'Trader', 1, 'NPC.e_collector_teacher_MDwarf',8,17.5,70,'male','L2Merchant',40,3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,780,382,278,0,333,0,0,0,50,120,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),
(90035, 31453, 'Ghost of Past', 1, '', 1, 'NPC.a_king_MHuman',8,21,70,'male','L2Npc',40,3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,780,382,278,0,333,0,0,0,55,132,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),
(90036, 20520, 'Pirate Captain Uthanka', 0, '', 0, 'Monster.orc_fighter_bi', 12, 26.4, 80, 'male','L2Monster',40,25700,1200,600.32,10.91,60, 57, 73, 76, 70, 80,1000000,370370,1100,1400,900,1500,278,500,333,2,0,0,49,200,'orc_clan',300,0,0,'LAST_HIT',0,0,0,'fighter','false'),
(90037, 20445, 'Uthanka Pirate', 0, '', 0, 'Monster.orc', 10, 21, 80, 'male', 'L2Minion', 40, 17100,940,100.58,10.91,40,43,30,21,20,10,0,0,700,800,600,430,278,0,333,3,0,0,49,202,'orc_clan',300,0,0,'LAST_HIT',0,0,0,'fighter','false'),

-- Clean Nearby Lands
-- Area 1
(90038,20862,'Death Lord',0,'',0,'Monster.death_lord',21,40,75,'male','L2Monster',40,4229,1673,13.43,3.09,40,43,30,21,20,10,7774,820,1720,475,957,425,278,0,333,78,0,0,44,191,'demonic_clan',0,1,0,'LAST_HIT',100,0,30,'fighter','false'),
(90039,21650,'Shrine Knight',0,'',0,'Monster.skeleton_knight',10,25,75,'male','L2Monster',40,4229,1673,13.43,3.09,40,43,30,21,20,10,10149,1071,1563,523,957,425,278,0,333,150,103,0,44,181,'NULL',0,1,0,'LAST_HIT',100,0,30,'fighter','false'),
(90040,21089,'Bloody Lord',0,'',0,'Monster.karik',25,90,75,'male','L2Monster',40,4229,1673,67.15,3.09,40,43,30,21,20,10,6817,719,1720,475,957,425,278,0,333,0,0,0,40,191,'bloody_clan',400,0,0,'LAST_HIT',100,0,30,'fighter','false'),
(90041,21184,'Lilim Slayer',0,'',0,'Monster2.lilim_assassin_10_bi',13,44,75,'male','L2Monster',40,4229,1673,53.72,3.09,40,43,30,21,20,10,8869,935,1655,367,957,425,317,0,333,236,236,0,36,200,'c_dungeon_lilim',400,0,0,'LAST_HIT',0,0,0,'fighter','false'),
(90042,21308,'Disciples of Punishment',0,'',0,'Monster2.apostle_warrior',8,30,75,'female','L2Monster',40,4229,1673,67.15,3.09,40,43,30,21,20,10,7860,829,2389,475,957,425,200,0,333,6717,0,0,55,209,'divine_clan',600,1,0,'LAST_HIT',200,0,30,'fighter','false'),
(90043,21307,'Elder Homunculus',0,'',0,'Monster2.homunculus',12,32.5,75,'male','L2Monster',40,4229,1673,53.72,3.09,40,43,30,21,20,10,7394,780,1892,367,957,425,278,0,333,0,0,0,33,220,'eye_clan',400,0,0,'LAST_HIT',100,0,30,'fighter','false'),
(90044,20972,'Shaman of Ancient Times',0,'',0,'Monster.lesser_giant_mage',21,44,75,'male','L2Monster',40,4229,1673,6.72,3.09,40,43,30,21,20,10,10970,1158,1563,445,957,425,278,0,333,3938,0,0,16,214,'NULL',0,0,0,'LAST_HIT',0,100,30,'mage','false'),
-- Area 2
(90045,21328,'Ketra Orc Scout',0,'',0,'Monster.ketra_orc_archer',12,27,78,'male','L2Monster',500,4428,1784,26.86,3.09,40,43,30,21,20,10,9816,1074,3464,611,1069,451,249,0,333,288,0,0,49,192,'ketra',300,0,0,'LAST_HIT',100,0,30,'archer','false'),
(90046,21371,'Varka\'s Head Magus',0,'',0,'Monster2.barka_silenos_shaman',14,39.5,78,'male','L2Monster',40,4813,2049,26.86,3.09,40,43,30,21,20,10,12360,1471,3136,572,1333,511,200,0,333,6716,0,0,39,192,'varka',300,0,0,'LAST_HIT',0,100,50,'balanced','false'),
(90047,21185,'Lilim Great Mystic',0,'',0,'Monster2.lilim_wizard_10_bi',10,37.5,78,'female','L2Monster',40,4428,1784,53.72,3.09,40,43,30,21,20,10,7305,799,2075,459,1069,451,278,0,333,0,0,0,50,180,'c_dungeon_lilim',400,0,0,'LAST_HIT',0,0,0,'mage','false'),
(90048,21165,'Lesser Ancient Warrior',0,'',0,'Monster.shadeless',7,28,78,'male','L2Monster',40,4550,1859,53.72,3.09,40,43,30,21,20,10,7761,871,2195,476,1144,468,278,0,333,0,0,0,48,200,'c_dungeon_clan',300,0,0,'LAST_HIT',0,0,0,'fighter','false'),
(90049,21393,'Magma Drake',0,'',0,'Monster2.inferno_drake_20_bi',29,42,78,'male','L2Monster',40,4813,2049,80.58,3.09,40,43,30,21,20,10,9494,1130,2257,572,1333,511,278,0,333,0,0,0,62,214,'fire_clan',500,0,0,'LAST_HIT',0,100,30,'balanced','false'),
-- Area 3
(90050,22223,'Velociraptor',0,'',0,'Monster3.velociraptor',19,47.2,80,'male','L2Monster',80,14019,2296,13.43,3.09,40,43,30,21,20,10,34684,3755,2199,625,1373,521,278,0,333,0,0,0,88,188,'dino',500,0,0,'LAST_HIT',0,0,0,'fighter','false'),
(90051,22224,'Ornithomimus',0,'',0,'Monster3.ornithomimus',15,37.2,80,'male','L2Monster',80,13858,2211,13.43,3.09,40,43,30,21,20,10,32698,3456,2298,612,1296,502,278,0,333,0,0,0,36,170,'dino',500,0,0,'LAST_HIT',0,0,0,'fighter','false'),
(90052,22225,'Deinonychus',0,'',0,'Monster3.deinonychus',14,14.3,80,'male','L2Monster',80,13763,2168,13.43,3.09,40,43,30,21,20,10,30343,3187,2695,571,1258,494,278,0,333,0,0,0,24,160,'dino',500,0,0,'LAST_HIT',0,0,0,'fighter','false'),
(90053,22226,'Pachycephalosaurus',0,'',0,'Monster3.pachycephalosaurus',22,40,80,'male','L2Monster',80,13953,2253,13.43,3.09,40,43,30,21,20,10,35125,3733,2129,615,1333,511,278,0,333,0,0,0,33,140,'dino',500,0,0,'LAST_HIT',0,0,0,'fighter','false'),
(90054,22227,'Wild Strider',0,'',0,'Monster3.wild_giant_strider',32,44.2,80,'male','L2Monster',80,13953,2253,13.43,3.09,40,43,30,21,20,10,34440,3682,2129,615,1333,511,278,0,333,0,0,0,50,140,'dino',500,0,0,'LAST_HIT',0,0,0,'fighter','false'),
-- Area 4
(90055,22399,'Greater Evil',0,'',0,'LineageMonster3.zombie_gateguard',23,38,85,'male','L2Monster',40,25719,3546,33.11,10.2,67,68,69,61,67,61,66586,0,2995,900,2112,800,333,0,456,9449,0,0,0,192,'',0,1,0,'LAST_HIT',0,0,0,'balanced','false'),
(90056,22353,'Celtus',0,'Chimera',0,'LineageMonster4.Chimera',35,36,84,'male','L2Monster',40,106195,2479,33.11,9.81,55,46,66,41,42,43,0,0,2554,950,1989,910,353,0,453,0,0,0,40,201,'CHIMERA_CLAN',500,0,0,'LAST_HIT',0,0,0,'balanced','false'),
(90057,22280,'Rodo Knight',0,'',0,'LineageMonster4.Crystal_Golem',40,47,82,'male','L2Monster',40,4664,1664,26.86,3.09,45,44,45,21,20,10,7968,830,2249,800,1220,900,364,0,431,0,0,0,49,192,NULL,0,0,0,'LAST_HIT',0,0,0,'fighter','false'),
(90058,22266,'Pythia',0,'',0,'LineageMonster4.tears',20,28,83,'male','L2Monster',40,4716,2154,26.86,3.09,45,44,46,21,25,25,13801,1489,2249,736,1258,494,278,0,560,9640,9638,0,60,180,'water_clan',400,0,0,'LAST_HIT',50,50,75,'balanced','false');

UPDATE `custom_npc` SET `sp` = 0, `faction_id` = '', `faction_range` = 0 WHERE `id` BETWEEN 90038 AND 90058;

INSERT INTO `custom_npc` VALUES
(90059, 30734, 'Master', 1, 'Caravan', 1, 'NPC.box', 18,14,70,'etc','L2CaravanLeader',40,386200,1493,110.85,2.78,60,57,73,76,70,80,0,0,1314,470,1780,1382,278,0,333,0,0,0,100,180,'caravan_clan',900,1,0,'LAST_HIT',0,0,0,'fighter','false'),
(90060, 30734, 'Carrier', 1, 'Caravan', 1, 'NPC.box', 18,14,70,'etc','L2CaravanMule',40,38620,1493,110.85,2.78,40,43,30,21,20,10,0,0,1314,470,1780,1382,278,0,333,0,0,0,120,180,'caravan_clan',300,1,0,'LAST_HIT',0,0,0,'fighter','false'),
(90061, 30502, 'Tracker', 1, 'Caravan', 1, 'NPC.e_clergyman_MOrc',8,29,70,'male','L2Npc',40,3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,780,382,278,0,333,0,0,0,50,120,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),
(90062, 31273, 'Vaclov', 1, 'Recipes', 1, 'NPC.e_mine_teacher_MDwarf',8,18,70,'male','L2Merchant',40,3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,780,382,278,0,333,0,0,0,50,120,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),
(90063, 20001, 'Gremlin', 0, '', 0, 'Monster.gremlin',10,15,1,'male','L2GremlinBall',40,62,44,3.16,0.91,40,43,30,21,20,10,29,2,9,39,3,32,278,0,333,0,0,0,22,90,'NULL',0,0,0,'LAST_HIT',0,0,0,'fighter','false'),
(90064, 18601, 'Red Marker', 1, 'Scoring Zone', 1, 'LineageNpcEV.trap_ironcastle_150p_a',18,32,70,'male','L2StaticNpc',40,2444,2444,0,0,10,10,10,10,10,10,0,0,500,500,500,500,278,0,333,0,0,NULL,60,60,'',0,0,0,'LAST_HIT',0,0,0,'balanced','false'),
(90065, 18602, 'Blue Marker', 1, 'Scoring Zone', 1, 'LineageNpcEV.trap_ironcastle_150p',18,32,70,'male','L2StaticNpc',40,2444,2444,0,0,10,10,10,10,10,10,0,0,500,500,500,500,278,0,333,0,0,NULL,60,60,'',0,0,0,'LAST_HIT',0,0,0,'balanced','false'),
(90066, 13162, 'Golden Golem', 1, '',1,'LineageNpc2.tin_golem',13,18.5,80,'male','L2Monster',40,8664,2444,110.85,2.78,60,57,73,76,70,80,0,0,3136,2572,1333,3511,278,0,333,0,0,NULL,60,180,'',0,0,0,'LAST_HIT',0,0,0,'fighter','false'),
(90067, 18369, 'Golem Warrior', 1, 'Purple',1,'LineageMonster4.Crystal_Golem',40.00,47.00,82,'male','L2Monster',40,24664,1935,260.86,3.09,60,57,73,76,70,80,0,0,3896,2598,883,5486,553,0,333,0,0,NULL,80,210,'',0,0,0,'LAST_HIT',0,0,0,'fighter','false'),
(90068, 18370, 'Golem Warrior', 1, 'Orange',1,'LineageMonster4.Crystal_Golem',40.00,47.00,82,'male','L2Monster',40,24664,1935,260.86,3.09,60,57,73,76,70,80,0,0,3896,2598,883,5486,553,0,333,0,0,NULL,80,210,'',0,0,0,'LAST_HIT',0,0,0,'fighter','false'),
(90069, 30734, 'Fenced Area', 1, '', 1, 'NPC.box', 18,14,70,'etc','L2Fence',40,3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,1780,382,278,0,333,0,0,0,55,132,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),

(90070, 32144, 'Xenia', 1, 'Skill Enchanter', 1, 'LineageNPC2.K_F1_master',12,23,70,'female','L2Trainer',40,2444,2444,0,0,10,10,10,10,10,10,0,0,500,500,500,500,278,0,333,9644,0,NULL,24,120,'',0,0,0,'LAST_HIT',0,0,0,'balanced','false'),
(90071, 31316, 'Ivanov', 1, 'Augmenter', 1, 'NPC.a_smith_MDwarf',7,16.5,70,'male','L2Trainer',40,3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,780,382,278,0,333,89,0,0,50,120,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),
(90072, 31953, 'Dimitrij', 1, 'Symbol Maker', 1, 'NPC.a_mageguild_teacher_MHuman',8,23,70,'male','L2SymbolMaker',40,3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,780,382,278,0,333,0,0,0,50,120,'NULL',0,0,0,'LAST_HIT',0,0,0,'mage','false'),
(90073, 32213, 'Aleksej', 1, 'Subclass/Clan/Ally', 1, 'LineageNPC2.K_M1_grand',13,25,70,'male','L2VillageMaster',40,2444,2444,0,0,10,10,10,10,10,10,0,0,500,500,500,500,278,0,333,9646,0,NULL,28,120,'',0,0,0,'LAST_HIT',0,0,0,'balanced','false'),
-- (90074, 31688, 'Grand Olympiad Manager', 0, '', 0, 'NPC.a_fighterguild_master_Mhuman',8,23.5,70,'male','L2OlympiadManager',40,3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,780,382,278,0,333,70,0,0,50,120,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),
-- (90075, 31690, 'Monument of Heroes', 0, '', 0, 'NPC.heroes_obelisk_human',24,69,70,'etc','L2OlympiadManager',40,3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,780,382,278,0,333,0,0,0,50,120,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),

(90076,25634,'Monster',1,'',1,'LineageMonster4.lich_A_132p',31.68,60.72,84,'male','L2ImperviousNpc',40,1038381,2444,0,0,10,10,10,10,10,10,0,0,500,500,500,500,253,0,253,0,0,NULL,80,120,'',0,0,0,'LAST_HIT',0,0,0,'balanced','false'),
(90077,25636,'Monster',1,'',0,'LineageMonster4.soul_wagon_130p',70,44.2,84,'male','L2ImperviousNpc',40,2444,2444,0,0,10,10,10,10,10,10,0,0,500,500,500,500,253,0,253,0,0,NULL,80,120,'',0,0,0,'LAST_HIT',0,0,0,'balanced','false'),
(90078,27077,'Monster',1,'',1,'Monster.unicorn',14,30,30,'male','L2ImperviousNpc',40,954,365,5.53,1.53,40,43,30,21,20,10,0,0,145,128,58,114,278,0,333,0,0,0,50,165,'NULL',0,0,0,'LAST_HIT',0,0,0,'fighter','false'),
(90079,27255,'Monster',1,'',1,'Monster.fiend_archer',17,55,81,'male','L2ImperviousNpc',500,41478,1896,120.87,3.09,40,43,30,21,20,10,0,0,4141,499,1182,477,249,150,333,0,0,0,77,198,'NULL',0,0,0,'LAST_HIT',0,0,0,'archer','false'),
(90080,27284,'Monster',1,'',1,'Monster2.ifrit',10,42,81,'male','L2ImperviousNpc',40,41478,1896,120.87,3.09,40,43,30,21,20,10,0,0,1863,587,1182,477,278,150,333,0,0,0,41,198,'NULL',0,0,0,'LAST_HIT',0,0,0,'fighter','false'),
(90081,27285,'Monster',1,'',1,'Monster.serpent_slave',17,46,81,'male','L2ImperviousNpc',40,41478,1896,120.87,3.09,40,43,30,21,20,10,0,0,1863,587,1182,477,278,150,333,72,0,0,77,198,'NULL',0,0,0,'LAST_HIT',0,0,0,'fighter','false'),
(90082,27311,'Monster',1,'',1,'Monster2.eligor',21,35,81,'male','L2ImperviousNpc',40,41478,1896,120.87,3.09,40,43,30,21,20,10,0,0,1863,587,1182,477,278,150,333,0,0,0,38,198,'NULL',0,0,0,'LAST_HIT',0,0,0,'fighter','false'),
(90083,27325,'Monster',1,'',1,'LineageMonster2.ahrimanes',14,58,76,'male','L2ImperviousNpc',40,4297,1710,13.43,3.09,40,43,30,21,20,10,0,0,2242,534,994,433,200,150,333,6722,0,0,38,198,'NULL',0,0,0,'LAST_HIT',0,0,0,'fighter','false'),
(90084,29129,'Monster',1,'',0,'LineageMonster.oel_mahum_champion_05te_120p',48,73.5,29,'male','L2ImperviousNpc',40,2444,2444,0,0,10,10,10,10,10,10,0,0,500,500,500,500,278,0,333,0,0,NULL,60,60,'',0,0,0,'LAST_HIT',0,0,0,'balanced','false'),
(90085,29135,'Monster',1,'',0,'LineageMonster3.Steel_Trapper_120p',45,56,49,'male','L2ImperviousNpc',40,2444,2444,0,0,10,10,10,10,10,10,0,0,500,500,500,500,278,0,333,0,0,NULL,60,60,'',0,0,0,'LAST_HIT',0,0,0,'balanced','false'),
(90086,29136,'Monster',1,'',0,'LineageMonster3.golem_cannon_catapult_a',23.5,34.7,49,'male','L2ImperviousNpc',40,2444,2444,0,0,10,10,10,10,10,10,0,0,500,500,500,500,278,0,333,0,0,NULL,60,60,'',0,0,0,'LAST_HIT',0,0,0,'balanced','false'),
(90087,29137,'Monster',1,'',0,'LineageMonster3.golem_repair',23,32.79,49,'male','L2ImperviousNpc',40,2444,2444,0,0,10,10,10,10,10,10,0,0,500,500,500,500,278,0,333,0,0,NULL,60,60,'',0,0,0,'LAST_HIT',0,0,0,'balanced','false'),
(90088,29143,'Monster',1,'',0,'LineageMonster4.eyeless',36,43.5,69,'male','L2ImperviousNpc',40,2444,2444,0,0,10,10,10,10,10,10,0,0,500,500,500,500,278,0,333,0,0,NULL,60,60,'',0,0,0,'LAST_HIT',0,0,0,'balanced','false'),
(90089,29151,'Monster',1,'',0,'LineageMonster4.voidhound_080p',64,48.42,85,'male','L2ImperviousNpc',40,6689,2444,0,0,10,10,10,10,10,10,0,0,500,500,500,500,278,0,333,0,0,NULL,60,60,'',0,0,0,'LAST_HIT',0,0,0,'balanced','false'),
(90090,35234,'Stalin',1,'King of Aden',1,'NPC.a_lord_MHuman',7,23,95,'male','L2Monster',40,845820,6694,500.43,80.09,60,57,73,76,70,80,0,0,9383,2523,957,4425,278,0,333,80,0,0,55,132,'',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),

(90091,32636,'Nadia',1,'Private Shop Area',1,'LineageNPC.e_smith_Fdwarf',8,17.5,70,'female','L2BaseTeleporter',40,2444,2444,0,0,10,10,10,10,10,10,0,0,500,500,500,500,278,0,333,0,0,NULL,60,60,'',0,0,0,'LAST_HIT',0,0,0,'balanced','false'),
(90092,13089,'Katia',1,'NPC Buffer',1,'LineageMonster4.Specular_Form_F',10,32.76,1,'female','L2TemplateBuffer',40,2444,2444,0,0,10,10,10,10,10,10,0,0,500,500,500,500,253,0,253,0,0,NULL,80,120,'',0,0,0,'LAST_HIT',0,0,0,'balanced','false'),
(90093, 32546, 'Natascha', 1, 'Event Manager', 1, 'LineageMonster4.death_scout_088p',14.4,22,70,'female', 'L2FakeManager', 40, 3862, 1493, 11.75, 2.78, 40, 43, 30, 21, 35, 10, 5879, 590, 1314, 470, 780, 382, 278, 0, 253, 0, 0, 0, 80, 120, '', null, 0, 0, 'LAST_HIT', 0, 0, 0, 'balanced', 'false'),
(90094,30767,'Auctioneer',0,'Item Auction',1,'NPC.a_maidB_FHuman',8,22,70,'female','L2Npc',40,3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,780,382,278,0,333,0,0,0,50,120,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false'),
(90095,30834,'Cema',0,'Perk Manager',1,'NPC.a_hardins_pupil_FHuman',8,23,70,'female','L2Npc',40,3862,1493,11.85,2.78,40,43,30,21,20,10,0,0,1314,470,780,382,278,0,333,0,0,0,50,120,'NULL',0,1,0,'LAST_HIT',0,0,0,'fighter','false');
