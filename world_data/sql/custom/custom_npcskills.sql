CREATE TABLE IF NOT EXISTS `custom_npcskills` (
  `npcid` INT(11) NOT NULL DEFAULT 0,
  `skillid` INT(11) NOT NULL DEFAULT 0,
  `level` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`npcid`,`skillid`,`level`)
) DEFAULT CHARSET=utf8;

TRUNCATE TABLE `custom_npcskills`;

-- Unholy servitor
INSERT INTO `custom_npcskills` VALUES
(90009,3330,1),
(90009,3331,1),
(90009,3630,1),
(90009,3631,1),
(90009,3329,1);

-- Zone guard resistance
INSERT INTO `custom_npcskills` SELECT `id`, 9117, 1 FROM `npc` WHERE `type` = 'L2ZoneGuard' OR `type` = 'L2ZoneHireling';

-- PvE quest area 1
INSERT INTO `custom_npcskills` (`npcid`,`skillid`,`level`) SELECT 90038, `skillid`, `level` FROM `npcskills` WHERE `npcId` = 20862;
INSERT INTO `custom_npcskills` (`npcid`,`skillid`,`level`) SELECT 90039, `skillid`, `level` FROM `npcskills` WHERE `npcId` = 21650;
INSERT INTO `custom_npcskills` (`npcid`,`skillid`,`level`) SELECT 90040, `skillid`, `level` FROM `npcskills` WHERE `npcId` = 21089;
INSERT INTO `custom_npcskills` (`npcid`,`skillid`,`level`) SELECT 90041, `skillid`, `level` FROM `npcskills` WHERE `npcId` = 21184;
INSERT INTO `custom_npcskills` (`npcid`,`skillid`,`level`) SELECT 90042, `skillid`, `level` FROM `npcskills` WHERE `npcId` = 21308;
INSERT INTO `custom_npcskills` (`npcid`,`skillid`,`level`) SELECT 90043, `skillid`, `level` FROM `npcskills` WHERE `npcId` = 21307;
INSERT INTO `custom_npcskills` (`npcid`,`skillid`,`level`) SELECT 90044, `skillid`, `level` FROM `npcskills` WHERE `npcId` = 20972;
-- PvE quest area 2
INSERT INTO `custom_npcskills` (`npcid`,`skillid`,`level`) SELECT 90045, `skillid`, `level` FROM `npcskills` WHERE `npcId` = 21328;
INSERT INTO `custom_npcskills` (`npcid`,`skillid`,`level`) SELECT 90046, `skillid`, `level` FROM `npcskills` WHERE `npcId` = 21371;
INSERT INTO `custom_npcskills` (`npcid`,`skillid`,`level`) SELECT 90047, `skillid`, `level` FROM `npcskills` WHERE `npcId` = 21185;
INSERT INTO `custom_npcskills` (`npcid`,`skillid`,`level`) SELECT 90048, `skillid`, `level` FROM `npcskills` WHERE `npcId` = 21165;
INSERT INTO `custom_npcskills` (`npcid`,`skillid`,`level`) SELECT 90049, `skillid`, `level` FROM `npcskills` WHERE `npcId` = 21393;
-- PvE quest area 3
INSERT INTO `custom_npcskills` (`npcid`,`skillid`,`level`) SELECT 90050, `skillid`, `level` FROM `npcskills` WHERE `npcId` = 22223;
INSERT INTO `custom_npcskills` (`npcid`,`skillid`,`level`) SELECT 90051, `skillid`, `level` FROM `npcskills` WHERE `npcId` = 22224;
INSERT INTO `custom_npcskills` (`npcid`,`skillid`,`level`) SELECT 90052, `skillid`, `level` FROM `npcskills` WHERE `npcId` = 22225;
INSERT INTO `custom_npcskills` (`npcid`,`skillid`,`level`) SELECT 90053, `skillid`, `level` FROM `npcskills` WHERE `npcId` = 22226;
INSERT INTO `custom_npcskills` (`npcid`,`skillid`,`level`) SELECT 90054, `skillid`, `level` FROM `npcskills` WHERE `npcId` = 22227;
-- PvE quest area 4
INSERT INTO `custom_npcskills` (`npcid`,`skillid`,`level`) SELECT 90055, `skillid`, `level` FROM `npcskills` WHERE `npcId` = 22399;
INSERT INTO `custom_npcskills` (`npcid`,`skillid`,`level`) SELECT 90056, `skillid`, `level` FROM `npcskills` WHERE `npcId` = 22353;
INSERT INTO `custom_npcskills` (`npcid`,`skillid`,`level`) SELECT 90057, `skillid`, `level` FROM `npcskills` WHERE `npcId` = 22280;
INSERT INTO `custom_npcskills` (`npcid`,`skillid`,`level`) SELECT 90058, `skillid`, `level` FROM `npcskills` WHERE `npcId` = 22266;
