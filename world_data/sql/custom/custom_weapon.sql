CREATE TABLE IF NOT EXISTS `custom_weapon` (
  `item_id` MEDIUMINT UNSIGNED NOT NULL,
  `item_display_id` MEDIUMINT UNSIGNED NOT NULL,
  `name` VARCHAR(120) NOT NULL,
  `bodypart` VARCHAR(15) NOT NULL DEFAULT '',
  `crystallizable` VARCHAR(5) NOT NULL DEFAULT 'false',
  `weight` MEDIUMINT(5) NOT NULL DEFAULT 0,
  `soulshots` TINYINT(2) NOT NULL DEFAULT 0,
  `spiritshots` TINYINT(2) NOT NULL DEFAULT 0,
  `material` VARCHAR(15) NOT NULL DEFAULT 'wood',
  `crystal_type` VARCHAR(4) NOT NULL DEFAULT 'none',
  `p_dam` SMALLINT(3) NOT NULL DEFAULT 0,
  `rnd_dam` TINYINT(2) NOT NULL DEFAULT 0,
  `weaponType` VARCHAR(10) NOT NULL DEFAULT '',
  `critical` TINYINT(2) NOT NULL DEFAULT 0,
  `hit_modify` TINYINT(1) NOT NULL DEFAULT 0,
  `avoid_modify` TINYINT(1) NOT NULL DEFAULT 0,
  `shield_def` SMALLINT(3) NOT NULL DEFAULT 0,
  `shield_def_rate` TINYINT(2) NOT NULL DEFAULT 0,
  `atk_speed` SMALLINT(3) NOT NULL DEFAULT 0,
  `mp_consume` TINYINT(2) NOT NULL DEFAULT 0,
  `m_dam` SMALLINT(3) NOT NULL DEFAULT 0,
  `duration` SMALLINT(3) NOT NULL DEFAULT -1,           -- duration in minutes for shadow items
  `time` MEDIUMINT(6) NOT NULL DEFAULT -1,              -- duration in minutes for time limited items
  `price` INT UNSIGNED NOT NULL DEFAULT 0,
  `crystal_count` SMALLINT(4) UNSIGNED NOT NULL DEFAULT 0,
  `sellable` VARCHAR(5) NOT NULL DEFAULT 'false',
  `dropable` VARCHAR(5) NOT NULL DEFAULT 'false',
  `destroyable` VARCHAR(5) NOT NULL DEFAULT 'true',
  `tradeable` VARCHAR(5) NOT NULL DEFAULT 'false',
  `depositable` VARCHAR(5) NOT NULL default 'false',
  `enchant4_skill` VARCHAR(70) NOT NULL DEFAULT '',
  `skills_onCast` VARCHAR(70) NOT NULL DEFAULT '',
  `skills_onCrit` VARCHAR(70) NOT NULL DEFAULT '',
  `change_weaponId` MEDIUMINT(5) NOT NULL DEFAULT 0,
  `skills_item` VARCHAR(70) NOT NULL DEFAULT '',
  PRIMARY KEY (`item_id`)
) DEFAULT CHARSET=utf8;

TRUNCATE TABLE `custom_weapon`;

INSERT INTO `custom_weapon` VALUES
(91001,9819,'Combat flag','lrhand','false',500,1,1,'wood','none',0,0,'none',0,0,0,0,0,325,0,0,-1,-1,0,0,'false','false','false','false','false','','','',0,'3358-1'),
(91002,14098,'Tin Golem Transformation Stick (event) - 7-day Limited Period','rhand','false',0,0,0,'fine_steel','none',1,10,'sword',8,0,0,0,0,379,0,1,-1,10080,0,0,'false','false','false','false','false','','','',0,'8265-1'),
(91003,20270,'Baguette\'s Dualsword - 7 Day Expiration Period','lrhand','false',500,3,3,'fine_steel','none',1,10,'dual',8,0,0,0,0,325,0,2,-1,10080,0,0,'false','false','false','false','false','','','',0,'21042-1');
