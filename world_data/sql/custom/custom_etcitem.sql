CREATE TABLE IF NOT EXISTS `custom_etcitem` (
  `item_id` MEDIUMINT UNSIGNED NOT NULL,
  `item_display_id` MEDIUMINT UNSIGNED NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `crystallizable` VARCHAR(5) NOT NULL DEFAULT 'false',
  `item_type` VARCHAR(14) NOT NULL DEFAULT 'none',
  `weight` MEDIUMINT(5) NOT NULL DEFAULT 0,
  `consume_type` VARCHAR(9) NOT NULL DEFAULT 'normal',
  `material` VARCHAR(15) NOT NULL DEFAULT 'wood',
  `crystal_type` VARCHAR(4) NOT NULL DEFAULT 'none',
  `duration` SMALLINT(3) NOT NULL DEFAULT -1,           -- duration in minutes for shadow items
  `time` MEDIUMINT(6) NOT NULL DEFAULT -1,              -- duration in minutes for time limited items
  `price` INT UNSIGNED NOT NULL DEFAULT 0,
  `crystal_count` SMALLINT(4) UNSIGNED NOT NULL DEFAULT 0,
  `sellable` VARCHAR(5) NOT NULL DEFAULT 'false',
  `dropable` VARCHAR(5) NOT NULL DEFAULT 'false',
  `destroyable` VARCHAR(5) NOT NULL DEFAULT 'true',
  `tradeable` VARCHAR(5) NOT NULL DEFAULT 'false',
  `depositable` VARCHAR(5) NOT NULL default 'false',
  `handler` VARCHAR(70) NOT NULL DEFAULT 'none',
  `skills_item` VARCHAR(70) NOT NULL DEFAULT '',
  PRIMARY KEY (`item_id`)
) DEFAULT CHARSET=utf8;

TRUNCATE TABLE `custom_etcitem`;

INSERT INTO `custom_etcitem` VALUES
(90001,20630,'Scavenged Force','false','none',0,'stackable','paper','none',-1,-1,0,0,'false','true','true','false','false','Scavenged',''),
(90002,6496,'Jade Treasure Chest','false','none',100,'stackable','paper','none',-1,-1,315,0,'true','true','true','true','true','LootedMats',''),
(90003,6514,'White Treasure Chest','false','none',100,'stackable','paper','none',-1,-1,1923,0,'true','true','true','true','true','LootedParts',''),
(90004,6511,'Red Treasure Chest','false','none',100,'stackable','paper','none',-1,-1,1402,0,'true','true','true','true','true','LootedRecipes',''),
(90005,10423,'Suicidal Grenade','false','none',0,'stackable','paper','none',-1,-1,0,0,'false','true','true','false','false','SuicideGrenade',''),
(90006,4037,'Coin of Luck','false','none',0,'stackable','paper','none',-1,-1,1,0,'false','false','true','false','false','none','');
