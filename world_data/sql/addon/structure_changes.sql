-- Table structure changes
-- Please keep all the structure changes in this file
ALTER TABLE `items` ADD `kills` INT NOT NULL DEFAULT 0;
ALTER TABLE `characters` MODIFY `heading` INT DEFAULT NULL;