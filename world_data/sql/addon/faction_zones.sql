CREATE TABLE IF NOT EXISTS `faction_zones` (
  `zoneId` TINYINT UNSIGNED NOT NULL,
  `ownerId` TINYINT(1) UNSIGNED NOT NULL,
  `defaultOwnerId` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `weight` TINYINT(2) UNSIGNED NOT NULL,
  `innerLevel` TINYINT(2) NOT NULL,
  `recoverCpLevel` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `recoverHpLevel` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `recoverMpLevel` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  -- CP if > 0, then HP (but doesn't kill)
  `damageLevel` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `damageMpLevel` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  -- This also specifies the elemental attribute
  `damageAbnormal` INT UNSIGNED NOT NULL DEFAULT 0,
  `recoverCpExpire` BIGINT UNSIGNED NOT NULL DEFAULT 0,
  `recoverHpExpire` BIGINT UNSIGNED NOT NULL DEFAULT 0,
  `recoverMpExpire` BIGINT UNSIGNED NOT NULL DEFAULT 0,
  `damageHpExpire` BIGINT UNSIGNED NOT NULL DEFAULT 0,
  `damageMpExpire` BIGINT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`zoneId`)
) DEFAULT CHARSET=utf8;

INSERT INTO `faction_zones` (`zoneId`, `ownerId`, `weight`, `innerLevel`) VALUES
(28, 2, 1, 15), -- TI Village
(29, 2, 1, 14), -- Southern Fort
(25, 2, 1, 13), -- Gludin Village
(26, 2, 1, 12), -- Kamael Village
(19, 2, 1, 11), -- DE Village
(8, 2, 1, 10), -- Gludio Castle
(7, 2, 1, 9), -- Town of Gludio
(21, 2, 1, 8), -- Elven Village
(13, 2, 1, 7), -- Town of Oren
(14, 2, 1, 6), -- Oren Castle
(24, 2, 1, 5), -- Ivory Tower
(16, 2, 1, 4), -- Rune Castle
(15, 2, 1, 3), -- Town of Rune
(27, 2, 1, 2), -- Orc Village
(18, 2, 1, 1), -- Schuttgart Castle
(17, 1, 1, 1), -- Town of Schuttgart
(20, 1, 1, 2), -- Dwarven Village
(30, 1, 1, 3), -- Western Fort
(9, 1, 1, 4), -- Town of Goddard
(10, 1, 1, 5), -- Goddard Castle
(1, 1, 50, 6), -- Town of Aden
(2, 1, 50, 7), -- Aden Castle
(23, 1, 1, 8), -- Hunter Village
(3, 1, 1, 9), -- Town of Dion
(4, 1, 1, 10), -- Dion Castle
(22, 1, 1, 11), -- Floran Village
(5, 1, 1, 12), -- Town of Giran
(6, 1, 1, 13), -- Giran Castle
(11, 1, 1, 14), -- Town of Heine
(12, 1, 1, 15); -- Innadril Castle

UPDATE `faction_zones` SET `defaultOwnerId` = `ownerId`;
