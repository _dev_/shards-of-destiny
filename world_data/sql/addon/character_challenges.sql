CREATE TABLE IF NOT EXISTS `character_challenges` (
  `charId` INT UNSIGNED NOT NULL,
  `challenge` SMALLINT UNSIGNED NOT NULL,
  `points` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`charId`,`challenge`)
) DEFAULT CHARSET=utf8;