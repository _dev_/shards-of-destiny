-- All changes except for custom tables are here for easy access/update
-- Please bear that in mind when changing tables!

-- Table cleanup
TRUNCATE TABLE `char_creation_items`;
TRUNCATE TABLE `droplist`;
TRUNCATE TABLE `grandboss_spawnlist`;
TRUNCATE TABLE `raidboss_spawnlist`;
TRUNCATE TABLE `random_spawn`;
TRUNCATE TABLE `random_spawn_loc`;
TRUNCATE TABLE `spawnlist`;

-- Remove prices from all items
UPDATE `armor` SET `price` = 0;
UPDATE `etcitem` SET `price` = 0;
UPDATE `weapon` SET `price` = 0;
-- Remove henna prices
UPDATE `henna` SET `price` = 0;

-- Remove XP & SP gain
UPDATE `npc` SET `exp` = 0, `sp` = 0;

-- Record changes
UPDATE `npc` SET `type` = 'L2ZoneArtefact' WHERE `type` LIKE 'L2Artefact';
UPDATE `npc` SET `type` = 'L2ZoneArtefact' WHERE `id` BETWEEN 36572 AND 36580;
UPDATE `npc` SET `type` = 'L2ZoneGuard', `level` = 78, `runspd` = 225, `walkspd` = 175, `matkspd` = 500, `atkspd` = 999, `faction_id` = 'zone_owner_servant', `faction_range` = 1000, `ss` = 9999, `bss` = 9999, `ss_rate` = 100, `AI` = 'balanced' WHERE `name` LIKE 'Territory Guard';
-- Reduced SP gain (1/2)
UPDATE `npc` SET `type` = 'L2ZoneGuard', `level` = 78, `faction_id` = 'zone_owner_servant', `ss` = 9999, `bss` = 9999, `ss_rate` = 100 WHERE `type` LIKE 'L2SiegeGuard';
UPDATE `npc` SET `type` = 'L2ZoneHireling' WHERE `type` LIKE 'L2ZoneGuard' AND `name` LIKE '%Mercenary%';
UPDATE `npc` SET `type` = 'L2ZoneHireling' WHERE `type` LIKE 'L2ZoneGuard' AND `name` LIKE '%Recruit';
UPDATE `npc` SET `collision_height` = 24, `type` = 'L2ZoneHireling' WHERE `id` IN (35060, 35061);
UPDATE `npc` SET `exp` = 54000, `sp` = 10000 WHERE `type` LIKE 'L2ZoneGuard';
UPDATE `npc` SET `exp` = 27000, `sp` = 5000 WHERE `type` LIKE 'L2ZoneHireling';
-- Aden and Rune OP guards (once)
-- UPDATE `npc` SET `pAtk` = `pAtk` / 2 WHERE `id` BETWEEN 35239 AND 35260;
-- UPDATE `npc` SET `pAtk` = `pAtk` / 2 WHERE `id` BETWEEN 35475 AND 35496;

UPDATE `etcitem` SET `skills_item` = '9102-2' WHERE `item_id` = 5592;
-- UPDATE `etcitem` SET `handler` = 'WondrousCubic' WHERE `item_id` = 10632;

-- Castle Artefacts (no need to modify)
INSERT INTO `spawnlist` VALUES
(null,'GludioArtefact1',1,35063,-18120,107984,-2483,0,0,16384,60,0,0),
(null,'GiranArtefact1',1,35147,117939,145090,-2550,0,0,32768,60,0,0),
(null,'DionArtefact1',1,35105,22081,161771,-2677,0,0,49017,60,0,0),
(null,'OrenArtefact1',1,35189,84014,37184,-2277,0,0,16384,60,0,0),
(null,'AdenArtefact1',1,35233,147465,1537,-373,0,0,16384,60,0,0),
(null,'InnadrilArtefact1',1,35279,116031,250555,-798,0,0,49200,60,0,0),
(null,'GoddardArtefact1',1,35322,146601,-50441,-1505,0,0,32768,60,0,0),
(null,'GoddardArtefact2',1,35323,148353,-50457,-1505,0,0,0,60,0,0),
(null,'RuneArtefact1',1,35469,9132,-49153,1094,0,0,6133,60,0,0),
(null,'SchuttgartArtefact1',1,35515,76668,-154520,226,0,0,0,60,0,0),
(null,'SchuttgartArtefact2',1,35514,78446,-154524,227,0,0,0,60,0,0);

INSERT INTO `char_creation_items` VALUES
-- (-1, 13124, 1, 'false'), -- All classes - Color Name 1Pack
(-1, 10650, 300, 'false'); -- All classes - Adventurer's Scroll of Escape

INSERT IGNORE INTO `teleport` VALUES
('Keucereus Upper South', 331001, -186736, 244154, 2668, 0, 0),
('Keucereus Lower South', 331002, -186312, 244345, 1569, 0, 0),
('Keucereus Lower North', 331003, -185255, 240853, 1569, 0, 0),
('Keucereus Shop Area', 331004, -184340, 244955, 1540, 0, 0),
('Keucereus Class Masters', 331005, -184920, 242860, 1578, 0, 0);

DELETE FROM `skill_trees` WHERE `skill_id` BETWEEN 9110 AND 9114;
DELETE FROM `skill_trees` WHERE `skill_id` BETWEEN 9118 AND 9119;
INSERT INTO `skill_trees` VALUES
(12, 9110, 2, 'Mystic nerf', 0, 40),
(13, 9110, 2, 'Mystic nerf', 0, 40),
(27, 9110, 2, 'Mystic nerf', 0, 40),
(40, 9110, 2, 'Mystic nerf', 0, 40),
(14, 9110, 1, 'Summoner nerf', 0, 40),
(28, 9110, 1, 'Summoner nerf', 0, 40),
(41, 9110, 1, 'Summoner nerf', 0, 40),
(15, 9110, 1, 'Enchanter nerf', 0, 20),
(29, 9110, 1, 'Enchanter nerf', 0, 20),
(42, 9110, 1, 'Enchanter nerf', 0, 20),
(50, 9110, 3, 'Irregularity nerf', 0, 20),
-- (128, 9110, 3, 'Irregularity nerf', 0, 40),
-- (129, 9110, 3, 'Irregularity nerf', 0, 40),
(128, 9110, 2, 'Mystic nerf', 0, 40),
(129, 9110, 2, 'Mystic nerf', 0, 40),
(0, 9111, 1, 'SoD skill - HF', 0, 1),
(10, 9111, 1, 'SoD skill - HM', 0, 1),
(18, 9111, 1, 'SoD skill - EF', 0, 1),
(25, 9111, 1, 'SoD skill - EM', 0, 1),
(31, 9111, 1, 'SoD skill - DF', 0, 1),
(38, 9111, 1, 'SoD skill - DM', 0, 1),
(44, 9111, 1, 'SoD skill - OF', 0, 1),
(49, 9111, 1, 'SoD skill - OM', 0, 1),
(53, 9111, 1, 'SoD skill - Dwarf', 0, 1),
(5, 9112, 1, 'Paladin nerf', 0, 40),
(21, 9113, 1, 'Voice Enchanter', 0, 40),
(34, 9113, 1, 'Voice Enchanter', 0, 40),
(2, 9114, 1, 'Flawed Dualism', 0, 40),
(48, 9114, 1, 'Flawed Dualism', 0, 40),
(128, 9118, 1, 'Shackles of War', 0, 40),
(129, 9118, 1, 'Shackles of War', 0, 40),
(127, 9119, 1, 'Regression', 0, 40);

INSERT INTO `grandboss_spawnlist` VALUES
(29001,-21610,181594,-5734,0,432000,432000,0,229898,667), -- Queen Ant (40)
(29006,17726,108915,-6480,0,259200,259200,0,622493,575), -- Core (50)
(29014,55024,17368,-5412,10126,345600,345600,0,622493,1660), -- Orfen (50)
(29022,55312,219168,-3223,0,432000,432000,0,858518,1975); -- Zaken (60)

UPDATE `npc` SET `level` = 82 WHERE `id` IN (29001,29006,29014,29022);
UPDATE `npc` SET `level` = 80 WHERE `id` IN
(29003,29004,29005,29007,29008,29009,29010,29011,29012,29013,29015,29016,29017,29018,
29023,29024,29026,29027);
UPDATE `armor` SET /*`duration` = 2880,*/ `time` = 7200 WHERE `item_id` BETWEEN 6656 AND 6662;

INSERT INTO `raidboss_spawnlist` VALUES
-- (25623, 1, 2295, 235924, -3263, 0, 432000, 432000, 0, 489654, 1859), -- Valdstone
-- (25624, 1, -17947, 253064, -3241, 45023, 432000, 432000, 0, 504491, 1973), -- Rok
-- (25625, 1, 47608, 145857, -3464, 40106, 432000, 432000, 0, 277325, 2050), -- Enira
(29096, 1, -26186, 186925, -4172, 8369, 172800, 172800, 0, 759807, 2277), -- Anais (87)
(25244,1,138680,29645,-2545,0,43200,86400,0,977229,3643), -- Last Lesser Giant Olkuth (75)
(25245,1,159630,27430,-3480,0,86400,86400,0,1891801,3420), -- Last Lesser Giant Glaki (78)
(25514,1,147750,-10730,-4445,0,129600,172800,0,714778,3718), -- Queen Shyeed (80)
(25527,1,171550,-13800,-2660,0,129600,172800,0,1608553,3718); -- Uruka (80)

INSERT INTO `droplist` VALUES
-- Simple bosses - larger adena drops
-- No materials though
-- Anais
(29096,57,50,200,0,1000000), -- Adena
(29096,4269,1,1,1,200000), -- Noblesse
-- Queen Shyeed
(25514,57,25,100,0,1000000), -- Adena
(25514,4269,1,1,1,100000), -- Noblesse
-- Uruka
(25527,57,25,100,0,1000000), -- Adena
(25527,4269,1,1,1,100000), -- Noblesse
-- Glaki
(25245,57,18,60,0,1000000), -- Adena
(25245,4269,1,1,1,40000), -- Noblesse
-- Olkuth
(25244,57,10,25,0,1000000), -- Adena
(25244,4269,1,1,1,10000); -- Noblesse

-- < Frintezza (Scarlet Van Halisha) Start>
INSERT INTO `droplist` VALUES
(29047,57,1,500,0,1000000),-- Adena 100% chance
(29047,8191,1,3,1,50000),-- Frintezza's Necklace 50% chance
(29047,90002,1,200,2,800000),-- Looted Goods - Brown Pouch 80% chance
(29047,90003,1,100,3,800000),-- Looted Goods - White Cargo box 80% chance
(29047,90004,1,10,4,800000),-- Looted Goods - Red Cargo box 80% chance
(29047,4269,1,1,5,1000000),-- Quest Item (Noblesse), Articles of Dead Heroes 100% chance
-- < Frintezza (Scarlet Van Halisha) End>

-- < Valakas Start>
(29028,57,300,2000,0,1000000),-- Adena 100% chance
(29028,6657,1,3,1,50000),-- Necklace of Valakas 50% chance
(29028,90002,1,180,2,800000),-- Looted Goods - Brown Pouch 80% chance
(29028,90003,1,90,3,800000),-- Looted Goods - White Cargo box 80% chance
(29028,90004,1,9,4,800000),-- Looted Goods - Red Cargo box 80% chance
(29028,4269,1,1,5,1000000),-- Quest Item (Noblesse), Articles of Dead Heroes 100% chance
-- < Valakas End>

-- < Antharas Start>
(29019,57,250,1800,0,1000000),-- Adena 100% chance
(29019,6656,1,3,1,50000),-- Earring of Antharas 50% chance
(29019,90002,1,170,2,800000),-- Looted Goods - Brown Pouch 80% chance
(29019,90003,1,80,3,800000),-- Looted Goods - White Cargo box 80% chance
(29019,90004,1,8,4,800000),-- Looted Goods - Red Cargo box 80% chance
(29019,4269,1,1,5,1000000),-- Quest Item (Noblesse), Articles of Dead Heroes 100% chance

(29066,57,250,1800,0,1000000),-- Adena 100% chance
(29066,6656,1,3,1,50000),-- Earring of Antharas 50% chance
(29066,90002,1,170,2,800000),-- Looted Goods - Brown Pouch 80% chance
(29066,90003,1,80,3,800000),-- Looted Goods - White Cargo box 80% chance
(29066,90004,1,8,4,800000),-- Looted Goods - Red Cargo box 80% chance
(29066,4269,1,1,5,1000000),-- Quest Item (Noblesse), Articles of Dead Heroes 100% chance

(29067,57,250,1800,0,1000000),-- Adena 100% chance
(29067,6656,1,3,1,50000),-- Earring of Antharas 50% chance
(29067,90002,1,170,2,800000),-- Looted Goods - Brown Pouch 80% chance
(29067,90003,1,80,3,800000),-- Looted Goods - White Cargo box 80% chance
(29067,90004,1,8,4,800000),-- Looted Goods - Red Cargo box 80% chance
(29067,4269,1,1,5,1000000),-- Quest Item (Noblesse), Articles of Dead Heroes 100% chance

(29068,57,250,1800,0,1000000),-- Adena 100% chance
(29068,6656,1,3,1,50000),-- Earring of Antharas 50% chance
(29068,90002,1,170,2,800000),-- Looted Goods - Brown Pouch 80% chance
(29068,90003,1,80,3,800000),-- Looted Goods - White Cargo box 80% chance
(29068,90004,1,8,4,800000),-- Looted Goods - Red Cargo box 80% chance
(29068,4269,1,1,5,1000000),-- Quest Item (Noblesse), Articles of Dead Heroes 100% chance
-- <Antharas End>

-- < Baium Start>
(29020,57,200,1500,0,1000000),-- Adena 100% chance
(29020,6658,1,3,1,50000),-- Ring of Baium 50% chance
(29020,90002,1,160,2,800000),-- Looted Goods - Brown Pouch 80% chance
(29020,90003,1,70,3,800000),-- Looted Goods - White Cargo box 80% chance
(29020,90004,1,7,4,800000),-- Looted Goods - Red Cargo box 80% chance
(29020,4269,1,1,5,1000000),-- Quest Item (Noblesse), Articles of Dead Heroes 100% chance
-- < Baium End>

-- < Zaken Start>
(29022,57,100,1000,0,1000000),-- Adena 100% chance
(29022,6659,1,3,1,30000),-- Zaken's Earring 30% chance
(29022,90002,1,100,2,800000),-- Looted Goods - Brown Pouch 80% chance
(29022,90003,1,40,3,800000),-- Looted Goods - White Cargo box 80% chance
(29022,90004,1,4,4,800000),-- Looted Goods - Red Cargo box 80% chance
(29022,4269,1,1,5,1000000),-- Quest Item (Noblesse), Articles of Dead Heroes 100% chance
-- < Zaken End>

-- < Queen Ant Start>
(29001,57,75,700,0,1000000),-- Adena 100% chance
(29001,6660,1,3,1,30000),-- Ring of Queen Ant 30% chance
(29001,90002,1,80,2,800000),-- Looted Goods - Brown Pouch 80% chance
(29001,90003,1,30,3,800000),-- Looted Goods - White Cargo box 80% chance
(29001,90004,1,3,4,800000),-- Looted Goods - Red Cargo box 80% chance
(29001,4269,1,1,5,1000000),-- Quest Item (Noblesse), Articles of Dead Heroes 100% chance
-- < Queen Ant End>

-- < Orfen Start>
(29014,57,50,450,0,1000000),-- Adena 100% chance
(29014,6661,1,3,1,30000),-- Earring of Orfen 30% chance
(29014,90002,1,80,2,800000),-- Looted Goods - Brown Pouch 80% chance
(29014,90003,1,30,3,800000),-- Looted Goods - White Cargo box 80% chance
(29014,90004,1,3,4,800000),-- Looted Goods - Red Cargo box 80% chance
(29014,4269,1,1,5,1000000),-- Quest Item (Noblesse), Articles of Dead Heroes 100% chance
-- < Orfen End>

-- < Core Start>
(29006,57,40,300,0,1000000),-- Adena 100% chance
(29006,6662,1,3,1,30000),-- Ring of Core 30% chance
(29006,90002,1,80,2,800000),-- Looted Goods - Brown Pouch 80% chance
(29006,90003,1,30,3,800000),-- Looted Goods - White Cargo box 80% chance
(29006,90004,1,3,4,800000),-- Looted Goods - Red Cargo box 80% chance
(29006,4269,1,1,5,1000000);-- Quest Item (Noblesse), Articles of Dead Heroes 100% chance
-- <Core End>

INSERT IGNORE INTO `skill_trees` VALUES
(135,837,1,'Painkiller',200000,58);

INSERT IGNORE INTO `minions` VALUES
(90036,90037,5,8),		-- Uthanka
(90059,90060,15,20);	-- Caravan

-- Clean Nearby Lands
-- Area 1
INSERT INTO `droplist` VALUES
(90038,57,1,2,0,500000), -- Adena
(90038,90002,1,1,1,100000), -- Looted Goods - Brown Pouch
(90038,90003,1,1,1,50000), -- Looted Goods - White Cargo box
(90038,90004,1,1,1,10000), -- Looted Goods - Red Cargo box
(90039,57,1,2,0,500000), -- Adena
(90039,90002,1,1,1,100000), -- Looted Goods - Brown Pouch
(90039,90003,1,1,1,50000), -- Looted Goods - White Cargo box
(90039,90004,1,1,1,10000), -- Looted Goods - Red Cargo box
(90040,57,5,10,0,500000), -- Adena
(90040,90002,1,1,1,100000), -- Looted Goods - Brown Pouch
(90040,90003,1,1,1,50000), -- Looted Goods - White Cargo box
(90040,90004,1,1,1,10000), -- Looted Goods - Red Cargo box
(90041,57,4,8,0,500000), -- Adena
(90041,90002,1,1,1,100000), -- Looted Goods - Brown Pouch
(90041,90003,1,1,1,50000), -- Looted Goods - White Cargo box
(90041,90004,1,1,1,10000), -- Looted Goods - Red Cargo box
(90042,57,5,10,0,500000), -- Adena
(90042,90002,1,1,1,100000), -- Looted Goods - Brown Pouch
(90042,90003,1,1,1,50000), -- Looted Goods - White Cargo box
(90042,90004,1,1,1,10000), -- Looted Goods - Red Cargo box
(90043,57,4,8,0,500000), -- Adena
(90043,90002,1,1,1,100000), -- Looted Goods - Brown Pouch
(90043,90003,1,1,1,50000), -- Looted Goods - White Cargo box
(90043,90004,1,1,1,10000), -- Looted Goods - Red Cargo box
(90044,57,2,4,0,500000), -- Adena
(90044,90002,1,1,1,100000), -- Looted Goods - Brown Pouch
(90044,90003,1,1,1,50000), -- Looted Goods - White Cargo box
(90044,90004,1,1,1,10000); -- Looted Goods - Red Cargo box

-- Area 2
INSERT INTO `droplist` VALUES
(90045,57,2,6,0,600000), -- Adena
(90045,90002,1,1,1,125000), -- Looted Goods - Brown Pouch
(90045,90003,1,1,1,25000), -- Looted Goods - White Cargo box
(90045,90004,1,1,1,2500), -- Looted Goods - Red Cargo box
(90046,57,2,6,0,600000), -- Adena
(90046,90002,1,1,1,125000), -- Looted Goods - Brown Pouch
(90046,90003,1,1,1,25000), -- Looted Goods - White Cargo box
(90046,90004,1,1,1,2500), -- Looted Goods - Red Cargo box
(90047,57,4,12,0,600000), -- Adena
(90047,90002,1,1,1,125000), -- Looted Goods - Brown Pouch
(90047,90003,1,1,1,25000), -- Looted Goods - White Cargo box
(90047,90004,1,1,1,2500), -- Looted Goods - Red Cargo box
(90048,57,4,12,0,600000), -- Adena
(90048,90002,1,1,1,125000), -- Looted Goods - Brown Pouch
(90048,90003,1,1,1,25000), -- Looted Goods - White Cargo box
(90048,90004,1,1,1,2500), -- Looted Goods - Red Cargo box
(90049,57,6,18,0,600000), -- Adena
(90049,90002,1,1,1,125000), -- Looted Goods - Brown Pouch
(90049,90003,1,1,1,25000), -- Looted Goods - White Cargo box
(90049,90004,1,1,1,2500); -- Looted Goods - Red Cargo box

-- Area 3
INSERT INTO `droplist` VALUES
(90050,57,3,6,0,700000), -- Adena
(90050,90002,1,1,1,200000), -- Looted Goods - Brown Pouch
(90050,90003,1,1,1,40000), -- Looted Goods - White Cargo box
(90050,90004,1,1,1,4000), -- Looted Goods - Red Cargo box
(90051,57,3,6,0,700000), -- Adena
(90051,90002,1,1,1,200000), -- Looted Goods - Brown Pouch
(90051,90003,1,1,1,40000), -- Looted Goods - White Cargo box
(90051,90004,1,1,1,4000), -- Looted Goods - Red Cargo box
(90052,57,3,6,0,700000), -- Adena
(90052,90002,1,1,1,200000), -- Looted Goods - Brown Pouch
(90052,90003,1,1,1,40000), -- Looted Goods - White Cargo box
(90052,90004,1,1,1,4000), -- Looted Goods - Red Cargo box
(90053,57,3,6,0,700000), -- Adena
(90053,90002,1,1,1,200000), -- Looted Goods - Brown Pouch
(90053,90003,1,1,1,40000), -- Looted Goods - White Cargo box
(90053,90004,1,1,1,4000), -- Looted Goods - Red Cargo box
(90054,57,3,6,0,700000), -- Adena
(90054,90002,1,1,1,200000), -- Looted Goods - Brown Pouch
(90054,90003,1,1,1,40000), -- Looted Goods - White Cargo box
(90054,90004,1,1,1,4000); -- Looted Goods - Red Cargo box

-- Area 4
INSERT INTO `droplist` VALUES
(90055,57,15,30,0,800000), -- Adena
(90055,90002,1,1,1,250000), -- Looted Goods - Brown Pouch
(90055,90003,1,1,1,50000), -- Looted Goods - White Cargo box
(90055,90004,1,1,1,5000), -- Looted Goods - Red Cargo box
(90056,57,5,10,0,800000), -- Adena
(90056,90002,1,1,1,250000), -- Looted Goods - Brown Pouch
(90056,90003,1,1,1,50000), -- Looted Goods - White Cargo box
(90056,90004,1,1,1,5000), -- Looted Goods - Red Cargo box
(90057,57,5,10,0,800000), -- Adena
(90057,90002,1,1,1,250000), -- Looted Goods - Brown Pouch
(90057,90003,1,1,1,50000), -- Looted Goods - White Cargo box
(90057,90004,1,1,1,5000), -- Looted Goods - Red Cargo box
(90058,57,5,10,0,800000), -- Adena
(90058,90002,1,1,1,250000), -- Looted Goods - Brown Pouch
(90058,90003,1,1,1,50000), -- Looted Goods - White Cargo box
(90058,90004,1,1,1,5000); -- Looted Goods - Red Cargo box

UPDATE `weapon` SET `skills_item` = CONCAT(`skills_item`,';9122-1') WHERE `name` LIKE '% - Towering Blow' AND `skills_item` NOT LIKE '%9122-1%';
UPDATE `weapon` SET `skills_item` = CONCAT(`skills_item`,';9123-1') WHERE `name` LIKE '% - Wide Blow' AND `skills_item` NOT LIKE '%9123-1%';

INSERT INTO `droplist` VALUES
(90059,57,1,30,0,1000000),-- Adena 100% chance
(90059,90002,1,50,2,800000),-- Looted Goods - Brown Pouch 80% chance
(90059,90003,1,10,3,800000),-- Looted Goods - White Cargo box 80% chance
(90059,90004,1,2,3,800000),-- Looted Goods - Red Cargo box 80% chance
(90060,57,1,5,0,1000000),-- Adena 100% chance
(90060,90002,1,3,2,800000),-- Looted Goods - Brown Pouch 80% chance
(90060,90003,1,2,2,500000),-- Looted Goods - White Cargo box 50% chance
(90060,90004,1,1,2,200000);-- Looted Goods - Red Cargo box 20% chance

INSERT INTO `skill_learn` (`npc_id`,`class_id`)
SELECT 90070, `class_id` FROM `skill_learn` WHERE `npc_id` = 32144;
INSERT INTO `skill_learn` (`npc_id`,`class_id`)
SELECT 90071, `class_id` FROM `skill_learn` WHERE `npc_id` = 31316;
