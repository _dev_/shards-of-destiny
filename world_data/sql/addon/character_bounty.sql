CREATE TABLE IF NOT EXISTS `character_bounty` (
  `charId` INT UNSIGNED NOT NULL,
  `bounty` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`charId`)
) DEFAULT CHARSET=utf8;