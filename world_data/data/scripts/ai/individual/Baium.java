/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package ai.individual;

import com.l2jfree.gameserver.datatables.SkillTable;
import com.l2jfree.gameserver.instancemanager.grandbosses.BaiumManager;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.L2Summon;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.quest.QuestState;
import com.l2jfree.gameserver.model.quest.jython.QuestJython;
import com.l2jfree.util.Rnd;

/**
 * @author savormix
 * 
 */
public class Baium extends QuestJython
{
	private static final String NAME = "baium";
	
	// Quest NPCs
	private static final int STATUE = 29025;
	
	// Quest monsters
	private static final int BAIUM = 29020;
	private static final int ARCHANGEL = 29021;
	
	private static final int SPEAR_POUND = 4132;
	private static final int ANGEL_HEAL = 4133;
	
	private Baium(int questId, String name, String descr)
	{
		super(questId, name, descr);
		addStartNpc(STATUE);
		addTalkId(STATUE);
		addAttackId(ARCHANGEL);
		addAttackId(BAIUM);
		addKillId(BAIUM);
	}
	
	@Override
	public String onAttack(L2Npc npc, L2PcInstance attacker, int damage, boolean isPet, L2Skill skill)
	{
		if (npc.getNpcId() == ARCHANGEL)
		{
			int rnd = Rnd.get(100);
			double percent = (npc.getCurrentHp() / npc.getMaxHp()) * 100;
			L2Skill sk;
			if (percent < 50 && rnd < 5)
			{
				sk = SkillTable.getInstance().getInfo(ANGEL_HEAL, 1);
				if (sk != null)
				{
					npc.setTarget(npc);
					npc.doCast(sk);
				}
			}
			else if (rnd < 10)
			{
				sk = SkillTable.getInstance().getInfo(SPEAR_POUND, 1);
				if (sk != null)
				{
					npc.setTarget(attacker);
					if (isPet)
					{
						L2Summon pet = attacker.getPet();
						if (pet != null && !pet.isDead())
							npc.setTarget(pet);
					}
					npc.doCast(sk);
				}
			}
		}
		else
			BaiumManager.getInstance().setLastAttackTime();
		return null;
	}
	
	public String onKill(L2Npc npc, L2PcInstance killer)
	{
		BaiumManager.getInstance().setCubeSpawn();
		QuestState qs = killer.getQuestState(NAME);
		if (qs != null)
			qs.exitQuest(true);
		return null;
	}
	
	@Override
	public String onTalk(L2Npc npc, L2PcInstance talker)
	{
		if (!npc.isBusy())
		{
			npc.onBypassFeedback(talker, "wake_baium");
			npc.setBusyMessage("Attending another player's request.");
			npc.setBusy(true);
		}
		else
			return npc.getBusyMessage();
		return null;
	}
	
	public static void main(String[] args)
	{
		new Baium(-1, NAME, "ai");
	}
}
