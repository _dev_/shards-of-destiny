/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package ai.individual;

import javolution.util.FastMap;

import org.sod.Calc;

import ai.group_template.L2AttackableAIScript;

import com.l2jfree.Config;
import com.l2jfree.gameserver.ai.CtrlIntention;
import com.l2jfree.gameserver.model.L2CharPosition;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.actor.L2Attackable;
import com.l2jfree.gameserver.model.actor.L2Character;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.instance.L2MonsterInstance;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.Chat;
import com.l2jfree.gameserver.network.client.packets.sendable.CreatureSay.ChatMessage;
import com.l2jfree.lang.L2Math;
import com.l2jfree.lang.L2TextBuilder;

/**
 * This implementation still has an exploit that allows
 * arbitrary numbers to be assigned to CaravanInfo path.
 * 
 * @author savormix
 * @deprecated Useless in the new concept server.
 */
@Deprecated
public class CaravanMaster extends L2AttackableAIScript
{
	private static final boolean DEBUG = false;
	private static final int MASTER = 90059;
	private static final int TRAVELLER = 90060;
	private static final int SEER = 90061;
	private static final int MAX_GEO_PLAN_DIST = 100 * 100;
	private static final int CARAVAN_RESPAWN = 1800000;
	
	private static final L2CharPosition[][] PATH = new L2CharPosition[][]
	{
			// Gludin -> Gludio
			new L2CharPosition[]
			{
					new L2CharPosition(-77308, 152240, -3177, 62930), new L2CharPosition(-75306, 151234, -3022, 60679),
					new L2CharPosition(-73573, 150276, -2854, 21642), new L2CharPosition(-72651, 149607, -2834, 58988),
					new L2CharPosition(-71328, 149724, -2733, 920), new L2CharPosition(-69388, 150819, -2733, 5359),
					new L2CharPosition(-67770, 151221, -2537, 2540), new L2CharPosition(-66535, 152422, -2538, 8046),
					new L2CharPosition(-65581, 153349, -2548, 8042), new L2CharPosition(-63557, 153771, -2548, 2143),
					new L2CharPosition(-61845, 154530, -2535, 4352), new L2CharPosition(-60361, 154890, -2555, 2482),
					new L2CharPosition(-59497, 154589, -2593, 62039), new L2CharPosition(-57003, 154503, -2550, 65176),
					new L2CharPosition(-55498, 154318, -2553, 64260), new L2CharPosition(-54002, 153249, -2571, 59064),
					new L2CharPosition(-52962, 152400, -2591, 58395), new L2CharPosition(-51901, 151243, -2601, 56476),
					new L2CharPosition(-51298, 149288, -2692, 52272), new L2CharPosition(-49919, 146966, -2801, 54741),
					new L2CharPosition(-48653, 145946, -2829, 58462), new L2CharPosition(-47610, 142422, -2960, 52153),
					new L2CharPosition(-45053, 138810, -2911, 55577), new L2CharPosition(-44690, 137199, -2885, 51463),
					new L2CharPosition(-44866, 133121, -2846, 48702), new L2CharPosition(-45866, 130747, -2850, 44993),
					new L2CharPosition(-45546, 129328, -2839, 51465), new L2CharPosition(-44380, 125972, -2839, 52639),
					new L2CharPosition(-42836, 124226, -2882, 56704), new L2CharPosition(-41561, 123300, -2919, 58984),
					new L2CharPosition(-38924, 124175, -2887, 3341), new L2CharPosition(-35989, 125106, -2888, 3203),
					new L2CharPosition(-33924, 125248, -2935, 716), new L2CharPosition(-32850, 125032, -2935, 63465),
					new L2CharPosition(-31930, 124412, -3064, 59350), new L2CharPosition(-30594, 125256, -3319, 5876),
					new L2CharPosition(-29376, 127459, -3439, 11116), new L2CharPosition(-27720, 127553, -3288, 591),
					new L2CharPosition(-26664, 127077, -3261, 61118), new L2CharPosition(-24934, 125354, -3479, 57365),
					new L2CharPosition(-20977, 124695, -3479, 63814),
			},
			// Gludio -> DE Village
			new L2CharPosition[]
			{
					new L2CharPosition(-6456, 123384, -3025, 56098), new L2CharPosition(-8401, 116360, -3025, 46334),
					new L2CharPosition(-9236, 114469, -3025, 44814), new L2CharPosition(-7911, 112273, -3025, 54814),
					new L2CharPosition(-7978, 109625, -3025, 48888), new L2CharPosition(-7417, 107626, -3270, 52005),
					new L2CharPosition(-5896, 105312, -2967, 55217), new L2CharPosition(-6248, 103895, -2873, 46612),
					new L2CharPosition(-8748, 102138, -2778, 39157), new L2CharPosition(-9406, 100211, -2903, 45719),
					new L2CharPosition(-9275, 96460, -2903, 49516), new L2CharPosition(-8594, 94404, -2903, 52488),
					new L2CharPosition(-9705, 92768, -2903, 42929), new L2CharPosition(-8871, 91569, -3042, 55491),
					new L2CharPosition(-7707, 90347, -3080, 57090), new L2CharPosition(-8414, 89009, -3205, 44081),
					new L2CharPosition(-9552, 87840, -3205, 41100), new L2CharPosition(-9983, 84646, -3205, 47752),
					new L2CharPosition(-9511, 82786, -3205, 51744), new L2CharPosition(-9808, 80050, -3205, 48024),
					new L2CharPosition(-10643, 77192, -3205, 46187), new L2CharPosition(-10686, 75640, -3205, 48863),
					new L2CharPosition(-11931, 75110, -3205, 36965), new L2CharPosition(-13895, 72841, -3205, 41710),
					new L2CharPosition(-16288, 70489, -3205, 40869), new L2CharPosition(-17784, 68539, -3456, 42322),
					new L2CharPosition(-20684, 67922, -3301, 34736), new L2CharPosition(-22380, 68184, -3205, 31101),
					new L2CharPosition(-23444, 68771, -3205, 27509), new L2CharPosition(-25322, 66517, -3205, 41906),
					new L2CharPosition(-26263, 63820, -3205, 45650), new L2CharPosition(-27607, 61399, -3418, 43863),
					new L2CharPosition(-29481, 58792, -3205, 42614), new L2CharPosition(-29684, 57010, -3205, 47968),
					new L2CharPosition(-29384, 53608, -3205, 50069), new L2CharPosition(-29688, 52515, -3205, 46322),
					new L2CharPosition(-30389, 51270, -3205, 43803), new L2CharPosition(-30699, 49700, -3205, 47118),
					new L2CharPosition(-30204, 47901, -3205, 51952), new L2CharPosition(-29483, 46125, -3205, 53174),
					new L2CharPosition(-29948, 43114, -3205, 47553), new L2CharPosition(-29837, 41470, -3309, 49855),
					new L2CharPosition(-28523, 39170, -3542, 54565), new L2CharPosition(-26158, 37314, -3375, 58595),
					new L2CharPosition(-23722, 36401, -3375, 61795), new L2CharPosition(-22088, 35314, -3375, 59413),
					new L2CharPosition(-21035, 33759, -3162, 55360), new L2CharPosition(-18919, 32633, -3162, 60435),
					new L2CharPosition(-17817, 31371, -3162, 56639), new L2CharPosition(-16010, 30522, -3282, 60954),
					new L2CharPosition(-15247, 29552, -3475, 55957), new L2CharPosition(-13533, 25697, -3498, 53462),
					new L2CharPosition(-9897, 22832, -3499, 58700), new L2CharPosition(-8273, 22013, -3557, 60816),
					new L2CharPosition(-5052, 21982, -3227, 117), new L2CharPosition(-3190, 21652, -3467, 63671),
					new L2CharPosition(-1526, 20227, -3379, 58149), new L2CharPosition(-131, 19903, -3341, 63155),
			},
			// Gludio -> Elven Village
			new L2CharPosition[]
			{
					new L2CharPosition(-6456, 123384, -3025, 56098), new L2CharPosition(-8401, 116360, -3025, 46334),
					new L2CharPosition(-9236, 114469, -3025, 44814), new L2CharPosition(-7911, 112273, -3025, 54814),
					new L2CharPosition(-7978, 109625, -3025, 48888), new L2CharPosition(-7417, 107626, -3270, 52005),
					new L2CharPosition(-5896, 105312, -2967, 55217), new L2CharPosition(-6248, 103895, -2873, 46612),
					new L2CharPosition(-8748, 102138, -2778, 39157), new L2CharPosition(-9406, 100211, -2903, 45719),
					new L2CharPosition(-9275, 96460, -2903, 49516), new L2CharPosition(-8594, 94404, -2903, 52488),
					new L2CharPosition(-9705, 92768, -2903, 42929), new L2CharPosition(-8871, 91569, -3042, 55491),
					new L2CharPosition(-7707, 90347, -3080, 57090), new L2CharPosition(-8414, 89009, -3205, 44081),
					new L2CharPosition(-9552, 87840, -3205, 41100), new L2CharPosition(-9983, 84646, -3205, 47752),
					new L2CharPosition(-9511, 82786, -3205, 51744), new L2CharPosition(-9808, 80050, -3205, 48024),
					new L2CharPosition(-10643, 77192, -3205, 46187), new L2CharPosition(-10686, 75640, -3205, 48863),
					new L2CharPosition(-8450, 76277, -3200, 2934), new L2CharPosition(-6816, 76761, -3200, 3003),
					new L2CharPosition(-5429, 76928, -3200, 1249), new L2CharPosition(-4548, 77578, -3200, 6630),
					new L2CharPosition(-3702, 77509, -3200, 64687), new L2CharPosition(-2690, 77006, -3200, 60724),
					new L2CharPosition(-454, 75522, -3200, 59424), new L2CharPosition(2231, 74459, -3200, 61604),
					new L2CharPosition(3046, 74431, -3200, 65177), new L2CharPosition(3666, 72494, -3200, 52383),
					new L2CharPosition(4514, 71047, -3200, 54681), new L2CharPosition(5054, 68363, -3260, 51222),
					new L2CharPosition(5340, 66330, -3260, 50609), new L2CharPosition(5436, 65117, -3260, 49975),
					new L2CharPosition(6599, 63272, -3260, 55018), new L2CharPosition(7727, 62312, -3260, 58181),
					new L2CharPosition(10691, 60384, -3260, 59520), new L2CharPosition(11435, 59693, -3436, 57729),
					new L2CharPosition(12315, 58256, -3403, 54883), new L2CharPosition(13665, 56402, -3587, 55716),
					new L2CharPosition(15192, 55325, -3672, 59128), new L2CharPosition(16694, 54848, -3713, 62328),
					new L2CharPosition(20191, 54978, -3566, 387), new L2CharPosition(23099, 54380, -3612, 63420),
					new L2CharPosition(24461, 54034, -3607, 62941), new L2CharPosition(25793, 54052, -3480, 140),
					new L2CharPosition(27235, 53507, -3481, 61766), new L2CharPosition(28942, 51928, -3481, 57750),
					new L2CharPosition(29948, 51405, -3727, 60535), new L2CharPosition(30641, 51427, -3727, 331),
					new L2CharPosition(32579, 52122, -3728, 3591), new L2CharPosition(33982, 52991, -3713, 5784),
					new L2CharPosition(36294, 53516, -3590, 2328),
			},
			// Oren -> HV
			new L2CharPosition[]
			{
					new L2CharPosition(77449, 60075, -2916, 50711), new L2CharPosition(76765, 62335, -2916, 19449),
					new L2CharPosition(75840, 63818, -3336, 22199), new L2CharPosition(75352, 65846, -3465, 18630),
					new L2CharPosition(75386, 67578, -3396, 15813), new L2CharPosition(75881, 69406, -3571, 13435),
					new L2CharPosition(76648, 71092, -3482, 11709), new L2CharPosition(77409, 71836, -2988, 7259),
					new L2CharPosition(79096, 71899, -3321, 291), new L2CharPosition(80964, 72601, -3259, 4044),
					new L2CharPosition(81592, 72505, -3470, 62802), new L2CharPosition(84228, 70757, -3274, 59428),
					new L2CharPosition(85779, 70482, -3269, 63705), new L2CharPosition(89042, 71493, -3543, 3133),
					new L2CharPosition(91178, 71242, -3682, 64315), new L2CharPosition(93419, 71855, -3717, 2784),
					new L2CharPosition(94175, 72719, -3707, 8886), new L2CharPosition(94368, 73376, -3670, 13403),
					new L2CharPosition(95554, 74455, -3669, 7699), new L2CharPosition(96425, 75795, -3581, 10372),
					new L2CharPosition(97634, 76242, -3519, 3693), new L2CharPosition(99092, 76270, -3426, 64064),
					new L2CharPosition(99992, 75652, -3106, 59259), new L2CharPosition(101238, 75642, -2697, 65452),
					new L2CharPosition(102221, 76127, -2445, 4780), new L2CharPosition(103029, 76293, -2301, 2113),
					new L2CharPosition(104057, 76061, -2020, 63220), new L2CharPosition(104766, 75571, -1824, 59228),
					new L2CharPosition(105407, 75616, -1629, 731), new L2CharPosition(105877, 75962, -1516, 6619),
					new L2CharPosition(105977, 76358, -1470, 13804), new L2CharPosition(106442, 77197, -1552, 11105),
					new L2CharPosition(106741, 77390, -1580, 5978), new L2CharPosition(108855, 77294, -1920, 65062),
					new L2CharPosition(110687, 77774, -2291, 2672), new L2CharPosition(112168, 78547, -2561, 5017),
			},
			// Giran -> Oren
			new L2CharPosition[]
			{
					new L2CharPosition(78471, 140874, -3633, 35850), new L2CharPosition(77051, 140228, -3733, 37221),
					new L2CharPosition(75771, 138580, -3719, 42264), new L2CharPosition(74325, 137754, -3650, 38181),
					new L2CharPosition(73980, 137085, -3620, 44185), new L2CharPosition(74210, 135632, -3660, 50789),
					new L2CharPosition(73674, 134655, -3658, 43918), new L2CharPosition(72119, 134090, -3706, 36403),
					new L2CharPosition(70874, 132266, -3754, 42904), new L2CharPosition(70272, 131636, -3763, 41197),
					new L2CharPosition(70198, 130718, -3728, 48313), new L2CharPosition(70572, 129573, -3736, 52444),
					new L2CharPosition(70618, 128477, -3755, 49589), new L2CharPosition(70335, 127484, -3772, 46256),
					new L2CharPosition(69787, 126509, -3819, 43811), new L2CharPosition(69895, 125699, -3763, 50534),
					new L2CharPosition(70626, 124857, -3593, 56609), new L2CharPosition(70679, 122840, -3608, 49426),
					new L2CharPosition(70127, 121457, -3609, 45190), new L2CharPosition(70288, 120569, -3585, 51022),
					new L2CharPosition(70596, 119724, -3600, 52797), new L2CharPosition(70404, 118127, -3672, 47903),
					new L2CharPosition(69745, 116838, -3591, 44222), new L2CharPosition(68858, 115580, -3536, 42746),
					new L2CharPosition(68322, 115077, -3575, 40628), new L2CharPosition(68149, 114322, -3664, 46802),
					new L2CharPosition(68540, 112894, -3757, 51939), new L2CharPosition(67766, 110662, -3713, 45670),
					new L2CharPosition(66858, 109483, -3695, 42306), new L2CharPosition(66792, 108664, -3689, 48313),
					new L2CharPosition(67401, 107822, -3702, 55683), new L2CharPosition(67820, 105993, -3702, 51500),
					new L2CharPosition(69643, 104845, -3700, 59674), new L2CharPosition(69940, 103608, -3643, 51609),
					new L2CharPosition(69533, 102595, -3709, 45167), new L2CharPosition(68629, 101755, -3714, 40577),
					new L2CharPosition(67803, 99807, -3714, 44968), new L2CharPosition(67647, 98834, -3724, 47493),
					new L2CharPosition(67753, 97166, -3728, 49813), new L2CharPosition(68623, 96207, -3700, 56836),
					new L2CharPosition(70261, 94868, -3627, 58388), new L2CharPosition(72071, 93050, -3448, 57321),
					new L2CharPosition(73128, 92065, -3495, 57711), new L2CharPosition(74154, 91505, -3401, 60324),
					new L2CharPosition(75216, 90382, -3278, 57052), new L2CharPosition(76369, 88851, -3227, 55884),
					new L2CharPosition(77766, 87510, -3393, 57557), new L2CharPosition(78238, 86408, -3571, 53372),
					new L2CharPosition(77543, 78909, -3571, 48188), new L2CharPosition(77589, 76095, -3571, 49322),
					new L2CharPosition(77652, 73865, -3749, 49446), new L2CharPosition(77102, 71911, -3683, 46290),
					new L2CharPosition(75885, 69478, -3730, 44314), new L2CharPosition(75489, 67963, -3716, 46485),
					new L2CharPosition(75275, 66705, -3730, 47394), new L2CharPosition(75400, 65296, -3728, 50074),
					new L2CharPosition(77259, 61336, -3113, 20752),
			},
			// Giran -> Giran Harbor
			new L2CharPosition[]
			{
					new L2CharPosition(80071, 159407, -3326, 8521), new L2CharPosition(81423, 159644, -3268, 1810),
					new L2CharPosition(83626, 160818, -3277, 5106), new L2CharPosition(85147, 162031, -3430, 7021),
					new L2CharPosition(84698, 163243, -3292, 20084), new L2CharPosition(84677, 164239, -3247, 16603),
					new L2CharPosition(83932, 166560, -2910, 19623), new L2CharPosition(83947, 167463, -2870, 16210),
					new L2CharPosition(84590, 168573, -2968, 10907), new L2CharPosition(84669, 169918, -2968, 15772),
					new L2CharPosition(85946, 171652, -2968, 9763), new L2CharPosition(85814, 172418, -2968, 18163),
					new L2CharPosition(85496, 173430, -2968, 19559), new L2CharPosition(83435, 174985, -3074, 26023),
					new L2CharPosition(83141, 178057, -2968, 17291), new L2CharPosition(82914, 178570, -3534, 20729),
					new L2CharPosition(81930, 179362, -3534, 25699), new L2CharPosition(80600, 179853, -3496, 29079),
					new L2CharPosition(78805, 179843, -3484, 32826), new L2CharPosition(78094, 180305, -3527, 26757),
					new L2CharPosition(76852, 181520, -3507, 24690), new L2CharPosition(75863, 181798, -3402, 29909),
					new L2CharPosition(74411, 181715, -3169, 33363), new L2CharPosition(72888, 181302, -3179, 35530),
					new L2CharPosition(70589, 180140, -3052, 37649), new L2CharPosition(68471, 178970, -3118, 38032),
					new L2CharPosition(67677, 177765, -3004, 43075), new L2CharPosition(67656, 176921, -2972, 48892),
					new L2CharPosition(66140, 176213, -2790, 37325), new L2CharPosition(64623, 175699, -2861, 36175),
					new L2CharPosition(63853, 174954, -2879, 40787), new L2CharPosition(62126, 174768, -2812, 33887),
					new L2CharPosition(60676, 174161, -2591, 36903), new L2CharPosition(59696, 174195, -2564, 32406),
					new L2CharPosition(58403, 175507, -2651, 24499), new L2CharPosition(57445, 176042, -2778, 27455),
					new L2CharPosition(55746, 176219, -2997, 31685), new L2CharPosition(54666, 176862, -3125, 27166),
					new L2CharPosition(53960, 177800, -3211, 23113), new L2CharPosition(53793, 178516, -3263, 18774),
					new L2CharPosition(54439, 180437, -3422, 13000), new L2CharPosition(54046, 181337, -3448, 20678),
					new L2CharPosition(52902, 182142, -3387, 26372), new L2CharPosition(51135, 182659, -3323, 29799),
			},
			// HV -> Aden
			new L2CharPosition[]
			{
					new L2CharPosition(118980, 70441, -2893, 56026), new L2CharPosition(121559, 70495, -2896, 218),
					new L2CharPosition(122787, 70381, -2878, 64570), new L2CharPosition(123838, 70487, -2841, 1048),
					new L2CharPosition(124150, 70879, -2836, 9372), new L2CharPosition(124309, 71877, -2861, 14736),
					new L2CharPosition(124569, 72253, -2864, 10073), new L2CharPosition(126134, 72450, -2826, 1306),
					new L2CharPosition(126494, 72845, -2837, 8675), new L2CharPosition(126554, 74645, -2619, 16036),
					new L2CharPosition(126386, 76325, -2619, 17423), new L2CharPosition(126545, 76765, -2047, 12767),
					new L2CharPosition(126851, 76892, -1967, 4103), new L2CharPosition(128661, 76815, -1798, 65092),
					new L2CharPosition(129149, 76351, -1708, 57606), new L2CharPosition(130595, 76136, -1323, 63996),
					new L2CharPosition(131729, 76128, -1359, 65462), new L2CharPosition(132940, 76476, -1536, 2918),
					new L2CharPosition(133699, 76281, -1848, 62912), new L2CharPosition(134122, 75401, -2263, 53825),
					new L2CharPosition(134292, 74651, -2539, 51476), new L2CharPosition(133872, 73595, -3003, 45203),
					new L2CharPosition(133948, 72973, -3128, 50420), new L2CharPosition(134626, 71890, -3327, 54986),
					new L2CharPosition(135704, 70926, -3327, 57925), new L2CharPosition(136548, 70715, -3327, 62980),
					new L2CharPosition(137312, 70511, -3327, 62814), new L2CharPosition(137835, 69379, -3689, 53666),
					new L2CharPosition(137856, 67714, -3672, 49283), new L2CharPosition(136402, 66138, -3728, 41379),
					new L2CharPosition(136179, 65198, -3707, 46722), new L2CharPosition(137462, 63285, -3655, 55313),
					new L2CharPosition(138084, 61720, -3710, 53097), new L2CharPosition(137447, 60579, -3556, 43841),
					new L2CharPosition(135869, 58925, -3556, 41205), new L2CharPosition(134910, 57837, -3374, 41616),
					new L2CharPosition(134943, 56664, -3516, 49445), new L2CharPosition(135600, 55679, -3646, 55287),
					new L2CharPosition(138394, 53692, -3561, 59088), new L2CharPosition(138627, 52820, -3445, 51875),
					new L2CharPosition(137040, 51038, -3583, 41563), new L2CharPosition(136752, 49158, -3623, 47566),
					new L2CharPosition(137480, 47646, -3613, 53832), new L2CharPosition(140137, 46791, -3531, 62288),
					new L2CharPosition(142045, 48019, -3531, 5964), new L2CharPosition(142500, 46847, -3409, 53014),
					new L2CharPosition(142120, 44992, -3344, 47044), new L2CharPosition(141339, 43944, -3406, 42471),
					new L2CharPosition(138860, 43141, -3574, 36035), new L2CharPosition(137065, 42884, -3558, 34251),
					new L2CharPosition(136006, 42083, -3571, 39522), new L2CharPosition(135695, 41115, -3624, 45909),
					new L2CharPosition(136330, 38758, -3636, 51896), new L2CharPosition(137523, 38183, -3523, 60851),
					new L2CharPosition(139151, 38092, -3218, 64953), new L2CharPosition(140172, 37321, -3135, 58789),
					new L2CharPosition(141048, 36028, -3129, 55362), new L2CharPosition(143360, 35621, -3049, 63718),
					new L2CharPosition(145540, 34546, -2823, 60757),
			},
			// Aden Castle -> Devastated Castle
			new L2CharPosition[]
			{
					new L2CharPosition(154937, 5247, -4451, 30510), new L2CharPosition(158589, 3057, -4395, 59901),
					new L2CharPosition(159221, 292, -3974, 50997), new L2CharPosition(158868, -1469, -3896, 47088),
					new L2CharPosition(158719, -4292, -3965, 48601), new L2CharPosition(159785, -5769, -3923, 55672),
					new L2CharPosition(161111, -6506, -3661, 60244), new L2CharPosition(161187, -7592, -3450, 49880),
					new L2CharPosition(160750, -8773, -3450, 45455), new L2CharPosition(160705, -9774, -3321, 48683),
					new L2CharPosition(160335, -11606, -3225, 47073), new L2CharPosition(160408, -13268, -3225, 49609),
					new L2CharPosition(160045, -13683, -3225, 41656), new L2CharPosition(159492, -14427, -3225, 42485),
					new L2CharPosition(159601, -16131, -2301, 49819), new L2CharPosition(160112, -17608, -2951, 53104),
					new L2CharPosition(160073, -19422, -3225, 48738), new L2CharPosition(160744, -19286, -1665, 1532),
					new L2CharPosition(163193, -20323, -1665, 61358), new L2CharPosition(164746, -20900, -2308, 61824),
					new L2CharPosition(166683, -21080, -2915, 64797), new L2CharPosition(168644, -20460, -1665, 4280),
					new L2CharPosition(169561, -20012, -2703, 4738), new L2CharPosition(171584, -19950, -3415, 64233),
					new L2CharPosition(174005, -20468, -3572, 63337), new L2CharPosition(175412, -21462, -3691, 59120),
					new L2CharPosition(177741, -21998, -3454, 63176), new L2CharPosition(180401, -20847, -2959, 4259),
					new L2CharPosition(181101, -19858, -2804, 9959), new L2CharPosition(181739, -18966, -2802, 9907),
					new L2CharPosition(182939, -18424, -2795, 4424), new L2CharPosition(183041, -16490, -2710, 15834),
					new L2CharPosition(181846, -15950, -2442, 28341),
			},/*
				new L2CharPosition[] {
					
				},*/
	};
	private static final String ATTACKED = "We are under attack!";
	private static final String KILLED = "For the glory of Gracia!";
	
	private final FastMap<Integer, CaravanInfo> _progress;
	
	public CaravanMaster()
	{
		super(-1, "caravan_master", "ai");
		addStartNpc(SEER);
		addFirstTalkId(SEER);
		addTalkId(SEER);
		_progress = new FastMap<Integer, CaravanInfo>().setShared(true);
		registerMobs(MASTER, TRAVELLER);
		for (int i = 0; i < PATH.length; i++)
			addSpawn(MASTER, PATH[i][0].x, PATH[i][0].y, PATH[i][0].z, PATH[i][0].heading, false, 0);
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		if (event.startsWith("panic"))
		{
			CaravanInfo ci = getInfo(npc);
			if (ci == null)
			{
				cancelQuestTimer(event, npc, player);
				return null;
			}
			if (DEBUG)
				_log.info(event + " combat status: " + ci.isInCombat());
			L2MonsterInstance master = ci.getMaster();
			if (!master.isInCombat())
			{
				cancelQuestTimer(event, npc, player);
				master.setWalking();
				for (L2MonsterInstance minion : master.getSpawnedMinions())
				{
					minion.setWalking();
					minion.clearAggroList();
					minion.getAI().setIntention(CtrlIntention.AI_INTENTION_ACTIVE);
				}
				master.callMinions();
				ci.updateInCombat();
				int trg = 0;
				double dist = Double.MAX_VALUE;
				for (int i = 0; i <= PATH[ci.getPath()].length; i++)
				{
					L2CharPosition cp = PATH[ci.getPath()][i];
					double ndist = L2Math.calculateDistance(cp.x, cp.y, master.getX(), master.getY());
					if (ndist < dist)
					{
						dist = ndist;
						trg = i;
					}
				}
				ci.setTarget(trg);
				onArrived(ci.getMaster());
			}
		}
		else if (event.startsWith("respawn"))
		{
			if (DEBUG)
				_log.info(event);
			int i = Integer.parseInt(event.substring(8));
			addSpawn(MASTER, PATH[i][0].x, PATH[i][0].y, PATH[i][0].z, PATH[i][0].heading, false, 0);
		}
		else
		{
			int path = -1;
			try
			{
				path = Integer.parseInt(event);
			}
			catch (Exception e)
			{
			}
			if (path < 0 || path > PATH.length)
				return null;
			if (!player.reduceAdena("Caravan", 10, npc, true))
				return "<html><body>Did I forget to tell you that I also hate beggars?</body></html>";
			L2CharPosition pos = Calc.getRandomFromArray(PATH[path]);
			player.teleToLocation(pos.x, pos.y, pos.z);
			player.setInstanceId(0);
			L2TextBuilder tb = new L2TextBuilder();
			tb.append("<html><body><center><br>You should search along the ");
			tb.append("road of <br><font color=\"LEVEL\">");
			tb.append(getPathName(path));
			tb.append("</font></center></body></html>");
			return tb.moveToString();
		}
		return null;
	}
	
	@Override
	public String onArrived(L2Character character)
	{
		if (!(character instanceof L2Attackable))
			return null;
		L2Attackable master = (L2Attackable) character;
		if (master.getNpcId() != MASTER)
			return null;
		CaravanInfo ci = getInfo(master);
		if (ci != null)
		{
			if (DEBUG)
				_log.info("NPC " + character.getObjectId() + " arrived.");
			L2CharPosition pos = PATH[ci.getPath()][ci.getTarget()];
			// when under attack, do nothing special.
			// if (ci.isInCombat()/* || character.getAI().getIntentionArg0() == pos*/)
			// return null;
			if (DEBUG)
			{
				_log.info("Not in combat, current target " + ci.getTarget());
				_log.info("Target X: " + pos.x + " Y: " + pos.y);
				_log.info("Currnt X: " + character.getX() + " Y: " + character.getY());
			}
			if (isAtTargetPos(character, pos))
			{
				ci.nextTarget();
				pos = PATH[ci.getPath()][ci.getTarget()];
			}
			if (DEBUG)
				_log.info("Will move to target " + ci.getTarget());
			// character.getAI().setIntention(CtrlIntention.AI_INTENTION_MOVE_TO, pos);
			master.setMoveAroundPos(pos);
		}
		return null;
	}
	
	@Override
	public String onAttack(L2Npc npc, L2PcInstance attacker, int damage, boolean isPet, L2Skill skill)
	{
		CaravanInfo ci = getInfo(npc);
		if (ci != null && !ci.isInCombat())
		{
			ci.setInCombat(true);
			L2MonsterInstance master = ci.getMaster();
			master.broadcastPacket(new ChatMessage(master, Chat.LOCAL, ATTACKED));
			master.setRunning();
			for (L2MonsterInstance minion : master.getSpawnedMinions())
				minion.setRunning();
			startQuestTimer("panic_" + master.getObjectId(), 5000, master, null, true);
			if (DEBUG)
				_log.info("NPC " + npc.getObjectId() + " (master " + master.getObjectId() + ") attacked");
		}
		return null;
	}
	
	@Override
	public String onFirstTalk(L2Npc npc, L2PcInstance player)
	{
		if (npc.getNpcId() != SEER)
			return null;
		L2TextBuilder tb = new L2TextBuilder();
		tb.append("<html><title>NoobWars.Gr legacy</title><body>");
		tb.append("Dear adventurer,<br1>");
		tb.append("Aden continent is swarming with D grader n00bs. They form up n00b t3am5 ");
		tb.append("and travel around lead by 1337357 C graders.<br1>");
		tb.append("I want them slaughtered because I hate them; ");
		tb.append("<font color=\"LEVEL\">You want them dead because they have materials.");
		tb.append(" I can drop you off somewhere along their path for a small fee of 10 adena.");
		tb.append("</font><br1><table><tr><td>Name</td><td>Status</td></tr>");
		for (int i = 0; i < PATH.length; i++)
		{
			tb.append("<tr><td>");
			tb.append(getPathName(i));
			tb.append("</td><td>");
			if (isRaidable(i))
			{
				tb.append("<a action=\"bypass -h Quest caravan_master ");
				tb.append(i);
				tb.append("\">Teleport</a>");
			}
			else
			{
				tb.append("Dealt with");
			}
			tb.append("</td></tr>");
		}
		tb.append("</table>");
		tb.append("</body></html>");
		return tb.moveToString();
	}
	
	@Override
	public String onKill(L2Npc npc, L2PcInstance killer, boolean isPet)
	{
		if (npc.getNpcId() == MASTER)
		{
			CaravanInfo ci = _progress.remove(npc.getObjectId());
			if (ci == null)
				return null;
			npc.broadcastPacket(new ChatMessage(npc, Chat.LOCAL, KILLED));
			int path = ci.getPath();
			startQuestTimer("respawn_" + path, CARAVAN_RESPAWN, null, null, false);
			if (DEBUG)
				_log.info("NPC " + npc.getObjectId() + " is dead, init respawn timer for path " + path);
		}
		return null;
	}
	
	@Override
	public String onSpawn(L2Npc npc)
	{
		if (npc.getNpcId() == MASTER)
		{
			int path = -1;
			for (int i = 0; i < PATH.length; i++)
			{
				if (isAtTargetPos(npc, PATH[i][0]))
				{
					path = i;
					break;
				}
			}
			if (path == -1)
			{
				_log.warn("Failed to spawn.");
				npc.doDie(null);
				return null;
			}
			CaravanInfo ci = new CaravanInfo(path, npc);
			CaravanInfo c2 = _progress.put(npc.getObjectId(), ci);
			if (c2 != null && !c2.getMaster().isDead())
			{
				_log.warn("Caravan replaced, taking care of the old one?!!");
				c2.getMaster().doDie(null);
			}
			L2CharPosition pos = PATH[ci.getPath()][ci.getTarget()];
			npc.setIsNoRndWalk(true);
			// npc.getAI().setIntention(CtrlIntention.AI_INTENTION_MOVE_TO, pos);
			L2Attackable master = (L2Attackable) npc;
			master.setMoveAroundPos(pos);
			if (DEBUG)
				_log.info("NPC " + npc.getObjectId() + " assigned to path " + ci.getPath());
		}
		return null;
	}
	
	private boolean isAtTargetPos(L2Character character, L2CharPosition pos)
	{
		if (Config.GEODATA == 0 && pos.x == character.getX() && pos.y == character.getY())
			return true;
		else if (L2Math.calculateDistance(pos.x, pos.y, character.getX(), character.getY()) < MAX_GEO_PLAN_DIST)
			return true;
		return false;
	}
	
	private CaravanInfo getInfo(L2Npc npc)
	{
		if (npc.getNpcId() == MASTER)
			return _progress.get(npc.getObjectId());
		else if (npc.getNpcId() == TRAVELLER)
			for (FastMap.Entry<Integer, CaravanInfo> e = _progress.head(), end = _progress.tail(); (e = e.getNext()) != end;)
			{
				CaravanInfo ci = e.getValue();
				if (ci.getMaster().getSpawnedMinions().contains(npc))
					return ci;
			}
		return null;
	}
	
	private boolean isRaidable(int path)
	{
		for (FastMap.Entry<Integer, CaravanInfo> e = _progress.head(), end = _progress.tail(); (e = e.getNext()) != end;)
			if (e.getValue().getPath() == path)
				return true;
		return false;
	}
	
	private String getPathName(int path)
	{
		switch (path)
		{
		case 0:
			return "Gludin<->Gludio";
		case 1:
			return "Gludio<->Dark Elven Village";
		case 2:
			return "Gludio<->Elven Village";
		case 3:
			return "Oren<->Hunters Village";
		case 4:
			return "Giran<->Oren";
		case 5:
			return "Giran<->Giran Harbor";
		case 6:
			return "Hunters Village<->Aden";
		case 7:
			return "Aden Castle<->Devastated Castle";
		default:
			return "??? <-> ???";
		}
	}
	
	public static void main(String[] args)
	{
		new CaravanMaster();
	}
	
	private static class CaravanInfo
	{
		private final int _path;
		private final L2MonsterInstance _master;
		private int _target;
		private boolean _inverted;
		private boolean _combat;
		
		public CaravanInfo(int path, L2Npc master)
		{
			_path = path;
			_master = (L2MonsterInstance) master;
			_target = 1;
			_combat = false;
		}
		
		public int getPath()
		{
			return _path;
		}
		
		public L2MonsterInstance getMaster()
		{
			return _master;
		}
		
		public int getTarget()
		{
			return _target;
		}
		
		public void nextTarget()
		{
			if (isInverted())
				_target--;
			else
				_target++;
			if (_target == 0 || _target == PATH[getPath()].length)
			{
				setInverted(!isInverted());
				nextTarget();
			}
		}
		
		public void setTarget(int target)
		{
			_target = target;
		}
		
		public boolean isInCombat()
		{
			return _combat;
		}
		
		public void setInCombat(boolean combat)
		{
			_combat = combat;
		}
		
		private void updateInCombat()
		{
			L2MonsterInstance master = getMaster();
			if (!isInCombat() || master.isInCombat())
				return;
			for (L2MonsterInstance minion : master.getSpawnedMinions())
				if (minion.isInCombat())
					return;
			setInCombat(false);
		}
		
		public boolean isInverted()
		{
			return _inverted;
		}
		
		public void setInverted(boolean inverted)
		{
			_inverted = inverted;
		}
	}
}
