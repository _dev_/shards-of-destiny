package custom.tutorial;

import com.l2jfree.gameserver.model.quest.jython.QuestJython;

/**
 * @author savormix
 * @since 2010.10.18
 */
public class GlobalTutorial extends QuestJython
{
	private static final String TUTORIAL_GLOBAL = "90000_GlobalTutorial";
	
	public GlobalTutorial()
	{
		super(-1, TUTORIAL_GLOBAL, "custom");
		// TODO Auto-generated constructor stub
	}
	
	// make faction preps
	// make base loc
	// make tut (listeners)
	// merge shops
	// make weapon MW shops
	
	public static void main(String[] args)
	{
		new GlobalTutorial();
	}
}
