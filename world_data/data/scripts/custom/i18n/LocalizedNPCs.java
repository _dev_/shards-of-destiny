/*
 * MISSING LICENSING INFO
 */
package custom.i18n;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.sod.L2Htm;
import org.sod.i18n.Internationalization;

import com.l2jfree.gameserver.communitybbs.Manager.SettingsBBSManager;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.quest.Quest;
import com.l2jfree.gameserver.network.client.packets.sendable.NpcHtmlMessage;
import com.l2jfree.lang.L2TextBuilder;

/**
 * When NPC htmls are generated on the fly, we can afford to have generic header/footer for all NPCs without
 * an expensive maintenance cost.
 * 
 * @author savormix
 */
public class LocalizedNPCs extends Quest
{
	private static final int WEAPONS_NPC = 90001;
	private static final int ARMOR_NPC = 90002;
	private static final int MISC_NPC = 90004;
	private static final int UPGRADE_NPC = 90017;
	private static final int MASTERWORK_NPC = 90018;
	private static final int RECIPE_NPC = 90062;
	private static final int AUGMENTATION_NPC = 90071;
	private static final int SYMBOL_MAKER_NPC = 90072;
	private static final int VILLAGE_MASTER_NPC = 90073;
	
	private final Map<Integer, String[]> _chat;
	
	private LocalizedNPCs(int questId, String name, String descr)
	{
		super(questId, name, descr);
		
		_chat = new HashMap<Integer, String[]>();
		generateWeaponShop();
		generateArmorShop();
		generateRecipeShop();
		generateMiscShop();
		generateUpgradeShop();
		generateMasterworkShop();
		generateAugmentationManager();
		generateDyeManager();
		generateVillageMaster();
		
		addFirstTalkId(WEAPONS_NPC, ARMOR_NPC, MISC_NPC, UPGRADE_NPC, MASTERWORK_NPC, RECIPE_NPC, AUGMENTATION_NPC,
				SYMBOL_MAKER_NPC, VILLAGE_MASTER_NPC);
		
		_log.info("Loaded " + _chat.size() + " localized NPCs.");
	}
	
	@Override
	public String onFirstTalk(L2Npc npc, L2PcInstance player)
	{
		String[] locald = _chat.get(npc.getNpcId());
		if (locald == null)
		{
			npc.showChatWindow(player);
			return null;
		}
		
		try
		{
			NpcHtmlMessage htm = new NpcHtmlMessage(npc, locald[player.getSoDPlayer().getSettings().getLocaleIndex()]);
			htm.replace("%objectId%", String.valueOf(npc.getObjectId()));
			player.sendPacket(htm);
		}
		catch (RuntimeException e)
		{
		}
		return null;
	}
	
	private void addFirstTalkId(int... npcs)
	{
		for (int npc : npcs)
			addFirstTalkId(npc);
	}
	
	public static void main(String[] args)
	{
		new LocalizedNPCs(-1, "_LocalizedNPCs", "custom");
	}
	
	private void generateWeaponShop()
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale[] locs = SettingsBBSManager.getInstance().getAllLocales();
		String[] chat = new String[locs.length];
		for (int i = 0; i < locs.length; i++)
		{
			Locale loc = locs[i];
			
			L2TextBuilder tb = new L2TextBuilder();
			L2Htm.writeHeader(tb, i18n.get(loc, "NPC_WEAPON_SHOP_TITLE"));
			tb.append(i18n.get(loc, "NPC_WEAPON_SHOP_DISCLAIMER"));
			tb.append("<br><table><tr><td align=\"center\">");
			
			L2Htm.writeImage(tb, "icon.weapon_sword_of_miracle_i00");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeImage(tb, "icon.weapon_naga_storm_i00");
			
			tb.append("</td></tr><tr><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90001001, 100, 25, false, "A Grade [C3-5]");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90001002, 100, 25, false, "A Grade [Int]");
			tb.append("</td></tr><tr><td align=\"center\">");
			
			L2Htm.writeImage(tb, "icon.weapon_hazard_bow_i00");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeImage(tb, "icon.weapon_yaksa_mace_i00");
			
			tb.append("</td></tr><tr><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90001003, 100, 25, false, "B Grade");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90001004, 100, 25, false, "C Grade");
			tb.append("</td></tr></table>");
			L2Htm.writeFooter(tb);
			
			chat[i] = tb.moveToString();
		}
		_chat.put(WEAPONS_NPC, chat);
	}
	
	private void generateArmorShop()
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale[] locs = SettingsBBSManager.getInstance().getAllLocales();
		String[] chat = new String[locs.length];
		for (int i = 0; i < locs.length; i++)
		{
			Locale loc = locs[i];
			
			L2TextBuilder tb = new L2TextBuilder();
			L2Htm.writeHeader(tb, i18n.get(loc, "NPC_ARMOR_SHOP_TITLE"));
			tb.append(i18n.get(loc, "NPC_ARMOR_SHOP_DISCLAIMER"));
			tb.append("<br><table><tr><td align=\"center\">");
			
			L2Htm.writeImage(tb, "icon.armor_t80_ul_i00");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeImage(tb, "icon.armor_t84_ul_i00");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeImage(tb, "icon.armor_t76_ul_i00");
			
			tb.append("</td></tr><tr><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90002001, 80, 25, false, "A Heavy");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90002002, 80, 25, false, "A Light");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90002003, 80, 25, false, "A Robe");
			tb.append("</td></tr><tr><td align=\"center\">");
			
			L2Htm.writeImage(tb, "icon.armor_t71_ul_i00");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeImage(tb, "icon.armor_t69_ul_i00");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeImage(tb, "icon.armor_t59_ul_i00");
			
			tb.append("</td></tr><tr><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90002004, 80, 25, false, "B Heavy");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90002005, 80, 25, false, "B Light");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90002006, 80, 25, false, "B Robe");
			tb.append("</td></tr><tr><td align=\"center\">");
			
			L2Htm.writeImage(tb, "icon.armor_t62_ul_i00");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeImage(tb, "icon.armor_t21_ul_i00");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeImage(tb, "icon.armor_divine_tunic_i00");
			
			tb.append("</td></tr><tr><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90002007, 80, 25, false, "C Heavy");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90002008, 80, 25, false, "C Light");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90002009, 80, 25, false, "C Robe");
			tb.append("</td></tr></table><br>");
			
			tb.append(i18n.get(loc, "NPC_JEWEL_SHOP_DISCLAIMER"));
			tb.append("<br><table><tr><td align=\"center\">");
			
			L2Htm.writeImage(tb, "icon.accessary_inferno_necklace_i00");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeImage(tb, "icon.accessary_earing_of_black_ore_i00");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeImage(tb, "icon.accessary_ring_of_binding_i00");
			
			tb.append("</td></tr><tr><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90003001, 80, 25, false, "A Grade");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90003002, 80, 25, false, "B Grade");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90003003, 80, 25, false, "C Grade");
			tb.append("</td></tr></table>");
			L2Htm.writeFooter(tb);
			
			chat[i] = tb.moveToString();
		}
		_chat.put(ARMOR_NPC, chat);
	}
	
	private void generateRecipeShop()
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale[] locs = SettingsBBSManager.getInstance().getAllLocales();
		String[] chat = new String[locs.length];
		for (int i = 0; i < locs.length; i++)
		{
			Locale loc = locs[i];
			
			L2TextBuilder tb = new L2TextBuilder();
			L2Htm.writeHeader(tb, i18n.get(loc, "NPC_RECIPE_SHOP_TITLE"));
			tb.append(i18n.get(loc, "NPC_RECIPE_SHOP_DISCLAIMER"));
			tb.append("<br><table><tr><td align=\"center\">");
			
			L2Htm.writeImage(tb, "icon.etc_recipe_white_i00");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeImage(tb, "icon.etc_recipe_blue_i00");
			
			tb.append("</td></tr><tr><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90062001, 100, 25, false, i18n.get(loc, "NPC_RECIPE_SHOP_TYPE_COMMON"));
			tb.append("</td><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90062002, 100, 25, false, i18n.get(loc, "NPC_RECIPE_SHOP_TYPE_DWARVEN"));
			tb.append("</td></tr></table>");
			L2Htm.writeFooter(tb);
			
			chat[i] = tb.moveToString();
		}
		_chat.put(RECIPE_NPC, chat);
	}
	
	private void generateMiscShop()
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale[] locs = SettingsBBSManager.getInstance().getAllLocales();
		String[] chat = new String[locs.length];
		for (int i = 0; i < locs.length; i++)
		{
			Locale loc = locs[i];
			
			L2TextBuilder tb = new L2TextBuilder();
			L2Htm.writeHeader(tb, i18n.get(loc, "NPC_MISC_SHOP_TITLE"));
			tb.append(i18n.get(loc, "NPC_MISC_SHOP_DISCLAIMER"));
			tb.append("<br><table><tr><td align=\"center\">");
			
			L2Htm.writeImage(tb, "icon.etc_spirit_bullet_red_i00");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeImage(tb, "icon.etc_elixer_refresh_i00");
			
			tb.append("</td></tr><tr><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90004001, 100, 25, false, i18n.get(loc, "NPC_MISC_SHOP_SOULSHOTS"));
			tb.append("</td><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90004002, 100, 25, false, i18n.get(loc, "NPC_MISC_SHOP_POTIONS"));
			tb.append("</td></tr><tr><td align=\"center\">");
			
			L2Htm.writeImage(tb, "BranchSys.icon.br_black_gem_mask_i00");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeImage(tb, "icon.etc_dex_symbol_i01");
			
			tb.append("</td></tr><tr><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90004003, 100, 25, false, i18n.get(loc, "NPC_MISC_SHOP_DECO"));
			tb.append("</td><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90004004, 100, 25, false, i18n.get(loc, "NPC_MISC_SHOP_DYES"));
			tb.append("</td></tr><tr><td align=\"center\">");
			
			L2Htm.writeImage(tb, "icon.etc_piece_bone_red_i00");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeImage(tb, "icon.etc_crystal_silver_i00");
			
			tb.append("</td></tr><tr><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90004005, 100, 25, false, i18n.get(loc, "NPC_MISC_SHOP_CONSUMABLES"));
			tb.append("</td><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90004006, 100, 25, false, i18n.get(loc, "NPC_MISC_SHOP_CRYSTALS"));
			tb.append("</td></tr></table>");
			L2Htm.writeFooter(tb);
			
			chat[i] = tb.moveToString();
		}
		_chat.put(MISC_NPC, chat);
	}
	
	private void generateUpgradeShop()
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale[] locs = SettingsBBSManager.getInstance().getAllLocales();
		String[] chat = new String[locs.length];
		for (int i = 0; i < locs.length; i++)
		{
			Locale loc = locs[i];
			
			L2TextBuilder tb = new L2TextBuilder();
			L2Htm.writeHeader(tb, i18n.get(loc, "NPC_UPGRADE_SHOP_TITLE"));
			tb.append(i18n.get(loc, "NPC_UPGRADE_SHOP_DISCLAIMER"));
			tb.append("<br><table><tr><td align=\"center\">");
			
			L2Htm.writeImage(tb, "icon.ench_wp_stone_i01");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeImage(tb, "icon.etc_holy_crystal_i00");
			
			tb.append("</td></tr><tr><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90017001, 100, 25, false,
					i18n.get(loc, "NPC_UPGRADE_SHOP_ENCHANT_CHANCE"));
			tb.append("</td><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90017003, 100, 25, false,
					i18n.get(loc, "NPC_UPGRADE_SHOP_ELEMENTAL_STONES"));
			tb.append("</td></tr><tr><td align=\"center\">");
			
			L2Htm.writeImage(tb, "icon.etc_mineral_special_i03");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeImage(tb, "icon.etc_mineral_general_i03");
			
			tb.append("</td></tr><tr><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90017004, 100, 25, false,
					i18n.get(loc, "NPC_UPGRADE_SHOP_LIFESTONES_WEAPON"));
			tb.append("</td><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90017005, 100, 25, false,
					i18n.get(loc, "NPC_UPGRADE_SHOP_LIFESTONES_JEWEL"));
			tb.append("</td></tr><tr><td align=\"center\">");
			
			L2Htm.writeImage(tb, "icon.etc_codex_of_giant_i00");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeImage(tb, "icon.etc_spell_books_element_i00");
			
			tb.append("</td></tr><tr><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90017002, 100, 25, false,
					i18n.get(loc, "NPC_UPGRADE_SHOP_GIANT_CODEXES"));
			tb.append("</td><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90017006, 100, 25, false,
					i18n.get(loc, "NPC_UPGRADE_SHOP_FORGOTTEN_SCROLLS"));
			tb.append("</td></tr></table>");
			L2Htm.writeFooter(tb);
			
			chat[i] = tb.moveToString();
		}
		_chat.put(UPGRADE_NPC, chat);
	}
	
	private void generateMasterworkShop()
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale[] locs = SettingsBBSManager.getInstance().getAllLocales();
		String[] chat = new String[locs.length];
		for (int i = 0; i < locs.length; i++)
		{
			Locale loc = locs[i];
			
			L2TextBuilder tb = new L2TextBuilder();
			L2Htm.writeHeader(tb, i18n.get(loc, "NPC_MASTERWORK_SHOP_TITLE"));
			tb.append(i18n.get(loc, "NPC_MASTERWORK_SHOP_DISCLAIMER"));
			tb.append("<br><table><tr><td align=\"center\">");
			
			L2Htm.writeImage(tb, "icon.armor_t80_ul_i00");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeImage(tb, "icon.armor_t84_ul_i00");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeImage(tb, "icon.armor_t76_ul_i00");
			
			tb.append("</td></tr><tr><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90018001, 80, 25, false, "S Grade");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90018002, 80, 25, false, "A Grade");
			tb.append("</td><td align=\"center\">");
			L2Htm.writeExchangeButton(tb, null, 90018003, 80, 25, false, "B Grade");
			tb.append("</td></tr></table>");
			L2Htm.writeFooter(tb);
			
			chat[i] = tb.moveToString();
		}
		_chat.put(MASTERWORK_NPC, chat);
	}
	
	private void generateAugmentationManager()
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale[] locs = SettingsBBSManager.getInstance().getAllLocales();
		String[] chat = new String[locs.length];
		for (int i = 0; i < locs.length; i++)
		{
			Locale loc = locs[i];
			
			L2TextBuilder tb = new L2TextBuilder();
			L2Htm.writeHeader(tb, i18n.get(loc, "NPC_REFINER_TITLE"));
			L2Htm.writeExchangeButton(tb, null, 1008, 250, 40, false, i18n.get(loc, "NPC_REFINER_TRADE"));
			L2Htm.writeNpcButton(tb, null, "Augment", 1, 250, 40, false, i18n.get(loc, "NPC_REFINER_AUGMENT"));
			L2Htm.writeNpcButton(tb, null, "Augment", 2, 250, 40, false, i18n.get(loc, "NPC_REFINER_DEAUGMENT"));
			L2Htm.writeFooter(tb);
			
			chat[i] = tb.moveToString();
		}
		_chat.put(AUGMENTATION_NPC, chat);
	}
	
	private void generateDyeManager()
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale[] locs = SettingsBBSManager.getInstance().getAllLocales();
		String[] chat = new String[locs.length];
		for (int i = 0; i < locs.length; i++)
		{
			Locale loc = locs[i];
			
			L2TextBuilder tb = new L2TextBuilder();
			L2Htm.writeHeader(tb, i18n.get(loc, "NPC_DYER_TITLE"));
			tb.append(i18n.get(loc, "NPC_DYER_DISCLAIMER"));
			tb.append("<br>");
			L2Htm.writeNpcButton(tb, null, "Draw", null, 250, 40, true, i18n.get(loc, "NPC_DYER_ADD_DYE"));
			L2Htm.writeNpcButton(tb, null, "RemoveList", null, 250, 40, true, i18n.get(loc, "NPC_DYER_REMOVE_DYE"));
			L2Htm.writeFooter(tb);
			
			chat[i] = tb.moveToString();
		}
		_chat.put(SYMBOL_MAKER_NPC, chat);
	}
	
	private void generateVillageMaster()
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale[] locs = SettingsBBSManager.getInstance().getAllLocales();
		String[] chat = new String[locs.length];
		for (int i = 0; i < locs.length; i++)
		{
			Locale loc = locs[i];
			
			L2TextBuilder tb = new L2TextBuilder();
			L2Htm.writeHeader(tb, i18n.get(loc, "NPC_VILLAGE_MASTER_TITLE"));
			L2Htm.writeNpcButton(tb, null, "Subclass", 0, 250, 40, true, i18n.get(loc, "NPC_VILLAGE_MASTER_SUBCLASS"));
			L2Htm.writeNpcButton(tb, null, "Quest", "9000_clan", 250, 40, true,
					i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN"));
			L2Htm.writeNpcButton(tb, null, "Quest", "9001_alliance", 250, 40, true,
					i18n.get(loc, "NPC_VILLAGE_MASTER_ALLIANCE"));
			L2Htm.writeFooter(tb);
			
			chat[i] = tb.moveToString();
		}
		_chat.put(VILLAGE_MASTER_NPC, chat);
	}
}
