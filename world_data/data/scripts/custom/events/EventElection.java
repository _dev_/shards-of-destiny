/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package custom.events;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sod.manager.GlobalEventManager;
import org.sod.manager.GlobalEventManager.Event;
import org.sod.manager.GlobalEventManager.EventGroup;
import org.sod.model.event.global.GlobalEvent;
import org.sod.model.event.global.Team;
import org.sod.model.event.global.impl.CaptureTheFlag;
import org.sod.model.event.global.impl.CaptureTheFlag.FlagTeam;
import org.sod.model.event.global.impl.Deathmatch;
import org.sod.model.event.global.impl.GolemDispute;
import org.sod.model.event.global.impl.GolemDispute.DisputeTeam;
import org.sod.model.event.global.impl.LastManStanding;
import org.sod.model.event.global.impl.LureTheGremlin;
import org.sod.model.event.global.impl.LureTheGremlin.LurerTeam;
import org.sod.model.event.global.impl.Regicide;
import org.sod.model.event.global.impl.Regicide.GuardianTeam;
import org.sod.model.event.global.impl.Regicide.ZergTeam;
import org.sod.model.event.global.impl.SlaughterHouse;
import org.sod.model.event.global.impl.TagTheMob;
import org.sod.model.event.global.impl.TeamDeathmatch;
import org.sod.model.event.global.impl.TeamKingOfTheHill;

import com.l2jfree.gameserver.model.Location;

/**
 * A script to register events in the event voting system.
 * 
 * @author savormix
 */
public final class EventElection
{
	private static final Log _log = LogFactory.getLog(EventElection.class);
	
	private EventElection()
	{
	}
	
	public static void main(String[] args)
	{
		GlobalEventManager gem = GlobalEventManager.getInstance();
		// please do not remove before altering the checked
		// method to support seamless removal
		if (!gem.unregisterAllEvents())
		{
			_log.warn("Could not reload EventElection, as the system is not idle!");
			return;
		}
		// Random event
		EventGroup eg = new EventGroup(1, "Random");
		// LtG
		GlobalEvent ge = new LureTheGremlin(50, false, new LurerTeam(new Location(11660, -49145, -3005),
				LureTheGremlin.COLOR_RED, 90001, LureTheGremlin.CORNER_NPC_RED), new LurerTeam(new Location(12660,
				-49150, -3005), LureTheGremlin.COLOR_BLUE, 90002, LureTheGremlin.CORNER_NPC_BLUE));
		Event e = new Event(ge);
		eg.add(e); // add to random event
		gem.registerStandaloneEvent(e, false);
		// LMS
		ge = new LastManStanding(false);
		e = new Event(80, ge);
		eg.add(e); // add to random event
		gem.registerStandaloneEvent(e, false);
		// DM
		ge = new Deathmatch(50, false);
		e = new Event(20, "Deathmatch", ge);
		eg.add(e); // add to random event
		gem.registerStandaloneEvent(e, false);
		// TvT
		ge = new TeamDeathmatch(500, false, new Team(-79729, 111361, -4895), new Team(-75268, 111975, -4895));
		e = new Event(2, ge);
		eg.add(e); // add to random event
		gem.registerStandaloneEvent(e, false);
		// CTF
		ge = new CaptureTheFlag(10, false, new FlagTeam(new Location(-80801, 86531, -5155), 0x660000, new Location(
				-81358, 86528, -5155)), new FlagTeam(new Location(-76337, 87139, -5155), 0x66FF, new Location(-75992,
				87133, -5155)));
		e = new Event(2, ge);
		eg.add(e); // add to random event
		gem.registerStandaloneEvent(e, false);
		// GG
		ge = new GolemDispute(20, false, new DisputeTeam(new Location(-106750, 239405, -3640),
				GolemDispute.COLOR_PURPLE, GolemDispute.GOLEM_PURPLE, new Location(-106890, 239575, -3635)),
				new DisputeTeam(new Location(-105190, 237860, -3615), GolemDispute.COLOR_ORANGE,
						GolemDispute.GOLEM_ORANGE, new Location(-105040, 237725, -3600)),
				GolemDispute.GOLDEN_GOLEM_NPC, new Location(-105980, 238660, -3680));
		e = new Event(8, ge);
		eg.add(e); // add to random event
		gem.registerStandaloneEvent(e, false);
		// Regicide
		ge = new Regicide(true, new GuardianTeam(Regicide.KING_NPC, new Location(147445, 22750, -1990),
				Regicide.ADEN_TEMPLE_SPAWN), new ZergTeam(Regicide.ADEN_TOWN_SPAWN), Regicide.ADEN_TOWN,
				Regicide.ADEN_TEMPLE);
		e = new Event(240, ge);
		eg.add(e); // add to random event
		gem.registerStandaloneEvent(e, false);
		// TKotH
		ge = new TeamKingOfTheHill(1000, false, new Team(-87185, -50905, -10730), new Team(-81950, -54335, -10730),
				90000);
		e = new Event(2, "TKotH (Underground coliseum)", ge);
		eg.add(e); // add to random event
		EventGroup tkoth = new EventGroup(2, ge.getImplementationName());
		tkoth.add(e);
		ge = new TeamKingOfTheHill(1000, false, new Team(new Location(-13750, 278440, -11935), new Location(-11250,
				280950, -11935)), new Team(new Location(-11240, 278420, -11935), new Location(-13760, 280945, -11935)),
				90005);
		e = new Event(2, "TKotH (Tully's workshop)", ge);
		eg.add(e); // add to random event
		tkoth.add(e);
		gem.registerEventGroup(tkoth, false);
		// Random event
		gem.registerEventGroup(eg, false);
		
		// Mini-events
		gem.registerStandaloneEvent(new Event(new TagTheMob(30, false)), true);
		gem.registerStandaloneEvent(new Event(2, new SlaughterHouse(false)), true);
		
		_log.info("EventElection: registered " + gem.getEvents().size() + " event groups.");
		_log.info("EventElection: registered " + gem.getMiniEvents().size() + " mini event groups.");
	}
}
