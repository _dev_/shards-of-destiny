/*
 * MISSING LICENSING INFO
 */
package custom.board_npcs;

import com.l2jfree.gameserver.communitybbs.Manager.ItemAuctionBBSManager;
import com.l2jfree.gameserver.communitybbs.Manager.SettingsBBSManager;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.quest.Quest;

/**
 * @author savormix
 * 
 */
public final class BoardManagers extends Quest
{
	private static final int AUCTION_NPC = 90094;
	private static final int PERK_NPC = 90095;
	
	private BoardManagers()
	{
		super(-1, "CommunityBoardNpcs", "custom");
		
		addFirstTalkId(AUCTION_NPC);
		addFirstTalkId(PERK_NPC);
	}
	
	@Override
	public String onFirstTalk(L2Npc npc, L2PcInstance player)
	{
		if (npc.getNpcId() == AUCTION_NPC)
			ItemAuctionBBSManager.getInstance().showMenu(player);
		else
			SettingsBBSManager.getInstance().showPerks(player);
		return null;
	}
	
	public static void main(String[] args)
	{
		new BoardManagers();
	}
}
