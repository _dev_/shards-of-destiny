/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package custom.Faction;

import org.sod.model.L2PlayerData;

import com.l2jfree.gameserver.model.L2ItemInstance;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.base.ClassLevel;
import com.l2jfree.gameserver.model.quest.jython.QuestJython;
import com.l2jfree.gameserver.templates.item.L2Item;

/**
 * Faction tutorial/selection quest.
 * 
 * @author savormix
 * @deprecated old SoD content
 */
@SuppressWarnings("unused")
@Deprecated
public final class FactionInitiation extends QuestJython {
	private static final String FACTION_INITIATION = "75000_EastAndWest";
	private static final int MIN_ONLINE_TO_COUNT = 0;
	private static final int MAX_MEMBER_VARIATION = 10;
	// Equipment verification
	private static final int ARMOR_SET_NO_BODY = (L2Item.SLOT_FEET + L2Item.SLOT_GLOVES + L2Item.SLOT_HEAD);
	private static final int ARMOR_SET_TWO_PIECE = (ARMOR_SET_NO_BODY + L2Item.SLOT_CHEST + L2Item.SLOT_LEGS);
	private static final int ARMOR_SET_ONE_PIECE = (ARMOR_SET_NO_BODY + L2Item.SLOT_FULL_ARMOR);
	private static final int ARMOR_SET_MULTIPLE = (ARMOR_SET_TWO_PIECE + L2Item.SLOT_FULL_ARMOR);
	private static final int RING_ONE = 1 << 0;
	private static final int RING_TWO = 1 << 1;
	private static final int EARRING_ONE = 1 << 2;
	private static final int EARRING_TWO = 1 << 3;
	private static final int NECKLACE = 1 << 4;
	private static final int JEWEL_VERIFY = (RING_ONE + RING_TWO + EARRING_ONE + EARRING_TWO + NECKLACE);
	private static final int COMBAT_POTION = 5591;
	private static final int HEALING_POTION = 1073;
	private static final int MANA_POTION = 726;
	
	// Quest NPCs
	private static final int NPC_BUFFER = 90000;
	private static final int CLASS_CHANGER = 90008;
	private static final int WEAPON_SELLER = 90001;
	private static final int ARMOR_SELLER = 90002;
	private static final int JEWEL_SELLER = 90003;
	private static final int MISC_SELLER = 90004;
	private static final int EAST_MASTER = 90011;
	private static final int WEST_MASTER = 90012;
	private static final int[] NPC = {
			NPC_BUFFER, CLASS_CHANGER, WEAPON_SELLER, ARMOR_SELLER, JEWEL_SELLER, MISC_SELLER,
			EAST_MASTER, WEST_MASTER
	};
	private static final String JOINED_EAST = "Our empire is in it's Age of Splendor!";
	private static final String JOINED_WEST = "You'll soon see the Scions of Destiny!";
	private static final int ANY_MASTER = -2;
	private static final int ANY_NPC = -1;
	
	// Quest items
	
	// Quest monsters
	
	public FactionInitiation(int questId, String name, String descr) {
		super(questId, name, descr);
		for (int npc : NPC) {
			addFirstTalkId(npc);
			addTalkId(npc);
		}
	}
	
	/*
	 * @Override public final String onAdvEvent(String event, L2Npc npc, L2PcInstance player) { if
	 * ("90011-2.htm".equals(event)) {
	 * player.getSoDPlayer().joinFaction(ConquestFactionManager.EAST); npc.broadcastPacket(new
	 * NpcSay(npc.getObjectId(), SystemChatChannelId.Chat_Critical_Announce.ordinal(),
	 * npc.getNpcId(), JOINED_EAST)); if (!player.isGM()) RespawnManager.moveToFaction(player); }
	 * else if ("90012-2.htm".equals(event)) {
	 * player.getSoDPlayer().joinFaction(ConquestFactionManager.WEST); npc.broadcastPacket(new
	 * NpcSay(npc.getObjectId(), SystemChatChannelId.Chat_Critical_Announce.ordinal(),
	 * npc.getNpcId(), JOINED_WEST)); if (!player.isGM()) RespawnManager.moveToFaction(player); }
	 * return event; }
	 */
	@Override
	public final String onFirstTalk(L2Npc npc, L2PcInstance player) {
		int talker = getNextTalker(player);
		int npcId = npc.getNpcId();
		if (talker == ANY_NPC || talker == npcId) {
			if (npcId == NPC_BUFFER || npcId == CLASS_CHANGER) {
				npc.showChatWindow(player);
				return null;
			} else if (npcId == EAST_MASTER || npcId == WEST_MASTER) {
				if (player.isGM())
					return npcId + "-gm.htm";
				else if (talker == ANY_NPC)
					return npcId + "-3.htm";
			}
			return npcId + ".htm";
		} else if (talker == ANY_MASTER) {
			if (npcId == EAST_MASTER || npcId == WEST_MASTER)
				return npcId + ".htm";
			else
				return EAST_MASTER + "-needed.htm";
		} else
			return talker + "-needed.htm";
	}
	
	private static final int getNextTalker(L2PcInstance player) {
		L2PlayerData dat = player.getSoDPlayer();
		if (/* dat.getPermFaction() != ConquestFactionManager.NONE || */player.isGM())
			return ANY_NPC;
		else if (player.getClassId().getLevel().compareTo(ClassLevel.Third) < 0)
			return CLASS_CHANGER;
		else {
			boolean wpn = true;
			int arm = 0;
			int acv = 0; // to verify accessory count
			for (L2ItemInstance it : player.getInventory().getItems()) {
				if (it.getItem().getItemGrade() > L2Item.CRYSTAL_NONE) {
					if (it.isWeapon())
						wpn = false;
					else if (it.isArmor()) {
						if (it.getItem().getType2() == L2Item.TYPE2_ACCESSORY) {
							int part = it.getItem().getBodyPart();
							if (part == L2Item.SLOT_L_FINGER + L2Item.SLOT_R_FINGER) {
								if ((acv & RING_ONE) != 0)
									acv |= RING_TWO;
								else
									acv |= RING_ONE;
							} else if (part == L2Item.SLOT_L_EAR + L2Item.SLOT_R_EAR) {
								if ((acv & EARRING_ONE) != 0)
									acv |= EARRING_TWO;
								else
									acv |= EARRING_ONE;
							} else
								acv |= NECKLACE;
						} else
							arm |= it.getItem().getBodyPart();
					}
				}
			}
			if (wpn)
				return WEAPON_SELLER;
			else if (arm != ARMOR_SET_TWO_PIECE && arm != ARMOR_SET_ONE_PIECE
					&& arm != ARMOR_SET_MULTIPLE)
				return ARMOR_SELLER;
			else if (acv != JEWEL_VERIFY)
				return JEWEL_SELLER;
			else if (player.getInventory().getItemByItemId(COMBAT_POTION) == null
					|| player.getInventory().getItemByItemId(HEALING_POTION) == null
					|| player.getInventory().getItemByItemId(MANA_POTION) == null)
				return MISC_SELLER;
			else if (dat.getBuffs().getTemplates().isEmpty()) {
				return NPC_BUFFER;
			} else {
				// permanent faction selection!
				/*
				 * int total = L2World.getInstance().getAllPlayersCount(); int east, west; // seems
				 * a better idea to prevent GMs from affecting the overall trend
				 * ConquestFactionManager fm = ConquestFactionManager.getInstance(); if (total >
				 * MIN_ONLINE_TO_COUNT) { east = fm.getOnlineCount(ConquestFactionManager.EAST);
				 * west = fm.getOnlineCount(ConquestFactionManager.WEST); } else { east =
				 * fm.getTotalCount(ConquestFactionManager.EAST); west =
				 * fm.getTotalCount(ConquestFactionManager.WEST); } int var =
				 * Math.min(MAX_MEMBER_VARIATION, total / 10); if (east + var < west) return
				 * EAST_MASTER; else if (west + var < east) return WEST_MASTER; else
				 */
				return ANY_MASTER;
			}
		}
	}
	
	public static void main(String[] args) {
		new FactionInitiation(-1, FACTION_INITIATION, "custom");
	}
}
