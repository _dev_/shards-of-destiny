/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package custom.Faction;

import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.quest.QuestState;
import com.l2jfree.gameserver.model.quest.State;
import com.l2jfree.gameserver.model.quest.jython.QuestJython;

/**
 * Rework of 136_MoreThanMeetsTheEye to allow sub certification.
 * @author savormix
 */
public final class FactionCertification extends QuestJython
{
	private static final String MORE_THAN_MEETS_THE_EYE = "136_MoreThanMeetsTheEye";

	// Quest NPCs
	private static final int HARDIN = 30832;
	private static final int AVANT_GARDE = 32323;

	// Quest items
	private static final int TRANSFORM_BOOK = 9648;

	public FactionCertification(int questId, String name, String descr)
	{
		super(questId, name, descr);
		addStartNpc(HARDIN);
		addTalkId(HARDIN);
		addFirstTalkId(AVANT_GARDE);
	}

	@Override
	public final String onTalk(L2Npc npc, L2PcInstance talker)
	{
		QuestState qs = talker.getQuestState(MORE_THAN_MEETS_THE_EYE);
		if (qs == null)
			return NO_QUEST;
		else if (qs.isCompleted())
			return QUEST_DONE;
		if (!talker.isGM())
			return "Sorry, this quest is no longer available.";
		qs.giveItems(TRANSFORM_BOOK, 1);
		qs.setState(State.COMPLETED);
		qs.exitQuest(false);
		talker.sendPacket(SND_FINISH);
		return QUEST_DONE;
	}

	@Override
	public final String onFirstTalk(L2Npc npc, L2PcInstance player)
	{
		QuestState qs = player.getQuestState(MORE_THAN_MEETS_THE_EYE);
		if (qs != null && qs.isCompleted())
			return "32323-00.htm";
		else
		{
			npc.showChatWindow(player);
			return null;
		}
	}

	public static void main(String[] args)
	{
		new FactionCertification(136, MORE_THAN_MEETS_THE_EYE, "More than the eyes see");
	}
}
