/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package custom.Faction;

import java.util.Arrays;
import java.util.Map;

import org.sod.Calc;
import org.sod.model.ChallengeTemplate;

import com.l2jfree.gameserver.datatables.SpawnTable;
import com.l2jfree.gameserver.instancemanager.InstanceManager;
import com.l2jfree.gameserver.model.L2Skill;
import com.l2jfree.gameserver.model.L2Spawn;
import com.l2jfree.gameserver.model.Location;
import com.l2jfree.gameserver.model.actor.L2Attackable;
import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.instance.L2MonsterInstance;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.itemcontainer.PcInventory;
import com.l2jfree.gameserver.model.quest.QuestState;
import com.l2jfree.gameserver.model.quest.State;
import com.l2jfree.gameserver.model.quest.jython.QuestJython;
import com.l2jfree.gameserver.network.serverpackets.NpcSay;
import com.l2jfree.util.Rnd;

/**
 * @author savormix
 * 
 */
public class CleanNearbyLands extends QuestJython
{
	private static final String CLEAN_NEARBY_LANDS = "75002_CleanNearbyLands";
	private static final String LEVEL = "lvl";
	private static final TrustLevel[] LEVELS =
	{
			new TrustLevel(0, 0, 0, 0, 0),
			new TrustLevel(1, 753, 100, 100000, 0xAEA1, 90038, 90039, 90040, 90041, 90042, 90043, 90044),
			new TrustLevel(2, 827, 500, 200000, 0xAEA2, 90045, 90046, 90047, 90048, 90049),
			new TrustLevel(3, 812, 2000, 300000, 0xAEA3, 90050, 90051, 90052, 90053, 90054),
			new TrustLevel(4, 0, 0, 500000, 0xAEA4, 90055, 90056, 90057, 90058),
	};
	private static final int GENERIC_DROP_RATE = 750000;
	private static final Location[] PORT_SPOTS = new Location[]
	{
			new Location(-151215, 255305, -190), new Location(-149355, 256775, -55),
			new Location(-147475, 255315, -200), new Location(-149360, 253055, -135),
	};
	
	// Quest NPCs
	private static final int MANAGER = 90032;
	private static final int TELEPORTER = 90033;
	private static final int TRADER = 90034;
	
	// Quest items
	private static final int KINSHIP_CRYSTAL = 750;
	private static final int CRACKED_KINSHIP_CRYSTAL = 751;
	private static final int SWORD_OF_BLACK_ICE = 777;
	
	// Quest monsters
	private static final int CAPTAIN_CHANCE = 500;
	private static final int CAPTAIN_ACTIVITY = 7200 * 1000;
	private static final int CAPTAIN_UTHANKA = 90036;
	private static final String CAPTAIN_SPAWNED = "Shake in fear, all you who value your lives!";
	private static final String CAPTAIN_NEARBY = "I'll kill you in an instant!";
	private static final String CAPTAIN_ATTACKED = "I'll make you feel suffering like a flame that is never extinguished!";
	private static final String CAPTAIN_KILLED = "Uh… I'm not dying; I'm just disappearing for a moment… I'll resurrect again!";
	
	private L2Npc _captain;
	
	public CleanNearbyLands()
	{
		super(-1, CLEAN_NEARBY_LANDS, "custom");
		addStartNpc(MANAGER);
		addStartNpc(TELEPORTER);
		addStartNpc(TRADER);
		addFirstTalkId(MANAGER);
		addFirstTalkId(TELEPORTER);
		addFirstTalkId(TRADER);
		addTalkId(MANAGER);
		addTalkId(TELEPORTER);
		addTalkId(TRADER);
		for (TrustLevel tl : LEVELS)
		{
			for (int id : tl.getMonsters())
			{
				addKillId(id);
				// addSpawnId(id);
			}
		}
		addAggroRangeEnterId(CAPTAIN_UTHANKA);
		addAttackId(CAPTAIN_UTHANKA);
		addKillId(CAPTAIN_UTHANKA);
		// Will not be necessary in GE/Freya
		Map<Integer, L2Spawn> spawns = SpawnTable.getInstance().getSpawnTable();
		for (L2Spawn sp : spawns.values())
		{
			for (TrustLevel tl : LEVELS)
			{
				if (Arrays.binarySearch(tl.getMonsters(), sp.getNpcId()) >= 0)
				{
					sp.setInstanceId(tl.getInstanceId());
					L2Npc npc = sp.getLastSpawn();
					if (npc != null)
						npc.setInstanceId(tl.getInstanceId());
				}
			}
		}
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		QuestState qs = player.getQuestState(CLEAN_NEARBY_LANDS);
		if (QUEST_START_EVT.equals(event))
		{
			qs.set(LEVEL, 1);
			qs.setState(State.STARTED);
			qs.sendPacket(SND_ACCEPT);
			qs.giveItems(KINSHIP_CRYSTAL, 1);
			return "90032-3.htm";
		}
		else if ("2".equals(event))
		{
			int lvl = qs.getInt(LEVEL);
			TrustLevel tl = LEVELS[lvl];
			if (qs.getQuestItemsCount(tl.getSampleType()) >= tl.getSamplesToNext())
			{
				qs.takeItems(tl.getSampleType(), -1);
				qs.set(LEVEL, ++lvl);
				qs.sendPacket(SND_MIDDLE);
				if (lvl > 3)
					player.getSoDPlayer().tryAddChallengePoints(ChallengeTemplate.JURASSIC_PARK, 1);
			}
			switch (lvl)
			{
			case 1:
				return "90032-3.htm";
			case 2:
				return "90032-7.htm";
			case 3:
				return "90032-8.htm";
			default:
				return QUEST_DONE;
			}
		}
		else if ("90032-6.htm".equals(event))
		{
			long reward = qs.getQuestItemsCount(CRACKED_KINSHIP_CRYSTAL) / 10;
			if (reward > 0)
			{
				qs.takeItems(CRACKED_KINSHIP_CRYSTAL, reward * 10);
				qs.rewardItems(KINSHIP_CRYSTAL, reward);
				qs.sendPacket(SND_MIDDLE);
			}
		}
		else
		{
			int lvl = qs.getInt(LEVEL);
			if (event.length() == 2 && event.charAt(0) == 't')
			{
				int req = 0;
				try
				{
					req = Integer.parseInt(event.substring(1));
				}
				catch (Exception e)
				{
				}
				if (req > 0 && lvl >= req)
				{
					TrustLevel tl = LEVELS[req];
					player.setInstanceId(tl.getInstanceId());
					player.teleToLocation(Calc.getRandomFromArray(PORT_SPOTS));
				}
				return null;
			}
		}
		return event;
	}
	
	@Override
	public String onAggroRangeEnter(L2Npc npc, L2PcInstance player, boolean isPet)
	{
		if (npc instanceof L2MonsterInstance)
		{
			L2MonsterInstance mob = (L2MonsterInstance) npc;
			if (mob.getMostHated() == null)
			{
				npc.broadcastPacket(new NpcSay(npc, CAPTAIN_NEARBY));
				mob.addDamageHate(player, 0, 999);
				mob.callMinionsToAssist(player);
			}
		}
		return null;
	}
	
	@Override
	public String onAttack(L2Npc npc, L2PcInstance attacker, int damage, boolean isPet, L2Skill skill)
	{
		if (npc.getQuestAttackStatus() != ATTACK_SINGLE)
		{
			npc.setQuestAttackStatus(ATTACK_SINGLE);
			npc.broadcastPacket(new NpcSay(npc, CAPTAIN_ATTACKED));
		}
		return null;
	}
	
	@Override
	public String onEvent(String event, QuestState qs)
	{
		if ("90032-3.htm".equals(event))
		{
			qs.set(LEVEL, 1);
			qs.sendPacket(SND_MIDDLE);
		}/*
			else if ("90032-6.htm".equals(event))
			{
			long reward = qs.getQuestItemsCount(CRACKED_KINSHIP_CRYSTAL) / 10;
			if (reward > 0)
			{
				qs.takeItems(CRACKED_KINSHIP_CRYSTAL, reward * 10);
				qs.rewardItems(KINSHIP_CRYSTAL, reward);
				qs.sendPacket(SND_MIDDLE);
			}
			}*/
		return event;
	}
	
	@Override
	public String onFirstTalk(L2Npc npc, L2PcInstance player)
	{
		QuestState qs = player.getQuestState(CLEAN_NEARBY_LANDS);
		int lvl;
		if (qs == null)
			lvl = 0;
		else
			lvl = qs.getInt(LEVEL);
		int id = npc.getNpcId();
		switch (id)
		{
		case MANAGER:
			if (qs == null || !qs.isStarted())
				return "90032.htm";
			long count = qs.getQuestItemsCount(SWORD_OF_BLACK_ICE);
			if (count > 0)
			{
				qs.takeItems(SWORD_OF_BLACK_ICE, 1);
				qs.rewardItems(PcInventory.ADENA_ID, 20);
				qs.sendPacket(SND_MIDDLE);
			}
			TrustLevel tl = LEVELS[lvl];
			if (tl.getLevel() == 0)
			{
				qs.set(LEVEL, 1);
				qs.sendPacket(SND_MIDDLE);
				return "90032-3.htm";
			}
			else if (tl.getSampleType() == 0)
				return "90032-4.htm";
			else
				return "90032-5.htm";
		case TELEPORTER:
		case TRADER:
			return id + "-" + lvl + ".htm";
		}
		return NO_QUEST;
	}
	
	@Override
	public String onKill(L2Npc npc, L2PcInstance player, boolean isPet)
	{
		QuestState qs = player.getQuestState(CLEAN_NEARBY_LANDS);
		int lvl = qs.getInt(LEVEL);
		if (!qs.isStarted() || lvl < 1)
			return null;
		if (npc.getNpcId() == CAPTAIN_UTHANKA)
		{
			npc.broadcastPacket(new NpcSay(npc, CAPTAIN_KILLED));
			qs.dropQuestItems(SWORD_OF_BLACK_ICE, 1, 1, 1000000, true);
			if (npc instanceof L2MonsterInstance)
				((L2MonsterInstance) npc).deleteSpawnedMinions();
		}
		L2PcInstance rewarded = null;
		for (int i = 1; i < LEVELS.length && rewarded == null; i++)
			rewarded = getRandomPartyMember(player, LEVEL, String.valueOf(i));
		if (rewarded == null)
			rewarded = player;
		qs = rewarded.getQuestState(CLEAN_NEARBY_LANDS);
		lvl = qs.getInt(LEVEL);
		TrustLevel tl = LEVELS[lvl];
		boolean divide = !tl.isOfLevel(npc.getNpcId());
		if (tl.getSamplesToNext() > 0)
			qs.dropQuestItems(tl.getSampleType(), 1, tl.getSamplesToNext(), GENERIC_DROP_RATE / (divide ? 5 : 1), true,
					false);
		qs.dropQuestItems(CRACKED_KINSHIP_CRYSTAL, 1, Long.MAX_VALUE, tl.getFineDropChance() / (divide ? 5 : 1), true,
				false);
		if (npc.getNpcId() != CAPTAIN_UTHANKA && (_captain == null || _captain.isDecayed())
				&& Rnd.get(CAPTAIN_CHANCE) == 0)
		{
			_captain = addSpawn(CAPTAIN_UTHANKA, npc.getX(), npc.getY(), npc.getZ(), player.getHeading(), true,
					CAPTAIN_ACTIVITY, npc.getInstanceId());
			_captain.broadcastPacket(new NpcSay(_captain, CAPTAIN_SPAWNED));
			if (_captain instanceof L2Attackable)
			{
				_captain.setIsRunning(true);
				((L2Attackable) _captain).addDamageHate(player, 0, 999);
				if (_captain instanceof L2MonsterInstance)
					((L2MonsterInstance) _captain).callMinionsToAssist(player);
			}
		}
		return null;
	}
	
	@Override
	public String onSpawn(L2Npc npc)
	{
		int instanceId = 0;
		for (TrustLevel tl : LEVELS)
			if (Arrays.binarySearch(tl.getMonsters(), npc.getNpcId()) >= 0)
				instanceId = tl.getInstanceId();
		npc.setInstanceId(instanceId);
		return null;
	}
	
	public static void main(String[] args)
	{
		new CleanNearbyLands();
	}
	
	private static class TrustLevel
	{
		private final int _level;
		private final int _sampleType;
		private final int _samplesToNext;
		private final int _fineDropChance;
		private final int _instanceId;
		private final int[] _monsters;
		
		@SuppressWarnings("deprecation")
		private TrustLevel(int level, int sampleType, int samplesToNext, int fineDropChance, int instanceId,
				int... monsters)
		{
			_level = level;
			_sampleType = sampleType;
			_samplesToNext = samplesToNext;
			_fineDropChance = fineDropChance;
			_instanceId = instanceId;
			_monsters = monsters;
			InstanceManager.getInstance().createInstance(getInstanceId());
		}
		
		public int getLevel()
		{
			return _level;
		}
		
		public int getSampleType()
		{
			return _sampleType;
		}
		
		public int getSamplesToNext()
		{
			return _samplesToNext;
		}
		
		public int getFineDropChance()
		{
			return _fineDropChance;
		}
		
		public int getInstanceId()
		{
			return _instanceId;
		}
		
		public int[] getMonsters()
		{
			return _monsters;
		}
		
		public boolean isOfLevel(int monsterId)
		{
			return Arrays.binarySearch(getMonsters(), monsterId) >= 0;
		}
	}
}
