/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package custom.Faction;

import java.util.Arrays;

import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.itemcontainer.PcInventory;
import com.l2jfree.gameserver.model.quest.QuestState;
import com.l2jfree.gameserver.model.quest.State;
import com.l2jfree.gameserver.model.quest.jython.QuestJython;

/**
 * @author savormix
 *
 */
public class DutyCalls extends QuestJython
{
	private static final String DUTY_CALLS = "75003_DutyCalls";
	private static final int NPC = 90035;
	
	private static final int SEALED_SCROLL = 767;
	private static final int MOONLIGHT_WHISPER = 789;
	private static final int REWARD_SCROLL = 5;
	private static final int REWARD_SONG = 8;
	
	private static final int[] GUARDS_NORMAL = {
		// Gludio
		35067, 35068, 35069, 35070, 35071, 35072, 35073, 35074, 35075, 35076, 35077, 35078,
		35080, 35081, 35082, 35083, 35084, 35085, 35086, 35087, 35088, 35089, 35090, 35091,
		// Dion
		35109, 35110, 35111, 35112, 35113, 35114, 35115, 35116, 35117, 35118, 35119, 35120,
		35122, 35123, 35124, 35125, 35126, 35127, 35128, 35129, 35130, 35131, 35132, 35133,
		// Giran
		35151, 35152, 35153, 35154, 35155, 35156, 35157, 35158, 35159, 35160, 35161, 35162,
		35164, 35165, 35166, 35167, 35168, 35169, 35170, 35171, 35172, 35173, 35174, 35175,
		// Oren
		35193, 35194, 35195, 35196, 35197, 35198, 35199, 35200, 35201, 35202, 35203, 35204,
		35206, 35207, 35208, 35209, 35210, 35211, 35212, 35213, 35214, 35215, 35216, 35217,
		// Innadril
		35283, 35284, 35285, 35286, 35287, 35288, 35289, 35290, 35291, 35292, 35293, 35294,
		35296, 35297, 35298, 35299, 35300, 35301, 35302, 35303, 35304, 35305, 35306, 35307,
		// Goddard
		35327, 35328, 35329, 35330, 35331, 35332, 35333, 35334, 35335, 35336, 35337, 35338,
		35340, 35341, 35342, 35343, 35344, 35345, 35346, 35347, 35348, 35349, 35350, 35351,
		// Schuttgart
		35519, 35520, 35521, 35522, 35523, 35524, 35525, 35526, 35527, 35528, 35529, 35530,
		35532, 35533, 35534, 35535, 35536, 35537, 35538, 35539, 35540, 35541, 35542, 35543,
	};
	private static final int[] GUARDS_STRONG = {
		// Aden
		35236, 35237, 35238, 35239, 35240, 35241, 35242, 35243, 35244, 35245, 35246, 35247,
		35249, 35250, 35251, 35252, 35253, 35254, 35255, 35256, 35257, 35258, 35259, 35260,
		// Rune
		35472, 35473, 35474, 35475, 35476, 35477, 35478, 35479, 35480, 35481, 35482, 35483,
		35485, 35486, 35487, 35488, 35489, 35490, 35491, 35492, 35493, 35494, 35495, 35496,
	};
	
	public DutyCalls()
	{
		super(-1, DUTY_CALLS, "custom");
		addStartNpc(NPC);
		addFirstTalkId(NPC);
		addTalkId(NPC);
		for (int id : GUARDS_NORMAL)
			addKillId(id);
		for (int id : GUARDS_STRONG)
			addKillId(id);
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		QuestState qs = player.getQuestState(DUTY_CALLS);
		//if (qs == null)
		//	qs = newQuestState(player);
		if (QUEST_START_EVT.equals(event))
		{
			qs.setState(State.STARTED);
			qs.sendPacket(SND_ACCEPT);
			return "90035-1.htm";
		}
		return null;
	}
	
	@Override
	public String onFirstTalk(L2Npc npc, L2PcInstance player)
	{
		QuestState qs = player.getQuestState(DUTY_CALLS);
		if (qs != null && qs.isStarted())
		{
			long norm = qs.getQuestItemsCount(SEALED_SCROLL);
			long fine = qs.getQuestItemsCount(MOONLIGHT_WHISPER);
			if (norm > 0 || fine > 0)
			{
				qs.takeItems(SEALED_SCROLL, norm);
				qs.takeItems(MOONLIGHT_WHISPER, fine);
				qs.rewardItems(PcInventory.ADENA_ID, fine * REWARD_SONG + norm * REWARD_SCROLL);
				qs.sendPacket(SND_MIDDLE);
				return "90035-3.htm";
			}
			else
				return "90035-2.htm";
		}
		else
			return "90035.htm";
	}
	
	@Override
	public String onKill(L2Npc npc, L2PcInstance killer, boolean isPet)
	{
		QuestState qs = killer.getQuestState(DUTY_CALLS);
		if (qs == null || !qs.isStarted())
			return null;
		int reward;
		//if (ArrayUtils.contains(GUARDS_STRONG, npc.getNpcId()))
		if (Arrays.binarySearch(GUARDS_STRONG, npc.getNpcId()) >= 0)
			reward = MOONLIGHT_WHISPER;
		else
			reward = SEALED_SCROLL;
		qs.dropQuestItems(reward, 1, Long.MAX_VALUE, 650000, true, false);
		return null;
	}
	
	public static void main(String[] args)
	{
		new DutyCalls();
	}
}
