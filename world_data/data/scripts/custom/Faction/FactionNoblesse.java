/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package custom.Faction;

import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.gameserver.model.itemcontainer.PcInventory;
import com.l2jfree.gameserver.model.quest.QuestState;
import com.l2jfree.gameserver.model.quest.jython.QuestJython;

/**
 * @author savormix
 * @deprecated old SoD content
 */
@Deprecated
public class FactionNoblesse extends QuestJython {
	private static final String FACTION_NOBLESSE = "75001_NobleBlood";
	
	// Quest NPCs
	private static final int HISTORIAN = 90023;
	
	// Quest items
	private static final int ARTICLES_OF_DEAD_HEROES = 4269;
	private static final int NOBLESS_TIARA = 7694;
	
	// Other
	private static final int DOCUMENT_REWARD = 100;
	
	public FactionNoblesse(int questId, String name, String descr) {
		super(questId, name, descr);
		addStartNpc(HISTORIAN);
		addFirstTalkId(HISTORIAN);
		addTalkId(HISTORIAN);
		questItemIds = new int[] {
			ARTICLES_OF_DEAD_HEROES
		};
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player) {
		QuestState qs = player.getQuestState(FACTION_NOBLESSE);
		if ("90023-2.htm".equals(event)) {
			long count = qs.getQuestItemsCount(ARTICLES_OF_DEAD_HEROES);
			if (count < 1)
				return "90023-3.htm";
			if (!player.isNoble() && player.getLevel() >= 79) {
				qs.takeItems(ARTICLES_OF_DEAD_HEROES, 1);
				player.setNoble(true);
				qs.giveItems(NOBLESS_TIARA, 1);
				player.broadcastFullInfo(); // show noble symbol in status
			} else {
				long reward = count * DOCUMENT_REWARD;
				qs.takeItems(ARTICLES_OF_DEAD_HEROES, count);
				qs.rewardItems(PcInventory.ADENA_ID, reward);
				return "90023-4.htm";
			}
		} else if ("90023-5.htm".equals(event)) {
			// TODO: relics
			return "90023-6.htm";
		}
		return event;
	}
	
	@Override
	public String onFirstTalk(L2Npc npc, L2PcInstance talker) {
		return onTalk(npc, talker);
	}
	
	@Override
	public String onTalk(L2Npc npc, L2PcInstance talker) {
		return "90023.htm";
	}
	
	public static void main(String[] args) {
		new FactionNoblesse(-1, FACTION_NOBLESSE, "custom");
	}
}
