/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package village_master;

import java.util.Locale;

import org.sod.L2Htm;
import org.sod.i18n.Internationalization;

import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.lang.L2TextBuilder;

/**
 * @author savormix
 * 
 */
public final class Alliance extends VillageMaster
{
	private static final String ALLIANCE = "9001_alliance";
	
	public Alliance(int questId, String name, String descr)
	{
		super(questId, name, descr);
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		
		if (player.getClan() == null)
		{
			L2TextBuilder tb = new L2TextBuilder();
			L2Htm.writeHeader(tb, null);
			tb.append(i18n.get(loc, "NPC_VILLAGE_MASTER_ALLIANCE_CLAN_REQUIRED"));
			L2Htm.writeFooter(tb);
			return tb.moveToString();
		}
		
		AllyRequest ar = L2Htm.getRequest(event, AllyRequest.values());
		if (ar == null)
			return super.onAdvEvent(event, npc, player);
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, null);
		switch (ar)
		{
		case SHOW_CREATE:
			tb.append(i18n.get(loc, "NPC_VILLAGE_MASTER_ALLIANCE_ENTER_NAME"));
			tb.append("<br>");
			L2Htm.writeTextField(tb, "name", 250, 45);
			tb.append("<br>");
			L2Htm.writeNpcButton(tb, npc.getObjectId(), "create_ally", "$name", 250, 40, true,
					i18n.get(loc, "NPC_VILLAGE_MASTER_ALLIANCE_CREATE"));
			L2Htm.writeQuestButton(tb, ALLIANCE, AllyRequest.CANCEL, 250, 40, true, i18n.get(loc, "CANCEL"));
			break;
		case SHOW_DISSOLVE:
			tb.append(i18n.get(loc, "NPC_VILLAGE_MASTER_ALLIANCE_DISSOLUTION"));
			tb.append("<br>");
			L2Htm.writeNpcButton(tb, npc.getObjectId(), "dissolve_ally", null, 250, 40, true,
					i18n.get(loc, "NPC_VILLAGE_MASTER_ALLIANCE_DISSOLVE"));
			L2Htm.writeQuestButton(tb, ALLIANCE, AllyRequest.CANCEL, 250, 40, true, i18n.get(loc, "CANCEL"));
			break;
		case CANCEL:
			tb.moveToString();
			return onTalk(npc, player);
		default:
			break;
		}
		L2Htm.writeFooter(tb);
		return tb.moveToString();
	}
	
	@Override
	public String onTalk(L2Npc npc, L2PcInstance talker)
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = talker.getSoDPlayer().getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, null);
		L2Htm.writeQuestButton(tb, ALLIANCE, AllyRequest.SHOW_CREATE, 250, 40, true,
				i18n.get(loc, "NPC_VILLAGE_MASTER_ALLIANCE_CREATE"));
		L2Htm.writeQuestButton(tb, ALLIANCE, AllyRequest.SHOW_DISSOLVE, 250, 40, true,
				i18n.get(loc, "NPC_VILLAGE_MASTER_ALLIANCE_DISSOLVE"));
		L2Htm.writeFooter(tb);
		return tb.moveToString();
	}
	
	public static void main(String[] args)
	{
		new Alliance(-1, ALLIANCE, "village_master");
	}
	
	public enum AllyRequest
	{
		SHOW_CREATE, SHOW_DISSOLVE, CANCEL;
	}
}
