/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package village_master;

import java.util.Locale;

import org.sod.L2Htm;
import org.sod.i18n.Internationalization;

import com.l2jfree.gameserver.model.actor.L2Npc;
import com.l2jfree.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfree.lang.L2TextBuilder;

/**
 * @author savormix
 * 
 */
public final class Clan extends VillageMaster
{
	private static final String CLAN = "9000_clan";
	
	public Clan(int questId, String name, String descr)
	{
		super(questId, name, descr);
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = player.getSoDPlayer().getSettings().getLocale();
		
		ClanRequest cr = L2Htm.getRequest(event, ClanRequest.values());
		if (cr == null)
			return super.onAdvEvent(event, npc, player);
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, null);
		switch (cr)
		{
		case SHOW_CREATE:
			tb.append(i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_ENTER_NAME"));
			tb.append("<br>");
			L2Htm.writeTextField(tb, "name", 250, 45);
			tb.append("<br>");
			L2Htm.writeNpcButton(tb, npc.getObjectId(), "create_clan", "$name", 250, 40, true,
					i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_CREATE"));
			L2Htm.writeQuestButton(tb, CLAN, ClanRequest.CLAN_MENU, 250, 40, true, i18n.get(loc, "CANCEL"));
			break;
		case SHOW_LEVELUP:
			if (!player.isClanLeader())
			{
				tb.append(i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_LEADER_ONLY"));
				break;
			}
			tb.append(i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_LEVEL_UP_REQUIREMENTS"));
			tb.append("<br>");
			for (int i = 1; i <= 11; i++)
			{
				tb.append("<font color=\"LEVEL\">");
				tb.append(i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_LEVEL", "%level%", String.valueOf(i)));
				tb.append("</font>: ");
				String req = i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_LEVEL_UP_REQUIREMENTS_" + i);
				if (req.isEmpty())
					req = i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_LEVEL_UP_REQUIREMENTS_HIDDEN");
				tb.append(req);
				tb.append("<br>");
			}
			tb.append("<br>");
			L2Htm.writeNpcButton(tb, npc.getObjectId(), "increase_clan_level", null, 250, 40, true,
					i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_LEVEL_UP"));
			L2Htm.writeQuestButton(tb, CLAN, ClanRequest.CLAN_MENU, 250, 40, true, i18n.get(loc, "CANCEL"));
			break;
		case SHOW_DISSOLVE:
			if (!player.isClanLeader())
			{
				tb.append(i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_LEADER_ONLY"));
				break;
			}
			tb.append(i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_DISSOLUTION_DISCLAIMER"));
			tb.append("<br>");
			L2Htm.writeNpcButton(tb, npc.getObjectId(), "dissolve_clan", null, 250, 40, true,
					i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_DISSOLVE"));
			L2Htm.writeQuestButton(tb, CLAN, ClanRequest.CLAN_MENU, 250, 40, true, i18n.get(loc, "CANCEL"));
			break;
		case SHOW_DISSOLVE_CANCEL:
			if (!player.isClanLeader())
			{
				tb.append(i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_LEADER_ONLY"));
				break;
			}
			tb.append(i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_RECOVERY_DISCLAIMER"));
			tb.append("<br>");
			L2Htm.writeNpcButton(tb, npc.getObjectId(), "recover_clan", null, 250, 40, true,
					i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_CANCEL_DISSOLVE"));
			L2Htm.writeQuestButton(tb, CLAN, ClanRequest.CLAN_MENU, 250, 40, true, i18n.get(loc, "CANCEL"));
			break;
		case SHOW_DELEGATE_LEADERSHIP:
			if (!player.isClanLeader())
			{
				tb.append(i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_LEADER_ONLY"));
				break;
			}
			tb.append(i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_DELEGATION_DISCLAIMER"));
			tb.append("<br>");
			L2Htm.writeQuestButton(tb, CLAN, ClanRequest.SHOW_DELEGATE_INIT, 250, 40, true,
					i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_REQUEST_DELEGATION"));
			L2Htm.writeQuestButton(tb, CLAN, ClanRequest.SHOW_DELEGATE_CANCEL, 250, 40, true,
					i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_CANCEL_DELEGATION"));
			L2Htm.writeQuestButton(tb, CLAN, ClanRequest.CLAN_MENU, 250, 40, true, i18n.get(loc, "CANCEL"));
			break;
		case SHOW_DELEGATE_CANCEL:
			if (!player.isClanLeader())
			{
				tb.append(i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_LEADER_ONLY"));
				break;
			}
			tb.append(i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_DELEGATION_CANCELLATION_DISCLAIMER"));
			tb.append("<br>");
			// not done
			L2Htm.writeQuestButton(tb, CLAN, ClanRequest.CLAN_MENU, 250, 40, true,
					i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_CANCEL_DELEGATION"));
			L2Htm.writeQuestButton(tb, CLAN, ClanRequest.CLAN_MENU, 250, 40, true, i18n.get(loc, "CANCEL"));
			break;
		case SHOW_DELEGATE_INIT:
			if (!player.isClanLeader())
			{
				tb.append(i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_LEADER_ONLY"));
				break;
			}
			tb.append(i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_DELEGATION_ENTER_NAME"));
			tb.append("<br>");
			L2Htm.writeTextField(tb, "name", 250, 45);
			tb.append("<br>");
			L2Htm.writeNpcButton(tb, npc.getObjectId(), "change_clan_leader", "$name", 250, 40, true,
					i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_DELEGATE_LEADERSHIP"));
			L2Htm.writeQuestButton(tb, CLAN, ClanRequest.CLAN_MENU, 250, 40, true, i18n.get(loc, "CANCEL"));
			break;
		case SHOW_ACADEMY:
		case SHOW_UNIT_GUARD:
		case SHOW_UNIT_KNIGHT:
		case CLAN_MENU:
			tb.moveToString();
			return onTalk(npc, player);
		default:
			break;
		}
		L2Htm.writeFooter(tb);
		return tb.moveToString();
	}
	
	@Override
	public String onTalk(L2Npc npc, L2PcInstance talker)
	{
		Internationalization i18n = Internationalization.getInstance();
		Locale loc = talker.getSoDPlayer().getSettings().getLocale();
		
		L2TextBuilder tb = new L2TextBuilder();
		L2Htm.writeHeader(tb, null);
		L2Htm.writeQuestButton(tb, CLAN, ClanRequest.SHOW_CREATE, 250, 40, true,
				i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_CREATE"));
		L2Htm.writeQuestButton(tb, CLAN, ClanRequest.SHOW_LEVELUP, 250, 40, true,
				i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_LEVEL_UP"));
		L2Htm.writeQuestButton(tb, CLAN, ClanRequest.SHOW_DISSOLVE, 250, 40, true,
				i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_DISSOLVE"));
		L2Htm.writeQuestButton(tb, CLAN, ClanRequest.SHOW_DISSOLVE_CANCEL, 250, 40, true,
				i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_CANCEL_DISSOLVE"));
		L2Htm.writeNpcButton(tb, npc.getNpcId(), "learn_clan_skills", null, 250, 40, true,
				i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_LEARN_SKILLS"));
		L2Htm.writeQuestButton(tb, CLAN, ClanRequest.SHOW_DELEGATE_LEADERSHIP, 250, 40, true,
				i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_DELEGATE_LEADERSHIP"));
		L2Htm.writeQuestButton(tb, CLAN, ClanRequest.SHOW_ACADEMY, 250, 40, true,
				i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_MANAGE_ACADEMY"));
		L2Htm.writeQuestButton(tb, CLAN, ClanRequest.SHOW_UNIT_GUARD, 250, 40, true,
				i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_MANAGE_GUARD_UNITS"));
		L2Htm.writeQuestButton(tb, CLAN, ClanRequest.SHOW_UNIT_KNIGHT, 250, 40, true,
				i18n.get(loc, "NPC_VILLAGE_MASTER_CLAN_MANAGE_KNIGHT_UNITS"));
		L2Htm.writeFooter(tb);
		return tb.moveToString();
	}
	
	public static void main(String[] args)
	{
		new Clan(-1, CLAN, "village_master");
	}
	
	public enum ClanRequest
	{
		CLAN_MENU, SHOW_CREATE, SHOW_LEVELUP, SHOW_DISSOLVE, SHOW_DISSOLVE_CANCEL, SHOW_DELEGATE_LEADERSHIP, SHOW_DELEGATE_INIT, SHOW_DELEGATE_CANCEL, SHOW_ACADEMY, SHOW_UNIT_GUARD, SHOW_UNIT_KNIGHT;
	}
}
